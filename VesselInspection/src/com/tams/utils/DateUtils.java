package com.tams.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;

/**
 * 
 * @author Ripunjay Shukla
 * @since 15 May 2015
 * 
 */
@SuppressLint("SimpleDateFormat")
public class DateUtils {
	public final static String _YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";
	public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public final static String YYYYMMDDHHMM = "yyyy/MM/dd hh:mm";
	public final static String YYYYMMDDHHMMA = "yyyy/MM/dd hh:mm a";
	public final static String MMDDYYYYHHMMSS = "MM/dd/yyyy hh:mm:ss a";
	public final static String DDMMYYYYHHMMSS = "dd/MM/yyyy hh:mm:ss";
	public final static String DATEFORMAT = "MM/dd/yyyy";
	public final static String YYYYMMDD = "yyyyMMdd";
	public final static String DDMMYYYY = "dd/MM/yyyy";
	public final static String DDMMYYYY_ = "dd-MM-yyyy";
	public final static String YYYYMMDD_ = "yyyy-MM-dd";
	public final static String DDMMYYYY_DAY = "dd-MM-yyyy (EEEE)";
	public final static String EEE_MMDD = "EEE MM/dd";
	public final static String MMM_DD_YYYY = "MMM dd, yyyy";
	public final static String _YYYYMMDDTHHMMSS = "yyyy-MM-dd'T'HH:mm:ss";

	/**
	 * This method give today date
	 * 
	 * @param fmt
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getTodaydate(String fmt) {
		Date date = new Date();
		return new SimpleDateFormat(fmt).format(date);
	}

	/**
	 * This method give today date
	 * 
	 * @param fmt
	 *            ,locale
	 * @return date
	 */
	public static String getTodaydate(String fmt, Locale locale) {
		Date date = new Date();
		return new SimpleDateFormat(fmt, locale).format(date);
	}

	/**
	 * This method give next date
	 * 
	 * @param fmt
	 *            ,date,day
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static Calendar getNextdate(String date, String fmt, int day) {
		Calendar calendar = Calendar.getInstance();
		try {
			Date d = new SimpleDateFormat(fmt).parse(date);
			calendar.setTime(d);
			calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);

			return calendar;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return calendar;
	}

	/**
	 * This method give format date
	 * 
	 * @param date
	 * @param fmt
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDate(Date date, String fmt) {
		return new SimpleDateFormat(fmt).format(date);
	}

	/**
	 * This method give today date time
	 * 
	 * @return today date time
	 */
	public static String getTodaydatetime() {
		String date = DateUtils.getTodaydate(DateUtils.YYYYMMDDHHMMSS);
		return DateUtils.formatDate(date, DateUtils.YYYYMMDDHHMMSS,
				DateUtils._YYYYMMDDHHMMSS);
	}

	/**
	 * It will provide parse date
	 * 
	 * @param date
	 * @return date
	 */
	public static Date parseDate(String date) {
		return parseDate(date, DDMMYYYY_);
	}

	/**
	 * It will provide parse date
	 * 
	 * @param date
	 * @param fmt
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static Date parseDate(String date, String fmt) {
		Date d = null;
		try {
			d = new SimpleDateFormat(fmt).parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}

	/**
	 * This method will give format date
	 * 
	 * @param date
	 * @param ori
	 * @param fmt
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDate(String date, String ori, String fmt) {
		try {
			Date d = new SimpleDateFormat(ori).parse(date);
			date = new SimpleDateFormat(fmt).format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * This method is to get future date
	 * 
	 * @param date
	 * @param days
	 * @return date
	 */
	public static String getFutureDate(Date date, int days) {
		return getFutureDate(date, days, DDMMYYYY_DAY);
	}

	/**
	 * This method is to get future date
	 * 
	 * @param date
	 * @param days
	 * @return date
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getFutureDate(Date date, int days, String fmt) {
		long datetime = getFutureDatetime(date, days);
		return new SimpleDateFormat(fmt).format(datetime);
	}

	/**
	 * This method is to get future date time
	 * 
	 * @param date
	 * @param days
	 * @return date
	 */
	public static Long getFutureDatetime(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + days);

		return calendar.getTimeInMillis();
	}

	/**
	 * This method will give back date
	 * 
	 * @param date
	 * @param days
	 * @return date
	 */
	public static String getBackDate(Date date, int days) {
		return getBackDate(date, days, DDMMYYYY_DAY);
	}

	/**
	 * This method will give back date
	 * 
	 * @param date
	 * @param days
	 * @param fmt
	 * @return date
	 */
	public static String getBackDate(Date date, int days, String fmt) {
		long datetime = getBackDatetime(date, days);
		return new SimpleDateFormat(fmt).format(datetime);
	}

	/**
	 * This method give back date time
	 * 
	 * @param date
	 * @param days
	 * @return long
	 */
	public static Long getBackDatetime(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - days);

		return calendar.getTimeInMillis();
	}

	/**
	 * This method will provide current date
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @param format
	 * @return current date
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getCalendarDate(int year, int month, int day,
			String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, day);
		Date date = calendar.getTime();

		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * This method will give backDateTime
	 * 
	 * @return backdateTime
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getBackdatetime() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2000);
		calendar.set(Calendar.MONTH, Calendar.JANUARY);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date date = calendar.getTime();
		return new SimpleDateFormat(YYYYMMDDHHMMSS).format(date);
	}

	/**
	 * this is to compare 2 dates
	 * 
	 * @param date1
	 * @param date2
	 * @return 0 means date1 is earlier than date2 or vice versa
	 */
	public static int compareDate(String date1, String date2, String format) {
		Date dateone = parseDate(date1, format);
		Date datetwo = parseDate(date2, format);
		if (dateone.before(datetwo))
			return 0;
		else
			return 1;
	}

	/**
	 * This method set dateTime to zero
	 * 
	 * @param input
	 * @param ori
	 * @param format
	 * @return date
	 */
	public static String setDateZeroTime(String input, String ori, String format) {
		Date date = parseDate(input, ori);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date output = calendar.getTime();
		String strDate = formatDate(output, format);
		return strDate;
	}

}
