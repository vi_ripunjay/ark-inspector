package com.tams.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tams.model.DeviceSyncTime;
import com.tams.model.FilledForm;
import com.tams.model.SyncHistory;
import com.tams.model.SyncStatus;
import com.tams.model.VesselInspection;
import com.tams.service.BackGroundTask;
import com.tams.sql.DBManager;
import com.tams.vessalinspection.LoginActivity;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.VessalInspectionHome;

/**
 * @file CommonUtil . java
 * 
 * @author Ripunjay Shukla
 * 
 * @date : May 13 2015
 */

public class CommonUtil {
	public static final String SERVER_ADDRESS = "server_address";
	public static final String SERVER_PORT = "server_port";
	public static final String SHIP_ID = "ship_id";
	public static final String SYNC_SHIP_ID = "sync_ship_id";
	public static final String TENANT_ID = "tenant_id";
	public static final String MAC_ID = "mac_id";
	public static final String USER_NAME = "user-name";
	public static final String PASSWORD = "password";
	public static final String SESSION = "session";
	public static final String MANUALSPINNERBUTTON = "manual_Spinner_button";
	public static final String CHAPTERSPINNERBUTTON = "chapter_Spinner_button";
	public static final String USERSPINNERBUTTON = "user_Spinner_button";
	public static final String MANUALID = "manual_id";
	public static final String CHAPTERSPINNERTITLE = "chapter_Spinner_Title";
	public static final String MANUALSPINNERTITLE = "manual_spinner_title";
	public static final String USER_NAME_TITLE = "user_spinner_title";
	public static final String SEARCHSTRING = "search_string";
	public static final String DEVICE_REGISTRATION_STATUS = "device_reg_status";
	public static int HAS_MORE_STATUS = 1;
	public static int HAS_MORE_FORM_STATUS = 1;
	public static int CURRENT_FORM_COUNT = 0;
	public static String VI_TITLE = "";
	public static final String VESSEL_INSPECTION_ID = "viid";
	public static final String USER_ID = "uid";
	public static final String ROLE_ID = "rid";
	public static final String DATA_ACCESS_MODE = "WiFi";
	public static final String USER_FIRST_NAME = "user_first_name";
	public static final String USER_LAST_NAME = "user_last_name";
	public static String editedData = "editedData";
	public static String synckFrom = "syncFrom";
	public static final String USER_LOGIN = "0";
	public static final String LAST_ACTIVE_SHIP = "last_active_ship";
	public static final String ACTIVE_INSPECTION_ID = "activeInspectionId";
	public static final String CALLING_THROUGH = "callingThrough";
	public static final String ACTIVATING_SHIP = "activatingShip";
	public static final String DESC_TYPE_HEADER = "HEADER";
	public static final String DESC_TYPE_FOOTER = "FOOTER";
	public static final String COLOR_CONDITION="COLORCONDITION";
	public static final String ALL_INSPECTION_IDS_FOR_UNDIRTY = "undirtyids";
	public static final String LOGIN_FROM_SHIP = "ship";
	public static final String SEND_TEMPLATE_DATA = "send";
	public static final String SYNC_UUID = "uuid";
	public static final String FILE_NOT_MATCH = "fnm";
	public static final String DROP_DB_FLAG = "ddf";

	public static boolean IS_SYNC_DONE = false;

	/**
	 * This method will IMEI number of current device
	 * 
	 * @param context
	 * @return IMEI number
	 */
	public static String getIMEI(final Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		Log.i("imei",
				""
						+ (telephonyManager.getDeviceId() != null ? telephonyManager
								.getDeviceId() : "ripunjayshukla"));
		if (!(telephonyManager.getDeviceId() != null && telephonyManager
				.getDeviceId().length() > 0)) {
			// get mac Id -start
			WifiManager wifiManager = (WifiManager) context
					.getSystemService(VessalInspectionHome.WIFI_SERVICE);
			boolean wifiEnable = wifiManager.isWifiEnabled();
			if (wifiEnable == false) {
				AlertDialog.Builder wifiDialog = new AlertDialog.Builder(
						context);
				wifiDialog.setTitle("Information");
				wifiDialog
						.setMessage("wifi is disable,can you want to enable it ?");

				wifiDialog.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								((Activity) (context))
										.startActivity(new Intent(
												Settings.ACTION_WIFI_SETTINGS));
								dialog.dismiss();
							}
						});
				wifiDialog.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});
				wifiDialog.show();

			} else {
				WifiInfo info = wifiManager.getConnectionInfo();
				return info.getMacAddress();
			}
			// get mac Id -end
		}
		return (telephonyManager.getDeviceId() != null ? telephonyManager
				.getDeviceId() : "ripunjayshukla");// telephonyManager.getDeviceId();
	}

	/**
	 * This method set the server address in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setServerAddress(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SERVER_ADDRESS, string);
		edit.commit();
	}

	public static void updateServerAddress(Context context, String serverAddress) {

		try {
			DBManager db = new DBManager(context);
			db.open();
			List<SyncStatus> syncStatusList = db.getSyncStatusData();

			if (syncStatusList != null && syncStatusList.size() > 0) {

				SyncStatus syncStatus = syncStatusList.get(0);
				syncStatus.setServerAddress(serverAddress);
				db.updateSyncStatusTable(syncStatus);
			}
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * This method set the chapter spinner title address in the shared
	 * preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setChapterSpinnerTitle(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(CHAPTERSPINNERTITLE, string);
		edit.commit();
	}

	public static void setDeviceRegistrationStatus(Context context,
			Boolean value) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putBoolean(DEVICE_REGISTRATION_STATUS, value);
		edit.commit();
	}

	public static void setManualSpinnerTitle(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(MANUALSPINNERTITLE, string);
		edit.commit();
	}

	/**
	 * This method set the manual spinner title in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setManualID(Context context, String manualID) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(MANUALID, manualID);
		edit.commit();
	}

	/**
	 * This method set the manual spinner title in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setManualSpinnerPosition(Context context, int position) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putInt(MANUALSPINNERBUTTON, position);
		edit.commit();
	}

	/**
	 * This method set the manual spinner title in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setChapterSpinnerPosition(Context context, int position) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putInt(CHAPTERSPINNERBUTTON, position);
		edit.commit();
	}

	/**
	 * This method set the shipId in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setShipID(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SHIP_ID, string);
		edit.commit();
	}

	/**
	 * @author pushkar.m This method set the tenantId in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setTenantID(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(TENANT_ID, string);
		edit.commit();
	}

	/**
	 * This method set the search string in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setSearchString(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SEARCHSTRING, string);
		edit.commit();
	}

	/**
	 * This method set the MacId in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setMacID(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(MAC_ID, string);
		edit.commit();
	}

	/**
	 * This method set the user name in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setUserName(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_NAME, string);
		edit.commit();
	}

	/**
	 * This method set the user name for spinner in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setUserNameSpinner(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_NAME_TITLE, string);
		edit.commit();
	}

	/**
	 * This method set the user spinner first name and last name in the shared
	 * preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setUserSpinnerPosition(Context context, int position) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putInt(USERSPINNERBUTTON, position);
		edit.commit();
	}

	/**
	 * This method get the user name from shared preference
	 * 
	 * @param context
	 * @return user name
	 */
	public static String getUserName(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(USER_NAME,
				null);
	}

	/**
	 * This method get the search string from shared preference
	 * 
	 * @param context
	 * @return user name
	 */
	public static String getSearchString(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(SEARCHSTRING,
				null);
	}

	public static Boolean getDeviceRegistrationStatus(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getBoolean(
				DEVICE_REGISTRATION_STATUS, false);
	}

	public static String getChapterSpinnerTitle(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				CHAPTERSPINNERTITLE, null);
	}

	public static String getManualSpinnerTitle(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				MANUALSPINNERTITLE, null);
	}

	public static int getManualSpinnerPosition(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getInt(
				MANUALSPINNERBUTTON, 0);
	}

	public static String getManualId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context))
				.getString(MANUALID, null);
	}

	public static int getChapterSpinnerPosition(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getInt(
				CHAPTERSPINNERBUTTON, 0);
	}

	public static String getUserSpinnerTitle(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				USER_NAME_TITLE, null);
	}

	public static int getUserSpinnerPosition(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getInt(
				USERSPINNERBUTTON, 0);
	}

	/**
	 * This method set the password in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setPassword(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(PASSWORD, string);
		edit.commit();
	}

	/**
	 * This method get the password from shared preference
	 * 
	 * @param context
	 * @return password
	 */
	public static String getPassword(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context))
				.getString(PASSWORD, null);
	}

	/**
	 * This method get the shipId from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getShipId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(SHIP_ID, null);
	}

	/**
	 * @author pushkar.m This method get the tenantId from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getTenantId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(TENANT_ID,
				null);
	}

	/**
	 * This method get the macID from shared preference
	 * 
	 * @param context
	 * @return macID
	 */
	public static String getMacId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(MAC_ID, null);
	}

	/**
	 * This method get the server address from shared preference
	 * 
	 * @param context
	 * @return server address
	 */
	public static String getServerAddress(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				SERVER_ADDRESS, null);
	}

	/**
	 * if session value is set as true than log out is not pressed after
	 * pressing the logout session value will be false
	 * 
	 * @param context
	 * @param session
	 */
	public static void setSession(Context context, boolean session) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putBoolean(SESSION, session);
		edit.commit();
	}

	/**
	 * This method gets the session value
	 * 
	 * @param context
	 * @return session active = true session inactive = false
	 */
	public static boolean getSession(Context context) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		return msharedPreferences.getBoolean(SESSION, false);
	}

	// public static void setServerPort(Context context, String string){
	// SharedPreferences msharedPreferences =
	// (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context);
	// Editor edit = msharedPreferences.edit();
	// edit.putString(SERVER_PORT, string);
	// edit.commit();
	// }
	//
	// public String getServerPort(Context context) {
	// return
	// ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(SERVER_PORT,
	// null);
	// }

	// convert InputStream to String
	/**
	 * 
	 * @param is
	 * @return input stream as string
	 */
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				Log.i("while	:	", "" + line);
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	/**
	 * @author pushkar.m This method returns main string bounded with
	 *         <![CDATA[...]]>
	 * @param data
	 * @return
	 */

	public static String unmarshal(String data) {
		String result = "";
		if (!"".equals(data) && data != null) {

			if (data.indexOf("<![CDATA[") != -1) {

				data = data.replace("<![CDATA[", "");

				result = data.replace("]]>", "");
				return result;
			} else
				result = data;
		}
		return result;
	}

	public static String marshal(String data) {
		String result = null;

		if (!"".equals(data) && data != null)
		{
			if (data.indexOf("<![CDATA[") == -1) {
				result = "<![CDATA[" + data + "]]>";
			}
			else{
				result = data;
			}
		}
		return result;
	}

	public static boolean isIS_SYNC_DONE() {
		return IS_SYNC_DONE;
	}

	public static void setIS_SYNC_DONE(boolean iS_SYNC_DONE) {
		IS_SYNC_DONE = iS_SYNC_DONE;
	}

	public static String getVesselInspectionId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				VESSEL_INSPECTION_ID, null);
	}

	public static void setVesselInspectionId(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(VESSEL_INSPECTION_ID, string);
		edit.commit();
	}

	public static String getUserId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(USER_ID, null);
	}

	public static void setUserId(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_ID, string);
		edit.commit();
	}

	public static String getRoleId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(ROLE_ID, null);
	}

	public static void setRoleId(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(ROLE_ID, string);
		edit.commit();
	}

	public static String getSynckFrom(Context context) {

		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(synckFrom,
				null);

	}

	public static void setSynckFrom(Context context, String syncFrom) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(synckFrom, syncFrom);
		edit.commit();
	}

	public static String getDataAccessMode(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				DATA_ACCESS_MODE, null);
	}

	public static void setDataAccessMode(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(DATA_ACCESS_MODE, string);
		edit.commit();
	}

	public static String getUserFName(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				USER_FIRST_NAME, null);
	}

	public static void setUserFName(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_FIRST_NAME, string);
		edit.commit();
	}

	public static String getUserLName(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				USER_LAST_NAME, null);
	}

	public static void setUserLName(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_LAST_NAME, string);
		edit.commit();
	}

	public static String getUserLogin(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(USER_LOGIN,
				null);
	}

	public static void setUserLogin(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(USER_LOGIN, string);
		edit.commit();
	}

	public static boolean isEditedData(Context context) {

		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getBoolean(editedData,
				false);
	}

	public static void setEditedData(Context context, boolean isEditeddata) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		// edit.putString(USER_LAST_NAME, string);
		edit.putBoolean(editedData, isEditeddata);
		edit.commit();

	}

	/**
	 * This method set the shipId for sync in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setSyncShipID(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SYNC_SHIP_ID, string);
		edit.commit();
	}

	/**
	 * This method get the shipId for sync from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getSyncShipId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(SYNC_SHIP_ID,
				null);
	}

	/**
	 * This method set generated uuid for sync in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setGeneratedUuid(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SYNC_UUID, string);
		edit.commit();
	}

	/**
	 * This method get the generated uuid for sync from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getGeneratedUuid(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(SYNC_UUID,
				null);
	}
	
	/**
	 * This method set FILE NOT MATCH for sync in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setFileNotmatchFlg(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(FILE_NOT_MATCH, string);
		edit.commit();
	}

	/**
	 * This method get the FILE NOT MATCH for sync from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getFileNotmatchFlg(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(FILE_NOT_MATCH,
				null);
	}

	/**
	 * This method set the last active shipId for sync in the shared preference
	 * 
	 * @param context
	 * @param string
	 */
	public static void setLastActiveShip(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(LAST_ACTIVE_SHIP, string);
		edit.commit();
	}

	/**
	 * This method get the last active shipId for sync from shared preference
	 * 
	 * @param context
	 * @return shipID
	 */
	public static String getLastActiveShip(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				LAST_ACTIVE_SHIP, null);
	}

	public static void setActiveInspectionID(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(ACTIVE_INSPECTION_ID, string);
		edit.commit();
	}

	public static String getDropDbFlag(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				DROP_DB_FLAG, null);
	}

	public static void setDropDbFlag(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(DROP_DB_FLAG, string);
		edit.commit();
	}

	public static String getActiveInspectionID(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				ACTIVE_INSPECTION_ID, null);
	}

	public static String getCallingThrough(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				CALLING_THROUGH, null);
	}

	public static void setCallingThrough(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(CALLING_THROUGH, string);
		edit.commit();
	}

	public static String getActivatingShip(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				ACTIVATING_SHIP, null);
	}

	public static void setActivatingShip(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(ACTIVATING_SHIP, string);
		edit.commit();
	}

	public static String getAllActiveInspIds(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				ALL_INSPECTION_IDS_FOR_UNDIRTY, null);
	}

	public static void setAllActiveInspIds(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(ALL_INSPECTION_IDS_FOR_UNDIRTY, string);
		edit.commit();
	}

	public static String getLoginFromShip(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				LOGIN_FROM_SHIP, null);
	}

	public static void setLoginFromShip(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(LOGIN_FROM_SHIP, string);
		edit.commit();
	}

	public static String getSendTemplateData(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				SEND_TEMPLATE_DATA, null);
	}

	public static void setSendTepmlateData(Context context, String string) {
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor edit = msharedPreferences.edit();
		edit.putString(SEND_TEMPLATE_DATA, string);
		edit.commit();
	}

	/**
	 * @author pushkar.m
	 * @brief Function is used to check whether Internet connection is available
	 *        or not
	 * @param context
	 * @param accessMode
	 *            0 = wifi , 1 = mobileData and 2 = any active network
	 * @return boolean value
	 */
	public static boolean hasConnection(Context context, int accessMode) {

		NetworkInfo netWorkInfo = null;
		boolean isConnected = false;
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (accessMode == 0) {

			netWorkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (netWorkInfo != null && netWorkInfo.isConnected()) {
				isConnected = true;
			}

		}

		if (accessMode == 1) {
			netWorkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (netWorkInfo != null && netWorkInfo.isConnected()) {
				isConnected = true;
			}

		}

		if (accessMode == 2) {
			netWorkInfo = cm.getActiveNetworkInfo();
			if (netWorkInfo != null && netWorkInfo.isConnected()) {
				isConnected = true;
			}
		}

		return isConnected;
	}

	/**
	 * @author pushkar.m
	 * @param context
	 * @return This method return active network
	 */
	public static String getActiveNetwork(Context context) {

		String availableNetwork = null;
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ntInfo = cm.getActiveNetworkInfo();

		if (ntInfo != null) {
			if (ntInfo.getType() == ConnectivityManager.TYPE_WIFI)
				availableNetwork = "WiFi";
			if (ntInfo.getType() == ConnectivityManager.TYPE_MOBILE)
				availableNetwork = "MobileData";
		}

		return availableNetwork;
	}

	/**
	 * @author pushkar.m
	 */
	public static String getAccessModeFromDb(Context context) {
		String accessMode = null;
		try {
			DBManager db = new DBManager(context);
			db.open();
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			SyncStatus syncStatus = null;
			if (syncStatusList != null && syncStatusList.size() > 0) {

				syncStatus = syncStatusList.get(0);
				accessMode = syncStatus.getSyncMode();

			}
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return accessMode;
	}

	/**
	 * @author ripunjay.s
	 */
	public static String getDataSyncModeFromDb(Context context) {
		String dataSyncMode = null;
		try {
			DBManager db = new DBManager(context);
			db.open();
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			SyncStatus syncStatus = null;
			if (syncStatusList != null && syncStatusList.size() > 0) {

				syncStatus = syncStatusList.get(0);
				dataSyncMode = syncStatus.getDataSyncMode();

			}
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if(dataSyncMode == null || "Cable".equalsIgnoreCase(dataSyncMode)){
			dataSyncMode = "Cable";
		}
		return dataSyncMode;
	}

	public static boolean checkConnectivity(Context context) {
		boolean isReady = false;
		String accessMode = CommonUtil.getAccessModeFromDb(context);
		if (accessMode != null && !"".equals(accessMode)) {
			if ("WiFi".equalsIgnoreCase(accessMode)) {
				if (!CommonUtil.hasConnection(context, 0)) {
					// pop up for enabling wifi
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.wifiEnableMsg), context);
				} else {
					isReady = true;
				}

			} else if ("MobileData".equalsIgnoreCase(accessMode)) {

				if (!CommonUtil.hasConnection(context, 1)) {
					// pop up for enabling MobileData
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.mobileDataEnableMsg), context);
				} else {
					isReady = true;
				}
			}
		} else {

			if (!CommonUtil.hasConnection(context, 2)) {
				// pop up for enabling network access
				launchPopUpEnablingNetwork(
						context.getResources().getString(
								R.string.networkEnableMsg), context);
			} else {
				isReady = true;
			}
		}
		return isReady;
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	public static void launchPopUpEnablingNetwork(String Message,
			Context context) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		final Context cntx = context;
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				BackGroundTask.context = cntx;
				if (CommonUtil.getUserLogin(cntx) != null
						&& "1".equals(CommonUtil.getUserLogin(cntx))) {
					if (CommonUtil.getActiveInspectionID(cntx) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(cntx))) {
						BackGroundTask.serviceProcessEnd();
					} else if(CommonUtil.getDropDbFlag(cntx) != null && "1".equalsIgnoreCase(CommonUtil.getDropDbFlag(cntx))){
						DBManager db = new DBManager(cntx);
						db.open();
						db.dropDb();
						db.close();
						deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(cntx, LoginActivity.class);
						cntx.startActivity(intent);
						// context.finish();
					}
					else{
						BackGroundTask.serviceProcessEnd();
					}

				} else {
					BackGroundTask.serviceProcessEnd();
				}

			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				BackGroundTask.context = cntx;
				if (CommonUtil.getUserLogin(cntx) != null
						&& "1".equals(CommonUtil.getUserLogin(cntx))) {
					if (CommonUtil.getActiveInspectionID(cntx) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(cntx))) {
						BackGroundTask.serviceProcessEnd();
					} else if(CommonUtil.getDropDbFlag(cntx) != null && "1".equalsIgnoreCase(CommonUtil.getDropDbFlag(cntx))){
						DBManager db = new DBManager(cntx);
						db.open();
						db.dropDb();
						db.close();
						deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(cntx, LoginActivity.class);
						cntx.startActivity(intent);
						// context.finish();
					}
					else{
						BackGroundTask.serviceProcessEnd();
					}
				} else {
					BackGroundTask.serviceProcessEnd();
				}

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	public static void launchPopUpForDirtyAlert(String Message, Context context) {

		final Dialog dialog = new Dialog(context);
		final Context cntx = context;
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author Ripunjay.s
	 * @param Message
	 * @param position
	 */
	public static void launchPopUpError(String Message, Context context) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_error_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * This method return storage directory access or not.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getMassStorageOn(Context context) {

		if (Environment.getExternalStorageDirectory() != null) {

			File storageDir = new File(
					Environment.getExternalStorageDirectory(), "/theark/vi/");

			if (!storageDir.exists()) {

				storageDir.mkdirs();
			}

			if (storageDir.exists()) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteRecursive(child);

		fileOrDirectory.delete();
	}

	/**
	 * Delete all related data of undirty inspection.
	 */
	public static void deleteAllRelatedDataOfUndirtyInspection(Context mContext) {
		DBManager db = new DBManager(mContext);
		db.open();
		try {
			List<VesselInspection> vslActiveList = db
					.getVesselInspectionDataByflgStatus(0);
			if (vslActiveList != null && vslActiveList.size() > 0) {
				for (VesselInspection vslInsp : vslActiveList) {
					long dirtyCount = db.getVesselInspectionDirtyData(vslInsp
							.getiVesselInspectionId());
					if (dirtyCount == 0) {
						List<FilledForm> ffList = db
								.getFilledFormDataByInspectionId(vslInsp
										.getiVesselInspectionId());
						if (ffList != null && ffList.size() > 0) {
							for (FilledForm ff : ffList) {
								CommonUtil.deleteRecursive(new File(Environment
										.getExternalStorageDirectory()
										+ "/theark/vi/"
										+ vslInsp.getiShipId()
										+ "/" + ff.getiFilledFormId()));
								CommonUtil.deleteRecursive(new File(Environment
										.getExternalStorageDirectory()
										+ "/theark/vi/process/"
										+ vslInsp.getiShipId()
										+ "/"
										+ ff.getiFilledFormId()));
							}
						}

						db.deleteVesselInspectionRelatedData(vslInsp
								.getiVesselInspectionId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		db.close();
	}

	/**
	 * Delete all related data of dirty inspection.
	 */
	public static void deleteAllRelatedDataOfDirtyInspection(Context mContext) {
		DBManager db = new DBManager(mContext);
		db.open();
		try {
			List<VesselInspection> vslActiveList = db
					.getVesselInspectionDataByflgStatus(0);
			if (vslActiveList != null && vslActiveList.size() > 0) {
				for (VesselInspection vslInsp : vslActiveList) {
					List<FilledForm> ffList = db
							.getFilledFormDataByInspectionId(vslInsp
									.getiVesselInspectionId());
					if (ffList != null && ffList.size() > 0) {
						for (FilledForm ff : ffList) {
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/"
									+ vslInsp.getiShipId()
									+ "/" + ff.getiFilledFormId()));
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/process/"
									+ vslInsp.getiShipId()
									+ "/"
									+ ff.getiFilledFormId()));
						}

						db.deleteVesselInspectionRelatedData(vslInsp
								.getiVesselInspectionId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		db.close();
	}

	/**
	 * Delete all related data of inactive inspection.
	 */
	public static void deleteAllRelatedDataOfInactiveInspection(Context mContext) {
		DBManager db = new DBManager(mContext);
		db.open();
		try {
			List<VesselInspection> vslActiveList = db
					.getVesselInspectionDataByflgStatus(-1);
			if (vslActiveList != null && vslActiveList.size() > 0) {
				for (VesselInspection vslInsp : vslActiveList) {
					List<FilledForm> ffList = db
							.getFilledFormDataByInspectionId(vslInsp
									.getiVesselInspectionId());
					if (ffList != null && ffList.size() > 0) {
						for (FilledForm ff : ffList) {
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/"
									+ vslInsp.getiShipId()
									+ "/" + ff.getiFilledFormId()));
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/process/"
									+ vslInsp.getiShipId()
									+ "/"
									+ ff.getiFilledFormId()));
						}

						db.deleteVesselInspectionRelatedData(vslInsp
								.getiVesselInspectionId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		db.close();
	}

	/**
	 * Delete all related data of given inspection.
	 */
	public static void deleteAllRelatedDataOfGivenInspection(Context mContext,
			String strInspectionId) {
		DBManager db = new DBManager(mContext);
		db.open();
		try {
			List<VesselInspection> vslActiveList = db
					.getVesselInspectionDataById(strInspectionId);
			if (vslActiveList != null && vslActiveList.size() > 0) {
				for (VesselInspection vslInsp : vslActiveList) {
					List<FilledForm> ffList = db
							.getFilledFormDataByInspectionId(vslInsp
									.getiVesselInspectionId());
					if (ffList != null && ffList.size() > 0) {
						for (FilledForm ff : ffList) {
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/"
									+ vslInsp.getiShipId()
									+ "/" + ff.getiFilledFormId()));
							CommonUtil.deleteRecursive(new File(Environment
									.getExternalStorageDirectory()
									+ "/theark/vi/process/"
									+ vslInsp.getiShipId()
									+ "/"
									+ ff.getiFilledFormId()));
						}

						db.deleteVesselInspectionRelatedData(vslInsp
								.getiVesselInspectionId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		db.close();
	}

	/**
	 * this method update devicesynctime table.
	 */
	public static void updateDeviceSyncTimeTable(Context context,
			String strShipId) {
		try {
			List<SyncHistory> shList = new ArrayList<SyncHistory>();
			List<DeviceSyncTime> dstList = new ArrayList<DeviceSyncTime>();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date curDate = new Date();
			DBManager db = new DBManager(context);
			db.open();
			DeviceSyncTime deviceSyncTime = new DeviceSyncTime();
			dstList = db.getDeviceSyncTimeData();
			int updatecounter = 0;
			if (dstList != null && dstList.size() > 0) {
				deviceSyncTime = dstList.get(0);
				updatecounter = 1;
			}
			shList = db.getSyncHistoryRow(strShipId,
					CommonUtil.getMacId(context));
			if (shList != null && shList.size() > 0) {
				for (SyncHistory sh : shList) {
					if (sh.getStrSyncMode() != null
							&& "ship".equalsIgnoreCase(sh.getStrSyncMode())) {
						deviceSyncTime.setShipSyncTime(sh.getDtGeneratedDate());
					} else {
						deviceSyncTime
								.setShoreSyncTime(sh.getDtGeneratedDate());
					}
				}
			}
			if (updatecounter == 1) {
				deviceSyncTime.setDtUpdated(df.format(curDate));
				deviceSyncTime.setUpdatedBy(CommonUtil.getUserId(context));
				deviceSyncTime.setFlgIsDirty(1);
				db.updateDeviceSyncTimeTable(deviceSyncTime);
			} else {
				deviceSyncTime.setiDeviceSyncTimeId(CommonUtil
						.getMacId(context) + "_" + curDate.getTime());
				deviceSyncTime.setiShipId(strShipId != null
						&& !"".equals(strShipId) ? Integer.parseInt(strShipId)
						: 0);
				deviceSyncTime
						.setiTenantId(CommonUtil.getTenantId(context) != null
								&& !"".equals(CommonUtil.getTenantId(context)) ? Integer
								.parseInt(CommonUtil.getTenantId(context)) : 0);
				deviceSyncTime.setStrMacId(CommonUtil.getMacId(context));
				deviceSyncTime.setDtCreated(df.format(curDate));
				deviceSyncTime.setDtUpdated(df.format(curDate));
				deviceSyncTime.setCreatedBy(CommonUtil.getUserId(context));
				deviceSyncTime.setUpdatedBy(CommonUtil.getUserId(context));
				db.insertDeviceSyncTimeTable(deviceSyncTime);
			}

			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 /**
     * @author pushkar.m
     * Delete all inspection which is not inspected by login user
     * @param context
     */
    public static void deleteInspectionForOtherUser(Context context){
            try {
				DBManager db = new DBManager(context);
				db.open();
				List<VesselInspection> viList = db.getVesselInspectionData();
				String userId = (getUserId(context) != null) ? getUserId(context) : "";
				
				if(!"".equals(userId)){
				        if(viList != null && viList.size() > 0){
				                for(VesselInspection vi : viList){
				                        if(!userId.equals(vi.getInspectedBy())){
				                                db.deleteVesselInspection(vi.getiVesselInspectionId());
				                        }
				                }
				        }
				}
				
				db.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
	
	/**
	 * @author ripunjay.s
	 * this method returns primary uuid.
	 * @return
	 */
	public static String getUUId(Context context){
		String uniqueId="";
		Date curDate = new Date();
		try {
			UUID uuid = UUID.randomUUID();			
			uniqueId = uuid.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(uniqueId == null || "".equalsIgnoreCase(uniqueId)){
			uniqueId = CommonUtil.getMacId(context)+""+curDate.getTime();
		}
		return uniqueId;
	}

}
