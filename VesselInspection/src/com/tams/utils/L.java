package com.tams.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class L {
    public static void fv(String msg) {
        Log.println(Log.VERBOSE, "TAMS", msg);
        /**
         * Ripunjay
         * if want to write in log file should be open below comment.
         */
       // i(msg);
    }

    public static void fi(Context cx, String eventId, String msg) {
        try {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("class", cx.getClass().getCanonicalName());
            parameters.put("info-msg", msg);
            //
            i(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            i(msg);
        }
    }

    public static void fw(Context cx, String eventId, String msg) {
        try {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("class", cx.getClass().getCanonicalName());
            parameters.put("warn-msg", msg);
            //
            w(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            w(msg);
        }
    }

    public static void fe(Context cx, String eventId, String msg) {
        try {
            e(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            e(msg);
        }
    }

    public static void fe(Context cx, String eventId, Throwable tr) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            tr.printStackTrace(pw);
            //
            sw.close();
            pw.close();
            //
            e(tr);
        } catch (Exception e) {
            e.printStackTrace();
            e(tr);
        }
    }

    public static void fwtf(Context cx, String msg) {
        try {
            wtf(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            wtf(msg);
        }
    }

    public static void fwtf(Context cx, Throwable tr) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            tr.printStackTrace(pw);
            //
            sw.close();
            pw.close();
            //
            wtf(tr);
        } catch (Exception e) {
            e.printStackTrace();
            wtf(tr);
        }
    }

    private static void i(String msg) {
        
        Log.i("TAMS ", msg);
        logToFile("INFO", msg, null);
    }

    private static void w(String msg) {
        
        Log.w("TAMS ", msg);
        logToFile("WARNING", msg, null);
    }

    private static void e(String msg) {
        Log.e("TAMS ", msg);
        logToFile("EXCEPTION", msg, null);
    }

    private static void e(Throwable tr) {
        Log.e("TAMS ", tr.getMessage(), tr);
        logToFile("EXCEPTION", tr.toString(), tr);
    }

    private static void wtf(String msg) {
        Log.println(Log.ASSERT, "TAMS ", msg);
        logToFile("WTF", msg, null);
    }

    private static void wtf(Throwable tr) {
        Log.println(Log.ASSERT, "TAMS ", tr.toString());
        logToFile("WTF", tr.toString(), tr);
    }

    private static void logToFile(String type, String msg, Throwable tr) {
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File PinigLogDir = new File(sdCard.getAbsoluteFile(), File.separator + "VesselInspection" + File.separator + "logs");
            if (!PinigLogDir.exists())
                PinigLogDir.mkdirs();
            File PinigLog = new File(PinigLogDir, "vi.txt");
            if (!PinigLog.exists())
                PinigLog.createNewFile();
            if (null == tr) {
                msg += "\n";
                FileOutputStream fos = new FileOutputStream(PinigLog, true);
                fos.write((new Date()).toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(msg.getBytes());
                fos.close();
            } else {
                StringWriter sw = new StringWriter();
                sw.append("\n").append(type).append("\n");
                PrintWriter pw = new PrintWriter(sw);
                tr.printStackTrace(pw);
                //
                FileOutputStream fos = new FileOutputStream(PinigLog, true);
                fos.write((new Date()).toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(sw.toString().getBytes());
                sw.close();
                pw.close();
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
