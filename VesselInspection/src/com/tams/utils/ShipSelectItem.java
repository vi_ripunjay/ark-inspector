package com.tams.utils;

public class ShipSelectItem {

    private String text;
    private String shipId;
    private String iTenantId;
    
    public String getiTenantId() {
		return iTenantId;
	}
	public ShipSelectItem(String text, String item,String iTenantId) {
            this.text = text;
            this.shipId = item;
            this.iTenantId = iTenantId;
            
    }

    public String getShipId() {
		return shipId;
	}
	public void setShipId(String shipId) {
		this.shipId = shipId;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}
	public String getText() {
        return text;
    }
    public String getUserId() {
        return shipId;
    }
    
    
    @Override
    public String toString() {
        return getText();
    }

}
