package com.tams.sync;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.commons.lang3.StringEscapeUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.os.Environment;
import android.telephony.TelephonyManager;

import com.tams.model.DeviceSyncTime;
import com.tams.model.SyncHistory;
import com.tams.model.VITemplateVersion;
import com.tams.model.VesselInspection;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;
import com.tams.vessalinspection.R;

public class GenerateXmlService {

	/**
	 * @author Ripunjay Shukla
	 * @since 29-06-2015 This class write for generate zip of xml for sync.
	 */

	private Map<String, Set<String>> tableColumnMap;

	private Map<String, String[][]> tableValueMap;

	private Context context;

	@SuppressWarnings("unused")
	private int lineCounter = 0;
	private int recordNum = 0;

	private String baseFoldername = "";

	private StringBuffer logMessages;

	public GenerateXmlService() {

	}

	public GenerateXmlService(Context mcontext) {

		this.context = mcontext;
	}

	public void generateXmlForDirtyRecord() {

		DBManager db = new DBManager(context);
		db.open();

		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow(CommonUtil.getSyncShipId(context),
				CommonUtil.getMacId(context));

		cleanProcessDirectory(getXmlProcessPath());
		cleanProcessDirectory(new File(getXmlSendPath()));
		cleanProcessDirectory(new File(getXmlImportPath()));

		/*
		 * if (!(shList == null || shList.size() == 0)) {
		 * 
		 * File sourceFile = generateZipForInitialSync(context);
		 * 
		 * String destinationFile = getInitialZipProcessPath(baseFoldername);
		 * createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);
		 * mooveFiletoSendFolder(destinationFile);
		 * cleanProcessDirectory(getXmlProcessPath());
		 * 
		 * } else {
		 */
		String iShipId = String.valueOf(CommonUtil.getSyncShipId(context));
		String iTenantId = String.valueOf(CommonUtil.getTenantId(context));

		File sourceFile = createOneTablerecord(context);

		String destinationFile = getZipProcessPath(baseFoldername);

		cleanProcessDirectory(new File(getXmlSendPath()));
		/**
		 * below 3 process comment for image sync in same folder after that zip
		 * create.
		 */
		/*
		 * createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);
		 * mooveFiletoSendFolder(destinationFile);
		 * cleanProcessDirectory(getXmlProcessPath());
		 */
		// }
		db.close();

	}

	public File getXmlProcessPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/");
		File procFile = new File(sb.toString());
		return procFile;
	}

	public String getZipProcessPath(String strFilename) {

		Date curDate = new Date();
		String uuid = CommonUtil.getUUId(context);
		CommonUtil.setGeneratedUuid(context, uuid);;
		
		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/"
						+ strFilename + getMacId() + "_"
						+ (CommonUtil.getUserId(context) != null ? CommonUtil.getUserId(context) : "0")
						+ "_" + uuid +"_" + curDate.getTime()
						+ "_Database-Synchronization_.zip");

		return sb.toString();
	}

	public String getInitialZipProcessPath(String strFilename) {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/"
						+ strFilename + getMacId() + "_"
						+ CommonUtil.getUserId(context)
						+ "_Initial-Synchronization_.zip");

		return sb.toString();
	}

	public String getXmlSendPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Export/");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}

		/**
		 * below for only creation directory
		 */
		StringBuffer sbRec = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Import/");
		File fileRec = new File(sbRec.toString());
		if (!fileRec.exists()) {
			fileRec.mkdirs();
		}

		return sb.toString();
	}
	
	public String getXmlImportPath() {
		
		/**
		 * below for only creation directory
		 */
		StringBuffer sbRec = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Import/");
		File fileRec = new File(sbRec.toString());
		if (!fileRec.exists()) {
			fileRec.mkdirs();
		}

		return sbRec.toString();
	}

	public File getBaseFolderpath() {
		StringBuffer baseFolderName = new StringBuffer("TABLETSHIP");
		baseFolderName.append("_" + 0 + "_" + (CommonUtil.getTenantId(context) != null && !"null".equalsIgnoreCase(CommonUtil.getTenantId(context)) ? CommonUtil.getTenantId(context) : "0")
				+ "_" + (CommonUtil.getSyncShipId(context) != null ? CommonUtil.getSyncShipId(context) : "0") + "_");

		File file = new File(Environment.getExternalStorageDirectory()
				+ "/xmlProcess/" + baseFolderName);
		if (!file.exists()) {
			file.mkdirs();
		}
		baseFoldername = baseFolderName.toString();
		return file;
	}

	/**
	 * @author ripunjay
	 * @return pass table for generate xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { DBHelper.FILLED_CHECKLIST, DBHelper.FILLED_FORM,DBHelper.TABLE_VesselInspectionFormListDesc,
				DBHelper.VESSEL_INSPECTION, DBHelper.TABLE_SYNC_HISTORY,
				DBHelper.FILLED_FORM_IMAGE };

		return tablename;
	}
	
	/**
	 * @author ripunjay
	 * @return pass master table for generate xml and comes in sync.
	 */
	public String[] getTableWithTamplateName() {
		String[] tablename = {DBHelper.TABLE_FORM_CATEGORY, DBHelper.TABLE_ROLE_TEMPLATE, DBHelper.CHECKLIST_SECTION,
				DBHelper.CHECKLIST_SECTION_ITEMS,DBHelper.FORM_SECTION, DBHelper.TABLE_FORM_SECTION_ITEM,
				DBHelper.TABLE_SHIP_FORM_SECTION_ITEM, DBHelper.TABLE_VI_TEMPLATE_VERSION};

		return tablename;
	}

	/**
	 * @author Ripunjay
	 * @param mContext
	 * @return create xml of given tables
	 */
	public File createOneTablerecord(Context mContext) {
		try {
			context = mContext;
			boolean createFileWriteOnfirstRecord = true;
			Writer fileWriter = null;
			StringBuffer dataXml = new StringBuffer();
			int recordcount = 0;
			logMessages = new StringBuffer();
			String shipid = CommonUtil.getSyncShipId(mContext);
			String tenantid = CommonUtil.getTenantId(mContext);

			File sourceFile = getBaseFolderpath();

			DBManager db = new DBManager(mContext);
			db.open();

			String[] tablename = getTablename();
			String[] templatetablename = getTableWithTamplateName();

			/**
			 * Get Active vessel inspection from database.
			 */
			List<VesselInspection> activeVsl = new ArrayList<VesselInspection>();
			activeVsl = db.getVesselInspectionDataByflgStatusAndShipIdForSync(0,
					shipid);
			StringBuffer activeVslId = new StringBuffer("'000'");
			
			if (CommonUtil.getActiveInspectionID(mContext) != null
					&& !"".equals(CommonUtil.getActiveInspectionID(mContext))) {
				activeVslId.append(",'"
						+ CommonUtil.getActiveInspectionID(mContext) + "'");
			}
			else {
				if (activeVsl != null && activeVsl.size() > 0) {
					for (VesselInspection vslInsp : activeVsl) {
						activeVslId.append(",'" + vslInsp.getiVesselInspectionId()
								+ "'");
					}
				}
			}
			
			/**
			 * Below ids set for undirty data after parse of set vslIds.
			 */
			CommonUtil.setAllActiveInspIds(mContext, activeVslId.toString());

			/**
			 * Get VesselinspectionSyncHistory on the basis of ship.
			 */
			List<SyncHistory> shList = new ArrayList<SyncHistory>();
			shList = db
					.getSyncHistoryRow(shipid, CommonUtil.getMacId(mContext));
			StringBuffer syncInfo = new StringBuffer();
			if (shList != null && shList.size() > 0) {
				if (shList.size() == 2) {
					for (SyncHistory sh : shList) {
						syncInfo.append("#" + sh.getStrSyncMode() + "-"
								+ sh.getSyncOrderid());
					}
				} else {
					String strMode="";
					for (SyncHistory sh : shList) {
						strMode = sh.getStrSyncMode();
						syncInfo.append("#" + sh.getStrSyncMode() + "-"
								+ sh.getSyncOrderid());
					}
					
					if(!"".equals(strMode) && "ship".equalsIgnoreCase(strMode)){
						syncInfo.append("#shore-0");
					}
					else{
						syncInfo.insert(0,"#ship-0");
					}
				}
			} else {
				syncInfo.append("#ship-0#shore-0");
			}
			if (CommonUtil.getActiveInspectionID(mContext) != null
					&& !"".equals(CommonUtil.getActiveInspectionID(mContext))) {
				syncInfo.append("#active");
			}
			else{
				syncInfo.append("#inactive");
			}
			
			/**
			 * TODO: write code for append #temp_ver-1#app_ver-1#uploaded_temp-0/1 default zero but with temp data 1
			 */
			VITemplateVersion viTemplateVersion = new VITemplateVersion();
			List<VITemplateVersion> viTmpVerlist = new ArrayList<VITemplateVersion>();
			viTmpVerlist = db.getVITemplateVersionByActiveVersion(1);
			if(viTmpVerlist != null && viTmpVerlist.size() > 0){
				viTemplateVersion = viTmpVerlist.get(0);
			}
			
			/**
			 * APK version number.
			 */
			PackageInfo pInfo = null;
			String version="";
			try {
				pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			} catch (Exception e) {			
				e.printStackTrace();
			}
			if(pInfo != null && pInfo.versionName != null){
				version = pInfo.versionName;
			}else{
				version ="2.0";
			}
			
			/**
			 * upload template flag.
			 */
			int uploadtmpFlg = 0;
			if(CommonUtil.getSendTemplateData(mContext) != null && "1".equalsIgnoreCase(CommonUtil.getSendTemplateData(mContext))){
				uploadtmpFlg = 1;
			}

			syncInfo.append("#temp_ver-"+viTemplateVersion.getVersionNumber()+"#app_ver-"+version+"#uploaded_temp-"+uploadtmpFlg);			
			

			/**
			 * Make a txt file for active vessel inspection ids for getting data
			 * from web.
			 */
			try {
				Writer fileWriterForSync = null;
				fileWriterForSync = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(
								sourceFile.getAbsolutePath() + "/"
										+ "ActiveVesselInspetion_"
										+ CommonUtil.getMacId(mContext) + "_"
										+ "_.txt"), "UTF-8"));

				fileWriterForSync.write(activeVslId.toString()+syncInfo.toString());
				fileWriterForSync.close();
			} catch (Exception e) {

				e.printStackTrace();
			}
			
			/**
			 * Make a txt file for device sync time for getting data
			 * from web.
			 */
			
			List<DeviceSyncTime> deviceSyncTimeList = new ArrayList<DeviceSyncTime>();
			deviceSyncTimeList = db.getDeviceSyncTimeData();
			StringBuffer dstBfr = new StringBuffer("");
			if(deviceSyncTimeList != null && deviceSyncTimeList.size() > 0){
				for(DeviceSyncTime dst : deviceSyncTimeList)
				{
					dstBfr.append("SHORESYNCTIME-"+(dst.getShoreSyncTime() != null && !"null".equalsIgnoreCase(dst.getShoreSyncTime()) ? dst.getShoreSyncTime() : 0)+"_SHIPSYNCTIME-"+(dst.getShipSyncTime() != null && !"null".equalsIgnoreCase(dst.getShipSyncTime()) ? dst.getShipSyncTime() : 0));
				}
			}
			else{
				dstBfr.append("SHORESYNCTIME-0_SHIPSYNCTIME-0");
			}
			try {
				Writer fileWriterForSync = null;
				fileWriterForSync = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(
								sourceFile.getAbsolutePath() + "/"
										+ "DeviceSyncTime_"
										+ CommonUtil.getMacId(mContext) + "_"+dstBfr.toString()
										+ "_.txt"), "UTF-8"));

				fileWriterForSync.write(dstBfr.toString());
				fileWriterForSync.close();
				
				dstBfr = null;
			} catch (Exception e) {

				e.printStackTrace();
			}		

			L.fv("table by ripunjay first "+tablename);
			/**
			 * below block for master template data.
			 */
			if(CommonUtil.getSendTemplateData(mContext) != null && "1".equalsIgnoreCase(CommonUtil.getSendTemplateData(mContext))){
				for (int i = 0; i < templatetablename.length; i++) {
					
					recordcount = 0;
					lineCounter = 0;
					createFileWriteOnfirstRecord = true;

					/**
					 * Get data per table according to active vessel inspection.
					 */
					Cursor cursor = db.getTemplateDataForSync(templatetablename[i], tenantid, shipid);

					List<String[]> tblDetails = db
							.getDbTableColumnDetails(templatetablename[i]);

					try {
						if (tblDetails != null && tblDetails.size() > 0) {

							// looping through all rows and adding to list
							if (cursor != null && cursor.moveToFirst()) {

								if (createFileWriteOnfirstRecord) {

									String pk = tblDetails.get(0)[0];

									dataXml = new StringBuffer(
											"<tables iTenantId='" + tenantid
													+ "' ishipId='" + shipid
													+ "' pk='" + pk + "' master='true'>");
									dataXml.append("<" + templatetablename[i] + " pk='"
											+ pk + "'>");
									fileWriter = new BufferedWriter(
											new OutputStreamWriter(
													new FileOutputStream(sourceFile
															.getAbsolutePath()
															+ "/"
															+ templatetablename[i]
															+ ".xml"), "UTF-8"));

									createFileWriteOnfirstRecord = false;
								}

								do {
									recordcount++;
									recordNum++;
									lineCounter++;
									/**
									 * If single table has more than
									 * 500(configurable) dirty data then need more
									 * than one file generateincluding timestamp.
									 */
									if (recordcount > Integer.parseInt(context
											.getResources().getString(
													R.string.recordCount))) {
										recordcount = 1;

										dataXml.append("</" + templatetablename[i] + ">");
										dataXml.append("</tables>");
										fileWriter.write(dataXml.toString());
										fileWriter.close();

										String pk = tblDetails.get(0)[0];

										dataXml = new StringBuffer(
												"<tables iTenantId='" + tenantid
														+ "' ishipId='" + shipid
														+ "' pk='" + pk + "'>");
										dataXml.append("<" + templatetablename[i] + " pk='"
												+ pk + "'>");
										fileWriter = new BufferedWriter(
												new OutputStreamWriter(
														new FileOutputStream(
																sourceFile
																		.getAbsolutePath()
																		+ "/"
																		+ templatetablename[i]
																		+ new Date()
																				.getTime()
																		+ ".xml"),
														"UTF-8"));
									}

									dataXml.append("<r" + recordcount + ">");

									for (String[] tblDet : tblDetails) {

										if (tblDet[1].equalsIgnoreCase("INTEGER")) {
											dataXml.append("<"
													+ tblDet[0]
													+ " t='"
													+ tblDet[1]
													+ "'>"
													+ cursor.getInt(cursor
															.getColumnIndex(tblDet[0]))
													+ "</" + tblDet[0] + ">");
										} else if (tblDet[1]
												.equalsIgnoreCase("TEXT")) {

											String data = cursor.getString(cursor
													.getColumnIndex(tblDet[0]));
											try {
												if (data != null
														&& !"null".equals(data)
														&& !"".equals(data)) {
													data = StringEscapeUtils
															.escapeXml(data);
												}
											} catch (Exception e) {
												e.printStackTrace();
											}

											dataXml.append("<"
													+ tblDet[0]
													+ " t='VARCHAR'>"
													+ ((data != null
															&& !"null".equals(data) && !""
																.equals(data)) ? data
															.replaceAll("&apos;",
																	"&apos;&apos;")
															: data) + "</"
													+ tblDet[0] + ">");

										} else if (tblDet[1]
												.equalsIgnoreCase("DATE")) {

											dataXml.append("<"
													+ tblDet[0]
													+ " t='"
													+ tblDet[1]
													+ "'>"
													+ cursor.getString(cursor
															.getColumnIndex(tblDet[0]))
													+ "</" + tblDet[0] + ">");
										}

									}
									dataXml.append("</r" + recordcount + ">");

								} while (cursor.moveToNext());

								dataXml.append("</" + templatetablename[i] + ">");
								dataXml.append("</tables>");
								fileWriter.write(dataXml.toString());
								fileWriter.close();

								logMessages.append(" " + templatetablename[i]
										+ " : record : " + lineCounter);
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						cursor.close();
					}

				
				}
			}
			
			/**
			 * Regular Sync
			 */
			for (int i = 0; i < tablename.length; i++) {

				L.fv("table by ripunjay "+tablename[i]);
				recordcount = 0;
				lineCounter = 0;
				// recordNum = 0;

				createFileWriteOnfirstRecord = true;

				/**
				 * Get data per table according to active vessel inspection.
				 */
				Cursor cursor = db.getDataForSync(tablename[i], shipid,
						CommonUtil.getMacId(mContext), activeVslId.toString());

				List<String[]> tblDetails = db
						.getDbTableColumnDetails(tablename[i]);

				try {
					if (tblDetails != null && tblDetails.size() > 0) {
						
						L.fv("table column details "+tblDetails);

						// looping through all rows and adding to list
						if (cursor != null && cursor.moveToFirst()) {

							if (createFileWriteOnfirstRecord) {

								String pk = tblDetails.get(0)[0];

								dataXml = new StringBuffer(
										"<tables iTenantId='" + tenantid
												+ "' ishipId='" + shipid
												+ "' pk='" + pk + "'>");
								dataXml.append("<" + tablename[i] + " pk='"
										+ pk + "'>");
								fileWriter = new BufferedWriter(
										new OutputStreamWriter(
												new FileOutputStream(sourceFile
														.getAbsolutePath()
														+ "/"
														+ tablename[i]
														+ ".xml"), "UTF-8"));

								createFileWriteOnfirstRecord = false;
							}

							do {
								recordcount++;
								recordNum++;
								lineCounter++;
								/**
								 * If single table has more than
								 * 500(configurable) dirty data then need more
								 * than one file generateincluding timestamp.
								 */
								if (recordcount > Integer.parseInt(context
										.getResources().getString(
												R.string.recordCount))) {
									recordcount = 1;

									dataXml.append("</" + tablename[i] + ">");
									dataXml.append("</tables>");
									fileWriter.write(dataXml.toString());
									fileWriter.close();

									String pk = tblDetails.get(0)[0];

									dataXml = new StringBuffer(
											"<tables iTenantId='" + tenantid
													+ "' ishipId='" + shipid
													+ "' pk='" + pk + "'>");
									dataXml.append("<" + tablename[i] + " pk='"
											+ pk + "'>");
									fileWriter = new BufferedWriter(
											new OutputStreamWriter(
													new FileOutputStream(
															sourceFile
																	.getAbsolutePath()
																	+ "/"
																	+ tablename[i]
																	+ new Date()
																			.getTime()
																	+ ".xml"),
													"UTF-8"));
								}

								dataXml.append("<r" + recordcount + ">");

								for (String[] tblDet : tblDetails) {

									if (tblDet[1].equalsIgnoreCase("INTEGER")) {
										dataXml.append("<"
												+ tblDet[0]
												+ " t='"
												+ tblDet[1]
												+ "'>"
												+ cursor.getInt(cursor
														.getColumnIndex(tblDet[0]))
												+ "</" + tblDet[0] + ">");
									} else if (tblDet[1]
											.equalsIgnoreCase("TEXT")) {

										String data = cursor.getString(cursor
												.getColumnIndex(tblDet[0]));
										try {
											if (data != null
													&& !"null".equals(data)
													&& !"".equals(data)) {
												data = StringEscapeUtils
														.escapeXml(data);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}

										/*
										 * dataXml = dataXml + "<" + tblDet[0] +
										 * " t='VARCHAR'>" +
										 * cursor.getString(cursor
										 * .getColumnIndex(tblDet[0])) + "</" +
										 * tblDet[0] + ">";
										 */

										dataXml.append("<"
												+ tblDet[0]
												+ " t='VARCHAR'>"
												+ ((data != null
														&& !"null".equals(data) && !""
															.equals(data)) ? data
														.replaceAll("&apos;",
																"&apos;&apos;")
														: data) + "</"
												+ tblDet[0] + ">");

									} else if (tblDet[1]
											.equalsIgnoreCase("DATE")) {

										dataXml.append("<"
												+ tblDet[0]
												+ " t='"
												+ tblDet[1]
												+ "'>"
												+ cursor.getString(cursor
														.getColumnIndex(tblDet[0]))
												+ "</" + tblDet[0] + ">");
									}

								}
								dataXml.append("</r" + recordcount + ">");

								/**
								 * Generate a infor file for sync.
								 */
								/*if (tablename[i]
										.equalsIgnoreCase(DBHelper.TABLE_SYNC_HISTORY)
										&& CommonUtil
												.getMacId(mContext)
												.equalsIgnoreCase(
														cursor.getString(cursor
																.getColumnIndexOrThrow(DBHelper.MAC_ID)))) {
									try {
										Writer fileWriterForSync = null;
										fileWriterForSync = new BufferedWriter(
												new OutputStreamWriter(
														new FileOutputStream(
																sourceFile
																		.getAbsolutePath()
																		+ "/"
																		+ "SyncHistory_"
																		+ cursor.getString(cursor
																				.getColumnIndexOrThrow(DBHelper.SYNC_MODE))
																		+ "_"
																		+ CommonUtil
																				.getMacId(mContext)
																		+ "_"
																		+ cursor.getString(cursor
																				.getColumnIndexOrThrow(DBHelper.SYNC_ACK_DATE))
																		+ "_.txt"),
														"UTF-8"));

										fileWriterForSync
												.write("Sync History info");
										fileWriterForSync.close();
									} catch (Exception e) {

										e.printStackTrace();
									}
								}*/

							} while (cursor.moveToNext());

							dataXml.append("</" + tablename[i] + ">");
							dataXml.append("</tables>");
							fileWriter.write(dataXml.toString());
							fileWriter.close();

							logMessages.append(" " + tablename[i]
									+ " : record : " + lineCounter);
						} else {
							/**
							 * Generate a infor initial file for sync.
							 */
							/*if (tablename[i]
									.equalsIgnoreCase(DBHelper.TABLE_SYNC_HISTORY)) {
								try {
									Writer fileWriterForSync = null;
									fileWriterForSync = new BufferedWriter(
											new OutputStreamWriter(
													new FileOutputStream(
															sourceFile
																	.getAbsolutePath()
																	+ "/"
																	+ "SyncHistory_"
																	+ 0
																	+ "_"
																	+ CommonUtil
																			.getMacId(mContext)
																	+ "_"
																	+ 0
																	+ "_.txt"),
													"UTF-8"));

									fileWriterForSync
											.write("Sync History info");
									fileWriterForSync.close();
								} catch (Exception e) {

									e.printStackTrace();
								}
							}*/

						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					cursor.close();
				}

			}
			if (recordNum > 0) {
				insertSyncHistoryRecord(logMessages.toString(), "progress",
						"TABLETSHIP", baseFoldername,
						Integer.parseInt(tenantid), Integer.parseInt(shipid));
			}

			db.close();
			return sourceFile;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@SuppressLint("SimpleDateFormat")
	public void insertSyncHistoryRecord(String logMessage, String progress,
			String generator, String strFilename, int iTenantId, int iShipId) {

		// DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		// Date curDate = new Date();

		DBManager db = new DBManager(context);
		db.open();

		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow(CommonUtil.getSyncShipId(context),
				CommonUtil.getMacId(context));
		SyncHistory syncHistory = new SyncHistory();
		if (shList != null && shList.size() > 0) {
			syncHistory = shList.get(0);
			syncHistory.setLogMessage(logMessage);
		}

		if (shList != null && shList.size() > 0) {
			syncHistory.setFlgIsDirty(1);
			if (syncHistory.getDtAcknowledgeDate() == null) {
				syncHistory.setDtAcknowledgeDate(syncHistory
						.getDtGeneratedDate());
			}
			db.updateSyncHistorydata(syncHistory);
		}

		db.close();

	}

	public File generateZipForInitialSync(Context mContext) {
		/**
		 * Write initial sync zip code here.
		 */
		try {

			String baseFolderName = "TABLETSHIP_0_0_0_";
			File sourceFile = new File(
					Environment.getExternalStorageDirectory() + "/xmlProcess/"
							+ baseFolderName);
			if (!sourceFile.exists()) {
				sourceFile.mkdirs();
			}

			baseFoldername = baseFolderName;

			context = mContext;
			Writer fileWriter = null;
			String dataXml = "";
			logMessages = new StringBuffer();
			String shipid = CommonUtil.getSyncShipId(mContext);
			String tenantid = CommonUtil.getTenantId(mContext);

			dataXml = "<tables iTenantId='" + tenantid + "' ishipId='" + shipid
					+ "' pk=''>";

			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(sourceFile.getAbsolutePath()
							+ "/initialSync.xml"), "UTF-8"));

			dataXml = dataXml + "</tables>";

			fileWriter.write(dataXml);
			fileWriter.close();

			/*
			 * insertSyncHistoryRecord("Initial Sync", "progress", "TABLETSHIP",
			 * baseFoldername, Integer.parseInt("1"), Integer.parseInt("1"));
			 */

			return sourceFile;

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DBManager db = new DBManager(context);
		db.open();
		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				/*db.updateDirtyRecordInTables(tbl,
						CommonUtil.getSyncShipId(context));*/
				db.updateDirtyRecordInTables(tbl,
						CommonUtil.getSyncShipId(context), CommonUtil.getAllActiveInspIds(context));
			}

		}
		db.close();
	}

	public Map<String, Set<String>> getTableColumnMap() {
		return tableColumnMap;
	}

	public void setTableColumnMap(Map<String, Set<String>> tableColumnMap) {
		this.tableColumnMap = tableColumnMap;
	}

	public Map<String, String[][]> getTableValueMap() {
		return tableValueMap;
	}

	public void setTableValueMap(Map<String, String[][]> tableValueMap) {
		this.tableValueMap = tableValueMap;
	}

	public void createZipFileofFolder(String folderPath, String zipFilePath) {
		try {
			File fileToAdd = new File(folderPath);
			if (fileToAdd.exists()) {
				ZipFile zipFile = new ZipFile(zipFilePath);
				ZipParameters parameters = new ZipParameters();
				parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
				parameters
						.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
				parameters.setEncryptFiles(true);
				parameters
						.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword("prashant");
				String[] fileArray = fileToAdd.list();
				for (int i = 0; i < fileArray.length; i++) {
					L.fv("Added in zip" + fileArray[i]);
					File tobeAdded = new File(fileToAdd.getAbsolutePath()
							+ File.separator + fileArray[i]);
					if (tobeAdded.isDirectory()) {
						zipFile.addFolder(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					} else {
						zipFile.addFile(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					}
				}
			}

		} catch (ZipException e) {
			e.printStackTrace();

		}
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public boolean mooveFiletoSendFolder(String filetomoove) {
		File file = new File(filetomoove);
		File dir = new File(getXmlSendPath());
		return file.renameTo(new File(dir, file.getName()));

	}

	public String getMacId() {
		String strmacId = "macid";
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		strmacId = tm.getDeviceId();
		return strmacId;
	}

	public String getPk() {
		String pk = "";
		Date curDate = new Date();
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		pk = tm.getDeviceId() + "_" + curDate.getTime();

		return pk;
	}

}
