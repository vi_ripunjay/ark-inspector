package com.tams.sync;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.lingala.zip4j.core.ZipFile;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;

public class ParseImageSyncService {

	/**
	 * @author Ripunjay SHukla
	 * @see parse xml of given zip and save and update data in db
	 */

	Handler handler;
	public static Context context;

	private String xmlProcessPath = "";
	private String strDownloadDate;

	public ParseImageSyncService() {

	}

	public ParseImageSyncService(Context context) {
		super();
		this.context = context;
	}

	public void startParseXmlAfterZip() {
		try {

			parseXmlForSync();
			
			updateDirtyFlagOfTables();
			
			deleteTransactionData();
			
			setDeviceTime();
						
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DBManager db = new DBManager(context);
		db.open();
		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				db.updateDirtyRecordInTables(tbl,
						CommonUtil.getSyncShipId(context), CommonUtil.getAllActiveInspIds(context));
			}
		
		}
		db.close();
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for parse xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { DBHelper.FILLED_CHECKLIST, DBHelper.FILLED_FORM,
				DBHelper.VESSEL_INSPECTION, DBHelper.FILLED_FORM_IMAGE,DBHelper.TABLE_VesselInspectionFormListDesc};

		return tablename;
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for parse xml and comes in sync.
	 */
	public String[] getTablenameForDelete() {
		String[] tablename = { "PowerPlusTransaction", "TrainingTransaction"};

		return tablename;
	}
	
	public boolean fileExist() {
		boolean exist = false;
		xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP") && name.contains("File");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	@SuppressLint("DefaultLocale")
	public void parseXmlForSync() {

		Map<String, List<String>> totalShiporShoreDatatoSync = new HashMap<String, List<String>>();
		List<String> syncOrderIdsForShip = new ArrayList<String>();

		xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP") && name.contains("File");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {

			for (String fileName : allzipFileFromXmlProcessPath) {

				String[] splitFileNameArray = fileName.split("_");

				String keystr = splitFileNameArray[3].replaceAll(".zip", "")
						.replaceAll(".ZIP", "") + "_" + splitFileNameArray[2];

				// order tenant ship
				List<String> list = totalShiporShoreDatatoSync.get(keystr);
				if (list == null) {
					list = new ArrayList<String>();
				}
				list.add(fileName);

				syncOrderIdsForShip.add(splitFileNameArray[3].replaceAll(
						".zip", "").replaceAll(".ZIP", "")
						+ "_" + splitFileNameArray[1]);
				totalShiporShoreDatatoSync.put(keystr, list);

				for (Iterator<String> iterator = totalShiporShoreDatatoSync
						.keySet().iterator(); iterator.hasNext();) {

					String keyStrValue = iterator.next();
					String tenantid = keyStrValue.split("_")[1];
					String shipid = keyStrValue.split("_")[0];

					List<String> totalFileForthisTenantShip = totalShiporShoreDatatoSync
							.get(keyStrValue);
					// overriding the sort method to do integer sorting for a
					// list of
					// strings passed
					Collections.sort(totalFileForthisTenantShip,
							new Comparator<String>() {
								@Override
								public int compare(String o1, String o2) {
									return Integer.parseInt(o1.split("_")[1])
											- Integer.parseInt(o2.split("_")[1]);

								}
							});

					for (Iterator<String> iterator2 = totalFileForthisTenantShip
							.iterator(); iterator2.hasNext();) {

						String zipFileName = iterator2.next();
						String[] arr1 = zipFileName.split("_");

						String extractPath = xmlProcessPath + "/extractZip"
								+ System.currentTimeMillis();

						try {
							extractZipFile(xmlProcessPath + "/" + zipFileName,
									extractPath);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						String allXmlFile[] = new File(extractPath)
								.list(new FilenameFilter() {
									@Override
									public boolean accept(File dir, String name) {
										return name.toUpperCase().endsWith(
												".XML");

									}
								});

						// cleanProcessDirectory(new File(extractPath));

						if (allXmlFile != null) {
							Arrays.sort(allXmlFile);
							for (String filenameInExtractedDir : allXmlFile) {

								try {
									parseXmlFile(extractPath + "/"
											+ filenameInExtractedDir, extractPath);

								} catch (Exception e) {

									e.printStackTrace();
								}

							}
						}

					}

				}
			}
		}

		cleanProcessDirectory(new File(xmlProcessPath));

	}

	/**
	 * This method is called for each file to be parsed and updated in the
	 * database
	 * 
	 * @param filename
	 */
	public void parseXmlFile(String filename, String extractPath) {
		String strIshipid = null;
		String strItenantid = null;
		String tablename = null;
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse("file://" + filename);
			doc.normalize();
			Node parentNode = doc.getFirstChild();
			strIshipid = parentNode.getAttributes().getNamedItem("ishipId")
					.getNodeValue();
			strItenantid = parentNode.getAttributes().getNamedItem("iTenantId")
					.getNodeValue();

			NodeList nodeList = parentNode.getChildNodes();

			int fileCount = 0;
			int fileCountFailed = 0;
			
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					String fileName = node.getAttributes()
							.getNamedItem("name").getNodeValue();
					String path = node.getAttributes().getNamedItem("path")
							.getNodeValue();
					
					File copyDir = new File(path);
					if (!copyDir.exists()) {
						if (!copyDir.mkdirs()) {
							fileCountFailed = fileCountFailed + 1;
						}
					}
					File src = new File(extractPath + File.separator
							+ fileName);
					File dest = new File(copyDir.getAbsoluteFile()
							+ File.separator + fileName);
					copyFile(src, dest);
					fileCount = fileCount + 1;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} 
	}

	public ContentValues getContentValueOfTable(
			Map<String, String> onerecordData, int flg) {

		ContentValues values = new ContentValues();

		StringBuffer sqlstr = new StringBuffer();
		// String sqlstr = "(";
		sqlstr.append("(");
		Set<String> allkeyset = onerecordData.keySet();
		if (flg == 0) {

			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {

				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}

		}
		if (flg == 1) {
			// sqlstr = " SET ";
			sqlstr = new StringBuffer();
			sqlstr.append(" SET ");
			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {
				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}
		}
		return values;
	}
	
	public void copyFile(File sourceFile, File destFile){
		try {
			
			if(!destFile.exists()){
				destFile.mkdirs();
			}
			if(sourceFile.exists())
			{
				InputStream in = new FileInputStream(sourceFile);
				OutputStream out = new FileOutputStream(destFile);
	
				byte[] buffer = new byte[1024];
	
				int length;
				// copy the file content in bytes
				while ((length = in.read(buffer)) > 0) {
				    out.write(buffer, 0, length);
				}
	
				in.close();
				out.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void extractZipFile(String zipPath, String extractPath)
			throws Exception {

		ZipFile zipFile = new ZipFile(zipPath);
		if (zipFile.isEncrypted()) {
			zipFile.setPassword("prashant");
		}
		zipFile.extractAll(extractPath);

	}

	/**
	 * Delete transaction data from device which have transaction date before 30 days
	 */
	@SuppressLint("SimpleDateFormat")
	public void deleteTransactionData() {
		//String[] tables = getTablenameForDelete();
		try {
			DBManager db = new DBManager(context);
			db.open();
			//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_MONTH, -30);
			
			/**
			 * VesselInspection and related data
			 */
			
			
			
			
			
			db.close();	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public String getXmlReceivePath() {
		//String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public String getXmlBackupPath() {
		String strBackupPath = "";

		return strBackupPath;
	}
	
	@SuppressWarnings("static-access")
	public void setDeviceTime() {
		try {
			if(strDownloadDate != null && !"".equals(strDownloadDate))
			{
				Calendar c = Calendar.getInstance();
				//c.set(2013, 8, 15, 12, 34, 56);
				c.setTimeInMillis(Long.parseLong(strDownloadDate));
				AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
				am.setTime(c.getTimeInMillis());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}