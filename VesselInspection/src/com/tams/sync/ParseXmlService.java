package com.tams.sync;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.lingala.zip4j.core.ZipFile;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.os.Environment;
import android.os.Handler;
import android.widget.MultiAutoCompleteTextView.CommaTokenizer;

import com.tams.fragment.AdminTab;
import com.tams.model.DeviceSyncTime;
import com.tams.model.FilledFormImages;
import com.tams.model.SyncHistory;
import com.tams.model.SyncStatus;
import com.tams.model.VesselInspection;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;

public class ParseXmlService {

	/**
	 * @author Ripunjay Shukla
	 * @see parse xml of given zip and save and update data in db
	 */

	Handler handler;
	public static Context context;

	private String xmlProcessPath = "";
	private String strDownloadDate;

	public ParseXmlService() {

	}

	public ParseXmlService(Context context) {
		super();
		this.context = context;
	}

	public void startParseXmlAfterZip() {
		try {

			parseXmlForSync();
			
			if(CommonUtil.getSendTemplateData(context) != null && "0".equalsIgnoreCase(CommonUtil.getSendTemplateData(context)))
			{
				updateDirtyFlagOfTables();
			}
			
			deleteTransactionData();
			
			/**
			 * Update active state of vesselinspection.
			 */
			if(CommonUtil.getActiveInspectionID(context) != null && !"".equals(CommonUtil.getActiveInspectionID(context))){
				DBManager db = new DBManager(context);
				db.open();
				List<VesselInspection> vslInspList = db.getVesselInspectionDataById(CommonUtil.getActiveInspectionID(context));
				if(vslInspList != null && vslInspList.size() > 0){
					VesselInspection vslInsp = vslInspList.get(0);
					if(vslInsp.getiShipId() == Integer.parseInt(CommonUtil.getSyncShipId(context) != null && !"".equals(CommonUtil.getSyncShipId(context)) ? CommonUtil.getSyncShipId(context) : "-1"))
					{
						if(vslInsp.getFlgStatus() < 3)
						{
							vslInsp.setFlgStatus(0);
							db.updateVesselInspectionTable(vslInsp);
						}
					}
					CommonUtil.setActiveInspectionID(context, null);
					CommonUtil.setDropDbFlag(context, "0");
				}
				db.close();
			}
			
			CommonUtil.updateDeviceSyncTimeTable(context, CommonUtil.getSyncShipId(context));
			
			setDeviceTime();
			
			CommonUtil.deleteAllRelatedDataOfInactiveInspection(context);
			
			updateSyncStatusTable();
						
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DBManager db = new DBManager(context);
		db.open();
		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				db.updateDirtyRecordInTables(tbl,
						CommonUtil.getSyncShipId(context), CommonUtil.getAllActiveInspIds(context));
			}
		
		}
		db.close();
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for parse xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { DBHelper.FILLED_CHECKLIST, DBHelper.FILLED_FORM,
				DBHelper.VESSEL_INSPECTION, DBHelper.FILLED_FORM_IMAGE,DBHelper.TABLE_VesselInspectionFormListDesc};

		return tablename;
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for parse xml and comes in sync.
	 */
	public String[] getTablenameForDelete() {
		String[] tablename = { "PowerPlusTransaction", "TrainingTransaction"};

		return tablename;
	}
	
	public boolean fileExist() {
		boolean exist = false;
		xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP") && name.contains("Database");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	@SuppressLint("DefaultLocale")
	public void parseXmlForSync() {

		Map<String, List<String>> totalShiporShoreDatatoSync = new HashMap<String, List<String>>();
		List<String> syncOrderIdsForShip = new ArrayList<String>();
		
		final String strMacId = CommonUtil.getMacId(context);

		xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP") && name.contains(strMacId);
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {

			for (String fileName : allzipFileFromXmlProcessPath) {

				String[] splitFileNameArray = fileName.split("_");

				String keystr = splitFileNameArray[3].replaceAll(".zip", "")
						.replaceAll(".ZIP", "") + "_" + splitFileNameArray[2];

				// order tenant ship
				List<String> list = totalShiporShoreDatatoSync.get(keystr);
				if (list == null) {
					list = new ArrayList<String>();
				}
				list.add(fileName);

				syncOrderIdsForShip.add(splitFileNameArray[3].replaceAll(
						".zip", "").replaceAll(".ZIP", "")
						+ "_" + splitFileNameArray[1]);
				totalShiporShoreDatatoSync.put(keystr, list);

				for (Iterator<String> iterator = totalShiporShoreDatatoSync
						.keySet().iterator(); iterator.hasNext();) {

					String keyStrValue = iterator.next();
					String tenantid = keyStrValue.split("_")[1];
					String shipid = keyStrValue.split("_")[0];

					List<String> totalFileForthisTenantShip = totalShiporShoreDatatoSync
							.get(keyStrValue);
					/** overriding the sort method to do integer sorting for a
					 *  list of strings passed
					 */
					Collections.sort(totalFileForthisTenantShip,
							new Comparator<String>() {
								@Override
								public int compare(String o1, String o2) {
									return Integer.parseInt(o1.split("_")[1])
											- Integer.parseInt(o2.split("_")[1]);

								}
							});

					for (Iterator<String> iterator2 = totalFileForthisTenantShip
							.iterator(); iterator2.hasNext();) {

						String zipFileName = iterator2.next();
						String[] arr1 = zipFileName.split("_");

						String extractPath = xmlProcessPath + "/extractZip"
								+ System.currentTimeMillis();

						try {
							extractZipFile(xmlProcessPath + "/" + zipFileName,
									extractPath);
						} catch (Exception e1) {							
							e1.printStackTrace();
						}

						/**
						 * Check txt file for send template data;
						 */
						File txtFile = new File(extractPath + "/sendtemplate.txt");
						if(txtFile.exists()){
							CommonUtil.setSendTepmlateData(context, "1");
						}
						else{
							CommonUtil.setSendTepmlateData(context, "0");
						}
						
						String allXmlFile[] = new File(extractPath)
								.list(new FilenameFilter() {
									@Override
									public boolean accept(File dir, String name) {
										return name.toUpperCase().endsWith(
												".XML");

									}
								});

						// cleanProcessDirectory(new File(extractPath));

						if (allXmlFile != null) {
							Arrays.sort(allXmlFile);
							for (String filenameInExtractedDir : allXmlFile) {

								if("database.xml".equalsIgnoreCase(filenameInExtractedDir)){
									try {
										parseImageXmlFile(extractPath + "/"
												+ filenameInExtractedDir, extractPath);

									} catch (Exception e) {

										e.printStackTrace();
									}
								}
								else{
									try {
										parseXmlFile(extractPath + "/"
												+ filenameInExtractedDir);
	
									} catch (Exception e) {
	
										e.printStackTrace();
									}
								}

							}
						}

					}

				}
			}
		}

		cleanProcessDirectory(new File(xmlProcessPath));

	}

	/**
	 * This method is called for each file to be parsed and updated in the
	 * database
	 * 
	 * @param filename
	 */
	public void parseXmlFile(String filename) {
		String strIshipid = null;
		String strItenantid = null;
		String tablename = null;
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse("file://" + filename);
			doc.normalize();
			Node parentNode = doc.getFirstChild();
			strIshipid = parentNode.getAttributes().getNamedItem("ishipId")
					.getNodeValue();
			strItenantid = parentNode.getAttributes().getNamedItem("iTenantId")
					.getNodeValue();

			NodeList nodeList = parentNode.getChildNodes();

			for (int i = 0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);

				String pk = parentNode.getAttributes().getNamedItem("pk")
						.getNodeValue();

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					tablename = node.getNodeName();

					List<String> lst = new ArrayList<String>();
					DBManager dataBase = new DBManager(context);
					dataBase.open();
					lst = dataBase.getPkDataForSync(pk, tablename);
					dataBase.close();

					NodeList nodeList2 = node.getChildNodes();

					for (int j = 0; j < nodeList2.getLength(); j++) {
						Node tempNode = nodeList2.item(j);
						NodeList nodeList21 = tempNode.getChildNodes();

						Map<String, String> onerecordData = new HashMap<String, String>();

						for (int j1 = 0; j1 < nodeList21.getLength(); j1++) {
							Node tempNode1 = nodeList21.item(j1);
							onerecordData.put(tempNode1.getNodeName(),
									tempNode1.getTextContent()
											+ "____"
											+ tempNode1.getAttributes()
													.getNamedItem("t")
													.getNodeValue());
						}

						if (lst.contains(onerecordData.get(pk).split("____")[0])) {

							if (onerecordData.get(pk).split("____")[1]
									.equalsIgnoreCase("int")
									|| onerecordData.get(pk).split("____")[1]
											.equalsIgnoreCase("double")) {

								/**
								 * UPDATE
								 */
								dataBase = new DBManager(context);
								dataBase.open();
								dataBase.updateRecordInTables(
										tablename,
										pk,
										onerecordData.get(pk).split("____")[0],
										getContentValueOfTable(onerecordData, 1));
								dataBase.close();

							} else {
								/**
								 * UPDATE
								 */
								ContentValues contentValues = getContentValueOfTable(onerecordData, 1);
								if(tablename.equalsIgnoreCase(DBHelper.TABLE_SYNC_HISTORY)){
									//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
									//Date curDate = new Date();
									strDownloadDate = contentValues.getAsString("dtGeneratedDate");
									contentValues.put("dtAcknowledgeDate", contentValues.getAsString("dtGeneratedDate"));
									contentValues.put("flgIsDirty", 1);
								}
																
								dataBase = new DBManager(context);
								dataBase.open();
								
								if(tablename.equalsIgnoreCase(DBHelper.VESSEL_INSPECTION)){		
									List<VesselInspection> viList = dataBase.getVesselInspectionDataById(contentValues.getAsString(DBHelper.VESSEL_INSPECTIONS_ID));
									if(viList != null && viList.size() > 0){
										VesselInspection vInsp = viList.get(0);
										if (vInsp.getFlgStatus() >= 0) {
											if(vInsp.getFlgStatus() > 2 && contentValues.getAsInteger(DBHelper.FLG_STATUS) < 2){
												vInsp.setFlgStatus(-1);
											}
											else{
												vInsp.setFlgStatus(contentValues.getAsInteger(DBHelper.FLG_STATUS));
											}						
											
										} else if (contentValues.getAsInteger(DBHelper.FLG_STATUS) > 2) {
											vInsp.setFlgStatus(contentValues.getAsInteger(DBHelper.FLG_STATUS));
										}
										
										contentValues.put(DBHelper.FLG_STATUS, vInsp.getFlgStatus());
										
										/*if(contentValues.getAsInteger(DBHelper.FLG_STATUS) < 2)
										{
											
										}*/
									}							
									
								}
								
								
								dataBase.updateRecordInTables(
										tablename,
										pk,
										onerecordData.get(pk).split("____")[0],
										contentValues);
								dataBase.close();

							}
						} else {

							/**
							 * insert
							 */
							ContentValues contentValues = getContentValueOfTable(onerecordData, 0);
							if(tablename.equalsIgnoreCase(DBHelper.TABLE_SYNC_HISTORY)){
								//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								//Date curDate = new Date();
								strDownloadDate = contentValues.getAsString("dtGeneratedDate");
								contentValues.put("dtAcknowledgeDate", contentValues.getAsString("dtGeneratedDate"));
								contentValues.put("flgIsDirty", 1);
							}
							
							if(tablename.equalsIgnoreCase(DBHelper.VESSEL_INSPECTION)){		
								if(contentValues.getAsInteger(DBHelper.FLG_STATUS) < 2)
								{
									contentValues.put(DBHelper.FLG_STATUS, -1);
								}
								
							}	
							
							dataBase = new DBManager(context);
							dataBase.open();
							dataBase.insertRecordInTables(tablename,contentValues);
							dataBase.close();

						}

					}
					lst = null;

				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} 
	}
	
	/**
	 * This method is called for each file to be parsed and updated in the
	 * database
	 * 
	 * @param filename
	 */
	public void parseImageXmlFile(String filename, String extractPath) {
		String strIshipid = null;
		String strItenantid = null;
		String tablename = null;
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse("file://" + filename);
			doc.normalize();
			Node parentNode = doc.getFirstChild();
			strIshipid = parentNode.getAttributes().getNamedItem("ishipId")
					.getNodeValue();
			strItenantid = parentNode.getAttributes().getNamedItem("iTenantId")
					.getNodeValue();

			NodeList nodeList = parentNode.getChildNodes();

			int fileCount = 0;
			int fileCountFailed = 0;
			
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					String fileName = node.getAttributes()
							.getNamedItem("name").getNodeValue();
					/*String path = node.getAttributes().getNamedItem("path")
							.getNodeValue();*/
					
					String formid = node.getAttributes().getNamedItem("vesselInspectionFormListId").getNodeValue();
					String formImageId = node.getAttributes().getNamedItem("vesselInspectionImageId").getNodeValue();
					String vesselInspectionId = node.getAttributes().getNamedItem("vesselInspectionId").getNodeValue();					
					String formSectionItemId = node.getAttributes().getNamedItem("formSectionItemId").getNodeValue();
					
					File storageDir = new File(Environment.getExternalStorageDirectory(),
                            context.getResources().getString(R.string.imagePath) + strIshipid + "/" + formid);
					
					//File copyDir = new File(path);
					if (!storageDir.exists()) {
						if (!storageDir.mkdirs()) {
							fileCountFailed = fileCountFailed + 1;
						}
					}
					File src = new File(extractPath + File.separator
							+ fileName);
					File dest = new File(storageDir.getAbsoluteFile()
							+ File.separator + fileName);
					copyFile(src, dest);
					
					DBManager dbm = new DBManager(context);
					dbm.open();
					List<FilledFormImages> imageList = dbm.getFilledFormImagesById(formImageId);
					if(imageList != null && imageList.size() > 0){
						FilledFormImages ffimg = imageList.get(0);
						ffimg.setDeviceOriginalImagePath("/file:"+dest.getAbsolutePath());
						ffimg.setDeviceRelativeImagePath(dest.getAbsolutePath());
						
						dbm.updateFilldFormImageTable(ffimg);
					}
					else{
						FilledFormImages ffimg = new FilledFormImages();
						ffimg.setStrFilledFormImageId(formImageId);
						ffimg.setiFilledFormId(formid);
						ffimg.setiVesselInspectionId(vesselInspectionId);
						ffimg.setiFormSectionItemId(formSectionItemId);
						ffimg.setStrImageName(fileName);
						ffimg.setDeviceOriginalImagePath("/file:"+dest.getAbsolutePath());
						ffimg.setDeviceRelativeImagePath(dest.getAbsolutePath());
						ffimg.setiTenantId(Integer.parseInt(CommonUtil.getTenantId(context)));
						ffimg.setiShipId(Integer.parseInt(CommonUtil.getSyncShipId(context)));
						
						dbm.insertFilldFormImageTable(ffimg);
						
					}
					dbm.close();			
					
					
					fileCount = fileCount + 1;				
					
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} 
	}

	public ContentValues getContentValueOfTable(
			Map<String, String> onerecordData, int flg) {

		ContentValues values = new ContentValues();

		StringBuffer sqlstr = new StringBuffer();
		// String sqlstr = "(";
		sqlstr.append("(");
		Set<String> allkeyset = onerecordData.keySet();
		if (flg == 0) {

			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {

				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}

		}
		if (flg == 1) {
			// sqlstr = " SET ";
			sqlstr = new StringBuffer();
			sqlstr.append(" SET ");
			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {
				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}
		}
		return values;
	}

	public void extractZipFile(String zipPath, String extractPath)
			throws Exception {

		ZipFile zipFile = new ZipFile(zipPath);
		if (zipFile.isEncrypted()) {
			zipFile.setPassword("prashant");
		}
		zipFile.extractAll(extractPath);

	}

	/**
	 * Delete transaction data from device which have transaction date before 30 days
	 */
	@SuppressLint("SimpleDateFormat")
	public void deleteTransactionData() {
		//String[] tables = getTablenameForDelete();
		try {
			DBManager db = new DBManager(context);
			db.open();
			//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_MONTH, -30);
			
			/**
			 * VesselInspection and related data
			 */
			
			
			
			
			
			db.close();	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public String getXmlReceivePath() {
		//String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public String getXmlBackupPath() {
		String strBackupPath = "";

		return strBackupPath;
	}
	
	public void copyFile(File sourceFile, File destFile){
		try {
			
			/*if(!destFile.exists()){
				destFile.mkdirs();
			}*/
			if(sourceFile.exists())
			{
				InputStream in = new FileInputStream(sourceFile);
				OutputStream out = new FileOutputStream(destFile);
	
				byte[] buffer = new byte[1024];
	
				int length;
				// copy the file content in bytes
				while ((length = in.read(buffer)) > 0) {
				    out.write(buffer, 0, length);
				}
	
				in.close();
				out.close();
			}
			else if(destFile.exists()){
				destFile.delete();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public void setDeviceTime() {
		try {
			if(strDownloadDate != null && !"".equals(strDownloadDate))
			{
				Calendar c = Calendar.getInstance();
				//c.set(2013, 8, 15, 12, 34, 56);
				c.setTimeInMillis(Long.parseLong(strDownloadDate));
				AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
				am.setTime(c.getTimeInMillis());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	private void updateSyncStatusTable(){
		try{
			DBManager db = new DBManager(context);
			SyncStatus syncStatus = null; 
			db.open();
			//DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dtFormat = new SimpleDateFormat("d MMM yyyy");
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			if(syncStatusList != null && syncStatusList.size() > 0){
				syncStatus = new SyncStatus();
				syncStatus = syncStatusList.get(0);
				syncStatus.setDtSyncDate(dtFormat.format(new Date()));
				db.updateSyncStatusTable(syncStatus);
			}
			else{
				
				String url = "";
				if(CommonUtil.getServerAddress(context) == null || "null".equalsIgnoreCase(CommonUtil.getServerAddress(context)) || "".equals(CommonUtil.getServerAddress(context))){
					url = context.getResources().getString(R.string.defultServiceUrl);
				}
				else{
					url = CommonUtil.getServerAddress(context);
				}
				syncStatus = new SyncStatus("123" + new Date().getTime(),
						dtFormat.format(new Date()),
						CommonUtil.getDataAccessMode(context),
						url,
						Integer.parseInt(CommonUtil.getTenantId(context) != null && !"null".equalsIgnoreCase(CommonUtil.getTenantId(context)) && !"".equals(CommonUtil.getTenantId(context)) ? CommonUtil.getTenantId(context) : "0"), 0, 0, 1,
						dtFormat.format(new Date()),
						dtFormat.format(new Date()),
						CommonUtil.getUserId(context),
						CommonUtil.getUserId(context),CommonUtil.getDataSyncModeFromDb(context));
				
				db.insertSyncStatusTable(syncStatus);
				
			}
			/*if(AdminTab.syncDate != null && !"".equals(AdminTab.syncDate))
				AdminTab.syncDate.setText("Last Sync Date : "+dtFormat.format(new Date()));*/
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}
