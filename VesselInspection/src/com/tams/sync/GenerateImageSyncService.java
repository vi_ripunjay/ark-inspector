package com.tams.sync;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.telephony.TelephonyManager;

import com.tams.model.SyncHistory;
import com.tams.model.VesselInspection;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;

public class GenerateImageSyncService {

	/**
	 * @author Ripunjay Shukla
	 * @since 18-11-2015 This class write for generate zip of xml for sync.
	 */

	private Map<String, Set<String>> tableColumnMap;

	private Map<String, String[][]> tableValueMap;

	private Context context;

	@SuppressWarnings("unused")
	private int lineCounter = 0;
	private int recordNum = 0;

	private String baseFoldername = "";

	private StringBuffer logMessages;

	public GenerateImageSyncService() {

	}

	public GenerateImageSyncService(Context mcontext) {

		this.context = mcontext;
	}

	public void generateXmlForDirtyRecord() {

		DBManager db = new DBManager(context);
		db.open();

		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow(CommonUtil.getSyncShipId(context),
				CommonUtil.getMacId(context));

		// cleanProcessDirectory(new File(getXmlSendPath()));

		/*
		 * if (!(shList == null || shList.size() == 0)) {
		 * 
		 * File sourceFile = generateZipForInitialSync(context);
		 * 
		 * String destinationFile = getInitialZipProcessPath(baseFoldername);
		 * createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);
		 * mooveFiletoSendFolder(destinationFile);
		 * cleanProcessDirectory(getXmlProcessPath());
		 * 
		 * } else {
		 */
		String iShipId = String.valueOf(CommonUtil.getSyncShipId(context));
		String iTenantId = String.valueOf(CommonUtil.getTenantId(context));
		CommonUtil.setShipID(context, iShipId);
		CommonUtil.setTenantID(context, iTenantId);
		File sourceFile = createOneTablerecord(context);

		String destinationFile = getZipProcessPath(baseFoldername);
		createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);

		// cleanProcessDirectory(new File(getXmlSendPath()));
		mooveFiletoSendFolder(destinationFile);

		cleanProcessDirectory(getXmlProcessPath());
		// updateDirtyFlagOfTables(); //update dirty record as undirty after
		// parsing data.
		// }
		db.close();

	}

	public File getXmlProcessPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/");
		File procFile = new File(sb.toString());
		return procFile;
	}

	public String getZipProcessPath(String strFilename) {

		Date curDate = new Date();
		String uuid = CommonUtil.getUUId(context);
		CommonUtil.setGeneratedUuid(context, uuid);
		;

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory()
						+ "/xmlProcess/"
						+ strFilename
						+ getMacId()
						+ "_"
						+ (CommonUtil.getUserId(context) != null ? CommonUtil
								.getUserId(context) : "0") + "_" + uuid + "_"
						+ curDate.getTime() + "_Database-Synchronization_.zip");

		return sb.toString();
	}

	public String getInitialZipProcessPath(String strFilename) {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/"
						+ strFilename + getMacId() + "_"
						+ CommonUtil.getUserId(context)
						+ "_Initial-Synchronization_.zip");

		return sb.toString();
	}

	public String getXmlSendPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Export/");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}

		/**
		 * below for only creation directory
		 */
		StringBuffer sbRec = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Import/");
		File fileRec = new File(sbRec.toString());
		if (!fileRec.exists()) {
			fileRec.mkdirs();
		}

		return sb.toString();
	}

	public File getBaseFolderpath() {
		StringBuffer baseFolderName = new StringBuffer("TABLETSHIP");
		baseFolderName.append("_"
				+ 0
				+ "_"
				+ (CommonUtil.getTenantId(context) != null
						&& !"null".equalsIgnoreCase(CommonUtil
								.getTenantId(context)) ? CommonUtil
						.getTenantId(context) : "0")
				+ "_"
				+ (CommonUtil.getSyncShipId(context) != null ? CommonUtil
						.getSyncShipId(context) : "0") + "_");

		File file = new File(Environment.getExternalStorageDirectory()
				+ "/xmlProcess/" + baseFolderName);
		if (!file.exists()) {
			file.mkdirs();
		}
		baseFoldername = baseFolderName.toString();
		return file;
	}

	/**
	 * @author ripunjay
	 * @return pass table for generate xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { DBHelper.FILLED_FORM_IMAGE };

		return tablename;
	}

	/**
	 * @author Ripunjay
	 * @param mContext
	 * @return create xml of given tables
	 */
	public File createOneTablerecord(Context mContext) {
		try {
			context = mContext;
			boolean createFileWriteOnfirstRecord = true;
			Writer fileWriter = null;
			StringBuffer dataXml = new StringBuffer();
			int recordcount = 0;
			logMessages = new StringBuffer();
			String shipid = CommonUtil.getSyncShipId(mContext);
			String tenantid = CommonUtil.getTenantId(mContext);

			File sourceFile = getBaseFolderpath();

			DBManager db = new DBManager(mContext);
			db.open();

			String[] tablename = getTablename();

			/**
			 * Get Active vessel inspection from database.
			 */
			List<VesselInspection> activeVsl = new ArrayList<VesselInspection>();
			activeVsl = db.getVesselInspectionDataByflgStatusAndShipIdForSync(
					0, shipid);
			StringBuffer activeVslId = new StringBuffer("'000'");

			if (CommonUtil.getActiveInspectionID(mContext) != null
					&& !"".equals(CommonUtil.getActiveInspectionID(mContext))) {
				activeVslId.append(",'"
						+ CommonUtil.getActiveInspectionID(mContext) + "'");
			} else {
				if (activeVsl != null && activeVsl.size() > 0) {
					for (VesselInspection vslInsp : activeVsl) {
						activeVslId.append(",'"
								+ vslInsp.getiVesselInspectionId() + "'");
					}
				}
			}

			for (int i = 0; i < tablename.length; i++) {

				recordcount = 0;
				lineCounter = 0;
				// recordNum = 0;

				createFileWriteOnfirstRecord = true;
				Cursor cursor = db.getDataForSync(tablename[i], shipid,
						CommonUtil.getMacId(mContext), activeVslId.toString());

				long dirtyCount = db.getFilledFormImagesDirtyCount(activeVslId
						.toString());

				List<String[]> tblDetails = db
						.getDbTableColumnDetails(tablename[i]);

				try {
					if (tblDetails != null && tblDetails.size() > 0) {

						// looping through all rows and adding to list
						if (cursor != null && cursor.moveToFirst()) {

							/*
							 * if (cursor .getInt(cursor
							 * .getColumnIndexOrThrow(DBHelper.FLG_DELETED)) ==
							 * 0){
							 */
							if (dirtyCount > 0) {
								if (createFileWriteOnfirstRecord) {

									String pk = tblDetails.get(0)[0];

									dataXml = new StringBuffer(
											"<files iTenantId='" + tenantid
													+ "' ishipId='" + shipid
													+ "' pk='" + pk + "'>");
									fileWriter = new BufferedWriter(
											new OutputStreamWriter(
													new FileOutputStream(
															sourceFile
																	.getAbsolutePath()
																	+ "/"
																	+ "database"// tablename[i]
																	+ ".xml"),
													"UTF-8"));

									createFileWriteOnfirstRecord = false;
								}

								do {
									recordcount++;
									recordNum++;
									lineCounter++;
									// dataXml = dataXml + "<r" + recordcount +
									// ">";

									if (cursor
											.getInt(cursor
													.getColumnIndexOrThrow(DBHelper.FLG_DELETED)) == 0) {
										File fileForName = new File(
												cursor.getString(cursor
														.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)));
										if (fileForName.exists()) {
											dataXml.append("<file name='"
													+ (fileForName.exists() == true ? fileForName
															.getName()
															: "test.jpg")
													+ "' "
													+ " vesselInspectionId='"
													+ cursor.getString(cursor
															.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID))
													+ "' "
													+ " formSectionItemId='"
													+ cursor.getString(cursor
															.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID))
													+ "' "
													+ " vesselInspectionFormListId='"
													+ cursor.getString(cursor
															.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID))
													+ "' "
													+ " vesselInspectionImageId='"
													+ cursor.getString(cursor
															.getColumnIndexOrThrow(DBHelper.FILLED_FORM_IMAGE_ID))
													+ "'></file>");

											try {
												copyFile(
														cursor.getString(cursor
																.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)),
														sourceFile
																.getAbsolutePath());

											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										} else {
											System.out
													.println("file not found.");
										}
										// dataXml = dataXml + "</r" +
										// recordcount +
										// ">";
									}
								} while (cursor.moveToNext());

								// dataXml = dataXml + "</" + tablename[i] +
								// ">";
								dataXml.append("</files>");
								fileWriter.write(dataXml.toString());
								fileWriter.close();

								logMessages.append(" " + tablename[i]
										+ " : record : " + recordcount);
							}
						}

					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} finally {
					cursor.close();
				}

			}
			if (recordNum > 0) {
				insertSyncHistoryRecord(logMessages.toString(), "progress",
						"TABLETSHIP", baseFoldername,
						Integer.parseInt(tenantid), Integer.parseInt(shipid));
			}

			db.close();
			return sourceFile;
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;

	}

	@SuppressLint("SimpleDateFormat")
	public void insertSyncHistoryRecord(String logMessage, String progress,
			String generator, String strFilename, int iTenantId, int iShipId) {

		// DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		// Date curDate = new Date();

		DBManager db = new DBManager(context);
		db.open();

		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow(CommonUtil.getSyncShipId(context),
				CommonUtil.getMacId(context));
		SyncHistory syncHistory = new SyncHistory();
		if (shList != null && shList.size() > 0) {
			syncHistory = shList.get(0);
			syncHistory.setLogMessage(logMessage);
		}

		if (shList != null && shList.size() > 0) {
			syncHistory.setFlgIsDirty(1);
			if (syncHistory.getDtAcknowledgeDate() == null) {
				syncHistory.setDtAcknowledgeDate(syncHistory
						.getDtGeneratedDate());
			}
			db.updateSyncHistorydata(syncHistory);
		}

		db.close();

	}

	public File generateZipForInitialSync(Context mContext) {
		/**
		 * Write initial sync zip code here.
		 */
		try {

			String baseFolderName = "TABLETSHIP_0_0_0_";
			File sourceFile = new File(
					Environment.getExternalStorageDirectory() + "/xmlProcess/"
							+ baseFolderName);
			if (!sourceFile.exists()) {
				sourceFile.mkdirs();
			}

			baseFoldername = baseFolderName;

			context = mContext;
			Writer fileWriter = null;
			String dataXml = "";
			logMessages = new StringBuffer();
			String shipid = CommonUtil.getSyncShipId(mContext);
			String tenantid = CommonUtil.getTenantId(mContext);

			dataXml = "<tables iTenantId='" + tenantid + "' ishipId='" + shipid
					+ "' pk=''>";

			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(sourceFile.getAbsolutePath()
							+ "/initialSync.xml"), "UTF-8"));

			dataXml = dataXml + "</tables>";

			fileWriter.write(dataXml);
			fileWriter.close();

			/*
			 * insertSyncHistoryRecord("Initial Sync", "progress", "TABLETSHIP",
			 * baseFoldername, Integer.parseInt("1"), Integer.parseInt("1"));
			 */

			return sourceFile;

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DBManager db = new DBManager(context);
		db.open();

		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				db.updateDirtyRecordInTables(tbl,
						CommonUtil.getSyncShipId(context),
						CommonUtil.getAllActiveInspIds(context));
			}

		}
		db.close();
	}

	public Map<String, Set<String>> getTableColumnMap() {
		return tableColumnMap;
	}

	public void setTableColumnMap(Map<String, Set<String>> tableColumnMap) {
		this.tableColumnMap = tableColumnMap;
	}

	public Map<String, String[][]> getTableValueMap() {
		return tableValueMap;
	}

	public void setTableValueMap(Map<String, String[][]> tableValueMap) {
		this.tableValueMap = tableValueMap;
	}

	public void createZipFileofFolder(String folderPath, String zipFilePath) {
		try {
			File fileToAdd = new File(folderPath);
			if (fileToAdd.exists()) {
				ZipFile zipFile = new ZipFile(zipFilePath);
				ZipParameters parameters = new ZipParameters();
				parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
				parameters
						.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
				parameters.setEncryptFiles(true);
				parameters
						.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword("prashant");
				String[] fileArray = fileToAdd.list();
				for (int i = 0; i < fileArray.length; i++) {
					L.fv("Added in zip" + fileArray[i]);
					File tobeAdded = new File(fileToAdd.getAbsolutePath()
							+ File.separator + fileArray[i]);
					if (tobeAdded.isDirectory()) {
						zipFile.addFolder(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					} else {
						zipFile.addFile(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					}
				}
			}

		} catch (ZipException e) {
			e.printStackTrace();

		}
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public void copyFile(String src, String dest) {
		try {
			File sourceFile = new File(src);

			File destFile = new File(dest);
			if (!destFile.exists()) {
				destFile.mkdirs();
			}
			File destFiles = new File(dest + File.separator
					+ sourceFile.getName());

			if (sourceFile.exists()) {
				InputStream in = new FileInputStream(sourceFile);
				OutputStream out = new FileOutputStream(destFiles);

				byte[] buffer = new byte[1024];

				int length;
				// copy the file content in bytes
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}

				in.close();
				out.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean mooveFiletoSendFolder(String filetomoove) {
		File file = new File(filetomoove);
		File dir = new File(getXmlSendPath());
		return file.renameTo(new File(dir, file.getName()));

	}

	public String getMacId() {
		String strmacId = "macid";
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		strmacId = tm.getDeviceId();
		return strmacId;
	}

	public String getPk() {
		String pk = "";
		Date curDate = new Date();
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		pk = tm.getDeviceId() + "_" + curDate.getTime();

		return pk;
	}

}
