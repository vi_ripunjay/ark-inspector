package com.tams.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import android.content.Context;
import android.util.Log;

import com.tams.model.SyncStatus;
import com.tams.sql.DBManager;
import com.tams.vessalinspection.R;

public class SyncServices extends SoapBase{

private static final String TAG = SyncServices.class.getSimpleName();
	
	public SyncServices(){
		
	}
	
	public SyncServices( Context context){
		super.ipaddress = ipaddress;		
		super.URL = getServerAddress(context);
	}

	
	
	public int ping(){
		String method = "Ping";
		int resp = 0;
        try {
    		init(method);
            // Decode result
            SoapObject soapobj = (SoapObject)envelope.bodyIn;
            Object property = soapobj.getProperty(0);
            String response = property.toString();
            return (response == null)? resp: Integer.valueOf(response);
        }catch(Exception e) {
        	Log.e(TAG, ""+e.getMessage());
        	return -1;
        }
	}
	
		
	@Override
	public String process(String method, PropertyInfo... pi) {
        try {
    		init(method, pi);
            // Decode result
            SoapObject soapobj = (SoapObject)envelope.bodyIn;
            String data = soapobj.getProperty(0).toString();
            response= decode(data);
        }
        catch(Exception e) {
        	Log.e(TAG, e.getMessage());
        }
        return response;
	}

	
	public InputStream processstream(String method, boolean isZip, PropertyInfo... pi) {
        try {
    		init(method, pi);
            // Decode result
            SoapObject soapobj = (SoapObject)envelope.bodyIn;
            String data = soapobj.getProperty(0).toString();
            InputStream is = decodestream(data, isZip);
            return is;
        }
        catch(Exception e) {
        	Log.e(TAG, e.getMessage());
        }
		return null;
	}
	
	public String process(InputStream is){
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
    
			StringBuffer buffer = new StringBuffer();
			char[] charBuffer = new char[1024]; 
			while(in.read(charBuffer) != -1) {
				buffer.append(charBuffer);
			}
			return  buffer.toString();
       	}
        catch(Exception e) {
        	Log.e(TAG, e.getMessage());
        }
        return "";
	}
	
	
	/**
	 * @author pushkar.m
	 * @param context
	 * @return
	 * This method return server address . First fetching from DB , if null then pick from String.xml
	 */
	private String getServerAddress(Context context){
		String serverAddress = null;
		DBManager db = new DBManager(context);
		db.open();
		
		List<SyncStatus> syncStatusList = db.getSyncStatusData();
		
		db.close();
		
		if(syncStatusList != null && syncStatusList.size() > 0){		
			
			serverAddress = syncStatusList.get(0).getServerAddress();			
		}	
		
		if(serverAddress == null || "".equals(serverAddress.trim())){
			serverAddress = context.getResources().getString(R.string.defultServiceUrl);
		}		
		
		return serverAddress;
	}
	
}
