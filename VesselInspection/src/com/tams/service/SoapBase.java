package com.tams.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;

public abstract class SoapBase {

	protected static String NAMESPACE = "http://service.smsWS.session.ark.com/";// "http://tempuri.org/";
	protected String SOAP_ACTION = "http://service.smsWS.session.ark.com/";// "http://tempuri.org/";
	// "http://172.28.5.158:80/NECSyncEnginev2/SyncServices.asmx";//"http://172.28.1.241:80/NECSyncEngineEAFMCG/SyncServices.asmx";//
	protected String ipaddress;
	protected String port;
	protected String URL = "/com.ark.session.smsWS.service/SmsService?wsdl";// "/NECSyncEngine/SyncServices.asmx";//"/VTVHService.asmx";//
	// protected String URL =
	// "http://update.necmalaysia.com.my:81/NECSyncEngine/SyncServices.asmx";
	protected SoapSerializationEnvelope envelope;
	// "http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl"

	// http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl
	private static final String TAG = SoapBase.class.getSimpleName();

	protected int responseCode;
	protected String message;
	protected String response;
	protected int TimeOut = 500000;

	public String getResponse() {
		return response;
	}

	public String getErrorMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}

	protected void init(String method, PropertyInfo... pi) {
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.implicitTypes = true;

		SoapObject Request = new SoapObject(NAMESPACE, method);
		for (PropertyInfo property : pi) {
			Request.addProperty(property);
		}
		envelope.setAddAdornments(false);
		envelope.setOutputSoapObject(Request);

		String myProxy = android.net.Proxy.getDefaultHost();
		int myPort = android.net.Proxy.getDefaultPort();
		HttpTransportSE androidHttpTransport;
		if (myProxy != null) {
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
					myProxy, myPort));
			// androidHttpTransport = new HttpTransportSE(proxy, URL);
			androidHttpTransport = new HttpTransportSE(proxy, URL, TimeOut);
		} else {
			androidHttpTransport = new HttpTransportSE(URL, TimeOut);
		}
		androidHttpTransport
				.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		androidHttpTransport.debug = true;
		try {
			androidHttpTransport.call(SOAP_ACTION + method, envelope);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
		}
		
		/**
		 * Close connection after call service.
		 */
		if(androidHttpTransport!=null){
			androidHttpTransport.reset();
            try {
            	androidHttpTransport.getServiceConnection().disconnect();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
	}

	public abstract String process(String method, PropertyInfo... pi);

	protected String compress(String string) {
		String data = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream(
					string.length());
			GZIPOutputStream gos = new GZIPOutputStream(os);
			gos.write(string.getBytes());
			gos.close();
			byte[] compressed = os.toByteArray();
			os.close();
			data = Base64.encode(compressed);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		return data;
	}

	protected InputStream decodestream(String data, boolean isZip)
			throws IOException {
		byte[] bytes = Base64.decode(data);

		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		if (isZip) {
			return new GZIPInputStream(bais);
		} else {
			return bais;
		}
	}

	protected String decode(String data) throws IOException {
		byte[] bytes = Base64.decode(data);

		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		GZIPInputStream gzip = new GZIPInputStream(bais);
		StringBuffer buffer = new StringBuffer();
		try {
			InputStreamReader reader = new InputStreamReader(gzip, "UTF-8");
			try {
				BufferedReader in = new BufferedReader(reader);
				char[] charBuffer = new char[1024];
				while (in.read(charBuffer) != -1) {
					buffer.append(charBuffer);
				}
			} finally {
				reader.close();
			}
		} finally {
			gzip.close();
		}

		return buffer.toString();
	}

	/**
	 * TODO : GZIP BYTES
	 * 
	 * @param bytes
	 * @return
	 * @throws IOException
	 */
	protected String decode(byte[] bytes) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GZIPOutputStream gos = new GZIPOutputStream(baos);
		gos.write(bytes);
		gos.flush();
		gos.close();
		String encodedString = android.util.Base64.encodeToString(
				baos.toByteArray(), 0);
		Log.w("ENCODED", encodedString);
		return encodedString;
	}
	/**
	 * protected String decode(String data) throws IOException { byte[] bytes =
	 * Base64.decode(data);
	 * 
	 * ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
	 * GZIPInputStream gzip = new GZIPInputStream(bais); BufferedInputStream bis
	 * = null; DataInputStream dis = null; StringBuffer buffer = new
	 * StringBuffer(); try { bis = new BufferedInputStream(gzip); dis = new
	 * DataInputStream(bis);
	 * 
	 * while (dis.available() != 0) { buffer.append(dis.readLine()); }
	 * 
	 * } finally { try { gzip.close(); bis.close(); dis.close(); } catch
	 * (IOException ex) { ex.printStackTrace(); } } return buffer.toString(); }
	 */
}
