/**
 * @file WebServiceMethod.java
 * 
 * @author Prashant Jha
 * 
 * @date  September 21,2013
 */

package com.tams.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import com.tams.utils.CommonUtil;

import android.content.Context;
import android.util.Log;
import android.widget.MultiAutoCompleteTextView.CommaTokenizer;

public class WebServiceMethod extends SyncServices {

	private static final String TAG = WebServiceMethod.class.getName();

	private static final String METHOD_LOGIN_USER = "validateUser";// loginDeviceUser";
	private static final String METHOD_GETALLVESSELINSPECTION_DATA = "getAllVesselInspectionData";
	private static final String METHOD_SEND_VI_CHECKLIST = "sendVesselInspectionChecklist";
	private static final String METHOD_SEND_VI_FORMS = "sendVesselInspectionForms";
	private static final String METHOD_SEND_MI_FORMS = "sendmarpolInspectionForms";
	private static final String METHOD_SENDACK_CHECKLIST = "ackChecklist";
	private static final String METHOD_SENDACK_VI_FORM = "ackVIForm";
	private static final String METHOD_SENDACK_MI_FORM = "ackMIForm";
	private static final String METHOD_GET_CHECKLIST_SECTION = "getCheckListSectionData";
	private static final String METHOD_GET_CHECKLIST_SECTION_ITEM = "getCheckListSectionItemData";
	private static final String METHOD_GET_FORM_SECTION = "getFormSectionData";
	private static final String METHOD_GET_FORM_SECTION_ITEM = "getFormSectionItemData";

	private static final String METHOD_GET_SHIP_FORM_SECTION_ITEM = "getShipFormSectionItemData";

	private static final String METHOD_REGISTERDEVICE = "registerDevice";
	private static final String METHOD_GETALLUSERS = "getAllUser";
	private static final String METHOD_UPDATEALLUSERS = "getUpdatedUser";
	private static final String METHOD_GETSHIPATTRIBUTES = "GetShipAttributes";
	private static final String METHOD_GETCREWLISTDATA = "GetCrewListData";

	private static final String METHOD_SENDFILLEDFORMS = "updateVIData";
	private static final String METHOD_SENDFILLEDCHECKLIST = "updateVIData";
	private static final String METHOD_SENDVESSELINSPECTIONDATA = "updateVIData";

	private static final String METHOD_GETFILLEDFORMS = "getFilledForm";
	private static final String METHOD_SENDACKCONTENT = "ackContent";
	private static final String METHOD_UNREGISTERDEVICE = "unregisterDevice";
	private static final String METHOD_GETALLIMAGE = "getAllImage";
	private static final String METHOD_GETUPDATEDVICHECKLIST = "getFilledCheckList";

	private static final String METHOD_SEND_FORMIMAGES = "getVslFilledFormImage";
	private static final String METHOD_SENDACK_FORMIMAGES = "ackFormImages";
	private static final String METHOD_GETALLSHIP = "getAllShip";
	private static final String METHOD_GETFORMCATEGORY = "getVIFormCategory";
	private static final String METHOD_GETUPDATEDVIDATA = "getUpdatedVesselInspection";
	private static final String METHOD_GETUPDATEDVIDATA_BY_VERSION = "getUpdatedVesselInspectionByVersion";
	private static final String METHOD_ACKUPDATEDVIDATA = "ackUpdatedVIData";
	private static final String METHOD_GETUPDATEDVIFORM = "getUpdatedVIForms";
	private static final String METHOD_GETUPDATEDVIIMAGE = "getUpdatedVIFormImages";
	private static final String METHOD_SEND_ACK_FILLED_FORM_IMAGE = "ackViFormImage";
	private static final String METHOD_SEND_ACK_FILLED_FORM = "ackVIFilledForm";
	private static final String METHOD_GET_ROLE_TEMPLATE = "getRoleTemplateData";
	private static final String METHOD_GET_ROLE_TEMPLATE_BY_ROLE = "getRoleTemplateDataByRole";
	private static final String METHOD_MATCH_VISYNC_HISTORY = "matchVISyncHistory";
	private static final String METHOD_GET_SHIP_EXIST = "isShipExist";
	private static final String METHOD_SEND_FILLED_FORM_HEADER = "updateVIFormHeader";
	private static final String METHOD_SEND_FILLED_FORM_DESC = "updateVIFormDesc";
	private static final String METHOD_SEND_FILLED_FORM_FOOTER = "updateVIFormFooter";

	private static final String METHOD_GET_UPDATED_FILLED_FORM_DESC = "getVIFormListDesc";

	private static final String METHOD_GET_UPDATED_FILLED_FORM_HEADER = "getVIFormListHeader";
	private static final String METHOD_GET_UPDATED_FILLED_FORM_FOOTER = "getVIFormListFooter";
	private static final String METHOD_SEND_ACK_FILLED_FORM_HEADER = "ackVIFilledFormHeader";
	private static final String METHOD_SEND_ACK_FILLED_FORM_FOOTER = "ackVIFilledFormFooter";

	private static final String METHOD_GET_VI_TEMPALTE_VERSIOV = "getVITemplateVersion";
	private static final String METHOD_SEND_DEVICE_SYNC_TIME = "updateDeviceSyncTime";

	private PropertyInfo propertyMACID = null;
	private PropertyInfo propertyShipID = null;
	private PropertyInfo propertyHasMore = null;
	private PropertyInfo propertyUserID = null;
	private PropertyInfo propertyUserPassword = null;
	private PropertyInfo propertyTenantId = null;

	/**
	 * This method will call at the time of web service object creation
	 */
	public WebServiceMethod() {
		super();
	}

	/**
	 * This method will call at the time of web service object creation
	 * 
	 * @param port
	 */
	public WebServiceMethod(Context port) {
		super(port);
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 */
	public String getAllShip(String userId, int tenantId) {

		String method = METHOD_GETALLSHIP;

		PropertyInfo propertyUserId = new PropertyInfo();
		propertyUserId.setName("arg0");
		propertyUserId.setValue(userId);
		propertyUserId.setType(String.class);

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg1");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyUserId, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedVIForms(String viId, int shipId, String userLogin) {
		String method = METHOD_GETUPDATEDVIFORM;

		PropertyInfo propertyViId = new PropertyInfo();
		propertyViId.setName("arg0");
		propertyViId.setValue(viId);
		propertyViId.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(Integer.class);

		PropertyInfo propertyUserLogin = new PropertyInfo();
		propertyUserLogin.setName("arg2");
		propertyUserLogin.setValue(userLogin);
		propertyUserLogin.setType(String.class);

		try {
			init(method, propertyViId, propertyShipID, propertyUserLogin);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedVICheckList(String viId, int shipId,
			String userLogin) {
		String method = METHOD_GETUPDATEDVICHECKLIST;

		PropertyInfo propertyViId = new PropertyInfo();
		propertyViId.setName("arg0");
		propertyViId.setValue(viId);
		propertyViId.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(Integer.class);

		PropertyInfo propertyUserLogin = new PropertyInfo();
		propertyUserLogin.setName("arg2");
		propertyUserLogin.setValue(userLogin);
		propertyUserLogin.setType(String.class);

		try {
			init(method, propertyViId, propertyShipID, propertyUserLogin);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedVIFormImages(String viId, int shipId,
			String userLogin, String notInIds) {
		String method = METHOD_GETUPDATEDVIIMAGE;

		PropertyInfo propertyViId = new PropertyInfo();
		propertyViId.setName("arg0");
		propertyViId.setValue(viId);
		propertyViId.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(Integer.class);

		PropertyInfo propertyUserLogin = new PropertyInfo();
		propertyUserLogin.setName("arg2");
		propertyUserLogin.setValue(userLogin);
		propertyUserLogin.setType(String.class);

		PropertyInfo propertyNotInId = new PropertyInfo();
		propertyNotInId.setName("arg3");
		propertyNotInId.setValue(notInIds);
		propertyNotInId.setType(String.class);

		try {
			init(method, propertyViId, propertyShipID, propertyUserLogin,
					propertyNotInId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String getUpdatedVIData(int tenantId, String userId, String userLogin) {
		String method = METHOD_GETUPDATEDVIDATA;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		PropertyInfo propertyUserID = new PropertyInfo();
		propertyUserID.setName("arg1");
		propertyUserID.setValue(userId);
		propertyUserID.setType(String.class);

		PropertyInfo propertyUserLogin = new PropertyInfo();
		propertyUserLogin.setName("arg2");
		propertyUserLogin.setValue(userLogin);
		propertyUserLogin.setType(String.class);

		try {
			init(method, propertyTenantId, propertyUserID, propertyUserLogin);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedVIDataByVersion(int tenantId, String userId,
			String userLogin, String version) {
		String method = METHOD_GETUPDATEDVIDATA_BY_VERSION;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		PropertyInfo propertyUserID = new PropertyInfo();
		propertyUserID.setName("arg1");
		propertyUserID.setValue(userId);
		propertyUserID.setType(String.class);

		PropertyInfo propertyUserLogin = new PropertyInfo();
		propertyUserLogin.setName("arg2");
		propertyUserLogin.setValue(userLogin);
		propertyUserLogin.setType(String.class);

		PropertyInfo propertyVersion = new PropertyInfo();
		propertyVersion.setName("arg3");
		propertyVersion.setValue(version);
		propertyVersion.setType(String.class);

		try {
			init(method, propertyTenantId, propertyUserID, propertyUserLogin,
					propertyVersion);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String ackUpdatedVIData(String data, String shipId) {
		String method = METHOD_ACKUPDATEDVIDATA;
		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(data);
		propertyXmlData.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyXmlData, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledFormAck(String ids, String shipId) {
		String method = METHOD_SEND_ACK_FILLED_FORM;
		PropertyInfo idsInfo = new PropertyInfo();
		idsInfo.setName("arg0");
		idsInfo.setValue(ids);
		idsInfo.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt(shipId));
		propertyShipID.setType(Integer.class);

		try {
			init(method, idsInfo, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	// public String sendFilledFormHeaderAck(String ids, String shipId) {
	public String sendFilledFormDescAck(String ids, String shipId) {
		String method = METHOD_SEND_ACK_FILLED_FORM_HEADER;
		PropertyInfo idsInfo = new PropertyInfo();
		idsInfo.setName("arg0");
		idsInfo.setValue(ids);
		idsInfo.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt(shipId));
		propertyShipID.setType(Integer.class);

		try {
			init(method, idsInfo, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledFormFooterAck(String ids, String shipId) {
		String method = METHOD_SEND_ACK_FILLED_FORM_FOOTER;
		PropertyInfo idsInfo = new PropertyInfo();
		idsInfo.setName("arg0");
		idsInfo.setValue(ids);
		idsInfo.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt(shipId));
		propertyShipID.setType(Integer.class);

		try {
			init(method, idsInfo, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 * @return
	 */
	public String getFormCategory(int tenantId) {
		String method = METHOD_GETFORMCATEGORY;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 * @return
	 */
	public String getRoleTemplateData(int tenantId) {
		String method = METHOD_GET_ROLE_TEMPLATE;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param tenantId
	 * @param roleId
	 * @return
	 */
	public String getRoleTemplateData(int tenantId, int roleId) {
		String method = METHOD_GET_ROLE_TEMPLATE_BY_ROLE;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		PropertyInfo propertyRoleId = new PropertyInfo();
		propertyRoleId.setName("arg1");
		propertyRoleId.setValue(roleId);
		propertyRoleId.setType(Integer.class);

		try {
			init(method, propertyTenantId, propertyRoleId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledFormImageAck(String ids) {
		String method = METHOD_SEND_ACK_FILLED_FORM_IMAGE;

		PropertyInfo propertyIdInfo = new PropertyInfo();
		propertyIdInfo.setName("arg0");
		propertyIdInfo.setValue(ids);
		propertyIdInfo.setType(String.class);

		try {
			init(method, propertyIdInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception ex) {
			Log.e(TAG, "" + ex.getMessage());
		}
		return response;

	}

	/**
	 * @author pushkar.m
	 * @param xmlData
	 * @return
	 */
	public String matchVISyncHistory(String xmlData) {
		String method = METHOD_MATCH_VISYNC_HISTORY;

		PropertyInfo propertyXmlDataInfo = new PropertyInfo();
		propertyXmlDataInfo.setName("arg0");
		propertyXmlDataInfo.setValue(xmlData);
		propertyXmlDataInfo.setType(String.class);

		try {
			init(method, propertyXmlDataInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception ex) {
			Log.e(TAG, "" + ex.getMessage());
		}
		return response;

	}

	public String getShipExistOrNot(int shipId) {
		String method = METHOD_GET_SHIP_EXIST;

		PropertyInfo propertyShipDataInfo = new PropertyInfo();
		propertyShipDataInfo.setName("arg0");
		propertyShipDataInfo.setValue(shipId);
		propertyShipDataInfo.setType(Integer.class);

		try {
			init(method, propertyShipDataInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception ex) {
			Log.e(TAG, "" + ex.getMessage());
		}
		return response;
	}

	/**
	 * 
	 * @param shipId
	 * @param macId
	 * @return xml as string
	 */
	public String registerDevice(String shipId, String macId) {

		String method = METHOD_REGISTERDEVICE;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt(shipId));
		Log.i("shipId inside webservice", shipId);
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String loginUser(String strUserId, String strPassword, String macId) {

		String method = METHOD_LOGIN_USER;

		propertyUserID = new PropertyInfo();
		propertyUserID.setName("arg0");
		propertyUserID.setValue(strUserId);
		propertyUserID.setType(String.class);

		propertyUserPassword = new PropertyInfo();
		propertyUserPassword.setName("arg1");
		propertyUserPassword.setValue(strPassword);
		propertyUserPassword.setType(String.class);

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg2");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		try {
			// init(method, propertyUserID, propertyUserPassword,
			// propertyMACID);

			init(method, propertyUserID, propertyUserPassword);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
			Log.e(TAG, "" + e.toString());
		}
		return response;
	}

	public String getAllUser(String shipId, String macId) {

		String method = METHOD_GETALLUSERS;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt(shipId));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;

	}

	public String getAllImage(String shipId, String macId) {

		String method = METHOD_GETALLIMAGE;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedAllImage(String shipId, String macId) {

		String method = METHOD_GETALLIMAGE;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getShipAttributes(String shipId, String macId) {

		String method = METHOD_GETSHIPATTRIBUTES;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getCrewListData(String shipId, String macId) {

		String method = METHOD_GETCREWLISTDATA;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedUser(String shipId, String macId) {

		Log.i("ship Id and deviceId for get updated user insdie web service  : ",
				"shipId=" + shipId + " device Id = " + macId);
		String method = METHOD_UPDATEALLUSERS;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;

	}

	public String sendVesselInspectionChecklist(String shipId, String macId,
			String xmlData) {

		String method = METHOD_SEND_VI_CHECKLIST;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(shipId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(macId);
		propertyShipID.setType(String.class);

		PropertyInfo pi = new PropertyInfo();
		pi.setName("arg2");
		pi.setValue(xmlData);
		pi.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID, pi);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendMarpoleInspectionForm(String shipId, String macId,
			String xmlData) {

		String method = METHOD_SEND_VI_FORMS;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(shipId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(macId);
		propertyShipID.setType(String.class);

		PropertyInfo pi = new PropertyInfo();
		pi.setName("arg2");
		pi.setValue(xmlData);
		pi.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID, pi);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendVesselInspectionForm(String shipId, String macId,
			String xmlData) {

		String method = METHOD_SEND_MI_FORMS;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(shipId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(macId);
		propertyShipID.setType(String.class);

		PropertyInfo pi = new PropertyInfo();
		pi.setName("arg2");
		pi.setValue(xmlData);
		pi.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID, pi);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendVesselInspectionAllData(String shipId, String macId,
			String xmlData) {

		String method = METHOD_GETALLVESSELINSPECTION_DATA;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(shipId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(macId);
		propertyShipID.setType(String.class);

		PropertyInfo pi = new PropertyInfo();
		pi.setName("arg2");
		pi.setValue(xmlData);
		pi.setType(String.class);

		try {
			init(method, propertyMACID, propertyShipID, pi);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendAckMiForm(String macId, String strContentId, int hasMore) {

		String method = METHOD_SENDACK_MI_FORM;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(strContentId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);

		try {
			if (hasMore != -1) {
				init(method, propertyMACID, propertyShipID, propertyHasMore);
			} else {
				init(method, propertyMACID, propertyShipID);
			}

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendAckCheckList(String macId, String strContentId,
			int hasMore) {

		String method = METHOD_SENDACK_CHECKLIST;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(strContentId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);

		try {
			if (hasMore != -1) {
				init(method, propertyMACID, propertyShipID, propertyHasMore);
			} else {
				init(method, propertyMACID, propertyShipID);
			}

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendAckViForm(String macId, String strContentId, int hasMore) {

		String method = METHOD_SENDACK_VI_FORM;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(strContentId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);

		try {
			if (hasMore != -1) {
				init(method, propertyMACID, propertyShipID, propertyHasMore);
			} else {
				init(method, propertyMACID, propertyShipID);
			}

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getCheckListSectionData(String macId, String shipId,
			int hasMore) {
		String method = METHOD_GET_CHECKLIST_SECTION;
		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg0");
		propertyShipID.setValue(Integer.parseInt(shipId));
		propertyShipID.setType(Integer.class);

		try {
			/*
			 * if (hasMore != -1) { init(method, propertyMACID, propertyShipID,
			 * propertyHasMore); } else { init(method, propertyMACID,
			 * propertyShipID); }
			 */
			init(method, propertyShipID);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();

		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getCheckListSectionItemData(String macId, String tenantId,
			int hasMore) {
		String method = METHOD_GET_CHECKLIST_SECTION_ITEM;

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt(tenantId));
		propertyTenantId.setType(Integer.class);
		try {
			/*
			 * if (hasMore != -1) { init(method, propertyMACID, propertyShipID,
			 * propertyHasMore); } else { init(method, propertyMACID,
			 * propertyShipID); }
			 */
			init(method, propertyTenantId);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getFormSectionData(String macId, String tenantId, int hasMore) {

		String method = METHOD_GET_FORM_SECTION;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setType(Integer.class);
		propertyTenantId.setValue(Integer.parseInt(tenantId));

		try {
			/*
			 * if (hasMore != -1) { init(method, propertyMACID, propertyShipID,
			 * propertyHasMore); } else { init(method, propertyMACID,
			 * propertyShipID); }
			 */

			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getFormSectionItemData(String macId, String tenantId,
			int hasMore) {

		String method = METHOD_GET_FORM_SECTION_ITEM;
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt(tenantId));
		propertyTenantId.setType(Integer.class);

		try {
			/*
			 * if (hasMore != -1) { init(method, propertyMACID, propertyShipID,
			 * propertyHasMore); } else { init(method, propertyMACID,
			 * propertyShipID); }
			 */

			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getShipFormSectionItemData(String macId, String tenantId,
			String shipId, int hasMore) {

		String method = METHOD_GET_SHIP_FORM_SECTION_ITEM;

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt(tenantId));
		propertyTenantId.setType(Integer.class);
		/*
		 * propertyShipID = new PropertyInfo(); propertyShicpID.setName("arg1");
		 * propertyShipID.setValue(Integer.parseInt(shipId));
		 * propertyShipID.setType(Integer.class);
		 */
		try {

			// init(method, propertyTenantId, propertyShipID);
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	// public String sendFilledFormHeader(String xmlData){
	public String sendFilledFormDesc(String xmlData) {
		// String method = METHOD_SEND_FILLED_FORM_HEADER;
		String method = METHOD_SEND_FILLED_FORM_DESC;
		PropertyInfo propertyData = new PropertyInfo();
		propertyData.setName("arg0");
		propertyData.setValue(xmlData);
		propertyData.setType(String.class);

		try {

			init(method, propertyData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledFormFooter(String xmlData) {
		String method = METHOD_SEND_FILLED_FORM_FOOTER;
		PropertyInfo propertyData = new PropertyInfo();
		propertyData.setName("arg0");
		propertyData.setValue(xmlData);
		propertyData.setType(String.class);

		try {

			init(method, propertyData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedFilledFormDesc(String viIds, int shipId,
			int loginUser, String notInDescIds) {
		String method = METHOD_GET_UPDATED_FILLED_FORM_DESC;

		PropertyInfo propertyVIId = new PropertyInfo();
		propertyVIId.setName("arg0");
		propertyVIId.setValue(viIds);
		propertyVIId.setType(String.class);

		PropertyInfo propertyShipId = new PropertyInfo();
		propertyShipId.setName("arg1");
		propertyShipId.setValue(shipId);
		propertyShipId.setType(Integer.class);

		PropertyInfo propertyLoginUser = new PropertyInfo();
		propertyLoginUser.setName("arg2");
		propertyLoginUser.setValue(loginUser);
		propertyLoginUser.setType(Integer.class);

		PropertyInfo propertyNotInId = new PropertyInfo();
		propertyNotInId.setName("arg3");
		propertyNotInId.setValue(notInDescIds);
		propertyNotInId.setType(String.class);

		try {

			init(method, propertyVIId, propertyShipId, propertyLoginUser,
					propertyNotInId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedFilledFormHeader(String viIds, int shipId,
			int loginUser) {
		String method = METHOD_GET_UPDATED_FILLED_FORM_HEADER;
		PropertyInfo propertyVIId = new PropertyInfo();
		propertyVIId.setName("arg0");
		propertyVIId.setValue(viIds);
		propertyVIId.setType(String.class);

		PropertyInfo propertyShipId = new PropertyInfo();
		propertyShipId.setName("arg1");
		propertyShipId.setValue(shipId);
		propertyShipId.setType(Integer.class);

		PropertyInfo propertyLoginUser = new PropertyInfo();
		propertyLoginUser.setName("arg2");
		propertyLoginUser.setValue(loginUser);
		propertyLoginUser.setType(Integer.class);

		try {

			init(method, propertyVIId, propertyShipId, propertyLoginUser);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUpdatedFilledFormFooter(String viIds, int shipId,
			int loginUser) {
		String method = METHOD_GET_UPDATED_FILLED_FORM_FOOTER;
		PropertyInfo propertyVIId = new PropertyInfo();
		propertyVIId.setName("arg0");
		propertyVIId.setValue(viIds);
		propertyVIId.setType(String.class);

		PropertyInfo propertyShipId = new PropertyInfo();
		propertyShipId.setName("arg1");
		propertyShipId.setValue(shipId);
		propertyShipId.setType(Integer.class);

		PropertyInfo propertyLoginUser = new PropertyInfo();
		propertyLoginUser.setName("arg2");
		propertyLoginUser.setValue(loginUser);
		propertyLoginUser.setType(Integer.class);

		try {

			init(method, propertyVIId, propertyShipId, propertyLoginUser);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getWeather(String xmlInput) throws MalformedURLException,
			IOException {

		// Code to make a webservice HTTP request
		String responseString = "";
		String outputString = "";
		String wsURL = "http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl";
		// Uri url = Uri.parse(wsURL);
		java.net.URL url = new java.net.URL(wsURL);
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConn = (HttpURLConnection) connection;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		// String xmlInput =
		// "  <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://litwinconsulting.com/webservices/\">\n"
		// +
		// "   <soapenv:Header/>\n" +
		// "   <soapenv:Body>\n" +
		// "      <web:GetWeather>\n" +
		// "         <!--Optional:-->\n" +
		// "         <web:City>" + city + "</web:City>\n" +
		// "      </web:GetWeather>\n" +
		// "   </soapenv:Body>\n" +
		// "  </soapenv:Envelope>";

		byte[] buffer = new byte[xmlInput.length()];
		buffer = xmlInput.getBytes();
		bout.write(buffer);
		byte[] b = bout.toByteArray();
		String SOAPAction = "http://service.smsWS.session.ark.com/ackContent";
		// Set the appropriate HTTP parameters.
		httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
		httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		httpConn.setRequestProperty("SOAPAction", SOAPAction);
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		OutputStream out = httpConn.getOutputStream();
		// Write the content of the request to the outputstream of the HTTP
		// Connection.
		out.write(b);
		out.close();
		// Ready with sending the request.

		// Read the response.
		InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
		BufferedReader in = new BufferedReader(isr);

		// Write the SOAP message response to a String.
		while ((responseString = in.readLine()) != null) {
			outputString = outputString + responseString;
		}
		// Parse the String output to a org.w3c.dom.Document and be able to
		// reach every node with the org.w3c.dom API.

		return outputString;
	}

	public String CallWebService(String url, String soapAction, String envelope) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		// request parameters

		// HttpParams params = httpClient.getParams();
		// HttpConnectionParams.setConnectionTimeout(params, 20000);
		// HttpConnectionParams.setSoTimeout(params, 25000);
		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		// POST the envelope
		HttpPost httppost = new HttpPost(
				"http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl");
		// add headers
		httppost.setHeader("soapaction",
				"http://service.smsWS.session.ark.com/ackContent");
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
		Log.i("Alok", "Alok=====");
		String responseString = "";
		try {

			// the entity holds the request
			HttpEntity entity = new StringEntity(envelope);
			httppost.setEntity(entity);

			// Response handler

			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response

				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {

					// get response entity
					HttpEntity entity = response.getEntity();

					// read the response as byte array
					StringBuffer out = new StringBuffer();
					byte[] b = EntityUtils.toByteArray(entity);

					// write the response byte array to a string buffer
					out.append(new String(b, 0, b.length));

					return out.toString();
				}
			};

			responseString = httpClient.execute(httppost, rh);

		} catch (Exception e) {
			Log.v("exception", e.toString());
		}

		String xml = responseString.toString();
		// close the connection
		System.out.println("xml file ------" + xml);
		httpClient.getConnectionManager().shutdown();
		return responseString;
	}

	public String sendAckContent(String macId, String strContentId, int hasMore) {

		String method = METHOD_SENDACKCONTENT;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(strContentId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);

		try {
			if (hasMore != -1) {
				init(method, propertyMACID, propertyShipID, propertyHasMore);
			} else {
				init(method, propertyMACID, propertyShipID);
			}

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String unregisterDevice(String macId, String shipId) {

		String method = METHOD_UNREGISTERDEVICE;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);
		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);
		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendVesselInspectionData(String macId, String shipId,
			String xmlData) {

		String method = METHOD_SENDVESSELINSPECTIONDATA;
		PropertyInfo propertyInfo = new PropertyInfo();
		propertyInfo.setName("arg0");
		propertyInfo.setValue(xmlData);
		propertyInfo.setType(String.class);

		try {
			init(method, propertyInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledCheckListData(String macId, String shipId,
			String xmlData) {

		String method = METHOD_SENDFILLEDCHECKLIST;
		PropertyInfo propertyInfo = new PropertyInfo();
		propertyInfo.setName("arg0");
		propertyInfo.setValue(xmlData);
		propertyInfo.setType(String.class);

		try {
			init(method, propertyInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendFilledFormData(String macId, String shipId, String xmlData) {

		String method = METHOD_SENDFILLEDFORMS;
		PropertyInfo propertyInfo = new PropertyInfo();
		propertyInfo.setName("arg0");
		propertyInfo.setValue(xmlData);
		propertyInfo.setType(String.class);

		try {
			init(method, propertyInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getFilledFormData(String macId, String shipId, int hasMore) {

		String method = METHOD_GETFILLEDFORMS;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);
		try {
			init(method, propertyMACID, propertyShipID, propertyHasMore);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author Ripunjay
	 * @param macId
	 * @param shipId
	 * @param xmlData
	 * @return
	 * @see This method send form images which capture by phone or tablate on
	 *      web.
	 */
	public String sendFormImagesData(String macId, String shipId, String xmlData) {

		String method = METHOD_SEND_FORMIMAGES;
		PropertyInfo propertyInfo = new PropertyInfo();
		propertyInfo.setName("arg0");
		propertyInfo.setValue(xmlData);
		propertyInfo.setType(String.class);

		try {
			init(method, propertyInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author Ripunjay
	 * @param macId
	 * @param filledFormId
	 * @param hasMore
	 * @return
	 * @see This method send acknoledgement of receive form images from web.
	 */
	public String sendAckFormImages(String macId, String filledFormId,
			int hasMore) {

		String method = METHOD_SENDACK_FORMIMAGES;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(filledFormId);
		propertyShipID.setType(String.class);

		propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg2");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);

		try {
			if (hasMore != -1) {
				init(method, propertyMACID, propertyShipID, propertyHasMore);
			} else {
				init(method, propertyMACID, propertyShipID);
			}
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getVITemplateVersion(int shipId, String macId, int tenantId) {

		String method = METHOD_GET_VI_TEMPALTE_VERSIOV;

		PropertyInfo propertyShipId = new PropertyInfo();
		propertyShipId.setName("arg0");
		propertyShipId.setValue(shipId);
		propertyShipId.setType(Integer.class);

		PropertyInfo propertyMacId = new PropertyInfo();
		propertyMacId.setName("arg1");
		propertyMacId.setValue(macId);
		propertyMacId.setType(String.class);

		PropertyInfo propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg2");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);

		try {

			init(method, propertyShipId, propertyMacId, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendDeviceSyncTime(String xmlData) {

		String method = METHOD_SEND_DEVICE_SYNC_TIME;
		PropertyInfo propertyData = new PropertyInfo();
		propertyData.setName("arg0");
		propertyData.setValue(xmlData);
		propertyData.setType(String.class);

		try {

			init(method, propertyData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}
}
