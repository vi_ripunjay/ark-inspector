package com.tams.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.kobjects.base64.Base64;
import org.xml.sax.SAXException;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tams.fragment.AdminFragment;
import com.tams.fragment.AdminTab;
import com.tams.fragment.VesselInspectionStartFragment;
import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.FilledCheckList;
import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.model.FormCategory;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.model.RoleTemplate;
import com.tams.model.ShipFormSectionItem;
import com.tams.model.ShipMaster;
import com.tams.model.SyncHistory;
import com.tams.model.SyncStatus;
import com.tams.model.VITemplateVersion;
import com.tams.model.VesselInspection;
import com.tams.model.VesselInspectionFormListDesc;
import com.tams.model.VesselInspectionFormListFooter;
import com.tams.parser.CheckListSectionItemsParser;
import com.tams.parser.CheckListSectionParser;
import com.tams.parser.FilledCheckListParsar;
import com.tams.parser.FilledFormImageparser;
import com.tams.parser.FilledFormParsar;
import com.tams.parser.FormCategoryParser;
import com.tams.parser.FormSectionItemParsar;
import com.tams.parser.FormSectionParsar;
import com.tams.parser.RoleTemplateParser;
import com.tams.parser.ShipFormSectionItemParser;
import com.tams.parser.ShipMasterParsar;
import com.tams.parser.VISyncHistoryParsar;
import com.tams.parser.VITemplateVersionParser;
import com.tams.parser.VesselInspectionFormListDescParser;
import com.tams.parser.VesselInspectionFormListFooterParser;
import com.tams.parser.VesselInspectionParsar;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;
import com.tams.vessalinspection.LoginActivity;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.SyncHandler;
import com.tams.vessalinspection.VessalInspectionHome;
import com.tams.vessalinspection.VesselInspectionStartActivity;

/**
 * @file BackGroundTask . java
 * 
 * @brief class is used to run the task in the backGround
 * 
 * @author Ripunjay Shukla
 * 
 * @date : Jun 21,2015
 */
public class BackGroundTask {

	public static Context context;
	private static ProgressDialog dialog = null;
	public static List<String> viFormList = null;
	public static List<String> miFormList = null;
	public static List<String> checkList = null;
	public static List<String> imageList = null;
	public static List<String> userList = null;

	public static boolean checkSendVesselInspection = false;
	public static boolean checkSendFilledCheckList = false;
	public static boolean registerDeviceStatus = false;
	public static boolean checkSendFilledForm = false;
	public static boolean checkGetCheckListSection = false;
	public static boolean checkGetCheckListSectionItem = false;
	public static boolean checkGetFormSection = false;
	public static boolean checkGetFormSectionItem = false;
	public static boolean checkGetShipFormSectionItem = false;

	public static boolean checkGetShipMaster = false;
	public static boolean checkGetFormCategory = false;
	public static boolean checkGetUpdatedViData = false;
	public static boolean checkGetUpdatedFilledFormData = false;
	public static boolean checkGetUpdatedFilledFormCheckListData = false;
	public static boolean checkGetRoleTemplateData = false;
	public static boolean checkGetShipExist = false;
	public static boolean checkGetFilledFormFooter = false;
	public static boolean checkGetFilledFormHeader = false;

	public static boolean checkUserUpdate = false;
	public static boolean checkImageUpdate = false;
	public static boolean checkFormImageUpdate = false;
	public static boolean checkUpdateManualSyncHistory = false;
	public static boolean checkFormImageUpdateByWeb = false;
	public static boolean checkUpdateFilledFormHeader = false;
	public static boolean checkUpdateFilledFormFooter = false;

	public static boolean checkGetVITemplateVerison = false;

	public static boolean IS_UPDATE_USER_SUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_SUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_UNSUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_RESPONSE_DATA = false;
	public static boolean IS_GET_IMAGES_RESPONSE_DATA = false;
	public static boolean IS_UPDATE_CONTENT_SUCCESS = false;

	public static boolean IS_SEND_FORM_IMAGES_RESPONSE_DATA = false;
	public static boolean IS_SEND_FORM_IMAGES_SUCCESS = false;
	public static boolean IS_SEND_FORM_IMAGES_UNSUCCESS = false;

	public static boolean IS_GET_UPDATED_FORM_IMAGE_RESPONSE_DATA = false;
	public static boolean IS_GET_UPDATED_FORM_IMAGE_SUCCESS = false;
	public static boolean IS_GET_UPDATED_FORM_IMAGE_UNSUCCESS = false;

	public static boolean IS_GET_UPDATED_FILLED_FORM_RESPONSE_DATA = false;
	public static boolean IS_GET_UPDATED_FILLED_FORM_SUCCESS = false;
	public static boolean IS_GET_UPDATED_FILLED_FORM_UNSUCCESS = false;

	public static boolean IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_RESPONSE_DATA = false;
	public static boolean IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_SUCCESS = false;
	public static boolean IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_UNSUCCESS = false;

	public static boolean IS_GET_FORM_IMAGES_RESPONSE_DATA = false;
	public static boolean IS_GET_FORM_IMAGES_SUCCESS = false;
	public static boolean IS_GET_FORM_IMAGES_UNSUCCESS = false;

	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_RESPONSE_DATA = false;
	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_SUCCESS = false;
	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_UNSUCCESS = false;

	public static boolean IS_GET_CHECKLIST_SECTION_SUCCESS = false;
	public static boolean IS_GET_CHECKLIST_SECTION_UNSUCCESS = false;
	public static boolean IS_GET_CHECKLIST_SECTION_RESPONSE_DATA = false;

	public static boolean IS_GET_CHECKLIST_SECTION_ITEM_SUCCESS = false;
	public static boolean IS_GET_CHECKLIST_SECTION_ITEM_UNSUCCESS = false;
	public static boolean IS_GET_CHECKLIST_SECTION_ITEM_RESPONSE_DATA = false;

	public static boolean IS_GET_SHIP_FORM_SECTION_ITEM_SUCCESS = false;
	public static boolean IS_GET_SHIP_FORM_SECTION_ITEM_UNSUCCESS = false;
	public static boolean IS_GET_SHIP_FORM_SECTION_ITEM_RESPONSE_DATA = false;

	public static boolean IS_GET_FORM_SECTION_SUCCESS = false;
	public static boolean IS_GET_FORM_SECTION_UNSUCCESS = false;
	public static boolean IS_GET_FORM_SECTION_RESPONSE_DATA = false;

	public static boolean IS_GET_FORM_SECTION_ITEM_SUCCESS = false;
	public static boolean IS_GET_FORM_SECTION_ITEM_UNSUCCESS = false;
	public static boolean IS_GET_FORM_SECTION_ITEM_RESPONSE_DATA = false;

	public static boolean IS_DATA_PRESENT_IN_DATABASE = false;

	public static boolean IS_GET_ALL_SHIPS_SUCCESS = false;
	public static boolean IS_GET_ALL_SHIPS_UNSUCCESS = false;
	public static boolean IS_GET_ALL_SHIPS_RESPONSE_DATA = false;

	public static boolean IS_GET_FORM_CATEGORY_SUCCESS = false;
	public static boolean IS_GET_FORM_CATEGORY_UNSUCCESS = false;
	public static boolean IS_GET_FORM_CATEGORY_RESPONSE_DATA = false;

	public static boolean IS_UPDATE_VESSEL_INSPECTION_SUCCESS = false;
	public static boolean IS_UPDATE_VESSEL_INSPECTION_UNSUCCESS = false;
	public static boolean IS_UPDATE_VESSEL_INSPECTION_RESPONSE_DATA = false;

	public static boolean IS_GET_SHIP_EXIST_SUCCESS = false;
	public static boolean IS_GET_SHIP_EXIST_UNSUCCESS = false;
	public static boolean IS_GET_SHIP_EXIST_RESPONSE_DATA = false;

	public static boolean IS_UPDATE_FILLED_FORM_HEADER_SUCCESS = false;
	public static boolean IS_UPDATE_FILLED_FORM_HEADER_UNSUCCESS = false;
	public static boolean IS_UPDATE_FILLED_FORM_HEADER_RESPONSE_DATA = false;

	public static boolean IS_UPDATE_FILLED_FORM_FOOTER_SUCCESS = false;
	public static boolean IS_UPDATE_FILLED_FORM_FOOTER_UNSUCCESS = false;
	public static boolean IS_UPDATE_FILLED_FORM_FOOTER_RESPONSE_DATA = false;

	String toastMsg = "";
	public StringBuffer formBuff = new StringBuffer();
	public int no = 1;

	LoginActivity loginActivity;
	VessalInspectionHome vesselInspectionHome;

	boolean syncProcessEnd = false;

	/**
	 * This method call at the time of backGroundTask object initialization and
	 * create required object
	 * 
	 * @param context
	 */
	public BackGroundTask(Context context) {
		super();
		BackGroundTask.context = context;

		viFormList = new ArrayList<String>();
		miFormList = new ArrayList<String>();
		checkList = new ArrayList<String>();
		imageList = new ArrayList<String>();
		userList = new ArrayList<String>();
		Log.i("background task", "inside constructor");
		// toastMsg = "";
	}

	public void getAllImage(String shipId, String deviceId) {

		GetAllImageTask task = new GetAllImageTask();
		task.execute(shipId, deviceId);
	}

	public void getUpdateImage(String shipId, String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUpdateImageTask task = new GetUpdateImageTask();
			task.execute(shipId, deviceId);
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}
	}

	/**
	 * This method will get updated user data from web service in the background
	 * task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getUpdatedUser(String shipId, String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {

			if (checkUserUpdate) {
				Log.i("inside Updated user", "test" + checkUserUpdate
						+ deviceId);
				// GetUpdatedUserTask task = new GetUpdatedUserTask();
				checkUserUpdate = !checkUserUpdate;
				Log.i("inside Updated user", "test" + checkUserUpdate);
				Log.i("ship Id and deviceId for get updated user : ", "shipId="
						+ CommonUtil.getShipId(context) + " device Id = "
						+ CommonUtil.getIMEI(context));
				// task.execute(shipId,deviceId);

			}
		} else {
			// serviceProcessEnd();
		}
	}

	public void getShipExistOrNot(String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {

			if (checkGetShipExist) {
				GetShipExistOrNotTask task = null;
				if (shipId != null && !"".equals(shipId)) {
					task = new GetShipExistOrNotTask(
							Integer.parseInt(shipId),
							CommonUtil.getCallingThrough(context) != null ? CommonUtil
									.getCallingThrough(context) : "");
				} else {
					task = new GetShipExistOrNotTask(
							0,
							CommonUtil.getCallingThrough(context) != null ? CommonUtil
									.getCallingThrough(context) : "");
				}
				checkGetShipExist = !checkGetShipExist;
				Log.i("inside getshipExistOrNot", "test" + checkGetShipExist);
				Log.i("ship Id for getshipExistOrNot : ", "shipId=" + shipId);
				task.execute();

			}
		} else {
			// serviceProcessEnd();
		}
	}

	/**
	 * This method will send vessel inspection data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void sendVesselInspection(String deviceId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkSendVesselInspection) {
				SendVesselInspectionDataTask dataTask = null;
				try {
					Log.i("inside send VesselInspection", "test"
							+ checkSendVesselInspection);
					dataTask = new SendVesselInspectionDataTask();
					dataTask.execute(deviceId, shipId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);

					checkSendVesselInspection = !checkSendVesselInspection;
					Log.i("inside send Filled Form", "test"
							+ checkSendVesselInspection);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);
					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * @author pushkar.m This method closes progress bar and open desire
	 *         page(home or admin)
	 */
	public static void serviceProcessEnd() {
		LoginActivity loginActivity;
		VessalInspectionHome vesselInspectionHome;
		if (AdminFragment.homeOrStart == null
				|| "".equals(AdminFragment.homeOrStart)) {
			if (VessalInspectionHome.sendingStatus == 2) {

				Intent i = new Intent(context,
						VesselInspectionStartActivity.class);
				i.putExtra("strVesselInspectionId",
						CommonUtil.getVesselInspectionId(context));
				context.startActivity(i);
				if (context instanceof VessalInspectionHome) {
					((VessalInspectionHome) context).finish();
				}
				if (context instanceof VesselInspectionStartActivity) {
					((VesselInspectionStartActivity) context).finish();
				}
			}
			if (VessalInspectionHome.miActionProgressItem != null) {
				VessalInspectionHome.miActionProgressItem.setVisible(false);
				VessalInspectionHome.miActionCancelItem.setVisible(true);
				VessalInspectionHome.miActionDraftItem.setVisible(true);
				VessalInspectionHome.miActionDoneItem.setVisible(false);
			}

		} else {

			if ("home".equals(AdminFragment.homeOrStart)) {

				if (VessalInspectionHome.miActionProgressItem != null) {
					VessalInspectionHome.miActionProgressItem.setVisible(false);
				}
			}
			if ("start".equals(AdminFragment.homeOrStart)) {

				if (VesselInspectionStartActivity.miActionProgressItem != null)
					VesselInspectionStartActivity.miActionProgressItem
							.setVisible(false);
				// for resfreshing start page if service calling through
				// Activate inspection
				if (VesselInspectionStartFragment.lastActiveInspection != null
						&& !"".equals(VesselInspectionStartFragment.lastActiveInspection)) {

					Intent i = new Intent(context,
							VesselInspectionStartActivity.class);
					context.startActivity(i);
					if (context instanceof VesselInspectionStartActivity) {
						((VesselInspectionStartActivity) context).finish();
					}
				}

			}
		}

		if (IS_UPDATE_VESSEL_INSPECTION_SUCCESS) {
			if (context instanceof LoginActivity) {
				loginActivity = (LoginActivity) context;
				loginActivity.showProgress(false);
				Intent i = new Intent(context,
						VesselInspectionStartActivity.class);
				context.startActivity(i);
				((LoginActivity) context).finish();
			}
		} else {
			if (AdminFragment.homeOrStart == null
					|| "".equals(AdminFragment.homeOrStart)) {
				Intent i = new Intent(context, LoginActivity.class);
				context.startActivity(i);

				if (context instanceof VesselInspectionStartActivity) {
					((VesselInspectionStartActivity) context).finish();
				}
				if (context instanceof VessalInspectionHome) {
					((VessalInspectionHome) context).finish();
				}
			}
		}

		CommonUtil.setEditedData(context, false);

		CommonUtil.deleteAllRelatedDataOfInactiveInspection(context);

	}

	/**
	 * @author ripunjay.s service end in case of review status and goes on home
	 *         page.
	 */
	public static void serviceProcessEndInReviewStatus() {

		if (VessalInspectionHome.miActionProgressItem != null) {
			VessalInspectionHome.miActionProgressItem.setVisible(false);

			Intent i = new Intent(context, VesselInspectionStartActivity.class);
			context.startActivity(i);
			if (context instanceof VessalInspectionHome) {
				((VessalInspectionHome) context).finish();
			}
		}

		CommonUtil.deleteAllRelatedDataOfInactiveInspection(context);
	}

	/**
	 * This method will send filled form data from web service in the background
	 * task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void sendFilledForms(String deviceId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendFillFormDataTask dataTask = null;
			if (checkSendFilledForm) {
				Log.i("inside send Filled Form", "test" + checkSendFilledForm);
				try {
					dataTask = new SendFillFormDataTask();
					dataTask.execute(deviceId, shipId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendFilledForm = !checkSendFilledForm;
					Log.i("inside send Filled Form", "test"
							+ checkSendFilledForm);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * This method will send filled checklist data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void sendFilledCheckList(String deviceId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkSendFilledCheckList) {
				Log.i("inside send FilledCheckList", "test"
						+ checkSendFilledCheckList);
				SendFillCheckListDataTask dataTask = null;
				try {
					dataTask = new SendFillCheckListDataTask();
					dataTask.execute(deviceId, shipId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendFilledCheckList = !checkSendFilledCheckList;
					Log.i("inside send Filled Form", "test"
							+ checkSendFilledCheckList);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	// ================Form Images service call and data update
	/**
	 * This method will send filled form template data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void sendFormImages(String shipId, String deviceId) {

		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkFormImageUpdate) {
				Log.i("inside send Form Images", "test" + checkFormImageUpdate);
				SendFormImagesDataTask dataTask = null;
				try {
					dataTask = new SendFormImagesDataTask();
					dataTask.execute(deviceId, shipId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkFormImageUpdate = !checkFormImageUpdate;
					Log.i("inside send Form Images", "test"
							+ checkFormImageUpdate);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * This method will get first time form images data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 * @author ripunjay
	 */
	public void getFormImages(String deviceId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetFormImagesDataTask dataTask = new GetFormImagesDataTask();
			dataTask.execute(deviceId, shipId);
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}
	}

	/**
	 * This method will get checklist section data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getCheckListSection(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetCheckListSection) {

				// GetCheckListSectionDataTask dataTask = new
				// GetCheckListSectionDataTask();

				GetCheckListSectionDataTask dataTask = new GetCheckListSectionDataTask(
						deviceId, tenantId, "0");
				dataTask.execute(deviceId, tenantId);
				checkGetCheckListSection = !checkGetCheckListSection;
				Log.i("inside get Checklist Section", "test"
						+ checkGetCheckListSection);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * This method will get checklist section item data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getCheckListSectionItem(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetCheckListSectionItem) {

				GetCheckListSectionItemDataTask dataTask = new GetCheckListSectionItemDataTask(
						deviceId, tenantId, "0");
				dataTask.execute(deviceId, tenantId);
				checkGetCheckListSectionItem = !checkGetCheckListSectionItem;
				Log.i("inside get Checklist Section", "test"
						+ checkGetCheckListSectionItem);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * This method will get FORM section data from web service in the background
	 * task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getFormSection(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetFormSection) {

				GetFormSectionDataTask dataTask = new GetFormSectionDataTask(
						deviceId, tenantId, "0");
				dataTask.execute(deviceId, tenantId);
				checkGetFormSection = !checkGetFormSection;
				Log.i("inside get form Section", "test" + checkGetFormSection);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * This method will get FORM section item data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getFormSectionItem(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetFormSectionItem) {

				GetFormSectionItemDataTask dataTask = new GetFormSectionItemDataTask(
						deviceId, tenantId, "0");
				dataTask.execute(deviceId, tenantId);
				checkGetFormSectionItem = !checkGetFormSectionItem;
				Log.i("inside get form Section", "test"
						+ checkGetFormSectionItem);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * This method will get Ship FORM section item data from web service in the
	 * background task
	 * 
	 * @param shipId
	 * @param deviceId
	 * @param tenantId
	 */
	public void getShipFormSectionItem(String deviceId, String tenantId,
			String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetShipFormSectionItem) {

				GetShipFormSectionItemDataTask dataTask = new GetShipFormSectionItemDataTask(
						deviceId, tenantId, shipId);
				dataTask.execute(deviceId, tenantId);
				checkGetShipFormSectionItem = !checkGetShipFormSectionItem;
				Log.i("inside get form Section", "test"
						+ checkGetShipFormSectionItem);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * @author pushkar.m This method will get all ships within tenant from web
	 *         service in the background task
	 * @param tenantId
	 */
	public void getAllShips(String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetShipMaster) {

				GetAllShipTask dataTask = new GetAllShipTask();
				dataTask.execute(tenantId);
				checkGetShipMaster = !checkGetShipMaster;
				Log.i("inside get form Section", "test" + checkGetShipMaster);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * @author pushkar.m This method will get all ships within tenant from web
	 *         service in the background task
	 * @param tenantId
	 */
	public void getUpdatedVIData(String userId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetUpdatedViData) {

				GetUpdatedVIDataTask dataTask = new GetUpdatedVIDataTask("",
						CommonUtil.getTenantId(context).toString(), userId);
				dataTask.execute(userId);
				checkGetUpdatedViData = !checkGetUpdatedViData;
				Log.i("inside get form Section", "test" + checkGetUpdatedViData);

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	private String getDraftedVIId() {

		StringBuffer viId = new StringBuffer("");
		if (CommonUtil.getActiveInspectionID(context) != null
				&& !"".equals(CommonUtil.getActiveInspectionID(context))) {
			return "'" + CommonUtil.getActiveInspectionID(context) + "'";
		} else {

			DBManager db = new DBManager(context);
			db.open();
			List<VesselInspection> viDraftList = new ArrayList<VesselInspection>();

			viDraftList = db.getVesselInspectionDataByflgStatusAndShipId(0,
					CommonUtil.getShipId(context));
			db.close();
			if (viDraftList != null && viDraftList.size() > 0) {

				for (VesselInspection vi : viDraftList) {
					viId.append("'" + vi.getiVesselInspectionId() + "',");
				}
				return viId.substring(0, viId.length() - 1);
			} else {
				return viId.toString();
			}
		}
	}

	public void getUpdatedFilledForm(String viId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);

		if (isReady) {
			if (checkGetUpdatedFilledFormData) {

				GetUpdatedFilledFormTask dataTask = new GetUpdatedFilledFormTask(
						"", getDraftedVIId(), shipId);
				dataTask.execute(viId);
				checkGetUpdatedFilledFormData = !checkGetUpdatedFilledFormData;
				Log.i("inside get form Section", "test"
						+ checkGetUpdatedFilledFormData);

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * This method will get form image data from web service in the background
	 * task
	 * 
	 * @param shipId
	 * @param deviceId
	 */
	public void getUpdatedFormImages(String deviceId, String shipId) {
		if (checkFormImageUpdateByWeb) {
			GetUpdatedFormImageDataTask dataTask = new GetUpdatedFormImageDataTask(
					"", getDraftedVIId(), shipId);
			dataTask.execute(deviceId, shipId);
			checkFormImageUpdateByWeb = !checkFormImageUpdateByWeb;

		}

	}

	public void getUpdatedFilledFormCheckList(String viId, String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetUpdatedFilledFormCheckListData) {

				GetUpdatedFilledFormCheckListTask dataTask = new GetUpdatedFilledFormCheckListTask(
						"", getDraftedVIId(), shipId);
				dataTask.execute(viId);
				checkGetUpdatedFilledFormCheckListData = !checkGetUpdatedFilledFormCheckListData;
				Log.i("inside get form Section", "test"
						+ checkGetUpdatedFilledFormCheckListData);

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	public void getRoleTemplateData(String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetRoleTemplateData) {
				int roleId = CommonUtil.getRoleId(context) != null
						&& !"".equals(CommonUtil.getRoleId(context)) ? Integer
						.parseInt(CommonUtil.getRoleId(context)) : 0;
				GetRoleTemplateDataTask dataTask = new GetRoleTemplateDataTask(
						Integer.parseInt(tenantId), roleId);
				dataTask.execute();
				checkGetRoleTemplateData = !checkGetRoleTemplateData;
				Log.i("inside get form Section", "test"
						+ checkGetUpdatedFilledFormCheckListData);

			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	public boolean matchManualSyncHistory(String shipId, String macId) {
		boolean result = false;

		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkUpdateManualSyncHistory) {
				MatchManualSyncHistoryTask dataTask = null;
				try {
					Log.i("inside match SyncHistory", "test"
							+ checkUpdateManualSyncHistory);
					dataTask = new MatchManualSyncHistoryTask();

					String syncFrom = CommonUtil.getSynckFrom(context);

					if (syncFrom != null
							&& !"".equals(CommonUtil.getSynckFrom(context))
							&& "process".equalsIgnoreCase(syncFrom)) {

						dataTask.execute(shipId, macId);
						result = dataTask.toBeSync;
						dataTask.get(Integer.parseInt(context.getResources()
								.getString(R.string.serviceCallTime)),
								TimeUnit.SECONDS);

						checkUpdateManualSyncHistory = !checkUpdateManualSyncHistory;
						Log.i("inside match synchistory", "test"
								+ checkUpdateManualSyncHistory);
					}
					if (syncFrom != null && !"".equals(syncFrom)
							&& "admin".equalsIgnoreCase(syncFrom)) {
						if (CommonUtil.getUserLogin(context) != null
								&& "1".equals(CommonUtil.getUserLogin(context))) {
							dataTask.execute(shipId, macId);
						} else {
							SyncHandler.context = context;
							Message shipMessage = new Message();
							shipMessage.what = SyncHandler.MSG_UPDATE_SHIP_MASETR;
							SyncHandler.handler.sendMessage(shipMessage);
						}
					}

				} catch (TimeoutException ex) {
					dataTask.cancel(true);
					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

		return result;
	}

	/**
	 * @author pushkar.m This method will get all ships within tenant from web
	 *         service in the background task
	 * @param tenantId
	 */
	public void getFormCategory(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetFormCategory) {

				GetFormCategoryTask dataTask = new GetFormCategoryTask(
						deviceId, tenantId, "0");
				dataTask.execute(tenantId);
				checkGetFormCategory = !checkGetFormCategory;
				Log.i("inside get form Section", "test" + checkGetFormCategory);

			}
		} else {
			// serviceProcessEnd();
		}

	}

	/**
	 * @author pushkar.m This method will send filledFormHeader to server
	 */
	public void sendFilledFormDesc() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkUpdateFilledFormHeader) {
				SendFilledFormDescTask dataTask = null;
				try {
					dataTask = new SendFilledFormDescTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkUpdateFilledFormHeader = !checkUpdateFilledFormHeader;
					Log.i("inside get form Section", "test"
							+ checkGetFormCategory);
				} catch (TimeoutException te) {
					dataTask.cancel(true);
					// serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		} else {
			// serviceProcessEnd();
		}
	}

	/**
	 * @author pushkar.m This method will send filledFormHeader to server
	 */
	public void sendFilledFormFooter() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkUpdateFilledFormFooter) {

				// nothing here
			}
		} else {
			// serviceProcessEnd();
		}
	}

	/**
	 * @author pushkar.m This method will send filledFormHeader to server
	 */
	public void getFilledFormDesc(String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetFilledFormHeader) {

				GetUpdatedFilledFormDescTask dataTask = new GetUpdatedFilledFormDescTask(
						"", getDraftedVIId(), shipId);
				dataTask.execute();
				checkGetFilledFormHeader = !checkGetFilledFormHeader;
				Log.i("inside get form Section", "test"
						+ checkGetFilledFormHeader);

			}
		} else {
			// serviceProcessEnd();
		}
	}

	/**
	 * @author pushkar.m This method will send filledFormHeader to server
	 */
	public void getFilledFormFooter(String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetFilledFormFooter) {

				GetUpdatedFilledFormFooterTask dataTask = new GetUpdatedFilledFormFooterTask(
						"", getDraftedVIId(), shipId);
				dataTask.execute();
				checkGetFilledFormFooter = !checkGetFilledFormFooter;
				Log.i("inside get form Section", "test"
						+ checkGetFilledFormFooter);

			}
		} else {
			serviceProcessEnd();
		}
	}

	/**
	 * @author pushkar.m This method will receive VITemplateVersion from server
	 */
	public void getVITemlateVersion() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			if (checkGetVITemplateVerison) {

				GetVITemplateVersionTask dataTask = new GetVITemplateVersionTask();
				dataTask.execute();
				checkGetFilledFormFooter = !checkGetFilledFormFooter;
				Log.i("inside get form Section", "test"
						+ checkGetFilledFormFooter);

			}
		} else {
			// serviceProcessEnd();
		}
	}

	// ========================================================================================

	class GetAllImageTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getAllImage(params[0], params[1]);
			Log.i("data", "" + data);
			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				getAllImage(data);
				// dialog.dismiss();
			} else {
				Message message = new Message();
				message.what = SyncHandler.MSG_FILLED_FORMS;
				SyncHandler.handler.sendMessage(message);
				registerDeviceStatus = false;

			}
		}

		public void insertImageIntoSDCard(FilledFormImages images)
				throws IOException {
			String filepath = null;
			try {
				URL url = new URL(images.getStrImagepath());
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);
				urlConnection.connect();

				File SDCardRoot = Environment.getExternalStorageDirectory()
						.getAbsoluteFile();
				String filename = images.getStrImagepath(); // "downloadedFile.png";
				Log.i("Local filename:", "" + filename);
				L.fv("Sd card dir name :" + SDCardRoot.getAbsolutePath()
						+ " and file is : " + filename);
				File file = new File(SDCardRoot, filename);
				if (file.createNewFile()) {
					file.createNewFile();
				}

				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream inputStream = urlConnection.getInputStream();
				int totalSize = urlConnection.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					Log.i("Progress:", "downloadedSize:" + downloadedSize
							+ "totalSize:" + totalSize);
				}
				fileOutput.close();
				if (downloadedSize == totalSize)
					filepath = file.getPath();
				Log.i("filepath:", " " + filepath);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		private void getAllImage(String data) {

			if (data != null && !"".equals(data)) {

				try {
					InputStream xmlStream = new ByteArrayInputStream(
							data.getBytes());
					SAXParserFactory saxParserFactory = SAXParserFactory
							.newInstance();
					SAXParser saxParser = saxParserFactory.newSAXParser();

				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	}

	// ========================================================================================

	class GetInsertImageIntoSDCard extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			String filepath = null;
			try {
				URL url = new URL(params[0]);// (images.getImagePath());
				Log.d("url   :", params[0]);
				// Log.d("888   :", params[1]);
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);
				urlConnection.connect();

				File file = new File(params[1]);
				if (!file.exists()) {
					file.getParentFile().mkdirs();
					file.createNewFile();
				}

				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream inputStream = urlConnection.getInputStream();
				int totalSize = urlConnection.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					// Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+
					// totalSize) ;
				}
				fileOutput.close();
				if (downloadedSize == totalSize)
					filepath = file.getPath();
				Log.i("filepath:", " " + filepath);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		/* Checks if external storage is available for read and write */
		public boolean isExternalStorageWritable() {
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			}
			return false;
		}

		/* Checks if external storage is available to at least read */
		public boolean isExternalStorageReadable() {
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)
					|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
				return true;
			}
			return false;
		}

	}

	// ========================================================================================

	class GetUpdateImageTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getAllImage(params[0], params[1]);
			Log.i("data", "" + data);
			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				getAllImage(data);
				// dialog.dismiss();
			} else {
				cancelProgressdialog();
				IS_GET_IMAGES_RESPONSE_DATA = true;

			}
		}

		public void insertImageIntoSDCard(FilledFormImages images)
				throws IOException {
			String filepath = null;
			try {
				URL url = new URL(images.getStrImagepath());
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);
				urlConnection.connect();

				File SDCardRoot = Environment.getExternalStorageDirectory()
						.getAbsoluteFile();
				String filename = images.getStrImagepath(); // "downloadedFile.png";
				Log.i("Local filename:", "" + filename);
				L.fv("Sd card dir name :" + SDCardRoot.getAbsolutePath()
						+ " and file is : " + filename);
				File file = new File(SDCardRoot, filename);
				if (file.createNewFile()) {
					file.createNewFile();
				}

				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream inputStream = urlConnection.getInputStream();
				int totalSize = urlConnection.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					Log.i("Progress:", "downloadedSize:" + downloadedSize
							+ "totalSize:" + totalSize);
				}
				fileOutput.close();
				if (downloadedSize == totalSize)
					filepath = file.getPath();
				Log.i("filepath:", " " + filepath);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		private void getAllImage(String data) {

			if (data != null && !"".equals(data)) {

				try {
					InputStream xmlStream = new ByteArrayInputStream(
							data.getBytes());
					SAXParserFactory saxParserFactory = SAXParserFactory
							.newInstance();
					SAXParser saxParser = saxParserFactory.newSAXParser();

				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	public String ReadFromfile(String fileName, Context context) {
		StringBuilder returnString = new StringBuilder();
		InputStream fIn = null;
		InputStreamReader isr = null;
		BufferedReader input = null;
		try {
			fIn = context.getResources().getAssets()
					.open(fileName, Context.MODE_WORLD_READABLE);
			isr = new InputStreamReader(fIn);
			input = new BufferedReader(isr);
			String line = "";
			while ((line = input.readLine()) != null) {
				returnString.append(line);
			}
		} catch (Exception e) {
			e.getMessage();
		} finally {
			try {
				if (isr != null)
					isr.close();
				if (fIn != null)
					fIn.close();
				if (input != null)
					input.close();
			} catch (Exception e2) {
				e2.getMessage();
			}
		}
		return returnString.toString();
	}

	private void checkExternalMedia() {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// Can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// Can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Can't read or write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		// Toast.makeText(getApplicationContext(),
		// "\n\nExternal Media: readable="
		// +mExternalStorageAvailable+" writable="+mExternalStorageWriteable,
		// Toast.LENGTH_LONG).show();
	}

	private void writeToSDFile(String data) {

		File root = android.os.Environment.getExternalStorageDirectory();

		File dir = new File(root.getAbsolutePath() + "/download");
		dir.mkdirs();
		File file = new File(dir, "myData.txt");

		try {
			FileOutputStream f = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(f);
			pw.println("Hi , How are you");
			pw.println(data);
			pw.flush();
			pw.close();
			f.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Log.i("TAG",
					"******* File not found. Did you"
							+ " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Toast.makeText(getApplicationContext(), "\n\nFile written to "+file,
		// Toast.LENGTH_LONG).show();
	}

	// =====================================================================================

	/**
	 * @author Ripunjay Shukla This class is used to create acknowledgment for
	 *         filledform in background.
	 * 
	 */
	class SendAckForFilledForm extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			StringBuffer formIdBuffer;
			try {
				formIdBuffer = formBuff;
			} catch (Exception e) {

				formIdBuffer = new StringBuffer();
			}

			int hasMore = -1;
			String flag = params[1];
			try {
				hasMore = 0;// Integer.parseInt(params[0]);
			} catch (Exception e) {
				hasMore = -1;
			}
			Log.i("form id ", "Alok" + formIdBuffer.toString());
			Log.i("mac id ", "" + CommonUtil.getIMEI(context));
			if (formIdBuffer.toString().trim().length() > 0) {
				// nothing to do here
			}

			Log.i("data", "" + data);

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				// sendAck(data);
			}

		}

	}

	public static void startProgressdialog() {
		dialog = new ProgressDialog(context);
		dialog.setMessage("Sync in progress..");
		dialog.show();
		dialog.setCancelable(false);
	}

	private void cancelProgressdialog() {
		// if(dialog != null)
		// dialog.dismiss();
	}

	public void sendAck(String data) {

	}

	// Pushkar : 29Oct15
	class GetUpdatedFormImageDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String macId;
		private String viId;
		private int shipId;

		public GetUpdatedFormImageDataTask(String macId, String viId,
				String shipId) {
			this.macId = macId;
			this.viId = viId;
			this.shipId = Integer.parseInt(((shipId != null && !""
					.equals(shipId.trim())) ? shipId : "0"));
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_UPDATED_FORM_IMAGE_SUCCESS == true
						&& IS_GET_UPDATED_FORM_IMAGE_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_UPDATED_FORM_IMAGE_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			if (viId != null && !"".equals(viId)) {
				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					StringBuffer notInIds = new StringBuffer(); // fetch image
																// id from
																// DBManager
					DBManager dbManager = new DBManager(context);
					dbManager.open();
					List<FilledFormImages> imageList = dbManager
							.getFilledFormImagesData();
					dbManager.close();
					if (imageList != null && imageList.size() > 0) {
						for (FilledFormImages ffm : imageList) {
							if (ffm.getFlgDeleted() == 0) {
								notInIds.append("'"
										+ ffm.getStrFilledFormImageId() + "',");
							}
						}
					}

					if (notInIds.length() > 0)
						data = services.getUpdatedVIFormImages(viId, shipId,
								CommonUtil.getUserLogin(context),
								notInIds.substring(0, notInIds.length() - 1));
					else
						data = services.getUpdatedVIFormImages(viId, shipId,
								CommonUtil.getUserLogin(context), "");
				} else {
					data = services.getUpdatedVIFormImages(viId, shipId,
							CommonUtil.getUserLogin(context), "");
				}

				if (data != null && !"".equals(data))
					IS_GET_UPDATED_FORM_IMAGE_RESPONSE_DATA = true;
				else
					IS_GET_UPDATED_FORM_IMAGE_UNSUCCESS = true;
				// }

				Log.i("data", "" + data + "do in background");
				Log.i("do in background of send filled form", "" + data);

				return true;
			} else
				return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			int hasMore = 0;

			if (result) {
				if (data != null) {
					String ackIds = parseUpdatedFormImageFromWeb(data);
					IS_GET_UPDATED_FORM_IMAGE_SUCCESS = true;

					if (ackIds != null && !"".equals(ackIds)) {
						if (ackIds.indexOf("hasMore") > 0) {
							ackIds = ackIds.replace("hasMore", "");
							hasMore = 1;
						}
						// called acknowledging ids methods
						SendAckForFilledFormImage safffi = new SendAckForFilledFormImage(
								hasMore, viId, shipId,
								CommonUtil.getUserLogin(context));
						safffi.execute(ackIds);
					} else {
						// serviceProcessEnd();
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_ROLE_TEMPLATE;
						SyncHandler.handler.sendMessage(message);
					}
				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {
				if (IS_GET_UPDATED_FORM_IMAGE_SUCCESS) {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_ROLE_TEMPLATE;
					SyncHandler.handler.sendMessage(message);
				}

			}
			// For case when service is calling for activating inspection
			// from
			// HOME
			if (hasMore == 0
					&& CommonUtil.getActiveInspectionID(context) != null
					&& !"".equals(CommonUtil.getActiveInspectionID(context))) {

				DBManager dbManager = new DBManager(context);
				dbManager.open();
				List<VesselInspection> vslList = dbManager
						.getVesselInspectionDataById(CommonUtil
								.getActiveInspectionID(context));
				if (vslList != null && vslList.size() > 0) {
					VesselInspection vsl = vslList.get(0);
					if (vsl.getFlgStatus() < 3) {
						vsl.setFlgStatus(0);
					}
					dbManager.updateVesselInspectionTable(vsl);

					dbManager.close();
				}
				// activeInspection.setText("Active");
				CommonUtil.setActiveInspectionID(context, null);
				CommonUtil.setDropDbFlag(context, "0");
			}

		}
	}

	// Pushkar : 26Nov15
	class MatchManualSyncHistoryTask extends
			AsyncTask<String, Integer, Boolean> {
		String responseData = "";
		boolean toBeSync = false;
		List<SyncHistory> syncHistoryList = null;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_UPDATED_MANUAL_SYNC_HISTORY_SUCCESS == true
						&& IS_GET_UPDATED_MANUAL_SYNC_HISTORY_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_UPDATED_MANUAL_SYNC_HISTORY_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {

			DBManager dbManager = new DBManager(context);
			dbManager.open();
			syncHistoryList = dbManager.getSyncHistoryRow(params[0], params[1]);

			dbManager.close();
			String dataXml = "";
			if (syncHistoryList != null && syncHistoryList.size() > 0) {

				dataXml = generateXmlForMatchManualSync(syncHistoryList);
			} else {
				dataXml = generateXmlForMatchManualSync(null);
			}

			if (dataXml != null && !"".equals(dataXml)) {

				WebServiceMethod service = new WebServiceMethod(context);
				responseData = service.matchVISyncHistory(dataXml);
				return true;
			} else {

				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);

			String syncFrom = CommonUtil.getSynckFrom(context);
			CommonUtil.setSynckFrom(context, null);

			if (result) {
				if (responseData != null && !"".equals(responseData)) {
					// toBeSync = parseViSyncHistoryData(responseData);
					int resultCode = parseViSyncHistoryData(responseData);
					if (resultCode == 0) {
						toBeSync = true;
					} else {
						toBeSync = false;
					}

					IS_GET_UPDATED_MANUAL_SYNC_HISTORY_SUCCESS = true;

					if (toBeSync) {

						if (syncFrom != null
								&& !"".equals(CommonUtil.getSynckFrom(context))
								&& "process".equalsIgnoreCase(syncFrom)) {
							SyncHandler.context = context;
							Message shipMessage = new Message();
							shipMessage.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
							SyncHandler.handler.sendMessage(shipMessage);
						}
						if (syncFrom != null && !"".equals(syncFrom)
								&& "admin".equalsIgnoreCase(syncFrom)) {
							SyncHandler.context = context;
							Message shipMessage = new Message();
							shipMessage.what = SyncHandler.MSG_UPDATE_SHIP_MASETR;
							SyncHandler.handler.sendMessage(shipMessage);
						}
					} else {

						if (resultCode == 3) {
							launchPopUpForNoSync(
									context.getResources()
											.getString(
													R.string.syncNotPossibleDueToWrongShip),
									0);
						} else if (resultCode == 4) {
							displayPopUpForIncompatibleVersion(context
									.getResources()
									.getString(
											R.string.versionIncompatibleMessage));
						} else {
							launchPopUpForNoSync(context.getResources()
									.getString(R.string.syncNotPossible), 0);
						}

					}

				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {
				// calling service pool
				if (syncFrom != null
						&& !"".equals(CommonUtil.getSynckFrom(context))
						&& "process".equalsIgnoreCase(syncFrom)) {
					SyncHandler.context = context;
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
					SyncHandler.handler.sendMessage(shipMessage);
				}
				if (syncFrom != null && !"".equals(syncFrom)
						&& "admin".equalsIgnoreCase(syncFrom)) {
					SyncHandler.context = context;
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_UPDATE_SHIP_MASETR;
					SyncHandler.handler.sendMessage(shipMessage);
				}

			}
		}

	}

	class GetShipExistOrNotTask extends AsyncTask<String, Integer, Boolean> {

		public String response;

		int shipId = 0;
		String callingThrough = "";

		public GetShipExistOrNotTask(int shipId, String callingThrough) {

			this.shipId = shipId;
			this.callingThrough = callingThrough;
		}

		@SuppressWarnings("unused")
		protected void onPreExecute() {
			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_SHIP_EXIST_SUCCESS == true
						&& IS_GET_SHIP_EXIST_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_SHIP_EXIST_UNSUCCESS = false;
				}
			}
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {
				WebServiceMethod services = new WebServiceMethod(context);
				response = services.getShipExistOrNot(shipId);

			} catch (Exception e) {
				// TODO: handle exception
			}

			if (response != null && "yes".equals(response))
				return true;
			else
				return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				if (callingThrough != null
						&& "activate".equalsIgnoreCase(callingThrough)) {
					SyncHandler.context = context;
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_UPDATE_VI_DATA;
					// shipMessage.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM;
					SyncHandler.handler.sendMessage(shipMessage);
				}
			} else {
				if (response != null) {
					displayPopUpNorNoShipExist(context.getResources()
							.getString(R.string.wrongShip));
				} else {
					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 0);
				}
			}
		}
	}

	class SendAckForFilledFormImage extends AsyncTask<String, Integer, Boolean> {

		private String data;
		int hasMore = 0;
		String viId = "";
		int shipId = 0;
		String userLogin = "";
		String ackIds = "";

		public SendAckForFilledFormImage(int hasMore, String viId, int shipId,
				String userLogin) {
			this.hasMore = hasMore;
			this.viId = viId;
			this.shipId = shipId;
			this.userLogin = CommonUtil.getUserLogin(context);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {
				WebServiceMethod services = new WebServiceMethod(context);
				services.sendFilledFormImageAck(params[0]);

			} catch (Exception e) {
				// TODO: handle exception
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				SyncHandler.context = context;
				if (hasMore > 0) {

					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_IMAGE;
					SyncHandler.handler.sendMessage(message);
				} else {

					Message message = new Message();
					message.what = SyncHandler.MSG_GET_ROLE_TEMPLATE;
					SyncHandler.handler.sendMessage(message);
				}
			}
		}
	}

	class GetRoleTemplateDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private int tenantId = 0;
		private int roleId = 0;

		public GetRoleTemplateDataTask(int tenantId, int roleId) {
			this.tenantId = tenantId;
			this.roleId = roleId;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {

				WebServiceMethod services = new WebServiceMethod(context);

				if (roleId == 0) {
					data = services.getRoleTemplateData(tenantId);
				} else {
					data = services.getRoleTemplateData(tenantId, roleId);
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				if (data != null && !"".equals(data)) {

					parseGetRoleTemplateResponse(data);

					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_VI_TEMPLATE_VERSION;
					SyncHandler.handler.sendMessage(message);

				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			}
		}
	}

	class AckFilledFormTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {
				WebServiceMethod services = new WebServiceMethod(context);
				services.sendFilledFormAck(params[0], params[1]);

			} catch (Exception e) {
				// TODO: handle exception
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_FILLED_FORM_DESC;
				SyncHandler.handler.sendMessage(message);
			}
		}
	}

	class AckFilledFormDescTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		public int hasMore = 0;
		String viId;
		int shipId = 0;
		String userLogin;

		public AckFilledFormDescTask(int hasMore, String viId, int shipId,
				String userLogin) {
			this.hasMore = hasMore;
			this.viId = viId;
			this.shipId = shipId;
			this.userLogin = CommonUtil.getUserLogin(context);
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {
				WebServiceMethod services = new WebServiceMethod(context);
				//services.sendFilledFormDescAck(params[0], params[1]);
				services.sendFilledFormDescAck(params[0], String.valueOf(shipId));

			} catch (Exception e) {
				// TODO: handle exception
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				if (hasMore == 0) {

					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST;
					SyncHandler.handler.sendMessage(message);
				} else {

					SyncHandler.context = context;
					Message message = new Message();
					// message.what = SyncHandler.MSG_UPDATE_FILLED_FORM_DESC;
					message.what = SyncHandler.MSG_GET_FILLED_FORM_DESC;
					SyncHandler.handler.sendMessage(message);
				}
			}
		}
	}

	class AckFilledFormFooterTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			try {
				WebServiceMethod services = new WebServiceMethod(context);
				services.sendFilledFormFooterAck(params[0], params[1]);

			} catch (Exception e) {
				// TODO: handle exception
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				SyncHandler.context = context;
				IS_GET_UPDATED_FORM_IMAGE_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST;
				SyncHandler.handler.sendMessage(message);
			}
		}
	}

	// Pushkar 30Oct15
	class GetUpdatedFilledFormTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		String macId;
		String viId;
		String shipId;

		GetUpdatedFilledFormTask(String macId, String viId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.viId = viId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_UPDATED_FILLED_FORM_SUCCESS == true
						&& IS_GET_UPDATED_FILLED_FORM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_UPDATED_FILLED_FORM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			if (viId != null && !"".equals(viId)) {

				data = services.getUpdatedVIForms(
						viId,
						Integer.parseInt((shipId != null
								&& !"".equals(shipId.trim()) ? shipId : "0")),
						CommonUtil.getUserLogin(context));

				if (data != null && !"".equals(data)) {

					IS_GET_UPDATED_FILLED_FORM_RESPONSE_DATA = true;
					return true;

				} else {

					IS_GET_UPDATED_FILLED_FORM_UNSUCCESS = true;
					return false;
				}
			} else {
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			String ids = "";
			super.onPostExecute(result);
			if (result) {

				ids = parseSendVesselInspectionFormListResponse(data);
				IS_GET_UPDATED_FILLED_FORM_SUCCESS = true;

				if (ids != null && !"".equals(ids)) {
					AckFilledFormTask ackTask = new AckFilledFormTask();
					ackTask.execute(ids, shipId);
				} else {

					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_FILLED_FORM_DESC;
					SyncHandler.handler.sendMessage(message);
				}

			} else {
				if (viId == null || "".equalsIgnoreCase(viId)) {
					launchPopUpForNoReviewStatus(context.getResources()
							.getString(R.string.noActiveInspectionOnDevice), 10);
				} else if (data == null) {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				} else {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_FILLED_FORM_DESC;
					SyncHandler.handler.sendMessage(message);
				}
			}
		}

	}

	/**
	 * @since 01-02-2016
	 * @author admin
	 * 
	 */
	class GetUpdatedFilledFormDescTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		String macId;
		String viId;
		int shipId;

		GetUpdatedFilledFormDescTask(String macId, String viId, String shipId) {
			this.macId = macId;
			this.shipId = Integer.parseInt((shipId != null && !""
					.equals(shipId)) ? shipId : "0");
			this.viId = viId;
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			if (viId != null && !"".equals(viId)) {
				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					StringBuffer notInIds = new StringBuffer(); // fetch
																// VIFLDesc id
																// from
																// DBManager
					DBManager dbManager = new DBManager(context);
					dbManager.open();
					List<VesselInspectionFormListDesc> viflDescList = dbManager
							.getAllVIFormListDescByVIId(viId);

					dbManager.close();
					if (viflDescList != null && viflDescList.size() > 0) {
						for (VesselInspectionFormListDesc viflDesc : viflDescList) {
							if (viflDesc.getFlgDeleted() == 0) {
								notInIds.append("'"
										+ viflDesc
												.getiVesselInspectionFormListDescId()
										+ "',");
							}
						}
					}

					if (notInIds.length() > 0)
						data = services.getUpdatedFilledFormDesc(viId, shipId,
								Integer.parseInt(CommonUtil
										.getUserLogin(context)), notInIds
										.substring(0, notInIds.length() - 1));
					else
						data = services.getUpdatedFilledFormDesc(viId, shipId,
								Integer.parseInt(CommonUtil
										.getUserLogin(context)), "");
				} else {
					data = services.getUpdatedFilledFormDesc(viId, shipId,
							Integer.parseInt(CommonUtil.getUserLogin(context)),
							"");
				}

				return true;
			} else
				return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
			int hasMore = 0;
			if (result) {

				if (data != null && !"".equals(data)) {
					String ackIds = parseGetFilledFormDescResponse(data);

					if (ackIds != null && !"".equals(ackIds)) {
						if (ackIds.indexOf("hasMore") > 0) {
							ackIds = ackIds.replace("hasMore", "");
							hasMore = 1;
						}
						// called acknowledging ids methods
						AckFilledFormDescTask safffi = new AckFilledFormDescTask(
								hasMore, viId, shipId,
								CommonUtil.getUserLogin(context));
						safffi.execute(ackIds);
					} else {
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST;
						SyncHandler.handler.sendMessage(message);
					}
				} else {
					/*
					 * SyncHandler.context = context; Message message = new
					 * Message(); message.what =
					 * SyncHandler.MSG_GET_FILLED_FORM_DESC;
					 * SyncHandler.handler.sendMessage(message);
					 */

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}

	class GetUpdatedFilledFormFooterTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		String macId;
		String viId;
		String shipId;

		GetUpdatedFilledFormFooterTask(String macId, String viId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.viId = viId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			data = services.getUpdatedFilledFormFooter(
					viId,
					(shipId != null && !"".equals(shipId.trim()) ? Integer
							.parseInt(shipId) : 0), Integer.parseInt(CommonUtil
							.getUserLogin(context)));

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			String ids = "";
			super.onPostExecute(result);
			if (result) {
				if (data != null) {
					ids = parseGetFilledFormFooterResponse(data);

					if (ids != null && !"".equals(ids)) {
						AckFilledFormFooterTask ackTask = new AckFilledFormFooterTask();
						ackTask.execute(ids, shipId);
					} else {

						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST;
						SyncHandler.handler.sendMessage(message);
					}
				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			}

		}
	}

	class GetUpdatedFilledFormCheckListTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String viId;
		private String shipId;
		private String macId;

		public GetUpdatedFilledFormCheckListTask(String macId, String viId,
				String shipId) {
			this.viId = viId;
			this.macId = macId;
			this.shipId = shipId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_SUCCESS == true
						&& IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			if (viId != null && !"".equals(viId)) {

				data = services.getUpdatedVICheckList(
						viId,
						Integer.parseInt((shipId != null
								&& !"".equals(shipId.trim()) ? shipId : "0")),
						CommonUtil.getUserLogin(context));

				if (data != null && !"".equals(data)) {

					IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_RESPONSE_DATA = true;
					return true;

				} else {

					IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_UNSUCCESS = true;
					return false;
				}
			} else {
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			String ids = "";
			super.onPostExecute(result);

			if (result) {

				ids = parseFilledChecklistResponse(data);
				IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_SUCCESS = true;

			} else {
				if (viId != null && data == null) {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);

				} else {
					IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_SUCCESS = true;
				}
			}

			if (IS_GET_UPDATED_FILLED_FORM_CHECK_LIST_SUCCESS) {
				SyncHandler.context = context;
				IS_GET_UPDATED_FORM_IMAGE_SUCCESS = true;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM_IMAGE;
				SyncHandler.handler.sendMessage(message);
			}

		}

	}

	class SendFilledFormDescTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_FILLED_FORM_HEADER_RESPONSE_DATA == true
						&& IS_UPDATE_FILLED_FORM_HEADER_UNSUCCESS == true) {
					// startProgressdialog();
					IS_UPDATE_FILLED_FORM_HEADER_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			xmlData = genrateXmlDataForFilledFormDesc();

			// Log.i("generated xml data", xmlData);

			if (xmlData != null && !"".equals(xmlData)) {
				data = services.sendFilledFormDesc(xmlData);

				if (data != null && !"".equals(data))
					IS_UPDATE_FILLED_FORM_HEADER_RESPONSE_DATA = true;
				else
					IS_UPDATE_FILLED_FORM_HEADER_UNSUCCESS = true;
			}

			Log.i("data", "" + data + "do in background");
			Log.i("do in background of send filled form", "" + data);

			if (xmlData != null && !"".equals(xmlData)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
			if (result) {
				if (data != null) {
					parseSendFilledFormDescResponse(data);
					IS_UPDATE_FILLED_FORM_HEADER_SUCCESS = true;
				} else {
					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {

				SyncHandler.context = context;
				IS_UPDATE_FILLED_FORM_HEADER_SUCCESS = true;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}

	class SendVesselInspectionDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// startProgressdialog();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_SEND_FILLED_FORM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_SEND_FILLED_FORM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			xmlData = genrateXmlDataForVesselInspection();

			if (xmlData != null && !"".equals(xmlData)) {

				data = services.sendVesselInspectionData(params[0], params[1],
						xmlData);

				if (data != null && !"".equals(data))
					IS_SEND_FILLED_FORM_RESPONSE_DATA = true;
				else
					IS_SEND_FILLED_FORM_UNSUCCESS = true;
			}

			Log.i("data", "" + data + "do in background");
			Log.i("do in background of send filled form", "" + data);

			if (xmlData != null && !"".equals(xmlData)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
			if (result) {
				if (data != null) {
					parseSendVesselInspectionResponse(data);
					IS_SEND_FILLED_FORM_SUCCESS = true;
				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {

				SyncHandler.context = context;
				IS_SEND_FILLED_FORM_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_SEND_FILLED_FORM_SYNC;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}

	// ================================================================
	class SendFillCheckListDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_SEND_FILLED_FORM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_SEND_FILLED_FORM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			xmlData = genrateXmlDataForFilledChecklist();

			// Log.i("generated xml data", xmlData);
			if (xmlData != null && !"".equals(xmlData)) {
				data = services.sendFilledCheckListData(params[0], params[1],
						xmlData);

				if (data != null && !"".equals(data))
					IS_SEND_FILLED_FORM_RESPONSE_DATA = true;
				else
					IS_SEND_FILLED_FORM_UNSUCCESS = true;
			}

			Log.i("data", "" + data + "do in background");
			Log.i("do in background of send filled form", "" + data);

			if (xmlData != null && !"".equals(xmlData)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				if (data != null) {
					parseSendFilledCheckListResponse(data);
					IS_SEND_FILLED_FORM_SUCCESS = true;
				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {

				IS_SEND_FILLED_FORM_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_SEND_FORM_IMAGES_SYNC;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}

	// =============================================

	class SendFillFormDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_SEND_FILLED_FORM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_SEND_FILLED_FORM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			xmlData = genrateXmlDataForForm();

			// Log.i("generated xml data", xmlData);
			if (xmlData != null && !"".equals(xmlData)) {
				data = services.sendFilledFormData(params[0], params[1],
						xmlData);

				if (data != null && !"".equals(data))
					IS_SEND_FILLED_FORM_RESPONSE_DATA = true;
				else
					IS_SEND_FILLED_FORM_UNSUCCESS = true;
			}

			Log.i("data", "" + data + "do in background");
			Log.i("do in background of send filled form", "" + data);

			if (xmlData != null && !"".equals(xmlData)) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				if (data != null) {
					parseSendFilledFormResponse(data);
					IS_SEND_FILLED_FORM_SUCCESS = true;
				} else {

					launchPopUpForNoSync(
							context.getResources().getString(
									R.string.syncNotPossibleDueToCommError), 10);
				}

			} else {

				SyncHandler.context = context;
				IS_SEND_FILLED_FORM_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_FILLED_FORM_DESC;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}

	private String parseGetRoleTemplateResponse(String data) {
		String result = "";
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			RoleTemplateParser roleTemplateParser = new RoleTemplateParser();

			saxParser.parse(xmlStream, roleTemplateParser);

			List<RoleTemplate> list = roleTemplateParser.getRoleTemplateData();

			if (list != null && list.size() > 0) {
				DBManager dbManager = new DBManager(context);
				dbManager.open();

				for (RoleTemplate roleTemp : list) {

					List<RoleTemplate> roleTempList = dbManager
							.getRoleTemplateDataById(roleTemp
									.getiRoleTemplateId());

					if (roleTempList != null && roleTempList.size() > 0)
						dbManager.updateRoleTemplateData(roleTemp);
					else
						dbManager.insertRoleTemplateData(roleTemp);
				}
				dbManager.close();
			}

			/*
			 * serviceProcessEnd(); if (CommonUtil.getLastActiveShip(context) !=
			 * null && !"".equals(CommonUtil.getLastActiveShip(context))) {
			 * CommonUtil.setShipID(context,
			 * CommonUtil.getLastActiveShip(context));
			 * CommonUtil.setLastActiveShip(context, null); }
			 */
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return result;
	}

	private String parseSendVesselInspectionFormListResponse(String data) {

		StringBuffer ids = new StringBuffer("");
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			FilledFormParsar formParser = new FilledFormParsar();

			saxParser.parse(xmlStream, formParser);

			List<FilledForm> list = formParser.getFilledFormData();

			if (list != null && list.size() > 0) {
				DBManager dbManager = new DBManager(context);
				dbManager.open();

				List<FilledForm> formListFromDb = new ArrayList<FilledForm>();
				Iterator<FilledForm> formIterator = list.iterator();

				while (formIterator.hasNext()) {

					FilledForm form = (FilledForm) formIterator.next();
					formListFromDb = dbManager.getFilledFormDataById(form
							.getiFilledFormId());

					form.setFlgIsDeviceDirty(0);
					form.setFlgIsDirty(0);
					if (formListFromDb != null && formListFromDb.size() > 0) {

						dbManager.updateFilldFormTable(form);
					} else {
						dbManager.insertFilldFormTable(form);
					}

					ids.append("'" + form.getiFilledFormId() + "',");
				}

				dbManager.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (ids.length() > 0)
			return ids.substring(0, ids.length() - 1);
		else
			return ids.toString();
	}

	private String parseFilledChecklistResponse(String data) {

		StringBuffer ids = new StringBuffer("");
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			FilledCheckListParsar checkListParser = new FilledCheckListParsar();

			saxParser.parse(xmlStream, checkListParser);

			List<FilledCheckList> list = checkListParser
					.getFilledCheckListData();

			if (list != null && list.size() > 0) {
				DBManager dbManager = new DBManager(context);
				dbManager.open();

				List<FilledCheckList> checkListFromDb = new ArrayList<FilledCheckList>();
				Iterator<FilledCheckList> formIterator = list.iterator();

				while (formIterator.hasNext()) {

					FilledCheckList form = (FilledCheckList) formIterator
							.next();
					checkListFromDb = dbManager.getFilledCheckListDataById(form
							.getiFilledCheckListId());

					form.setFlgIsDirty(0);
					form.setFlgIsDeviceDirty(0);
					long result = 0;
					if (checkListFromDb != null && checkListFromDb.size() > 0) {
						result = dbManager.updateFilldCheckListTable(form);
					} else {
						result = dbManager.insertFilldCheckListTable(form);
					}

					ids.append("'" + form.getiFilledCheckListId() + "',");
				}

				dbManager.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (ids.length() > 0)
			return ids.substring(0, ids.length() - 1);
		else
			return ids.toString();
	}

	private String parseUpdatedFormImageFromWeb(String data) {
		StringBuffer ackIds = new StringBuffer("");
		FilledFormImageparser formImageParser = null;
		try {
			if (data != null && !"".equals(data)) {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				formImageParser = new FilledFormImageparser();

				saxParser.parse(xmlStream, formImageParser);
				List<FilledFormImages> list = formImageParser.getImageData();
				if (list != null && list.size() > 0) {

					File storageDir = null;
					DBManager dbManager = new DBManager(context);
					dbManager.open();
					for (FilledFormImages img : list) {

						storageDir = new File(
								Environment.getExternalStorageDirectory(),
								context.getResources().getString(
										R.string.imagePath)
										+ img.getiShipId()
										+ "/"
										+ img.getiFilledFormId()
										+ "/"
										+ img.getStrImageName());

						if (img.getStrFilledFormImageId() != null
								&& !"".equals(img.getStrFilledFormImageId())) {
							img.setFlgIsDirty(0);

							List<FilledFormImages> imgListFromDB = dbManager
									.getFilledFormImagesById(img
											.getStrFilledFormImageId());

							if (imgListFromDB != null
									&& imgListFromDB.size() > 0) {

								img.setiFilledFormId(imgListFromDB.get(0)
										.getiFilledFormId());
								img.setiFormSectionId(imgListFromDB.get(0)
										.getiFormSectionId());
								img.setiFormSectionItemId(imgListFromDB.get(0)
										.getiFormSectionItemId());
								if (img.getStrImagepath() == null)
									img.setStrImagepath(imgListFromDB.get(0)
											.getStrImagepath());
								if (img.getStrRelativeImagePath() == null
										|| "".equals(img
												.getStrRelativeImagePath()))
									img.setStrRelativeImagePath(imgListFromDB
											.get(0).getStrRelativeImagePath());

								dbManager.updateFilldFormImageForWeb(img);
							} else {
								img.setStrImagepath(storageDir
										.getAbsolutePath());
								img.setStrRelativeImagePath(storageDir
										.getAbsolutePath()
										.replace("/file:", ""));

								img.setDeviceOriginalImagePath(storageDir
										.getAbsolutePath());
								img.setDeviceRelativeImagePath(storageDir
										.getAbsolutePath()
										.replace("/file:", ""));

								dbManager.insertFilldFormImageTable(img);
							}

							if (img.getDownLoadPath() != null
									&& !"".equals(img.getDownLoadPath())) {

								GetInsertImageIntoSDCard task = new GetInsertImageIntoSDCard();
								task.execute(img.getDownLoadPath(),
										storageDir.getAbsolutePath());
							}

							if (img.getFlgDeleted() == 1) {
								if (storageDir != null) {
									if (storageDir.exists())
										storageDir.delete();
								}
							}

							ackIds.append("'" + img.getStrFilledFormImageId()
									+ "',");
						}
					}
					dbManager.close();
				}

			} else {

			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (ackIds.length() > 0) {
			if (formImageParser != null && formImageParser.getHasMore() > 0)
				return ackIds.substring(0, ackIds.length() - 1) + "hasMore";
			else
				return ackIds.substring(0, ackIds.length() - 1);
		} else
			return ackIds.toString();
	}

	/**
	 * This method parse the xml response get after sending of send filled form.
	 * 
	 * @param data
	 */
	private void parseSendFilledFormResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			if (data != null && !"".equals(data)) {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				FilledFormParsar formParser = new FilledFormParsar();

				saxParser.parse(xmlStream, formParser);
				// Ripunjay:list Type Change
				List<FilledForm> list = formParser.getFilledFormData();
				for (FilledForm filledForm : list) {
					Log.i("test", filledForm.getiFilledFormId());
				}
				Iterator<FilledForm> formIterator = list.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (formIterator.hasNext()) {
					FilledForm form = (FilledForm) formIterator.next();
					Log.i("test", form.getiFilledFormId());

					result = dbManager.updateDirtyFlagFilldFormTable(form
							.getiFilledFormId());

					result = dbManager.updateFilldFormEditFlag(
							form.getiFilledFormId(), form.getFlgIsEdited());
				}

				list = new ArrayList<FilledForm>();
				list = dbManager.getFilledFormData();
				dbManager.close();

				if (list != null && list.size() > 0) {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_SEND_FILLED_FORM_SYNC;
					SyncHandler.handler.sendMessage(message);
				} else {

					SyncHandler.context = context;
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_FILLED_FORM_DESC;
					SyncHandler.handler.sendMessage(message);

				}

			} else {

				SyncHandler.context = context;
				IS_SEND_FILLED_FORM_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_SEND_FILLED_FORM_SYNC;
				SyncHandler.handler.sendMessage(message);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m This method return true/false on behalf of incoming
	 *         response of matchManualSync service
	 * @param data
	 * @return
	 */
	private int parseViSyncHistoryData(String data) {
		int resultCode = 1;
		Log.i("Stremdata", "" + data);
		try {
			if (data != null && !"".equals(data)) {

				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VISyncHistoryParsar viSyncHistoryParser = new VISyncHistoryParsar();

				saxParser.parse(xmlStream, viSyncHistoryParser);
				resultCode = viSyncHistoryParser.getResultCode();

				if (viSyncHistoryParser.getFullData() == 1)
					CommonUtil.setUserLogin(context, "1");

				List<SyncHistory> viSyncHistoryList = viSyncHistoryParser
						.getSyncHistoryData();

				Iterator<SyncHistory> syncHistoryIterator = viSyncHistoryList
						.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;

				while (syncHistoryIterator.hasNext()) {
					SyncHistory syncHistory = (SyncHistory) syncHistoryIterator
							.next();
					Log.i("test", syncHistory.getIsyncHistoryId());

					List<SyncHistory> checkDbList = dbManager
							.getSyncHistoryDataById(String.valueOf(syncHistory
									.getIsyncHistoryId()));
					syncHistory.setDtAcknowledgeDate(syncHistory
							.getDtGeneratedDate());
					if (checkDbList != null && checkDbList.size() > 0) {

						result = dbManager.updateSyncHistorydata(syncHistory);
					} else {

						result = dbManager.insertSyncHistorydata(syncHistory);
					}

				}
				dbManager.close();

				/**
				 * update device sync time.
				 */
				if (!(CommonUtil.getUserLogin(context) != null && CommonUtil
						.getUserLogin(context).equalsIgnoreCase("1"))) {
					CommonUtil.updateDeviceSyncTimeTable(context,
							CommonUtil.getShipId(context));
				}
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * if (resultCode == 0) return true; else return false;
		 */

		return resultCode;
	}

	/**
	 * This method parse the xml response get after sending of send filled
	 * checklist.
	 * 
	 * @param data
	 */
	private void parseSendFilledCheckListResponse(String data) {

		Log.i("Stremdata", "" + data);
		try {
			if (data != null && !"".equals(data)) {

				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				FilledCheckListParsar fclParser = new FilledCheckListParsar();

				saxParser.parse(xmlStream, fclParser);
				// Ripunjay:list Type Change
				List<FilledCheckList> list = fclParser.getFilledCheckListData();

				Iterator<FilledCheckList> iterator = list.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (iterator.hasNext()) {
					FilledCheckList fchkList = (FilledCheckList) iterator
							.next();
					Log.i("test", fchkList.getiFilledCheckListId());

					result = dbManager
							.updateDirtyFlagFilldCheckListTable(fchkList
									.getiFilledCheckListId());
				}

				list = new ArrayList<FilledCheckList>();
				list = dbManager.getFilledCheckListData();
				dbManager.close();

				if (list != null && list.size() > 0) {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
					SyncHandler.handler.sendMessage(message);
				} else {

					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_SEND_FORM_IMAGES_SYNC;
					SyncHandler.handler.sendMessage(message);

				}
			} else {

				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
				SyncHandler.handler.sendMessage(message);

			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * This method parse the xml response get after sending of send
	 * vesselinspection.
	 * 
	 * @param data
	 */
	private void parseSendVesselInspectionResponse(String data) {

		Log.i("Stremdata", "" + data);

		try {
			if (data != null && !"".equals(data)) {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VesselInspectionParsar viParser = new VesselInspectionParsar();

				saxParser.parse(xmlStream, viParser);
				// Ripunjay:list Type Change
				List<VesselInspection> list = viParser
						.getVesselInspectionData();

				Iterator<VesselInspection> iterator = list.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (iterator.hasNext()) {
					VesselInspection vslInspc = (VesselInspection) iterator
							.next();
					Log.i("test", vslInspc.getiVesselInspectionId());

					result = dbManager
							.updateDirtyVesselInspectionTable(vslInspc
									.getiVesselInspectionId());
				}
				dbManager.close();

				SyncHandler.context = context;
				IS_SEND_FILLED_FORM_SUCCESS = true;
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_SEND_FILLED_FORM_SYNC;
				SyncHandler.handler.sendMessage(message);

			} else {
				// problem in service calling , recalling again
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
				SyncHandler.handler.sendMessage(message);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * This method gets form data from database then create a xml for those
	 * forms and return as xml in form of string.
	 * 
	 * @return xmlString
	 */
	private String genrateXmlDataForForm() {
		List<FilledForm> formList = new ArrayList<FilledForm>();
		DBManager dbManager = new DBManager(context);
		dbManager.open();
		List<FilledForm> list = dbManager.getFilledFormData();
		dbManager.close();
		int count = 0;
		if (list != null && list.size() > 0) {
			for (FilledForm form : list) {
				Log.i("form", form.toString());
				if (count >= 100)
					break;
				if (form.getFlgIsDirty() == 1
						&& form.getiShipId() == Integer.parseInt(CommonUtil
								.getShipId(context))) {
					formList.add(form);
					count = count + 1;
				}
			}

			if (formList != null && formList.size() > 0) {
				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get form data ", "inside");
				Iterator<FilledForm> formIterator = formList.iterator();
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<updateVIData>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");
				xmlData.append("<versionNo>");
				xmlData.append(context.getResources().getString(
						R.string.versionNumber));
				xmlData.append("</versionNo>");

				while (formIterator.hasNext()) {
					FilledForm form = (FilledForm) formIterator.next();

					xmlData.append("<table>");

					xmlData.append("<tableName>");
					xmlData.append("VesselInspectionFormList");
					xmlData.append("</tableName>");

					xmlData.append("<iVesselInspectionFormListId>");
					if (form.getiFilledFormId() != null) {
						xmlData.append(form.getiFilledFormId());
					}
					xmlData.append("</iVesselInspectionFormListId>");

					xmlData.append("<iVesselInspectionId>");
					if (form.getiVesselInspectionId() != null) {
						xmlData.append(form.getiVesselInspectionId());
					}
					xmlData.append("</iVesselInspectionId>");

					xmlData.append("<iFormSectionId>");
					if (form.getiFormSectionId() != null) {
						xmlData.append(form.getiFormSectionId());
					}
					xmlData.append("</iFormSectionId>");

					xmlData.append("<iFormSectionItemsId>");
					if (form.getiFormSectionItemId() != null) {
						xmlData.append(form.getiFormSectionItemId());
					}
					xmlData.append("</iFormSectionItemsId>");

					/*
					 * xmlData.append("<strRemarks>"); if (form.getStrRemarks()
					 * != null && !"".equals(form.getStrRemarks()) &&
					 * !"null".equals(form.getStrRemarks())) {
					 * xmlData.append(form.getStrRemarks()); }
					 * xmlData.append("</strRemarks>");
					 */

					xmlData.append("<flgChecked>");
					if (form.getFlgChecked() != null) {
						xmlData.append(form.getFlgChecked());
					}
					xmlData.append("</flgChecked>");

					xmlData.append("<strHeader>");
					if (form.getStrHeaderDesc() != null
							&& !"".equals(form.getStrHeaderDesc())
							&& !"null".equals(form.getStrHeaderDesc())) {

						xmlData.append(CommonUtil.marshal(form
								.getStrHeaderDesc()));
					}
					xmlData.append("</strHeader>");

					xmlData.append("<strFooter>");
					if (form.getStrFooterDesc() != null
							&& !"".equals(form.getStrFooterDesc())
							&& !"null".equals(form.getStrFooterDesc())) {

						xmlData.append(CommonUtil.marshal(form
								.getStrFooterDesc()));
					}
					xmlData.append("</strFooter>");

					xmlData.append("<flgStatus>");
					if (form.getFlgStatus() > -1) {
						xmlData.append(form.getFlgStatus());
					}
					xmlData.append("</flgStatus>");

					xmlData.append("<sequence>");
					if (form.getSequence() > -1) {
						xmlData.append(form.getSequence());
					}
					xmlData.append("</sequence>");

					xmlData.append("<flgDeleted>");

					xmlData.append(form.getFlgDeleted());

					xmlData.append("</flgDeleted>");

					xmlData.append("<flgIsDirty>");

					xmlData.append(form.getFlgIsDirty());

					xmlData.append("</flgIsDirty>");

					xmlData.append("<flgIsEdited>");

					xmlData.append(form.getFlgIsEdited());

					xmlData.append("</flgIsEdited>");

					xmlData.append("<flgIsFooterEdited>");

					xmlData.append(form.getFlgIsFooterEdited());

					xmlData.append("</flgIsFooterEdited>");

					xmlData.append("<flgIsHeaderEdited>");

					xmlData.append(form.getFlgIsHeaderEdited());

					xmlData.append("</flgIsHeaderEdited>");

					xmlData.append("<flgIsDeviceDirty>");

					xmlData.append(form.getFlgIsDeviceDirty());

					xmlData.append("</flgIsDeviceDirty>");

					xmlData.append("<dtCreated>");
					if (form.getCreatedDate() != null
							&& !"null".equalsIgnoreCase(form.getCreatedDate())) {
						xmlData.append(form.getCreatedDate());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					if (form.getCreatedBy() != null) {

						xmlData.append(CommonUtil.getUserId(context));
					}
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (form.getModifiedDate() != null
							&& !"null".equalsIgnoreCase(form.getModifiedDate())) {
						xmlData.append(form.getModifiedDate());
					}
					xmlData.append("</dtUpdated>");

					xmlData.append("<updatedBy>");
					if (form.getModifiedBy() != null) {
						xmlData.append(1);
					}
					xmlData.append("</updatedBy>");

					xmlData.append("<iTenantId>");

					xmlData.append(form.getiTenantId());

					xmlData.append("</iTenantId>");

					xmlData.append("<iShipId>");

					xmlData.append(form.getiShipId());

					xmlData.append("</iShipId>");

					xmlData.append("</table>");
				}
				xmlData.append("</updateVIData>");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form,";
				return null;
			}
		} else {
			Log.i("get form data ", "else");
			toastMsg = toastMsg + "No update present for send filled form,";
			return null;
		}

	}

	private String genrateXmlDataForFilledChecklist() {

		List<FilledCheckList> filledCheckList = new ArrayList<FilledCheckList>();
		DBManager dbManager = new DBManager(context);
		dbManager.open();
		List<FilledCheckList> list = dbManager.getFilledCheckListData();
		dbManager.close();
		int count = 0;
		if (list != null && list.size() > 0) {
			for (FilledCheckList fcl : list) {
				Log.i("form", fcl.toString());
				if (count >= 40)
					break;
				if (fcl.getFlgIsDirty() == 1
						&& fcl.getiShipId() == Integer.parseInt(CommonUtil
								.getShipId(context))) {
					filledCheckList.add(fcl);
					count = count + 1;
				}
			}

			if (filledCheckList != null && filledCheckList.size() > 0) {
				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get form data ", "inside");
				Iterator<FilledCheckList> filledChecklistIterator = filledCheckList
						.iterator();
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<updateVIData>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");
				xmlData.append("<versionNo>");
				xmlData.append(context.getResources().getString(
						R.string.versionNumber));
				xmlData.append("</versionNo>");

				while (filledChecklistIterator.hasNext()) {
					FilledCheckList filledCL = (FilledCheckList) filledChecklistIterator
							.next();

					xmlData.append("<table>");

					xmlData.append("<tableName>");
					xmlData.append("VesselInspectionCheckList");
					xmlData.append("</tableName>");

					xmlData.append("<iVesselInspectionCheckListId>");
					if (filledCL.getiFilledCheckListId() != null) {
						xmlData.append(filledCL.getiFilledCheckListId());
					}
					xmlData.append("</iVesselInspectionCheckListId>");

					xmlData.append("<iVesselInspectionId>");
					if (filledCL.getiVesselInspectionId() != null) {
						xmlData.append(filledCL.getiVesselInspectionId());
					}
					xmlData.append("</iVesselInspectionId>");

					xmlData.append("<iCheckListSectionId>");
					if (filledCL.getiCheckListSectionId() != null) {
						xmlData.append(filledCL.getiCheckListSectionId());
					}
					xmlData.append("</iCheckListSectionId>");

					xmlData.append("<iCheckListSectionItemsId>");
					if (filledCL.getiCheckListSectionItemsId() != null) {
						xmlData.append(filledCL.getiCheckListSectionItemsId());
					}
					xmlData.append("</iCheckListSectionItemsId>");

					xmlData.append("<flgChecked>");
					if (filledCL.getFlgChecked() != null) {
						xmlData.append(filledCL.getFlgChecked());
					}
					xmlData.append("</flgChecked>");

					xmlData.append("<strRemarks>");
					if (filledCL.getStrRemarks() != null) {

						xmlData.append(CommonUtil.marshal(filledCL
								.getStrRemarks()));
					}
					xmlData.append("</strRemarks>");

					xmlData.append("<flgStatus>");
					if (filledCL.getFlgStatus() > -1) {
						xmlData.append(filledCL.getFlgStatus());
					}
					xmlData.append("</flgStatus>");

					/*
					 * xmlData.append("<sequence>"); if (filledCL.getSequence()
					 * > -1) { xmlData.append(filledCL.getSequence()); }
					 * xmlData.append("</sequence>");
					 */

					xmlData.append("<flgDeleted>");
					if (filledCL.getCreatedDate() != null) {
						xmlData.append(filledCL.getFlgDeleted());
					}
					xmlData.append("</flgDeleted>");

					xmlData.append("<flgIsDirty>");
					xmlData.append(filledCL.getFlgIsDirty());
					xmlData.append("</flgIsDirty>");

					xmlData.append("<dtCreated>");
					if (filledCL.getCreatedDate() != null
							&& !"null".equalsIgnoreCase(filledCL
									.getCreatedDate())) {
						xmlData.append(filledCL.getCreatedDate());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					if (filledCL.getCreatedBy() != null) {

						xmlData.append(CommonUtil.getUserId(context));
					}
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (filledCL.getModifiedDate() != null
							&& !"null".equalsIgnoreCase(filledCL
									.getModifiedDate())) {
						xmlData.append(filledCL.getModifiedDate());
					}
					xmlData.append("</dtUpdated>");

					xmlData.append("<updatedBy>");
					if (filledCL.getModifiedBy() != null) {
						xmlData.append(1);
					}
					xmlData.append("</updatedBy>");

					xmlData.append("<iTenantId>");
					if (filledCL.getiTenantId() > 0) {
						xmlData.append(filledCL.getiTenantId());
					}
					xmlData.append("</iTenantId>");

					xmlData.append("<iShipId>");
					if (filledCL.getiShipId() > 0) {
						xmlData.append(filledCL.getiShipId());
					}
					xmlData.append("</iShipId>");

					xmlData.append("<flgIsDeviceDirty>");
					xmlData.append(filledCL.getFlgIsDeviceDirty());
					xmlData.append("</flgIsDeviceDirty>");

					xmlData.append("<flgIsEdited>");
					xmlData.append(filledCL.getFlgIsEdited());
					xmlData.append("</flgIsEdited>");

					xmlData.append("</table>");
				}
				xmlData.append("</updateVIData>");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form,";
				return null;
			}
		} else {
			Log.i("get form data ", "else");
			toastMsg = toastMsg + "No update present for send filled form,";
			return null;
		}

	}

	private String genrateXmlDataForVesselInspection() {

		List<VesselInspection> vesselInspectionList = new ArrayList<VesselInspection>();
		DBManager dbManager = new DBManager(context);
		dbManager.open();
		List<VesselInspection> list = dbManager
				.getVesselInspectionData(CommonUtil.getShipId(context));
		dbManager.close();
		if (list != null && list.size() > 0) {
			for (VesselInspection vInsp : list) {
				Log.i("form", vInsp.toString());
				// if (vInsp.getFlgIsDirty() == 1) {
				if (vInsp.getFlgIsDirty() == 1 && vInsp.getFlgStatus() >= 0) {
					vesselInspectionList.add(vInsp);
				}
			}

			if (vesselInspectionList != null && vesselInspectionList.size() > 0) {

				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get form data ", "inside");

				String version = "";
				try {
					PackageInfo pInfo = context.getPackageManager()
							.getPackageInfo(context.getPackageName(), 0);
					if (pInfo != null && pInfo.versionName != null) {
						version = pInfo.versionName;
					} else {
						version = "2.0";
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				Iterator<VesselInspection> vesselInspectionIterator = vesselInspectionList
						.iterator();
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<updateVIData>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");
				xmlData.append("<versionNo>");
				xmlData.append(version);
				xmlData.append("</versionNo>");
				while (vesselInspectionIterator.hasNext()) {
					VesselInspection vslInspc = (VesselInspection) vesselInspectionIterator
							.next();

					xmlData.append("<table>");

					xmlData.append("<tableName>");
					xmlData.append("VesselInspection");
					xmlData.append("</tableName>");

					xmlData.append("<iVesselInspectionId>");
					if (vslInspc.getiVesselInspectionId() != null) {
						xmlData.append(vslInspc.getiVesselInspectionId());
					}
					xmlData.append("</iVesselInspectionId>");

					xmlData.append("<strVesselInspectionName>");

					xmlData.append("</strVesselInspectionName>");

					xmlData.append("<strRemarks>");
					xmlData.append(version);
					xmlData.append("</strRemarks>");

					xmlData.append("<flgStatus>");
					if (vslInspc.getFlgStatus() > -1) {
						xmlData.append(vslInspc.getFlgStatus());
					}
					xmlData.append("</flgStatus>");

					xmlData.append("<sequence>");
					if (vslInspc.getSequence() > -1) {
						xmlData.append(vslInspc.getSequence());
					}
					xmlData.append("</sequence>");

					xmlData.append("<flgDeleted>");
					xmlData.append(vslInspc.getFlgDeleted());
					xmlData.append("</flgDeleted>");

					xmlData.append("<dtInspectionDate>");
					if (vslInspc.getInspectionDate() != null) {
						xmlData.append(vslInspc.getInspectionDate());
					}
					xmlData.append("</dtInspectionDate>");

					xmlData.append("<dtCreated>");
					if (vslInspc.getCreatedDate() != null
							&& !"null".equalsIgnoreCase(vslInspc
									.getCreatedDate())) {
						xmlData.append(vslInspc.getCreatedDate());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					xmlData.append(CommonUtil.getUserId(context));
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (vslInspc.getModifiedDate() != null
							&& !"null".equalsIgnoreCase(vslInspc
									.getModifiedDate())) {
						xmlData.append(vslInspc.getModifiedDate());
					}
					xmlData.append("</dtUpdated>");
					xmlData.append("<inspectedBy>");
					xmlData.append(CommonUtil.getUserId(context));
					xmlData.append("</inspectedBy>");

					xmlData.append("<iVesselInspectionStatusId>");
					if (vslInspc.getFlgStatus() > -1) {
						xmlData.append(vslInspc.getFlgStatus());
					}
					xmlData.append("</iVesselInspectionStatusId>");

					xmlData.append("<flgIsDirty>");
					xmlData.append(vslInspc.getFlgIsDirty());
					xmlData.append("</flgIsDirty>");

					xmlData.append("<flgReverted>");
					xmlData.append(vslInspc.getFlgReverted());
					xmlData.append("</flgReverted>");

					xmlData.append("<flgStatus>");
					xmlData.append(vslInspc.getFlgStatus());
					xmlData.append("</flgStatus>");

					/*
					 * xmlData.append("<acceptedBy>"); //
					 * xmlData.append(vslInspc.getAcceptedBy());
					 * xmlData.append("</acceptedBy>");
					 * 
					 * xmlData.append("<approvedBy>"); //
					 * xmlData.append(vslInspc.getApprovedBy());
					 * xmlData.append("</approvedBy>");
					 * 
					 * xmlData.append("<completedBy>"); //
					 * xmlData.append(vslInspc.getCompletedBy());
					 * xmlData.append("</completedBy>");
					 * 
					 * xmlData.append("<reviewedBy>"); //
					 * xmlData.append(vslInspc.getReviewedBy());
					 * xmlData.append("</reviewedBy>");
					 */

					xmlData.append("<flgIsLocked>");
					xmlData.append(vslInspc.getFlgIsLocked());
					xmlData.append("</flgIsLocked>");

					xmlData.append("<iShipId>");
					xmlData.append(vslInspc.getiShipId());
					xmlData.append("</iShipId>");

					xmlData.append("<iTenantId>");
					xmlData.append(vslInspc.getiTenantId());
					xmlData.append("</iTenantId>");

					xmlData.append("<updatedBy>");
					xmlData.append(CommonUtil.getUserId(context));
					xmlData.append("</updatedBy>");

					xmlData.append("<flgIsDeviceDirty>");
					xmlData.append(vslInspc.getFlgIsDeviceDirty());
					xmlData.append("</flgIsDeviceDirty>");

					xmlData.append("<flgIsEdited>");
					xmlData.append(vslInspc.getFlgIsEdited());
					xmlData.append("</flgIsEdited>");

					/*
					 * xmlData.append("<strExecutiveSummary>");
					 * xmlData.append(vslInspc.getStrExecutiveSummary());
					 * xmlData.append("</strExecutiveSummary>");
					 */

					xmlData.append("</table>");
				}
				xmlData.append("</updateVIData>");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form,";
				return null;
			}
		} else {
			Log.i("get form data ", "else");
			toastMsg = toastMsg + "No update present for send filled form,";
			return null;
		}
	}

	// ============Inner class for send FormImages data=====================

	class SendFormImagesDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private boolean isMassDataOn = false;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (checkFormImageUpdate) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_SEND_FORM_IMAGES_UNSUCCESS == true) {
					// startProgressdialog();
					IS_SEND_FORM_IMAGES_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			if (!CommonUtil.getMassStorageOn(context)) {
				xmlData = generateXmlDataForFormImages();
				// Log.i("generated xml data", xmlData);

				if (xmlData != null) {
					data = services.sendFormImagesData(params[0], params[1],
							xmlData);

					if (data != null)
						IS_SEND_FORM_IMAGES_RESPONSE_DATA = true;
					else
						IS_SEND_FORM_IMAGES_UNSUCCESS = true;
				}
			} else {
				isMassDataOn = true;
			}

			Log.i("data", "" + data + "do in background");
			Log.i("do in background of send filled form", "" + data);
			/*
			 * if (data != null) { return true; } return false;
			 */
			if (xmlData != null && !"".equals(xmlData))
				return true;
			else
				return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseSendFormImagesResponse(data);
				IS_SEND_FORM_IMAGES_SUCCESS = true;

			} else {

				if (isMassDataOn) {
					CommonUtil
							.launchPopUpError(
									"Web Synch failed as USB cable has been connected while Web Synch was in progress",
									context);
					// serviceProcessEnd();
					if (VessalInspectionHome.miActionProgressItem != null) {
						VessalInspectionHome.miActionProgressItem
								.setVisible(false);
						VessalInspectionHome.miActionCancelItem
								.setVisible(true);
						VessalInspectionHome.miActionDraftItem.setVisible(true);
						VessalInspectionHome.miActionDoneItem.setVisible(false);
					}
				} else {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_VI_DATA;
					SyncHandler.handler.sendMessage(message);
				}

			}
		}

	}

	/**
	 * @author ripunjay
	 * @return
	 */
	private String generateXmlDataForFormImages() {
		List<FilledFormImages> formList = new ArrayList<FilledFormImages>();
		DBManager dbManager = new DBManager(context);
		dbManager.open();
		List<FilledFormImages> list = dbManager.getFilledFormImagesData();
		dbManager.close();
		Intent i = new Intent(context, VesselInspectionStartActivity.class);
		L.fv("Entering in generateXmlDataForFormImages method.");
		int count = 0;
		if (list != null && list.size() > 0) {
			for (FilledFormImages formImg : list) {
				Log.i("formImg", formImg.toString());
				if (count >= 3)
					break;
				if (formImg.getFlgIsDirty() == 1
						&& formImg.getiShipId() == Integer.parseInt(CommonUtil
								.getShipId(context))) {
					formList.add(formImg);
					count = count + 1;
				}
			}

			if (formList != null && formList.size() > 0) {
				L.fv("generating xml data form form images.");
				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get formImg data ", "inside");
				Iterator<FilledFormImages> formIterator = formList.iterator();

				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<UpdateFormImages>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");

				while (formIterator.hasNext()) {
					FilledFormImages formImages = (FilledFormImages) formIterator
							.next();

					xmlData.append("<FormImages>");

					xmlData.append("<strformImageId>");
					if (formImages.getStrFilledFormImageId() != null) {
						xmlData.append(formImages.getStrFilledFormImageId());
					}
					xmlData.append("</strformImageId>");

					xmlData.append("<strFormId>");
					if (formImages.getiFilledFormId() != null) {
						xmlData.append(formImages.getiFilledFormId());
					}
					xmlData.append("</strFormId>");

					xmlData.append("<vesselInspectionId>");

					if (formImages.getiVesselInspectionId() != null) {
						xmlData.append(formImages.getiVesselInspectionId());
					}

					xmlData.append("</vesselInspectionId>");

					xmlData.append("<formSectionItemId>");
					if (formImages.getiFormSectionItemId() != null) {
						xmlData.append(formImages.getiFormSectionItemId());
					}
					xmlData.append("</formSectionItemId>");

					xmlData.append("<deviceOrininalPath>");
					if (formImages.getDeviceOriginalImagePath() != null) {
						xmlData.append(formImages.getDeviceOriginalImagePath());
					}
					xmlData.append("</deviceOrininalPath>");

					xmlData.append("<deviceRelativePath>");
					if (formImages.getDeviceRelativeImagePath() != null) {
						xmlData.append(formImages.getDeviceRelativeImagePath());
					}
					xmlData.append("</deviceRelativePath>");

					xmlData.append("<formImageData>");

					L.fv("StrformImagePath == : "
							+ formImages.getStrImagepath()
							+ "   form images absolute path : "
							+ formImages.getStrRelativeImagePath());

					// ================================
					String finalName = "";
					String shipId = String.valueOf(formImages.getiShipId());

					File storageDir = new File(
							Environment.getExternalStorageDirectory(), context
									.getResources().getString(
											R.string.imagePath)
									+ shipId
									+ "/"
									+ formImages.getiFilledFormId());
					// L.fv("new way : path " + storageDir.getAbsolutePath());
					if (storageDir.exists()) {
						String strFilename[] = formImages.getStrImagepath()
								.split("/");
						String fname = strFilename[strFilename.length - 1];
						// L.fv("image fname : " + fname);

						File[] imagesList = storageDir.listFiles();
						if (imagesList != null && imagesList.length > 0) {
							for (File file : imagesList) {
								if (file.getName().equalsIgnoreCase(fname)) {
									finalName = file.getName();
								}
							}
						}

						File file = new File(storageDir.getAbsolutePath() + "/"
								+ finalName);

						if (file.exists()) {
							// L.fv("file exist in location == : "+formImages.getStrformImagePath());
							String imageDataString = "";
							try {
								FileInputStream fis = new FileInputStream(file);
								byte[] imagedata = new byte[(int) file.length()];
								fis.read(imagedata);
								imageDataString = Base64.encode(imagedata);
							} catch (IOException ex) {
								ex.printStackTrace();
							}

							xmlData.append("<![CDATA[");
							xmlData.append(imageDataString);
							xmlData.append("]]>");

						}

					}

					xmlData.append("</formImageData>");

					xmlData.append("<flgDeleted>");
					xmlData.append(formImages.getFlgDeleted());
					xmlData.append("</flgDeleted>");

					xmlData.append("<strImageFileName>");

					if (formImages.getStrImageName() != null
							&& formImages.getStrImageName().equalsIgnoreCase(
									"Image")) {
						if (finalName != null) {
							xmlData.append(finalName);
						}
					} else {
						xmlData.append(formImages.getStrImageName());
					}

					xmlData.append("</strImageFileName>");

					xmlData.append("<strDescription>");
					if (formImages.getStrDesc() != null
							&& !"".equals(formImages.getStrDesc().trim())
							&& !"null".equals(formImages.getStrDesc().trim()))
						xmlData.append(CommonUtil.marshal(formImages
								.getStrDesc()));
					xmlData.append("</strDescription>");

					xmlData.append("<dtCreated>");
					if (formImages.getCreatedDate() != null
							&& !"null".equalsIgnoreCase(formImages
									.getCreatedDate())) {
						xmlData.append(formImages.getCreatedDate());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					if (formImages.getCreatedBy() != null) {

						xmlData.append(CommonUtil.getUserId(context));
					}
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (formImages.getModifiedDate() != null
							&& !"null".equalsIgnoreCase(formImages
									.getModifiedDate())) {
						xmlData.append(formImages.getModifiedDate());
					}
					xmlData.append("</dtUpdated>");

					xmlData.append("<sequence>");
					xmlData.append(formImages.getSequence());
					xmlData.append("</sequence>");

					xmlData.append("<flgStatus>");
					xmlData.append(formImages.getFlgStatus());
					xmlData.append("</flgStatus>");

					xmlData.append("<iFormSectionId>");
					xmlData.append(formImages.getiFormSectionId());
					xmlData.append("</iFormSectionId>");

					xmlData.append("</FormImages>");
				}
				xmlData.append("</UpdateFormImages>");
				L.fv("after generate xml data of form images.");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form images,";
				return null;
			}
		} else {
			Log.i("get form images data ", "else");
			toastMsg = toastMsg + "No update present for send form images,";
			return null;
		}

	}

	/**
	 * This method parse the xml response get after sending of send filled form.
	 * 
	 * @param data
	 */
	private void parseSendFormImagesResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			if (data != null && !"".equals(data)) {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				FilledFormImageparser formImageParser = new FilledFormImageparser();
				L.fv("parsering form image data is start");
				saxParser.parse(xmlStream, formImageParser);
				// Ripunjay:list Type Change
				List<FilledFormImages> list = formImageParser.getImageData();
				L.fv("After parsering form image data "
						+ (list != null ? list.size() : 0));
				Iterator<FilledFormImages> formImagesIterator = list.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (formImagesIterator.hasNext()) {
					FilledFormImages formImg = (FilledFormImages) formImagesIterator
							.next();
					if (formImg.getStrFilledFormImageId() != null
							&& !"".equals(formImg.getStrFilledFormImageId())) {
						L.fv(" form image id : "
								+ formImg.getStrFilledFormImageId());
						FilledFormImages formImgUpdt = new FilledFormImages();
						formImgUpdt = dbManager.getFilledFormImagesById(
								formImg.getStrFilledFormImageId()).get(0);
						formImgUpdt.setFlgIsDirty(0);
						result = dbManager
								.updateFilldFormImageTable(formImgUpdt);
						L.fv("after update form images flg dirty : " + result);
					}

				}

				list = new ArrayList<FilledFormImages>();
				list = dbManager.getDirtyFilledFormImagesData();
				dbManager.close();

				if (list != null && list.size() > 0) {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_SEND_FORM_IMAGES_SYNC;
					SyncHandler.handler.sendMessage(message);
				} else {

					IS_SEND_FORM_IMAGES_SUCCESS = true;
					SyncHandler.context = context;

					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_VI_DATA;
					SyncHandler.handler.sendMessage(message);

				}
			} else {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_SEND_FORM_IMAGES_SYNC;
				SyncHandler.handler.sendMessage(message);
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class GetFormImagesDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");

			WebServiceMethod services = new WebServiceMethod(context);

			if (data != null && !"".equals(data)) {
				IS_GET_FORM_IMAGES_RESPONSE_DATA = true;
			} else {
				IS_GET_FORM_IMAGES_UNSUCCESS = true;
			}

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				IS_GET_FORM_IMAGES_SUCCESS = true;
				parseGetFormImagesResponse(data);
			} else {
			}
		}

	}

	private String generateXmlForMatchManualSync(
			List<SyncHistory> syncHistoryList) {
		StringBuffer result = new StringBuffer();
		StringBuffer syncInfo = new StringBuffer();

		result.append("<MatchManualSync>");

		result.append("<shipId>");
		result.append(CommonUtil.getShipId(context));
		result.append("</shipId>");
		result.append("<macId>");
		result.append(CommonUtil.getIMEI(context));
		result.append("</macId>");
		result.append("<templateVersion>");
		DBManager dbManager = new DBManager(context);
		dbManager.open();
		result.append(dbManager.getTemplateVersionNo());
		dbManager.close();
		result.append("</templateVersion>");

		if (syncHistoryList != null && syncHistoryList.size() > 0) {
			if (syncHistoryList.size() == 2) {
				for (SyncHistory sh : syncHistoryList) {
					syncInfo.append(sh.getSyncOrderid() + "_"
							+ sh.getStrSyncMode() + "#");
				}
				syncInfo = syncInfo.replace(syncInfo.length() - 1,
						syncInfo.length() - 1, "");
			} else {
				String strMode = "";
				for (SyncHistory sh : syncHistoryList) {
					strMode = sh.getStrSyncMode();
					syncInfo.append(sh.getSyncOrderid() + "_"
							+ sh.getStrSyncMode());
				}

			}
		} else {
			syncInfo.append("0_ship#0_shore");
		}

		result.append("<acknowledgeDate>");
		result.append("");
		result.append("</acknowledgeDate>");
		result.append("<generatedDate>");
		result.append("");
		result.append("</generatedDate>");
		result.append("<syncDate>");
		result.append("");
		result.append("</syncDate>");
		result.append("<syncInfo>");
		result.append(syncInfo);
		result.append("</syncInfo>");

		result.append("<versionNo>");
		PackageInfo pInfo = null;
		String version = "";
		try {
			pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (pInfo != null && pInfo.versionName != null) {
			version = pInfo.versionName;
		} else {
			version = "2.0";
		}
		result.append(version);
		result.append("</versionNo>");

		result.append("<iTenantId>");
		result.append(CommonUtil.getTenantId(context));
		result.append("</iTenantId>");

		result.append("</MatchManualSync>");

		return result.toString();
	}

	/**
	 * This method parse the xml response get after getFormImagesData.
	 * 
	 * @param data
	 */
	private void parseGetFormImagesResponse(String data) {

		Log.i("Stremdata", "" + data);
		try {

			/**
			 * Ripunjay : comment for writing insertion or updation logic for
			 * filed form.
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author Ripunjay Shukla This class is used to create acknowledgment for
	 *         filledform in background.
	 * 
	 */
	class SendAckForFormImages extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			StringBuffer formIdBuffer;
			try {
				formIdBuffer = formBuff;
			} catch (Exception e) {

				formIdBuffer = new StringBuffer();
			}

			int hasMore = -1;
			String flag = params[1];
			try {
				hasMore = 0;// Integer.parseInt(params[0]);
			} catch (Exception e) {
				hasMore = -1;
			}
			Log.i("form id ", "Ripunjay " + formIdBuffer.toString());
			Log.i("mac id ", "" + CommonUtil.getIMEI(context));
			if (formIdBuffer.toString().trim().length() > 0) {
				data = services.sendAckFormImages(CommonUtil.getIMEI(context),
						formIdBuffer.toString().trim(), hasMore);
			}

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				// sendAck(data);
			}

		}

	}

	/**
	 * Get master data
	 * 
	 * @author admin
	 * 
	 */
	class GetCheckListSectionDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetCheckListSectionDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_CHECKLIST_SECTION_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_CHECKLIST_SECTION_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getCheckListSectionData(macId, tenantId, 0);

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetCheckListSectionResponse(data);

				SyncHandler.context = context;
				Message checkListSectionItemmessage = new Message();
				checkListSectionItemmessage.what = SyncHandler.MSG_UPDATE_CHECKLIST_SECTION_ITEM_DATA;
				SyncHandler.handler.sendMessage(checkListSectionItemmessage);

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);

			}

		}

	}

	/**
	 * Get master data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetCheckListSectionItemDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetCheckListSectionItemDataTask(String macId, String tenantId,
				String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_CHECKLIST_SECTION_ITEM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_CHECKLIST_SECTION_ITEM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			data = services.getCheckListSectionItemData(macId, tenantId, 0);
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_CHECKLIST_SECTION_ITEM_RESPONSE_DATA = true;
			else
				IS_GET_CHECKLIST_SECTION_ITEM_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetCheckListSectionItemResponse(data);
				IS_GET_CHECKLIST_SECTION_ITEM_SUCCESS = true;
				if (IS_GET_CHECKLIST_SECTION_ITEM_SUCCESS) {

					Message formCategoryMessage = new Message();
					formCategoryMessage.what = SyncHandler.MSG_UPDATE_FORM_CATEGORY;
					SyncHandler.handler.sendMessage(formCategoryMessage);

				}

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}
	}

	/**
	 * Get master data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetFormSectionDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetFormSectionDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_FORM_SECTION_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_FORM_SECTION_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getFormSectionData(macId, tenantId, 0);
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_FORM_SECTION_RESPONSE_DATA = true;
			else
				IS_GET_FORM_SECTION_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetFormSectionResponse(data);
				IS_GET_FORM_SECTION_SUCCESS = true;

				if (IS_GET_FORM_SECTION_SUCCESS) {
					Message formSectionItemmessage = new Message();
					formSectionItemmessage.what = SyncHandler.MSG_UPDATE_FORM_SECTION_ITEM;
					SyncHandler.handler.sendMessage(formSectionItemmessage);
				}

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	/**
	 * Get master data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetFormSectionItemDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetFormSectionItemDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_FORM_SECTION_ITEM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_FORM_SECTION_ITEM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getFormSectionItemData(macId, tenantId, 0);
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_FORM_SECTION_ITEM_RESPONSE_DATA = true;
			else
				IS_GET_FORM_SECTION_ITEM_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetFormSectionItemResponse(data);
				IS_GET_FORM_SECTION_ITEM_SUCCESS = true;

				if (IS_GET_FORM_SECTION_ITEM_SUCCESS) {

					updateSyncStatusTable();

					SyncHandler.context = context;
					Message message = new Message();
					SyncHandler.buildToast(toastMsg);
					message.what = SyncHandler.MSG_GET_FORM_SECTION_DYNAMIC_ITEM;
					SyncHandler.handler.sendMessage(message);

				}

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	/**
	 * Get master data ShipFormSectionItem
	 * 
	 * @author Ripunjay.S
	 * 
	 */
	class GetShipFormSectionItemDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetShipFormSectionItemDataTask(String macId, String tenantId,
				String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_SHIP_FORM_SECTION_ITEM_UNSUCCESS == true) {
					// startProgressdialog();
					IS_GET_SHIP_FORM_SECTION_ITEM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getShipFormSectionItemData(macId, tenantId, shipId,
					0);
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_SHIP_FORM_SECTION_ITEM_RESPONSE_DATA = true;
			else
				IS_GET_SHIP_FORM_SECTION_ITEM_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetShipFormSectionItemResponse(data);
				IS_GET_SHIP_FORM_SECTION_ITEM_SUCCESS = true;
				if (IS_GET_SHIP_FORM_SECTION_ITEM_SUCCESS) {

					updateSyncStatusTable();

					SyncHandler.context = context;
					Message message = new Message();
					SyncHandler.buildToast(toastMsg);
					message.what = SyncHandler.MSG_UPDATE_VI_DATA;
					SyncHandler.handler.sendMessage(message);

				}

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	private void updateSyncStatusTable() {
		try {
			DBManager db = new DBManager(context);
			SyncStatus syncStatus = null;
			db.open();
			// DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dtFormat = new SimpleDateFormat("d MMM yyyy");
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			if (syncStatusList != null && syncStatusList.size() > 0) {
				syncStatus = new SyncStatus();
				syncStatus = syncStatusList.get(0);
				syncStatus.setDtSyncDate(dtFormat.format(new Date()));
				db.updateSyncStatusTable(syncStatus);
			} else {
				syncStatus = new SyncStatus("123" + new Date().getTime(),
						dtFormat.format(new Date()),
						CommonUtil.getDataAccessMode(context),
						CommonUtil.getServerAddress(context),
						Integer.parseInt(CommonUtil.getTenantId(context)), 0,
						0, 1, dtFormat.format(new Date()),
						dtFormat.format(new Date()),
						CommonUtil.getUserId(context),
						CommonUtil.getUserId(context),
						CommonUtil.getDataSyncModeFromDb(context));

			}
			if (AdminTab.syncDate != null && !"".equals(AdminTab.syncDate))
				AdminTab.syncDate.setText("Last Sync Date : "
						+ dtFormat.format(new Date()));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Get updated vi data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetUpdatedVIDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;
		private String userId;

		GetUpdatedVIDataTask(String macId, String tenantId, String userId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
			this.userId = userId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_VESSEL_INSPECTION_SUCCESS == true
						&& IS_UPDATE_VESSEL_INSPECTION_UNSUCCESS == true) {
					// startProgressdialog();
					IS_UPDATE_VESSEL_INSPECTION_UNSUCCESS = false;
				}
			}
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			WebServiceMethod services = new WebServiceMethod(context);

			String version = "";
			try {
				PackageInfo pInfo = context.getPackageManager().getPackageInfo(
						context.getPackageName(), 0);
				if (pInfo != null && pInfo.versionName != null) {
					version = pInfo.versionName;
				} else {
					version = "2.0";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			data = services.getUpdatedVIDataByVersion(
					Integer.parseInt(tenantId), userId,
					CommonUtil.getUserLogin(context), version);
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_UPDATE_VESSEL_INSPECTION_RESPONSE_DATA = true;
			else
				IS_UPDATE_VESSEL_INSPECTION_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			} else
				return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				
				syncProcessEnd = false;
				parseUpdatedVIDataResponse(data);
				IS_UPDATE_VESSEL_INSPECTION_SUCCESS = true;
				if (!syncProcessEnd) {

					if (CommonUtil.getUserLogin(context) != null
							&& "1".equals(CommonUtil.getUserLogin(context))) {
						// serviceProcessEnd();

						String viId = getDraftedVIId();
						if (viId != null && !"".equals(viId)) {
							Message formSectionMessage = new Message();
							formSectionMessage.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM;
							SyncHandler.handler.sendMessage(formSectionMessage);
						} else {
							Message message = new Message();
							message.what = SyncHandler.MSG_GET_ROLE_TEMPLATE;
							SyncHandler.handler.sendMessage(message);
						}

					} else if (CommonUtil.getSynckFrom(context) != null
							&& "admin".equals(CommonUtil.getSynckFrom(context))) {
						// serviceProcessEnd();
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_ROLE_TEMPLATE;
						SyncHandler.handler.sendMessage(message);
					} else {
						Message formSectionMessage = new Message();
						formSectionMessage.what = SyncHandler.MSG_UPDATE_VI_FILLED_FORM;
						SyncHandler.handler.sendMessage(formSectionMessage);
					}
				} else {

					launchPopUpForActivateInspectionNotAvailableOnServer(
							context.getResources().getString(
									R.string.activatingInspectionNotOnServer),
							0);
				}

			} else {
				/*
				 * if (CommonUtil.getUserLogin(context) != null &&
				 * "1".equals(CommonUtil.getUserLogin(context)))
				 * serviceProcessEnd(); else { Message formSectionMessage = new
				 * Message(); formSectionMessage.what =
				 * SyncHandler.MSG_UPDATE_VI_FILLED_FORM;
				 * SyncHandler.handler.sendMessage(formSectionMessage); }
				 * 
				 * if (CommonUtil.getLastActiveShip(context) != null &&
				 * !"".equals(CommonUtil.getLastActiveShip(context))) {
				 * CommonUtil.setShipID(context,
				 * CommonUtil.getLastActiveShip(context));
				 * CommonUtil.setLastActiveShip(context, null); }
				 */

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	/**
	 * Get All Ship from a tenant
	 * 
	 * @author pushkar.m
	 */
	class GetFormCategoryTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetFormCategoryTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_FORM_CATEGORY_SUCCESS == true) {
					// startProgressdialog();
					IS_GET_FORM_CATEGORY_SUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getFormCategory(Integer.parseInt(params[0]));
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_FORM_CATEGORY_RESPONSE_DATA = true;
			else
				IS_GET_FORM_CATEGORY_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetFormCategoryResponse(data);
				IS_GET_FORM_CATEGORY_SUCCESS = true;

				if (IS_GET_FORM_CATEGORY_SUCCESS) {
					Message formSectionMessage = new Message();
					formSectionMessage.what = SyncHandler.MSG_UPDATE_FORM_SECTION;
					SyncHandler.handler.sendMessage(formSectionMessage);
				}

			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	/**
	 * Get All Ship from a tenant
	 * 
	 * @author pushkar.m
	 */
	class GetAllShipTask extends AsyncTask<String, Integer, Boolean> {

		private String data;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_UPDATE_CONTENT_SUCCESS == true
						&& IS_GET_ALL_SHIPS_SUCCESS == true) {
					// startProgressdialog();
					IS_GET_ALL_SHIPS_SUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getAllShip(CommonUtil.getUserId(context),
					Integer.parseInt(params[0]));
			// Log.i("generated xml data", xmlData);
			if (data != null && !"".equals(data))
				IS_GET_ALL_SHIPS_RESPONSE_DATA = true;
			else
				IS_GET_ALL_SHIPS_UNSUCCESS = true;

			Log.i("data", "" + data + "do in background");

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetAllShipResponse(data);
				IS_GET_ALL_SHIPS_SUCCESS = true;

				SyncHandler.context = context;
				Message checkListSectionmessage = new Message();
				checkListSectionmessage.what = SyncHandler.MSG_UPDATE_CHECKLISTSECTION_DATA;
				SyncHandler.handler.sendMessage(checkListSectionmessage);

				// if(TestActivity.syncStatus == true)
				// cancelProgressdialog();
			} else {
				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}
		}

	}

	/**
	 * Get All Ship from a tenant
	 * 
	 * @author pushkar.m
	 */
	class AckUpdatedViTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private int shipId;

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {

			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			services.ackUpdatedVIData(params[0], params[1]);

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

			} else {

			}
		}

	}

	class GetVITemplateVersionTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private int shipId;
		private int tenantId;
		private String macId;

		public GetVITemplateVersionTask() {
			shipId = ((CommonUtil.getShipId(context) != null && !""
					.equals(CommonUtil.getShipId(context))) ? Integer
					.parseInt(CommonUtil.getShipId(context)) : 0);
			tenantId = Integer.parseInt(CommonUtil.getTenantId(context));
			macId = CommonUtil.getMacId(context);
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {

			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getVITemplateVersion(shipId, macId, tenantId);

			if (data != null && !"".equals(data)) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetViTemplateVersionData(data);

				serviceProcessEnd();
				CommonUtil.setDropDbFlag(context, "0");
				if (CommonUtil.getLastActiveShip(context) != null
						&& !"".equals(CommonUtil.getLastActiveShip(context))) {
					CommonUtil.setShipID(context,
							CommonUtil.getLastActiveShip(context));
					CommonUtil.setLastActiveShip(context, null);
				}
			} else {

				launchPopUpForNoSync(
						context.getResources().getString(
								R.string.syncNotPossibleDueToCommError), 10);
			}

		}

	}

	/**
	 * This method parse the xml response get after parsing of get ship form
	 * section item.
	 * 
	 * @param data
	 */
	private void parseGetShipFormSectionItemResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			ShipFormSectionItemParser shipFormSectionItemParser = new ShipFormSectionItemParser();

			saxParser.parse(xmlStream, shipFormSectionItemParser);

			List<ShipFormSectionItem> list = shipFormSectionItemParser
					.getFormSectionItemData();

			Iterator<ShipFormSectionItem> shipFormSectionItemIterator = list
					.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (shipFormSectionItemIterator.hasNext()) {
				ShipFormSectionItem sfsi = (ShipFormSectionItem) shipFormSectionItemIterator
						.next();
				Log.i("test", sfsi.getiShipFormSectionItemId());

				List<ShipFormSectionItem> sFSIDbList = dbManager
						.getShipFormSectionItemDataById(sfsi
								.getiShipFormSectionItemId());
				if (sFSIDbList != null && sFSIDbList.size() > 0) {

					result = dbManager.updateShipFormSectionItemTable(sfsi);
				} else {

					result = dbManager.insertShipFormSectionItemTable(sfsi);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_SHIP_FORM_SECTION_ITEM_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m
	 * @param data
	 */
	private void parseGetFormCategoryResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			FormCategoryParser formCategoryParser = new FormCategoryParser();

			saxParser.parse(xmlStream, formCategoryParser);
			// Ripunjay:list Type Change
			List<FormCategory> list = formCategoryParser.getFormCategoryData();

			Iterator<FormCategory> formCategoryIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (formCategoryIterator.hasNext()) {
				FormCategory formCategory = (FormCategory) formCategoryIterator
						.next();
				Log.i("test", String.valueOf(formCategory.getiFormCategoryId()));

				List<FormCategory> checkDbList = dbManager
						.getFormCategoryDataById(String.valueOf(formCategory
								.getiFormCategoryId()));
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateFormCategoryTable(formCategory);
				} else {

					result = dbManager.insertFormCategoryTable(formCategory);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_CHECKLIST_SECTION_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get from getAllShip service.
	 * 
	 * @author pushkar.m
	 * @param data
	 */
	private void parseGetAllShipResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			ShipMasterParsar shipMasterParsar = new ShipMasterParsar();

			saxParser.parse(xmlStream, shipMasterParsar);
			// Ripunjay:list Type Change
			List<ShipMaster> list = shipMasterParsar.getShipMasterData();

			Iterator<ShipMaster> ShipMasterIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (ShipMasterIterator.hasNext()) {
				ShipMaster shipMaster = (ShipMaster) ShipMasterIterator.next();
				Log.i("test", String.valueOf(shipMaster.getiShipId()));

				List<ShipMaster> checkDbList = dbManager
						.getShipMasterDataById(String.valueOf(shipMaster
								.getiShipId()));
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateShipMasterTable(shipMaster);
				} else {

					result = dbManager.insertShipMasterTable(shipMaster);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_CHECKLIST_SECTION_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after sending of send filled form.
	 * 
	 * @param data
	 */
	private void parseGetCheckListSectionResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			CheckListSectionParser checkListSectionParsar = new CheckListSectionParser();

			saxParser.parse(xmlStream, checkListSectionParsar);
			// Ripunjay:list Type Change
			List<CheckListSection> list = checkListSectionParsar
					.getCheckListSectionData();

			Iterator<CheckListSection> chkListSectionIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (chkListSectionIterator.hasNext()) {
				CheckListSection cls = (CheckListSection) chkListSectionIterator
						.next();
				cls.setFlgIsDirty(0);
				Log.i("test", cls.getiCheckListSectionId());

				List<CheckListSection> checkDbList = dbManager
						.getCheckListSectionDataById(cls
								.getiCheckListSectionId());
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateCheckListSectionData(cls);
				} else {

					result = dbManager.insertCheckListSectionTable(cls);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_CHECKLIST_SECTION_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after sending of send
	 * checklistsectionitem.
	 * 
	 * @param data
	 */
	private void parseGetCheckListSectionItemResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			CheckListSectionItemsParser checkListSectionItemParsar = new CheckListSectionItemsParser();

			saxParser.parse(xmlStream, checkListSectionItemParsar);
			// Ripunjay:list Type Change
			List<CheckListSectionItems> list = checkListSectionItemParsar
					.getCheckListSectionItemsData();

			Iterator<CheckListSectionItems> chkListSectionItemIterator = list
					.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (chkListSectionItemIterator.hasNext()) {
				CheckListSectionItems clsi = (CheckListSectionItems) chkListSectionItemIterator
						.next();
				Log.i("test", clsi.getiCheckListSectionItemsId());

				List<CheckListSectionItems> checkDbList = dbManager
						.getCheckListSectionItemsDataById(clsi
								.getiCheckListSectionItemsId());
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateCheckListSectionItemTable(clsi);
				} else {

					result = dbManager.insertCheckListSectionItemTable(clsi);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_CHECKLIST_SECTION_ITEM_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after sending of send form
	 * section.
	 * 
	 * @param data
	 */
	private void parseGetFormSectionResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			FormSectionParsar formSectionParsar = new FormSectionParsar();

			saxParser.parse(xmlStream, formSectionParsar);

			List<FormSection> list = formSectionParsar.getFormSectionData();

			Iterator<FormSection> formSectionIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (formSectionIterator.hasNext()) {
				FormSection fs = (FormSection) formSectionIterator.next();
				Log.i("test", fs.getiFormSectionId());

				List<FormSection> checkDbList = dbManager
						.getFormSectionDataById(fs.getiFormSectionId());
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateFormSectionTable(fs);
				} else {

					result = dbManager.insertFormSectionTable(fs);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_FORM_SECTION_SUCCESS = true;
			/*
			 * SyncHandler.context = context; Message message = new Message();
			 * message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
			 * SyncHandler.handler.sendMessage(message);
			 */

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after sending of send form section
	 * item.
	 * 
	 * @param data
	 */
	private void parseGetFormSectionItemResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			FormSectionItemParsar formSectionItemParsar = new FormSectionItemParsar();

			saxParser.parse(xmlStream, formSectionItemParsar);
			// Ripunjay:list Type Change
			List<FormSectionItem> list = formSectionItemParsar
					.getFormSectionItemData();

			Iterator<FormSectionItem> formSectionItemIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;

			while (formSectionItemIterator.hasNext()) {
				FormSectionItem fsi = (FormSectionItem) formSectionItemIterator
						.next();
				Log.i("test", fsi.getiFormSectionItemId());

				List<FormSectionItem> checkDbList = dbManager
						.getFormSectionItemDataById(fsi.getiFormSectionItemId());
				if (checkDbList != null && checkDbList.size() > 0) {

					result = dbManager.updateFormSectionItemTable(fsi);
				} else {

					result = dbManager.insertFormSectionItemTable(fsi);
				}

			}
			Log.i("result ", "" + result);
			dbManager.close();

			SyncHandler.context = context;
			IS_GET_FORM_SECTION_ITEM_SUCCESS = true;
			/*
			 * SyncHandler.context = context; Message message = new Message();
			 * message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
			 * SyncHandler.handler.sendMessage(message);
			 */

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m This method parse the xml response get after sending of
	 *         send form section item.
	 * 
	 * @param data
	 */
	private void parseUpdatedVIDataResponse(String data) {
		Log.i("Stremdata", "" + data);
		List<String> idListForAck = new ArrayList<String>();
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			VesselInspectionParsar viParsar = new VesselInspectionParsar();

			saxParser.parse(xmlStream, viParsar);
			// Ripunjay:list Type Change
			List<VesselInspection> list = viParsar.getVesselInspectionData();

			boolean isActivatingInspectionAvailable = false;
			String activatingInspectionId = CommonUtil
					.getActiveInspectionID(context) != null
					&& !"".equals(CommonUtil.getActiveInspectionID(context)) ? CommonUtil
					.getActiveInspectionID(context) : "";

			Iterator<VesselInspection> viIterator = list.iterator();
			DBManager dbManager = new DBManager(context);
			dbManager.open();
			long result = 0;
			int shipId = 0;

			/**
			 * flgStatus 0 drafted 1,2 blank. 3 sent for review. 4 complete.
			 */
			while (viIterator.hasNext()) {
				VesselInspection vi = (VesselInspection) viIterator.next();

				if (vi.getiVesselInspectionId().equals(activatingInspectionId)) {
					isActivatingInspectionAvailable = true;
				}

				List<VesselInspection> viList = dbManager
						.getVesselInspectionDataById(vi
								.getiVesselInspectionId());

				if (viList != null && viList.size() > 0) {

					VesselInspection vIns = viList.get(0);
					vIns.setiVesselInspectionStatusId(vi
							.getiVesselInspectionStatusId());

					if (vIns.getFlgStatus() >= 0) {
						if (vIns.getFlgStatus() > 2 && vi.getFlgStatus() < 2) {
							vIns.setFlgStatus(-1);
						} else {
							if(vi.getFlgStatus() == -1){
								vIns.setFlgStatus(0);
							}
							else{
								vIns.setFlgStatus(vi.getFlgStatus());
							}							
						}

					} else if (vi.getFlgStatus() > 2) {
						vIns.setFlgStatus(vi.getFlgStatus());
					}

					vIns.setFlgDeleted(vi.getFlgDeleted());
					vIns.setFlgIsLocked(vi.getFlgIsLocked());
					shipId = vIns.getiShipId();

					/**
					 * For sync with server flag.
					 */
					vIns.setFlgIsEdited(5);
					if (vIns.getFlgStatus() < 4) {
						result = dbManager.updateVesselInspectionTable(vIns);
					} else {
						dbManager.deleteVesselInspection(vIns
								.getiVesselInspectionId());
						idListForAck.add(vIns.getiVesselInspectionId());
					}
				} else {
					vi.setFlgIsDirty(0);
					/**
					 * For sync with server flag.
					 */
					vi.setFlgIsEdited(5);
					if (vi.getFlgStatus() < 3)
						vi.setFlgStatus(-1);
					dbManager.insertVesselInspectionTable(vi);
				}

			}

			// Acknowleding deleted vesselInspection
			if (idListForAck != null && idListForAck.size() > 0) {
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<updateVIData>");
				xmlData.append("<shipId>");
				xmlData.append(String.valueOf(shipId));
				xmlData.append("</shipId>");
				xmlData.append("<VesselInspection>");

				for (String strId : idListForAck) {

					xmlData.append("<iVesselInspectionId>");
					xmlData.append(strId);
					xmlData.append("</iVesselInspectionId>");
				}
				xmlData.append("</VesselInspection>");
				xmlData.append("</updateVIData>");

				AckUpdatedViTask ackTask = new AckUpdatedViTask();
				ackTask.execute(xmlData.toString(),
						CommonUtil.getShipId(context));

			}
			Log.i("result ", "" + result);
			dbManager.close();

			if (CommonUtil.getCallingThrough(context) != null
					&& "activate".equals(CommonUtil.getCallingThrough(context))) {
				if (!isActivatingInspectionAvailable)
					this.syncProcessEnd = true;
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void parseSendFilledFormDescResponse(String xmlData) {

		try {
			if (xmlData != null && !"".equals(xmlData)) {
				InputStream xmlStream = new ByteArrayInputStream(
						xmlData.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VesselInspectionFormListDescParser formParser = new VesselInspectionFormListDescParser();

				saxParser.parse(xmlStream, formParser);
				//
				List<VesselInspectionFormListDesc> descList = formParser
						.getVesselInspectionFormListDescData();

				Iterator<VesselInspectionFormListDesc> formDescIterator = descList
						.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (formDescIterator.hasNext()) {
					VesselInspectionFormListDesc formDesc = (VesselInspectionFormListDesc) formDescIterator
							.next();

					result = dbManager.updateDirtyFlagFormListDescTable(
							formDesc.getiVesselInspectionFormListDescId(), 0);

				}

				List<VesselInspectionFormListDesc> descDirtyList = null;
				String viId = getDraftedVIId();

				if (viId.contains(",")) {

					descDirtyList = dbManager
							.getVIFormListDescForMultipleVIIds(viId);

				} else {

					descDirtyList = dbManager.getVIFormListDescByVIId(viId
							.replace("'", ""));

				}
				dbManager.close();

				if (descDirtyList != null && descDirtyList.size() > 0) {
					SyncHandler.context = context;
					Message message = new Message();

					message.what = SyncHandler.MSG_UPDATE_FILLED_FORM_DESC;
					SyncHandler.handler.sendMessage(message);
				} else {
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_UPDATE_FILLED_CHECKLIST_SYNC;
					SyncHandler.handler.sendMessage(message);
				}

			} else {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_UPDATE_FILLED_FORM_DESC;
				SyncHandler.handler.sendMessage(message);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void parseSendFilledFormFooterResponse(String xmlData) {

		try {
			if (xmlData != null && !"".equals(xmlData)) {
				InputStream xmlStream = new ByteArrayInputStream(
						xmlData.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VesselInspectionFormListFooterParser formParser = new VesselInspectionFormListFooterParser();

				saxParser.parse(xmlStream, formParser);
				//
				List<VesselInspectionFormListFooter> footerList = formParser
						.getVesselInspectionFormListFooterData();

				Iterator<VesselInspectionFormListFooter> formFooterIterator = footerList
						.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (formFooterIterator.hasNext()) {
					VesselInspectionFormListFooter formFooter = (VesselInspectionFormListFooter) formFooterIterator
							.next();

					result = dbManager.updateDirtyFlagFormListFooterTable(
							formFooter.getiVesselInspectionFormListFooterId(),
							0);

				}

			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void parseGetViTemplateVersionData(String xmlData) {

		try {
			if (xmlData != null && !"".equals(xmlData)) {
				InputStream xmlStream = new ByteArrayInputStream(
						xmlData.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VITemplateVersionParser tvParser = new VITemplateVersionParser();

				saxParser.parse(xmlStream, tvParser);
				//
				List<VITemplateVersion> tvList = tvParser
						.getTempalteVersionData();

				Iterator<VITemplateVersion> tvIterator = tvList.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				while (tvIterator.hasNext()) {
					VITemplateVersion vitv = (VITemplateVersion) tvIterator
							.next();

					List<VITemplateVersion> dbTempVerList = dbManager
							.getVITemplateVersionById(vitv
									.getiVitemplateVersionId());

					if (dbTempVerList != null && dbTempVerList.size() > 0) {

						result = dbManager.updateViTemplateversionTable(vitv);

					} else {

						result = dbManager.insertViTemplateversionTable(vitv);

					}

				}

			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String parseGetFilledFormDescResponse(String xmlData) {

		StringBuffer ackIds = new StringBuffer();
		VesselInspectionFormListDescParser formDescParser = null;

		try {
			if (xmlData != null && !"".equals(xmlData)) {
				InputStream xmlStream = new ByteArrayInputStream(
						xmlData.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();

				formDescParser = new VesselInspectionFormListDescParser();

				saxParser.parse(xmlStream, formDescParser);

				List<VesselInspectionFormListDesc> descList = formDescParser
						.getVesselInspectionFormListDescData();

				Iterator<VesselInspectionFormListDesc> formDescIterator = descList
						.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				VesselInspectionFormListDesc viflDesc = null;
				while (formDescIterator.hasNext()) {
					VesselInspectionFormListDesc formDesc = (VesselInspectionFormListDesc) formDescIterator
							.next();

					List<VesselInspectionFormListDesc> descListFromDB = dbManager
							.getVIFormListDescById(formDesc
									.getiVesselInspectionFormListDescId());

					if (descListFromDB != null && descListFromDB.size() > 0) {

						viflDesc = descListFromDB.get(0);
						viflDesc.setStrDesc(formDesc.getStrDesc());
						viflDesc.setFlgIsDirty(0);
						viflDesc.setFlgStatus(formDesc.getFlgStatus());
						dbManager.updateVIFLDesc(viflDesc);

					} else {

						viflDesc = new VesselInspectionFormListDesc();
						viflDesc.setiVesselInspectionFormListDescId(formDesc
								.getiVesselInspectionFormListDescId());
						viflDesc.setiVesselInspectionId(formDesc
								.getiVesselInspectionId());
						viflDesc.setiFilledFormId(formDesc.getiFilledFormId());
						viflDesc.setiFormSectionId(formDesc.getiFormSectionId());
						viflDesc.setiFormSectionItemId(formDesc
								.getiFormSectionItemId());
						viflDesc.setCreatedBy(formDesc.getCreatedBy());
						viflDesc.setCreatedDate(formDesc.getCreatedDate());
						viflDesc.setFlgChecked(formDesc.getFlgChecked());
						viflDesc.setFlgDeleted(formDesc.getFlgDeleted());
						viflDesc.setFlgIsDeviceDirty(0);
						viflDesc.setFlgIsDirty(0);
						viflDesc.setFlgIsEdited(formDesc.getFlgIsEdited());
						viflDesc.setFlgIsHeaderEdited(formDesc
								.getFlgIsHeaderEdited());
						viflDesc.setFlgStatus(formDesc.getFlgStatus());
						viflDesc.setiShipId(formDesc.getiShipId());
						viflDesc.setiTenantId(formDesc.getiTenantId());
						viflDesc.setSequence(formDesc.getSequence());
						viflDesc.setModifiedBy(formDesc.getModifiedBy());
						viflDesc.setModifiedDate(formDesc.getModifiedDate());
						// viflDesc.setStrHeaderDesc(formHeader.getStrHeaderDesc());
						viflDesc.setStrDesc(formDesc.getStrDesc());
						viflDesc.setStrDescType(formDesc.getStrDescType());

						dbManager.insertVIFLDesc(viflDesc);

					}

					ackIds.append("'"
							+ viflDesc.getiVesselInspectionFormListDescId()
							+ "',");
					viflDesc = null;

				}
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (ackIds.length() > 0) {
			if (formDescParser != null && formDescParser.getHasMore() > 0)
				return ackIds.substring(0, ackIds.length() - 1) + "hasMore";
			else
				return ackIds.substring(0, ackIds.length() - 1);
		} else
			return ackIds.toString();
	}

	public String parseGetFilledFormFooterResponse(String xmlData) {

		StringBuffer ackIds = new StringBuffer();
		try {
			if (xmlData != null && !"".equals(xmlData)) {
				InputStream xmlStream = new ByteArrayInputStream(
						xmlData.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				VesselInspectionFormListFooterParser formParser = new VesselInspectionFormListFooterParser();

				saxParser.parse(xmlStream, formParser);
				//
				List<VesselInspectionFormListFooter> footerList = formParser
						.getVesselInspectionFormListFooterData();

				Iterator<VesselInspectionFormListFooter> formFooterIterator = footerList
						.iterator();
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				long result = 0;
				String formid = "";
				VesselInspectionFormListFooter viflFooter = null;
				while (formFooterIterator.hasNext()) {
					VesselInspectionFormListFooter formFooter = (VesselInspectionFormListFooter) formFooterIterator
							.next();

					List<VesselInspectionFormListFooter> footerListFromDB = dbManager
							.getVIFLFooterForSectionItem(
									formFooter.getiVesselInspectionId(),
									formFooter.getiFilledFormId(),
									formFooter.getiFormSectionItemId());
					if (footerListFromDB != null && footerListFromDB.size() > 0) {

						viflFooter = footerListFromDB.get(0);
						viflFooter.setStrFooterDesc(formFooter
								.getStrFooterDesc());
						dbManager.updateVIFLFooter(viflFooter);

					} else {

						viflFooter = new VesselInspectionFormListFooter();
						viflFooter
								.setiVesselInspectionFormListFooterId(getPk());
						viflFooter.setiFilledFormId(formFooter
								.getiFilledFormId());
						viflFooter.setiFormSectionId(formFooter
								.getiFormSectionId());
						viflFooter.setiFormSectionItemId(formFooter
								.getiFormSectionItemId());
						viflFooter.setCreatedBy(formFooter.getCreatedBy());
						viflFooter.setCreatedDate(formFooter.getCreatedDate());
						viflFooter.setFlgChecked(formFooter.getFlgChecked());
						viflFooter.setFlgDeleted(formFooter.getFlgDeleted());
						viflFooter.setFlgIsDeviceDirty(formFooter
								.getFlgIsDeviceDirty());
						viflFooter.setFlgIsDirty(formFooter.getFlgIsDirty());
						viflFooter.setFlgIsEdited(formFooter.getFlgIsEdited());
						viflFooter.setFlgIsFooterEdited(formFooter
								.getFlgIsFooterEdited());
						viflFooter.setFlgStatus(formFooter.getFlgStatus());
						viflFooter.setiShipId(formFooter.getiShipId());
						viflFooter.setiTenantId(formFooter.getiTenantId());
						viflFooter.setSequence(formFooter.getSequence());
						viflFooter.setModifiedBy(formFooter.getModifiedBy());
						viflFooter
								.setModifiedDate(formFooter.getModifiedDate());
						viflFooter.setStrFooterDesc(formFooter
								.getStrFooterDesc());

						dbManager.insertVIFLFooter(viflFooter);

					}

					result = dbManager.updateDirtyFlagFormListFooterTable(
							formFooter.getiVesselInspectionFormListFooterId(),
							0);

					ackIds.append("'"
							+ viflFooter.getiVesselInspectionFormListFooterId()
							+ "',");

					viflFooter = null;

				}
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (ackIds.indexOf(",") > 0)
			return ackIds.toString().substring(0, ackIds.length() - 1);
		else
			return ackIds.toString();
	}

	public String genrateXmlDataForFilledFormDesc() {
		StringBuffer xmlData = new StringBuffer();

		String viId = getDraftedVIId();
		DBManager dbManager = new DBManager(context);
		dbManager.open();

		List<VesselInspectionFormListDesc> descList = null;
		if (viId.contains(",")) {

			descList = dbManager.getVIFormListDescForMultipleVIIds(viId);

		} else {

			descList = dbManager.getVIFormListDescByVIId(viId.replace("'", ""));

		}

		dbManager.close();
		int counter = 0;
		if (descList != null && descList.size() > 0) {
			xmlData.append("<UpdateVesselInspectionFormListDesc>");
			xmlData.append("<shipId>");
			xmlData.append(CommonUtil.getShipId(context));
			xmlData.append("</shipId>");

			for (VesselInspectionFormListDesc viflDesc : descList) {

				if (counter > 100)
					break;

				xmlData.append("<VesselInspectionFormListDesc>");

				xmlData.append("<iVesselInspectionFormListDescId>");
				xmlData.append(viflDesc.getiVesselInspectionFormListDescId());
				xmlData.append("</iVesselInspectionFormListDescId>");

				xmlData.append("<flgDeleted>");
				xmlData.append(String.valueOf(viflDesc.getFlgDeleted()));
				xmlData.append("</flgDeleted>");
				xmlData.append("<iVesselInspectionFormListId>");
				xmlData.append(viflDesc.getiFilledFormId());
				xmlData.append("</iVesselInspectionFormListId>");

				xmlData.append("<iFormSectionId>");
				xmlData.append(viflDesc.getiFormSectionId());
				xmlData.append("</iFormSectionId>");

				xmlData.append("<iFormSectionItemsId>");
				xmlData.append(viflDesc.getiFormSectionItemId());
				xmlData.append("</iFormSectionItemsId>");

				xmlData.append("<iVesselInspectionId>");
				xmlData.append(viflDesc.getiVesselInspectionId());
				xmlData.append("</iVesselInspectionId>");

				xmlData.append("<strDescription>");
				if (viflDesc.getStrDesc() != null
						&& !"".equals(viflDesc.getStrDesc().trim())
						&& !"NULL".equalsIgnoreCase(viflDesc.getStrDesc()))
					xmlData.append(CommonUtil.marshal(viflDesc.getStrDesc()));
				xmlData.append("</strDescription>");

				xmlData.append("<strDescType>");
				if (viflDesc.getStrDescType() != null
						&& !"".equals(viflDesc.getStrDescType().trim())
						&& !"NULL".equalsIgnoreCase(viflDesc.getStrDescType()))
					xmlData.append(viflDesc.getStrDescType());
				xmlData.append("</strDescType>");

				xmlData.append("<flgIsDirty>");
				xmlData.append(String.valueOf(viflDesc.getFlgIsDirty()));
				xmlData.append("</flgIsDirty>");

				xmlData.append("<flgIsEdited>");
				xmlData.append(String.valueOf(viflDesc.getFlgIsEdited()));
				xmlData.append("</flgIsEdited>");

				xmlData.append("<flgIsDeviceDirty>");
				xmlData.append(String.valueOf(viflDesc.getFlgIsDeviceDirty()));
				xmlData.append("</flgIsDeviceDirty>");

				xmlData.append("<flgChecked>");
				xmlData.append(viflDesc.getFlgChecked());
				xmlData.append("</flgChecked>");

				xmlData.append("<sequence>");
				xmlData.append(String.valueOf(viflDesc.getSequence()));
				xmlData.append("</sequence>");

				xmlData.append("<flgStatus>");
				xmlData.append(String.valueOf(viflDesc.getFlgStatus()));
				xmlData.append("</flgStatus>");

				xmlData.append("<createdBy>");
				xmlData.append(viflDesc.getCreatedBy());
				xmlData.append("</createdBy>");

				xmlData.append("<modifiedBy>");
				xmlData.append(viflDesc.getModifiedBy());
				xmlData.append("</modifiedBy>");

				xmlData.append("<createdDate>");
				xmlData.append(viflDesc.getCreatedDate());
				xmlData.append("</createdDate>");

				xmlData.append("<modifiedDate>");
				xmlData.append(viflDesc.getModifiedDate());
				xmlData.append("</modifiedDate>");

				xmlData.append("<iTenantId>");
				xmlData.append(viflDesc.getiTenantId());
				xmlData.append("</iTenantId>");

				xmlData.append("<iShipId>");
				xmlData.append(viflDesc.getiShipId());
				xmlData.append("</iShipId>");

				xmlData.append("</VesselInspectionFormListDesc>");

				counter = counter + 1;
			}

			xmlData.append("</UpdateVesselInspectionFormListDesc>");
		}

		return xmlData.toString();
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpEnablingNetwork(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				serviceProcessEnd();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				serviceProcessEnd();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void displayPopUpNorNoShipExist(String Message) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				VesselInspectionStartFragment.lastActiveInspection = null;
				serviceProcessEnd();

			}
		});

		dialog.show();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void displayPopUpForIncompatibleVersion(String Message) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				VesselInspectionStartFragment.lastActiveInspection = null;

				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					if (CommonUtil.getActiveInspectionID(context) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(context))) {
						serviceProcessEnd();
					} else if (CommonUtil.getDropDbFlag(context) != null
							&& "1".equalsIgnoreCase(CommonUtil
									.getDropDbFlag(context))) {
						DBManager db = new DBManager(context);
						db.open();
						db.dropDb();
						db.close();
						CommonUtil.deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(context, LoginActivity.class);
						context.startActivity(intent);
						// context.finish();
					} else {
						serviceProcessEnd();
					}
				} else {
					serviceProcessEnd();
				}
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				VesselInspectionStartFragment.lastActiveInspection = null;

				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					if (CommonUtil.getActiveInspectionID(context) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(context))) {
						serviceProcessEnd();
					} else if (CommonUtil.getDropDbFlag(context) != null
							&& "1".equalsIgnoreCase(CommonUtil
									.getDropDbFlag(context))) {
						DBManager db = new DBManager(context);
						db.open();
						db.dropDb();
						db.close();
						CommonUtil.deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(context, LoginActivity.class);
						context.startActivity(intent);
						// context.finish();
					} else {
						serviceProcessEnd();
					}
				} else {
					serviceProcessEnd();
				}
			}
		});

		dialog.show();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForNoSync(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				// serviceProcessEnd();
				if (CommonUtil.getLastActiveShip(context) != null
						&& !"".equals(CommonUtil.getLastActiveShip(context))) {
					CommonUtil.setShipID(context,
							CommonUtil.getLastActiveShip(context));
					CommonUtil.setLastActiveShip(context, null);
				}

				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					if (CommonUtil.getActiveInspectionID(context) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(context))) {
						BackGroundTask.serviceProcessEnd();
					} else if (CommonUtil.getDropDbFlag(context) != null
							&& "1".equalsIgnoreCase(CommonUtil
									.getDropDbFlag(context))) {
						DBManager db = new DBManager(context);
						db.open();
						db.dropDb();
						db.close();
						CommonUtil.deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(context, LoginActivity.class);
						context.startActivity(intent);
						// context.finish();
					} else {
						serviceProcessEnd();
					}

				} else {
					BackGroundTask.serviceProcessEnd();
				}
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (CommonUtil.getLastActiveShip(context) != null
						&& !"".equals(CommonUtil.getLastActiveShip(context))) {
					CommonUtil.setShipID(context,
							CommonUtil.getLastActiveShip(context));
					CommonUtil.setLastActiveShip(context, null);
				}
				if (CommonUtil.getUserLogin(context) != null
						&& "1".equals(CommonUtil.getUserLogin(context))) {
					if (CommonUtil.getActiveInspectionID(context) != null
							&& !"".equals(CommonUtil
									.getActiveInspectionID(context))) {
						BackGroundTask.serviceProcessEnd();
					} else if (CommonUtil.getDropDbFlag(context) != null
							&& "1".equalsIgnoreCase(CommonUtil
									.getDropDbFlag(context))) {
						DBManager db = new DBManager(context);
						db.open();
						db.dropDb();
						db.close();
						CommonUtil.deleteRecursive(new File(Environment
								.getExternalStorageDirectory() + "/theark/vi"));
						LoginActivity.isNewUser = true;
						Intent intent = new Intent(context, LoginActivity.class);
						context.startActivity(intent);
						// context.finish();
					} else {
						serviceProcessEnd();
					}

				} else {
					BackGroundTask.serviceProcessEnd();
				}

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForNoReviewStatus(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				serviceProcessEndInReviewStatus();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				serviceProcessEndInReviewStatus();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForActivateInspectionNotAvailableOnServer(
			String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				CommonUtil.setSynckFrom(context, null);
				CommonUtil.setUserLogin(context, "0");
				CommonUtil.setCallingThrough(context, null);
				CommonUtil.setActivatingShip(context, null);
				serviceProcessEnd();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				CommonUtil.setSynckFrom(context, null);
				CommonUtil.setUserLogin(context, "0");
				CommonUtil.setCallingThrough(context, null);
				CommonUtil.setActivatingShip(context, null);
				serviceProcessEnd();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(context) + "_" + curDate.getTime();

		return pkId;
	}

}
