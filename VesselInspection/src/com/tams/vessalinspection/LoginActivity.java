package com.tams.vessalinspection;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.tams.fragment.AdminFragment;
import com.tams.model.SyncStatus;
import com.tams.model.UserMaster;
import com.tams.model.VesselInspection;
import com.tams.parser.UserMasterParser;
import com.tams.service.WebServiceMethod;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

/**
 * @author Ripunjay Shukla A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements LoaderCallbacks<Cursor> {

	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// UI references.
	private AutoCompleteTextView mEmailView;
	private EditText mPasswordView;
	private View mProgressView;
	private View mLoginFormView;
	private TextView validText;
	public CheckBox loginFromShip;
	Context context;
	String activeAccessMode;

	public static boolean isNewUser = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);

		setContentView(R.layout.activity_login);

		validText = (TextView) findViewById(R.id.validText);
		loginFromShip = (CheckBox) findViewById(R.id.loginFromShip);
		// Set up the login form.
		mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
		populateAutoComplete();
		mPasswordView = (EditText) findViewById(R.id.password);
		Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
		mLoginFormView = findViewById(R.id.login_form);
		mProgressView = findViewById(R.id.login_progress);

		AdminFragment.homeOrStart = null;

		CommonUtil.setTenantID(context, null);
		CommonUtil.setUserId(context, null);
		CommonUtil.setShipID(context, null);
		CommonUtil.setActiveInspectionID(context, null);
		CommonUtil.setCallingThrough(context, null);

		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		CommonUtil.setMacID(getApplicationContext(),
				(tm.getDeviceId() != null ? tm.getDeviceId() : "tmpdvid"));

		DBManager db = new DBManager(getApplicationContext());
		db.open();
		// db.dropDb();
		UserMaster usr = new UserMaster();
		/**
		 * Only get active user
		 */
		// List<UserMaster> umList = db.getUserMasterDataByStatus(0);
		List<UserMaster> umList = db.getUserMasterDataByStatus(2);
		db.close();
		if (umList != null && umList.size() > 0 && isNewUser == false) {
			usr = umList.get(0);
			showProgress(false);
			CommonUtil.setUserName(getApplicationContext(),
					usr.getStrUserName());
			CommonUtil.setUserFName(context, usr.getStrFirstName());
			CommonUtil.setUserLName(context, usr.getStrLastName());
			CommonUtil.setTenantID(context, String.valueOf(usr.getiTenantId()));
			CommonUtil.setUserId(context, usr.getiUserId());
			CommonUtil.setRoleId(context, usr.getiRoleId());

			CommonUtil.setLoginFromShip(context, "0");

			/**
			 * Load vesselInspection which have flagStatus=0 i.e. in draft mode
			 * while this exist more than one in db.
			 * 
			 */
			db.open();
			List<VesselInspection> viDraftList = new ArrayList<VesselInspection>();
			viDraftList = db.getVesselInspectionDataByflgStatus(0);

			Intent i = new Intent(LoginActivity.this,
					VesselInspectionStartActivity.class);
			startActivity(i);
			finish();

		} else {
			isNewUser = false;
			mPasswordView
					.setOnEditorActionListener(new TextView.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView textView,
								int id, KeyEvent keyEvent) {
							if (id == R.id.login || id == EditorInfo.IME_NULL) {
								CommonUtil.setUserLogin(context, "1");
								CommonUtil.setDropDbFlag(context, "1");
								attemptLogin();
								return true;
							}
							return false;
						}
					});

			mEmailSignInButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					CommonUtil.setUserLogin(context, "1");
					CommonUtil.setDropDbFlag(context, "1");
					attemptLogin();
				}
			});

		}

		loginFromShip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (loginFromShip.isChecked()) {
					CommonUtil.setLoginFromShip(context, "1");
				} else {
					CommonUtil.setLoginFromShip(context, "0");
				}

			}
		});

		if (CommonUtil.getLoginFromShip(context) != null
				&& "1".equalsIgnoreCase(CommonUtil.getLoginFromShip(context))) {
			loginFromShip.setChecked(true);
			mEmailView.setText(CommonUtil.getUserName(context));
			mPasswordView.setText(CommonUtil.getPassword(context));
			attemptLogin();
		}
	}

	private void populateAutoComplete() {
		getLoaderManager().initLoader(0, null, this);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		boolean isReady = false;
		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		String email = mEmailView.getText().toString();
		String password = mPasswordView.getText().toString();
		String strmacid = "";

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(email)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!isEmailValid(email)) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {

			if (loginFromShip.isChecked()) {

				/*
				 * CommonUtil.setUserName(context, email);
				 * CommonUtil.setPassword(context, password);
				 * 
				 * DBManager db = new DBManager(context); db.open();
				 * List<UserMaster> uMastList = new ArrayList<UserMaster>();
				 * uMastList = db.getUserMasterByUserName(email); if (uMastList
				 * != null && uMastList.size() > 0) { boolean
				 * userNamePasswordmatch = false; for (UserMaster um :
				 * uMastList) { if (um.getStrPassword().equalsIgnoreCase(
				 * doMD5(password))) { userNamePasswordmatch = true;
				 *//**
				 * 2 for active user.
				 */
				/*
				 * um.setFlgStatus(2); db.updateUserMasterTable(um);
				 * 
				 * CommonUtil.setUserName(getApplicationContext(),
				 * um.getStrUserName()); CommonUtil.setUserFName(context,
				 * um.getStrFirstName()); CommonUtil.setUserLName(context,
				 * um.getStrLastName()); CommonUtil.setTenantID(context,
				 * String.valueOf(um.getiTenantId()));
				 * CommonUtil.setUserId(context, um.getiUserId());
				 * CommonUtil.setRoleId(context, um.getiRoleId());
				 * 
				 * CommonUtil.setLoginFromShip(context, "0");
				 * 
				 * break; } } if (userNamePasswordmatch) {
				 * CommonUtil.deleteInspectionForOtherUser(context); Intent i =
				 * new Intent(this, VesselInspectionStartActivity.class);
				 * i.putExtra("loginFromShip", "normal"); startActivity(i);
				 * finish(); } else { mPasswordView
				 * .setError(getString(R.string.error_incorrect_password));
				 * mPasswordView.requestFocus(); } } else {
				 * 
				 * List<UserMaster> uMastListCount = new
				 * ArrayList<UserMaster>(); uMastListCount =
				 * db.getUserMasterData(); if (uMastListCount != null &&
				 * uMastListCount.size() > 0) {
				 * validText.setVisibility(View.VISIBLE);
				 * validText.setText("Invalid user name and password !"); } else
				 * {
				 * 
				 * Intent i = new Intent(this,
				 * VesselInspectionStartActivity.class);
				 * i.putExtra("loginFromShip", "ship"); startActivity(i);
				 * finish(); }
				 * 
				 * }
				 */
				List<UserMaster> uMastListCount = new ArrayList<UserMaster>();
				DBManager db = new DBManager(context);
				db.open();
				uMastListCount = db.getUserMasterData();
				if (uMastListCount != null && uMastListCount.size() > 0) {

					CommonUtil.setUserName(context, email);
					CommonUtil.setPassword(context, password);
					/*
					 * DBManager db = new DBManager(context); db.open();
					 */
					List<UserMaster> uMastList = new ArrayList<UserMaster>();
					uMastList = db.getUserMasterByUserName(email);
					if (uMastList != null && uMastList.size() > 0) {
						boolean userNamePasswordmatch = false;
						for (UserMaster um : uMastList) {
							if (um.getStrPassword().equalsIgnoreCase(
									doMD5(password))) {
								userNamePasswordmatch = true;

								/**
								 * 2 for active user.
								 */
								um.setFlgStatus(2);
								db.updateUserMasterTable(um);

								CommonUtil.setUserName(getApplicationContext(),
										um.getStrUserName());
								CommonUtil.setUserFName(context,
										um.getStrFirstName());
								CommonUtil.setUserLName(context,
										um.getStrLastName());
								CommonUtil.setTenantID(context,
										String.valueOf(um.getiTenantId()));
								CommonUtil.setUserId(context, um.getiUserId());
								CommonUtil.setRoleId(context, um.getiRoleId());

								CommonUtil.setLoginFromShip(context, "0");

								break;
							}
						}
						if (userNamePasswordmatch) {
							CommonUtil.deleteInspectionForOtherUser(context);
							Intent i = new Intent(context,
									VesselInspectionStartActivity.class);
							i.putExtra("loginFromShip", "normal");
							startActivity(i);
							finish();
						} else {
							mPasswordView
									.setError(getString(R.string.error_incorrect_password));
							mPasswordView.requestFocus();
						}
					} else {
						uMastListCount = new ArrayList<UserMaster>();
						uMastListCount = db.getUserMasterData();
						if (uMastListCount != null && uMastListCount.size() > 0) {
							validText.setVisibility(View.VISIBLE);
							validText
									.setText("Invalid user name and password !");
						} else {

							Intent i = new Intent(context,
									VesselInspectionStartActivity.class);
							i.putExtra("loginFromShip", "ship");
							startActivity(i);
							finish();
						}

					}

				} else {

					launchPopUpForShipLogin(
							context.getResources().getString(
									R.string.msgForShipLogin), email, password);
				}
				
				db.close();

			} else {
				isReady = CommonUtil.checkConnectivity(context);

				if (isReady) {
					showProgress(true);
					mAuthTask = new UserLoginTask(email, password, strmacid);
					mAuthTask.execute((Void) null);
				}
			}
		}
	}

	private boolean isEmailValid(String email) {
		// TODO: Replace this with your own logic
		// return email.contains("@");
		return email.contains("");
	}

	private boolean isPasswordValid(String password) {
		// TODO: Replace this with your own logic
		return password.length() > 4;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		return new CursorLoader(this,
				// Retrieve data rows for the device user's 'profile' contact.
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
				ProfileQuery.PROJECTION,

				// Select only email addresses.
				ContactsContract.Contacts.Data.MIMETYPE + " = ?",
				new String[] { ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE },

				// Show primary email addresses first. Note that there won't be
				// a primary email address if the user hasn't specified one.
				ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		List<String> emails = new ArrayList<String>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			emails.add(cursor.getString(ProfileQuery.ADDRESS));
			cursor.moveToNext();
		}

		addEmailsToAutoComplete(emails);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {

	}

	private interface ProfileQuery {
		String[] PROJECTION = { ContactsContract.CommonDataKinds.Email.ADDRESS,
				ContactsContract.CommonDataKinds.Email.IS_PRIMARY, };

		int ADDRESS = 0;
		@SuppressWarnings("unused")
		int IS_PRIMARY = 1;
	}

	private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
		// Create adapter to tell the AutoCompleteTextView what to show in its
		// dropdown list.
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				LoginActivity.this,
				android.R.layout.simple_dropdown_item_1line,
				emailAddressCollection);

		mEmailView.setAdapter(adapter);
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		private final String mEmail;
		private final String mPassword;
		private final String mStrMacId;
		private String data = "";

		UserLoginTask(String email, String password, String strMacId) {
			mEmail = email;
			mPassword = password;
			mStrMacId = strMacId;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				WebServiceMethod services = new WebServiceMethod(
						getApplicationContext());
				data = services.loginUser(mEmail, mPassword, mStrMacId);
				Log.i("data", "" + data);
				if (data != null) {
					return true;
				}

				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return false;
			}

			for (String credential : DUMMY_CREDENTIALS) {
				String[] pieces = credential.split(":");
				if (pieces[0].equals(mEmail)) {
					// Account exists, return true if the password matches.
					return pieces[1].equals(mPassword);
				}
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			int tenantId = 0;
			// showProgress(false);

			if (success) {

				DBManager db = new DBManager(context);
				db.open();

				db.updateUserMasterStatus(1);

				List<UserMaster> userList = new ArrayList<UserMaster>();

				userList = loginDeviceUser(data);

				if (userList != null && userList.size() > 0) {
					SimpleDateFormat simpleDF = new SimpleDateFormat(
							"yyyy-MM-dd");

					String modifiedDate = simpleDF.format(new Date());
					for (UserMaster um : userList) {
						tenantId = Integer
								.parseInt((um.getiTenantId() != null ? um
										.getiTenantId() : "0"));
						UserMaster uMast = db
								.getUserMasterById(um.getiUserId());
						if (uMast != null && uMast.getiUserId() != null
								&& !"".equals(uMast.getiUserId())) {
							uMast.setDtUpdated(modifiedDate);
							uMast.setFlgStatus(2);
							db.updateUserMasterTable(uMast);
						} else {

							/*
							 * new UserMaster("'"+mEmail+"'",
							 * "'"+um.getRoleId()+"'", "'"+mEmail+"'",
							 * "'"+mPassword+"'", "'"+um.getFirstName()+"'",
							 * "'"+um.getLastName()+"'", 0, 1, 0, 1,
							 * "'"+modifiedDate+"'", "'"+modifiedDate+"'",
							 * "createdBy", "modifiedBy", um.getiTenantId(), 0);
							 */

							um.setStrUserName(mEmail);
							um.setStrPassword(mPassword);
							um.setDtUpdated(modifiedDate);
							um.setFlgStatus(2);
							db.insertUserMasterTable(um);
						}

						CommonUtil.setUserName(getApplicationContext(), mEmail);
						CommonUtil.setUserFName(context, um.getStrFirstName());
						CommonUtil.setUserLName(context, um.getStrLastName());
						CommonUtil.setTenantID(context,
								String.valueOf(tenantId));
						CommonUtil.setUserId(context, um.getiUserId());
						CommonUtil.setRoleId(context, um.getiRoleId());
						CommonUtil.setShipID(context,
								String.valueOf(um.getiShipId()));

						updateSyncStatus();
					}
					db.close();

					/*
					 * SyncHandler.context=context; Message shipMessage = new
					 * Message(); shipMessage.what =
					 * SyncHandler.MSG_UPDATE_SHIP_MASETR;
					 * SyncHandler.handler.sendMessage(shipMessage);
					 */

					CommonUtil.setDropDbFlag(context, "1");
					CommonUtil.setSynckFrom(context, "admin");
					CommonUtil.setUserLogin(context, "1");
					SyncHandler.context = context;
					Message manualMessage = new Message();
					manualMessage.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(manualMessage);

					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					validText.setVisibility(View.VISIBLE);

					if (data == null || "".equals(data)) {

						validText.setText(context.getResources().getString(
								R.string.syncNotPossibleDueToCommError));

					} else {
						validText.setText("Invalid user name and password !");
					}

					showProgress(false);
					/*
					 * Intent i = new Intent(LoginActivity.this,
					 * VesselInspectionStartActivity.class); startActivity(i);
					 * finish();
					 */
				}

			} else {
				if (data != null) {
					mPasswordView
							.setError(getString(R.string.error_incorrect_password));
					mPasswordView.requestFocus();
				} else {

					mPasswordView
							.setError(getString(R.string.syncNotPossibleDueToCommError));
					mPasswordView.requestFocus();
				}
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	/**
	 * This method takes xml in string as input then start parsing of that xml.
	 * Check whether parsing is completed or not.If completed then insert data
	 * into sync table.In case of failure pop up will launch.
	 * 
	 * @param data
	 */
	@SuppressLint("SimpleDateFormat")
	private List<UserMaster> loginDeviceUser(String data) {
		if (data != null) {
			try {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				UserMasterParser userMasterParser = new UserMasterParser();

				saxParser.parse(xmlStream, userMasterParser);
				List<UserMaster> userList = userMasterParser
						.getUserMasterData();

				return userList;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpEnablingNetwork(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m update SyncStatus table
	 */
	private void updateSyncStatus() {
		try {
			DBManager db = new DBManager(context);
			db.open();
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			SyncStatus syncStatus = null;
			SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			if (syncStatusList != null && syncStatusList.size() > 0) {

				syncStatus = syncStatusList.get(0);

				if (syncStatus.getDtSyncDate() == null
						|| "".equals(syncStatus.getDtSyncDate().trim()))
					syncStatus.setDtSyncDate(dtFormat.format(new Date()));
				if (syncStatus.getServerAddress() == null
						|| "".equals(syncStatus.getServerAddress().trim()))
					syncStatus.setServerAddress(getResources().getString(
							R.string.defultServiceUrl));

				db.updateSyncStatusTable(syncStatus);
			} else {
				syncStatus = new SyncStatus("123" + new Date().getTime(),
						dtFormat.format(new Date()), activeAccessMode,
						getResources().getString(R.string.defultServiceUrl),
						Integer.parseInt(CommonUtil.getTenantId(context)), 0,
						0, 1, dtFormat.format(new Date()),
						dtFormat.format(new Date()),
						CommonUtil.getUserId(context),
						CommonUtil.getUserId(context),
						CommonUtil.getDataSyncModeFromDb(context));

				db.insertSyncStatusTable(syncStatus);
			}
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String doMD5(String string) {
		byte[] defaultBytes = string.getBytes();
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append("0");
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException nsae) {
			return null;
		}

	}

	/**
	 * @author pushkar.m used for displaying confirmation dialog for web sync
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForShipLogin(String Message, String eml, String pswd) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		final String email = eml;
		final String password = pswd;
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_cance_confirmation_popup);
		Button crossButton = (Button) dialog
				.findViewById(R.id.cancelCrossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog
				.findViewById(R.id.cancelDialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.cancelYes);
		ok.setText("Continue");

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				CommonUtil.setUserName(context, email);
				CommonUtil.setPassword(context, password);
				DBManager db = new DBManager(context);
				db.open();
				List<UserMaster> uMastList = new ArrayList<UserMaster>();
				uMastList = db.getUserMasterByUserName(email);
				if (uMastList != null && uMastList.size() > 0) {
					boolean userNamePasswordmatch = false;
					for (UserMaster um : uMastList) {
						if (um.getStrPassword().equalsIgnoreCase(
								doMD5(password))) {
							userNamePasswordmatch = true;

							/**
							 * 2 for active user.
							 */
							um.setFlgStatus(2);
							db.updateUserMasterTable(um);

							CommonUtil.setUserName(getApplicationContext(),
									um.getStrUserName());
							CommonUtil.setUserFName(context,
									um.getStrFirstName());
							CommonUtil.setUserLName(context,
									um.getStrLastName());
							CommonUtil.setTenantID(context,
									String.valueOf(um.getiTenantId()));
							CommonUtil.setUserId(context, um.getiUserId());
							CommonUtil.setRoleId(context, um.getiRoleId());

							CommonUtil.setLoginFromShip(context, "0");

							break;
						}
					}
					if (userNamePasswordmatch) {
						CommonUtil.deleteInspectionForOtherUser(context);
						Intent i = new Intent(context,
								VesselInspectionStartActivity.class);
						i.putExtra("loginFromShip", "normal");
						startActivity(i);
						finish();
					} else {
						mPasswordView
								.setError(getString(R.string.error_incorrect_password));
						mPasswordView.requestFocus();
					}
				} else {
					List<UserMaster> uMastListCount = new ArrayList<UserMaster>();
					uMastListCount = db.getUserMasterData();
					if (uMastListCount != null && uMastListCount.size() > 0) {
						validText.setVisibility(View.VISIBLE);
						validText.setText("Invalid user name and password !");
					} else {

						Intent i = new Intent(context,
								VesselInspectionStartActivity.class);
						i.putExtra("loginFromShip", "ship");
						startActivity(i);
						finish();
					}

				}

			}

		});
		Button cancel = (Button) dialog.findViewById(R.id.cancelNo);
		cancel.setText("Cancel");
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

}
