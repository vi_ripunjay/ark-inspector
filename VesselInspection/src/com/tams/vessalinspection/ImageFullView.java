package com.tams.vessalinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tams.adapter.FullScreenImageAdapter;
import com.tams.fragment.MarpolInspectionTab;
import com.tams.fragment.VesselInspectionTab;
import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.SimpleImageInfo;

public class ImageFullView extends Activity {

	private Context context = null;
	private ImageView imageViewAsGallery;
	@SuppressWarnings("deprecation")
	private Gallery galleryForView;

	final String NO_FORM_FOUND = "No Form found";
	FilledForm filledForm = null;

	private Integer imageIDs[];
	private Bitmap thumbnails[];
	private String arrPath[];
	private Uri[] mUrls;
	String[] mFiles = null;
	private int count;
	private boolean thumbnailsselection[];

	private String strFilledFormId;
	private int imgPosition;

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;

	private boolean imageSelector = false;
	private Map<Integer, String> imageGalleryMap;
	private Map<Integer, String> imageSelectorMap;
	
	public static MenuItem miActionSelectionItem;
	public static MenuItem miActionDeleteItem;
	public static MenuItem miActionLargeItem;

	LinearLayout myGallery;
	TextView imageSelectionMode;
	TextView selectBelowImage;

	//Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);

		setContentView(R.layout.activity_image_full_view);

		myGallery = (LinearLayout) findViewById(R.id.gallryView);
		imageSelectionMode = (TextView) findViewById(R.id.imageSelectionMode);
		selectBelowImage = (TextView) findViewById(R.id.selectBelowImage);
		
		selectBelowImage.setVisibility(View.GONE);
		imageSelectionMode.setVisibility(View.GONE);
		
		
		context = this;
		Intent intent = getIntent();

		imageSelectorMap = new HashMap<Integer, String>();
		imageGalleryMap = new HashMap<Integer, String>();

		strFilledFormId = intent.getStringExtra("strFilledFormId");
		if (intent.getStringExtra("imgPosition") != null) {
			imgPosition = Integer
					.parseInt(intent.getStringExtra("imgPosition"));
		} else {
			imgPosition = 0;
		}
		viewPager = (ViewPager) findViewById(R.id.pager);
		// progress bar -start
		/*
		 * final ProgressDialog dialog = ProgressDialog.show(ImageFullView.this,
		 * "Please wait", "Loading...", true); new Thread() { public void run()
		 * { try { Thread.sleep(2200);
		 * 
		 * } catch (InterruptedException e) { e.toString(); e.printStackTrace();
		 * } finally { dialog.dismiss(); } } }.start();
		 */

		// imageViewAsGallery = (ImageView) findViewById(R.id.formImage);

		String shipId = CommonUtil.getShipId(context);
		String formId = strFilledFormId != null ? strFilledFormId : "form";
		
		/*List<FilledForm> ffList = new ArrayList<FilledForm>();
		DBManager db = new DBManager(context);
		db.open();
		ffList = db.getFilledFormDataById(formId);
		if(ffList != null && ffList.size() > 0){
			FilledForm ff = new FilledForm();
			ff = ffList.get(0);
			//ff.setFlgIsDirty(1);
			db.updateFilldFormTable(ff);
		}
		db.close();
		*/
		//File storageDir = new File(Environment.getExternalStorageDirectory()
			//	.getAbsolutePath(), "/theark/vi/process/" + shipId + "/" + formId);
		
		File storageDir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath(), context.getResources().getString(R.string.imagePath) + shipId + "/" + formId);

		if (storageDir.exists()) {
			File[] imagesList = storageDir.listFiles();
			if (imagesList.length > 0) {
				// L.fv("image list size :" + imagesList.length);
				mFiles = new String[imagesList.length];

				for (int i = 0; i < imagesList.length; i++) {
					mFiles[i] = imagesList[i].getAbsolutePath();
				}
				// L.fv("mFiles  size :" + mFiles.length);
				mUrls = new Uri[mFiles.length];

				for (int i = 0; i < mFiles.length; i++) {
					mUrls[i] = Uri.parse(mFiles[i]);
				}
			} else {

				if (VessalInspectionHome.viTabFlag != null
						&& !"".equals(VessalInspectionHome.viTabFlag)
						&& VessalInspectionHome.viTabFlag
								.equalsIgnoreCase("vi")) {
					VesselInspectionTab.refreshAdapter();
					VessalInspectionHome.viTabFlag = "";
				} else if (VessalInspectionHome.viTabFlag != null
						&& !"".equals(VessalInspectionHome.viTabFlag)
						&& VessalInspectionHome.viTabFlag
								.equalsIgnoreCase("mi")) {
					MarpolInspectionTab.refreshAdapter();
					VessalInspectionHome.viTabFlag = "";
				}
				finish();
			}
		}

		int i = 0;
		if (mFiles != null && mFiles.length > 0) {
			for (String str : mFiles) {
				myGallery.addView(insertPhoto(str, i));
				i++;
			}
		}

		if (mUrls != null && mUrls.length > 0) {
			adapter = new FullScreenImageAdapter(getApplicationContext(),
					mFiles);
			viewPager.setAdapter(adapter);
			// displaying selected image first
			viewPager.setCurrentItem(imgPosition);
		} else {
			//Toast.makeText(context, "Form image not found", Toast.LENGTH_SHORT)	.show();
		}
	}

	@SuppressLint("NewApi")
	View insertPhoto(String str, int id) {
		// TODO Auto-generated method stub
		LinearLayout layout = new LinearLayout(getApplicationContext());

		try {
			SimpleImageInfo sii = new SimpleImageInfo(new File(str));
			Double height = 200.0;
			Double width = 150.0;
			Double aspectratio = 1.0;
			if (sii.getHeight() > sii.getWidth()) {
				height = 200.0;
				width = 150.0;
				aspectratio = (double) sii.getHeight()
						/ (double) sii.getWidth();
			} else {
				height = 150.0;
				width = 200.0;
				aspectratio = (double) sii.getWidth()
						/ (double) sii.getHeight();
			}

			if (sii.getHeight() > height) {
				height = height * aspectratio;
				width = width * aspectratio;
			} else {
				height = (double) sii.getHeight();
				width = (double) sii.getWidth();
			}
			DecimalFormat decFormat = new DecimalFormat("##");

			layout.setLayoutParams(new LayoutParams(Integer.parseInt(decFormat
					.format(width)) + 20, Integer.parseInt(decFormat
					.format(height)) + 20));
			layout.setGravity(Gravity.CENTER);

			Bitmap bm = decodeSampledBitmapFromUriForInsert(str,
					Integer.parseInt(decFormat.format(width)),
					Integer.parseInt(decFormat.format(height)));

			final ImageView imageView = new ImageView(getApplicationContext());
			// imageView.setLayoutParams(new LayoutParams(220, 220));
			imageView.setLayoutParams(new LayoutParams(Integer
					.parseInt(decFormat.format(width)), Integer
					.parseInt(decFormat.format(height))));
			imageView.setId(id);

			/**
			 * put id and path for image reference
			 */
			if (imageGalleryMap == null) {
				imageGalleryMap = new HashMap<Integer, String>();
			}
			imageGalleryMap.put(id, str);
			/**
			 * if want to fitxy then open below comment section. if want to
			 * display as image orientation then need to comment.
			 */
			// imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setImageBitmap(bm);

			layout.addView(imageView);

			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (imageSelector) {
						/**
						 * for selection
						 */
						if (imageSelectorMap == null) {
							imageSelectorMap = new HashMap<Integer, String>();
						}
						if (imageSelectorMap.containsKey(imageView.getId())) {
							imageView.setBackground(getResources().getDrawable(
									R.drawable.image_unselection));
							imageSelectorMap.remove(imageView.getId());
						} else {
							imageView.setBackground(getResources().getDrawable(
									R.drawable.image_selection));

							imageSelectorMap.put(imageView.getId(),
									imageGalleryMap.get(imageView.getId()));
						}

                       /* For selection image changes.
                        * 22-jan-2016
                        */
                       if(imageSelectorMap.size() == 0){
                               imageSelector = false;                                                        
                               miActionSelectionItem.setIcon(getResources().getDrawable(R.drawable.select_mul_icon));
                               miActionDeleteItem.setVisible(false);
                               miActionLargeItem.setVisible(false);
                               selectBelowImage.setVisibility(View.GONE);
                               imageSelectionMode.setVisibility(View.GONE);
                       }

					} else {
						viewPager.setCurrentItem(imageView.getId());
					}
					
					

					// Toast.makeText(getApplicationContext(), "Image View ",
					// Toast.LENGTH_LONG).show();
				}
			});
			
			 imageView.setOnLongClickListener(new OnLongClickListener() {
                 
                 @Override
                 public boolean onLongClick(View v) {
                         if (imageSelector) {
                                 /*imageSelector = false;
                                 miActionSelectionItem.setIcon(getResources().getDrawable(R.drawable.select_mul_icon));
                                 miActionDeleteItem.setVisible(false);
                                 miActionLargeItem.setVisible(false);
                                 selectBelowImage.setVisibility(View.GONE);
                                 imageSelectionMode.setVisibility(View.GONE);
                                 
                                 imageView.setBackground(getResources().getDrawable(
                                                 R.drawable.image_unselection));*/
                                 
                                 
                         } else {
                                 imageSelector = true;
                                 miActionDeleteItem.setVisible(true);
                                 miActionLargeItem.setVisible(true);
                                 miActionSelectionItem.setIcon(getResources().getDrawable(R.drawable.back_arrow));
                                 
                                 selectBelowImage.setVisibility(View.VISIBLE);
                                 imageSelectionMode.setVisibility(View.VISIBLE);
                                 
                                 if (imageSelector) {
                                         /**
                                          * for selection
                                          */
                                         if (imageSelectorMap == null) {
                                                 imageSelectorMap = new HashMap<Integer, String>();
                                         }
                                         if (imageSelectorMap.containsKey(imageView.getId())) {
                                                 imageView.setBackground(getResources().getDrawable(
                                                                 R.drawable.image_unselection));
                                                 imageSelectorMap.remove(imageView.getId());
                                         } else {
                                                 imageView.setBackground(getResources().getDrawable(
                                                                 R.drawable.image_selection));

                                                 imageSelectorMap.put(imageView.getId(),
                                                                 imageGalleryMap.get(imageView.getId()));
                                         }

                                 }
                                 
                         }
                         return true;
                 }
         });

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return layout;
	}

	public Bitmap decodeSampledBitmapFromUri(BitmapFactory.Options options, String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		//final BitmapFactory.Options options = new BitmapFactory.Options();
		//options.inJustDecodeBounds = true;
		//BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		
		try {
			bm = BitmapFactory.decodeFile(path, options);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bm;
	}
	
	public Bitmap decodeSampledBitmapFromUriForInsert(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		
		try {
			bm = BitmapFactory.decodeFile(path, options);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_full_view, menu);
		
		miActionSelectionItem = menu.findItem(R.id.selectImage);
		miActionDeleteItem = menu.findItem(R.id.deleteImage);
		miActionLargeItem = menu.findItem(R.id.largeImage);
		miActionSelectionItem.setIcon(getResources().getDrawable(R.drawable.select_mul_icon));
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.deleteImage) {
			deleteImage();
			return true;
		} else if (id == R.id.selectImage) {
			if (imageSelector) {
				imageSelector = false;
				miActionDeleteItem.setVisible(false);
				miActionLargeItem.setVisible(false);
				
				Intent intent = new Intent(this, ImageFullView.class);
				intent.putExtra("strFilledFormId", strFilledFormId);
				intent.putExtra("imgPosition", String.valueOf(0));
				startActivity(intent);
				finish();
			} else {
				imageSelector = true;
				miActionDeleteItem.setVisible(true);
				miActionLargeItem.setVisible(true);
				miActionSelectionItem.setIcon(getResources().getDrawable(R.drawable.back_arrow));
				
				selectBelowImage.setVisibility(View.VISIBLE);
				imageSelectionMode.setVisibility(View.VISIBLE);
				
			}

			return true;
		}

		else if (id == R.id.largeImage) {
			//scallingImage();

			if (imageSelectorMap != null && imageSelectorMap.size() > 0) {
				launchPopUpForLargeImage(getResources().getString(R.string.scaleImageMsg),0);
			} else {
				launchPopUpForDeleteConformation(getResources().getString(R.string.selectImageMsgForLarge), 0);
			}
			
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void scallingImage(){

		if (imageSelectorMap != null && imageSelectorMap.size() > 0) {

			for (java.util.Map.Entry<Integer, String> str : imageSelectorMap
					.entrySet()) {
				String relativePath = "";
				DBManager db = new DBManager(context);
				db.open();
				List<FilledFormImages> imgList = new ArrayList<FilledFormImages>();
				/*imgList = db.getFilledFormImagesByImagePath("/file:"
						+ str.getValue());*/
				imgList = db.getFilledFormImagesByImagePath(str.getValue());
				
				if (imgList != null && imgList.size() > 0) {
					for (FilledFormImages ffi : imgList) {
						/**
						 * Below code for resize and compress image.
						 */
						try {
							
							Double heightWidth = Double.parseDouble(getResources().getString(R.string.largeHeightWidth).trim());
							Double height = 2000.0;
							Double width = 1500.0;
							Double aspectratio = 1.0;
							DecimalFormat decFormat = new DecimalFormat(
									"##");
							//relativePath = ffi.getStrImagepath().replace("/file:", "");							
							relativePath = ffi.getDeviceOriginalImagePath().replace("/file:", "");

							BitmapFactory.Options options = new BitmapFactory.Options();
	                        options.inJustDecodeBounds = true;

	                        //Returns null, sizes are in the options variable
	                        BitmapFactory.decodeFile(relativePath, options);
	                        int actualWidth = options.outWidth;
	                        int actualHeight = options.outHeight;

	                        if(actualHeight > heightWidth || actualWidth > heightWidth)
	                        {
								if (actualHeight > actualWidth) {
								   height = heightWidth;
	                               aspectratio = (double) actualHeight/ (double) actualWidth;
	                               width = height/aspectratio;
								} else {
									width = heightWidth;
									aspectratio = (double) actualWidth/ (double) actualHeight;
		                            height = width/aspectratio;
								}
	                        }
	                        else{
	                        	 height = (double) actualHeight;
	                             width = (double) actualWidth;
	                        }
						
							Bitmap imageBitmap = decodeSampledBitmapFromUri(options,
									relativePath, Integer.parseInt(decFormat.format(width)),
									Integer.parseInt(decFormat.format(height)));
							Bitmap scaleimageBitmap = Bitmap.createScaledBitmap(imageBitmap, Integer.parseInt(decFormat.format(width)), Integer.parseInt(decFormat.format(height)), false);
							ByteArrayOutputStream bytes = new ByteArrayOutputStream();
							scaleimageBitmap.compress(
									Bitmap.CompressFormat.JPEG, 60, bytes);

							String relativePathArr[] = relativePath
									.split("/vi/");
							if (relativePathArr != null
									&& relativePathArr.length > 0) {
								relativePath = relativePathArr[0]
										+ "/vi/process/"
										+ relativePathArr[1];
							}

							String procesImageDirpath = relativePath
									.substring(0,
											relativePath.lastIndexOf("/"));
							File storageDir = new File(procesImageDirpath);
							if (!storageDir.exists()) {

								storageDir.mkdirs();
							}

							File f = new File(relativePath);
							if (!f.exists()) {
								f.createNewFile();
							}
							FileOutputStream fo = new FileOutputStream(f);
							fo.write(bytes.toByteArray());
							fo.close();
							
							
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						// =================End
						// resizing=============================
						
						//Pushkar : 26Aug15 : Updating FilledFormImage table after scalling image set flgStatus=1 and flgIsDirty=1
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date curDate = new Date();
						ffi.setModifiedDate(df.format(curDate));
						ffi.setFlgIsDirty(1);
						CommonUtil.setEditedData(context, true);
						ffi.setFlgStatus(1);
						db.updateFilldFormImageTable(ffi);
					}
				}
				db.close();
			}
			imageSelectorMap = null;
			Intent intent = new Intent(this, ImageFullView.class);
			intent.putExtra("strFilledFormId", strFilledFormId);
			intent.putExtra("imgPosition", String.valueOf(0));
			startActivity(intent);
			finish();
		}
	}

	/**
	 * Delete image from storage and db.
	 */
	public void deleteImage() {

		if (imageSelectorMap != null && imageSelectorMap.size() > 0) {
			launchPopUpForDeleteImage(getResources().getString(R.string.deleteImageMsg), 0);
		} else {
			launchPopUpForDeleteConformation(getResources().getString(R.string.selectImageMsg), 0);
		}
	}

	/**
	 * Delete image from storage and db.
	 */
	public void deleteImageFinal() {
		for (java.util.Map.Entry<Integer, String> str : imageSelectorMap
				.entrySet()) {
			DBManager db = new DBManager(context);
			db.open();
			// List<FilledFormImages> imgList = new
			// ArrayList<FilledFormImages>();
			// imgList = db.getFilledFormImagesById(str.getValue());
		//	List<FilledFormImages> formImageList= db.getFilledFormImagesByImagePath("/file:"+ str.getValue());
			List<FilledFormImages> formImageList= db.getFilledFormImagesByImagePath( str.getValue());
			
			/*
			long result = db.updateFilldFormImageTableFlag(context, "/file:"
					+ str.getValue());
			*/
			long result = db.updateFilldFormImageTableFlag(context, str.getValue());
			
			
			db.close();
			
			if(formImageList != null && formImageList.size() > 0){
				//File relativeFile = new File(formImageList.get(0).getStrRelativeImagePath());
				File relativeFile = new File(formImageList.get(0).getDeviceRelativeImagePath());
				if(relativeFile.exists())
					relativeFile.delete();
			}
			
			File photoFile = new File(str.getValue());
			if (photoFile.exists()) {
				// L.fv("Ready for temp file delete from actual path."+photoFile.getAbsolutePath());
				photoFile.delete();
			}

			
			
			
		}
		imageSelectorMap = null;
		Intent intent = new Intent(this, ImageFullView.class);
		intent.putExtra("strFilledFormId", strFilledFormId);
		intent.putExtra("imgPosition", String.valueOf(0));
		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() {

		if (VessalInspectionHome.viTabFlag != null
				&& !"".equals(VessalInspectionHome.viTabFlag)
				&& VessalInspectionHome.viTabFlag.equalsIgnoreCase("vi")) {
			VesselInspectionTab.showAllGroup=1;
			VesselInspectionTab.refreshAdapter();
			VessalInspectionHome.viTabFlag = "";
		} else if (VessalInspectionHome.viTabFlag != null
				&& !"".equals(VessalInspectionHome.viTabFlag)
				&& VessalInspectionHome.viTabFlag.equalsIgnoreCase("mi")) {
			MarpolInspectionTab.showAllGroup=1;
			MarpolInspectionTab.refreshAdapter();
			VessalInspectionHome.viTabFlag = "";
		}
		super.onBackPressed();
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForDeleteConformation(String Message,
			final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @Ripujay This function is called when image deleted then then it launches
	 *          the dialog with confirmation messages message.
	 * 
	 * @param Text
	 *            ,Integer
	 */
	private void launchPopUpForDeleteImage(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_delete_image_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				deleteImageFinal();
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}
	
	/**
     * @author pushkar.m
     *  This function is called when scaling/enlarging image  then then it launches
     *          the dialog with confirmation messages message.
     *
     * @param Text
     *            ,Integer
     */
    private void launchPopUpForLargeImage(String Message, final int position) {
            // final Editor editor = msharedPreferences.edit();
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_confirmation_large_image_popup);
            Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
            crossButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                            dialog.dismiss();
                    }
            });

            TextView textView = (TextView) dialog.findViewById(R.id.largeImageDialogMessage);
            textView.setText(Message);
            Button ok = (Button) dialog.findViewById(R.id.lagrgeImageYes);
            ok.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                            dialog.dismiss();
                            scallingImage();
                    }
            });
            Button cancel = (Button) dialog.findViewById(R.id.largeImagelNo);
            cancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                            dialog.dismiss();
                    }
            });

            dialog.show();
            dialog.setCanceledOnTouchOutside(false);
    }

}
