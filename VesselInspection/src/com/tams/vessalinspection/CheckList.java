package com.tams.vessalinspection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.tams.adapter.CheckListAdapter;
import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.FilledCheckList;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

public class CheckList extends Fragment {
	ExpandableListView checkList;
	Button submitButton;
	Intent intentCL;
	
	// ArrayList<String> groupItem = new ArrayList<String>();
	// ArrayList<Object> childItem = new ArrayList<Object>();

	ArrayList<CheckListSection> checkListSection = new ArrayList<CheckListSection>();
	ArrayList<Object> checkListSectionItem = new ArrayList<Object>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		intentCL = getActivity().getIntent();
		String strVesselInspectionId = intentCL.getStringExtra("strVesselInspectionId");
		View android = inflater.inflate(R.layout.checklist_frag, container,
				false);
		// ((TextView)android.findViewById(R.id.textView)).setText("CheckList");

		checkList = (ExpandableListView) android.findViewById(R.id.checkList);
		/*submitButton = (Button) android.findViewById(R.id.btnSubmit);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Object ci : checkListSectionItem) {
					ArrayList<FilledCheckList> cildList = (ArrayList<FilledCheckList>) ci;
					for (FilledCheckList fcl : cildList) {
						System.out.println("child Title : "
								+ fcl.getFlgChecked());
						System.out.println("child flgChecked : "
								+ fcl.getFlgChecked());
						System.out.println("child Desc : " + fcl.getStrDesc());
						
						DBManager db = new DBManager(getActivity());
						db.open();
						db.insertFilldCheckListTable(fcl);
						db.close();
					}
				}

			}
		});*/

		setGroupData(strVesselInspectionId);
		// setChildGroupData();

		final CheckListAdapter mNewAdapter = new CheckListAdapter(0,checkListSection,
				checkListSectionItem,getActivity());
		mNewAdapter.setInflater((LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				getActivity());
		checkList.setAdapter(mNewAdapter);		
		
		// expandbleLis.setOnChildClickListener(this);
		return android;
	}

	public void setGroupData(String strVesselInspectionId) {
		/*
		 * groupItem.add("TechNology"); groupItem.add("Mobile");
		 * groupItem.add("Manufacturer"); groupItem.add("Extras");
		 */

		checkListSection = new ArrayList<CheckListSection>();
		checkListSectionItem = new ArrayList<Object>();
		DBManager db = new DBManager(getActivity());
		db.open();
		List<CheckListSection> csList = new ArrayList<CheckListSection>();
		csList = db.getCheckListSectionData();
		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		if (csList != null && csList.size() > 0) {

			for (CheckListSection cs : csList) {
				checkListSection.add(cs);

				/**
				 * load form section item
				 */
				List<CheckListSectionItems> clsiList = new ArrayList<CheckListSectionItems>();
				// need to change method
				clsiList = db.getCheckListSectionItemsDataBySectionId(cs
						.getiCheckListSectionId());

				List<FilledCheckList> filedCheckListBySecList = new ArrayList<FilledCheckList>();
				int seq = 1;
				for (CheckListSectionItems clsSecitm : clsiList) {
					/**
					 * get filled checkList value on the basis of
					 * vesselInspection, checklistsection and
					 * checklistsectionitrm
					 */
					List<FilledCheckList> fldCheckList = new ArrayList<FilledCheckList>();
					fldCheckList = db.getFilledCheckListDataByIds(
							strVesselInspectionId,
							cs.getiCheckListSectionId(),
							clsSecitm.getiCheckListSectionItemsId());

					if (fldCheckList != null && fldCheckList.size() > 0) {

						filedCheckListBySecList.addAll(fldCheckList);

					} else {
						FilledCheckList filledCheckList = new FilledCheckList(
								getPk(), strVesselInspectionId,
								clsSecitm.getiCheckListSectionItemsId(),
								cs.getiCheckListSectionId(), "No", false,false,
								"Desc", "strRemarks", 0, seq, 0, 1,
								format.format(cudate), format.format(cudate),
								"createdBy", "modifiedBy", 28, 0);
						filedCheckListBySecList.add(filledCheckList);
					}

					seq++;
				}

				checkListSectionItem.add(filedCheckListBySecList);

			}
		}

		db.close();

	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getActivity()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	public void setChildGroupData() {
		/**
		 * Add Data For TecthNology
		 */
		ArrayList<Simple> child = new ArrayList<Simple>();

		child.add(new Simple("Java", false, ""));
		child.add(new Simple(".Net", false, ""));
		child.add(new Simple("PHP", false, ""));
		child.add(new Simple("Drupal", false, ""));
		// childItem.add(child);

		/**
		 * Add Data For Mobile
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Android", true, "test"));
		child.add(new Simple("Window Mobile", false, ""));
		child.add(new Simple("iPHone", true, "ok"));
		child.add(new Simple("Blackberry", false, ""));
		// childItem.add(child);
		/**
		 * Add Data For Manufacture
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("HTC", true, ""));
		child.add(new Simple("Apple", false, ""));
		child.add(new Simple("Samsung", true, "ripunjay"));
		child.add(new Simple("Nokia", false, ""));
		// childItem.add(child);
		/**
		 * Add Data For Extras
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Contact Us", false, ""));
		child.add(new Simple("About Us", true, ""));
		child.add(new Simple("Location", true, "my will"));
		child.add(new Simple("Root Cause", false, ""));
		// childItem.add(child);
	}
	
}
