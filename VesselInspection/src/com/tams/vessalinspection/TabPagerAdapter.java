package com.tams.vessalinspection;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
    public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
        case 0:
            //Fragement for CheckList Tab
            return new CheckList();
        case 1:
           //Fragment for VesselInspectionForm Tab
            return new VesselInspectionForm();
        case 2:
            //Fragment for MarpolInspectionForm Tab
            return new MarpolInspectionForm();
        }
		return null;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3; //No of Tabs
	}


    }