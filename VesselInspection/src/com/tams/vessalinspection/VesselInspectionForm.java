package com.tams.vessalinspection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.tams.adapter.VesselInsepectionFormAdapter;
import com.tams.model.FilledForm;
import com.tams.model.FormCategory;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

public class VesselInspectionForm extends Fragment {
	ExpandableListView viFormList;
	Button submitButton;
	Intent intentVI;

	// ArrayList<String> groupItem = new ArrayList<String>();
	// ArrayList<Object> childItem = new ArrayList<Object>();

	List<FormSection> formSection = new ArrayList<FormSection>();
	List<Object> formSectionItem = new ArrayList<Object>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		intentVI = getActivity().getIntent();
		String strVesselInspectionId = intentVI.getStringExtra("strVesselInspectionId");
		
		View vesselInspec = inflater.inflate(R.layout.vessel_inspection_frag,
				container, false);

		// ((TextView)android.findViewById(R.id.textView)).setText("CheckList");

		viFormList = (ExpandableListView) vesselInspec
				.findViewById(R.id.vesselInspectionFormId);
	/*	submitButton = (Button) vesselInspec.findViewById(R.id.btnVISubmit);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Object fsi : formSectionItem) {
					@SuppressWarnings("unchecked")
					List<FilledForm> fildFrmItmList = (ArrayList<FilledForm>) fsi;
					for (FilledForm fldFrm : fildFrmItmList) {
						System.out.println("child Title : "
								+ fldFrm.getStrHeaderDesc());
						System.out.println("child footer : "
								+ fldFrm.getStrFooterDesc());
						System.out.println("child Desc : "
								+ fldFrm.getCreatedBy());
						
						DBManager db = new DBManager(getActivity());
						db.open();
						if(fldFrm.getiFilledFormId() != null){
							db.updateFilldFormTable(fldFrm);
						}
						else{
							db.insertFilldFormTable(fldFrm);
						}
						db.close();
					}
				}

			}
		});*/

		setGroupData(strVesselInspectionId);
		// setChildGroupData();

		VesselInsepectionFormAdapter vifAdapter = new VesselInsepectionFormAdapter(0,getActivity(),
				formSection, formSectionItem);
		vifAdapter.setInflater(
				(LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE), getActivity());
		viFormList.setAdapter(vifAdapter);
		// expandbleLis.setOnChildClickListener(this);
		return vesselInspec;
	}

	public void setGroupData(String strVesselInspectionId) {

		formSection = new ArrayList<FormSection>();
		formSectionItem = new ArrayList<Object>();
		String formCategoryId="";
		
		DBManager db = new DBManager(getActivity());
		db.open();
		
		List<FormCategory> fcList = new ArrayList<FormCategory>();
		fcList = db.getFormCategoryByCode("VI");
		if(fcList.size() > 0)
			formCategoryId=fcList.get(0).getiFormCategoryId();
		List<FormSection> fsList = new ArrayList<FormSection>();
		
		//fsList = db.getFormSectionData("VI");
		fsList = db.getFormSectionData(formCategoryId);
		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		if (fsList != null && fsList.size() > 0) {

			for (FormSection fs : fsList) {
				formSection.add(fs);

				/**
				 * load form section item
				 */
				List<FormSectionItem> fsiList = new ArrayList<FormSectionItem>();
				fsiList = db.getFormSectionItemDataBySectionId(fs
						.getiFormSectionId());
				// formSectionItem.add(fsiList);
				List<FilledForm> filedFormBySecList = new ArrayList<FilledForm>();
				int seq = 1;
				for (FormSectionItem frmSecitm : fsiList) {
					/**
					 * get filled checkList value on the basis of
					 * vesselInspection, formsection and formsectionitrm
					 */
					List<FilledForm> fldFrmList = new ArrayList<FilledForm>();
					fldFrmList = db.getFilledFormDataByIds(
							strVesselInspectionId, fs.getiFormSectionId(),
							frmSecitm.getiFormSectionItemId());
					if (fldFrmList != null && fldFrmList.size() > 0) {
						
						filedFormBySecList.addAll(fldFrmList);
						
					} else {
						/*FilledForm filledForm = new FilledForm(getPk(),
								strVesselInspectionId, fs.getiFormSectionId(),
								frmSecitm.getiFormSectionItemId(),
								"Header Desc", "Footer Desc", seq,
								format.format(cudate), format.format(cudate),
								28, 0);
						*/
						FilledForm filledForm = new FilledForm();
                        filledForm.setiFilledFormId(getPk());
                        filledForm.setiVesselInspectionId(strVesselInspectionId);
                        filledForm.setiFormSectionId(fs.getiFormSectionId());
                        filledForm.setiFormSectionItemId(frmSecitm.getiFormSectionItemId());
                        filledForm.setStrHeaderDesc("");
                        filledForm.setStrFooterDesc("");
                        filledForm.setSequence(seq);
                        filledForm.setCreatedDate(format.format(cudate));
                        filledForm.setModifiedDate(format.format(cudate));
                        filledForm.setiTenantId(Integer.parseInt(CommonUtil.getTenantId(getActivity())));
                        filledForm.setiShipId(Integer.parseInt(CommonUtil.getShipId(getActivity())));
                        filledForm.setCreatedBy(CommonUtil.getUserId(getActivity()));
                        filledForm.setModifiedBy(CommonUtil.getUserId(getActivity()));
                        
                        filedFormBySecList.add(filledForm);
					}
					seq++;
				}

				formSectionItem.add(filedFormBySecList);

			}
		}

		db.close();
		/*
		 * groupItem.add("EXTERNAL CONDITION");
		 * groupItem.add("DECK CONDITION & MACHINERY");
		 * groupItem.add("CARGO HANDLING EQUIPMENT");
		 */
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getActivity()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	public void setChildGroupData() {
		/**
		 * Add Data For EXTERNAL CONDITION
		 */
		ArrayList<Simple> child = new ArrayList<Simple>();

		child.add(new Simple("Top sides", false, ""));
		child.add(new Simple("Anti fouling", false, ""));
		child.add(new Simple("Markings and Logos", false, ""));
		child.add(new Simple("Hull openings", false, ""));

		// childItem.add(child);

		/**
		 * Add Data For DECK CONDITION & MACHINERY
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Main deck", false, "test"));
		child.add(new Simple("Focsle", false, ""));
		child.add(new Simple("Accommodation decks", false, ""));
		child.add(new Simple("Poop", false, ""));
		child.add(new Simple("Monkey Island and fittings including masts",
				false, ""));

		// childItem.add(child);
		/**
		 * Add Data For CARGO HANDLING EQUIPMEN
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Loadicator Coamings & compression bars", false,
				""));
		child.add(new Simple(
				"Cargo Securing material (twistlocks, turnbuckles)", false, ""));
		child.add(new Simple(
				"Container base sockets + lashing eyes on pontoons & pedestals",
				false, ""));
		child.add(new Simple("Lashing Bins", false, ""));
		child.add(new Simple(
				"Stevedore Safety arrangements (walkways, ladders, lighting, railings)",
				false, ""));

		// childItem.add(child);

	}
	
	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.vessal_inspection_home, menu); return
	 * true; }
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		/*
		 * int id = item.getItemId(); if (id == R.id.action_settings) { return
		 * true; }
		 */
		return super.onOptionsItemSelected(item);
	}

}
