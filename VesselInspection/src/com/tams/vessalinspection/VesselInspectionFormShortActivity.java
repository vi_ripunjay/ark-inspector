package com.tams.vessalinspection;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.tams.adapter.VesselInsepectionFormShortAdapter;
import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;

public class VesselInspectionFormShortActivity extends Activity {

	ExpandableListView viShortFormList;
	Button submitButton;
	Intent intentVI;
	private int lastClickedPosition=0;
	List<FormSection> formSection = new ArrayList<FormSection>();
	public List<Object> shortFormSectionItem = new ArrayList<Object>();
	public static Context viContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vessel_inspection_form_short);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);
		
		intentVI = getIntent();
		String strVesselInspectionId = intentVI.getStringExtra("strVesselInspectionId");
		String strSectionId = intentVI.getStringExtra("strFormSectionId");
		
		viContext = this;
		
		viShortFormList = (ExpandableListView) findViewById(R.id.vesselInspectionFormShortId);
		
		setGroupData(strVesselInspectionId, strSectionId);
		
		VesselInsepectionFormShortAdapter vifAdapter = new VesselInsepectionFormShortAdapter(viContext,
				formSection,shortFormSectionItem);
		vifAdapter.setInflater(
				(LayoutInflater) getSystemService(
						Context.LAYOUT_INFLATER_SERVICE), this);
		viShortFormList.setAdapter(vifAdapter);
		
		viShortFormList.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				
				//viFormList.setSelection(groupPosition);
				Boolean shouldExpand = (!viShortFormList.isGroupExpanded(groupPosition));        
				viShortFormList.collapseGroup(lastClickedPosition);

			    if (shouldExpand){
			        //generateExpandableList();
			    	viShortFormList.expandGroup(groupPosition);
			    	viShortFormList.setSelectionFromTop(groupPosition, 0);
			    }                
			    lastClickedPosition = groupPosition;
			    //v.setBackgroundColor(getResources().getColor(R.color.green));
				return true;
			}
		});
		
	}
	
	public void setGroupData(String strVesselInspectionId, String strSectionId) {

		formSection = new ArrayList<FormSection>();
		shortFormSectionItem = new ArrayList<Object>();
		
		DBManager db = new DBManager(getApplicationContext());
		db.open();
		List<FormSection> fsList = new ArrayList<FormSection>();
		fsList = db.getFormSectionDataById(strSectionId);
		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		if (fsList != null && fsList.size() > 0) {

			for (FormSection fs : fsList) {
				formSection.add(fs);

				/**
				 * load form section item
				 */
				List<FormSectionItem> fsiList = new ArrayList<FormSectionItem>();
				fsiList = db.getFormSectionItemDataBySectionId(fs
						.getiFormSectionId());
				// formSectionItem.add(fsiList);
				List<FilledForm> filedFormBySecList = new ArrayList<FilledForm>();
				int seq = 1;
				for (FormSectionItem frmSecitm : fsiList) {
					/**
					 * get filled checkList value on the basis of
					 * vesselInspection, formsection and formsectionitrm
					 */
					List<FilledForm> fldFrmList = new ArrayList<FilledForm>();
					fldFrmList = db.getFilledFormDataByIds(
							strVesselInspectionId, fs.getiFormSectionId(),
							frmSecitm.getiFormSectionItemId());
					if (fldFrmList != null && fldFrmList.size() > 0) {
						
						filedFormBySecList.addAll(fldFrmList);
						
					} else {						
						
						FilledForm filledForm = new FilledForm();
                        filledForm.setiFilledFormId(getPk());
                        filledForm.setiVesselInspectionId(strVesselInspectionId);
                        filledForm.setiFormSectionId(fs.getiFormSectionId());
                        filledForm.setiFormSectionItemId(frmSecitm.getiFormSectionItemId());
                        filledForm.setStrHeaderDesc("");
                        filledForm.setStrFooterDesc("");
                        filledForm.setSequence(seq);
                        filledForm.setCreatedDate(format.format(cudate));
                        filledForm.setModifiedDate(format.format(cudate));
                        filledForm.setiTenantId(Integer.parseInt(CommonUtil.getTenantId(getApplicationContext())));
                        filledForm.setiShipId(Integer.parseInt(CommonUtil.getShipId(getApplicationContext())));
                        filledForm.setCreatedBy(CommonUtil.getUserId(getApplicationContext()));
                        filledForm.setModifiedBy(CommonUtil.getUserId(getApplicationContext()));
                
                        filedFormBySecList.add(filledForm);
					}
					
					
					/*
					 * Pushkar :  Entry point for VesselInspectionFormListDesc for all item
					 * 
					 */
					
					
					
					
					
					seq++;
				}

				shortFormSectionItem.add(filedFormBySecList);

			}
		}

		db.close();
		
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getApplicationContext()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.vessel_inspection_form_short, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * First it will check whether the form is locked or not. If form is locked
	 * then it display message that form is locked and it can't open
	 * FormListActivity. else it takes user to take picture after clicking the
	 * take picture icon present in every row of forms table in the
	 * FormListActivity.
	 * 
	 * @param form
	 */
	static final int REQUEST_TAKE_PHOTO = 1;
	String mCurrentPhotoPath;
	FilledForm filledFormForAssociatedImage = null;

	public void takePicture(FilledForm filledForm) {

		filledFormForAssociatedImage = filledForm;

		// startActivity(intent);
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// L.fv("stp 2 : starting take picture activity");
			// Create the File where the photo should go
			File photoFile = null;
			try {
				// L.fv("step 3 : creating take picture file");
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				ex.printStackTrace();
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				// L.fv("step 4 : saving take picture activity");
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

			}
		}
	}

	/**
	 * View images as gallery
	 * 
	 * @param filledForm
	 */
	public void viewPicture(FilledForm filledForm) {
		// Intent intent = new Intent(this, ViewImageActivity.class);
		Intent intent = new Intent(this, ImageFullView.class);

		intent.putExtra("strFilledFormId", filledForm.getiFilledFormId());
		intent.putExtra("imgPosition", "0");
		startActivity(intent);
	}
	
	
	/**
	 * View images as gallery
	 * 
	 * @param filledForm
	 */
	public void viewPictureForGl(FilledForm filledForm,int pos) {
		// Intent intent = new Intent(this, ViewImageActivity.class);
		Intent intent = new Intent(this, ImageFullView.class);

		intent.putExtra("strFilledFormId", filledForm.getiFilledFormId());
		intent.putExtra("imgPosition", String.valueOf(pos));
		startActivity(intent);
	}
	

	/**
	 * @author ripunjay This method call when images done.
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// L.fv("Entering in onActivityResult method and requestCode is "+requestCode);
		File photoFile = new File(mCurrentPhotoPath);
		// Check which request we're responding to
		if (requestCode == REQUEST_TAKE_PHOTO) {
			// Make sure the request was successful
			// L.fv("after comparing requestCode and REQUEST_TAKE_PHOTO is ");
			if (resultCode == RESULT_OK) {
				// L.fv("after comparing resultCode and RESULT_OK is ");
				// The user picked a contact.
				// The Intent's data Uri identifies which contact was selected.
				// Bundle extras = data.getExtras();
				// Bitmap imageBitmap = (Bitmap) extras.get("data");
				// mImageView.setImageBitmap(imageBitmap);

				/**
				 * Saving data in formimages table.
				 */
				// L.fv("step 4.1 : saving image in db "+REQUEST_TAKE_PHOTO);

				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				FilledFormImages filledFormImages = new FilledFormImages();
				String strformImageId = CommonUtil
						.getMacId(getApplicationContext())
						+ ""
						+ new Date().getTime();
				filledFormImages.setStrFilledFormImageId(strformImageId);
				filledFormImages.setCreatedDate(formate.format(new Date()));
				filledFormImages.setFlgDeleted(0);
				filledFormImages.setFlgIsDirty(1);
				CommonUtil.setEditedData(getApplicationContext(), true);
				filledFormImages.setiFilledFormId(filledFormForAssociatedImage
						.getiFilledFormId());
				filledFormImages.setiFormSectionItemId(filledFormForAssociatedImage.getiFormSectionItemId());
				filledFormImages.setiVesselInspectionId(filledFormForAssociatedImage.getiVesselInspectionId());
				// filledFormImages.setRelativeFormImagePath(photoFile.getAbsolutePath());
				
				filledFormImages.setiFormSectionId(filledFormForAssociatedImage.getiFormSectionId());
				
				filledFormImages.setStrImagepath(photoFile.getAbsolutePath());
				filledFormImages.setStrImageName("Image");
				filledFormImages.setiShipId(Integer.parseInt(CommonUtil.getShipId(getApplicationContext()).toString()));
				filledFormImages.setiTenantId(28);

				DBManager dbManager = new DBManager(getApplicationContext());
				dbManager.open();
				L.fv("step 4.2 : creating dbmanager ");
				long result = dbManager
						.insertFilldFormImageTable(filledFormImages);
				dbManager.close();
				L.fv("step 4.3 ripunjay : after insert data in db " + result);

				List<FilledFormImages> fiList = new ArrayList<FilledFormImages>();
				dbManager.open();
				fiList = dbManager.getFilledFormImagesData();
				dbManager.close();
				L.fv("step 4.4 : geting image data from db with fiList "
						+ fiList.size());

				// Call this method for add image to gallery.
				galleryAddPic();

			} else {
				// L.fv("File exist on actual path."+photoFile.getAbsolutePath()
				// +" and file exist +"+photoFile.exists());
				if (photoFile.exists()) {
					// L.fv("Ready for temp file delete from actual path."+photoFile.getAbsolutePath());
					photoFile.delete();
				}
			}
			filledFormForAssociatedImage = null;
		}
	}

	/**
	 * @author ripujay
	 * @return
	 * @throws IOException
	 *             create temp image file on specified location
	 */
	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String shipId = CommonUtil.getShipId(getApplicationContext());
		String formId = filledFormForAssociatedImage != null ? filledFormForAssociatedImage
				.getiFilledFormId() : "form";
		File storageDir = new File(Environment.getExternalStorageDirectory(),
				"/theark/vi/" + shipId + "/" + formId);

		if (!storageDir.exists()) {

			storageDir.mkdirs();
		}

		File image = new File(storageDir.getAbsoluteFile() + "/"
				+ imageFileName + ".jpg");

		/*
		 * File image = File.createTempFile( imageFileName, prefix ".jpg",
		 * suffix storageDir directory );
		 */

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		// L.fv("creating image file on given location "+mCurrentPhotoPath);
		return image;
	}

	/**
	 * @author ripunjay this method for image add to gallery.
	 */
	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

}
