package com.tams.vessalinspection;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

import com.tams.model.FilledForm;

public class ViewImageActivity extends AbstractActivity implements
		OnClickListener {

	/**
	 * @author Ripunjay
	 * @see This activity for display image as gallery view from given folder
	 *      i.e. list of images.
	 */

	private Context context = null;
	private ImageView imageViewAsGallery;
	@SuppressWarnings("deprecation")
	private Gallery galleryForView;

	final String NO_FORM_FOUND = "No Form found";
	FilledForm filledForm = null;

	private Integer imageIDs[];
	private Bitmap thumbnails[];
	private String arrPath[];
	private Uri[] mUrls;
	String[] mFiles = null;
	private int count;
	private boolean thumbnailsselection[];

	private String strFilledFormId;

	/**
	 * @brief onCreate method always execute once when activity is created for
	 *        the first time Inside this function creating the objects for
	 *        widgets and setting the event for them.
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_image);

		context = this;

		Intent intent = getIntent();

		strFilledFormId = intent.getStringExtra("strFilledFormId");

		// progress bar -start
		final ProgressDialog dialog = ProgressDialog.show(
				ViewImageActivity.this, "Please wait", "Loading...", true);
		new Thread() {
			public void run() {
				try {
					Thread.sleep(2200);

				} catch (InterruptedException e) {
					e.toString();
					e.printStackTrace();
				} finally {
					dialog.dismiss();
				}
			}
		}.start();

		galleryForView = (Gallery) findViewById(R.id.galleryImage);
		imageViewAsGallery = (ImageView) findViewById(R.id.formImage);

		String shipId = "0";
		String formId = strFilledFormId != null ? strFilledFormId : "form";
		// File storageDir = new File(Environment.getExternalStorageDirectory(),
		// "/theark/vi/process/" + shipId + "/" + formId);

		File storageDir = new File(Environment.getExternalStorageDirectory(),
				context.getResources().getString(R.string.imagePath) + shipId
						+ "/" + formId);

		if (storageDir.exists()) {
			File[] imagesList = storageDir.listFiles();
			// L.fv("image list size :" + imagesList.length);
			mFiles = new String[imagesList.length];

			for (int i = 0; i < imagesList.length; i++) {
				mFiles[i] = imagesList[i].getAbsolutePath();
			}
			// L.fv("mFiles  size :" + mFiles.length);
			mUrls = new Uri[mFiles.length];

			for (int i = 0; i < mFiles.length; i++) {
				mUrls[i] = Uri.parse(mFiles[i]);
			}
		}
		if (mUrls != null && mUrls.length > 0) {
			galleryForView.setAdapter(new ImageAdapter(this));
			ImageView imageView = (ImageView) findViewById(R.id.formImage);
			imageView.setImageURI(mUrls[0]);
			// L.fv("Step 44.0 : after setting adaptor");
			galleryForView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					// L.fv("Step 44.1 : in listener");
					// Toast.makeText(getBaseContext(),"pic" +
					// (position + 1) +
					// " selected",Toast.LENGTH_SHORT).show();
					// display the images selected
					ImageView imageView = (ImageView) findViewById(R.id.formImage);
					imageView.setImageURI(mUrls[position]);
					// imageView.setLayoutParams(new Gallery.LayoutParams(150,
					// 150));
					// imageView.setImageResource(imageIDs[position]);
				}
			});
		} else {
			Toast.makeText(context, "Form image not found", Toast.LENGTH_SHORT)
					.show();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_image, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public class ImageAdapter extends BaseAdapter {
		private Context contxt;
		private int itemBackground;

		public ImageAdapter(Context c) {
			contxt = c;

			// sets a grey background; wraps around the images
			TypedArray a = contxt.obtainStyledAttributes(R.styleable.MyGallery);
			itemBackground = a.getResourceId(
					R.styleable.MyGallery_android_galleryItemBackground, 0);
			a.recycle();
			// L.fv("Step 4.1 : enter in image adapter");
		}

		// returns the number of images
		public int getCount() {
			return mUrls.length;
		}

		// returns the ID of an item
		public Object getItem(int position) {
			return position;
		}

		// returns the ID of an item
		public long getItemId(int position) {
			return position;
		}

		// returns an ImageView view
		@SuppressWarnings("deprecation")
		public View getView(int position, View convertView, ViewGroup parent) {
			// L.fv("Step 4.2 : Image adaptor getView Method");
			ImageView imageView = new ImageView(contxt);

			Drawable toRecycle = imageView.getDrawable();
			if (toRecycle != null) {
				((BitmapDrawable) imageView.getDrawable()).getBitmap()
						.recycle();
			}

			imageView.setImageBitmap(decodeSampledBitmapFromResource(
					getResources(), R.id.formImage, 100, 100));

			// imageView.setImageURI(mUrls[position]);
			// imageView.setImageResource(imageIDs[position]);
			imageView.setLayoutParams(new Gallery.LayoutParams(80, 70));
			imageView.setBackgroundResource(itemBackground);

			return imageView;
		}

		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
				int reqWidth, int reqHeight) {

			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(res, resId, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeResource(res, resId, options);
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		System.gc();
	}

}
