package com.tams.vessalinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.tams.adapter.NavDrawerListAdapter;
import com.tams.fragment.AdminFragment;
import com.tams.fragment.CheckListTab;
import com.tams.fragment.Exit;
import com.tams.fragment.MarpolInspectionTab;
import com.tams.fragment.VesselInspectionTab;
import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.model.NavDrawerItem;
import com.tams.model.RoleTemplate;
import com.tams.model.ShipMaster;
import com.tams.model.VIAlertTime;
import com.tams.model.VesselInspection;
import com.tams.service.BackGroundTask;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;
import com.tams.utils.ShipSelectItem;

public class VessalInspectionHome extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	String strRoleType = "";

	List<ShipMaster> shipList;
	Spinner shipMasterDropDown;
	EditText inspectionDate;
	private DatePickerDialog datePickerDialog;
	private SimpleDateFormat dateFormatter;
	private String strVesselInspectionId;
	String strFormSectionId = "";

	private BackGroundTask mBackGroundtask;

	Context context;

	VesselInspectionTab vesselInspectionTab;
	MarpolInspectionTab marpolInspectionTab;
	CheckListTab checkListTab;
	String viTitle = "";
	Intent intentHome;

	public static MenuItem miActionProgressItem;
	public static MenuItem miActionCancelItem;
	public static MenuItem miActionDraftItem;
	public static MenuItem miActionDoneItem;

	public int fragmentPosition = 0;

	public static String viTabFlag = "";

	// 0 = Initial , 1 = draft , 2 = Done
	public static int sendingStatus = 0;
	//public static int takePictureStatus = 0;// when take picture then it become
											// 1.

	private List<RoleTemplate> roleTempList;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = this;
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		// ab.setDisplayUseLogoEnabled(false);
		ab.setDisplayShowHomeEnabled(false);

		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
				| ActionBar.DISPLAY_SHOW_TITLE);

		setContentView(R.layout.activity_home);

		shipMasterDropDown = (Spinner) findViewById(R.id.shipName);
		inspectionDate = (EditText) findViewById(R.id.inspectionDate);
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

		intentHome = getIntent();
		
		/*if(savedInstanceState == null){
			
			}
		 */
		AdminFragment.homeOrStart = "";
		VesselInspectionStartActivity.undoIconVisibleOrNot = 1;

		strVesselInspectionId = intentHome
				.getStringExtra("strVesselInspectionId");
		if (strVesselInspectionId == null
				|| "".equalsIgnoreCase(strVesselInspectionId)) {
			strVesselInspectionId = CommonUtil
					.getVesselInspectionId(getApplicationContext());
		}
		strFormSectionId = intentHome.getStringExtra("strFormSectionId");

		mTitle = mDrawerTitle = getTitle();
		mTitle = mDrawerTitle = getTitle();

		// load menu icons
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);
		// load slide menu items
		// navMenuTitles =
		// getResources().getStringArray(R.array.nav_drawer_items);

		DBManager db = new DBManager(getApplicationContext());
		db.open();
		/**
		 * Get active vesselInspection and get their dirty count and alert generated.
		 */
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		List<VesselInspection> vslActiveList = db.getVesselInspectionDataByflgStatus(0);
		if(vslActiveList != null && vslActiveList.size() > 0){
			for(VesselInspection vslInsp : vslActiveList){		
				long dirtyCount = db.getVesselInspectionDirtyData(vslInsp.getiVesselInspectionId());
				if(dirtyCount > 0){
					List<VIAlertTime> viAlertTimeList = new ArrayList<VIAlertTime>();
					viAlertTimeList = db.getVIAlertTimeData();
					if(viAlertTimeList != null && viAlertTimeList.size() > 0){
						VIAlertTime viAlertTime = new VIAlertTime();						
						viAlertTime = viAlertTimeList.get(0);
						if((curDate.getTime() - Long.parseLong(viAlertTime.getAlertTime())) >= 43200000){
							
							viAlertTime.setAlertTime(""+curDate.getTime());
							viAlertTime.setDtUpdated(format.format(curDate));
							viAlertTime.setUpdatedBy(CommonUtil.getUserId(context));
							viAlertTime.setFlgIsDirty(1);						
							db.updateVIAlertTimeTable(viAlertTime);
							CommonUtil.launchPopUpForDirtyAlert(getResources().getString(R.string.dirtyAlertString), context);
						}
					}
					else{
						VIAlertTime viAlertTime = new VIAlertTime();
						viAlertTime.setiVIAlertTimeId(getPk());
						viAlertTime.setAlertTime(""+curDate.getTime());
						viAlertTime.setStrMacId(CommonUtil.getMacId(context));
						viAlertTime.setDtCreated(format.format(curDate));
						viAlertTime.setDtUpdated(format.format(curDate));
						viAlertTime.setCreatedBy(CommonUtil.getUserId(context));
						viAlertTime.setUpdatedBy(CommonUtil.getUserId(context));
						viAlertTime.setiTenantId((CommonUtil.getTenantId(context) != null && !"".equalsIgnoreCase(CommonUtil.getTenantId(context)) ? Integer.parseInt(CommonUtil.getTenantId(context)) : 0));
						
						db.insertVIAlertTimeTable(viAlertTime);
					}
				}
			}
		}		

		// db.insertSyncHistorydata(new
		// SyncHistory("123","2015-11-30","","","ship","",0,1,"1234567","1234567","2015-11-30","macId","macId",535,27));

		List<RoleTemplate> localList = new ArrayList<RoleTemplate>();
		roleTempList = new ArrayList<RoleTemplate>();
		localList = db.getRoleTempalteData(Integer.parseInt(CommonUtil
				.getTenantId(context)), Integer.parseInt((CommonUtil
				.getRoleId(context) != null && !"".equals(CommonUtil.getRoleId(
				context).trim())) ? CommonUtil.getRoleId(context) : "0"));
		if(localList != null && localList.size() > 0){
			for(RoleTemplate rt : localList){
				if(rt.getTemplate_code() != null && !"_help".equalsIgnoreCase(rt.getTemplate_code()))
				{
					roleTempList.add(rt);
				}
			}
		}

		navDrawerItems = new ArrayList<NavDrawerItem>();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		if (roleTempList != null && roleTempList.size() > 0) {
			int x = 0;
			navMenuTitles = new String[roleTempList.size()];
			for (RoleTemplate roleTmp : roleTempList) {

				// if(roleTmp.getTemaplate_name().equals("CL"))
				// navDrawerItems.add(new
				// NavDrawerItem(roleTmp.getTemaplate_name(),
				// navMenuIcons.getResourceId(roleTmp.getSequence(), -1)));
				// navDrawerItems.add(new
				// NavDrawerItem(roleTmp.getTemaplate_name(),
				// getDrawableResourceByName(roleTmp.getIcons())));
				
					navDrawerItems.add(new NavDrawerItem(
							roleTmp.getTemplate_name(), getResources()
									.getIdentifier(roleTmp.getIcons(), "drawable",
											this.getPackageName())));
					navMenuTitles[x] = roleTmp.getTemplate_name();
					x = x + 1;
				
			}
			/*
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[3],
			 * navMenuIcons .getResourceId(3, -1)));
			 * 
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[4],
			 * navMenuIcons .getResourceId(4, -1)));
			 * 
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
			 * navMenuIcons .getResourceId(5, -1)));
			 */
		}

		/*
		 * navDrawerItems = new ArrayList<NavDrawerItem>();
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
		 * .getResourceId(0, -1)));
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
		 * .getResourceId(1, -1)));
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
		 * .getResourceId(2, -1)));
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
		 * .getResourceId(3, -1)));
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
		 * .getResourceId(4, -1)));
		 * 
		 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
		 * .getResourceId(5, -1)));
		 */

		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				// invalidateOptionsMenu(); ===================
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		if (Build.VERSION.SDK_INT >= 18) {
			getActionBar().setHomeAsUpIndicator(
					getResources().getDrawable(R.drawable.ic_drawer));
		}

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}

		/**
		 * Data for testing Check List :
		 */
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		CommonUtil.setMacID(getApplicationContext(),
				(tm.getDeviceId() != null ? tm.getDeviceId() : "tmpdvid"));

		shipList = new ArrayList<ShipMaster>();
		shipList = db.getShipMasterData();

		ArrayList<ShipSelectItem> shipSelectlist = new ArrayList<ShipSelectItem>();
		shipSelectlist.add(new ShipSelectItem("Select", "Select", "Select"));
		if (shipList != null && shipList.size() > 0) {
			for (ShipMaster sm : shipList) {
				String shipId = String.valueOf(sm.getiShipId());
				String tenantId = String.valueOf(sm.getiTenantId());
				String shipName = sm.getStrShipName();
				shipSelectlist.add(new ShipSelectItem(shipName, shipId,
						tenantId));
			}
		}

		ArrayAdapter<ShipSelectItem> myAdapter = new ArrayAdapter<ShipSelectItem>(
				getApplicationContext(), android.R.layout.simple_spinner_item,
				shipSelectlist);

	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getApplicationContext()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	private void setDateField() {
		System.out.println("hello  i am here");
		Calendar newCalendar = Calendar.getInstance();
		datePickerDialog = new DatePickerDialog(context,
				new OnDateSetListener() {

					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						inspectionDate.setText(dateFormatter.format(newDate
								.getTime()));
					}

				}, newCalendar.get(Calendar.YEAR),
				newCalendar.get(Calendar.MONTH),
				newCalendar.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vessal_inspection_home, menu);
		miActionProgressItem = menu.findItem(R.id.miActionProgress);
		miActionCancelItem = menu.findItem(R.id.cancel);
		miActionDraftItem = menu.findItem(R.id.draft);
		miActionDoneItem = menu.findItem(R.id.done);

		if (roleTempList != null && roleTempList.size() > 2) {
			if (fragmentPosition > (roleTempList.size() - 3)) {
				// item.setVisible(false);
				miActionCancelItem.setVisible(false);
				miActionDraftItem.setVisible(false);
				miActionDoneItem.setVisible(false);
			}
		} else {
			miActionCancelItem.setVisible(false);
			miActionDraftItem.setVisible(false);
			miActionDoneItem.setVisible(false);
		}
		miActionDoneItem.setVisible(false);
		return true;
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		if (roleTempList != null && roleTempList.size() > 2) {
			if (fragmentPosition > (roleTempList.size() - 3)) {
				// item.setVisible(false);
				miActionCancelItem.setVisible(false);
				miActionDraftItem.setVisible(false);
				miActionDoneItem.setVisible(false);

				return true;
			} else if (fragmentPosition < (roleTempList.size() - 2)) {
				item.setVisible(true);
				miActionDoneItem.setVisible(false);
			}
		} else {
			miActionCancelItem.setVisible(false);
			miActionDraftItem.setVisible(false);
			miActionDoneItem.setVisible(false);

			return true;
		}

		miActionDoneItem.setVisible(false);

		/*
		 * if(fragmentPosition>2){ //item.setVisible(false);
		 * miActionCancelItem.setVisible(false);
		 * miActionDraftItem.setVisible(false);
		 * miActionDoneItem.setVisible(false); return true; } else
		 * if(fragmentPosition < 3){ item.setVisible(true); }
		 */

		vesselInspectionTab = new VesselInspectionTab();
		marpolInspectionTab = new MarpolInspectionTab();
		checkListTab = new CheckListTab();

		int id = item.getItemId();

		if (id == R.id.done) {

			String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
			if (dataSyncMode != null && "cable".equalsIgnoreCase(dataSyncMode)) {
				/**
				 * Nothing to do hear.
				 */
			} else {
				launchPopUpForFinish(
						getResources().getString(R.string.inspectionDoneMsg), 0);
			}
			return true;

		} else if (id == R.id.draft) {
			CommonUtil.setActiveInspectionID(context, null);
			CommonUtil.setCallingThrough(context, null);
			CommonUtil.setDropDbFlag(context, "0");
			
			
			DBManager dbm = new DBManager(context);
			dbm.open();
			List<VesselInspection> viList = dbm.getVesselInspectionDataById(CommonUtil.getVesselInspectionId(context));
			dbm.close();
			if(viList != null && viList.size() > 0 && viList.get(0).getFlgIsEdited() == 5){
				draftProcess();
			}else{
			
			launchPopUpForSync(
					getResources().getString(R.string.syncInspectionMsg), 0);
			}
			return true;

		} else if (id == R.id.cancel) {

			CommonUtil.setActiveInspectionID(context, null);
			launchPopUpForCancel(
					getResources().getString(R.string.cancelInspectionMsg), 0);
			return true;
		}
		/*else if (id == R.id.inspectionDetails) {

            SubMenu subm = item.getSubMenu();
            subm.clear();
            
            DBManager db = new DBManager(getApplicationContext());
            db.open();
            VesselInspection vi = db.getVesselInspectionDataById(
                            strVesselInspectionId).get(0);
            String shipName = db.getShipNameById(Integer.parseInt(CommonUtil.getShipId(context) != null && !"".equals(CommonUtil.getShipId(context)) ? CommonUtil.getShipId(context) : "0"));
            db.close();
            subm.add(shipName +" "+vi.getInspectionDate());
            
            //subm.addSubMenu(shipName +"\n "+vi.getInspectionDate());
            MenuItem subItm = subm.getItem();
            subItm.setTitle(shipName +"\n"+vi.getInspectionDate());
            return true;

    }*/

		return true;

	}

	/**
	 * @author pushkar.m This method check whether network is enabled or not .
	 *         If enabled then process to draft inspection
	 */
	private void draftProcess() {

		boolean isReady = false;
		sendingStatus = 1;
		/*
		 * vesselInspectionTab.saveViData(); marpolInspectionTab.saveMiData();
		 * checkListTab.saveCheckListData();
		 */
		/*String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
		if (dataSyncMode != null && "cable".equalsIgnoreCase(dataSyncMode)) {
			
			CommonUtil.launchPopUpEnablingNetwork(context.getResources()
					.getString(R.string.changeCableToWeb), context);
			
		} else {
*/			isReady = CommonUtil.checkConnectivity(context);
			if (isReady) {
				CommonUtil.setSynckFrom(context, "process");
				CommonUtil.setUserLogin(context, "0");
				SyncHandler.context = context;
				Message message = new Message();
				// message.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
				message.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
				SyncHandler.handler.sendMessage(message);

				miActionProgressItem.setVisible(true);
				miActionCancelItem.setVisible(false);
				miActionDraftItem.setVisible(false);
				miActionDoneItem.setVisible(false);
			}
		//}

	}

	/**
	 * @Ripujay This function is called when any form lock then then it launches
	 *          the dialog with confirmation messages message.
	 * 
	 * @param Text
	 *            ,Integer
	 */
	private void launchPopUpForFinish(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				boolean isReady = false;

				dialog.dismiss();

				sendingStatus = 2;
				/*
				 * vesselInspectionTab.saveViData();
				 * marpolInspectionTab.saveMiData();
				 * checkListTab.saveCheckListData();
				 */
				if (strVesselInspectionId != null) {
					DBManager db = new DBManager(getApplicationContext());
					db.open();
					VesselInspection vi = db.getVesselInspectionDataById(
							strVesselInspectionId).get(0);
					vi.setFlgStatus(1);
					vi.setFlgIsDirty(1);
					CommonUtil.setEditedData(getApplicationContext(), true);
					vi.setFlgIsLocked(1);
					// String viTitle=intentHome.getStringExtra("viTitle");
					vi.setStrInspectionTitle(intentHome
							.getStringExtra("viTitle"));
					db.updateVesselInspectionTable(vi);
					db.close();
				}

				isReady = CommonUtil.checkConnectivity(context);

				if (isReady) {
					CommonUtil.setSynckFrom(context, "process");
					SyncHandler.context = context;
					Message message = new Message();
					// message.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
					message.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(message);

					miActionProgressItem.setVisible(true);
					miActionCancelItem.setVisible(false);
					miActionDraftItem.setVisible(false);
					miActionDoneItem.setVisible(false);
				}

			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @Ripujay This function is called when any form lock then then it launches
	 *          the dialog with confirmation messages message.
	 * 
	 * @param Text
	 *            ,Integer
	 */
	private void launchPopUpForCancel(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_cance_confirmation_popup);
		Button crossButton = (Button) dialog
				.findViewById(R.id.cancelCrossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog
				.findViewById(R.id.cancelDialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.cancelYes);

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				//sendingStatus = 2;
				sendingStatus = 0;

				if (strVesselInspectionId != null) {
					DBManager db = new DBManager(getApplicationContext());
					db.open();
					VesselInspection vi = db.getVesselInspectionDataById(
							strVesselInspectionId).get(0);
					vi.setFlgStatus(0);
					vi.setFlgIsDirty(1);
					CommonUtil.setEditedData(getApplicationContext(), true);
					vi.setFlgDeleted(1);
					db.updateVesselInspectionTable(vi);
					db.close();
				}

				/*String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
				if (dataSyncMode != null
						&& "cable".equalsIgnoreCase(dataSyncMode)) {
					Intent i = new Intent(context,
							VesselInspectionStartActivity.class);
					i.putExtra("strVesselInspectionId",
							CommonUtil.getVesselInspectionId(context));
					context.startActivity(i);
					if (context instanceof VessalInspectionHome) {
						((VessalInspectionHome) context).finish();
					}

				} else {*/
					/*boolean isReady = CommonUtil.checkConnectivity(context);

					if (isReady) {
						CommonUtil.setSynckFrom(context, "process");
						CommonUtil.setUserLogin(context, "0");
						SyncHandler.context = context;
						Message message = new Message();
						// message.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
						message.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
						SyncHandler.handler.sendMessage(message);

						miActionProgressItem.setVisible(true);
						miActionCancelItem.setVisible(false);
						miActionDraftItem.setVisible(false);
						miActionDoneItem.setVisible(false);
					}*/
				
					Intent i = new Intent(context,
							VesselInspectionStartActivity.class);
					i.putExtra("strVesselInspectionId",
							CommonUtil.getVesselInspectionId(context));
					context.startActivity(i);
					if (context instanceof VessalInspectionHome) {
						((VessalInspectionHome) context).finish();
					}
				
				}

			//}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancelNo);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

   /** @author pushkar.m
    * used for displaying confirmation dialog for web sync
    * @param Message
    * @param position
    */
   private void launchPopUpForSync(String Message, final int position) {
           // final Editor editor = msharedPreferences.edit();
           final Dialog dialog = new Dialog(context);
           dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
           dialog.setContentView(R.layout.custom_dialog_cance_confirmation_popup);
           Button crossButton = (Button) dialog
                           .findViewById(R.id.cancelCrossButton);
           crossButton.setOnClickListener(new OnClickListener() {

                   @Override
                   public void onClick(View v) {
                           dialog.dismiss();
                   }
           });

           TextView textView = (TextView) dialog
                           .findViewById(R.id.cancelDialogMessage);
           textView.setText(Message);
           Button ok = (Button) dialog.findViewById(R.id.cancelYes);

           ok.setOnClickListener(new OnClickListener() {

                   @Override
                   public void onClick(View v) {
                           dialog.dismiss();
                         draftProcess();
                   }

           });
           Button cancel = (Button) dialog.findViewById(R.id.cancelNo);
           cancel.setOnClickListener(new OnClickListener() {

                   @Override
                   public void onClick(View v) {
                           dialog.dismiss();
                   }
           });

           dialog.show();
           dialog.setCanceledOnTouchOutside(false);
   }
	
	
	private void launchPopUpForBackButton(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Intent i = new Intent(VessalInspectionHome.this,
						VesselInspectionStartActivity.class);
				startActivity(i);
				finish();
				CommonUtil.setEditedData(context, false);
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	@SuppressLint("NewApi")
	public void displayView(int position) {
		// update the main content by replacing fragments

		Fragment fragment = null;

		/*
		 * switch (position) { case 0: fragment = new
		 * VesselInspectionFragment(0); if(AdminFragment.homeOrStart != null)
		 * AdminFragment.homeOrStart = null; break; case 1: fragment = new
		 * VesselInspectionFragment(1); if(AdminFragment.homeOrStart != null)
		 * AdminFragment.homeOrStart = null; break; case 2: fragment = new
		 * VesselInspectionFragment(2); if(AdminFragment.homeOrStart != null)
		 * AdminFragment.homeOrStart = null; break; case 3: fragment = new
		 * AdminFragment(); AdminFragment.homeOrStart="home"; break; case 4:
		 * fragment = new VesselInspectionHelp(); break; case 5: fragment = new
		 * Exit(); break;
		 * 
		 * default: break; }
		 */
		/*
		 * RoleTemplate roleTemp = roleTempList.get(position); if(roleTemp !=
		 * null){ fragment = new VesselInspectionFragment(position ,
		 * roleTemp.getTemplate_code()); }
		 * 
		 * 
		 * if (fragment != null) { try { // set to handle back Button
		 * FragmentManager fragmentManager = getSupportFragmentManager();
		 * 
		 * while (fragmentManager.getBackStackEntryCount() > 1) {
		 * fragmentManager.popBackStackImmediate(); }
		 * 
		 * fragmentManager.beginTransaction() .replace(R.id.frame_container,
		 * fragment) .addToBackStack(null).commitAllowingStateLoss();
		 * 
		 * // update selected item and title, then close the drawer
		 * mDrawerList.setItemChecked(position, true);
		 * mDrawerList.setSelection(position);
		 * 
		 * fragmentPosition = position; if(position < 3){
		 * setTitle("Vessel Inspection"); }else{
		 * setTitle(navMenuTitles[position]);
		 * 
		 * }
		 * 
		 * mDrawerLayout.closeDrawer(mDrawerList); } catch (Exception e) { //
		 * TODO Auto-generated catch block Log.e("Home Fragment : ",
		 * e.toString());
		 * 
		 * } } else { // error in creating fragment Log.e("MainActivity",
		 * "Error in creating fragment"); }
		 */
		RoleTemplate roleTemp = new RoleTemplate();
		if (roleTempList != null && roleTempList.size() > 0) {
			roleTemp = roleTempList.get(position);
		} else {
			roleTemp = null;
		}

		if (roleTemp != null
				&& roleTemp.getTemplate_code().equals("_checkList")) {
			fragment = new CheckListTab(roleTemp.getiFormCategoryId());
			if (AdminFragment.homeOrStart != null)
				AdminFragment.homeOrStart = null;
		}

		if (roleTemp != null && roleTemp.getTemplate_code().equals("_form")) {
			fragment = new VesselInspectionTab(roleTemp.getiFormCategoryId());
			if (AdminFragment.homeOrStart != null)
				AdminFragment.homeOrStart = null;
		}

		if (roleTemp != null
				&& roleTemp.getTemplate_code().equals("_checkListForm")) {
			fragment = new MarpolInspectionTab(roleTemp.getiFormCategoryId());
			if (AdminFragment.homeOrStart != null)
				AdminFragment.homeOrStart = null;
		}

		if (roleTemp != null && roleTemp.getTemplate_code().equals("_admin")) {
			fragment = new AdminFragment();
			AdminFragment.homeOrStart = "home";
		}

		/*if (roleTemp != null && roleTemp.getTemplate_code().equals("_help"))
			fragment = new VesselInspectionHelp();*/

		if (roleTemp != null && roleTemp.getTemplate_code().equals("_exit"))
			fragment = new Exit();

		if (fragment != null) {
			try {
				// set to handle back Button
				FragmentManager fragmentManager = getSupportFragmentManager();

				while (fragmentManager.getBackStackEntryCount() > 1) {
					fragmentManager.popBackStackImmediate();
				}

				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment)
						.addToBackStack(null).commitAllowingStateLoss();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);

				fragmentPosition = position;
				/*
				 * if(position < 3){ setTitle("Vessel Inspection"); }else{
				 * setTitle(navMenuTitles[position]);
				 * 
				 * }
				 */
				setTitle(navMenuTitles[position]);

				mDrawerLayout.closeDrawer(mDrawerList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("Home Fragment : ", e.toString());

			}
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
		if (fragmentPosition > 99) {
			// item.setVisible(false);
			miActionCancelItem.setVisible(false);
			miActionDraftItem.setVisible(false);
			miActionDoneItem.setVisible(false);
		}

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * First it will check whether the form is locked or not. If form is locked
	 * then it display message that form is locked and it can't open
	 * FormListActivity. else it takes user to take picture after clicking the
	 * take picture icon present in every row of forms table in the
	 * FormListActivity.
	 * 
	 * @param form
	 */
	static final int REQUEST_TAKE_PHOTO = 1;
	public static String mCurrentPhotoPath;
	public static FilledForm filledFormForAssociatedImage = null;

	public void takePicture(FilledForm filledForm) {

		filledFormForAssociatedImage = filledForm;

		// startActivity(intent);
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// L.fv("stp 2 : starting take picture activity");
			// Create the File where the photo should go
			File photoFile = null;
			try {
				// L.fv("step 3 : creating take picture file");
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				ex.printStackTrace();
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				// L.fv("step 4 : saving take picture activity");
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

			}
			/*
			 * if(vesselInspectionTab == null){ vesselInspectionTab = new
			 * VesselInspectionTab(); } vesselInspectionTab.refreshAdapter();
			 * 
			 * if(marpolInspectionTab == null){ marpolInspectionTab = new
			 * MarpolInspectionTab(); } marpolInspectionTab.refreshAdapter();
			 */
		}
	}

	/**
	 * View images as gallery
	 * 
	 * @param filledForm
	 */
	public void viewPicture(FilledForm filledForm) {
		// Intent intent = new Intent(this, ViewImageActivity.class);
		Intent intent = new Intent(this, ImageFullView.class);

		intent.putExtra("strFilledFormId", filledForm.getiFilledFormId());
		intent.putExtra("imgPosition", "0");
		startActivity(intent);
	}

	/**
	 * View images as gallery
	 * 
	 * @param filledForm
	 */
	public void viewPictureForGl(FilledForm filledForm, int pos) {
		// Intent intent = new Intent(this, ViewImageActivity.class);
		Intent intent = new Intent(this, ImageFullView.class);

		intent.putExtra("strFilledFormId", filledForm.getiFilledFormId());
		intent.putExtra("imgPosition", String.valueOf(pos));
		startActivity(intent);
	}

	/**
	 * @author ripunjay This method call when images done.
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// L.fv("Entering in onActivityResult method and requestCode is "+requestCode);
		File photoFile = new File(mCurrentPhotoPath);
		String relativePath = "";
		// Check which request we're responding to
		if (requestCode == REQUEST_TAKE_PHOTO) {
			// Make sure the request was successful
			// L.fv("after comparing requestCode and REQUEST_TAKE_PHOTO is ");
			if (resultCode == RESULT_OK) {

				// L.fv("after comparing resultCode and RESULT_OK is ");
				// The user picked a contact.
				// The Intent's data Uri identifies which contact was selected.
				// Bundle extras = data.getExtras();
				// Bitmap imageBitmap = (Bitmap) extras.get("data");
				// mImageView.setImageBitmap(imageBitmap);

				/**
				 * Below code for resize and compress image.
				 */
				try {

					Double heightWidth = Double.parseDouble(getResources()
							.getString(R.string.regularHeightWidth).trim());
					Double height = 870.0;
					Double width = 695.0;
					Double aspectratio = 1.0;
					DecimalFormat decFormat = new DecimalFormat("##");
					relativePath = photoFile.getAbsolutePath().replace(
							"/file:", "");

					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inJustDecodeBounds = true;

					// Returns null, sizes are in the options variable
					BitmapFactory.decodeFile(relativePath, options);
					int actualWidth = options.outWidth;
					int actualHeight = options.outHeight;
					// If you want, the MIME type will also be decoded (if
					// possible)
					String type = options.outMimeType;
					

					// BufferedImage bimg = ImageIO.read(new
					// File(relativePath));

					if (actualHeight > heightWidth || actualWidth > heightWidth) {
						if (actualHeight > actualWidth) {
							height = heightWidth;
							aspectratio = (double) actualHeight
									/ (double) actualWidth;
							width = height / aspectratio;
						} else {
							width = heightWidth;
							aspectratio = (double) actualWidth
									/ (double) actualHeight;
							height = width / aspectratio;
						}
					} else {
						height = (double) actualHeight;
						width = (double) actualWidth;
					}

					Bitmap imageBitmap = decodeSampledBitmapFromUri(options,
							relativePath, Integer.parseInt(decFormat.format(width)),
							Integer.parseInt(decFormat.format(height)));
					Bitmap scaledimageBitmap = Bitmap.createScaledBitmap(imageBitmap,
							Integer.parseInt(decFormat.format(width)),
							Integer.parseInt(decFormat.format(height)), true);
					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					scaledimageBitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

					String relativePathArr[] = relativePath.split("/vi/");
					if (relativePathArr != null && relativePathArr.length > 0) {
						relativePath = relativePathArr[0] + "/vi/process/"
								+ relativePathArr[1];
					}

					String procesImageDirpath = relativePath.substring(0,
							relativePath.lastIndexOf("/"));
					File storageDir = new File(procesImageDirpath);
					if (!storageDir.exists()) {

						storageDir.mkdirs();
					}

					File f = new File(relativePath);
					if (!f.exists()) {
						f.createNewFile();
					}
					FileOutputStream fo = new FileOutputStream(f);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// =================End resizing=============================

				/**
				 * Saving data in formimages table.
				 */
				// L.fv("step 4.1 : saving image in db "+REQUEST_TAKE_PHOTO);

				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				FilledFormImages filledFormImages = new FilledFormImages();
				String strformImageId = CommonUtil
						.getMacId(getApplicationContext())
						+ ""
						+ new Date().getTime();
				filledFormImages.setStrFilledFormImageId(strformImageId);
				filledFormImages.setCreatedDate(formate.format(new Date()));
				filledFormImages.setModifiedDate(formate.format(new Date()));
				filledFormImages.setFlgDeleted(0);
				filledFormImages.setFlgIsDirty(1);
				CommonUtil.setEditedData(getApplicationContext(), true);
				filledFormImages.setiFilledFormId(filledFormForAssociatedImage
						.getiFilledFormId());
				filledFormImages
						.setiFormSectionItemId(filledFormForAssociatedImage
								.getiFormSectionItemId());
				// filledFormImages.setiVesselInspectionId(filledFormForAssociatedImage.getiVesselInspectionId());
				// filledFormImages.setiVesselInspectionId(getIntent().getStringExtra("strVesselInspectionId"));
				filledFormImages.setiVesselInspectionId(CommonUtil
						.getVesselInspectionId(getApplicationContext()));

				filledFormImages.setStrRelativeImagePath(relativePath);
				filledFormImages.setStrImagepath(photoFile.getAbsolutePath());
				
				filledFormImages.setDeviceRelativeImagePath(relativePath);
				filledFormImages.setDeviceOriginalImagePath(photoFile.getAbsolutePath());
				
				String [] fileName = photoFile.getAbsolutePath().split("/");
				
				filledFormImages.setStrImageName(fileName != null ? fileName[fileName.length -1] : "Image");
				filledFormImages.setiShipId(Integer.parseInt(CommonUtil
						.getShipId(context).toString()));

				// filledFormImages.setiTenantId(28);
				filledFormImages.setiTenantId(Integer.parseInt(CommonUtil
						.getTenantId(context)));
				filledFormImages.setiFormSectionId(filledFormForAssociatedImage
						.getiFormSectionId());

				DBManager dbManager = new DBManager(getApplicationContext());
				dbManager.open();
				L.fv("step 4.2 : creating dbmanager ");

				int sequence = dbManager
						.getVesselInspectionImageSequence(
								CommonUtil
										.getVesselInspectionId(getApplicationContext()),
								filledFormImages.getiFormSectionItemId());
				filledFormImages.setSequence(sequence);

				long result = dbManager
						.insertFilldFormImageTable(filledFormImages);
				/*
				 * List<FilledForm> filledFormList =
				 * dbManager.getFilledFormDataById(filledFormForAssociatedImage
				 * .getiFilledFormId());
				 * 
				 * if(filledFormList != null && filledFormList.size() > 0){
				 * FilledForm filledForm = filledFormList.get(0);
				 * filledForm.setFlgIsDirty(1);
				 * dbManager.updateFilldFormTable(filledForm); }
				 */

				dbManager.close();
				L.fv("step 4.3 ripunjay : after insert data in db " + result);

				List<FilledFormImages> fiList = new ArrayList<FilledFormImages>();
				dbManager.open();
				fiList = dbManager.getFilledFormImagesData();
				dbManager.close();
				L.fv("step 4.4 : geting image data from db with fiList "
						+ fiList.size());

				// Call this method for add image to gallery.
				galleryAddPic();

				// refresh adapter
				if (viTabFlag != null && !"".equals(viTabFlag)
						&& viTabFlag.equalsIgnoreCase("vi")) {
					VesselInspectionTab.showAllGroup = 1;
					VesselInspectionTab.refreshAdapter();
					viTabFlag = "";
				} else if (viTabFlag != null && !"".equals(viTabFlag)
						&& viTabFlag.equalsIgnoreCase("mi")) {
					MarpolInspectionTab.showAllGroup = 1;
					MarpolInspectionTab.refreshAdapter();
					viTabFlag = "";
				}

			} else {
				// L.fv("File exist on actual path."+photoFile.getAbsolutePath()
				// +" and file exist +"+photoFile.exists());
				if (photoFile.exists()) {
					// L.fv("Ready for temp file delete from actual path."+photoFile.getAbsolutePath());
					photoFile.delete();
				}
			}
			filledFormForAssociatedImage = null;
		}
	}

	/**
	 * @author ripujay
	 * @return
	 * @throws IOException
	 *             create temp image file on specified location
	 */
	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String shipId = CommonUtil.getShipId(context);
		String formId = filledFormForAssociatedImage != null ? filledFormForAssociatedImage
				.getiFilledFormId() : "form";
		File storageDir = new File(Environment.getExternalStorageDirectory(),
				"/theark/vi/" + shipId + "/" + formId);

		if (!storageDir.exists()) {

			storageDir.mkdirs();
		}

		File image = new File(storageDir.getAbsoluteFile() + "/"
				+ imageFileName + ".jpg");

		/*
		 * File image = File.createTempFile( imageFileName, prefix ".jpg",
		 * suffix storageDir directory );
		 */

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		// L.fv("creating image file on given location "+mCurrentPhotoPath);
		return image;
	}

	/**
	 * @author ripunjay this method for image add to gallery.
	 */
	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	@Override
	public void onBackPressed() {

		// super.onBackPressed();
		if (fragmentPosition < 3) {
			DBManager db = new DBManager(context);
			db.open();
			List<VesselInspection> viDraftList = new ArrayList<VesselInspection>();
			viDraftList = db.getVesselInspectionDataByflgStatus(0);
			db.close();

			if (CommonUtil.isEditedData(context))
				launchPopUpForBackButton(
						getResources().getString(R.string.backButtonMsg), 1);
			else {
				/*if (!CommonUtil.isEditedData(context) && viDraftList != null
						&& viDraftList.size() > 1) {*/
					Intent i = new Intent(VessalInspectionHome.this,
							VesselInspectionStartActivity.class);
					startActivity(i);
					finish();
				//}

			}

			//CommonUtil.setEditedData(context, false);
		}

	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpEnablingNetwork(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	public Bitmap decodeSampledBitmapFromUri(BitmapFactory.Options options, String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		//////////final BitmapFactory.Options options = new BitmapFactory.Options();
		/////////options.inJustDecodeBounds = true;
		//BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, reqWidth,	reqHeight);		
		
		// this options allow android to claim the bitmap memory if it runs low on memory
        //options.inPurgeable = true;
        //options.inInputShareable = true;
        //options.inTempStorage = new byte[16 * 1024];
        
		try {
			bm = BitmapFactory.decodeFile(path, options);
			//bm = BitmapFactory.decodeFile(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

	/*
	 * private Drawable getDrawableResourceByName(String name ) { String
	 * packageName = getPackageName(); int resId =
	 * getResources().getIdentifier(name, "drawable", packageName); return
	 * getResources().getDrawable(resId); }
	 */

	private int getDrawableResourceByName(String name) {
		String packageName = getPackageName();
		int resId = getResources().getIdentifier(name, "drawable", packageName);
		return resId;
	}
}
