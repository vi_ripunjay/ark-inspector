package com.tams.vessalinspection;

public class Simple {

    private boolean flgChecked = true;
    private String strDescription;
    private String strTitle;
    
    public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public Simple(){}
    public Simple(String strTitle, boolean flgChecked, String strDescription) {
		this.strTitle = strTitle;
    	this.flgChecked = flgChecked;
		this.strDescription = strDescription;

    }
	public boolean isFlgChecked() {
		return flgChecked;
	}
	public void setFlgChecked(boolean flgChecked) {
		this.flgChecked = flgChecked;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

}
