/**
 * @file  SyncHandler . java
 * 
 * @brief  This class work as a handler for incremental sync
 * 
 * @author Ripunjay Shukla
 * 
 * @date : Mar 01,2015
 */
package com.tams.vessalinspection;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.tams.service.BackGroundTask;
import com.tams.utils.CommonUtil;

public class SyncHandler {

	public static Context context = null;
	public static final int MSG_UPDATE_VSL_INSPECTION = 1;
	public static final int MSG_UPDATE_FILLEDFORM_SYNC = 2;
	public static final int MSG_UPDATE_FILLED_CHECKLIST_SYNC = 3;
	public static final int MSG_SEND_FILLED_FORM_SYNC = 4;
	public static final int MSG_UPDATE_TAG = 5;
	public static final int MSG_FILLED_FORMS = 6;
	public static final int MSG_GET_UPDATED_IMAGE = 7;
	public static final int MSG_SEND_FORM_IMAGES_SYNC = 8;
	public static final int MSG_UPDATE_FORM_IMAGES_SYNC = 9;
	public static final int MSG_UPDATE_CHECKLISTSECTION_DATA = 10;
	public static final int MSG_UPDATE_SHIP_MASETR = 11;
	public static final int MSG_UPDATE_CHECKLIST_SECTION_ITEM_DATA = 12;
	public static final int MSG_UPDATE_FORM_SECTION = 13;
	public static final int MSG_UPDATE_FORM_SECTION_ITEM = 14;
	public static final int MSG_UPDATE_FORM_CATEGORY = 15;
	public static final int MSG_UPDATE_VI_DATA = 16;
	public static final int MSG_UPDATE_VI_FILLED_FORM = 17;
	public static final int MSG_UPDATE_VI_FILLED_FORM_IMAGE = 18;
	public static final int MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST = 19;
	public static final int MSG_GET_ROLE_TEMPLATE = 20;
	public static final int MSG_MATCH_MANUAL_SYNC_HISTORY = 21;
	public static final int MSG_GET_SHIP_EXIST = 22;
	public static final int MSG_UPDATE_FILLED_FORM_HEADER = 23;
	public static final int MSG_UPDATE_FILLED_FORM_FOOTER = 24;
	public static final int MSG_GET_FILLED_FORM_HEADER = 25;
	public static final int MSG_GET_FILLED_FORM_FOOTER = 26;
	public static final int MSG_GET_FILLED_FORM_DESC = 27;
	public static final int MSG_UPDATE_FILLED_FORM_DESC = 28;
	public static final int MSG_GET_FORM_SECTION_DYNAMIC_ITEM = 29;
	public static final int MSG_GET_VI_TEMPLATE_VERSION = 30;
	static String toastMsg = "";

	/**
	 * This will initialize the context on creation of syncHandler object.
	 * 
	 * @param contextTemp
	 */
	public SyncHandler(Context contextTemp) {
		context = contextTemp;
	}

	/**
	 * This method will take string as input,add input to previous string and
	 * gives output as a string.
	 * 
	 * @param toast
	 * @return string
	 */
	public static String buildToast(String toast) {
		toastMsg = toastMsg + toast;
		return toastMsg;
	}

	/**
	 * This method will clear the current string.
	 */
	public static void releaseToast() {
		toastMsg = "";
	}

	/**
	 * @brief A handler which perform the task based on the message.what it
	 *        gets. e.g: message.what = MSG_UPDATE_FORMTEMPLATE_SYNC then it
	 *        calls updateAllFormTemplates method from web service
	 */
	public static Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == MSG_UPDATE_VSL_INSPECTION) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkSendVesselInspection = true;
				Log.i("ship Id and deviceId for get updated user : ", "shipId="
						+ CommonUtil.getShipId(context) + " device Id = "
						+ CommonUtil.getIMEI(context));
				backGroundTask.sendVesselInspection(
						CommonUtil.getIMEI(context),
						CommonUtil.getShipId(context));
				Log.i("ship Id and deviceId for get updated user after function call : ",
						"shipId=" + CommonUtil.getShipId(context)
								+ " device Id = " + CommonUtil.getIMEI(context));

			} else if (msg.what == MSG_SEND_FILLED_FORM_SYNC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkSendFilledForm = true;
				backGroundTask.sendFilledForms(CommonUtil.getIMEI(context),
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_UPDATE_FILLED_CHECKLIST_SYNC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkSendFilledCheckList = true;
				backGroundTask.sendFilledCheckList(CommonUtil.getIMEI(context),
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_GET_UPDATED_IMAGE) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkImageUpdate = true;
				backGroundTask.getUpdateImage(CommonUtil.getShipId(context),
						CommonUtil.getIMEI(context));
			} else if (msg.what == MSG_SEND_FORM_IMAGES_SYNC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkFormImageUpdate = true;
				backGroundTask.sendFormImages(CommonUtil.getShipId(context),
						CommonUtil.getMacId(context));
			} else if (msg.what == MSG_UPDATE_FORM_IMAGES_SYNC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkFormImageUpdateByWeb = true;
				backGroundTask.getUpdatedFormImages(
						CommonUtil.getIMEI(context),
						CommonUtil.getShipId(context));
			}

			/** Start : Added by Pushkar Mishra **/

			else if (msg.what == MSG_UPDATE_CHECKLISTSECTION_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetCheckListSection = true;
				backGroundTask.getCheckListSection("",
						CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_SHIP_MASETR) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetShipMaster = true;
				backGroundTask.getAllShips(CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_CHECKLIST_SECTION_ITEM_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetCheckListSectionItem = true;
				backGroundTask.getCheckListSectionItem("",
						CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_FORM_SECTION) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetFormSection = true;
				backGroundTask.getFormSection("",
						CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_FORM_SECTION_ITEM) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetFormSectionItem = true;
				backGroundTask.getFormSectionItem("",
						CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_FORM_CATEGORY) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetFormCategory = true;
				backGroundTask.getFormCategory("",
						CommonUtil.getTenantId(context));
			} else if (msg.what == MSG_UPDATE_VI_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetUpdatedViData = true;
				backGroundTask.getUpdatedVIData(CommonUtil.getUserId(context));
			} else if (msg.what == MSG_UPDATE_VI_FILLED_FORM) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetUpdatedFilledFormData = true;
				backGroundTask.getUpdatedFilledForm("",
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_UPDATE_VI_FILLED_FORM_IMAGE) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkFormImageUpdateByWeb = true;
				backGroundTask.getUpdatedFormImages("",
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_UPDATE_VI_FILLED_FORM_CHECK_LIST) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetUpdatedFilledFormCheckListData = true;
				backGroundTask.getUpdatedFilledFormCheckList("",
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_GET_ROLE_TEMPLATE) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetRoleTemplateData = true;
				backGroundTask.getRoleTemplateData(CommonUtil
						.getTenantId(context));
			} else if (msg.what == MSG_MATCH_MANUAL_SYNC_HISTORY) {
				// CommonUtil.setUserLogin(context, "0");
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkUpdateManualSyncHistory = true;
				backGroundTask.matchManualSyncHistory(
						CommonUtil.getShipId(context),
						CommonUtil.getMacId(context));
			} else if (msg.what == MSG_GET_SHIP_EXIST) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetShipExist = true;
				backGroundTask.getShipExistOrNot(CommonUtil
						.getActivatingShip(context));
			} else if (msg.what == MSG_UPDATE_FILLED_FORM_DESC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkUpdateFilledFormHeader = true;
				backGroundTask.sendFilledFormDesc();
			} else if (msg.what == MSG_UPDATE_FILLED_FORM_FOOTER) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkUpdateFilledFormFooter = true;
				backGroundTask.sendFilledFormFooter();
			} else if (msg.what == MSG_GET_FILLED_FORM_DESC) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetFilledFormHeader = true;
				backGroundTask.getFilledFormDesc(CommonUtil.getShipId(context));
			} else if (msg.what == MSG_GET_FILLED_FORM_FOOTER) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetFilledFormFooter = true;
				backGroundTask.getFilledFormFooter(CommonUtil
						.getShipId(context));
			} else if (msg.what == MSG_GET_FORM_SECTION_DYNAMIC_ITEM) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetShipFormSectionItem = true;
				backGroundTask.getShipFormSectionItem(
						CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context));
			} else if (msg.what == MSG_GET_VI_TEMPLATE_VERSION) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetVITemplateVerison = true;
				backGroundTask.getVITemlateVersion();
			}

			/** End : Added by Pushkar Mishra **/
			return true;
		}

	});
}
