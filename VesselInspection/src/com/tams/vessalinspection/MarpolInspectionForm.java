package com.tams.vessalinspection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.tams.adapter.MarpolInspectionFormAdapter;
import com.tams.model.FilledForm;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

public class MarpolInspectionForm extends Fragment {
	ExpandableListView marpolFormList;
	Button submitButton;
	Intent intentMI;

	List<FormSection> formSection = new ArrayList<FormSection>();
	List<Object> formSectionItem = new ArrayList<Object>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		intentMI = getActivity().getIntent();
		String strVesselInspectionId = intentMI.getStringExtra("strVesselInspectionId");
		
		View vesselInspec = inflater.inflate(R.layout.marpol_inspection_frag,
				container, false);

		marpolFormList = (ExpandableListView) vesselInspec
				.findViewById(R.id.marpolInspectionFormId);
	/*	submitButton = (Button) vesselInspec.findViewById(R.id.btnMISubmit);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Object fsi : formSectionItem) {
					@SuppressWarnings("unchecked")
					List<FilledForm> fildFrmItmList = (ArrayList<FilledForm>) fsi;
					for (FilledForm fldFrm : fildFrmItmList) {
						System.out.println("child Title : "
								+ fldFrm.getStrHeaderDesc());
						System.out.println("child footer : "
								+ fldFrm.getStrFooterDesc());
						System.out.println("child Desc : "
								+ fldFrm.getCreatedBy());
						DBManager db = new DBManager(getActivity());
						db.open();
						db.insertFilldFormTable(fldFrm);
						db.close();
					}
				}

			}
		});*/

		setGroupData(strVesselInspectionId);
		// setChildGroupData();

		MarpolInspectionFormAdapter mifAdapter = new MarpolInspectionFormAdapter(0,getActivity(),
				formSection, formSectionItem);
		mifAdapter.setInflater(
				(LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE), getActivity());
		marpolFormList.setAdapter(mifAdapter);
		return vesselInspec;
	}

	public void setGroupData(String strVesselInspectionId) {

		formSection = new ArrayList<FormSection>();
		formSectionItem = new ArrayList<Object>();
		
		DBManager db = new DBManager(getActivity());
		db.open();
		List<FormSection> fsList = new ArrayList<FormSection>();
		fsList = db.getFormSectionData("MI");
		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		if (fsList != null && fsList.size() > 0) {

			for (FormSection fs : fsList) {
				formSection.add(fs);

				/**
				 * load form section item
				 */
				List<FormSectionItem> fsiList = new ArrayList<FormSectionItem>();
				fsiList = db.getFormSectionItemDataBySectionId(fs
						.getiFormSectionId());
				// formSectionItem.add(fsiList);
				List<FilledForm> filedFormBySecList = new ArrayList<FilledForm>();
				int seq = 1;
				for (FormSectionItem frmSecitm : fsiList) {
					/**
					 * get filled checkList value on the basis of
					 * vesselInspection, formsection and formsectionitrm
					 */
					List<FilledForm> fldFrmList = new ArrayList<FilledForm>();
					fldFrmList = db.getFilledFormDataByIds(
							strVesselInspectionId, fs.getiFormSectionId(),
							frmSecitm.getiFormSectionItemId());
					if (fldFrmList != null && fldFrmList.size() > 0) {

						filedFormBySecList.addAll(fldFrmList);

					} else {
						/*FilledForm filledForm = new FilledForm(getPk(),
								strVesselInspectionId,
								fs.getiFormSectionId(),
								frmSecitm.getiFormSectionItemId(),
								"Header Desc", "Footer Desc", seq,
								format.format(cudate), format.format(cudate),
								28, 0);*/
						FilledForm filledForm = new FilledForm();
                        filledForm.setiFilledFormId(getPk());
                        filledForm.setiVesselInspectionId(strVesselInspectionId);
                        filledForm.setiFormSectionId(fs.getiFormSectionId());
                        filledForm.setiFormSectionItemId(frmSecitm.getiFormSectionItemId());
                        filledForm.setStrHeaderDesc("");
                        filledForm.setStrFooterDesc("");
                        filledForm.setSequence(seq);
                        filledForm.setCreatedDate(format.format(cudate));
                        filledForm.setModifiedDate(format.format(cudate));
                        filledForm.setiTenantId(Integer.parseInt(CommonUtil.getTenantId(getActivity())));
                        filledForm.setiShipId(Integer.parseInt(CommonUtil.getShipId(getActivity())));
                        filledForm.setCreatedBy(CommonUtil.getUserId(getActivity()));
                        filledForm.setModifiedBy(CommonUtil.getUserId(getActivity()));
                        
						filedFormBySecList.add(filledForm);
					}

					seq++;
				}

				formSectionItem.add(filedFormBySecList);

			}
		}

		db.close();
		/*
		 * groupItem.add("EXTERNAL CONDITION");
		 * groupItem.add("DECK CONDITION & MACHINERY");
		 * groupItem.add("CARGO HANDLING EQUIPMENT");
		 */
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getActivity()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	public void setChildGroupData() {
		/**
		 * Add Data For EXTERNAL CONDITION
		 */
		ArrayList<Simple> child = new ArrayList<Simple>();

		child.add(new Simple("Top sides", false, ""));
		child.add(new Simple("Anti fouling", false, ""));
		child.add(new Simple("Markings and Logos", false, ""));
		child.add(new Simple("Hull openings", false, ""));

		// childItem.add(child);

		/**
		 * Add Data For DECK CONDITION & MACHINERY
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Main deck", false, "test"));
		child.add(new Simple("Focsle", false, ""));
		child.add(new Simple("Accommodation decks", false, ""));
		child.add(new Simple("Poop", false, ""));
		child.add(new Simple("Monkey Island and fittings including masts",
				false, ""));

		// childItem.add(child);
		/**
		 * Add Data For CARGO HANDLING EQUIPMEN
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Loadicator Coamings & compression bars", false,
				""));
		child.add(new Simple(
				"Cargo Securing material (twistlocks, turnbuckles)", false, ""));
		child.add(new Simple(
				"Container base sockets + lashing eyes on pontoons & pedestals",
				false, ""));
		child.add(new Simple("Lashing Bins", false, ""));
		child.add(new Simple(
				"Stevedore Safety arrangements (walkways, ladders, lighting, railings)",
				false, ""));

		// childItem.add(child);

	}

}
