package com.tams.vessalinspection;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tams.adapter.NavDrawerListAdapter;
import com.tams.fragment.AdminFragment;
import com.tams.fragment.Exit;
import com.tams.fragment.VesselInspectionHelp;
import com.tams.fragment.VesselInspectionStartFragment;
import com.tams.model.NavDrawerItem;
import com.tams.model.VesselInspection;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;

public class VesselInspectionStartActivity extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;
	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	Context context;
	Intent intentStart;
	public static MenuItem miActionProgressItem;
	public static MenuItem miActionUndoItem;
	
	/**
	 * Visible in case of 1;
	 * Invisible in case of 0;
	 */
	public static int undoIconVisibleOrNot=0;

	private String strVesselInspectionOldId = "";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = this;
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		context = getApplicationContext();
		CommonUtil.setShipID(context, "");
		AdminFragment.homeOrStart="";
		
		/**
		 * Data for testing Check List :
		 */
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		CommonUtil.setMacID(getApplicationContext(),
				(tm.getDeviceId() != null ? tm.getDeviceId() : "tmpdvid"));		
		
		/**
		 * work on 4.4.2
		 */
		// ab.setDisplayUseLogoEnabled(false);

		ab.setDisplayShowHomeEnabled(false);

		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
				| ActionBar.DISPLAY_SHOW_TITLE);
		
		setContentView(R.layout.activity_start_fragment);

		mTitle = mDrawerTitle = getTitle();
		mTitle = mDrawerTitle = getTitle();

		// load menu icons
		navMenuIcons = getResources().obtainTypedArray(
				R.array.nav_drawer_start_icons);
		// load slide menu items
		navMenuTitles = getResources().getStringArray(
				R.array.nav_drawer_start_items);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_start);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu_start);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));

		/*navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));*/

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));

		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				// invalidateOptionsMenu(); ===================
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		if (Build.VERSION.SDK_INT >= 18) {
			getActionBar().setHomeAsUpIndicator(
					getResources().getDrawable(R.drawable.ic_drawer));
		}

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}

		intentStart = getIntent();
		strVesselInspectionOldId = intentStart
				.getStringExtra("strVesselInspectionId");
		
		String loginFromShip = intentStart.getStringExtra("loginFromShip");
		if(loginFromShip != null && "ship".equalsIgnoreCase(loginFromShip)){
			displayView(1);
		}

	}


	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);

		}
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	@SuppressLint("NewApi")
	public void displayView(int position) {
		// update the main content by replacing fragments

		Fragment fragment = null;

		switch (position) {
		case 0:
			fragment = new VesselInspectionStartFragment();
			AdminFragment.homeOrStart="start";
			break;
		case 1:
			fragment = new AdminFragment();
			AdminFragment.homeOrStart="start";
			VesselInspectionStartActivity.undoIconVisibleOrNot = 0;
			break;
		/*case 2:
			fragment = new VesselInspectionHelp();
			VesselInspectionStartActivity.undoIconVisibleOrNot = 0;
			break;*/
		case 2:
			fragment = new Exit();
			VesselInspectionStartActivity.undoIconVisibleOrNot = 0;
			break;

		default:
			break;
		}

		if (fragment != null) {
			try {
				// set to handle back Button
				FragmentManager fragmentManager = getSupportFragmentManager();

				while (fragmentManager.getBackStackEntryCount() > 1) {
					fragmentManager.popBackStackImmediate();
				}

				fragmentManager.beginTransaction()
						.replace(R.id.frame_container_start, fragment)
						.addToBackStack(null).commitAllowingStateLoss();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				setTitle(navMenuTitles[position]);

				mDrawerLayout.closeDrawer(mDrawerList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("Home Fragment : ", e.toString());

			}
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vessel_inspection_start, menu);
		
		
		miActionProgressItem = menu.findItem(R.id.progressStart);
		miActionUndoItem = menu.findItem(R.id.undoIcon);
		miActionProgressItem.setVisible(false);
		
		if (strVesselInspectionOldId != null
				&& !"".equals(strVesselInspectionOldId)){
			
			if(VesselInspectionStartActivity.undoIconVisibleOrNot == 1){
				//miActionUndoItem.setVisible(true);
				miActionUndoItem.setVisible(false);
			}
			else{
				miActionUndoItem.setVisible(false);
			}			
		}
		else	
		{
			miActionUndoItem.setVisible(false);
		}
		
		return true;
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		int id = item.getItemId();
		DBManager db = new DBManager(getApplicationContext());
		db.open();
		List<VesselInspection> vinsp = new ArrayList<VesselInspection>();
		VesselInspection vi = new VesselInspection();
		vinsp = db.getVesselInspectionDataById(strVesselInspectionOldId);

		if (vinsp != null && vinsp.size() > 0) {
			vi = new VesselInspection();
			vi = vinsp.get(0);
			if (vi.getFlgStatus() == 0) {
				item.setVisible(false);
			}

		}

		if (id == R.id.undoIcon) {
			
			CommonUtil.setShipID(context, String.valueOf(vi.getiShipId()));
			String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
			if(dataSyncMode != null && "cable".equalsIgnoreCase(dataSyncMode)){
				vi.setFlgStatus(0);
				vi.setFlgIsDirty(1);
				vi.setFlgDeleted(0);
				vi.setFlgIsLocked(0);
				db.updateVesselInspectionTable(vi);
				
				Intent i = new Intent(VesselInspectionStartActivity.this,
						VessalInspectionHome.class);
				i.putExtra("strVesselInspectionId", strVesselInspectionOldId);
				startActivity(i);
				finish();
				
				item.setVisible(false);
				
			}
			else {			
				if(CommonUtil.checkConnectivity(VesselInspectionStartActivity.this)){
					
					vi.setFlgStatus(0);
					vi.setFlgIsDirty(1);
					vi.setFlgDeleted(0);
					vi.setFlgIsLocked(0);
					db.updateVesselInspectionTable(vi);
					
					
					SyncHandler.context = context;
		            Message message = new Message();
		            message.what = SyncHandler.MSG_UPDATE_VSL_INSPECTION;
		            SyncHandler.handler.sendMessage(message);
		           
		            VessalInspectionHome.miActionProgressItem.setVisible(true);
		            VessalInspectionHome.miActionCancelItem.setVisible(false);
		            VessalInspectionHome.miActionDraftItem.setVisible(false);
		            VessalInspectionHome.miActionDoneItem.setVisible(false);
		            VessalInspectionHome.sendingStatus = 0;
	
				Intent i = new Intent(VesselInspectionStartActivity.this,
						VessalInspectionHome.class);
				i.putExtra("strVesselInspectionId", strVesselInspectionOldId);
				startActivity(i);
				finish();
				
				item.setVisible(false);
				}
			}
			return true;
		}
		db.close();
		return true;

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}

}
