package com.tams.vessalinspection;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;

public class MainActivity extends FragmentActivity {
	ViewPager Tab;
	TabPagerAdapter TabAdapter;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);
		
		setContentView(R.layout.activity_main);

		TabAdapter = new TabPagerAdapter(getSupportFragmentManager());

		Tab = (ViewPager) findViewById(R.id.pager);
		Tab.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

				actionBar = getActionBar();
				actionBar.setSelectedNavigationItem(position);
			}
		});
		Tab.setAdapter(TabAdapter);

		actionBar = getActionBar();
		// Enable Tabs on Action Bar
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabReselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

				Tab.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}
		};
		// Add New Tab
		actionBar.addTab(actionBar.newTab().setText("Check List")
				.setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setText("Vessel Inspection")
				.setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setText("Marpol Inspection")
				.setTabListener(tabListener));

	}

	/**
	 * First it will check whether the form is locked or not. If form is locked
	 * then it display message that form is locked and it can't open
	 * FormListActivity. else it takes user to take picture after clicking the
	 * take picture icon present in every row of forms table in the
	 * FormListActivity.
	 * 
	 * @param form
	 */
	static final int REQUEST_TAKE_PHOTO = 1;
	String mCurrentPhotoPath;
	FilledForm filledFormForAssociatedImage = null;

	public void takePicture(FilledForm filledForm) {

		filledFormForAssociatedImage = filledForm;

		// startActivity(intent);
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// L.fv("stp 2 : starting take picture activity");
			// Create the File where the photo should go
			File photoFile = null;
			try {
				// L.fv("step 3 : creating take picture file");
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				ex.printStackTrace();
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				// L.fv("step 4 : saving take picture activity");
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

			}
		}
	}

	/**
	 * View images as gallery
	 * 
	 * @param filledForm
	 */
	public void viewPicture(FilledForm filledForm) {
		// Intent intent = new Intent(this, ViewImageActivity.class);
		Intent intent = new Intent(this, ImageFullView.class);

		intent.putExtra("strFilledFormId", filledForm.getiFilledFormId());
		startActivity(intent);
	}

	/**
	 * @author ripunjay This method call when images done.
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// L.fv("Entering in onActivityResult method and requestCode is "+requestCode);
		File photoFile = new File(mCurrentPhotoPath);
		// Check which request we're responding to
		if (requestCode == REQUEST_TAKE_PHOTO) {
			// Make sure the request was successful
			// L.fv("after comparing requestCode and REQUEST_TAKE_PHOTO is ");
			if (resultCode == RESULT_OK) {
				// L.fv("after comparing resultCode and RESULT_OK is ");
				// The user picked a contact.
				// The Intent's data Uri identifies which contact was selected.
				// Bundle extras = data.getExtras();
				// Bitmap imageBitmap = (Bitmap) extras.get("data");
				// mImageView.setImageBitmap(imageBitmap);

				/**
				 * Saving data in formimages table.
				 */
				// L.fv("step 4.1 : saving image in db "+REQUEST_TAKE_PHOTO);

				SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
				FilledFormImages filledFormImages = new FilledFormImages();
				String strformImageId = CommonUtil
						.getMacId(getApplicationContext())
						+ ""
						+ new Date().getTime();
				filledFormImages.setStrFilledFormImageId(strformImageId);
				filledFormImages.setCreatedDate(formate.format(new Date()));
				filledFormImages.setFlgDeleted(0);
				filledFormImages.setFlgIsDirty(1);
				CommonUtil.setEditedData(getApplicationContext(), true);
				filledFormImages.setiFilledFormId(filledFormForAssociatedImage
						.getiFilledFormId());
				// filledFormImages.setRelativeFormImagePath(photoFile.getAbsolutePath());
				filledFormImages.setStrImagepath(photoFile.getAbsolutePath());
				filledFormImages.setStrImageName("Image");
				filledFormImages.setiShipId(0);
				filledFormImages.setiTenantId(28);
				filledFormImages.setiFormSectionId(filledFormForAssociatedImage.getiFormSectionId());

				DBManager dbManager = new DBManager(getApplicationContext());
				dbManager.open();
				L.fv("step 4.2 : creating dbmanager ");
				long result = dbManager
						.insertFilldFormImageTable(filledFormImages);
				dbManager.close();
				L.fv("step 4.3 ripunjay : after insert data in db " + result);

				List<FilledFormImages> fiList = new ArrayList<FilledFormImages>();
				dbManager.open();
				fiList = dbManager.getFilledFormImagesData();
				dbManager.close();
				L.fv("step 4.4 : geting image data from db with fiList "
						+ fiList.size());

				// Call this method for add image to gallery.
				galleryAddPic();

			} else {
				// L.fv("File exist on actual path."+photoFile.getAbsolutePath()
				// +" and file exist +"+photoFile.exists());
				if (photoFile.exists()) {
					// L.fv("Ready for temp file delete from actual path."+photoFile.getAbsolutePath());
					photoFile.delete();
				}
			}
			filledFormForAssociatedImage = null;
		}
	}

	/**
	 * @author ripujay
	 * @return
	 * @throws IOException
	 *             create temp image file on specified location
	 */
	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String shipId = "0";
		String formId = filledFormForAssociatedImage != null ? filledFormForAssociatedImage
				.getiFilledFormId() : "form";
		File storageDir = new File(Environment.getExternalStorageDirectory(),
				"/theark/vi/" + shipId + "/" + formId);

		if (!storageDir.exists()) {

			storageDir.mkdirs();
		}

		File image = new File(storageDir.getAbsoluteFile() + "/"
				+ imageFileName + ".jpg");

		/*
		 * File image = File.createTempFile( imageFileName, prefix ".jpg",
		 * suffix storageDir directory );
		 */

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		// L.fv("creating image file on given location "+mCurrentPhotoPath);
		return image;
	}

	/**
	 * @author ripunjay this method for image add to gallery.
	 */
	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

}
