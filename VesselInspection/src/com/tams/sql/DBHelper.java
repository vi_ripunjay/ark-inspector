/**
 * @file  DBHelper . java
 * 
 * @author Ripunjay Shukla
 * 
 * @date : May 11,2015
 */
package com.tams.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-13
	 */

	// Database name and version no
	public static final String DATABASE_NAME = "VesselInspection.db";
	private static final int DATABASE_VERSION = 3;

	// Table of vessel inspection
	public static final String CHECKLIST_SECTION = "CheckListSection";
	public static final String CHECKLIST_SECTION_ITEMS = "CheckListSectionItems";
	public static final String FILLED_CHECKLIST = "VesselInspectionCheckList";
	public static final String TABLE_FORM_CATEGORY = "FormCategory";

	public static final String FORM_SECTION = "FormSection";
	public static final String TABLE_FORM_SECTION_ITEM = "FormSectionItems";
	public static final String FILLED_FORM = "VesselInspectionFormList";
	public static final String FILLED_FORM_IMAGE = "VesselInspectionImage";
	public static final String SHIP_MASTER = "ShipMaster";
	public static final String TENANT = "Tenant";
	public static final String VESSEL_INSPECTION = "VesselInspection";
	public static final String TABLE_USERS = "UserMaster";
	public static final String TABLE_ROLE = "Roles";
	public static final String TABLE_USER_SERVICETERM = "UserServiceTerm";
	public static final String TABLE_ROLE_TEMPLATE = "VIRoleTemplate";
	public static final String TABLE_VI_TEMPLATE = "VesselInspectionTemplate";

	public static final String TABLE_SHIP_FORM_SECTION_ITEM = "ShipFormSectionItem";
	public static final String SHIP_TYPE_ID = "iShipTypeId";
	public static final String ASSOCIATEED_TENANT = "associatedTenant";
	public static final String DYNAMIC_FORM_LIST_ID = "iDynamicFormListId";

	public static final String TABLE_VesselInspectionFormListDesc = "VesselInspectionFormListDesc";
	public static final String TABLE_VesselInspectionFormListFooter = "VesselInspectionFormListFooter";

	public static final String VesselInspectionFormListDesc_ID = "iVesselInspectionFormListDescId";
	public static final String VesselInspectionFormListFooter_ID = "iVesselInspectionFormListFooterId";

	public static final String STR_DESC_TYPE = "strDescType";

	public static final String STR_DESC = "strDescription";

	// Common Columns fields
	public static final String MAC_ID = "strMacId";
	public static final String VERSION = "version";
	public static final String FLG_DELETED = "flgDeleted";
	public static final String FLG_IS_DIRTY = "flgIsDirty";
	public static final String FLG_DEVICE_IS_DIRTY = "flgIsDeviceDirty";
	public static final String FLG_STATUS = "flgStatus";
	public static final String FLG_REVERTED = "flgReverted";
	public static final String FLG_LARGE = "flgIsLarge";
	public static final String CREATED_DATE = "dtCreated";
	public static final String FLG_IS_EDITED = "flgIsEdited";
	public static final String MODIFIED_DATE = "dtUpdated";
	public static final String CREATED_BY = "createdBy";
	public static final String MODIFIED_BY = "updatedBy";
	public static final String SEQUENCE_NUMBER = "sequence";
	public static final String FIRST_AND_LAST_NAME = "strFirstNameLastName";
	public static final String TENANT_ID = "iTenantId";
	public static final String SHIP_ID = "iShipId";

	public static final String MODIFIED_mod_DATE = "modifiedDate";
	public static final String MODIFIED_mod_BY = "modifiedBy";
	public static final String CREATED_mod_DATE = "createdDate";

	public static final String INSPECTED_BY = "inspectedBy";
	public static final String REVIEWED_BY = "reviewedBy";
	public static final String APPROVED_BY = "approvedBy";
	public static final String ACCEPTED_BY = "acceptedBy";
	public static final String COMPLETED_BY = "completedBy";
	public static final String Executive_Summary = "strExecutiveSummary";

	// sync_status table fields
	public static final String TABLE_SYNC_STATUS = "SyncStatus";
	public static final String SYNC_STATUS_ID = "iSyncStatusId";
	public static final String SYNC_DATE = "dtSyncDate";
	public static final String SYNC_MODE = "strSyncMode";
	public static final String DATA_SYNC_MODE = "dataSyncMode";
	public static final String SERVER_ADDRESS = "server_address";

	// FormCategory table fields
	public static final String FORM_CATEGORY_ID = "iFormCategoryId";
	public static final String FORM_CATEGORY_NAME = "strCategoryName";
	public static final String FORM_CATEGORY_CODE = "strCategoryCode";
	public static final String STR_DESCRIPTION = "strDescription";
	public static final String FORM_CATEGORY_SEQUENCE = "sequence";

	// CheckListSection table fields
	public static final String CHECKLIST_SECTION_ID = "iCheckListSectionId";
	public static final String SECTION_NAME = "strSectionName";
	public static final String REMARKS = "strRemarks";

	// CheckListSectionItems table fields
	public static final String CHECKLIST_SECTIONITEMS_ID = "iCheckListSectionItemsId";
	public static final String ITEMS_NAME = "strItemNames";
	public static final String ITEM_CONDITION = "strItemConditions";
	public static final String COMMENTORY = "strCommentory";
	public static final String ITEM_REMARKS = "strItemRemarks";
	public static final String FLG_INSPECTED = "flgInspected";

	// FilledCheckList table fields
	public static final String FILLED_CHECKLIST_ID = "iVesselInspectionCheckListId";
	public static final String FLG_CHECKED = "flgChecked";
	public static final String viflDesc = "strDescription";

	// FilledForm table fields
	public static final String FILLED_FORM_ID = "iVesselInspectionFormListId";
	public static final String FORM_HEADER_DESC = "strHeader";
	public static final String FORM_FOOTER_DESC = "strFooter";
	public static final String FLG_IS_HEADER_EDITED = "flgIsHeaderEdited";
	public static final String FLG_IS_FOOTER_EDITED = "flgIsFooterEdited";

	// FormSection table fields
	public static final String FORM_SECTION_ID = "iFormSectionId";
	public static final String FORM_SECTION_NAME = "strSectionName";
	public static final String FORM_CATEGORY = "strFormCategory";

	// FormSectionItem table fields
	public static final String FORM_SECTION_ITEM_ID = "iFormSectionItemsId";
	public static final String FORM_SECTION_ITEM_NAME = "strSectionItemNames";

	public static final String SHIP_FORM_SECTION_ITEM_ID = "iShipFormSectionItemId";

	// ShipMaster table fields
	public static final String SHIP_NAME = "strShipName";

	// Tenant table fields
	public static final String TENANT_NAME = "strCompanyName";

	// vesselInspection table fields
	public static final String VESSEL_INSPECTIONS_ID = "iVesselInspectionId";
	public static final String VESSEL_INSPECTIONS_TITLE = "strVesselInspectionName";
	public static final String VESSEL_INSPECTION_DATE = "dtInspectionDate";
	public static final String FLG_IS_LOCKED = "flgIsLocked";
	public static final String VESSEL_INSPECTIONS_STATUS_ID = "iVesselInspectionStatusId";

	// Users Table fields
	public static final String USER_ID = "user_id";
	public static final String USER_NAME = "user_name";
	public static final String PASSWORD = "password";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String ISDELETED = "is_deleted_flag";

	// Role table fields
	public static final String ROLE_ID = "iRoleId";
	public static final String ROLE_NAME = "strRole";

	// UserServiceTerm table fields
	public static final String USER_SERVICETERM_ID = "UserServiceTermId";
	public static final String START_DATE = "dtTermFrom";
	public static final String END_DATE = "dtTermTo";

	// FilledFormImage table fields
	public static final String FILLED_FORM_IMAGE_ID = "iVesselInspectionImageId";
	public static final String IMAGE_PATH = "strImagePath";
	public static final String IMAGE_NAME = "strFileName";// strImageName
	public static final String IMAGE_RELATIVE_PATH = "strRelativeFileName";// "strImageRelativePath";
	public static final String ORIGINAL_PATH = "strOriginalImagePath";
	public static final String TEMP_FILE_NAME = "strTempFileName";
	public static final String DEVICE_ORG_PATH = "deviceOrininalPath";
	public static final String DEVICE_REL_PATH = "deviceRelativePath";

	// RoleTemplate table fields
	public static final String ROLE_TEMPLATE_ID = "iVIRoleTemplateId";
	public static final String TEMPLATE_NAME = "strName";
	public static final String TEMPLATE_CODE = "strCode";
	public static final String ICONS = "icons";

	public static final String VI_TEMPLATE_ID = "iVesselInspectionTemplateId";

	// Table SHIP_MASTER and its field
	public static final String TABLE_SHIP_MASTER = "ShipMaster";
	public static final String KEY_SHIP_ID = "iShipId";
	public static final String KEY_SOCIETY_ID = "iClassificationSocietyId";
	public static final String KEY_SHIP_TYPE_ID = "iShipTypeId";
	public static final String KEY_SHIP_NAME = "strShipName";
	public static final String KEY_S_DESCRIPTION = "strDescription";
	public static final String KEY_DT_CREATED = "dtCreated";
	public static final String KEY_DT_UPDATED = "dtUpdated";
	public static final String KEY_FLG_STATUS = "flgStatus";
	public static final String KEY_FLG_DELETED = "flgDeleted";
	public static final String KEY_FLG_IS_DIRTY = "flgIsDirty";
	public static final String KEY_RULE_LIST_ID = "iRuleListId";
	public static final String KEY_SHIP_IMO_NUMBER = "iShipIMONumber";
	public static final String KEY_STR_FLAG = "strFlag";
	public static final String KEY_LOGO = "strLogo";
	public static final String KEY_FILE_SIZE = "fileSize";
	public static final String KEY_FILE_NAME = "strFileName";
	public static final String KEY_FILE_PATH = "strFilePath";
	public static final String KEY_FILE_TYPE = "strFileType";
	public static final String KEY_SHIP_CODE = "strShipCode";

	// Table ROLE and its field
	public static final String KEY_ROLE_ID = "iRoleId";
	public static final String KEY_ROLE = "strRole";
	public static final String KEY_STR_TYPE = "strType";
	public static final String KEY_TXT_DESCRIPTION = "txtDescription";
	public static final String KEY_RANK_PRIORITY = "iRankPriority";
	public static final String KEY_STR_ROLE_TYPE = "strRoleType"; // PICK FROM
																	// TENENT

	// Table UserMaster and its field
	public static final String TABLE_USER_MASTER = "UserMaster";
	public static final String KEY_iUSER_ID = "iUserId";
	public static final String KEY_USER_NAME = "strUserName";
	public static final String KEY_USER_PASSWORD = "strPassword";
	public static final String KEY_FIRST_NAME = "strFirstName";
	public static final String KEY_LAST_NAME = "strLastName";
	public static final String KEY_DATE_OF_BIRTH = "dtDateOfBirth";
	public static final String KEY_EMAIL = "strEmail";
	public static final String KEY_SHIP_HOLIDAY_LIST_ID = "iShipHolidayListId";
	public static final String KEY_MIN_WORK_HOUR_WEEK_DAYS = "fltMinWorkHourWeekDays";
	public static final String KEY_MIN_WORK_HOUR_SATUREDAY = "fltMinWorkHourSaturdays";
	public static final String KEY_MIN_WORK_HOUR_SUNDAY = "fltMinWorkHourSundays";
	public static final String KEY_MIN_WORK_HOUR_HOLIDAY = "fltMinWorkHourHolidays";
	public static final String KEY_FLTOT_INCLUDED_IN_WAGE = "fltOTIncludedInWage";
	public static final String KEY_FLTOT_RATE_PER_HOUR = "fltOTRatePerHour";
	public static final String KEY_FLG_IS_OVER_TIME_ENABLED = "flgIsOverTimeEnabled";
	public static final String KEY_FLG_IS_WATCHKEEPER = "flgIsWatchkeeper";
	public static final String KEY_FAX_NUMBER = "faxNumber";
	public static final String KEY_HAND_PHONE = "handPhone";
	public static final String KEY_LANDLINE_NUMBER = "landLineNumber";
	public static final String KEY_PAN_NUMBER = "panNumber";
	public static final String KEY_PIN_CODE = "pinCode";
	public static final String KEY_ADDRESS_FIRST = "addressFirst";
	public static final String KEY_ADDRESS_SECOND = "addressSecond";
	public static final String KEY_ADDRESS_THIRD = "addressThird";
	public static final String KEY_CITY = "city";
	public static final String KEY_STATE = "state";
	public static final String KEY_COUNTRY_ID = "iCountryId";
	public static final String KEY_EMPLOYEE_NO = "strEmployeeNo";
	public static final String KEY_MIDDLE_NAME = "strMiddleName";

	// Table UserServiceTerm and its field
	public static final String TABLE_USER_SERVICE_TERM = "UserServiceTerm";
	public static final String KEY_USER_SERVICE_TERM_ID = "iUserServiceTermId";
	public static final String KEY_DT_TERM_FROM = "dtTermFrom";
	public static final String KEY_DT_TERM_TO = "dtTermTo";

	/**
	 * VIWebDirtyTables
	 * 
	 * @param context
	 */
	public static final String TABLE_VI_WEB_DIRTY_TABLES = "VIWebDirtyTables";

	/**
	 * Column VIWebDirtyTables
	 * 
	 * @param context
	 */
	public static final String KEY_VI_WEB_DIRTY_TABLES_ID = "iVIWebDirtyTablesId";
	public static final String STR_TABLE_NAME = "strTableName";
	public static final String FLAG_DIRTY_STATUS = "flgDirtyStatus";

	// Table UserServiceTermRole and its field
	public static final String TABLE_USER_SERVICE_TERM_ROLE = "UserServiceTermRole";
	public static final String KEY_USER_SERVICE_TERM_ROLE_ID = "strUserServiceTermRoleId";
	public static final String POWER_PLUS_SCORE = "powerPlusScore";
	public static final String FIELD_ONE = "strField1";
	public static final String FIELD_TWO = "strField2";

	/**
	 * SyncHistory
	 * 
	 * @param context
	 */
	public static final String TABLE_SYNC_HISTORY = "VesselInspectionSyncHistory";

	/**
	 * Column SyncHistory
	 * 
	 * @param context
	 */
	public static final String KEY_SYNC_HISTORY_ID = "iVesselInspectionSyncHistoryId";
	public static final String LOG_MSG = "logMessage";
	public static final String SYNC_PROGRESS = "progress";
	public static final String SYNC_GENERATOR = "generator";
	public static final String SYNC_FILE_NAME = "strFilename";
	public static final String SYNC_ORDER_ID = "syncOrderid";
	public static final String SYNC_ACK_DATE = "dtAcknowledgeDate";
	public static final String SYNC_LAST_DOWN_DATE = "lastDownLoadDate";
	public static final String SYNC_GEN_DATE = "dtGeneratedDate";
	public static final String REG_TABLET_ID = "strRegisterTabletId";

	/**
	 * VITemplateVersion
	 * 
	 * @param context
	 */
	public static final String TABLE_VI_TEMPLATE_VERSION = "VITemplateVersion";

	/**
	 * Column VITemplateVersion
	 * 
	 * @param context
	 */
	public static final String KEY_VI_TEMPLATE_VERSION_ID = "iVitemplateVersionId";
	public static final String COMPITABLE_VERSION = "competibleVersion";
	public static final String VERSION_NUMBER = "versionNumber";
	public static final String FLG_ACTIVE_VERSION="flgActiveVersion";

	/**
	 * DeviceSyncTime
	 * 
	 * @param context
	 */
	public static final String TABLE_DEVICE_SYNC_TIME = "DeviceSyncTime";

	/**
	 * Column DeviceSyncTime
	 * 
	 * @param context
	 */
	public static final String KEY_DEVICE_SYNC_TIME_ID = "iDeviceSyncTimeId";
	public static final String SHIP_SYNC_TIME = "shipSyncTime";
	public static final String SHORE_SYNC_TIME = "shoreSyncTime";
	
	/**
	 * VIAlertTime
	 * 
	 * @param context
	 */
	public static final String TABLE_VI_ALERT_TIME = "VIAlertTime";

	/**
	 * Column VIAlertTime
	 * 
	 * @param context
	 */
	public static final String KEY_VI_ALERT_TIME_ID = "iVIAlertTimeId";
	public static final String VI_ALERT_TIME = "alertTime";
	
	

	static final String[] CHECKLIST_SECTION_COLUMN = new String[] {
			CHECKLIST_SECTION_ID, SECTION_NAME, REMARKS, FLG_STATUS,
			SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE,
			MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID, SHIP_ID };
	static final String[] CHECKLIST_SECTION_ITEMS_COLUMN = new String[] {
			CHECKLIST_SECTIONITEMS_ID, CHECKLIST_SECTION_ID, ITEMS_NAME,
			ITEM_CONDITION, COMMENTORY, ITEM_REMARKS, FLG_INSPECTED,
			FLG_STATUS, SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY,
			CREATED_DATE, MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID,
			SHIP_ID };
	static final String[] FILLED_CHECKLIST_COLUMN = new String[] {
			FILLED_CHECKLIST_ID, CHECKLIST_SECTION_ID,
			CHECKLIST_SECTIONITEMS_ID, VESSEL_INSPECTIONS_ID, FLG_CHECKED,
			STR_DESC, FLG_STATUS, SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY,
			CREATED_DATE, MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID,
			SHIP_ID };
	static final String[] FORM_SECTION_COLUMN = new String[] { FORM_SECTION_ID,
			FORM_SECTION_NAME, FLG_STATUS, SEQUENCE_NUMBER, FLG_DELETED,
			FLG_IS_DIRTY, CREATED_DATE, MODIFIED_DATE, CREATED_BY, MODIFIED_BY,
			TENANT_ID, SHIP_ID };
	static final String[] FILLED_FORM_COLUMN = new String[] { FILLED_FORM_ID,
			VESSEL_INSPECTIONS_ID, FORM_SECTION_ID, FLG_STATUS,
			SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE,
			MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID, SHIP_ID,
			FORM_HEADER_DESC, FORM_FOOTER_DESC };
	static final String[] SHIP_MASTER_COLUMN = new String[] { SHIP_ID,
			SHIP_NAME, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE, STR_DESC,
			TENANT_ID };
	static final String[] TENANT_COLUMN = new String[] { TENANT_ID,
			TENANT_NAME, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE };

	static final String[] VESSEL_INSPECTION_COLUMN = new String[] {
			VESSEL_INSPECTIONS_ID, VESSEL_INSPECTIONS_TITLE, FLG_STATUS,
			SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE,
			MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID, SHIP_ID,
			FLG_IS_LOCKED };

	static final String[] TABLE_USERS_COLUMN = new String[] { USER_ID, ROLE_ID,
			USER_NAME, PASSWORD, FIRST_NAME, LAST_NAME, FLG_STATUS,
			SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE,
			MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID, SHIP_ID };

	static final String[] TABLE_ROLE_COLUMN = new String[] { ROLE_ID,
			ROLE_NAME, FLG_STATUS, SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY,
			CREATED_DATE, MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID,
			SHIP_ID };
	static final String[] TABLE_USER_SERVICETERM__COLUMN = new String[] {
			USER_SERVICETERM_ID, USER_ID, START_DATE, END_DATE, FLG_STATUS,
			SEQUENCE_NUMBER, FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE,
			MODIFIED_DATE, CREATED_BY, MODIFIED_BY, TENANT_ID, SHIP_ID };

	static final String[] TABLE_FORM_CATEGORY_COLUMN = new String[] {
			FORM_CATEGORY_ID, FORM_CATEGORY_NAME, FORM_CATEGORY_CODE,
			STR_DESCRIPTION, FORM_CATEGORY_SEQUENCE, TENANT_ID, FLG_STATUS,
			FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE, MODIFIED_DATE, CREATED_BY,
			MODIFIED_BY };

	/**
	 * It will call at the time of DBHelper class
	 * 
	 * @param context
	 */
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * @author ripunjay.s Creating SYNC_STATUS table
	 */
	public static final String SYNC_STATUS_CREATE = "CREATE TABLE "
			+ TABLE_SYNC_STATUS + " ( " + SYNC_STATUS_ID
			+ " TEXT PRIMARY KEY , " + SYNC_DATE + " DATE , " + SYNC_MODE
			+ " TEXT , " + DATA_SYNC_MODE + " TEXT , " + SERVER_ADDRESS
			+ " TEXT , " + TENANT_ID + " TEXT ," + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT " + ");";
	
	/**
	 * @author ripunjay.s Creating SYNC_STATUS table
	 */
	public static final String VI_ALERT_TIME_CREATE = "CREATE TABLE "
			+ TABLE_VI_ALERT_TIME + " ( " + KEY_VI_ALERT_TIME_ID
			+ " TEXT PRIMARY KEY , " + VI_ALERT_TIME + " TEXT , " + MAC_ID
			+ " TEXT , " + TENANT_ID + " TEXT ," + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT " + ");";

	/**
	 * @author ripunjay.s Creating VI_TEMPLATE_VERSION table
	 */
	public static final String VI_TEMPLATE_VERSION_CREATE = "CREATE TABLE "
			+ TABLE_VI_TEMPLATE_VERSION + " ( " + KEY_VI_TEMPLATE_VERSION_ID
			+ " TEXT PRIMARY KEY , " + VERSION_NUMBER + " TEXT , " + REMARKS
			+ " TEXT , " + COMPITABLE_VERSION + " TEXT , " + TENANT_ID
			+ " TEXT ," + SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , "
			+ FLG_DEVICE_IS_DIRTY + " INTEGER DEFAULT 0 , " + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_ACTIVE_VERSION + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " DATE , " + MODIFIED_DATE + " DATE , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT " + ");";

	/**
	 * @author pushkar.m Creating FORM_CATEGORY table
	 */
	public static final String FORM_CATEGORY_CREATE = "CREATE TABLE "
			+ TABLE_FORM_CATEGORY + " ( " + FORM_CATEGORY_ID
			+ " TEXT PRIMARY KEY , " + FORM_CATEGORY_NAME + " TEXT , "
			+ VI_TEMPLATE_ID + " INTEGER , " + FORM_CATEGORY_CODE + " TEXT , "
			+ STR_DESCRIPTION + " TEXT , " + FORM_CATEGORY_SEQUENCE
			+ " INTEGER DEFAULT 0 , " + TENANT_ID + " TEXT ," + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT " + ");";

	/**
	 * creating CHECKLIST_SECTION table
	 */
	public static final String CHECKLIST_SECTION_CREATE = "CREATE TABLE "
			+ CHECKLIST_SECTION + " ( " + CHECKLIST_SECTION_ID
			+ " TEXT PRIMARY KEY , " + SECTION_NAME + " TEXT , "
			+ STR_DESCRIPTION + " TEXT , " + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FORM_CATEGORY_ID + " TEXT , "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, " + TENANT_ID
			+ " TEXT " + ");";

	/**
	 * creating CHECKLIST_SECTION_ITEMS table
	 */
	public static final String CHECKLIST_SECTION_ITEMS_CREATE = "CREATE TABLE "
			+ CHECKLIST_SECTION_ITEMS + " ( " + CHECKLIST_SECTIONITEMS_ID
			+ " TEXT PRIMARY KEY , " + CHECKLIST_SECTION_ID + " TEXT , "
			+ ITEMS_NAME + " TEXT , " + ITEM_CONDITION + " TEXT, " + COMMENTORY
			+ " TEXT, " + ITEM_REMARKS + " TEXT, " + FLG_INSPECTED
			+ " INTEGER DEFAULT 0 , " + FLG_STATUS + " INTEGER DEFAULT 0 , "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, " + TENANT_ID
			+ " TEXT " + ");";

	/**
	 * creating FILLED_CHECKLIST table
	 */
	public static final String FILLED_CHECKLIST_CREATE = "CREATE TABLE "
			+ FILLED_CHECKLIST + " ( " + FILLED_CHECKLIST_ID
			+ " TEXT PRIMARY KEY , " + CHECKLIST_SECTION_ID + " TEXT , "
			+ CHECKLIST_SECTIONITEMS_ID + " TEXT , " + VESSEL_INSPECTIONS_ID
			+ " TEXT, " + FLG_CHECKED + " TEXT, " + REMARKS + " TEXT, "
			+ FLG_STATUS + " INTEGER DEFAULT 0 , " + FLG_DEVICE_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + FLG_IS_EDITED + " INTEGER DEFAULT 0 , "
			+ FLG_DELETED + " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY
			+ " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";

	/**
	 * creating FORM_SECTION table
	 */
	/*
	 * public static final String FORM_SECTION_CREATE = "CREATE TABLE " +
	 * FORM_SECTION + " ( " + FORM_SECTION_ID + " TEXT PRIMARY KEY , " +
	 * FORM_SECTION_NAME + " TEXT , "+ FORM_CATEGORY + " TEXT , " + REMARKS +
	 * " TEXT , " + FLG_STATUS + " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER +
	 * " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , " +
	 * FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , " +
	 * MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY +
	 * " TEXT, " + TENANT_ID + " TEXT " + ");";
	 */
	public static final String FORM_SECTION_CREATE = "CREATE TABLE "
			+ FORM_SECTION + " ( " + FORM_SECTION_ID + " TEXT PRIMARY KEY , "
			+ FORM_SECTION_NAME + " TEXT , " + FORM_CATEGORY_ID + " TEXT , "
			+ REMARKS + " TEXT , " + FLG_STATUS + " INTEGER DEFAULT 0 , "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, " + TENANT_ID
			+ " TEXT " + ");";

	/**
	 * creating TABLE_FORM_SECTION_ITEM table
	 */
	public static final String FORM_SECTION_ITEM_CREATE = "CREATE TABLE "
			+ TABLE_FORM_SECTION_ITEM + " ( " + FORM_SECTION_ITEM_ID
			+ " TEXT PRIMARY KEY , " + FORM_SECTION_ID + " TEXT , "
			+ FORM_SECTION_ITEM_NAME + " TEXT , " + ITEM_REMARKS + " TEXT , "
			+ FLG_STATUS + " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT, " + TENANT_ID + " TEXT " + ");";

	/**
	 * creating TABLE_SHIP_FORM_SECTION_ITEM table
	 */
	public static final String SHIP_FORM_SECTION_ITEM_CREATE = "CREATE TABLE "
			+ TABLE_SHIP_FORM_SECTION_ITEM + " ( " + SHIP_FORM_SECTION_ITEM_ID
			+ " TEXT PRIMARY KEY , " + FORM_SECTION_ID + " TEXT , "
			+ FORM_SECTION_ITEM_ID + " TEXT ,  " + FIELD_ONE + " TEXT ,"
			+ FIELD_TWO + " TEXT ," + FLG_STATUS + " INTEGER DEFAULT 0 ,  "
			+ FLG_DELETED + " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY	+ " TEXT, "
			+ SHIP_TYPE_ID + " TEXT , " + ASSOCIATEED_TENANT + " TEXT, " + DYNAMIC_FORM_LIST_ID	+ " TEXT, "
			+ SHIP_ID + " TEXT, " + TENANT_ID + " TEXT " + ");";

	/**
	 * creating FILLED_FORM table
	 */
	public static final String FILLED_FORM_CREATE = "CREATE TABLE "
			+ FILLED_FORM + " ( " + FILLED_FORM_ID + " TEXT PRIMARY KEY , "
			+ FORM_SECTION_ID + " TEXT , " + FORM_SECTION_ITEM_ID + " TEXT , "
			+ VESSEL_INSPECTIONS_ID + " TEXT, " + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + FLG_CHECKED + " TEXT, "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FORM_HEADER_DESC + " TEXT , "
			+ FORM_FOOTER_DESC + " TEXT ," + FLG_IS_EDITED
			+ " INTEGER DEFAULT 0 ," + FLG_IS_HEADER_EDITED
			+ " INTEGER DEFAULT 0 ," + FLG_IS_FOOTER_EDITED
			+ " INTEGER DEFAULT 0 ," + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ FLG_DEVICE_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID
			+ " TEXT " + ");";

	/**
	 * creating TABLE_VesselInspectionFormListDesc table
	 */
	public static final String VELLES_INSPECTION_FORM_LIST_DESC_CREATE = "CREATE TABLE "
			+ TABLE_VesselInspectionFormListDesc
			+ " ( "
			+ VesselInspectionFormListDesc_ID
			+ " TEXT PRIMARY KEY , "
			+ FILLED_FORM_ID
			+ " TEXT , "
			+ FORM_SECTION_ID
			+ " TEXT , "
			+ FORM_SECTION_ITEM_ID
			+ " TEXT , "
			+ VESSEL_INSPECTIONS_ID
			+ " TEXT, "
			+ FLG_STATUS
			+ " INTEGER DEFAULT 0 , "
			+ FLG_CHECKED
			+ " TEXT, "
			+ SEQUENCE_NUMBER
			+ " INTEGER DEFAULT 0 , "
			+ FLG_DELETED
			+ " INTEGER DEFAULT 0 , "
			+ STR_DESC
			+ " TEXT , "
			+ STR_DESC_TYPE
			+ " TEXT , "
			+ FLG_IS_EDITED
			+ " INTEGER DEFAULT 0 ,"
			+ FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ FLG_DEVICE_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ CREATED_mod_DATE
			+ " TEXT , "
			+ MODIFIED_mod_DATE
			+ " TEXT , "
			+ CREATED_BY
			+ " TEXT, "
			+ MODIFIED_mod_BY
			+ " TEXT, "
			+ TENANT_ID
			+ " TEXT, "
			+ SHIP_ID
			+ " TEXT " + ");";

	/**
	 * creating TABLE_VesselInspectionFormListFooter table
	 */
	public static final String VELLES_INSPECTION_FORM_LIST_FOOTER_CREATE = "CREATE TABLE "
			+ TABLE_VesselInspectionFormListFooter
			+ " ( "
			+ VesselInspectionFormListFooter_ID
			+ " TEXT PRIMARY KEY , "
			+ FILLED_FORM_ID
			+ " TEXT , "
			+ FORM_SECTION_ID
			+ " TEXT , "
			+ FORM_SECTION_ITEM_ID
			+ " TEXT , "
			+ VESSEL_INSPECTIONS_ID
			+ " TEXT, "
			+ FLG_STATUS
			+ " INTEGER DEFAULT 0 , "
			+ FLG_CHECKED
			+ " TEXT, "
			+ SEQUENCE_NUMBER
			+ " INTEGER DEFAULT 0 , "
			+ FLG_DELETED
			+ " INTEGER DEFAULT 0 , "
			+ FORM_FOOTER_DESC
			+ " TEXT , "
			+ FLG_IS_EDITED
			+ " INTEGER DEFAULT 0 ,"
			+ FLG_IS_FOOTER_EDITED
			+ " INTEGER DEFAULT 0 ,"
			+ FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ FLG_DEVICE_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ CREATED_DATE
			+ " TEXT , "
			+ MODIFIED_DATE
			+ " TEXT , "
			+ CREATED_BY
			+ " TEXT, "
			+ MODIFIED_BY
			+ " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";

	/**
	 * creating FILLED_FORM_Image table FLG_IS_EDITED
	 */
	public static final String FILLED_FORM_IMAGE_CREATE = "CREATE TABLE "
			+ FILLED_FORM_IMAGE + " ( " + FILLED_FORM_IMAGE_ID
			+ " TEXT PRIMARY KEY , " + FILLED_FORM_ID + " TEXT , "
			+ VESSEL_INSPECTIONS_ID + " TEXT, " + FORM_SECTION_ID + " TEXT , "
			+ FORM_SECTION_ITEM_ID + " TEXT, " + IMAGE_NAME + " TEXT , "
			+ IMAGE_PATH + " TEXT, " + IMAGE_RELATIVE_PATH + " TEXT ,"
			+ ORIGINAL_PATH + " TEXT ," + TEMP_FILE_NAME + " TEXT ,"
			+ DEVICE_ORG_PATH + " TEXT ," + DEVICE_REL_PATH + " TEXT ,"
			+ FLG_LARGE + " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + FLG_DEVICE_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + FLG_IS_EDITED + " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, " + STR_DESC
			+ " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";

	/**
	 * creating VESSEL_INSPECTION table
	 */
	public static final String VESSEL_INSPECTION_CREATE = "CREATE TABLE "
			+ VESSEL_INSPECTION + " ( " + VESSEL_INSPECTIONS_ID
			+ " TEXT PRIMARY KEY , " + VESSEL_INSPECTIONS_TITLE + " TEXT, "
			+ VESSEL_INSPECTIONS_STATUS_ID + " TEXT, " + REMARKS + " TEXT, "
			+ FLG_REVERTED + " INTEGER DEFAULT 0 , " + FLG_STATUS
			+ " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER
			+ " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + FLG_IS_LOCKED
			+ " INTEGER DEFAULT 0 , " + VESSEL_INSPECTION_DATE + " DATE ,"
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, " + INSPECTED_BY
			+ " TEXT, " + REVIEWED_BY + " TEXT, " + APPROVED_BY + " TEXT, "
			+ ACCEPTED_BY + " TEXT, " + COMPLETED_BY + " TEXT, "
			+ FLG_DEVICE_IS_DIRTY + " INTEGER DEFAULT 0 , " + FLG_IS_EDITED
			+ " INTEGER DEFAULT 0 , " + Executive_Summary + " TEXT, "
			+ TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";

	/**
	 * creating TABLE_USERS table
	 */
	/*
	 * public static final String TABLE_USERS_CREATE = "CREATE TABLE " +
	 * TABLE_USERS + " ( " + USER_ID + " TEXT PRIMARY KEY, " + ROLE_ID +
	 * " TEXT , " + USER_NAME + " TEXT NOT NULL, " + PASSWORD +
	 * " TEXT NOT NULL, " + FIRST_NAME + " TEXT NOT NULL, " + LAST_NAME +
	 * " TEXT, " + FLG_STATUS + " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER +
	 * " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , " +
	 * FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , " +
	 * MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY +
	 * " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";
	 *//**
	 * creating TABLE_ROLE table
	 */
	/*
	 * public static final String TABLE_ROLE_CREATE = "CREATE TABLE " +
	 * TABLE_ROLE + " ( " + ROLE_ID + " TEXT PRIMARY KEY, " + ROLE_NAME +
	 * " TEXT , " + FLG_STATUS + " INTEGER DEFAULT 0 , " + SEQUENCE_NUMBER +
	 * " INTEGER DEFAULT 0 , " + FLG_DELETED + " INTEGER DEFAULT 0 , " +
	 * FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , " +
	 * MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY +
	 * " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT " + ");";
	 *//**
	 * creating TABLE_USER_SERVICETERM table
	 */
	/*
	 * public static final String TABLE_USER_SERVICETERM_CREATE =
	 * "CREATE TABLE " + TABLE_USER_SERVICETERM + " ( " + USER_SERVICETERM_ID +
	 * " TEXT PRIMARY KEY, " + USER_ID + " TEXT , " + START_DATE + " TEXT, " +
	 * END_DATE + " TEXT, " + FLG_STATUS + " INTEGER DEFAULT 0 , " +
	 * SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + FLG_DELETED +
	 * " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " +
	 * CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , " + CREATED_BY +
	 * " TEXT, " + MODIFIED_BY + " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID +
	 * " TEXT " + ");";
	 *//**
	 * creating TABLE_USER_SERVICETERM table SHIP_ID, SHIP_NAME, FLG_DELETED,
	 * FLG_IS_DIRTY, CREATED_DATE, STR_DESC, TENANT_ID ;
	 */
	/*
	 * public static final String SHIP_MASTER_CREATE = "CREATE TABLE " +
	 * SHIP_MASTER + " ( " + SHIP_ID + " TEXT PRIMARY KEY, " + SHIP_NAME +
	 * " TEXT , " + STR_DESC + " TEXT, " + FLG_DELETED + " INTEGER DEFAULT 0 , "
	 * + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , " +
	 * TENANT_ID + " TEXT " + ");";
	 *//**
	 * creating TABLE_USER_SERVICETERM table TENANT_ID, TENANT_NAME,
	 * FLG_DELETED, FLG_IS_DIRTY, CREATED_DATE
	 */
	/*
	 * public static final String TENANT_CREATE = "CREATE TABLE " + TENANT +
	 * " ( " + TENANT_ID + " TEXT PRIMARY KEY, " + TENANT_NAME + " TEXT , " +
	 * FLG_DELETED + " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY +
	 * " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT " + ");";
	 */

	/**
	 * creating TABLE_ROLE table
	 */
	public static final String TABLE_ROLE_TEMPALTE_CREATE = "CREATE TABLE "
			+ TABLE_ROLE_TEMPLATE + " ( " + ROLE_TEMPLATE_ID
			+ " TEXT PRIMARY KEY ," + ROLE_ID + " INTEGER , " + TEMPLATE_NAME
			+ " TEXT , " + TEMPLATE_CODE + " TEXT," + ICONS + " TEXT , "
			+ FORM_CATEGORY_ID + " TEXT," + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + CREATED_mod_DATE
			+ " TEXT , " + MODIFIED_mod_DATE + " TEXT , " + VI_TEMPLATE_ID
			+ " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_mod_BY + " TEXT, "
			+ TENANT_ID + " INTEGER)";

	/**
	 * creating TABLE_VI_TEMPLATE table
	 */
	public static final String TABLE_VI_TEMPLATE_CREATE = "CREATE TABLE "
			+ TABLE_VI_TEMPLATE + " ( " + VI_TEMPLATE_ID
			+ " TEXT PRIMARY KEY ," + TEMPLATE_NAME + " TEXT , "
			+ TEMPLATE_CODE + " TEXT," + FLG_DELETED + " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0 , " + STR_DESC + " TEXT, "
			+ SEQUENCE_NUMBER + " INTEGER DEFAULT 0 , " + ICONS + " TEXT , "
			+ FLG_STATUS + " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT , " + CREATED_BY + " TEXT, " + MODIFIED_BY
			+ " TEXT, " + TENANT_ID + " INTEGER)";

	/**
	 * creating TABLE_SYNC_HISTORY table
	 */
	public static final String CREATE_SYNC_HISTORY_TABLE = "CREATE TABLE "
			+ TABLE_SYNC_HISTORY + "(" + KEY_SYNC_HISTORY_ID
			+ " TEXT PRIMARY KEY," + SYNC_DATE + " DATE," + LOG_MSG + " TEXT,"
			+ SYNC_PROGRESS + " TEXT," + SYNC_GENERATOR + " TEXT,"
			+ SYNC_FILE_NAME + " TEXT," + SYNC_ORDER_ID + " INTEGER,"
			+ FLG_DELETED + " INTEGER," + FLG_IS_DIRTY + " TEXT,"
			+ SYNC_ACK_DATE + " TEXT," + SYNC_LAST_DOWN_DATE + " DATE,"
			+ SYNC_GEN_DATE + " TEXT," + MAC_ID + " TEXT," + REG_TABLET_ID
			+ " TEXT," + SYNC_MODE + " TEXT," + SHIP_ID + " TEXT," + TENANT_ID
			+ " TEXT" + ")";

	/**
	 * creating TENANT table
	 */
	public static final String CREATE_TENANT_TABLE = "CREATE TABLE " + TENANT
			+ "(" + TENANT_ID + " TEXT PRIMARY KEY," + TENANT_NAME + " TEXT"
			+ ")";

	/**
	 * creating TABLE_SHIP_MASTER table
	 */
	public static final String CREATE_SHIP_MASTER_TABLE = "CREATE TABLE "
			+ TABLE_SHIP_MASTER + "(" + KEY_SHIP_ID + " TEXT PRIMARY KEY,"
			+ TENANT_ID + " TEXT," + KEY_SOCIETY_ID + " TEXT,"
			+ KEY_SHIP_TYPE_ID + " TEXT," + KEY_SHIP_NAME + " TEXT,"
			+ KEY_S_DESCRIPTION + " TEXT," + KEY_DT_CREATED + " TEXT,"
			+ KEY_DT_UPDATED + " TEXT," + KEY_FLG_STATUS + " TEXT,"
			+ KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
			+ KEY_RULE_LIST_ID + " TEXT," + KEY_SHIP_IMO_NUMBER + " TEXT,"
			+ KEY_STR_FLAG + " TEXT," + KEY_LOGO + " TEXT," + KEY_FILE_SIZE
			+ " TEXT," + KEY_FILE_NAME + " TEXT," + KEY_FILE_PATH + " TEXT,"
			+ KEY_FILE_TYPE + " TEXT," + KEY_SHIP_CODE + " TEXT" + ")";

	/**
	 * creating TABLE_ROLE table
	 */
	public static final String CREATE_ROLE_TABLE = "CREATE TABLE " + TABLE_ROLE
			+ "(" + KEY_ROLE_ID + " TEXT  PRIMARY KEY," + KEY_ROLE + " TEXT,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_STR_TYPE + " TEXT,"
			+ KEY_FLG_DELETED + " TEXT," + KEY_TXT_DESCRIPTION + " TEXT,"
			+ KEY_RANK_PRIORITY + " TEXT," + TENANT_ID + " TEXT,"
			+ KEY_STR_ROLE_TYPE + " TEXT" + ")";

	/**
	 * creating TABLE_USER_MASTER table
	 */
	public static final String CREATE_USER_MASTER_TABLE = "CREATE TABLE "
			+ TABLE_USER_MASTER + "(" + KEY_iUSER_ID + " TEXT PRIMARY KEY,"
			+ TENANT_ID + " TEXT," + KEY_USER_NAME + " TEXT,"
			+ KEY_USER_PASSWORD + " TEXT," + KEY_FIRST_NAME + " TEXT,"
			+ KEY_LAST_NAME + " TEXT," + KEY_DATE_OF_BIRTH + " TEXT,"
			+ KEY_TXT_DESCRIPTION + " TEXT," + KEY_EMAIL + " TEXT,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_FLG_STATUS + " TEXT," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_SHIP_HOLIDAY_LIST_ID + " TEXT,"
			+ KEY_MIN_WORK_HOUR_WEEK_DAYS + " TEXT,"
			+ KEY_MIN_WORK_HOUR_SATUREDAY + " TEXT," + KEY_MIN_WORK_HOUR_SUNDAY
			+ " TEXT," + KEY_MIN_WORK_HOUR_HOLIDAY + " TEXT,"
			+ KEY_FLTOT_INCLUDED_IN_WAGE + " TEXT," + KEY_FLTOT_RATE_PER_HOUR
			+ " TEXT," + KEY_FLG_IS_OVER_TIME_ENABLED + " TEXT," + KEY_ROLE_ID
			+ " TEXT," + KEY_FLG_IS_WATCHKEEPER + " TEXT," + KEY_FAX_NUMBER
			+ " TEXT," + KEY_HAND_PHONE + " TEXT," + KEY_LANDLINE_NUMBER
			+ " TEXT," + KEY_PAN_NUMBER + " TEXT," + KEY_PIN_CODE + " TEXT,"
			+ KEY_ADDRESS_FIRST + " TEXT," + KEY_ADDRESS_SECOND + " TEXT,"
			+ KEY_ADDRESS_THIRD + " TEXT," + KEY_CITY + " TEXT," + KEY_STATE
			+ " TEXT," + KEY_COUNTRY_ID + " TEXT," + KEY_EMPLOYEE_NO + " TEXT,"
			+ KEY_MIDDLE_NAME + " TEXT" + ")";

	/**
	 * creating TABLE_USER_SERVICE_TERM table
	 */
	public static final String CREATE_USER_SERVICE_TERM_TABLE = "CREATE TABLE "
			+ TABLE_USER_SERVICE_TERM + "(" + KEY_USER_SERVICE_TERM_ID
			+ " TEXT PRIMARY KEY," + KEY_iUSER_ID + " TEXT," + KEY_DT_TERM_FROM
			+ " DATE," + KEY_DT_TERM_TO + " DATE," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_DT_CREATED + " TEXT,"
			+ KEY_DT_UPDATED + " TEXT," + KEY_SHIP_ID + " TEXT," + TENANT_ID
			+ " TEXT" + ")";

	/**
	 * creating TABLE_USER_SERVICE_TERM_ROLE table
	 */
	public static final String CREATE_USER_SERVICE_TERM_ROLE_TABLE = "CREATE TABLE "
			+ TABLE_USER_SERVICE_TERM_ROLE
			+ "("
			+ KEY_USER_SERVICE_TERM_ROLE_ID
			+ " TEXT PRIMARY KEY,"
			+ KEY_DT_CREATED
			+ " TEXT,"
			+ KEY_DT_UPDATED
			+ " TEXT,"
			+ KEY_FLG_DELETED
			+ " TEXT,"
			+ KEY_FLG_IS_DIRTY
			+ " TEXT,"
			+ KEY_ROLE_ID
			+ " TEXT,"
			+ KEY_SHIP_ID
			+ " TEXT,"
			+ TENANT_ID
			+ " TEXT,"
			+ KEY_iUSER_ID
			+ " TEXT,"
			+ KEY_USER_SERVICE_TERM_ID
			+ " TEXT,"
			+ KEY_DT_TERM_FROM
			+ " DATE,"
			+ KEY_DT_TERM_TO
			+ " DATE,"
			+ POWER_PLUS_SCORE
			+ " INTEGER,"
			+ FIELD_ONE
			+ " INTEGER," + FIELD_TWO + " INTEGER" + ")";

	/**
	 * creating VIWebDirtyTables table
	 */
	public static final String CREATE_VI_WEBDIRTY_TABLE = "CREATE TABLE "
			+ TABLE_VI_WEB_DIRTY_TABLES + "(" + KEY_VI_WEB_DIRTY_TABLES_ID
			+ " TEXT  PRIMARY KEY," + REMARKS + " TEXT," + STR_TABLE_NAME
			+ " TEXT," + FLAG_DIRTY_STATUS + " INTEGER," + SEQUENCE_NUMBER
			+ " INTEGER," + KEY_FLG_DELETED + " INTEGER," + KEY_FLG_IS_DIRTY
			+ " INTEGER," + TENANT_ID + " INTEGER " + ")";

	/**
	 * @author pushkar.m Creating DEVICE_SYNC_TIME table
	 */
	public static final String DEVICE_SYNC_TIME_CREATE = "CREATE TABLE "
			+ TABLE_DEVICE_SYNC_TIME + " ( " + KEY_DEVICE_SYNC_TIME_ID
			+ " TEXT PRIMARY KEY , " + SHIP_SYNC_TIME + " TEXT , "
			+ SHORE_SYNC_TIME + " TEXT , " + MAC_ID + " TEXT, " + TENANT_ID
			+ " TEXT ," + FLG_DEVICE_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ FLG_DELETED + " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT , " + SHIP_ID + " TEXT, " + CREATED_BY
			+ " TEXT, " + MODIFIED_BY + " TEXT " + ")";

	/**
	 * It will create the tables in local database
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CHECKLIST_SECTION_CREATE);
		database.execSQL(CHECKLIST_SECTION_ITEMS_CREATE);
		database.execSQL(FILLED_CHECKLIST_CREATE);
		database.execSQL(FORM_SECTION_CREATE);
		database.execSQL(FORM_SECTION_ITEM_CREATE);
		database.execSQL(FILLED_FORM_CREATE);
		database.execSQL(FILLED_FORM_IMAGE_CREATE);
		database.execSQL(CREATE_SHIP_MASTER_TABLE);
		database.execSQL(CREATE_USER_MASTER_TABLE);
		database.execSQL(CREATE_TENANT_TABLE);
		database.execSQL(VESSEL_INSPECTION_CREATE);
		database.execSQL(CREATE_ROLE_TABLE);
		database.execSQL(CREATE_USER_SERVICE_TERM_TABLE);
		database.execSQL(CREATE_USER_SERVICE_TERM_ROLE_TABLE);
		database.execSQL(FORM_CATEGORY_CREATE);
		database.execSQL(SYNC_STATUS_CREATE);
		database.execSQL(TABLE_ROLE_TEMPALTE_CREATE);
		database.execSQL(CREATE_SYNC_HISTORY_TABLE);
		database.execSQL(TABLE_VI_TEMPLATE_CREATE);
		database.execSQL(VELLES_INSPECTION_FORM_LIST_DESC_CREATE);
		database.execSQL(CREATE_VI_WEBDIRTY_TABLE);
		database.execSQL(SHIP_FORM_SECTION_ITEM_CREATE);
		database.execSQL(VI_TEMPLATE_VERSION_CREATE);
		database.execSQL(DEVICE_SYNC_TIME_CREATE);
		database.execSQL(VI_ALERT_TIME_CREATE);
		// database.execSQL(VELLES_INSPECTION_FORM_LIST_FOOTER_CREATE);

	}

	/**
	 * It will upgrade the tables in local database
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + CHECKLIST_SECTION);
		db.execSQL("DROP TABLE IF EXISTS " + CHECKLIST_SECTION_ITEMS);
		db.execSQL("DROP TABLE IF EXISTS " + FILLED_CHECKLIST);
		db.execSQL("DROP TABLE IF EXISTS " + FORM_SECTION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM_SECTION_ITEM);
		db.execSQL("DROP TABLE IF EXISTS " + FILLED_FORM);
		db.execSQL("DROP TABLE IF EXISTS " + FILLED_FORM_IMAGE);
		db.execSQL("DROP TABLE IF EXISTS " + VESSEL_INSPECTION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
		db.execSQL("DROP TABLE IF EXISTS " + SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICETERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_STATUS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_TEMPLATE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VI_TEMPLATE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VesselInspectionFormListDesc);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VI_WEB_DIRTY_TABLES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_FORM_SECTION_ITEM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VI_TEMPLATE_VERSION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE_SYNC_TIME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VI_ALERT_TIME);
		// db.execSQL("DROP TABLE IF EXISTS " +
		// TABLE_VesselInspectionFormListFooter);
		onCreate(db);
	}
}