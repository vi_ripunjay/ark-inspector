/**
 * @file  DBManager . java
 *  
 * @author Ripunjay Shukla
 * 
 * @date : May 21,2015
 */
package com.tams.sql;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.DeviceSyncTime;
import com.tams.model.FilledCheckList;
import com.tams.model.FilledForm;
import com.tams.model.FilledFormImages;
import com.tams.model.FormCategory;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.model.Role;
import com.tams.model.RoleTemplate;
import com.tams.model.ShipFormSectionItem;
import com.tams.model.ShipMaster;
import com.tams.model.SyncHistory;
import com.tams.model.SyncStatus;
import com.tams.model.Tenant;
import com.tams.model.UserMaster;
import com.tams.model.UserServiceTerm;
import com.tams.model.VIAlertTime;
import com.tams.model.VITemplateVersion;
import com.tams.model.VesselInspection;
import com.tams.model.VesselInspectionFormListDesc;
import com.tams.model.VesselInspectionFormListFooter;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;

@SuppressLint("SimpleDateFormat")
public class DBManager {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-13
	 */

	private SQLiteDatabase database;
	private DBHelper dbHelper;

	/**
	 * This method will call at the initialization of DBManager class
	 * 
	 * @param context
	 */
	public DBManager(Context context) {
		dbHelper = new DBHelper(context);
	}

	/**
	 * It will open database in writable mode
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * It will call close database
	 */
	public void close() {
		dbHelper.close();
	}

	/**
	 * This method will drop all the table from database
	 */
	public void dropDb() {
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.CHECKLIST_SECTION);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.CHECKLIST_SECTION_ITEMS);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.FILLED_CHECKLIST);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.FORM_SECTION);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_FORM_SECTION_ITEM);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.FILLED_FORM);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.FILLED_FORM_IMAGE);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.VESSEL_INSPECTION);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_ROLE);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_USERS);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_USER_SERVICETERM);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_USER_SERVICE_TERM_ROLE);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.SHIP_MASTER);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TENANT);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_FORM_CATEGORY);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_SYNC_STATUS);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_SYNC_HISTORY);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_ROLE_TEMPLATE);
		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_VI_TEMPLATE);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_VesselInspectionFormListDesc);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_VesselInspectionFormListFooter);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_VI_WEB_DIRTY_TABLES);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_SHIP_FORM_SECTION_ITEM);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_VI_TEMPLATE_VERSION);
		database.execSQL("DROP TABLE IF EXISTS "
				+ DBHelper.TABLE_DEVICE_SYNC_TIME);

		database.execSQL("DROP TABLE IF EXISTS " + DBHelper.TABLE_VI_ALERT_TIME);

		createDb();
	}

	/**
	 * This method will create all the table from database
	 */
	public void createDb() {
		database.execSQL(DBHelper.CHECKLIST_SECTION_CREATE);
		database.execSQL(DBHelper.CHECKLIST_SECTION_ITEMS_CREATE);
		database.execSQL(DBHelper.FILLED_CHECKLIST_CREATE);
		database.execSQL(DBHelper.FORM_SECTION_CREATE);
		database.execSQL(DBHelper.FILLED_FORM_CREATE);
		database.execSQL(DBHelper.FORM_SECTION_ITEM_CREATE);
		database.execSQL(DBHelper.FILLED_FORM_IMAGE_CREATE);
		database.execSQL(DBHelper.VESSEL_INSPECTION_CREATE);
		database.execSQL(DBHelper.CREATE_USER_MASTER_TABLE);
		database.execSQL(DBHelper.CREATE_ROLE_TABLE);
		database.execSQL(DBHelper.CREATE_USER_SERVICE_TERM_TABLE);
		database.execSQL(DBHelper.CREATE_SHIP_MASTER_TABLE);
		database.execSQL(DBHelper.CREATE_TENANT_TABLE);
		database.execSQL(DBHelper.FORM_CATEGORY_CREATE);
		database.execSQL(DBHelper.SYNC_STATUS_CREATE);
		database.execSQL(DBHelper.CREATE_SYNC_HISTORY_TABLE);
		database.execSQL(DBHelper.CREATE_USER_SERVICE_TERM_ROLE_TABLE);
		database.execSQL(DBHelper.TABLE_ROLE_TEMPALTE_CREATE);
		database.execSQL(DBHelper.TABLE_VI_TEMPLATE_CREATE);
		// database.execSQL(DBHelper.VELLES_INSPECTION_FORM_LIST_HEADER_CREATE);
		database.execSQL(DBHelper.VELLES_INSPECTION_FORM_LIST_DESC_CREATE);
		database.execSQL(DBHelper.VELLES_INSPECTION_FORM_LIST_FOOTER_CREATE);
		database.execSQL(DBHelper.CREATE_VI_WEBDIRTY_TABLE);
		database.execSQL(DBHelper.SHIP_FORM_SECTION_ITEM_CREATE);
		database.execSQL(DBHelper.VI_TEMPLATE_VERSION_CREATE);
		database.execSQL(DBHelper.DEVICE_SYNC_TIME_CREATE);
		database.execSQL(DBHelper.VI_ALERT_TIME_CREATE);

	}

	/**
	 * @author pushkar.m This method will insert single record in form_category
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFormCategoryTable(FormCategory formCategory) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					formCategory.getiFormCategoryId());
			contentValues.put(DBHelper.FORM_CATEGORY_NAME,
					formCategory.getStrCategoryName());
			contentValues.put(DBHelper.FORM_CATEGORY_CODE,
					formCategory.getStrCategoryCode());
			contentValues.put(DBHelper.CREATED_DATE,
					formCategory.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formCategory.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, formCategory.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					formCategory.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					formCategory.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, formCategory.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formCategory.getFlgIsDirty());
			contentValues.put(DBHelper.FORM_CATEGORY_SEQUENCE,
					formCategory.getSequence());
			contentValues.put(DBHelper.STR_DESCRIPTION,
					formCategory.getStrDescription());
			contentValues.put(DBHelper.TENANT_ID, formCategory.getiTenantId());
			contentValues.put(DBHelper.VI_TEMPLATE_ID,
					formCategory.getiVesselInspectionTemplateId());

			result = database.insert(DBHelper.TABLE_FORM_CATEGORY, null,
					contentValues);
			Log.i("FORM_CATEGORY value :== ",
					"inside insert of form_category table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in form_category table and return
	 * the value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */
	public long updateFormCategoryTable(FormCategory formCategory) {
		// TODO Auto-generated method stub
		long result = 0;

		String whereClause = DBHelper.FORM_CATEGORY_ID + " =  '"
				+ formCategory.getiFormCategoryId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					formCategory.getiFormCategoryId());
			contentValues.put(DBHelper.FORM_CATEGORY_NAME,
					formCategory.getStrCategoryName());
			contentValues.put(DBHelper.FORM_CATEGORY_CODE,
					formCategory.getStrCategoryCode());
			contentValues.put(DBHelper.CREATED_DATE,
					formCategory.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formCategory.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, formCategory.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					formCategory.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					formCategory.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, formCategory.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formCategory.getFlgIsDirty());
			contentValues.put(DBHelper.FORM_CATEGORY_SEQUENCE,
					formCategory.getSequence());
			contentValues.put(DBHelper.STR_DESCRIPTION,
					formCategory.getStrDescription());
			contentValues.put(DBHelper.TENANT_ID, formCategory.getiTenantId());
			contentValues.put(DBHelper.VI_TEMPLATE_ID,
					formCategory.getiVesselInspectionTemplateId());

			result = database.update(DBHelper.TABLE_FORM_CATEGORY,
					contentValues, whereClause, null);
			Log.i("FORM_CATEGORY value :== ",
					"inside insert of form_category table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will insert single record in Checklist_Section table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertCheckListSectionTable(CheckListSection checkListSection) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					checkListSection.getiCheckListSectionId());
			contentValues.put(DBHelper.SECTION_NAME,
					checkListSection.getStrSectionName());
			contentValues.put(DBHelper.CREATED_DATE,
					checkListSection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					checkListSection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					checkListSection.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					checkListSection.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					checkListSection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					checkListSection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					checkListSection.getFlgIsDirty());
			contentValues.put(DBHelper.STR_DESCRIPTION,
					checkListSection.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					checkListSection.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					checkListSection.getiTenantId());

			result = database.insert(DBHelper.CHECKLIST_SECTION, null,
					contentValues);
			Log.i("CHECKLIST_SECTION value :== ",
					"inside insert of CheckListSection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Checklist_Section table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateCheckListSectionData(CheckListSection checkListSection) {
		// TODO Auto-generated method stub
		long result = 0;

		String whereClause = DBHelper.CHECKLIST_SECTION_ID + " =  '"
				+ checkListSection.getiCheckListSectionId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					checkListSection.getiCheckListSectionId());
			contentValues.put(DBHelper.SECTION_NAME,
					checkListSection.getStrSectionName());
			contentValues.put(DBHelper.CREATED_DATE,
					checkListSection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					checkListSection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					checkListSection.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					checkListSection.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					checkListSection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					checkListSection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					checkListSection.getFlgIsDirty());
			contentValues.put(DBHelper.STR_DESCRIPTION,
					checkListSection.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					checkListSection.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					checkListSection.getiTenantId());

			result = database.update(DBHelper.CHECKLIST_SECTION, contentValues,
					whereClause, null);
			Log.i("CHECKLIST_SECTION value :== ",
					"inside update of CheckListSection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m fetch the complete data of form_category table.
	 * @return list of CheckListSection object
	 */
	public List<FormCategory> getFormCategoryData() {

		List<FormCategory> formCategoryData = new ArrayList<FormCategory>();
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_FORM_CATEGORY, null, null,
					null, null, null, null);
			Log.i("dbmanager", "inside getFormCategoryData");
			if (cursor != null && cursor.moveToFirst()) {
				formCategoryData = new ArrayList<FormCategory>();

				do {
					FormCategory formCategory = new FormCategory();
					formCategory.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					formCategory
							.setStrCategoryName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_NAME)));
					formCategory
							.setStrCategoryCode(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_CODE)));
					formCategory.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESCRIPTION)));
					formCategory
							.setSequence(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_SEQUENCE)));
					formCategory.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formCategory.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formCategory.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formCategory.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					formCategory.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formCategory.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formCategory.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formCategory.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formCategory
							.setiVesselInspectionTemplateId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					formCategoryData.add(formCategory);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formCategoryData;
	}

	/**
	 * fetch the complete data of CheckListSection table.
	 * 
	 * @return list of CheckListSection object
	 */
	public List<CheckListSection> getCheckListSectionData() {

		List<CheckListSection> checkListSectionData = new ArrayList<CheckListSection>();
		Cursor cursor = null;

		String orderBy = DBHelper.SEQUENCE_NUMBER;

		try {

			cursor = database.query(DBHelper.CHECKLIST_SECTION, null, null,
					null, null, null, orderBy);
			Log.i("dbmanager", "inside getCheckListSectionData");
			if (cursor != null && cursor.moveToFirst()) {
				checkListSectionData = new ArrayList<CheckListSection>();

				do {
					CheckListSection checkListSection = new CheckListSection();
					checkListSection
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					checkListSection.setStrSectionName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SECTION_NAME)));
					checkListSection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					checkListSection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					checkListSection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					checkListSection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					checkListSection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					checkListSection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					checkListSection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					checkListSection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					checkListSectionData.add(checkListSection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return checkListSectionData;
	}

	/**
	 * @author pushkar.m fetch the complete data of form_category table By Id.
	 * @return list of FormCategory object
	 */
	public List<FormCategory> getFormCategoryDataById(String iFormCategoryId) {

		List<FormCategory> formCategoryData = new ArrayList<FormCategory>();
		Cursor cursor = null;

		String whereClause = DBHelper.FORM_CATEGORY_ID + " =  '"
				+ iFormCategoryId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_FORM_CATEGORY, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFormCategoryDataById");
			if (cursor != null && cursor.moveToFirst()) {
				formCategoryData = new ArrayList<FormCategory>();

				do {
					FormCategory formCategory = new FormCategory();
					formCategory.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					formCategory
							.setStrCategoryName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_NAME)));
					formCategory
							.setStrCategoryCode(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_CODE)));
					formCategory.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESCRIPTION)));
					formCategory
							.setSequence(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_SEQUENCE)));
					formCategory.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formCategory.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formCategory.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formCategory.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					formCategory.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formCategory.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formCategory.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formCategory.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formCategory
							.setiVesselInspectionTemplateId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					formCategoryData.add(formCategory);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getFormCategoryDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formCategoryData;
	}

	/**
	 * @author pushkar.m fetch the single row data of form_category table By
	 *         category code.
	 * @return list of FormCategory object
	 */
	public List<FormCategory> getFormCategoryByCode(String strCategoryCode) {

		List<FormCategory> formCategoryData = new ArrayList<FormCategory>();
		Cursor cursor = null;

		String whereClause = DBHelper.FORM_CATEGORY_CODE + " =  '"
				+ strCategoryCode + "'";

		try {
			cursor = database.query(DBHelper.TABLE_FORM_CATEGORY, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFormCategoryDataById");
			if (cursor != null && cursor.moveToFirst()) {
				formCategoryData = new ArrayList<FormCategory>();

				do {
					FormCategory formCategory = new FormCategory();
					formCategory.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					formCategory
							.setStrCategoryName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_NAME)));
					formCategory
							.setStrCategoryCode(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_CODE)));
					formCategory.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESCRIPTION)));
					formCategory
							.setSequence(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_SEQUENCE)));
					formCategory.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formCategory.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formCategory.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formCategory.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					formCategory.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formCategory.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formCategory.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formCategory.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formCategory
							.setiVesselInspectionTemplateId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					formCategoryData.add(formCategory);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getFormCategoryDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formCategoryData;
	}

	/**
	 * fetch the complete data of CheckListSection table By Id.
	 * 
	 * @return list of CheckListSection object
	 */
	public List<CheckListSection> getCheckListSectionDataById(
			String strCheckListSectionId) {

		List<CheckListSection> checkListSectionData = new ArrayList<CheckListSection>();
		Cursor cursor = null;

		String whereClause = DBHelper.CHECKLIST_SECTION_ID + " =  '"
				+ strCheckListSectionId + "'";

		try {
			cursor = database.query(DBHelper.CHECKLIST_SECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getCheckListSectionData");
			if (cursor != null && cursor.moveToFirst()) {
				checkListSectionData = new ArrayList<CheckListSection>();

				do {
					CheckListSection checkListSection = new CheckListSection();
					checkListSection
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					checkListSection.setStrSectionName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SECTION_NAME)));
					checkListSection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					checkListSection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					checkListSection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					checkListSection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					checkListSection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					checkListSection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					checkListSection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					checkListSection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					checkListSectionData.add(checkListSection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return checkListSectionData;
	}

	/**
	 * This method will insert single record in UserMaster table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertUserMasterTable(UserMaster umd) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues values = new ContentValues();
			values.put(DBHelper.KEY_iUSER_ID, umd.getiUserId());
			values.put(DBHelper.TENANT_ID, umd.getiTenantId());
			values.put(DBHelper.KEY_USER_NAME, umd.getStrUserName());
			values.put(DBHelper.KEY_USER_PASSWORD, umd.getStrPassword());
			values.put(DBHelper.KEY_FIRST_NAME, umd.getStrFirstName());
			values.put(DBHelper.KEY_LAST_NAME, umd.getStrLastName());
			values.put(DBHelper.KEY_DATE_OF_BIRTH, umd.getDtDateOfBirth());
			values.put(DBHelper.KEY_TXT_DESCRIPTION, umd.getTxtDescription());
			values.put(DBHelper.KEY_EMAIL, umd.getStrEmail());
			values.put(DBHelper.KEY_DT_CREATED, umd.getDtCreated());
			values.put(DBHelper.KEY_DT_UPDATED, umd.getDtUpdated());
			values.put(DBHelper.KEY_FLG_STATUS, umd.getFlgStatus());
			values.put(DBHelper.KEY_FLG_DELETED, umd.getFlgDeleted());
			values.put(DBHelper.KEY_FLG_IS_DIRTY, umd.getFlgIsDirty());
			values.put(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID,
					umd.getiShipHolidayListId());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS,
					umd.getFltMinWorkHourWeekDays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY,
					umd.getFltMinWorkHourSaturdays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY,
					umd.getFltMinWorkHourSundays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY,
					umd.getFltMinWorkHourHolidays());
			values.put(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE,
					umd.getFltOTIncludedInWage());
			values.put(DBHelper.KEY_FLTOT_RATE_PER_HOUR,
					umd.getFltOTRatePerHour());
			values.put(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED,
					umd.getFlgIsOverTimeEnabled());
			values.put(DBHelper.KEY_ROLE_ID, umd.getiRoleId());
			values.put(DBHelper.KEY_FLG_IS_WATCHKEEPER,
					umd.getFltMinWorkHourHolidays());
			values.put(DBHelper.KEY_FAX_NUMBER, umd.getFaxNumber());
			values.put(DBHelper.KEY_HAND_PHONE, umd.getHandPhone());
			values.put(DBHelper.KEY_LANDLINE_NUMBER, umd.getLandLineNumber());
			values.put(DBHelper.KEY_PAN_NUMBER, umd.getPanNumber());
			values.put(DBHelper.KEY_PIN_CODE, umd.getPinCode());
			values.put(DBHelper.KEY_ADDRESS_FIRST, umd.getAddressFirst());
			values.put(DBHelper.KEY_ADDRESS_SECOND, umd.getAddressSecond());
			values.put(DBHelper.KEY_ADDRESS_THIRD, umd.getAddressThird());
			values.put(DBHelper.KEY_CITY, umd.getCity());
			values.put(DBHelper.KEY_STATE, umd.getState());
			values.put(DBHelper.KEY_COUNTRY_ID, umd.getiCountryId());

			values.put(DBHelper.KEY_EMPLOYEE_NO, umd.getStrEmployeeNo());
			values.put(DBHelper.KEY_MIDDLE_NAME, umd.getStrMiddleName());

			result = database.insert(DBHelper.TABLE_USERS, null, values);
			Log.i("UserMaster value :== ", "inside insert of UserMaster table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in UserMaster table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateUserMasterTable(UserMaster umd) {
		// TODO Auto-generated method stub
		long result = 0;

		String whereClause = DBHelper.KEY_iUSER_ID + " =  '" + umd.getiUserId()
				+ "'";

		try {
			ContentValues values = new ContentValues();
			values.put(DBHelper.KEY_iUSER_ID, umd.getiUserId());
			values.put(DBHelper.TENANT_ID, umd.getiTenantId());
			values.put(DBHelper.KEY_USER_NAME, umd.getStrUserName());
			values.put(DBHelper.KEY_USER_PASSWORD, umd.getStrPassword());
			values.put(DBHelper.KEY_FIRST_NAME, umd.getStrFirstName());
			values.put(DBHelper.KEY_LAST_NAME, umd.getStrLastName());
			values.put(DBHelper.KEY_DATE_OF_BIRTH, umd.getDtDateOfBirth());
			values.put(DBHelper.KEY_TXT_DESCRIPTION, umd.getTxtDescription());
			values.put(DBHelper.KEY_EMAIL, umd.getStrEmail());
			values.put(DBHelper.KEY_DT_CREATED, umd.getDtCreated());
			values.put(DBHelper.KEY_DT_UPDATED, umd.getDtUpdated());
			values.put(DBHelper.KEY_FLG_STATUS, umd.getFlgStatus());
			values.put(DBHelper.KEY_FLG_DELETED, umd.getFlgDeleted());
			values.put(DBHelper.KEY_FLG_IS_DIRTY, umd.getFlgIsDirty());
			values.put(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID,
					umd.getiShipHolidayListId());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS,
					umd.getFltMinWorkHourWeekDays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY,
					umd.getFltMinWorkHourSaturdays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY,
					umd.getFltMinWorkHourSundays());
			values.put(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY,
					umd.getFltMinWorkHourHolidays());
			values.put(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE,
					umd.getFltOTIncludedInWage());
			values.put(DBHelper.KEY_FLTOT_RATE_PER_HOUR,
					umd.getFltOTRatePerHour());
			values.put(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED,
					umd.getFlgIsOverTimeEnabled());
			values.put(DBHelper.KEY_ROLE_ID, umd.getiRoleId());
			values.put(DBHelper.KEY_FLG_IS_WATCHKEEPER,
					umd.getFltMinWorkHourHolidays());
			values.put(DBHelper.KEY_FAX_NUMBER, umd.getFaxNumber());
			values.put(DBHelper.KEY_HAND_PHONE, umd.getHandPhone());
			values.put(DBHelper.KEY_LANDLINE_NUMBER, umd.getLandLineNumber());
			values.put(DBHelper.KEY_PAN_NUMBER, umd.getPanNumber());
			values.put(DBHelper.KEY_PIN_CODE, umd.getPinCode());
			values.put(DBHelper.KEY_ADDRESS_FIRST, umd.getAddressFirst());
			values.put(DBHelper.KEY_ADDRESS_SECOND, umd.getAddressSecond());
			values.put(DBHelper.KEY_ADDRESS_THIRD, umd.getAddressThird());
			values.put(DBHelper.KEY_CITY, umd.getCity());
			values.put(DBHelper.KEY_STATE, umd.getState());
			values.put(DBHelper.KEY_COUNTRY_ID, umd.getiCountryId());

			values.put(DBHelper.KEY_EMPLOYEE_NO, umd.getStrEmployeeNo());
			values.put(DBHelper.KEY_MIDDLE_NAME, umd.getStrMiddleName());

			result = database.update(DBHelper.TABLE_USERS, values, whereClause,
					null);
			Log.i("UserMaster value :== ", "inside update of UserMaster table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update all record in UserMaster table as status=1 and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateUserMasterStatus(int status) {
		// TODO Auto-generated method stub
		long result = 0;

		String whereClause = DBHelper.FLG_STATUS + " = 2 ";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_STATUS, status);

			result = database.update(DBHelper.TABLE_USERS, contentValues,
					whereClause, null);
			Log.i("UserMaster value :== ",
					"inside update of UserMasterStatus table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of UserMaster table.
	 * 
	 * @return list of UserMaster object
	 */
	public List<UserMaster> getUserMasterData() {

		List<UserMaster> userMasterData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_USERS, null, null, null,
					null, null, null);
			Log.i("dbmanager", "inside getUserMasterData");
			if (cursor != null && cursor.moveToFirst()) {
				userMasterData = new ArrayList<UserMaster>();

				do {
					UserMaster data = new UserMaster();

					data.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_iUSER_ID)));
					data.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					data.setStrUserName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_NAME)));
					data.setStrPassword(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_PASSWORD)));
					data.setStrFirstName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FIRST_NAME)));
					data.setStrLastName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LAST_NAME)));
					data.setDtDateOfBirth(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DATE_OF_BIRTH)));
					data.setTxtDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_TXT_DESCRIPTION)));
					data.setStrEmail(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMAIL)));
					data.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_CREATED)));
					data.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_UPDATED)));
					data.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_STATUS)));
					data.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_DELETED)));
					data.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_DIRTY)));
					data.setiShipHolidayListId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID)));
					data.setFltMinWorkHourWeekDays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS)));
					data.setFltMinWorkHourSaturdays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY)));
					data.setFltMinWorkHourSundays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY)));
					data.setFltMinWorkHourHolidays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY)));
					data.setFltOTIncludedInWage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE)));
					data.setFltOTRatePerHour(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_RATE_PER_HOUR)));
					data.setFlgIsOverTimeEnabled(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED)));
					data.setiRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ROLE_ID)));
					data.setFlgIsWatchkeeper(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_WATCHKEEPER)));
					data.setFaxNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FAX_NUMBER)));
					data.setHandPhone(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_HAND_PHONE)));
					data.setLandLineNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LANDLINE_NUMBER)));
					data.setPanNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PAN_NUMBER)));
					data.setPinCode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PIN_CODE)));
					data.setAddressFirst(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_FIRST)));
					data.setAddressSecond(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_SECOND)));
					data.setAddressThird(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_THIRD)));
					data.setCity(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_CITY)));
					data.setState(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_STATE)));
					data.setiCountryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_COUNTRY_ID)));

					data.setStrEmployeeNo(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMPLOYEE_NO)));
					data.setStrMiddleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIDDLE_NAME)));

					userMasterData.add(data);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserMasterData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return userMasterData;
	}

	/**
	 * fetch the single data of UserMaster table By Id.
	 * 
	 * @return list of UserMaster object
	 */
	public UserMaster getUserMasterById(String strUserId) {

		Cursor cursor = null;
		String whereClause = DBHelper.KEY_iUSER_ID + " =  '" + strUserId + "'";
		UserMaster data = new UserMaster();

		try {
			cursor = database.query(DBHelper.TABLE_USERS, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getUserMaster");
			if (cursor != null && cursor.moveToFirst()) {
				do {
					data = new UserMaster();

					data.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_iUSER_ID)));
					data.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					data.setStrUserName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_NAME)));
					data.setStrPassword(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_PASSWORD)));
					data.setStrFirstName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FIRST_NAME)));
					data.setStrLastName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LAST_NAME)));
					data.setDtDateOfBirth(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DATE_OF_BIRTH)));
					data.setTxtDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_TXT_DESCRIPTION)));
					data.setStrEmail(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMAIL)));
					data.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_CREATED)));
					data.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_UPDATED)));
					data.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_STATUS)));
					data.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_DELETED)));
					data.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_DIRTY)));
					data.setiShipHolidayListId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID)));
					data.setFltMinWorkHourWeekDays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS)));
					data.setFltMinWorkHourSaturdays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY)));
					data.setFltMinWorkHourSundays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY)));
					data.setFltMinWorkHourHolidays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY)));
					data.setFltOTIncludedInWage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE)));
					data.setFltOTRatePerHour(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_RATE_PER_HOUR)));
					data.setFlgIsOverTimeEnabled(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED)));
					data.setiRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ROLE_ID)));
					data.setFlgIsWatchkeeper(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_WATCHKEEPER)));
					data.setFaxNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FAX_NUMBER)));
					data.setHandPhone(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_HAND_PHONE)));
					data.setLandLineNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LANDLINE_NUMBER)));
					data.setPanNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PAN_NUMBER)));
					data.setPinCode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PIN_CODE)));
					data.setAddressFirst(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_FIRST)));
					data.setAddressSecond(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_SECOND)));
					data.setAddressThird(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_THIRD)));
					data.setCity(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_CITY)));
					data.setState(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_STATE)));
					data.setiCountryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_COUNTRY_ID)));

					data.setStrEmployeeNo(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMPLOYEE_NO)));
					data.setStrMiddleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIDDLE_NAME)));

				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserMaster else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return data;
	}

	/**
	 * fetch the single data of UserMaster table By Id.
	 * 
	 * @return list of UserMaster object
	 */
	public List<UserMaster> getUserMasterByUserName(String strUserName) {

		Cursor cursor = null;
		String whereClause = DBHelper.KEY_USER_NAME + " =  '" + strUserName
				+ "'";
		List<UserMaster> usrList = new ArrayList<UserMaster>();
		try {
			cursor = database.query(DBHelper.TABLE_USERS, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getUserMaster");
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserMaster data = new UserMaster();

					data.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_iUSER_ID)));
					data.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					data.setStrUserName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_NAME)));
					data.setStrPassword(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_PASSWORD)));
					data.setStrFirstName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FIRST_NAME)));
					data.setStrLastName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LAST_NAME)));
					data.setDtDateOfBirth(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DATE_OF_BIRTH)));
					data.setTxtDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_TXT_DESCRIPTION)));
					data.setStrEmail(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMAIL)));
					data.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_CREATED)));
					data.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_UPDATED)));
					data.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_STATUS)));
					data.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_DELETED)));
					data.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_DIRTY)));
					data.setiShipHolidayListId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID)));
					data.setFltMinWorkHourWeekDays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS)));
					data.setFltMinWorkHourSaturdays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY)));
					data.setFltMinWorkHourSundays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY)));
					data.setFltMinWorkHourHolidays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY)));
					data.setFltOTIncludedInWage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE)));
					data.setFltOTRatePerHour(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_RATE_PER_HOUR)));
					data.setFlgIsOverTimeEnabled(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED)));
					data.setiRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ROLE_ID)));
					data.setFlgIsWatchkeeper(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_WATCHKEEPER)));
					data.setFaxNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FAX_NUMBER)));
					data.setHandPhone(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_HAND_PHONE)));
					data.setLandLineNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LANDLINE_NUMBER)));
					data.setPanNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PAN_NUMBER)));
					data.setPinCode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PIN_CODE)));
					data.setAddressFirst(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_FIRST)));
					data.setAddressSecond(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_SECOND)));
					data.setAddressThird(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_THIRD)));
					data.setCity(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_CITY)));
					data.setState(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_STATE)));
					data.setiCountryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_COUNTRY_ID)));

					data.setStrEmployeeNo(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMPLOYEE_NO)));
					data.setStrMiddleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIDDLE_NAME)));

					usrList.add(data);

				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserMaster else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return usrList;
	}

	/**
	 * fetch the single data of UserMaster table By Id.
	 * 
	 * @return list of UserMaster object
	 */
	public List<UserMaster> getUserMasterDataByStatus(int status) {

		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " =  '" + status + "'";
		List<UserMaster> usrList = new ArrayList<UserMaster>();
		UserMaster data = new UserMaster();

		try {
			cursor = database.query(DBHelper.TABLE_USERS, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getUserMaster");
			if (cursor != null && cursor.moveToFirst()) {
				do {
					data = new UserMaster();

					data.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_iUSER_ID)));
					data.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					data.setStrUserName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_NAME)));
					data.setStrPassword(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_USER_PASSWORD)));
					data.setStrFirstName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FIRST_NAME)));
					data.setStrLastName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LAST_NAME)));
					data.setDtDateOfBirth(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DATE_OF_BIRTH)));
					data.setTxtDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_TXT_DESCRIPTION)));
					data.setStrEmail(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMAIL)));
					data.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_CREATED)));
					data.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_DT_UPDATED)));
					data.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_STATUS)));
					data.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_DELETED)));
					data.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_DIRTY)));
					data.setiShipHolidayListId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SHIP_HOLIDAY_LIST_ID)));
					data.setFltMinWorkHourWeekDays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_WEEK_DAYS)));
					data.setFltMinWorkHourSaturdays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SATUREDAY)));
					data.setFltMinWorkHourSundays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_SUNDAY)));
					data.setFltMinWorkHourHolidays(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIN_WORK_HOUR_HOLIDAY)));
					data.setFltOTIncludedInWage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_INCLUDED_IN_WAGE)));
					data.setFltOTRatePerHour(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLTOT_RATE_PER_HOUR)));
					data.setFlgIsOverTimeEnabled(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_OVER_TIME_ENABLED)));
					data.setiRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ROLE_ID)));
					data.setFlgIsWatchkeeper(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FLG_IS_WATCHKEEPER)));
					data.setFaxNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_FAX_NUMBER)));
					data.setHandPhone(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_HAND_PHONE)));
					data.setLandLineNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_LANDLINE_NUMBER)));
					data.setPanNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PAN_NUMBER)));
					data.setPinCode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_PIN_CODE)));
					data.setAddressFirst(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_FIRST)));
					data.setAddressSecond(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_SECOND)));
					data.setAddressThird(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_ADDRESS_THIRD)));
					data.setCity(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_CITY)));
					data.setState(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_STATE)));
					data.setiCountryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_COUNTRY_ID)));

					data.setStrEmployeeNo(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_EMPLOYEE_NO)));
					data.setStrMiddleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_MIDDLE_NAME)));

					usrList.add(data);

				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserMaster else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return usrList;
	}

	/**
	 * This method will insert single record in Checklist_Section_Item table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertCheckListSectionItemTable(
			CheckListSectionItems checkListSectionItems) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.CHECKLIST_SECTIONITEMS_ID,
					checkListSectionItems.getiCheckListSectionItemsId());
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					checkListSectionItems.getiCheckListSectionId());
			contentValues.put(DBHelper.ITEMS_NAME,
					checkListSectionItems.getStrItemNames());
			contentValues.put(DBHelper.CREATED_DATE,
					checkListSectionItems.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					checkListSectionItems.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					checkListSectionItems.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					checkListSectionItems.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					checkListSectionItems.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					checkListSectionItems.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					checkListSectionItems.getFlgIsDirty());
			contentValues.put(DBHelper.ITEM_REMARKS,
					checkListSectionItems.getStrItemRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					checkListSectionItems.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					checkListSectionItems.getiTenantId());

			result = database.insert(DBHelper.CHECKLIST_SECTION_ITEMS, null,
					contentValues);
			Log.i("CHECKLIST_SECTION_ITEMS value :== ",
					"inside insert of CheckListSectionItems table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Checklist_Section_Item table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateCheckListSectionItemTable(
			CheckListSectionItems checkListSectionItems) {
		// TODO Auto-generated method stub
		long result = 0;

		String whereClause = DBHelper.CHECKLIST_SECTION_ID + " =  '"
				+ checkListSectionItems.getiCheckListSectionItemsId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.CHECKLIST_SECTIONITEMS_ID,
					checkListSectionItems.getiCheckListSectionItemsId());
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					checkListSectionItems.getiCheckListSectionId());
			contentValues.put(DBHelper.ITEMS_NAME,
					checkListSectionItems.getStrItemNames());
			contentValues.put(DBHelper.CREATED_DATE,
					checkListSectionItems.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					checkListSectionItems.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					checkListSectionItems.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					checkListSectionItems.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					checkListSectionItems.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					checkListSectionItems.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					checkListSectionItems.getFlgIsDirty());
			contentValues.put(DBHelper.ITEM_REMARKS,
					checkListSectionItems.getStrItemRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					checkListSectionItems.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					checkListSectionItems.getiTenantId());

			result = database.update(DBHelper.CHECKLIST_SECTION_ITEMS,
					contentValues, whereClause, null);
			Log.i("CHECKLIST_SECTION_ITEMS value :== ",
					"inside update of CheckListSectionItems table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of CheckListSectionItems table.
	 * 
	 * @return list of CheckListSectionItems object
	 */
	public List<CheckListSectionItems> getCheckListSectionItemsData() {

		List<CheckListSectionItems> checkListSectionItemsData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.CHECKLIST_SECTION_ITEMS, null,
					null, null, null, null, null);
			Log.i("dbmanager", "inside getCheckListSectionItemsData");
			if (cursor != null && cursor.moveToFirst()) {
				checkListSectionItemsData = new ArrayList<CheckListSectionItems>();

				do {
					CheckListSectionItems checkListSectionItems = new CheckListSectionItems();
					checkListSectionItems
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					checkListSectionItems
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					checkListSectionItems
							.setStrItemNames(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEMS_NAME)));
					checkListSectionItems
							.setStrItemRemarks(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));

					checkListSectionItems
							.setCreatedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					checkListSectionItems
							.setModifiedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					checkListSectionItems.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					checkListSectionItems.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					checkListSectionItems.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					checkListSectionItems.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					checkListSectionItems.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					checkListSectionItems.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					checkListSectionItemsData.add(checkListSectionItems);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside checkListSectionItemsData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return checkListSectionItemsData;
	}

	/**
	 * fetch the complete data of CheckListSectionItems table.
	 * 
	 * @return list of CheckListSectionItems on the basis of id object
	 */
	public List<CheckListSectionItems> getCheckListSectionItemsDataById(
			String checkListSectionItemsId) {

		List<CheckListSectionItems> checkListSectionItemsData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.CHECKLIST_SECTIONITEMS_ID + " =  '"
				+ checkListSectionItemsId + "'";

		try {
			cursor = database.query(DBHelper.CHECKLIST_SECTION_ITEMS, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getCheckListSectionItemsData");
			if (cursor != null && cursor.moveToFirst()) {
				checkListSectionItemsData = new ArrayList<CheckListSectionItems>();

				do {
					CheckListSectionItems checkListSectionItems = new CheckListSectionItems();
					checkListSectionItems
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					checkListSectionItems
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					checkListSectionItems
							.setStrItemNames(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEMS_NAME)));
					checkListSectionItems
							.setStrItemRemarks(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));

					checkListSectionItems
							.setCreatedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					checkListSectionItems
							.setModifiedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					checkListSectionItems.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					checkListSectionItems.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					checkListSectionItems.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					checkListSectionItems.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					checkListSectionItems.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					checkListSectionItems.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					checkListSectionItemsData.add(checkListSectionItems);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside checkListSectionItemsData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return checkListSectionItemsData;
	}

	/**
	 * fetch the complete data of CheckListSectionItems table by section id.
	 * 
	 * @return list of CheckListSectionItems on the basis of section id object
	 */
	public List<CheckListSectionItems> getCheckListSectionItemsDataBySectionId(
			String checkListSectionId) {

		List<CheckListSectionItems> checkListSectionItemsData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.CHECKLIST_SECTION_ID + " =  '"
				+ checkListSectionId + "' order by " + DBHelper.SEQUENCE_NUMBER;

		try {
			cursor = database.query(DBHelper.CHECKLIST_SECTION_ITEMS, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getCheckListSectionItemsData");
			if (cursor != null && cursor.moveToFirst()) {
				checkListSectionItemsData = new ArrayList<CheckListSectionItems>();

				do {
					CheckListSectionItems checkListSectionItems = new CheckListSectionItems();
					checkListSectionItems
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					checkListSectionItems
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					checkListSectionItems
							.setStrItemNames(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEMS_NAME)));
					checkListSectionItems
							.setStrItemRemarks(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));

					checkListSectionItems
							.setCreatedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					checkListSectionItems
							.setModifiedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					checkListSectionItems.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					checkListSectionItems.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					checkListSectionItems.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					checkListSectionItems.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					checkListSectionItems.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					checkListSectionItems.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					checkListSectionItemsData.add(checkListSectionItems);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside checkListSectionItemsData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return checkListSectionItemsData;
	}

	/**
	 * This method will insert single record in FILLED_CHECKLIST table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFilldCheckListTable(FilledCheckList filledCheckList) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_CHECKLIST_ID,
					filledCheckList.getiFilledCheckListId());
			contentValues.put(DBHelper.CHECKLIST_SECTIONITEMS_ID,
					filledCheckList.getiCheckListSectionItemsId());
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					filledCheckList.getiCheckListSectionId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledCheckList.getiVesselInspectionId());
			contentValues.put(DBHelper.FLG_CHECKED,
					filledCheckList.getFlgChecked());
			contentValues.put(DBHelper.CREATED_DATE,
					filledCheckList.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledCheckList.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					filledCheckList.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					filledCheckList.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					filledCheckList.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					filledCheckList.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					filledCheckList.getFlgIsDirty());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					filledCheckList.getFlgIsDeviceDirty());
			contentValues
					.put(DBHelper.REMARKS, filledCheckList.getStrRemarks());
			/*
			 * contentValues.put(DBHelper.SEQUENCE_NUMBER,
			 * filledCheckList.getSequence());
			 */
			contentValues.put(DBHelper.TENANT_ID,
					filledCheckList.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledCheckList.getiShipId());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					filledCheckList.getFlgIsEdited());

			result = database.insert(DBHelper.FILLED_CHECKLIST, null,
					contentValues);
			Log.i("FILLED_CHECKLIST value :== ",
					"inside insert of FILLED_CHECKLIST table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will insert single record in FILLED_CHECKLIST table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFilldCheckListTable(FilledCheckList filledCheckList) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_CHECKLIST_ID + " =  '"
				+ filledCheckList.getiFilledCheckListId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_CHECKLIST_ID,
					filledCheckList.getiFilledCheckListId());
			contentValues.put(DBHelper.CHECKLIST_SECTIONITEMS_ID,
					filledCheckList.getiCheckListSectionItemsId());
			contentValues.put(DBHelper.CHECKLIST_SECTION_ID,
					filledCheckList.getiCheckListSectionId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledCheckList.getiVesselInspectionId());
			contentValues.put(DBHelper.FLG_CHECKED,
					filledCheckList.getFlgChecked());
			contentValues.put(DBHelper.CREATED_DATE,
					filledCheckList.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledCheckList.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					filledCheckList.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					filledCheckList.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					filledCheckList.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					filledCheckList.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					filledCheckList.getFlgIsDirty());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					filledCheckList.getFlgIsDeviceDirty());
			contentValues
					.put(DBHelper.REMARKS, filledCheckList.getStrRemarks());
			/*
			 * contentValues.put(DBHelper.SEQUENCE_NUMBER,
			 * filledCheckList.getSequence());
			 */
			contentValues.put(DBHelper.TENANT_ID,
					filledCheckList.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledCheckList.getiShipId());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					filledCheckList.getFlgIsEdited());

			result = database.update(DBHelper.FILLED_CHECKLIST, contentValues,
					whereClause, null);
			Log.i("FILLED_CHECKLIST value :== ",
					"inside insert of FILLED_CHECKLIST table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update dirty flag single record in FILLED_CHECKLIST
	 * table and return the value whether the record is inserted successfully in
	 * database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateDirtyFlagFilldCheckListTable(String strFilledCheckListId) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_CHECKLIST_ID + " =  '"
				+ strFilledCheckListId + "'";

		try {

			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_DIRTY, 0);

			result = database.update(DBHelper.FILLED_CHECKLIST, contentValues,
					whereClause, null);
			Log.i("FILLED_CHECKLIST value :== ",
					"inside update of FILLED_CHECKLIST table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of FilledCheckList table.
	 * 
	 * @return list of FilledCheckList object
	 */
	public List<FilledCheckList> getFilledCheckListData() {

		List<FilledCheckList> filledCheckListData = null;
		Cursor cursor = null;

		try {
			String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1'";
			cursor = database.query(DBHelper.FILLED_CHECKLIST, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledCheckListData");
			if (cursor != null && cursor.moveToFirst()) {
				filledCheckListData = new ArrayList<FilledCheckList>();

				do {
					FilledCheckList filledCheckList = new FilledCheckList();

					filledCheckList
							.setiFilledCheckListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_CHECKLIST_ID)));
					filledCheckList
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					filledCheckList
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					filledCheckList
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledCheckList.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));

					/*
					 * filledCheckList.setStrDesc(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.REMARKS)));
					 */
					filledCheckList.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					filledCheckList.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledCheckList.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledCheckList.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledCheckList.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					filledCheckList.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledCheckList.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledCheckList
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					filledCheckList.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledCheckList.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledCheckList.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledCheckListData.add(filledCheckList);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledCheckListData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledCheckListData;
	}

	/**
	 * fetch the complete data of FilledCheckList table by ID.
	 * 
	 * @return list of FilledCheckList object
	 */
	public List<FilledCheckList> getFilledCheckListDataById(
			String strFilledCheckListId) {

		List<FilledCheckList> filledCheckListData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FILLED_CHECKLIST_ID + " =  '"
				+ strFilledCheckListId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_CHECKLIST, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledCheckListData");
			if (cursor != null && cursor.moveToFirst()) {
				filledCheckListData = new ArrayList<FilledCheckList>();

				do {
					FilledCheckList filledCheckList = new FilledCheckList();

					filledCheckList
							.setiFilledCheckListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_CHECKLIST_ID)));
					filledCheckList
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					filledCheckList
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					filledCheckList
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledCheckList.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));

					/*
					 * filledCheckList.setStrDesc(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.REMARKS)));
					 */
					filledCheckList.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					filledCheckList.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledCheckList.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledCheckList.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledCheckList.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					filledCheckList.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledCheckList.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledCheckList
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					filledCheckList.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledCheckList.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledCheckList.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledCheckListData.add(filledCheckList);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledCheckListData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledCheckListData;
	}

	/**
	 * fetch the complete data of FilledCheckList table by vesselInspectionId,
	 * checkListSectionId, and checkListSecionItemId.
	 * 
	 * @return list of FilledCheckList object
	 */
	public List<FilledCheckList> getFilledCheckListDataByIds(
			String vesselInspectionId, String checklistSectionId,
			String checklistSecionItemId) {

		List<FilledCheckList> filledCheckListData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " =  '"
				+ vesselInspectionId + "' and " + DBHelper.CHECKLIST_SECTION_ID
				+ " = '" + checklistSectionId + "' and "
				+ DBHelper.CHECKLIST_SECTIONITEMS_ID + " = '"
				+ checklistSecionItemId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_CHECKLIST, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledCheckListData");
			if (cursor != null && cursor.moveToFirst()) {
				filledCheckListData = new ArrayList<FilledCheckList>();

				do {
					FilledCheckList filledCheckList = new FilledCheckList();

					filledCheckList
							.setiFilledCheckListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_CHECKLIST_ID)));
					filledCheckList
							.setiCheckListSectionItemsId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTIONITEMS_ID)));
					filledCheckList
							.setiCheckListSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.CHECKLIST_SECTION_ID)));
					filledCheckList
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledCheckList.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));

					/*
					 * filledCheckList.setStrDesc(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.REMARKS)));
					 */

					filledCheckList.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					filledCheckList.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledCheckList.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledCheckList.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledCheckList.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					filledCheckList.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledCheckList.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledCheckList
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					filledCheckList.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledCheckList.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledCheckList.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledCheckListData.add(filledCheckList);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledCheckListData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledCheckListData;
	}

	/**
	 * This method will insert single record in FormSection table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFormSectionTable(FormSection formSection) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_SECTION_ID,
					formSection.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_NAME,
					formSection.getStrSectionName());
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					formSection.getStrFormCategory());
			contentValues.put(DBHelper.CREATED_DATE,
					formSection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formSection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, formSection.getCreatedBy());
			contentValues
					.put(DBHelper.MODIFIED_BY, formSection.getModifiedBy());
			contentValues
					.put(DBHelper.FLG_DELETED, formSection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, formSection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formSection.getFlgIsDirty());
			contentValues.put(DBHelper.REMARKS, formSection.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					formSection.getSequence());
			contentValues.put(DBHelper.TENANT_ID, formSection.getiTenantId());

			result = database
					.insert(DBHelper.FORM_SECTION, null, contentValues);
			Log.i("FORM_SECTION value :== ",
					"inside insert of CheckListSection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in FormSection table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFormSectionTable(FormSection formSection) {

		long result = 0;
		String whereClause = DBHelper.FORM_SECTION_ID + " =  '"
				+ formSection.getiFormSectionId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_SECTION_ID,
					formSection.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_NAME,
					formSection.getStrSectionName());
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					formSection.getStrFormCategory());
			contentValues.put(DBHelper.CREATED_DATE,
					formSection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formSection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, formSection.getCreatedBy());
			contentValues
					.put(DBHelper.MODIFIED_BY, formSection.getModifiedBy());
			contentValues
					.put(DBHelper.FLG_DELETED, formSection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, formSection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formSection.getFlgIsDirty());
			contentValues.put(DBHelper.REMARKS, formSection.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					formSection.getSequence());
			contentValues.put(DBHelper.TENANT_ID, formSection.getiTenantId());

			result = database.update(DBHelper.FORM_SECTION, contentValues,
					whereClause, null);
			Log.i("FORM_SECTION value :== ",
					"inside update of CheckListSection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of FormSection table.
	 * 
	 * @return list of FormSection object
	 */
	public List<FormSection> getFormSectionData(String strCategoryId) {

		List<FormSection> formSectionData = null;
		Cursor cursor = null;

		String whereClause = DBHelper.FORM_CATEGORY_ID + " =  '"
				+ strCategoryId + "' and " + DBHelper.FLG_DELETED + "='0'";
		String orderBy = DBHelper.SEQUENCE_NUMBER;

		try {
			cursor = database.query(DBHelper.FORM_SECTION, null, whereClause,
					null, null, null, orderBy);
			Log.i("dbmanager", "inside getFormSectionData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionData = new ArrayList<FormSection>();

				do {
					FormSection formSection = new FormSection();
					formSection.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSection
							.setStrSectionName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_NAME)));
					formSection.setStrFormCategory(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					formSection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));
					formSection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionData.add(formSection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionData;
	}

	/**
	 * fetch the complete data of FormSection table by Id.
	 * 
	 * @return list of FormSection object by id
	 */
	public List<FormSection> getFormSectionDataById(String strFormSectionid) {

		List<FormSection> formSectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FORM_SECTION_ID + " =  '"
				+ strFormSectionid + "'";

		try {
			cursor = database.query(DBHelper.FORM_SECTION, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getFormSectionData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionData = new ArrayList<FormSection>();

				do {
					FormSection formSection = new FormSection();
					formSection.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSection
							.setStrSectionName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_NAME)));
					formSection.setStrFormCategory(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					formSection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));
					formSection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionData.add(formSection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionData;
	}

	/**
	 * This method will insert single record in FormSectionItem table and return
	 * the value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFormSectionItemTable(FormSectionItem formSectionItem) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					formSectionItem.getiFormSectionItemId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					formSectionItem.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_NAME,
					formSectionItem.getStrItemName());
			contentValues.put(DBHelper.CREATED_DATE,
					formSectionItem.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formSectionItem.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					formSectionItem.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					formSectionItem.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					formSectionItem.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					formSectionItem.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formSectionItem.getFlgIsDirty());
			contentValues.put(DBHelper.ITEM_REMARKS,
					formSectionItem.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					formSectionItem.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					formSectionItem.getiTenantId());

			result = database.insert(DBHelper.TABLE_FORM_SECTION_ITEM, null,
					contentValues);
			Log.i("TABLE_FORM_SECTION_ITEM value :== ",
					"inside insert of TABLE_FORM_SECTION_ITEM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in FormSectionItem table and return
	 * the value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFormSectionItemTable(FormSectionItem formSectionItem) {

		long result = 0;
		String whereClause = DBHelper.FORM_SECTION_ITEM_ID + " =  '"
				+ formSectionItem.getiFormSectionItemId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					formSectionItem.getiFormSectionItemId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					formSectionItem.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_NAME,
					formSectionItem.getStrItemName());
			contentValues.put(DBHelper.CREATED_DATE,
					formSectionItem.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					formSectionItem.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					formSectionItem.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					formSectionItem.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					formSectionItem.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					formSectionItem.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					formSectionItem.getFlgIsDirty());
			contentValues.put(DBHelper.ITEM_REMARKS,
					formSectionItem.getStrRemarks());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					formSectionItem.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					formSectionItem.getiTenantId());

			result = database.update(DBHelper.TABLE_FORM_SECTION_ITEM,
					contentValues, whereClause, null);
			Log.i("TABLE_FORM_SECTION_ITEM value :== ",
					"inside update of TABLE_FORM_SECTION_ITEM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of FormSectionItem table by Id.
	 * 
	 * @return list of FormSection object by id
	 */
	public List<FormSectionItem> getFormSectionItemDataById(
			String strFormSectionItemId) {

		List<FormSectionItem> formSectionItemData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FORM_SECTION_ITEM_ID + " =  '"
				+ strFormSectionItemId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_FORM_SECTION_ITEM, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionItemData = new ArrayList<FormSectionItem>();

				do {
					FormSectionItem formSectionItem = new FormSectionItem();
					formSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					formSectionItem.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSectionItem
							.setStrItemName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_NAME)));
					formSectionItem.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					formSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSectionItem.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionItemData.add(formSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionItemData;
	}

	/**
	 * fetch the complete data of FormSectionItem table by Id.
	 * 
	 * @return list of FormSection object by id
	 */
	public List<FormSectionItem> getFormSectionItemDataBySectionId(
			String strFormSectionId) {

		List<FormSectionItem> formSectionItemData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FORM_SECTION_ID + " =  '"
				+ strFormSectionId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_FORM_SECTION_ITEM, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionItemData = new ArrayList<FormSectionItem>();

				do {
					FormSectionItem formSectionItem = new FormSectionItem();
					formSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					formSectionItem.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSectionItem
							.setStrItemName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_NAME)));
					formSectionItem.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					formSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSectionItem.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionItemData.add(formSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionItemData;
	}

	/**
	 * @author pushkar.m fetch the complete data of FormSectionItem according to
	 *         ship.
	 * @return list of FormSection object by id
	 */
	public List<FormSectionItem> getShipFormSectionItemDataBySectionId(
			String iFormSectionId, int shipId) {

		List<FormSectionItem> formSectionItemData = null;
		Cursor cursor = null;
		/*
		 * String whereClause = DBHelper.FORM_SECTION_ID + " =  '" +
		 * strFormSectionId + "'";
		 */

		String whereClause = DBHelper.FORM_SECTION_ITEM_ID + " in (select "
				+ DBHelper.FORM_SECTION_ITEM_ID + " from "
				+ DBHelper.TABLE_SHIP_FORM_SECTION_ITEM + " where "
				+ DBHelper.SHIP_ID + "=" + shipId + " and "
				+ DBHelper.FORM_SECTION_ID + "='" + iFormSectionId + "' and "
				+ DBHelper.FLG_DELETED + " = 0 )";

		try {
			cursor = database.query(DBHelper.TABLE_FORM_SECTION_ITEM, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionItemData = new ArrayList<FormSectionItem>();

				do {
					FormSectionItem formSectionItem = new FormSectionItem();
					formSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					formSectionItem.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSectionItem
							.setStrItemName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_NAME)));
					formSectionItem.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					formSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSectionItem.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionItemData.add(formSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionItemData;
	}

	/**
	 * fetch the complete data of FormSectionItem table .
	 * 
	 * @return list of FormSection object
	 */
	public List<FormSectionItem> getFormSectionItemData() {

		List<FormSectionItem> formSectionItemData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_FORM_SECTION_ITEM, null,
					null, null, null, null, null);
			Log.i("dbmanager", "inside getFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				formSectionItemData = new ArrayList<FormSectionItem>();

				do {
					FormSectionItem formSectionItem = new FormSectionItem();
					formSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					formSectionItem.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					formSectionItem
							.setStrItemName(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_NAME)));
					formSectionItem.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					formSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					formSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					formSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					formSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					formSectionItem.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					formSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					formSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					formSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					formSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					formSectionItemData.add(formSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formSectionItemData;
	}

	/**
	 * This method will insert single record in ShipFormSectionItem table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertShipFormSectionItemTable(
			ShipFormSectionItem shipFormSectionItem) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SHIP_FORM_SECTION_ITEM_ID,
					shipFormSectionItem.getiShipFormSectionItemId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					shipFormSectionItem.getiFormSectionId());

			/*
			 * contentValues.put(DBHelper.FORM_SECTION_ITEM_NAME,
			 * shipFormSectionItem.getStrItemName());
			 */
			contentValues.put(DBHelper.CREATED_DATE,
					shipFormSectionItem.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					shipFormSectionItem.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					shipFormSectionItem.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					shipFormSectionItem.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					shipFormSectionItem.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					shipFormSectionItem.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					shipFormSectionItem.getFlgIsDirty());
			/*
			 * contentValues .put(DBHelper.ITEM_REMARKS,
			 * shipFormSectionItem.getStrRemarks());
			 * contentValues.put(DBHelper.SEQUENCE_NUMBER,
			 * shipFormSectionItem.getSequence());
			 */
			contentValues.put(DBHelper.TENANT_ID,
					shipFormSectionItem.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID,
					shipFormSectionItem.getiShipId());
			contentValues.put(DBHelper.FIELD_ONE,
					shipFormSectionItem.getStrField1());

			contentValues.put(DBHelper.FIELD_TWO,
					shipFormSectionItem.getStrField2());

			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					shipFormSectionItem.getiFormSectionItemId());

			contentValues.put(DBHelper.SHIP_TYPE_ID,
					shipFormSectionItem.getiShipTypeId());
			contentValues.put(DBHelper.ASSOCIATEED_TENANT,
					shipFormSectionItem.getAssociatedTenant());
			contentValues.put(DBHelper.DYNAMIC_FORM_LIST_ID,
					shipFormSectionItem.getiDynamicFormListId());

			result = database.insert(DBHelper.TABLE_SHIP_FORM_SECTION_ITEM,
					null, contentValues);
			Log.i("TABLE_SHIP_FORM_SECTION_ITEM value :== ",
					"inside insert of TABLE_SHIP_FORM_SECTION_ITEM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in ShipFormSectionItem table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateShipFormSectionItemTable(
			ShipFormSectionItem shipFormSectionItem) {

		long result = 0;
		String whereClause = DBHelper.SHIP_FORM_SECTION_ITEM_ID + " =  '"
				+ shipFormSectionItem.getiShipFormSectionItemId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SHIP_FORM_SECTION_ITEM_ID,
					shipFormSectionItem.getiShipFormSectionItemId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					shipFormSectionItem.getiFormSectionId());
			/*
			 * contentValues.put(DBHelper.FORM_SECTION_ITEM_NAME,
			 * shipFormSectionItem.getStrItemName());
			 */
			contentValues.put(DBHelper.CREATED_DATE,
					shipFormSectionItem.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					shipFormSectionItem.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					shipFormSectionItem.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					shipFormSectionItem.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					shipFormSectionItem.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					shipFormSectionItem.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					shipFormSectionItem.getFlgIsDirty());
			/*
			 * contentValues .put(DBHelper.ITEM_REMARKS,
			 * shipFormSectionItem.getStrRemarks());
			 * 
			 * contentValues.put(DBHelper.SEQUENCE_NUMBER,
			 * shipFormSectionItem.getSequence());
			 */
			contentValues.put(DBHelper.TENANT_ID,
					shipFormSectionItem.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID,
					shipFormSectionItem.getiShipId());

			contentValues.put(DBHelper.FIELD_ONE,
					shipFormSectionItem.getStrField1());

			contentValues.put(DBHelper.FIELD_TWO,
					shipFormSectionItem.getStrField2());

			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					shipFormSectionItem.getiFormSectionItemId());

			contentValues.put(DBHelper.SHIP_TYPE_ID,
					shipFormSectionItem.getiShipTypeId());
			contentValues.put(DBHelper.ASSOCIATEED_TENANT,
					shipFormSectionItem.getAssociatedTenant());
			contentValues.put(DBHelper.DYNAMIC_FORM_LIST_ID,
					shipFormSectionItem.getiDynamicFormListId());

			result = database.update(DBHelper.TABLE_SHIP_FORM_SECTION_ITEM,
					contentValues, whereClause, null);
			Log.i("TABLE_SHIP_FORM_SECTION_ITEM value :== ",
					"inside update of TABLE_SHIP_FORM_SECTION_ITEM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of ShipFormSectionItem table .
	 * 
	 * @return list of ShipFormSectionItem object
	 */
	public List<ShipFormSectionItem> getShipFormSectionItemData() {

		List<ShipFormSectionItem> shipFormSectionItemData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_SHIP_FORM_SECTION_ITEM,
					null, null, null, null, null, null);
			Log.i("dbmanager", "inside getShipFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				shipFormSectionItemData = new ArrayList<ShipFormSectionItem>();

				do {
					ShipFormSectionItem shipFormSectionItem = new ShipFormSectionItem();
					shipFormSectionItem
							.setiShipFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.SHIP_FORM_SECTION_ITEM_ID)));
					shipFormSectionItem
							.setiFormSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					/*
					 * shipFormSectionItem
					 * .setStrItemName(cursor.getString(cursor
					 * .getColumnIndexOrThrow
					 * (DBHelper.FORM_SECTION_ITEM_NAME)));
					 * shipFormSectionItem.setStrRemarks(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					 */
					shipFormSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));

					shipFormSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));

					shipFormSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));

					shipFormSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					/*
					 * shipFormSectionItem.setSequence(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					 */
					shipFormSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));

					shipFormSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));

					shipFormSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));

					shipFormSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					shipFormSectionItem.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					shipFormSectionItem.setStrField1(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_ONE)));

					shipFormSectionItem.setStrField2(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_TWO)));

					shipFormSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));

					shipFormSectionItem.setiShipTypeId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_TYPE_ID)));
					shipFormSectionItem
							.setAssociatedTenant(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ASSOCIATEED_TENANT)));
					shipFormSectionItem
							.setiDynamicFormListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DYNAMIC_FORM_LIST_ID)));

					shipFormSectionItemData.add(shipFormSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return shipFormSectionItemData;
	}

	/**
	 * fetch the data of ShipFormSectionItem table by ID.
	 * 
	 * @return list of ShipFormSectionItem object
	 */
	public List<ShipFormSectionItem> getShipFormSectionItemDataById(
			String strShipFormSectionItemId) {

		List<ShipFormSectionItem> shipFormSectionItemData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.SHIP_FORM_SECTION_ITEM_ID + " =  '"
				+ strShipFormSectionItemId + "' and " + DBHelper.FLG_DELETED
				+ "='0'";

		try {
			cursor = database.query(DBHelper.TABLE_SHIP_FORM_SECTION_ITEM,
					null, whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getShipFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				shipFormSectionItemData = new ArrayList<ShipFormSectionItem>();

				do {
					ShipFormSectionItem shipFormSectionItem = new ShipFormSectionItem();
					shipFormSectionItem
							.setiShipFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.SHIP_FORM_SECTION_ITEM_ID)));
					shipFormSectionItem
							.setiFormSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					/*
					 * shipFormSectionItem
					 * .setStrItemName(cursor.getString(cursor
					 * .getColumnIndexOrThrow
					 * (DBHelper.FORM_SECTION_ITEM_NAME)));
					 * shipFormSectionItem.setStrRemarks(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					 */
					shipFormSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));

					shipFormSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));

					shipFormSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));

					shipFormSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					/*
					 * shipFormSectionItem.setSequence(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					 */
					shipFormSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));

					shipFormSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));

					shipFormSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));

					shipFormSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					shipFormSectionItem.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					shipFormSectionItem.setStrField1(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_ONE)));

					shipFormSectionItem.setStrField2(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_TWO)));

					shipFormSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					shipFormSectionItem.setiShipTypeId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_TYPE_ID)));
					shipFormSectionItem
							.setAssociatedTenant(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ASSOCIATEED_TENANT)));
					shipFormSectionItem
							.setiDynamicFormListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DYNAMIC_FORM_LIST_ID)));

					shipFormSectionItemData.add(shipFormSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return shipFormSectionItemData;
	}

	/**
	 * fetch the data of ShipFormSectionItem table by section ID.
	 * 
	 * @return list of ShipFormSectionItem object
	 */
	public List<ShipFormSectionItem> getShipFormSectionItemDataByFormSectionId(
			String strFormSectionId) {

		List<ShipFormSectionItem> shipFormSectionItemData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FORM_SECTION_ID + " =  '"
				+ strFormSectionId + "' and " + DBHelper.FLG_DELETED + "='0'";

		try {
			cursor = database.query(DBHelper.TABLE_SHIP_FORM_SECTION_ITEM,
					null, whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getShipFormSectionItemData");
			if (cursor != null && cursor.moveToFirst()) {
				shipFormSectionItemData = new ArrayList<ShipFormSectionItem>();

				do {
					ShipFormSectionItem shipFormSectionItem = new ShipFormSectionItem();
					shipFormSectionItem
							.setiShipFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.SHIP_FORM_SECTION_ITEM_ID)));
					shipFormSectionItem
							.setiFormSectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					/*
					 * shipFormSectionItem
					 * .setStrItemName(cursor.getString(cursor
					 * .getColumnIndexOrThrow
					 * (DBHelper.FORM_SECTION_ITEM_NAME)));
					 * shipFormSectionItem.setStrRemarks(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.ITEM_REMARKS)));
					 */
					shipFormSectionItem.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));

					shipFormSectionItem.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));

					shipFormSectionItem.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));

					shipFormSectionItem.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));

					/*
					 * shipFormSectionItem.setSequence(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					 */
					shipFormSectionItem.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));

					shipFormSectionItem.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));

					shipFormSectionItem.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));

					shipFormSectionItem.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					shipFormSectionItem.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					shipFormSectionItem.setStrField1(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_ONE)));

					shipFormSectionItem.setStrField2(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FIELD_TWO)));

					shipFormSectionItem
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					shipFormSectionItem.setiShipTypeId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_TYPE_ID)));
					shipFormSectionItem
							.setAssociatedTenant(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.ASSOCIATEED_TENANT)));
					shipFormSectionItem
							.setiDynamicFormListId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DYNAMIC_FORM_LIST_ID)));

					shipFormSectionItemData.add(shipFormSectionItem);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return shipFormSectionItemData;
	}

	/**
	 * This method will insert single record in VesselInspection table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertVesselInspectionTable(VesselInspection vesselInspection) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					vesselInspection.getiVesselInspectionId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_TITLE,
					vesselInspection.getStrInspectionTitle());
			contentValues.put(DBHelper.VESSEL_INSPECTION_DATE,
					vesselInspection.getInspectionDate());
			contentValues.put(DBHelper.CREATED_DATE,
					vesselInspection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					vesselInspection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					vesselInspection.getCreatedBy());
			contentValues.put(DBHelper.INSPECTED_BY,
					vesselInspection.getInspectedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					vesselInspection.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					vesselInspection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					vesselInspection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					vesselInspection.getFlgIsDirty());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					vesselInspection.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					vesselInspection.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, vesselInspection.getiShipId());
			contentValues.put(DBHelper.FLG_IS_LOCKED,
					vesselInspection.getFlgIsLocked());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					vesselInspection.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					vesselInspection.getFlgIsEdited());
			contentValues.put(DBHelper.REVIEWED_BY,
					vesselInspection.getReviewedBy());
			contentValues.put(DBHelper.APPROVED_BY,
					vesselInspection.getApprovedBy());
			contentValues.put(DBHelper.ACCEPTED_BY,
					vesselInspection.getAcceptedBy());
			contentValues.put(DBHelper.COMPLETED_BY,
					vesselInspection.getCompletedBy());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_STATUS_ID,
					vesselInspection.getiVesselInspectionStatusId());
			contentValues.put(DBHelper.FLG_REVERTED,
					vesselInspection.getFlgReverted());
			contentValues.put(DBHelper.REMARKS,
					vesselInspection.getStrRemarks());

			result = database.insert(DBHelper.VESSEL_INSPECTION, null,
					contentValues);
			Log.i("VESSEL_INSPECTION value :== ",
					"inside insert of vesselInspection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in VesselInspection table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateVesselInspectionTable(VesselInspection vesselInspection) {

		long result = 0;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ vesselInspection.getiVesselInspectionId() + "'";
		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					vesselInspection.getiVesselInspectionId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_TITLE,
					vesselInspection.getStrInspectionTitle());
			contentValues.put(DBHelper.VESSEL_INSPECTION_DATE,
					vesselInspection.getInspectionDate());
			contentValues.put(DBHelper.CREATED_DATE,
					vesselInspection.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					vesselInspection.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					vesselInspection.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					vesselInspection.getModifiedBy());
			contentValues.put(DBHelper.INSPECTED_BY,
					vesselInspection.getInspectedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					vesselInspection.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					vesselInspection.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					vesselInspection.getFlgIsDirty());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					vesselInspection.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					vesselInspection.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, vesselInspection.getiShipId());
			contentValues.put(DBHelper.FLG_IS_LOCKED,
					vesselInspection.getFlgIsLocked());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					vesselInspection.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					vesselInspection.getFlgIsEdited());
			contentValues.put(DBHelper.REVIEWED_BY,
					vesselInspection.getReviewedBy());
			contentValues.put(DBHelper.APPROVED_BY,
					vesselInspection.getApprovedBy());
			contentValues.put(DBHelper.ACCEPTED_BY,
					vesselInspection.getAcceptedBy());
			contentValues.put(DBHelper.COMPLETED_BY,
					vesselInspection.getCompletedBy());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_STATUS_ID,
					vesselInspection.getiVesselInspectionStatusId());
			contentValues.put(DBHelper.FLG_REVERTED,
					vesselInspection.getFlgReverted());
			contentValues.put(DBHelper.REMARKS,
					vesselInspection.getStrRemarks());

			result = database.update(DBHelper.VESSEL_INSPECTION, contentValues,
					whereClause, null);
			Log.i("VESSEL_INSPECTION value :== ",
					"inside insert of vesselInspection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update dirty flag of single record in VesselInspection
	 * table and return the value whether the record is inserted successfully in
	 * database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateDirtyVesselInspectionTable(String strVesselInspectionId) {

		long result = 0;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strVesselInspectionId + "'";
		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_DIRTY, 0);

			/**
			 * below flag for sync confirmation
			 */
			contentValues.put(DBHelper.FLG_IS_EDITED, 5);

			result = database.update(DBHelper.VESSEL_INSPECTION, contentValues,
					whereClause, null);
			Log.i("VESSEL_INSPECTION value :== ",
					"inside update of vesselInspection table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of VesselInspection table by flgStatus.
	 * 
	 * @return list of VesselInspection object by flgStatus
	 * 
	 *         flgStatus 1: for pending , o : for complete
	 */
	public List<VesselInspection> getVesselInspectionDataByflgStatus(
			int flgStatus) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " =  '" + flgStatus
				+ "' and " + DBHelper.FLG_DELETED + "='0'";

		try {

			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table by flgStatus and latest
	 * inspection fetch
	 * 
	 * @return list of VesselInspection object by flgStatus
	 * 
	 *         flgStatus 1: for pending , o : for complete
	 */
	public List<VesselInspection> getVesselInspectionLatestData(int flgStatus) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " =  '" + flgStatus
				+ "' and " + DBHelper.FLG_DELETED + "='0' order by "
				+ DBHelper.VESSEL_INSPECTION_DATE + " DESC";

		try {

			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	public List<VesselInspection> getVesselInspectionDataWithinStatus(
			int flgStatus) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " < '" + flgStatus
				+ "' and " + DBHelper.FLG_DELETED
				+ "='0' order by "+DBHelper.VESSEL_INSPECTION_DATE+" DESC";

		try {

			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table by flgStatus and
	 * iShipId.
	 * 
	 * @return list of VesselInspection object by flgStatus
	 * 
	 *         flgStatus 1: for pending , o : for complete
	 */
	public List<VesselInspection> getVesselInspectionDataByflgStatusAndShipId(
			int flgStatus, String shipId) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " =  '" + flgStatus
				+ "' and " + DBHelper.SHIP_ID + "= '" + shipId + "' and "
				+ DBHelper.FLG_DELETED + "='0'";

		try {
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));

					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table by flgStatus and
	 * iShipId.
	 * 
	 * @return list of VesselInspection object by flgStatus
	 * 
	 *         flgStatus 1: for pending , o : for complete
	 */
	public List<VesselInspection> getVesselInspectionDataByflgStatusAndShipIdForSync(
			int flgStatus, String shipId) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FLG_STATUS + " =  '" + flgStatus
				+ "' and " + DBHelper.SHIP_ID + "= '" + shipId + "'"; // and
																		// " + DBHelper.FLG_DELETED + "='0'";

		try {
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));

					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table by flgStatus, date and
	 * iShipId.
	 * 
	 * @return list of VesselInspection object by flgStatus
	 * 
	 *         flgStatus 1: for pending , o : for complete
	 */
	public List<VesselInspection> getVesselInspectionDataByflgStatusDateAndShipId(
			int flgStatus, String shipId, String inspectionDate) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		/*
		 * String whereClause = DBHelper.FLG_STATUS + " =  '" + flgStatus +
		 * "' and " + DBHelper.SHIP_ID + "= '" + shipId + "' and " +
		 * DBHelper.VESSEL_INSPECTION_DATE + "='" + inspectionDate + "' and " +
		 * DBHelper.FLG_DELETED + "='0'";
		 */

		String whereClause = DBHelper.FLG_STATUS + " <  '" + flgStatus
				+ "' and " + DBHelper.SHIP_ID + "= '" + shipId + "' and "
				+ DBHelper.VESSEL_INSPECTION_DATE + "='" + inspectionDate
				+ "' and " + DBHelper.FLG_DELETED + "='0'";

		try {
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));

					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table by Id.
	 * 
	 * @return list of VesselInspection object by id
	 */
	public List<VesselInspection> getVesselInspectionDataById(
			String strVesselInspectionId) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " =  '"
				+ strVesselInspectionId + "'";// and
												// "+DBHelper.FLG_DELETED + "='0'";

		try {
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table.
	 * 
	 * @return list of VesselInspection object
	 */
	public List<VesselInspection> getVesselInspectionData() {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;

		try {

			// String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1'";
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null, null,
					null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	/**
	 * fetch the complete data of VesselInspection table.
	 * 
	 * @return list of VesselInspection object
	 */
	public List<VesselInspection> getVesselInspectionData(String shipId) {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;

		try {

			// String whereClause = DBHelper.SHIP_ID + " =  '1'";
			String whereClause = DBHelper.SHIP_ID + " =  '" + shipId + "'";
			/*
			 * cursor = database.query(DBHelper.VESSEL_INSPECTION, null, null,
			 * null, null, null, null);
			 */
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	public List<VesselInspection> getDirtyVesselInspection() {

		List<VesselInspection> vesselInspectionData = null;
		Cursor cursor = null;

		try {

			String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1'";
			cursor = database.query(DBHelper.VESSEL_INSPECTION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVesselInspectionData");
			if (cursor != null && cursor.moveToFirst()) {
				vesselInspectionData = new ArrayList<VesselInspection>();

				do {
					VesselInspection vesselInspection = new VesselInspection();
					vesselInspection
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					vesselInspection
							.setStrInspectionTitle(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_TITLE)));
					vesselInspection
							.setInspectionDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTION_DATE)));
					vesselInspection.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vesselInspection.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vesselInspection.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vesselInspection.setInspectedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.INSPECTED_BY)));
					vesselInspection.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vesselInspection.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vesselInspection.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vesselInspection.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vesselInspection.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					vesselInspection.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					vesselInspection.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					vesselInspection.setFlgIsLocked(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_LOCKED)));

					vesselInspection
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vesselInspection.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					vesselInspection.setReviewedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REVIEWED_BY)));
					vesselInspection.setApprovedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.APPROVED_BY)));
					vesselInspection.setAcceptedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ACCEPTED_BY)));
					vesselInspection.setCompletedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.COMPLETED_BY)));
					vesselInspection
							.setiVesselInspectionStatusId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_STATUS_ID)));
					vesselInspection.setFlgReverted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_REVERTED)));
					vesselInspection.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));

					vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return vesselInspectionData;
	}

	public String getShipNameById(int shipId) {
		String shipName = "";
		Cursor cursor = null;
		// ArrayList<ShipMaster> shipMasterData;
		ShipMaster shipMaster = null;
		try {
			String whereClause = DBHelper.SHIP_ID + " = " + shipId;
			cursor = database.query(DBHelper.SHIP_MASTER, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getShipNameById");
			if (cursor != null && cursor.moveToFirst()) {
				// shipMasterData = new ArrayList<ShipMaster>();

				do {
					shipMaster = new ShipMaster();

					shipMaster.setStrShipName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_NAME)));

					shipMaster.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					shipMaster.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					// vesselInspectionData.add(vesselInspection);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}

			if (shipMaster != null && shipMaster.getStrShipName() != null
					&& !"".equals(shipMaster.getStrShipName()))
				shipName = shipMaster.getStrShipName();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return shipName;
	}

	/**
	 * This method will insert single record in ShipMaster table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertShipMasterTable(ShipMaster shipMaster) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SHIP_ID, shipMaster.getiShipId());
			contentValues.put(DBHelper.SHIP_NAME, shipMaster.getStrShipName());
			contentValues.put(DBHelper.CREATED_DATE, shipMaster.getDtCreated());
			contentValues.put(DBHelper.FLG_DELETED, shipMaster.getFlgDeleted());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, shipMaster.getFlgIsDirty());
			contentValues
					.put(DBHelper.STR_DESC, shipMaster.getStrDescription());
			contentValues.put(DBHelper.TENANT_ID, shipMaster.getiTenantId());

			result = database.insert(DBHelper.SHIP_MASTER, null, contentValues);
			Log.i("SHIP_MASTER value :== ",
					"inside insert of shipMaster table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in ShipMaster table and return the
	 * value whether the record is updated successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateShipMasterTable(ShipMaster shipMaster) {

		long result = 0;
		String whereClause = DBHelper.SHIP_ID + " = '"
				+ shipMaster.getiShipId() + "'";
		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SHIP_ID, shipMaster.getiShipId());
			contentValues.put(DBHelper.SHIP_NAME, shipMaster.getStrShipName());
			contentValues.put(DBHelper.CREATED_DATE, shipMaster.getDtCreated());
			contentValues.put(DBHelper.FLG_DELETED, shipMaster.getFlgDeleted());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, shipMaster.getFlgIsDirty());
			contentValues
					.put(DBHelper.STR_DESC, shipMaster.getStrDescription());
			contentValues.put(DBHelper.TENANT_ID, shipMaster.getiTenantId());

			result = database.update(DBHelper.SHIP_MASTER, contentValues,
					whereClause, null);
			Log.i("SHIP_MASTER value :== ",
					"inside update of shipMaster table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of ShipMaster table by Id.
	 * 
	 * @return list of ShipMaster object by id
	 */
	public List<ShipMaster> getShipMasterDataById(String strShipMasterId) {

		List<ShipMaster> shipMasterData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.SHIP_ID + " =  '" + strShipMasterId + "'";

		try {
			cursor = database.query(DBHelper.SHIP_MASTER, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getShipMasterData");
			if (cursor != null && cursor.moveToFirst()) {
				shipMasterData = new ArrayList<ShipMaster>();

				do {
					ShipMaster shipMaster = new ShipMaster();
					shipMaster.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					shipMaster.setStrShipName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_NAME)));
					shipMaster.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					shipMaster.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					shipMaster.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					shipMaster.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					shipMaster.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					shipMasterData.add(shipMaster);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return shipMasterData;
	}

	/**
	 * fetch the complete data of ShipMaster table.
	 * 
	 * @return list of ShipMaster object
	 */
	public List<ShipMaster> getShipMasterData() {

		List<ShipMaster> shipMasterData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.SHIP_MASTER, null, null, null,
					null, null, null);
			Log.i("dbmanager", "inside getShipMasterData");
			if (cursor != null && cursor.moveToFirst()) {
				shipMasterData = new ArrayList<ShipMaster>();

				do {
					ShipMaster shipMaster = new ShipMaster();
					shipMaster.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					shipMaster.setStrShipName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_NAME)));
					shipMaster.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					shipMaster.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					shipMaster.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					shipMaster.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					shipMaster.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					shipMasterData.add(shipMaster);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return shipMasterData;
	}

	/**
	 * This method will insert single record in Tenant table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertTenantTable(Tenant tenant) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.TENANT_ID, tenant.getiTenantId());
			contentValues.put(DBHelper.TENANT_NAME, tenant.getStrTenantName());
			/*
			 * contentValues.put(DBHelper.CREATED_DATE,
			 * tenant.getCreatedDate()); contentValues.put(DBHelper.FLG_DELETED,
			 * tenant.getFlgDeleted()); contentValues.put(DBHelper.FLG_IS_DIRTY,
			 * tenant.getFlgIsDirty()); contentValues.put(DBHelper.STR_DESC,
			 * tenant.getStrDesc());
			 */

			result = database.insert(DBHelper.TENANT, null, contentValues);
			Log.i("TENANT value :== ", "inside insert of tenant table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Tenant table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateTenantTable(Tenant tenant) {

		long result = 0;
		String whereClause = DBHelper.TENANT_ID + " = '"
				+ tenant.getiTenantId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.TENANT_ID, tenant.getiTenantId());
			contentValues.put(DBHelper.TENANT_NAME, tenant.getStrTenantName());
			/*
			 * contentValues.put(DBHelper.CREATED_DATE,
			 * tenant.getCreatedDate()); contentValues.put(DBHelper.FLG_DELETED,
			 * tenant.getFlgDeleted()); contentValues.put(DBHelper.FLG_IS_DIRTY,
			 * tenant.getFlgIsDirty()); contentValues.put(DBHelper.STR_DESC,
			 * tenant.getStrDesc());
			 */

			result = database.update(DBHelper.TENANT, contentValues,
					whereClause, null);
			Log.i("TENANT value :== ", "inside insert of tenant table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of Tenant table by Id.
	 * 
	 * @return list of Tenant object by id
	 */
	public List<Tenant> getTenantDataById(String strTenantId) {

		List<Tenant> tenantData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.TENANT_ID + " =  '" + strTenantId + "'";

		try {
			cursor = database.query(DBHelper.TENANT_ID, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getTenantData");
			if (cursor != null && cursor.moveToFirst()) {
				tenantData = new ArrayList<Tenant>();

				do {
					Tenant tenant = new Tenant();
					tenant.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					tenant.setStrTenantName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_NAME)));
					/*
					 * tenant.setCreatedDate(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					 * tenant.setFlgDeleted(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					 * tenant.setFlgIsDirty(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					 * tenant.setStrDesc(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.STR_DESC)));
					 */

					tenantData.add(tenant);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getTenantData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return tenantData;
	}

	/**
	 * fetch the complete data of Tenant table.
	 * 
	 * @return list of Tenant object
	 */
	public List<Tenant> getTenantData() {

		List<Tenant> tenantData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TENANT_ID, null, null, null, null,
					null, null);
			Log.i("dbmanager", "inside getTenantData");
			if (cursor != null && cursor.moveToFirst()) {
				tenantData = new ArrayList<Tenant>();

				do {
					Tenant tenant = new Tenant();
					tenant.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					tenant.setStrTenantName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_NAME)));
					/*
					 * tenant.setCreatedDate(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					 * tenant.setFlgDeleted(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					 * tenant.setFlgIsDirty(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					 * tenant.setStrDesc(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.STR_DESC)));
					 */

					tenantData.add(tenant);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside gettagData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return tenantData;
	}

	/**
	 * This method will insert single record in Role table and return the value
	 * whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertRoleTable(Role role) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.ROLE_ID, role.getRoleId());
			contentValues.put(DBHelper.ROLE_NAME, role.getRoleName());
			contentValues.put(DBHelper.CREATED_DATE, role.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE, role.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, role.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, role.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, role.getIsDeleted());
			contentValues.put(DBHelper.FLG_STATUS, role.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY, role.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, role.getiTenantId());

			result = database.insert(DBHelper.TABLE_ROLE, null, contentValues);
			Log.i("TABLE_ROLE value :== ", "inside insert of role table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Role table and return the value
	 * whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateRoleTable(Role role) {

		long result = 0;
		String whereClause = DBHelper.ROLE_ID + " = '" + role.getRoleId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.ROLE_ID, role.getRoleId());
			contentValues.put(DBHelper.ROLE_NAME, role.getRoleName());
			contentValues.put(DBHelper.CREATED_DATE, role.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE, role.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, role.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, role.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, role.getIsDeleted());
			contentValues.put(DBHelper.FLG_STATUS, role.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY, role.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, role.getiTenantId());

			result = database.update(DBHelper.TABLE_ROLE, contentValues,
					whereClause, null);
			Log.i("TABLE_ROLE value :== ", "inside update of role table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of Role table by Id.
	 * 
	 * @return list of Role object by id
	 */
	public List<Role> getRoleDataById(String strRoleId) {

		List<Role> roleData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.ROLE_ID + " =  '" + strRoleId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_ROLE, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getRoleData");
			if (cursor != null && cursor.moveToFirst()) {
				roleData = new ArrayList<Role>();

				do {
					Role role = new Role();
					role.setRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_ID)));
					role.setRoleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_NAME)));
					role.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					role.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					role.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					role.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					role.setIsDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					role.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					role.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					role.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					roleData.add(role);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getRoleData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return roleData;
	}

	/**
	 * fetch the complete data of Role table.
	 * 
	 * @return list of Role object
	 */
	public List<Role> getRoleData() {

		List<Role> roleData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_ROLE, null, null, null,
					null, null, null);
			Log.i("dbmanager", "inside getRoleData");
			if (cursor != null && cursor.moveToFirst()) {
				roleData = new ArrayList<Role>();

				do {
					Role role = new Role();
					role.setRoleId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_ID)));
					role.setRoleName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_NAME)));
					role.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					role.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					role.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					role.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					role.setIsDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					role.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					role.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					role.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					roleData.add(role);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getRoleData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return roleData;
	}

	/**
	 * This method will insert single record in Role table and return the value
	 * whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertUserServiceTermTable(UserServiceTerm ust) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.USER_SERVICETERM_ID,
					ust.getIserServiceTermId());
			contentValues.put(DBHelper.KEY_iUSER_ID, ust.getUserId());
			contentValues.put(DBHelper.START_DATE, ust.getStartDate());
			contentValues.put(DBHelper.END_DATE, ust.getEndDate());
			contentValues.put(DBHelper.CREATED_DATE, ust.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE, ust.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, ust.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, ust.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, ust.getIsDeleted());
			contentValues.put(DBHelper.FLG_STATUS, ust.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY, ust.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, ust.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, ust.getiShipId());

			result = database.insert(DBHelper.TABLE_USER_SERVICETERM, null,
					contentValues);
			Log.i("TABLE_ROLE value :== ",
					"inside insert of UserServiceTerm table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Role table and return the value
	 * whether the record is updated successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateUserServiceTermTable(UserServiceTerm ust) {

		long result = 0;
		String whereClause = DBHelper.USER_SERVICETERM_ID + " = '"
				+ ust.getIserServiceTermId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.USER_SERVICETERM_ID,
					ust.getIserServiceTermId());
			contentValues.put(DBHelper.KEY_iUSER_ID, ust.getUserId());
			contentValues.put(DBHelper.START_DATE, ust.getStartDate());
			contentValues.put(DBHelper.END_DATE, ust.getEndDate());
			contentValues.put(DBHelper.CREATED_DATE, ust.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE, ust.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, ust.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, ust.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, ust.getIsDeleted());
			contentValues.put(DBHelper.FLG_STATUS, ust.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY, ust.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, ust.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, ust.getiShipId());

			result = database.update(DBHelper.TABLE_USER_SERVICETERM,
					contentValues, whereClause, null);
			Log.i("TABLE_ROLE value :== ",
					"inside update of UserServiceTerm table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of Role table by Id.
	 * 
	 * @return list of Role object by id
	 */
	public List<UserServiceTerm> getUserServiceTermDataById(
			String strUserServiceTermId) {

		List<UserServiceTerm> userServiceTermData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.USER_SERVICETERM_ID + " =  '"
				+ strUserServiceTermId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_USER_SERVICETERM, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getUserServiceTermData");
			if (cursor != null && cursor.moveToFirst()) {
				userServiceTermData = new ArrayList<UserServiceTerm>();

				do {
					UserServiceTerm userServiceTerm = new UserServiceTerm();
					userServiceTerm
							.setIserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.USER_SERVICETERM_ID)));
					userServiceTerm.setStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.START_DATE)));
					userServiceTerm.setEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.END_DATE)));
					userServiceTerm.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					userServiceTerm.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					userServiceTerm.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					userServiceTerm.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					userServiceTerm.setIsDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					userServiceTerm.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					userServiceTerm.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					userServiceTerm.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					userServiceTerm.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					userServiceTermData.add(userServiceTerm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserServiceTermData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return userServiceTermData;
	}

	/**
	 * fetch the complete data of Role table.
	 * 
	 * @return list of Role object
	 */
	public List<UserServiceTerm> getUserServiceTermData() {

		List<UserServiceTerm> userServiceTermData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_USER_SERVICETERM, null,
					null, null, null, null, null);
			Log.i("dbmanager", "inside getUserServiceTermData");
			if (cursor != null && cursor.moveToFirst()) {
				userServiceTermData = new ArrayList<UserServiceTerm>();

				do {
					UserServiceTerm userServiceTerm = new UserServiceTerm();
					userServiceTerm
							.setIserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.USER_SERVICETERM_ID)));
					userServiceTerm.setStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.START_DATE)));
					userServiceTerm.setEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.END_DATE)));
					userServiceTerm.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					userServiceTerm.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					userServiceTerm.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					userServiceTerm.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					userServiceTerm.setIsDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					userServiceTerm.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					userServiceTerm.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					userServiceTerm.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					userServiceTerm.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					userServiceTermData.add(userServiceTerm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getuserServiceTermData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return userServiceTermData;
	}

	/**
	 * This method will insert single record in FILLED_fORM table and return the
	 * value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFilldFormTable(FilledForm filledForm) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_ID,
					filledForm.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					filledForm.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					filledForm.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledForm.getiVesselInspectionId());
			contentValues.put(DBHelper.FORM_HEADER_DESC,
					filledForm.getStrHeaderDesc());
			contentValues.put(DBHelper.FORM_FOOTER_DESC,
					filledForm.getStrFooterDesc());
			contentValues.put(DBHelper.CREATED_DATE,
					filledForm.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledForm.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, filledForm.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, filledForm.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, filledForm.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, filledForm.getFlgStatus());
			contentValues.put(DBHelper.FLG_CHECKED, filledForm.getFlgChecked());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, filledForm.getFlgIsDirty());

			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					filledForm.getSequence());
			contentValues.put(DBHelper.TENANT_ID, filledForm.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledForm.getiShipId());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					filledForm.getFlgIsEdited());
			contentValues.put(DBHelper.FLG_IS_HEADER_EDITED,
					filledForm.getFlgIsHeaderEdited());
			contentValues.put(DBHelper.FLG_IS_FOOTER_EDITED,
					filledForm.getFlgIsFooterEdited());

			result = database.insert(DBHelper.FILLED_FORM, null, contentValues);
			Log.i("FILLED_FORM value :== ",
					"inside insert of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in FILLED_fORM table and return the
	 * value whether the record is updated successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFilldFormTable(FilledForm filledForm) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_FORM_ID + " = '"
				+ filledForm.getiFilledFormId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_ID,
					filledForm.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					filledForm.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					filledForm.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledForm.getiVesselInspectionId());
			contentValues.put(DBHelper.FORM_HEADER_DESC,
					filledForm.getStrHeaderDesc());
			contentValues.put(DBHelper.FORM_FOOTER_DESC,
					filledForm.getStrFooterDesc());
			contentValues.put(DBHelper.CREATED_DATE,
					filledForm.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledForm.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, filledForm.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, filledForm.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, filledForm.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, filledForm.getFlgStatus());
			contentValues.put(DBHelper.FLG_CHECKED, filledForm.getFlgChecked());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, filledForm.getFlgIsDirty());

			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					filledForm.getSequence());
			contentValues.put(DBHelper.TENANT_ID, filledForm.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledForm.getiShipId());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					filledForm.getFlgIsEdited());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					filledForm.getFlgIsEdited());
			contentValues.put(DBHelper.FLG_IS_HEADER_EDITED,
					filledForm.getFlgIsHeaderEdited());
			contentValues.put(DBHelper.FLG_IS_FOOTER_EDITED,
					filledForm.getFlgIsFooterEdited());

			result = database.update(DBHelper.FILLED_FORM, contentValues,
					whereClause, null);
			Log.i("FILLED_FORM value :== ",
					"inside insert of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update dirty flag single record in FILLED_fORM table and
	 * return the value whether the record is updated successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateDirtyFlagFilldFormTable(String strFilledFormId) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_FORM_ID + " = '" + strFilledFormId
				+ "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_DIRTY, 0);

			result = database.update(DBHelper.FILLED_FORM, contentValues,
					whereClause, null);
			Log.i("FILLED_FORM value :== ",
					"inside insert of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public long updateDirtyFlagFormListDescTable(String strFormListDescId,
			int flgIsDirty) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListDesc_ID + " = '"
				+ strFormListDescId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_DIRTY, flgIsDirty);

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListDesc, contentValues,
					whereClause, null);

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception while updating flgIsDirty in VesselINspectionFormListDesc",
					exceptionString);
		}

		return result;
	}

	public long updateDirtyFlagFormListFooterTable(String strFormListFooterId,
			int flgIsDirty) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListFooter_ID
				+ " = '" + strFormListFooterId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_DIRTY, flgIsDirty);

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListFooter,
					contentValues, whereClause, null);

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception while updating flgIsDirty in VeselInspectionFormListFooter",
					exceptionString);
		}

		return result;
	}

	public long updateFormListHeaderEditFlag(String strFormListDescId,
			int flgIsEdited) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListDesc_ID + " = '"
				+ strFormListDescId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_EDITED, flgIsEdited);

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListDesc, contentValues,
					whereClause, null);

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public long updateFormListFooterEditFlag(String strFormListFooterId,
			int flgIsEdited) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListFooter_ID
				+ " = '" + strFormListFooterId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_EDITED, flgIsEdited);

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListFooter,
					contentValues, whereClause, null);

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public long updateFilldFormEditFlag(String strFilledFormId, int flgIsEdited) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_FORM_ID + " = '" + strFilledFormId
				+ "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.FLG_IS_EDITED, flgIsEdited);

			result = database.update(DBHelper.FILLED_FORM, contentValues,
					whereClause, null);
			Log.i("FILLED_FORM value :== ",
					"inside insert of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of FilledForm table by ID.
	 * 
	 * @return list of FilledForm object
	 */
	public List<FilledForm> getFilledFormDataById(String strFilledFormId) {

		List<FilledForm> filledFormData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FILLED_FORM_ID + " =  '"
				+ strFilledFormId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormData = new ArrayList<FilledForm>();

				do {
					FilledForm filledForm = new FilledForm();

					filledForm.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledForm.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledForm
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					filledForm
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledForm.setStrHeaderDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_HEADER_DESC)));
					filledForm.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));

					filledForm.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledForm.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledForm.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledForm.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledForm.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledForm.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledForm.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					filledForm.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledForm.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));

					filledForm.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledForm.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					filledForm
							.setFlgIsHeaderEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_HEADER_EDITED)));
					filledForm
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));

					filledFormData.add(filledForm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormData;
	}

	/**
	 * fetch the all ids of FilledForm table by inspection ID.
	 * 
	 * @return list of FilledForm object
	 */
	public List<FilledForm> getFilledFormDataByInspectionId(String strVslInspId) {

		List<FilledForm> filledFormData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " =  '"
				+ strVslInspId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormData = new ArrayList<FilledForm>();

				do {
					FilledForm filledForm = new FilledForm();

					filledForm.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));

					filledFormData.add(filledForm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormData;
	}

	/**
	 * fetch the complete data of FilledForm table by IDS.
	 * 
	 * @return list of FilledForm object
	 */
	public List<FilledForm> getFilledFormDataByIds(
			String strVesselInspectionId, String strFormSectionId,
			String strFormsectionItemId) {

		List<FilledForm> filledFormData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " =  '"
				+ strVesselInspectionId + "' and " + DBHelper.FORM_SECTION_ID
				+ " ='" + strFormSectionId + "' and "
				+ DBHelper.FORM_SECTION_ITEM_ID + " = '" + strFormsectionItemId
				+ "'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormData = new ArrayList<FilledForm>();

				do {
					FilledForm filledForm = new FilledForm();

					filledForm.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledForm.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledForm
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					filledForm
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledForm.setStrHeaderDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_HEADER_DESC)));
					filledForm.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));

					filledForm.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledForm.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledForm.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledForm.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledForm.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledForm.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledForm.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					filledForm
							.setFlgIsHeaderEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_HEADER_EDITED)));
					filledForm
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					filledForm.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledForm.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					filledForm.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledForm.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormData.add(filledForm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormData;
	}

	/**
	 * fetch the complete data of FilledForm table.
	 * 
	 * @return list of FilledForm object
	 */
	public List<FilledForm> getFilledFormData() {

		List<FilledForm> filledFormData = null;
		Cursor cursor = null;

		try {

			String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1'";
			cursor = database.query(DBHelper.FILLED_FORM, null, whereClause,
					null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormData = new ArrayList<FilledForm>();

				do {
					FilledForm filledForm = new FilledForm();

					filledForm.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledForm.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledForm
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					filledForm
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					filledForm.setStrHeaderDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_HEADER_DESC)));
					filledForm.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));

					filledForm.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledForm.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledForm.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledForm.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledForm.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledForm.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledForm.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					filledForm
							.setFlgIsHeaderEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_HEADER_EDITED)));
					filledForm
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					filledForm.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					filledForm.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					filledForm.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledForm.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormData.add(filledForm);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormData;
	}

	/**
	 * This method will insert single record in FilledFormImages table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertFilldFormImageTable(FilledFormImages filledFormImages) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_IMAGE_ID,
					filledFormImages.getStrFilledFormImageId());
			contentValues.put(DBHelper.FILLED_FORM_ID,
					filledFormImages.getiFilledFormId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledFormImages.getiVesselInspectionId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					filledFormImages.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					filledFormImages.getiFormSectionItemId());
			contentValues.put(DBHelper.IMAGE_PATH,
					filledFormImages.getStrImagepath());
			contentValues.put(DBHelper.IMAGE_RELATIVE_PATH,
					filledFormImages.getStrRelativeImagePath());
			contentValues.put(DBHelper.IMAGE_NAME,
					filledFormImages.getStrImageName());
			contentValues.put(DBHelper.CREATED_DATE,
					filledFormImages.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledFormImages.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					filledFormImages.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					filledFormImages.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					filledFormImages.getFlgDeleted());
			contentValues.put(DBHelper.FLG_LARGE,
					filledFormImages.getFlgStatus());
			contentValues.put(DBHelper.STR_DESC, filledFormImages.getStrDesc());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					filledFormImages.getFlgIsDirty());

			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					filledFormImages.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					filledFormImages.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledFormImages.getiShipId());

			contentValues.put(DBHelper.DEVICE_ORG_PATH,
					filledFormImages.getDeviceOriginalImagePath());
			contentValues.put(DBHelper.DEVICE_REL_PATH,
					filledFormImages.getDeviceRelativeImagePath());

			result = database.insert(DBHelper.FILLED_FORM_IMAGE, null,
					contentValues);
			Log.i("FILLED_FORM value :== ",
					"inside insert of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in FilledFormImages table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFilldFormImageTable(FilledFormImages filledFormImages) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.FILLED_FORM_IMAGE_ID + " = '"
				+ filledFormImages.getStrFilledFormImageId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_IMAGE_ID,
					filledFormImages.getStrFilledFormImageId());
			contentValues.put(DBHelper.FILLED_FORM_ID,
					filledFormImages.getiFilledFormId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					filledFormImages.getiVesselInspectionId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					filledFormImages.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					filledFormImages.getiFormSectionItemId());
			contentValues.put(DBHelper.IMAGE_PATH,
					filledFormImages.getStrImagepath());
			contentValues.put(DBHelper.IMAGE_RELATIVE_PATH,
					filledFormImages.getStrRelativeImagePath());
			contentValues.put(DBHelper.IMAGE_NAME,
					filledFormImages.getStrImageName());
			contentValues.put(DBHelper.CREATED_DATE,
					filledFormImages.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					filledFormImages.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY,
					filledFormImages.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					filledFormImages.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					filledFormImages.getFlgDeleted());
			contentValues.put(DBHelper.FLG_LARGE,
					filledFormImages.getFlgStatus());
			contentValues.put(DBHelper.STR_DESC, filledFormImages.getStrDesc());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					filledFormImages.getFlgIsDirty());

			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					filledFormImages.getSequence());
			contentValues.put(DBHelper.TENANT_ID,
					filledFormImages.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, filledFormImages.getiShipId());

			contentValues.put(DBHelper.DEVICE_ORG_PATH,
					filledFormImages.getDeviceOriginalImagePath());
			contentValues.put(DBHelper.DEVICE_REL_PATH,
					filledFormImages.getDeviceRelativeImagePath());

			result = database.update(DBHelper.FILLED_FORM_IMAGE, contentValues,
					whereClause, null);
			Log.i("FILLED_FORM_IMAGE value :== ",
					"inside update of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public void updateFilldFormImageForWeb(FilledFormImages img) {
		String query = "update " + DBHelper.FILLED_FORM_IMAGE + "  set "
				+ DBHelper.FLG_DELETED + "=" + img.getFlgDeleted() + " , " + ""
				+ DBHelper.FLG_IS_DIRTY + "=" + img.getFlgIsDirty() + " ,"
				+ DBHelper.STR_DESC + "='" + img.getStrDesc() + "' " + "where "
				+ DBHelper.FILLED_FORM_IMAGE_ID + "='"
				+ img.getStrFilledFormImageId() + "' ";

		database.execSQL(query);

	}

	/**
	 * This method will update single record in FilledFormImages table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateFilldFormImageTableFlag(Context mContext,
			String strImgPath) {
		// TODO Auto-generated method stub
		long result = 0;
		// String whereClause = DBHelper.IMAGE_PATH + " = '" + strImgPath + "'";
		// String whereClause = DBHelper.IMAGE_RELATIVE_PATH + " = '" +
		// strImgPath + "'";
		String whereClause = DBHelper.DEVICE_REL_PATH + " = '" + strImgPath
				+ "'";

		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			ContentValues contentValues = new ContentValues();

			contentValues.put(DBHelper.MODIFIED_DATE, df.format(new Date()));
			contentValues.put(DBHelper.MODIFIED_BY,
					CommonUtil.getUserId(mContext));
			contentValues.put(DBHelper.FLG_DELETED, 1);
			contentValues.put(DBHelper.FLG_IS_DIRTY, 1);

			result = database.update(DBHelper.FILLED_FORM_IMAGE, contentValues,
					whereClause, null);
			Log.i("FILLED_FORM_IMAGE value :== ",
					"inside update of FILLED_FORM table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * fetch the complete data of FilledFormImages table by strimagepath.
	 * 
	 * @return list of FilledFormImages object
	 */
	public List<FilledFormImages> getFilledFormImagesByImagePath(
			String strFilledFormImagePath) {

		List<FilledFormImages> filledFormImagesData = null;
		Cursor cursor = null;
		/*
		 * String whereClause = DBHelper.IMAGE_PATH + " =  '" +
		 * strFilledFormImagePath + "' and " + DBHelper.FLG_DELETED + "='0'";
		 */
		/*
		 * String whereClause = DBHelper.IMAGE_PATH + " =  '" +
		 * strFilledFormImagePath + "'";
		 */
		/*
		 * String whereClause = DBHelper.IMAGE_RELATIVE_PATH + " =  '" +
		 * strFilledFormImagePath + "'";
		 */
		String whereClause = DBHelper.DEVICE_REL_PATH + " =  '"
				+ strFilledFormImagePath + "'";
		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormImageData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormImagesData = new ArrayList<FilledFormImages>();

				do {
					FilledFormImages filledFormImages = new FilledFormImages();

					filledFormImages
							.setStrFilledFormImageId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_FORM_IMAGE_ID)));
					filledFormImages.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledFormImages.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledFormImages
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));

					filledFormImages.setStrImagepath(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_PATH)));
					filledFormImages
							.setStrRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.IMAGE_RELATIVE_PATH)));

					filledFormImages.setStrImageName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_NAME)));
					filledFormImages
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));

					filledFormImages.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledFormImages.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledFormImages.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledFormImages.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledFormImages.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledFormImages.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledFormImages.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_LARGE)));
					filledFormImages.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					filledFormImages.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					filledFormImages.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledFormImages.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormImages
							.setDeviceOriginalImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_ORG_PATH)));
					filledFormImages
							.setDeviceRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)));

					filledFormImagesData.add(filledFormImages);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormImagesDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormImagesData;
	}

	/**
	 * fetch the complete data of FilledFormImages table by ID.
	 * 
	 * @return list of FilledFormImages object
	 */
	public List<FilledFormImages> getFilledFormImagesById(
			String strFilledFormImagesId) {

		List<FilledFormImages> filledFormImagesData = null;
		Cursor cursor = null;
		String whereClause = DBHelper.FILLED_FORM_IMAGE_ID + " =  '"
				+ strFilledFormImagesId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormImageData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormImagesData = new ArrayList<FilledFormImages>();

				do {
					FilledFormImages filledFormImages = new FilledFormImages();

					filledFormImages
							.setStrFilledFormImageId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_FORM_IMAGE_ID)));
					filledFormImages.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledFormImages.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledFormImages
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));

					filledFormImages.setStrImagepath(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_PATH)));
					filledFormImages
							.setStrRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.IMAGE_RELATIVE_PATH)));

					filledFormImages.setStrImageName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_NAME)));
					filledFormImages
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));

					filledFormImages.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledFormImages.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledFormImages.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledFormImages.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledFormImages.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledFormImages.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledFormImages.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_LARGE)));
					filledFormImages.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					filledFormImages.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					filledFormImages.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledFormImages.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormImages
							.setDeviceOriginalImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_ORG_PATH)));
					filledFormImages
							.setDeviceRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)));

					filledFormImagesData.add(filledFormImages);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormImagesDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormImagesData;
	}
	
	/**
	 * fetch the dirty count of FilledFormImages.
	 * 
	 * @return list of FilledFormImages object
	 */
	public long getFilledFormImagesDirtyCount(String activeVslInsp) {

		List<FilledFormImages> filledFormImagesData = null;
		Cursor cursor = null;
		long dirtyCount=0;
		String whereClause = DBHelper.FLG_IS_DIRTY +"='1' and "+DBHelper.FLG_DELETED+"='0' and " + DBHelper.VESSEL_INSPECTIONS_ID + " in ("+ activeVslInsp + ")";

		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormImageData");
			if (cursor != null && cursor.moveToFirst()) {
				dirtyCount = cursor.getCount();
			} else {
				Log.i("dbmanager", "inside filledFormImagesDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return dirtyCount;
	}

	/**
	 * fetch the complete data of FilledFormImages table.
	 * 
	 * @return list of FilledFormImages object
	 */
	public List<FilledFormImages> getFilledFormImagesData() {

		List<FilledFormImages> filledFormImagesData = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null, null,
					null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormImageData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormImagesData = new ArrayList<FilledFormImages>();

				do {
					FilledFormImages filledFormImages = new FilledFormImages();

					filledFormImages
							.setStrFilledFormImageId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_FORM_IMAGE_ID)));
					filledFormImages.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledFormImages.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledFormImages
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));

					filledFormImages.setStrImagepath(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_PATH)));
					filledFormImages
							.setStrRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.IMAGE_RELATIVE_PATH)));

					filledFormImages.setStrImageName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_NAME)));
					filledFormImages
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));

					filledFormImages.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledFormImages.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledFormImages.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledFormImages.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledFormImages.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledFormImages.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledFormImages.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_LARGE)));
					filledFormImages.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					filledFormImages.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					filledFormImages.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledFormImages.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormImages
							.setDeviceOriginalImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_ORG_PATH)));
					filledFormImages
							.setDeviceRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)));

					filledFormImagesData.add(filledFormImages);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormImagesDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormImagesData;
	}

	/**
	 * fetch the complete data of FilledFormImages table.
	 * 
	 * @return list of FilledFormImages object
	 */
	public List<FilledFormImages> getDirtyFilledFormImagesData() {

		List<FilledFormImages> filledFormImagesData = null;
		Cursor cursor = null;

		String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getFilledFormImageData");
			if (cursor != null && cursor.moveToFirst()) {
				filledFormImagesData = new ArrayList<FilledFormImages>();

				do {
					FilledFormImages filledFormImages = new FilledFormImages();

					filledFormImages
							.setStrFilledFormImageId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FILLED_FORM_IMAGE_ID)));
					filledFormImages.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					filledFormImages.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					filledFormImages
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));

					filledFormImages.setStrImagepath(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_PATH)));
					filledFormImages
							.setStrRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.IMAGE_RELATIVE_PATH)));

					filledFormImages.setStrImageName(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.IMAGE_NAME)));
					filledFormImages
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));

					filledFormImages.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					filledFormImages.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					filledFormImages.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					filledFormImages.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					filledFormImages.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					filledFormImages.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					filledFormImages.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_LARGE)));
					filledFormImages.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					filledFormImages.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					filledFormImages.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					filledFormImages.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));

					filledFormImages
							.setDeviceOriginalImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_ORG_PATH)));
					filledFormImages
							.setDeviceRelativeImagePath(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.DEVICE_REL_PATH)));

					filledFormImagesData.add(filledFormImages);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside filledFormImagesDataData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return filledFormImagesData;
	}

	public int getVesselInspectionImageSequence(String strInspectionId,
			String strSctionItemId) {

		Cursor cursor = null;
		int count = 0;
		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " =  '"
				+ strInspectionId + "' and " + DBHelper.FORM_SECTION_ITEM_ID
				+ " = '" + strSctionItemId + "'";

		try {
			cursor = database.query(DBHelper.FILLED_FORM_IMAGE, null,
					whereClause, null, null, null, null);

			count = cursor.getCount();

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return count;
	}

	/**
	 * @author ripunjay.s This method will insert single record in syns_status
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertSyncStatusTable(SyncStatus syncStatus) {
		// TODO Auto-generated method stub
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SYNC_STATUS_ID,
					syncStatus.getiSyncStatusId());
			contentValues.put(DBHelper.SYNC_DATE, syncStatus.getDtSyncDate());
			contentValues.put(DBHelper.SYNC_MODE, syncStatus.getSyncMode());
			contentValues.put(DBHelper.DATA_SYNC_MODE,
					syncStatus.getDataSyncMode());
			contentValues.put(DBHelper.SERVER_ADDRESS,
					syncStatus.getServerAddress());
			contentValues.put(DBHelper.CREATED_DATE,
					syncStatus.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					syncStatus.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, syncStatus.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, syncStatus.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, syncStatus.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, syncStatus.getFlgStatus());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, syncStatus.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, syncStatus.getiTenantId());

			result = database.insert(DBHelper.TABLE_SYNC_STATUS, null,
					contentValues);
			Log.i("SYNC_STATUS value :== ",
					"inside insert of sync_status table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author ripunjay.s This method will update single record in syns_status
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateSyncStatusTable(SyncStatus syncStatus) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.SYNC_STATUS_ID + " = '"
				+ syncStatus.getiSyncStatusId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.SYNC_STATUS_ID,
					syncStatus.getiSyncStatusId());
			contentValues.put(DBHelper.SYNC_DATE, syncStatus.getDtSyncDate());
			contentValues.put(DBHelper.SYNC_MODE, syncStatus.getSyncMode());
			contentValues.put(DBHelper.DATA_SYNC_MODE,
					syncStatus.getDataSyncMode());
			contentValues.put(DBHelper.SERVER_ADDRESS,
					syncStatus.getServerAddress());
			contentValues.put(DBHelper.CREATED_DATE,
					syncStatus.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					syncStatus.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, syncStatus.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, syncStatus.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, syncStatus.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, syncStatus.getFlgStatus());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, syncStatus.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, syncStatus.getiTenantId());

			result = database.update(DBHelper.TABLE_SYNC_STATUS, contentValues,
					whereClause, null);
			Log.i("SYNC_STATUS value :== ",
					"inside update of sync_status table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author ripunjay.s fetch the single row data of sync_status table By
	 *         syncStatusid.
	 * @return list of SyncStatus object
	 */
	public List<SyncStatus> getSyncStatusDataById(String strSyncStatusId) {

		List<SyncStatus> syncStatusData = new ArrayList<SyncStatus>();
		Cursor cursor = null;

		String whereClause = DBHelper.SYNC_STATUS_ID + " =  '"
				+ strSyncStatusId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_SYNC_STATUS, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getSyncStatusDataById");
			if (cursor != null && cursor.moveToFirst()) {
				syncStatusData = new ArrayList<SyncStatus>();

				do {
					SyncStatus syncStatus = new SyncStatus();
					syncStatus.setiSyncStatusId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_STATUS_ID)));
					syncStatus.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_DATE)));
					syncStatus.setSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_MODE)));
					syncStatus.setDataSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.DATA_SYNC_MODE)));
					syncStatus.setServerAddress(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SERVER_ADDRESS)));
					syncStatus.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					syncStatus.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					syncStatus.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					syncStatus.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					syncStatus.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					syncStatus.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					syncStatus.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					syncStatus.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					syncStatusData.add(syncStatus);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return syncStatusData;
	}

	/**
	 * @author ripunjay.s fetch the all row data of sync_status table.
	 * @return list of SyncStatus object
	 */
	public List<SyncStatus> getSyncStatusData() {

		List<SyncStatus> syncStatusData = new ArrayList<SyncStatus>();
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_SYNC_STATUS, null, null,
					null, null, null, null);
			Log.i("dbmanager", "inside getSyncStatusData");
			if (cursor != null && cursor.moveToFirst()) {
				syncStatusData = new ArrayList<SyncStatus>();

				do {
					SyncStatus syncStatus = new SyncStatus();
					syncStatus.setiSyncStatusId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_STATUS_ID)));
					syncStatus.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_DATE)));
					syncStatus.setSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_MODE)));
					syncStatus.setDataSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.DATA_SYNC_MODE)));
					syncStatus.setServerAddress(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SERVER_ADDRESS)));
					syncStatus.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					syncStatus.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					syncStatus.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					syncStatus.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					syncStatus.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					syncStatus.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					syncStatus.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					syncStatus.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					syncStatusData.add(syncStatus);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataData");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return syncStatusData;
	}

	/**
	 * @author ripunjay.s This method will insert single record in VIAlertTime
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertVIAlertTimeTable(VIAlertTime viAlertTime) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_VI_ALERT_TIME_ID,
					viAlertTime.getiVIAlertTimeId());
			contentValues.put(DBHelper.VI_ALERT_TIME,
					viAlertTime.getAlertTime());
			contentValues.put(DBHelper.MAC_ID, viAlertTime.getStrMacId());
			contentValues
					.put(DBHelper.CREATED_DATE, viAlertTime.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viAlertTime.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY, viAlertTime.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, viAlertTime.getUpdatedBy());
			contentValues
					.put(DBHelper.FLG_DELETED, viAlertTime.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, viAlertTime.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					viAlertTime.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viAlertTime.getiTenantId());

			result = database.insert(DBHelper.TABLE_VI_ALERT_TIME, null,
					contentValues);
			Log.i("VI_ALERT_TIME value :== ",
					"inside insert of VI_ALERT_TIME table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author ripunjay.s This method will update single record in alerttime
	 *         table and return the value whether the record is updated
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateVIAlertTimeTable(VIAlertTime viAlertTime) {

		long result = 0;
		String whereClause = DBHelper.KEY_VI_ALERT_TIME_ID + " = '"
				+ viAlertTime.getiVIAlertTimeId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_VI_ALERT_TIME_ID,
					viAlertTime.getiVIAlertTimeId());
			contentValues.put(DBHelper.VI_ALERT_TIME,
					viAlertTime.getAlertTime());
			contentValues.put(DBHelper.MAC_ID, viAlertTime.getStrMacId());
			contentValues
					.put(DBHelper.CREATED_DATE, viAlertTime.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viAlertTime.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY, viAlertTime.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, viAlertTime.getUpdatedBy());
			contentValues
					.put(DBHelper.FLG_DELETED, viAlertTime.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS, viAlertTime.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					viAlertTime.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viAlertTime.getiTenantId());

			result = database.update(DBHelper.TABLE_VI_ALERT_TIME,
					contentValues, whereClause, null);
			Log.i("VI_ALERT_TIME value :== ",
					"inside update of VI_ALERT_TIME table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author ripunjay.s fetch the single row data of VIAlertTime table By
	 *         strVIAlertTimeId.
	 * @return list of VIAlertTime object
	 */
	public List<VIAlertTime> getVIAlertTimeDataById(String strVIAlertTimeId) {

		List<VIAlertTime> viAlertTimeData = new ArrayList<VIAlertTime>();
		Cursor cursor = null;

		String whereClause = DBHelper.KEY_VI_ALERT_TIME_ID + " =  '"
				+ strVIAlertTimeId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_VI_ALERT_TIME, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside VIAlertTime");
			if (cursor != null && cursor.moveToFirst()) {
				viAlertTimeData = new ArrayList<VIAlertTime>();

				do {
					VIAlertTime viAlertTime = new VIAlertTime();
					viAlertTime
							.setiVIAlertTimeId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.KEY_VI_ALERT_TIME_ID)));
					viAlertTime.setAlertTime(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VI_ALERT_TIME)));
					viAlertTime.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MAC_ID)));
					viAlertTime.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viAlertTime.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viAlertTime.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viAlertTime.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viAlertTime.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viAlertTime.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viAlertTime.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viAlertTime.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					viAlertTimeData.add(viAlertTime);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return viAlertTimeData;
	}

	/**
	 * @author ripunjay.s fetch the single row data of VIAlertTime table .
	 * @return list of VIAlertTime object
	 */
	public List<VIAlertTime> getVIAlertTimeData() {

		List<VIAlertTime> viAlertTimeData = new ArrayList<VIAlertTime>();
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_VI_ALERT_TIME, null, null,
					null, null, null, null);
			Log.i("dbmanager", "inside VIAlertTime");
			if (cursor != null && cursor.moveToFirst()) {
				viAlertTimeData = new ArrayList<VIAlertTime>();

				do {
					VIAlertTime viAlertTime = new VIAlertTime();
					viAlertTime
							.setiVIAlertTimeId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.KEY_VI_ALERT_TIME_ID)));
					viAlertTime.setAlertTime(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VI_ALERT_TIME)));
					viAlertTime.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MAC_ID)));
					viAlertTime.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viAlertTime.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viAlertTime.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viAlertTime.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viAlertTime.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viAlertTime.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viAlertTime.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viAlertTime.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					viAlertTimeData.add(viAlertTime);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside all data else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return viAlertTimeData;
	}

	public void updateDirtyAsDraftStatus(String strVesselInspectionId) {
		// update VesselInspection
		String sqlStatement = "update " + DBHelper.VESSEL_INSPECTION + " set "
				+ DBHelper.FLG_IS_DIRTY + "='1' where "
				+ DBHelper.VESSEL_INSPECTIONS_ID + "='" + strVesselInspectionId
				+ "'";
		database.execSQL(sqlStatement);
		sqlStatement = "update " + DBHelper.FILLED_FORM + " set "
				+ DBHelper.FLG_IS_DIRTY + "='1' where "
				+ DBHelper.VESSEL_INSPECTIONS_ID + "='" + strVesselInspectionId
				+ "'";
		database.execSQL(sqlStatement);
		sqlStatement = "update " + DBHelper.FILLED_CHECKLIST + " set "
				+ DBHelper.FLG_IS_DIRTY + "='1' where "
				+ DBHelper.VESSEL_INSPECTIONS_ID + "='" + strVesselInspectionId
				+ "'";
		database.execSQL(sqlStatement);
	}

	public void deleteVesselInspection(String strVesselInspectionId) {

		try {
			String sqlStatement = "delete from "
					+ DBHelper.TABLE_VesselInspectionFormListDesc + "  where "
					+ DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);

			sqlStatement = "delete from " + DBHelper.FILLED_FORM + "  where "
					+ DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
			sqlStatement = "delete from " + DBHelper.FILLED_FORM_IMAGE
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
			sqlStatement = "delete from " + DBHelper.FILLED_CHECKLIST
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
			sqlStatement = "delete from " + DBHelper.VESSEL_INSPECTION
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteVesselInspectionRelatedData(String strVesselInspectionId) {

		try {
			String sqlStatement = "delete from "
					+ DBHelper.TABLE_VesselInspectionFormListDesc + "  where "
					+ DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);

			sqlStatement = "delete from " + DBHelper.FILLED_FORM + "  where "
					+ DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
			sqlStatement = "delete from " + DBHelper.FILLED_FORM_IMAGE
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);
			sqlStatement = "delete from " + DBHelper.FILLED_CHECKLIST
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);

			sqlStatement = "update " + DBHelper.VESSEL_INSPECTION + " set "
					+ DBHelper.FLG_STATUS + "='-1' where "
					+ DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "'";
			database.execSQL(sqlStatement);

			sqlStatement = "delete from " + DBHelper.VESSEL_INSPECTION
					+ "  where " + DBHelper.VESSEL_INSPECTIONS_ID + "='"
					+ strVesselInspectionId + "' and " + DBHelper.FLG_STATUS
					+ "= '-1'" + " and " + DBHelper.FLG_IS_EDITED + "<5";
			database.execSQL(sqlStatement);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public void truncateUserMaster() {

		String sqlStatement = "delete from " + DBHelper.TABLE_USERS;

		database.execSQL(sqlStatement);
	}

	public long insertRoleTemplateData(RoleTemplate roleTemp) {
		// TODO Auto-generated method stub
		long result = 0;

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.ROLE_TEMPLATE_ID,
					roleTemp.getiRoleTemplateId());
			contentValues.put(DBHelper.ROLE_ID, roleTemp.getIroleId());
			contentValues.put(DBHelper.TEMPLATE_NAME,
					roleTemp.getTemplate_name());
			contentValues.put(DBHelper.ICONS, roleTemp.getIcons());
			contentValues.put(DBHelper.CREATED_mod_DATE,
					roleTemp.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_mod_DATE,
					roleTemp.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, roleTemp.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_mod_BY,
					roleTemp.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, roleTemp.getFlgDeleted());
			contentValues.put(DBHelper.FLG_IS_DIRTY, roleTemp.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, roleTemp.getiTenantId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER, roleTemp.getSequence());
			contentValues.put(DBHelper.TEMPLATE_CODE,
					roleTemp.getTemplate_code());
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					roleTemp.getiFormCategoryId());
			contentValues.put(DBHelper.VI_TEMPLATE_ID,
					roleTemp.getiVesselInspectionTemplateId());

			result = database.insert(DBHelper.TABLE_ROLE_TEMPLATE, null,
					contentValues);
			Log.i("ROLE_TEMPLATE value :== ",
					"inside insert of sync_status table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public long updateRoleTemplateData(RoleTemplate roleTemp) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.ROLE_TEMPLATE_ID + " = '"
				+ roleTemp.getiRoleTemplateId() + "'";

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.ROLE_TEMPLATE_ID,
					roleTemp.getiRoleTemplateId());
			contentValues.put(DBHelper.ROLE_ID, roleTemp.getIroleId());
			contentValues.put(DBHelper.TEMPLATE_NAME,
					roleTemp.getTemplate_name());
			contentValues.put(DBHelper.ICONS, roleTemp.getIcons());
			contentValues.put(DBHelper.CREATED_mod_DATE,
					roleTemp.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_mod_DATE,
					roleTemp.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, roleTemp.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_mod_BY,
					roleTemp.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, roleTemp.getFlgDeleted());
			contentValues.put(DBHelper.FLG_IS_DIRTY, roleTemp.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, roleTemp.getiTenantId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER, roleTemp.getSequence());
			contentValues.put(DBHelper.TEMPLATE_CODE,
					roleTemp.getTemplate_code());
			contentValues.put(DBHelper.FORM_CATEGORY_ID,
					roleTemp.getiFormCategoryId());
			contentValues.put(DBHelper.VI_TEMPLATE_ID,
					roleTemp.getiVesselInspectionTemplateId());

			result = database.update(DBHelper.TABLE_ROLE_TEMPLATE,
					contentValues, whereClause, null);
			Log.i("ROLE_TEMPLATE value :== ",
					"inside insert of sync_status table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public List<RoleTemplate> getRoleTemplateDataById(String roleTemplateId) {

		List<RoleTemplate> roleTemplateData = null;
		Cursor cursor = null;

		String whereClause = DBHelper.ROLE_TEMPLATE_ID + " =  '"
				+ roleTemplateId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_ROLE_TEMPLATE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getRleTemplateDataByRoleId");
			if (cursor != null && cursor.moveToFirst()) {
				roleTemplateData = new ArrayList<RoleTemplate>();

				do {
					RoleTemplate roleTemp = new RoleTemplate();
					roleTemp.setiRoleTemplateId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_TEMPLATE_ID)));
					roleTemp.setIroleId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_ID)));
					roleTemp.setTemplate_name(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_NAME)));
					roleTemp.setIcons(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ICONS)));
					roleTemp.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					roleTemp.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					roleTemp.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					roleTemp.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					roleTemp.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					roleTemp.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					roleTemp.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					roleTemp.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					roleTemp.setTemplate_code(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_CODE)));
					roleTemp.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					roleTemp.setiVesselInspectionTemplateId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					roleTemplateData.add(roleTemp);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getRoleDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return roleTemplateData;
	}

	public List<RoleTemplate> getRoleTemplateDataByRoleId(int roleId) {

		List<RoleTemplate> roleTemplateData = null;
		Cursor cursor = null;

		String whereClause = DBHelper.ROLE_ID + " =  '" + roleId + "'";

		try {
			cursor = database.query(DBHelper.TABLE_ROLE_TEMPLATE, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getRleTemplateDataByRoleId");
			if (cursor != null && cursor.moveToFirst()) {
				roleTemplateData = new ArrayList<RoleTemplate>();

				do {
					RoleTemplate roleTemp = new RoleTemplate();
					roleTemp.setiRoleTemplateId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_TEMPLATE_ID)));
					roleTemp.setIroleId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_ID)));
					roleTemp.setTemplate_name(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_NAME)));
					roleTemp.setIcons(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ICONS)));
					roleTemp.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					roleTemp.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					roleTemp.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					roleTemp.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					roleTemp.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					roleTemp.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					roleTemp.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					roleTemp.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					roleTemp.setTemplate_code(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_CODE)));

					roleTemp.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					roleTemp.setiVesselInspectionTemplateId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					roleTemplateData.add(roleTemp);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getRoleDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return roleTemplateData;
	}

	public List<RoleTemplate> getRoleTempalteData(int tenantId, int roleId) {
		List<RoleTemplate> roleTemplateList = null;

		Cursor cursor = null;
		String whereClause = DBHelper.ROLE_ID + " = " + roleId + " and "
				+ DBHelper.FLG_DELETED + " = '0' ";// +" and "+DBHelper.TENANT_ID
													// + " =  "+ tenantId ;
		String orderBy = DBHelper.SEQUENCE_NUMBER;

		try {
			cursor = database.query(DBHelper.TABLE_ROLE_TEMPLATE, null,
					whereClause, null, null, null, orderBy);
			Log.i("dbmanager", "inside getRoleTemplateData");
			if (cursor != null && cursor.moveToFirst()) {
				roleTemplateList = new ArrayList<RoleTemplate>();

				do {
					RoleTemplate roleTemplate = new RoleTemplate();
					roleTemplate.setiRoleTemplateId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_TEMPLATE_ID)));
					roleTemplate.setIroleId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.ROLE_ID)));
					roleTemplate.setTemplate_name(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_NAME)));
					roleTemplate.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					roleTemplate
							.setModifiedDate(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					roleTemplate.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					roleTemplate.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					roleTemplate.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					roleTemplate.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));

					roleTemplate.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));

					roleTemplate.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					roleTemplate.setIcons(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.ICONS)));

					roleTemplate.setTemplate_code(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TEMPLATE_CODE)));

					roleTemplate.setiFormCategoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_CATEGORY_ID)));
					roleTemplate
							.setiVesselInspectionTemplateId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VI_TEMPLATE_ID)));

					roleTemplateList.add(roleTemplate);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataData");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return roleTemplateList;
	}

	/**
	 * return SyncHistory data
	 * 
	 * @return
	 */
	public List<SyncHistory> getSyncHistoryRow(String shipId, String macId) {
		List<SyncHistory> syncHistoryList = new ArrayList<SyncHistory>();

		String selectQuery = "SELECT  * FROM " + DBHelper.TABLE_SYNC_HISTORY
				+ " where " + DBHelper.FLG_DELETED + " ='0' and "
				+ DBHelper.SHIP_ID + "='" + shipId + "' and " + DBHelper.MAC_ID
				+ "='" + macId + "' order by " + DBHelper.SYNC_MODE;
		Cursor cursor = null;

		try {
			cursor = database.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					SyncHistory sh = new SyncHistory();
					// sh.setId(Integer.parseInt(cursor.getString(0)));
					sh.setIsyncHistoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SYNC_HISTORY_ID)));
					sh.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_DATE)));
					sh.setLogMessage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.LOG_MSG)));
					sh.setProgress(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_PROGRESS)));
					sh.setGenerator(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_GENERATOR)));
					sh.setStrFilename(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_FILE_NAME)));
					sh.setSyncOrderid(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_ORDER_ID)));
					sh.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					sh.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					sh.setDtAcknowledgeDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_ACK_DATE)));
					sh.setDtGeneratedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_GEN_DATE)));
					sh.setLastDownLoadDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_LAST_DOWN_DATE)));
					sh.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MAC_ID)));
					sh.setStrRegisterTabletId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REG_TABLET_ID)));
					sh.setiShipId(Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID))));
					sh.setiTenantId(Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID))));
					sh.setStrSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_MODE)));

					// Adding contact to list
					syncHistoryList.add(sh);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return syncHistoryList;
	}

	/**
	 * return SyncHistory data
	 * 
	 * @return
	 */
	public List<SyncHistory> getSyncHistoryDataById(String syncHistoryId) {
		List<SyncHistory> syncHistoryList = new ArrayList<SyncHistory>();

		String selectQuery = "SELECT  * FROM " + DBHelper.TABLE_SYNC_HISTORY
				+ " where " + DBHelper.KEY_SYNC_HISTORY_ID + "='"
				+ syncHistoryId + "'";
		Cursor cursor = null;

		try {
			cursor = database.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					SyncHistory sh = new SyncHistory();
					// sh.setId(Integer.parseInt(cursor.getString(0)));
					sh.setIsyncHistoryId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.KEY_SYNC_HISTORY_ID)));
					sh.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_DATE)));
					sh.setLogMessage(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.LOG_MSG)));
					sh.setProgress(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_PROGRESS)));
					sh.setGenerator(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_GENERATOR)));
					sh.setStrFilename(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_FILE_NAME)));
					sh.setSyncOrderid(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_ORDER_ID)));
					sh.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					sh.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					sh.setDtAcknowledgeDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_ACK_DATE)));
					sh.setDtGeneratedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_GEN_DATE)));
					sh.setLastDownLoadDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_LAST_DOWN_DATE)));
					sh.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MAC_ID)));
					sh.setStrRegisterTabletId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REG_TABLET_ID)));
					sh.setiShipId(Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID))));
					sh.setiTenantId(Integer.parseInt(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID))));
					sh.setStrSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SYNC_MODE)));

					// Adding contact to list
					syncHistoryList.add(sh);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return syncHistoryList;
	}

	/**
	 * @author ripunjay
	 * @param SyncHistory
	 * @return
	 */
	public long insertSyncHistorydata(SyncHistory syncHistory) {

		long result = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(DBHelper.KEY_SYNC_HISTORY_ID,
					syncHistory.getIsyncHistoryId());
			values.put(DBHelper.SYNC_DATE, syncHistory.getDtSyncDate());
			values.put(DBHelper.LOG_MSG, syncHistory.getLogMessage());
			values.put(DBHelper.SYNC_PROGRESS, syncHistory.getProgress());
			values.put(DBHelper.SYNC_GENERATOR, syncHistory.getGenerator());
			values.put(DBHelper.SYNC_FILE_NAME, syncHistory.getStrFilename());
			values.put(DBHelper.FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
			values.put(DBHelper.SYNC_ORDER_ID, syncHistory.getSyncOrderid());
			values.put(DBHelper.TENANT_ID, syncHistory.getiTenantId());
			values.put(DBHelper.SHIP_ID, syncHistory.getiShipId());
			values.put(DBHelper.FLG_DELETED, syncHistory.getFlgDeleted());
			values.put(DBHelper.SYNC_ACK_DATE,
					syncHistory.getDtAcknowledgeDate());
			values.put(DBHelper.SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
			values.put(DBHelper.SYNC_LAST_DOWN_DATE,
					syncHistory.getLastDownLoadDate());
			values.put(DBHelper.MAC_ID, syncHistory.getStrMacId());
			values.put(DBHelper.SYNC_MODE, syncHistory.getStrSyncMode());
			values.put(DBHelper.REG_TABLET_ID,
					syncHistory.getStrRegisterTabletId());

			result = database.insert(DBHelper.TABLE_SYNC_HISTORY, null, values);
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}
		return result;
	}

	/**
	 * @author ripunjay
	 * @param SyncHistory
	 * @return
	 */
	public long updateSyncHistorydata(SyncHistory syncHistory) {

		long result = 0;

		try {
			ContentValues values = new ContentValues();
			values.put(DBHelper.KEY_SYNC_HISTORY_ID,
					syncHistory.getIsyncHistoryId());
			values.put(DBHelper.SYNC_DATE, syncHistory.getDtSyncDate());
			values.put(DBHelper.LOG_MSG, syncHistory.getLogMessage());
			values.put(DBHelper.SYNC_PROGRESS, syncHistory.getProgress());
			values.put(DBHelper.SYNC_GENERATOR, syncHistory.getGenerator());
			values.put(DBHelper.SYNC_FILE_NAME, syncHistory.getStrFilename());
			values.put(DBHelper.FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
			values.put(DBHelper.SYNC_ORDER_ID, syncHistory.getSyncOrderid());
			values.put(DBHelper.TENANT_ID, syncHistory.getiTenantId());
			values.put(DBHelper.SHIP_ID, syncHistory.getiShipId());
			values.put(DBHelper.FLG_DELETED, syncHistory.getFlgDeleted());
			values.put(DBHelper.SYNC_ACK_DATE,
					syncHistory.getDtAcknowledgeDate());
			values.put(DBHelper.SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
			values.put(DBHelper.SYNC_LAST_DOWN_DATE,
					syncHistory.getLastDownLoadDate());
			values.put(DBHelper.MAC_ID, syncHistory.getStrMacId());
			values.put(DBHelper.SYNC_MODE, syncHistory.getStrSyncMode());
			values.put(DBHelper.REG_TABLET_ID,
					syncHistory.getStrRegisterTabletId());

			String whereClause = DBHelper.KEY_SYNC_HISTORY_ID + " =  '"
					+ syncHistory.getIsyncHistoryId() + "'";

			result = database.update(DBHelper.TABLE_SYNC_HISTORY, values,
					whereClause, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @return list of all VesselInspectionFormListHeader object
	 */
	public List<VesselInspectionFormListDesc> getVIFormListDescData() {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		// String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1' ";
		String whereClause = "";

		try {

			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListDescData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author pushkar.m
	 * 
	 * @return VesselInspectionFormListHeader object list for combination of
	 *         VesselInspectionId , FilledFormId and FormSectionItemId
	 */
	public List<VesselInspectionFormListDesc> getVIFLDescForSectionItem(
			String viId, String filledFormId, String sectionItemId) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '" + viId
				+ "' and " + DBHelper.FILLED_FORM_ID + " = '" + filledFormId
				+ "' and " + DBHelper.FORM_SECTION_ITEM_ID + " = '"
				+ sectionItemId + "' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	public List<VesselInspectionFormListDesc> getVIFLDescForSectionItemByType(
			String viId, String filledFormId, String sectionItemId,
			String descType) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '" + viId
				+ "' and " + DBHelper.FILLED_FORM_ID + " = '" + filledFormId
				+ "' and " + DBHelper.FORM_SECTION_ITEM_ID + " = '"
				+ sectionItemId + "' " + "and " + DBHelper.STR_DESC_TYPE
				+ " = '" + descType + "' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list for perticular id
	 */
	public List<VesselInspectionFormListDesc> getVIFormListDescById(
			String viflDescId) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VesselInspectionFormListDesc_ID + "='"
				+ viflDescId + "'  ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list for perticular id
	 */
	public List<VesselInspectionFormListDesc> getVIFormListDescByVIId(
			String viId) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + "='" + viId
				+ "' and " + DBHelper.FLG_IS_DIRTY + " = '1' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListDescData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author ripunjay.s
	 * @return VesselInspectionFormListDesc object list by vi id
	 */
	public List<VesselInspectionFormListDesc> getAllVIFormListDescByVIId(
			String viId) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " in(" + viId
				+ ") ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListDescData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list VesselInspection ids
	 */
	public List<VesselInspectionFormListDesc> getVIFormListDescForMultipleVIIds(
			String viId) {

		List<VesselInspectionFormListDesc> formDescList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " in (" + viId
				+ ") and " + DBHelper.FLG_IS_DIRTY + " = '1' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListDescData");
			if (cursor != null && cursor.moveToFirst()) {
				formDescList = new ArrayList<VesselInspectionFormListDesc>();

				do {
					VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
					viflDesc.setiVesselInspectionFormListDescId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListDesc_ID)));
					viflDesc.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflDesc.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflDesc.setiFormSectionItemId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflDesc.setiVesselInspectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflDesc.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_mod_DATE)));
					viflDesc.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_DATE)));
					viflDesc.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_mod_BY)));
					viflDesc.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflDesc.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflDesc.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflDesc.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflDesc.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflDesc.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflDesc.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflDesc.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));

					viflDesc.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflDesc.setStrDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC)));
					viflDesc.setStrDescType(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.STR_DESC_TYPE)));
					viflDesc.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formDescList.add(viflDesc);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formDescList;
	}

	/**
	 * @author pushkar.m
	 * @return update VesselInspectionFormListHeader table
	 */
	public long updateVIFLDesc(VesselInspectionFormListDesc viflDesc) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListDesc_ID + " = '"
				+ viflDesc.getiVesselInspectionFormListDescId() + "'";

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_ID,
					viflDesc.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					viflDesc.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					viflDesc.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					viflDesc.getiVesselInspectionId());
			contentValues.put(DBHelper.CREATED_mod_DATE,
					viflDesc.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_mod_DATE,
					viflDesc.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, viflDesc.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_mod_BY,
					viflDesc.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, viflDesc.getFlgDeleted());
			contentValues.put(DBHelper.FLG_IS_DIRTY, viflDesc.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viflDesc.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, viflDesc.getiShipId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER, viflDesc.getSequence());
			contentValues.put(DBHelper.FLG_STATUS, viflDesc.getFlgStatus());
			contentValues.put(DBHelper.FLG_CHECKED, viflDesc.getFlgChecked());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viflDesc.getFlgIsDeviceDirty());
			contentValues
					.put(DBHelper.FLG_IS_EDITED, viflDesc.getFlgIsEdited());
			/*
			 * contentValues.put(DBHelper.FLG_IS_HEADER_EDITED,
			 * viflDesc.getFlgIsHeaderEdited());
			 */
			contentValues.put(DBHelper.STR_DESC, viflDesc.getStrDesc());
			contentValues
					.put(DBHelper.STR_DESC_TYPE, viflDesc.getStrDescType());
			contentValues.put(DBHelper.FLG_CHECKED, viflDesc.getFlgChecked());

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListDesc, contentValues,
					whereClause, null);
			Log.i("viflDesc value :== ",
					"inside insert of VesselInspectionFormListHeader table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @return insert record in VesselInspectionFormListHeader table
	 */
	// public long insertVIFLHeader(VesselInspectionFormListDesc viflDesc) {
	public long insertVIFLDesc(VesselInspectionFormListDesc viflDesc) {
		// TODO Auto-generated method stub
		long result = 0;

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.VesselInspectionFormListDesc_ID,
					viflDesc.getiVesselInspectionFormListDescId());
			contentValues.put(DBHelper.FILLED_FORM_ID,
					viflDesc.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					viflDesc.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					viflDesc.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					viflDesc.getiVesselInspectionId());
			contentValues.put(DBHelper.CREATED_mod_DATE,
					viflDesc.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_mod_DATE,
					viflDesc.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, viflDesc.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_mod_BY,
					viflDesc.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, viflDesc.getFlgDeleted());
			contentValues.put(DBHelper.FLG_IS_DIRTY, viflDesc.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viflDesc.getiTenantId());
			contentValues.put(DBHelper.SHIP_ID, viflDesc.getiShipId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER, viflDesc.getSequence());
			contentValues.put(DBHelper.FLG_STATUS, viflDesc.getFlgStatus());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viflDesc.getFlgIsDeviceDirty());
			contentValues
					.put(DBHelper.FLG_IS_EDITED, viflDesc.getFlgIsEdited());
			/*
			 * contentValues.put(DBHelper.FLG_IS_HEADER_EDITED,
			 * viflDesc.getFlgIsHeaderEdited());
			 */
			contentValues.put(DBHelper.STR_DESC, viflDesc.getStrDesc());
			contentValues.put(DBHelper.FLG_CHECKED, viflDesc.getFlgChecked());
			contentValues
					.put(DBHelper.STR_DESC_TYPE, viflDesc.getStrDescType());

			result = database.insert(
					DBHelper.TABLE_VesselInspectionFormListDesc, null,
					contentValues);
			Log.i("VIFLHeader value :== ",
					"inside insert of VesselInspectionFormListDesc table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @return list of all VesselInspectionFormListFooter object
	 */
	public List<VesselInspectionFormListFooter> getVIFormListFooterData() {

		List<VesselInspectionFormListFooter> formFooterList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.FLG_IS_DIRTY + " =  '1' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListFooterData");
			if (cursor != null && cursor.moveToFirst()) {
				formFooterList = new ArrayList<VesselInspectionFormListFooter>();

				do {
					VesselInspectionFormListFooter viflFooter = new VesselInspectionFormListFooter();
					viflFooter
							.setiVesselInspectionFormListFooterId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListFooter_ID)));
					viflFooter.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflFooter.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflFooter
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflFooter
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflFooter.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viflFooter.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viflFooter.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viflFooter.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflFooter.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflFooter.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflFooter.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflFooter.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflFooter.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflFooter
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflFooter.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflFooter
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					viflFooter.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflFooter.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));
					viflFooter.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formFooterList.add(viflFooter);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getVIFormListFooterData else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formFooterList;
	}

	/**
	 * @author pushkar.m
	 * 
	 * @return VesselInspectionFormListFooter object list for combination of
	 *         VesselInspectionId , FilledFormId and FormSectionItemId
	 */
	public List<VesselInspectionFormListFooter> getVIFLFooterForSectionItem(
			String viId, String filledFormId, String sectionItemId) {

		List<VesselInspectionFormListFooter> formFooterList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '" + viId
				+ "' and " + DBHelper.FILLED_FORM_ID + " = '" + filledFormId
				+ "' and " + DBHelper.FORM_SECTION_ITEM_ID + " = '"
				+ sectionItemId + "' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListFooterData");
			if (cursor != null && cursor.moveToFirst()) {
				formFooterList = new ArrayList<VesselInspectionFormListFooter>();

				do {
					VesselInspectionFormListFooter viflFooter = new VesselInspectionFormListFooter();
					viflFooter
							.setiVesselInspectionFormListFooterId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListFooter_ID)));
					viflFooter.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflFooter.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflFooter
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflFooter
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflFooter.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viflFooter.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viflFooter.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viflFooter.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflFooter.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflFooter.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflFooter.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflFooter.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflFooter.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflFooter
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflFooter.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflFooter
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					viflFooter.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflFooter.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));
					viflFooter.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formFooterList.add(viflFooter);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getVIFLFooterForSectionItem else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formFooterList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list for perticular id
	 */
	public List<VesselInspectionFormListFooter> getVIFormListFooterById(
			String viflFooterId) {

		List<VesselInspectionFormListFooter> formFooterList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VesselInspectionFormListFooter_ID + "='"
				+ viflFooterId + "'  ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListFooterData");
			if (cursor != null && cursor.moveToFirst()) {
				formFooterList = new ArrayList<VesselInspectionFormListFooter>();

				do {
					VesselInspectionFormListFooter viflFooter = new VesselInspectionFormListFooter();
					viflFooter
							.setiVesselInspectionFormListFooterId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListFooter_ID)));
					viflFooter.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflFooter.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflFooter
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflFooter
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflFooter.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viflFooter.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viflFooter.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viflFooter.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflFooter.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflFooter.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflFooter.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflFooter.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflFooter.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflFooter
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflFooter.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflFooter
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					viflFooter.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflFooter.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));
					viflFooter.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formFooterList.add(viflFooter);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getVIFormListFooter else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formFooterList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list for VesselInspection
	 *         ids
	 */
	public List<VesselInspectionFormListFooter> getVIFormListFooterByVIId(
			String viId) {

		List<VesselInspectionFormListFooter> formFooterList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '" + viId
				+ "' and " + DBHelper.FLG_IS_DIRTY + " = '1' ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				formFooterList = new ArrayList<VesselInspectionFormListFooter>();

				do {
					VesselInspectionFormListFooter viflFooter = new VesselInspectionFormListFooter();
					viflFooter
							.setiVesselInspectionFormListFooterId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListFooter_ID)));
					viflFooter.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflFooter.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflFooter
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflFooter
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflFooter.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viflFooter.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viflFooter.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viflFooter.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflFooter.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflFooter.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflFooter.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflFooter.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflFooter.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflFooter
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflFooter.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflFooter
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					viflFooter.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflFooter.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));
					viflFooter.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formFooterList.add(viflFooter);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager",
						"inside getVIFormListFooterForMultipleIds else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formFooterList;
	}

	/**
	 * @author pushkar.m
	 * @return VesselInspectionFormListHeader object list for VesselInspection
	 *         ids
	 */
	public List<VesselInspectionFormListFooter> getVIFormListFooterForMultipleVIIds(
			String viId) {

		List<VesselInspectionFormListFooter> formFooterList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " in (" + viId
				+ ") ";

		try {
			cursor = database.query(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				formFooterList = new ArrayList<VesselInspectionFormListFooter>();

				do {
					VesselInspectionFormListFooter viflFooter = new VesselInspectionFormListFooter();
					viflFooter
							.setiVesselInspectionFormListFooterId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VesselInspectionFormListFooter_ID)));
					viflFooter.setiFilledFormId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FILLED_FORM_ID)));
					viflFooter.setiFormSectionId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ID)));
					viflFooter
							.setiFormSectionItemId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.FORM_SECTION_ITEM_ID)));
					viflFooter
							.setiVesselInspectionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.VESSEL_INSPECTIONS_ID)));
					viflFooter.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					viflFooter.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					viflFooter.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					viflFooter.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					viflFooter.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					viflFooter.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					viflFooter.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					viflFooter.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					viflFooter.setFlgChecked(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_CHECKED)));
					viflFooter
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					viflFooter.setFlgIsEdited(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_EDITED)));
					viflFooter
							.setFlgIsFooterEdited(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_IS_FOOTER_EDITED)));
					viflFooter.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					viflFooter.setStrFooterDesc(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.FORM_FOOTER_DESC)));
					viflFooter.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));

					formFooterList.add(viflFooter);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager",
						"inside getVIFormListFooterForMultipleIds else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return formFooterList;
	}

	/**
	 * @author pushkar.m
	 * @return update VesselInspectionFormListHeader table
	 */
	public long updateVIFLFooter(VesselInspectionFormListFooter viflFooter) {
		// TODO Auto-generated method stub
		long result = 0;
		String whereClause = DBHelper.VesselInspectionFormListFooter_ID
				+ " = '" + viflFooter.getiVesselInspectionFormListFooterId()
				+ "'";

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.FILLED_FORM_ID,
					viflFooter.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					viflFooter.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					viflFooter.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					viflFooter.getiVesselInspectionId());
			contentValues.put(DBHelper.CREATED_DATE,
					viflFooter.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viflFooter.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, viflFooter.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, viflFooter.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, viflFooter.getFlgDeleted());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, viflFooter.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viflFooter.getiTenantId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					viflFooter.getSequence());
			contentValues.put(DBHelper.FLG_STATUS, viflFooter.getFlgStatus());
			contentValues.put(DBHelper.FLG_CHECKED, viflFooter.getFlgChecked());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viflFooter.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					viflFooter.getFlgIsEdited());
			contentValues.put(DBHelper.FLG_IS_FOOTER_EDITED,
					viflFooter.getFlgIsFooterEdited());
			contentValues.put(DBHelper.FORM_FOOTER_DESC,
					viflFooter.getStrFooterDesc());
			contentValues.put(DBHelper.FLG_CHECKED, viflFooter.getFlgChecked());

			result = database.update(
					DBHelper.TABLE_VesselInspectionFormListFooter,
					contentValues, whereClause, null);
			Log.i("VIFLHeader value :== ",
					"inside insert of VesselInspectionFormListFooter table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @return insert record in VesselInspectionFormListHeader table
	 */
	public long insertVIFLFooter(VesselInspectionFormListFooter viflFooter) {
		// TODO Auto-generated method stub
		long result = 0;

		try {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.VesselInspectionFormListFooter_ID,
					viflFooter.getiVesselInspectionFormListFooterId());
			contentValues.put(DBHelper.FILLED_FORM_ID,
					viflFooter.getiFilledFormId());
			contentValues.put(DBHelper.FORM_SECTION_ID,
					viflFooter.getiFormSectionId());
			contentValues.put(DBHelper.FORM_SECTION_ITEM_ID,
					viflFooter.getiFormSectionItemId());
			contentValues.put(DBHelper.VESSEL_INSPECTIONS_ID,
					viflFooter.getiVesselInspectionId());
			contentValues.put(DBHelper.CREATED_DATE,
					viflFooter.getCreatedDate());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viflFooter.getModifiedDate());
			contentValues.put(DBHelper.CREATED_BY, viflFooter.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY, viflFooter.getModifiedBy());
			contentValues.put(DBHelper.FLG_DELETED, viflFooter.getFlgDeleted());
			contentValues
					.put(DBHelper.FLG_IS_DIRTY, viflFooter.getFlgIsDirty());
			contentValues.put(DBHelper.TENANT_ID, viflFooter.getiTenantId());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					viflFooter.getSequence());
			contentValues.put(DBHelper.FLG_STATUS, viflFooter.getFlgStatus());
			contentValues.put(DBHelper.FLG_CHECKED, viflFooter.getFlgChecked());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viflFooter.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_IS_EDITED,
					viflFooter.getFlgIsEdited());
			contentValues.put(DBHelper.FLG_IS_FOOTER_EDITED,
					viflFooter.getFlgIsFooterEdited());
			contentValues.put(DBHelper.FORM_FOOTER_DESC,
					viflFooter.getStrFooterDesc());
			contentValues.put(DBHelper.FLG_CHECKED, viflFooter.getFlgChecked());

			result = database.insert(
					DBHelper.TABLE_VesselInspectionFormListFooter, null,
					contentValues);
			Log.i("VIFLHeader value :== ",
					"inside insert of VesselInspectionFormListFooter table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author Ripunjay.s This method will insert single record in Vitemplate
	 *         version table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertViTemplateversionTable(VITemplateVersion viTemplateVersion) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_VI_TEMPLATE_VERSION_ID,
					viTemplateVersion.getiVitemplateVersionId());
			contentValues.put(DBHelper.COMPITABLE_VERSION,
					viTemplateVersion.getCompetibleVersion());
			contentValues.put(DBHelper.VERSION_NUMBER,
					viTemplateVersion.getVersionNumber());
			contentValues.put(DBHelper.CREATED_DATE,
					viTemplateVersion.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viTemplateVersion.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY,
					viTemplateVersion.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					viTemplateVersion.getUpdatedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					viTemplateVersion.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					viTemplateVersion.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					viTemplateVersion.getFlgIsDirty());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					viTemplateVersion.getSequence());
			contentValues.put(DBHelper.REMARKS,
					viTemplateVersion.getStrRemarks());
			contentValues.put(DBHelper.TENANT_ID,
					viTemplateVersion.getiTenantId());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viTemplateVersion.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_ACTIVE_VERSION,
					viTemplateVersion.getFlgActiveVersion());

			result = database.insert(DBHelper.TABLE_VI_TEMPLATE_VERSION, null,
					contentValues);
			Log.i("TABLE_VI_TEMPLATE_VERSION value :== ",
					"inside insert of TABLE_VI_TEMPLATE_VERSION table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author Ripunjay.s This method will update single record in Vitemplate
	 *         version table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateViTemplateversionTable(VITemplateVersion viTemplateVersion) {

		long result = 0;

		String whereClause = DBHelper.KEY_VI_TEMPLATE_VERSION_ID + " = '"
				+ viTemplateVersion.getiVitemplateVersionId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_VI_TEMPLATE_VERSION_ID,
					viTemplateVersion.getiVitemplateVersionId());
			contentValues.put(DBHelper.COMPITABLE_VERSION,
					viTemplateVersion.getCompetibleVersion());
			contentValues.put(DBHelper.VERSION_NUMBER,
					viTemplateVersion.getVersionNumber());
			contentValues.put(DBHelper.CREATED_DATE,
					viTemplateVersion.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					viTemplateVersion.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY,
					viTemplateVersion.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					viTemplateVersion.getUpdatedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					viTemplateVersion.getFlgDeleted());
			contentValues.put(DBHelper.FLG_STATUS,
					viTemplateVersion.getFlgStatus());
			contentValues.put(DBHelper.FLG_IS_DIRTY,
					viTemplateVersion.getFlgIsDirty());
			contentValues.put(DBHelper.SEQUENCE_NUMBER,
					viTemplateVersion.getSequence());
			contentValues.put(DBHelper.REMARKS,
					viTemplateVersion.getStrRemarks());
			contentValues.put(DBHelper.TENANT_ID,
					viTemplateVersion.getiTenantId());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					viTemplateVersion.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.FLG_ACTIVE_VERSION,
					viTemplateVersion.getFlgActiveVersion());

			result = database.update(DBHelper.TABLE_VI_TEMPLATE_VERSION,
					contentValues, whereClause, null);
			Log.i("TABLE_VI_TEMPLATE_VERSION value :== ",
					"inside update of TABLE_VI_TEMPLATE_VERSION table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	public List<VITemplateVersion> getVITemplateVersionById(String vitvId) {

		List<VITemplateVersion> templateVersionList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.KEY_VI_TEMPLATE_VERSION_ID + " = '"
				+ vitvId + "' ";

		try {
			cursor = database.query(DBHelper.TABLE_VI_TEMPLATE_VERSION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				templateVersionList = new ArrayList<VITemplateVersion>();

				do {
					VITemplateVersion vitVersion = new VITemplateVersion();
					vitVersion
							.setiVitemplateVersionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.KEY_VI_TEMPLATE_VERSION_ID)));
					vitVersion
							.setCompetibleVersion(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.COMPITABLE_VERSION)));
					vitVersion.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vitVersion.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vitVersion.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vitVersion.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vitVersion
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vitVersion.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vitVersion.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					/*
					 * vitVersion.setiShipId(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					 */
					vitVersion.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					/*
					 * vitVersion.setMacId(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.MAC_ID)));
					 */
					vitVersion.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));
					vitVersion.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vitVersion.setVersionNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VERSION_NUMBER)));
					vitVersion.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vitVersion
							.setFlgActiveVersion(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_ACTIVE_VERSION)));

					templateVersionList.add(vitVersion);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager",
						"inside getVIFormListFooterForMultipleIds else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return templateVersionList;
	}

	/**
	 * Get vi template version by status.
	 * 
	 * @param vitvId
	 * @return
	 */
	public List<VITemplateVersion> getVITemplateVersionByActiveVersion(
			int flgActiveVersion) {

		List<VITemplateVersion> templateVersionList = null;
		Cursor cursor = null;

		String whereClause = DBHelper.FLG_ACTIVE_VERSION + " = '"
				+ flgActiveVersion + "' order by " + DBHelper.CREATED_DATE
				+ " desc";

		try {
			cursor = database.query(DBHelper.TABLE_VI_TEMPLATE_VERSION, null,
					whereClause, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {
				templateVersionList = new ArrayList<VITemplateVersion>();

				do {
					VITemplateVersion vitVersion = new VITemplateVersion();
					vitVersion
							.setiVitemplateVersionId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.KEY_VI_TEMPLATE_VERSION_ID)));
					vitVersion
							.setCompetibleVersion(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.COMPITABLE_VERSION)));
					vitVersion.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					vitVersion.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					vitVersion.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					vitVersion.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					vitVersion
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					vitVersion.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					vitVersion.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_STATUS)));
					/*
					 * vitVersion.setiShipId(cursor.getInt(cursor
					 * .getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					 */
					vitVersion.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					/*
					 * vitVersion.setMacId(cursor.getString(cursor
					 * .getColumnIndexOrThrow(DBHelper.MAC_ID)));
					 */
					vitVersion.setStrRemarks(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.REMARKS)));
					vitVersion.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					vitVersion.setVersionNumber(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VERSION_NUMBER)));
					vitVersion.setSequence(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SEQUENCE_NUMBER)));
					vitVersion
							.setFlgActiveVersion(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_ACTIVE_VERSION)));

					templateVersionList.add(vitVersion);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager",
						"inside getVIFormListFooterForMultipleIds else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return templateVersionList;
	}

	public String getTemplateVersionNo() {

		String versionNumber = "";
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_VI_TEMPLATE_VERSION, null,
					null, null, null, null, null);
			Log.i("dbmanager", "inside getVIFormListHeaderData");
			if (cursor != null && cursor.moveToFirst()) {

				if (cursor.moveToNext()) {
					versionNumber = cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.VERSION_NUMBER));
				}
			} else {
				Log.i("dbmanager", "inside getTemplateVersionNumber");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return versionNumber;
	}

	/**
	 * @author pushkar.m
	 * @param deviceSyncTime
	 * @return 0 = successfull and -1 = failure
	 */
	public long insertDeviceSyncTimeTable(DeviceSyncTime deviceSyncTime) {

		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_DEVICE_SYNC_TIME_ID,
					deviceSyncTime.getiDeviceSyncTimeId());

			contentValues.put(DBHelper.CREATED_DATE,
					deviceSyncTime.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					deviceSyncTime.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY,
					deviceSyncTime.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					deviceSyncTime.getUpdatedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					deviceSyncTime.getFlgDeleted());

			contentValues.put(DBHelper.FLG_IS_DIRTY,
					deviceSyncTime.getFlgIsDirty());
			contentValues.put(DBHelper.SHIP_SYNC_TIME,
					deviceSyncTime.getShipSyncTime());
			contentValues.put(DBHelper.SHORE_SYNC_TIME,
					deviceSyncTime.getShoreSyncTime());
			contentValues
					.put(DBHelper.TENANT_ID, deviceSyncTime.getiTenantId());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					deviceSyncTime.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.SHIP_ID, deviceSyncTime.getiShipId());
			contentValues.put(DBHelper.MAC_ID, deviceSyncTime.getStrMacId());

			result = database.insert(DBHelper.TABLE_DEVICE_SYNC_TIME, null,
					contentValues);
			Log.i("TABLE_VI_TEMPLATE_VERSION value :== ",
					"inside insert of TABLE_DEVICE_SYNC_TIME table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @param deviceSyncTime
	 * @return 0 = successfull and -1 = failure
	 */
	public long updateDeviceSyncTimeTable(DeviceSyncTime deviceSyncTime) {

		long result = 0;

		String whereClause = DBHelper.KEY_DEVICE_SYNC_TIME_ID + " = '"
				+ deviceSyncTime.getiDeviceSyncTimeId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(DBHelper.KEY_DEVICE_SYNC_TIME_ID,
					deviceSyncTime.getiDeviceSyncTimeId());

			contentValues.put(DBHelper.CREATED_DATE,
					deviceSyncTime.getDtCreated());
			contentValues.put(DBHelper.MODIFIED_DATE,
					deviceSyncTime.getDtUpdated());
			contentValues.put(DBHelper.CREATED_BY,
					deviceSyncTime.getCreatedBy());
			contentValues.put(DBHelper.MODIFIED_BY,
					deviceSyncTime.getUpdatedBy());
			contentValues.put(DBHelper.FLG_DELETED,
					deviceSyncTime.getFlgDeleted());

			contentValues.put(DBHelper.FLG_IS_DIRTY,
					deviceSyncTime.getFlgIsDirty());
			contentValues.put(DBHelper.SHIP_SYNC_TIME,
					deviceSyncTime.getShipSyncTime());
			contentValues.put(DBHelper.SHORE_SYNC_TIME,
					deviceSyncTime.getShoreSyncTime());
			contentValues
					.put(DBHelper.TENANT_ID, deviceSyncTime.getiTenantId());
			contentValues.put(DBHelper.FLG_DEVICE_IS_DIRTY,
					deviceSyncTime.getFlgIsDeviceDirty());
			contentValues.put(DBHelper.SHIP_ID, deviceSyncTime.getiShipId());
			contentValues.put(DBHelper.MAC_ID, deviceSyncTime.getStrMacId());

			result = database.update(DBHelper.TABLE_DEVICE_SYNC_TIME,
					contentValues, whereClause, null);

			Log.i("TABLE_DEVICE_SYNC_TIME value :== ",
					"inside update of TABLE_DEVICE_SYNC_TIME table ");

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * @author pushkar.m
	 * @return
	 */
	public List<DeviceSyncTime> getDeviceSyncTimeData() {

		List<DeviceSyncTime> syncTimeList = null;
		Cursor cursor = null;

		try {
			cursor = database.query(DBHelper.TABLE_DEVICE_SYNC_TIME, null,
					null, null, null, null, null);
			Log.i("dbmanager", "inside getDeviceSyncTimeData");
			if (cursor != null && cursor.moveToFirst()) {
				syncTimeList = new ArrayList<DeviceSyncTime>();

				do {
					DeviceSyncTime deviceSyncTime = new DeviceSyncTime();
					deviceSyncTime
							.setiDeviceSyncTimeId(cursor.getString(cursor
									.getColumnIndexOrThrow(DBHelper.KEY_DEVICE_SYNC_TIME_ID)));

					deviceSyncTime.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_BY)));
					deviceSyncTime.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.CREATED_DATE)));
					deviceSyncTime.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_DATE)));
					deviceSyncTime.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_DELETED)));
					deviceSyncTime
							.setFlgIsDeviceDirty(cursor.getInt(cursor
									.getColumnIndexOrThrow(DBHelper.FLG_DEVICE_IS_DIRTY)));
					deviceSyncTime.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.FLG_IS_DIRTY)));
					deviceSyncTime.setShipSyncTime(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_SYNC_TIME)));
					deviceSyncTime.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.SHIP_ID)));
					deviceSyncTime.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(DBHelper.TENANT_ID)));
					deviceSyncTime.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MAC_ID)));

					deviceSyncTime.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.MODIFIED_BY)));
					deviceSyncTime.setShoreSyncTime(cursor.getString(cursor
							.getColumnIndexOrThrow(DBHelper.SHORE_SYNC_TIME)));

					syncTimeList.add(deviceSyncTime);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getDeviceSyncTimeData ");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return syncTimeList;
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableDetails() {
		ArrayList<String[]> result = null;
		Cursor c = null;
		try {
			c = database.rawQuery(
					"SELECT name FROM sqlite_master WHERE type='table'", null);
			result = new ArrayList<String[]>();
			int i = 0;
			result.add(c.getColumnNames());
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				String[] temp = new String[c.getColumnCount()];
				for (i = 0; i < temp.length; i++) {
					temp[i] = c.getString(i);
				}
				result.add(temp);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (c != null)
				c.close();
		}
		return result;
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableColumnDetails(String tableName) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		Cursor cursor = database.rawQuery("PRAGMA table_info(" + tableName
				+ ")", null);
		try {
			int nameIdx = cursor.getColumnIndexOrThrow("name");
			int typeIdx = cursor.getColumnIndexOrThrow("type");
			int notNullIdx = cursor.getColumnIndexOrThrow("notnull");
			int dfltValueIdx = cursor.getColumnIndexOrThrow("dflt_value");

			ArrayList<String> integerDefault1NotNull = new ArrayList<String>();

			while (cursor.moveToNext()) {
				String[] temp = new String[2];
				String type = cursor.getString(typeIdx);
				if (!cursor.getString(nameIdx).equalsIgnoreCase("id")) {
					if ("INTEGER".equals(type)) {
						// Integer column
						if (cursor.getInt(notNullIdx) == 1) {
							// NOT NULL
							String defaultValue = cursor
									.getString(dfltValueIdx);
							if ("1".equals(defaultValue)) {
								integerDefault1NotNull.add(cursor
										.getString(nameIdx));
							}
						}
					}
					temp[0] = cursor.getString(nameIdx);
					temp[1] = cursor.getString(typeIdx);

					result.add(temp);
				}
			}
			System.out
					.println("integerDefault1NotNull now contains a list of all columns "
							+ " defined as INTEGER NOT NULL DEFAULT 1, "
							+ integerDefault1NotNull);
		} finally {
			cursor.close();
		}

		return result;
	}

	public Cursor getDataForSync(String tableName, String strShipId,
			String strMacId, String activeVslInsp) {
		try {
			Cursor cursor;
			StringBuffer selectQuery = null;
			if (!tableName.equalsIgnoreCase(DBHelper.TABLE_SYNC_HISTORY)) {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName
						+ " where " + DBHelper.FLG_IS_DIRTY + "='1' and "
						+ DBHelper.SHIP_ID + "='" + strShipId + "' and "
						+ DBHelper.VESSEL_INSPECTIONS_ID + " in ("
						+ activeVslInsp + ")");
				L.fv(" strShipId = " + strShipId + " inspectionid is "
						+ activeVslInsp);

			} else {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName
						+ " where " + DBHelper.SHIP_ID + "='" + strShipId
						+ "' and " + DBHelper.MAC_ID + "='" + strMacId + "'");
			}

			cursor = database.rawQuery(selectQuery.toString(), null);

			if (cursor != null && cursor.getCount() > 0) {
				L.fv("cursor count is " + cursor.getCount());
			} else {
				L.fv("cursor count is null");
			}
			return cursor;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			L.fv("cursor count is " + e.getMessage());
			return null;
		}
	}

	/**
	 * @author ripunjay.s
	 * @param tableName
	 * @param strTenantId
	 * @return This method return template table data
	 */
	public Cursor getTemplateDataForSync(String tableName, String strTenantId, String strShipId) {
		try {
			Cursor cursor;
			StringBuffer selectQuery = null;

			if(tableName != null && DBHelper.TABLE_SHIP_FORM_SECTION_ITEM.equalsIgnoreCase(tableName)){
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName
				 + " where " + DBHelper.SHIP_ID + "='" + strShipId + "'");

			}
			else{
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName);
				// + " where " + DBHelper.TENANT_ID + "='" + strTenantId + "'");
			}

			cursor = database.rawQuery(selectQuery.toString(), null);

			/*
			 * if (cursor != null && cursor.getCount() > 0) {
			 * 
			 * }
			 */
			return cursor;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<String> getPkDataForSync(String strPk, String tableName) {

		String selectQuery = "SELECT  " + strPk + " FROM " + tableName;// +" where "+
																		// KEY_FLG_DELETED+"='0'";

		List<String> pkList = new ArrayList<String>();
		Cursor cursor = null;
		try {
			cursor = database.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {
					String strPkId = "";
					strPkId = cursor.getString(0);
					pkList.add(strPkId);

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return pkList;
	}

	public void insertRecordInTables(String strTablename, ContentValues values) {

		// Inserting Row
		long result = database.insert(strTablename, null, values);
		// 2nd argument is String containing nullColumnHack

	}

	public long updateRecordInTables(String strTablename, String strPk_Nmae,
			String pkValues, ContentValues values) {

		String whereClause = strPk_Nmae + " =  '" + pkValues + "'";

		long result = database.update(strTablename, values, whereClause, null);

		return result;
	}

	public long updateDirtyRecordInTables(String strTablename,
			String strShipId, String allActiveInspIds) {
		/**
		 * below code on 1 jun 2016.
		 */
		// String whereClause = DBHelper.SHIP_ID + " = '" + strShipId + "'";
		String whereClause = DBHelper.SHIP_ID + " = '" + strShipId + "' and "
				+ DBHelper.VESSEL_INSPECTIONS_ID + " in(" + allActiveInspIds
				+ ")";

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "0");

		/**
		 * below flag for sync confirmation
		 */
		if (strTablename != null
				&& DBHelper.VESSEL_INSPECTION.equalsIgnoreCase(strTablename)) {
			values.put(DBHelper.FLG_IS_EDITED, "5");
		}
		long result = database.update(strTablename, values, whereClause, null);
		return result;
	}

	public long deleteRecordFromTables(String strTablename, String strDate) {
		String whereClause = DBHelper.CREATED_DATE + " <  '" + strDate + "'";
		long result = database.delete(strTablename, whereClause, null);

		return result;
	}

	public long updateDirtyRecordInMasterTable(String strTablename,
			String strInspectionId) {

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strInspectionId + "'";

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "1");
		long result = database.update(strTablename, values, whereClause, null);
		return result;
	}

	/**
	 * Below method soft delete record from tables
	 * 
	 * @param strTablename
	 * @param strInspectionId
	 * @return
	 */
	public long markDeleteRecordInTable(String strTablename,
			String strInspectionId) {

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strInspectionId + "' and " + DBHelper.FLG_IS_DIRTY + "='0'";

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "0");
		values.put(DBHelper.FLG_DELETED, "1");
		long result = database.update(strTablename, values, whereClause, null);

		whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '" + strInspectionId
				+ "' and " + DBHelper.FLG_IS_DIRTY + "='1'";

		values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "2");
		values.put(DBHelper.FLG_DELETED, "1");
		long result1 = database.update(strTablename, values, whereClause, null);
		return result;
	}

	/**
	 * Below method mark dirty which have flgisDirty=2from tables
	 * 
	 * @param strTablename
	 * @param strInspectionId
	 * @return
	 */
	public long markDirtyRecordInTable(String strTablename,
			String strInspectionId) {

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strInspectionId + "' and " + DBHelper.FLG_IS_DIRTY + "='2'";

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "1");
		long result = database.update(strTablename, values, whereClause, null);
		return result;
	}

	/**
	 * This method write for mark all data dirty and synchistory truncate.
	 * 
	 * @return
	 */
	public long updateAllDataDirty(String strVslInspId, String strShipId) {

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "1");

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strVslInspId + "'";
		long result1 = database.update(DBHelper.VESSEL_INSPECTION, values,
				whereClause, null);
		long result2 = database.update(DBHelper.FILLED_CHECKLIST, values,
				whereClause, null);
		long result3 = database.update(DBHelper.FILLED_FORM, values,
				whereClause, null);
		long result33 = database.update(DBHelper.TABLE_VesselInspectionFormListDesc, values,
				whereClause, null);
		long result4 = database.update(DBHelper.FILLED_FORM_IMAGE, values,
				whereClause, null);

		values = new ContentValues();
		whereClause = DBHelper.SHIP_ID + " = '" + strShipId + "'";
		values.put(DBHelper.FLG_IS_DIRTY, "1");
		values.put(DBHelper.SYNC_ORDER_ID, "0");
		long result5 = database.update(DBHelper.TABLE_SYNC_HISTORY, values,
				whereClause, null);

		return result1;
	}

	/**
	 * This method write for mark all image data dirty and synchistory truncate.
	 * 
	 * @return
	 */
	public long updateAllImageDataDirty(String strVslInspId) {

		ContentValues values = new ContentValues();
		values.put(DBHelper.FLG_IS_DIRTY, "1");

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strVslInspId + "'";
		long result = database.update(DBHelper.FILLED_FORM_IMAGE, values,
				whereClause, null);

		return result;
	}

	/**
	 * @author ripunjay
	 * @return pass table to get their dirty data.
	 */
	public String[] getVslTablename() {
		String[] tablename = { DBHelper.FILLED_CHECKLIST, DBHelper.FILLED_FORM,
				DBHelper.VESSEL_INSPECTION,DBHelper.TABLE_VesselInspectionFormListDesc, DBHelper.FILLED_FORM_IMAGE };

		return tablename;
	}

	/**
	 * @author ripunjay.s This method return that vessel inspection have dirty
	 *         data or not. *
	 * @return
	 */
	public long getVesselInspectionDirtyData(String strVslInspId) {

		String whereClause = DBHelper.VESSEL_INSPECTIONS_ID + " = '"
				+ strVslInspId + "' and " + DBHelper.FLG_IS_DIRTY + " ='1'";
		long counter = 0;
		try {
			Cursor cursor;
			StringBuffer selectQuery = null;
			String tables[] = getVslTablename();
			for (String tablename : tables) {
				selectQuery = new StringBuffer("SELECT  * FROM " + tablename
						+ " where " + whereClause);
				cursor = database.rawQuery(selectQuery.toString(), null);
				if (cursor != null && cursor.getCount() > 0) {
					counter = cursor.getCount();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -5;
		}
		return counter;
	}
}
