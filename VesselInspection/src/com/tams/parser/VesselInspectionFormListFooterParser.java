package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.VesselInspectionFormListFooter;
import com.tams.utils.CommonUtil;

public class VesselInspectionFormListFooterParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2016-01-11
	 */

	private static final String TAG = VesselInspectionFormListFooterParser.class.getName()
			.toString();

	private List<VesselInspectionFormListFooter> dataList;
	private boolean isSuccess;
	private VesselInspectionFormListFooter vesselInspectionFormListFooter;
	private StringBuffer buffer;
	private boolean isVesselInspectionFormListFooterSucess = false;
	private boolean debug = true;

	/**
	 * get the value of VesselInspectionFormListFooter data
	 * 
	 * @return list with content objects
	 */
	public List<VesselInspectionFormListFooter> getVesselInspectionFormListFooterData() {
		return dataList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		dataList = new ArrayList<VesselInspectionFormListFooter>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("VesselInspectionFormListFooterList")) {
				isVesselInspectionFormListFooterSucess = true;
				Log.i("tag list true", "" + isVesselInspectionFormListFooterSucess + isSuccess);
			} else if (localName.equals("VesselInspectionFormListFooter")) {
				vesselInspectionFormListFooter = new VesselInspectionFormListFooter();
			} else if (localName.equals("iVesselInspectionFormListFooterId")) {

			} else if (localName.equals("iFilledFormId")) {

			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("iFormSectionId")) {

			} else if (localName.equals("iFormSectionItemId")) {

			} else if (localName.equals("strFooterDesc")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("flgChecked")) {

			} else if (localName.equals("flgIsEdited")) {

			} else if (localName.equals("flgIsFooterEdited")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isVesselInspectionFormListFooterSucess) {
				if (localName.equals("VesselInspectionFormListFooterList")) {
					isVesselInspectionFormListFooterSucess = false;
					Log.i("tag list true", "" + isVesselInspectionFormListFooterSucess + isSuccess);
				} else if (localName.equals("VesselInspectionFormListFooter")) {
					dataList.add(vesselInspectionFormListFooter);
				} else if (localName.equals("iVesselInspectionFormListFooterId")) {

					vesselInspectionFormListFooter.setiVesselInspectionFormListFooterId(buffer.toString().trim()); 
					
				} else if (localName.equals("iFilledFormId")) {

					vesselInspectionFormListFooter.setiFilledFormId(buffer.toString().trim());

				} else if (localName.equals("iVesselInspectionId")) {

					vesselInspectionFormListFooter.setiVesselInspectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionId")) {

					vesselInspectionFormListFooter.setiFormSectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionItemId")) {

					vesselInspectionFormListFooter.setiFormSectionItemId(buffer.toString().trim());

				} else if (localName.equals("strFooterDesc")) {

					vesselInspectionFormListFooter.setStrFooterDesc(CommonUtil.unmarshal(buffer
							.toString().trim()));

				} else if (localName.equals("flgStatus")) {

					vesselInspectionFormListFooter.setFlgStatus(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("sequence")) {

					vesselInspectionFormListFooter.setSequence(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgDeleted")) {

					vesselInspectionFormListFooter.setFlgDeleted(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgIsDirty")) {

					vesselInspectionFormListFooter.setFlgIsDirty(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("createdDate")) {

					vesselInspectionFormListFooter.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					vesselInspectionFormListFooter.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {

					vesselInspectionFormListFooter.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					vesselInspectionFormListFooter.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					vesselInspectionFormListFooter.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("iShipId")) {

					vesselInspectionFormListFooter.setiShipId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgChecked")) {
					if(buffer != null && buffer.toString() != null && !"".equals(buffer.toString().trim()))
					{
                        vesselInspectionFormListFooter.setFlgChecked(buffer.toString().trim());
					}
				} else if (localName.equals("flgIsEdited")) {
					vesselInspectionFormListFooter.setFlgIsEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsFooterEdited")) {
					vesselInspectionFormListFooter.setFlgIsFooterEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsDeviceDirty")) {
					vesselInspectionFormListFooter.setFlgIsDeviceDirty(Integer
							.parseInt(buffer.toString().trim()));
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}