package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.ShipFormSectionItem;
import com.tams.utils.CommonUtil;

public class ShipFormSectionItemParser extends DefaultHandler  {

	/**
	 * @author Ripunjay Shukla
	 * @since 2016-01-25
	 */

	private static final String TAG = ShipFormSectionItemParser.class.getName()
			.toString();

	private List<ShipFormSectionItem> shipFormSectionItemsList;
	private boolean isSuccess;
	private ShipFormSectionItem shipFormSectionItem;
	private StringBuffer buffer;
	private boolean isShipFormSectionItemsSucess = false;
	private boolean debug = true;

	/**
	 * get the value of ShipFormSectionItem data
	 * 
	 * @return list with content objects
	 */
	public List<ShipFormSectionItem> getFormSectionItemData() {
		return shipFormSectionItemsList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		shipFormSectionItemsList = new ArrayList<ShipFormSectionItem>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("ShipFormSectionItem")) {
				isShipFormSectionItemsSucess = true;
				shipFormSectionItem = new ShipFormSectionItem();
				Log.i("tag list true", "" + isShipFormSectionItemsSucess
						+ isSuccess);
			} else if (localName.equals("iShipFormSectionItemId")) {

			} else if (localName.equals("iFormSectionId")) {

			} /*else if (localName.equals("strItemRemarks")) {

			} else if (localName.equals("strRemarks")) {

			}*/ else if (localName.equals("flgStatus")) {

			} /*else if (localName.equals("sequence")) {

			}*/ else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("strShipId")) {

			} else if (localName.equals("strField1")) {

			} else if (localName.equals("strField2")) {

			} else if (localName.equals("iFormSectionItemsId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isShipFormSectionItemsSucess) {
				if (localName.equals("ShipFormSectionItem")) {
					isShipFormSectionItemsSucess = false;
					shipFormSectionItemsList.add(shipFormSectionItem);
					Log.i("tag list true", "" + isShipFormSectionItemsSucess
							+ isSuccess);
				} else if (localName.equals("iShipFormSectionItemId")) {

					shipFormSectionItem.setiShipFormSectionItemId(buffer.toString()
							.trim());

				} else if (localName.equals("iFormSectionId")) {

					shipFormSectionItem
							.setiFormSectionId(buffer.toString().trim());

				}/* else if (localName.equals("strSectionItemNames")) {
					shipFormSectionItem.setStrItemName(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strItemRemarks")) {

					shipFormSectionItem.setStrRemarks(buffer.toString().trim());

				} */else if (localName.equals("flgStatus")) {

					shipFormSectionItem.setFlgStatus(Integer.parseInt(buffer
							.toString().trim()));

				} /*else if (localName.equals("sequence")) {

					shipFormSectionItem.setSequence(Integer.parseInt(buffer
							.toString().trim()));

				}*/ else if (localName.equals("flgDeleted")) {

					shipFormSectionItem.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					shipFormSectionItem.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtCreated")) {

					shipFormSectionItem.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					shipFormSectionItem.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {

					shipFormSectionItem.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					shipFormSectionItem.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					shipFormSectionItem.setiTenantId(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("strShipId")) {
					shipFormSectionItem.setiShipId(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("strField1")) {
					shipFormSectionItem.setStrField1(buffer
							.toString().trim());

				} else if (localName.equals("strField2")) {
					shipFormSectionItem.setStrField2(buffer
							.toString().trim());
				}
				
				else if (localName.equals("iFormSectionItemsId")) {
					shipFormSectionItem.setiFormSectionItemId(buffer
							.toString().trim());	
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}
