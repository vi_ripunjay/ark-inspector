package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FilledCheckList;
import com.tams.utils.CommonUtil;

public class FilledCheckListParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = FilledCheckListParsar.class.getName()
			.toString();

	private List<FilledCheckList> filledCheckListList;
	private boolean isSuccess;
	private FilledCheckList filledCheckList;
	private StringBuffer buffer;
	private boolean isFilledCheckListSucess = false;
	private boolean debug = true;

	/**
	 * get the value of FilledCheckList data
	 * 
	 * @return list with content objects
	 */
	public List<FilledCheckList> getFilledCheckListData() {
		return filledCheckListList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		filledCheckListList = new ArrayList<FilledCheckList>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("filledCheckListList")) {
				isFilledCheckListSucess = true;
				Log.i("tag list true", "" + isFilledCheckListSucess
						+ isSuccess);
			} else if (localName.equals("filledCheckList")) {
				filledCheckList = new FilledCheckList();
			} else if (localName.equals("iFilledCheckListId")) {

			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("iCheckListSectionItemsId")) {

			} else if (localName.equals("iCheckListSectionId")) {

			} else if (localName.equals("flgChecked")) {

			} else if (localName.equals("strRemarks")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("flgIsEdited")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isFilledCheckListSucess) {
				if (localName.equals("FilledCheckList")) {
					isFilledCheckListSucess = false;
					Log.i("tag list true", "" + isFilledCheckListSucess
							+ isSuccess);
				} else if (localName.equals("filledCheckList")) {
					filledCheckListList.add(filledCheckList);
				} else if (localName.equals("iFilledCheckListId")) {

					filledCheckList.setiFilledCheckListId(buffer.toString()
							.trim());

				} else if (localName.equals("iVesselInspectionId")) {

					filledCheckList.setiVesselInspectionId(buffer.toString().trim());

				} else if (localName.equals("iCheckListSectionItemsId")) {

					filledCheckList.setiCheckListSectionItemsId(buffer.toString().trim());

				} else if (localName.equals("iCheckListSectionId")) {

					filledCheckList.setiCheckListSectionId(buffer.toString().trim());

				} else if (localName.equals("flgChecked")) {

				  if(buffer != null && buffer.toString() != null && !"".equals(buffer.toString().trim()))
				  {
					filledCheckList.setFlgChecked(buffer.toString().trim());
				  }

				} else if (localName.equals("strRemarks")) {

					filledCheckList.setStrRemarks(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("flgStatus")) {

					filledCheckList.setFlgStatus(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("sequence")) {

					filledCheckList.setSequence(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgDeleted")) {

					filledCheckList.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					filledCheckList.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("createdDate")) {

					filledCheckList.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					filledCheckList.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {

					filledCheckList.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					filledCheckList.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					filledCheckList.setiTenantId(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("iShipId")) {

					filledCheckList.setiShipId(Integer.parseInt(buffer
							.toString().trim()));

				}
				 else if (localName.equals("flgIsEdited")) {
					 filledCheckList.setFlgIsEdited(Integer.parseInt(buffer
								.toString().trim()));
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}