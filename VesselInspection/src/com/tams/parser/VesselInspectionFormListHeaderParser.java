package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.VesselInspectionFormListDesc;
import com.tams.utils.CommonUtil;

public class VesselInspectionFormListHeaderParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2016-01-11
	 */

	private static final String TAG = VesselInspectionFormListHeaderParser.class.getName()
			.toString();

	private List<VesselInspectionFormListDesc> dataList;
	private boolean isSuccess;
	private VesselInspectionFormListDesc vesselInspectionFormListHeader;
	private StringBuffer buffer;
	private boolean isVesselInspectionFormListHeaderSucess = false;
	private boolean debug = true;

	/**
	 * get the value of VesselInspectionFormListHeader data
	 * 
	 * @return list with content objects
	 */
	public List<VesselInspectionFormListDesc> getVesselInspectionFormListHeaderData() {
		return dataList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		dataList = new ArrayList<VesselInspectionFormListDesc>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("VesselInspectionFormListDescList")) {
				isVesselInspectionFormListHeaderSucess = true;
				Log.i("tag list true", "" + isVesselInspectionFormListHeaderSucess + isSuccess);
			} else if (localName.equals("VesselInspectionFormListDesc")) {
				vesselInspectionFormListHeader = new VesselInspectionFormListDesc();
			} else if (localName.equals("iVesselInspectionFormListDescId")) {

			} else if (localName.equals("iFilledFormId")) {

			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("iFormSectionId")) {

			} else if (localName.equals("iFormSectionItemId")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("flgChecked")) {

			} else if (localName.equals("flgIsEdited")) {

			} else if (localName.equals("flgIsHeaderEdited")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			}
			else if (localName.equals("descType")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isVesselInspectionFormListHeaderSucess) {
				if (localName.equals("VesselInspectionFormListDescList")) {
					isVesselInspectionFormListHeaderSucess = false;
					Log.i("tag list true", "" + isVesselInspectionFormListHeaderSucess + isSuccess);
				} else if (localName.equals("VesselInspectionFormListDesc")) {
					
					dataList.add(vesselInspectionFormListHeader);
					
				} else if (localName.equals("iVesselInspectionFormListHeaderId")) {

					vesselInspectionFormListHeader.setiVesselInspectionFormListDescId(buffer.toString().trim()); 
					
				} else if (localName.equals("iFilledFormId")) {

					vesselInspectionFormListHeader.setiFilledFormId(buffer.toString().trim());

				} else if (localName.equals("iVesselInspectionId")) {

					vesselInspectionFormListHeader.setiVesselInspectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionId")) {

					vesselInspectionFormListHeader.setiFormSectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionItemId")) {

					vesselInspectionFormListHeader.setiFormSectionItemId(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					vesselInspectionFormListHeader.setFlgStatus(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("sequence")) {

					vesselInspectionFormListHeader.setSequence(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgDeleted")) {

					vesselInspectionFormListHeader.setFlgDeleted(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgIsDirty")) {

					vesselInspectionFormListHeader.setFlgIsDirty(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("createdDate")) {

					vesselInspectionFormListHeader.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					vesselInspectionFormListHeader.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {

					vesselInspectionFormListHeader.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					vesselInspectionFormListHeader.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					vesselInspectionFormListHeader.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("iShipId")) {

					vesselInspectionFormListHeader.setiShipId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgChecked")) {
					if(buffer != null && buffer.toString() != null && !"".equals(buffer.toString().trim()))
					{
                        vesselInspectionFormListHeader.setFlgChecked(buffer.toString().trim());
					}
				} else if (localName.equals("flgIsEdited")) {
					vesselInspectionFormListHeader.setFlgIsEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsHeaderEdited")) {
					vesselInspectionFormListHeader.setFlgIsHeaderEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsDeviceDirty")) {
					vesselInspectionFormListHeader.setFlgIsDeviceDirty(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("descType")) {
					vesselInspectionFormListHeader.setStrDescType(buffer.toString().trim());
				} else if (localName.equals("strDescription")) {
					vesselInspectionFormListHeader.setStrDesc(buffer.toString().trim());
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}