package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.SyncHistory;

public class VISyncHistoryParsar extends DefaultHandler {

	private static final String TAG = VISyncHistoryParsar.class.getName()
			.toString();

	private List<SyncHistory> syncHistoryList;
	private boolean isSuccess;
	private SyncHistory syncHistory;
	private StringBuffer buffer;
	private boolean isSyncHistorySucess = false;
	private boolean debug = true;
	private boolean syncMatch = false;
	private int fullData=0;
	int errorCode = 0;

	/**
	 * get the value of SyncHistory data
	 * 
	 * @return list with content objects
	 */
	public List<SyncHistory> getSyncHistoryData() {
		return syncHistoryList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		syncHistoryList = new ArrayList<SyncHistory>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			// if (localName.equals("SyncHistoryList")) {

			if (localName.equals("VesselInspectionSyncHistory")) {
				isSyncHistorySucess = true;
				syncHistory = new SyncHistory();
				Log.i("tag list true", "" + isSyncHistorySucess + isSuccess);

			} else if (localName.equals("iVesselInspectionSyncHistoryId")) {

			} else if (localName.equals("dtSyncDate")) {

			} else if (localName.equals("logMessage")) {

			} else if (localName.equals("progress")) {

			} else if (localName.equals("generator")) {

			} else if (localName.equals("strFilename")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("syncOrderid")) {

			} else if (localName.equals("dtAcknowledgeDate")) {

			} else if (localName.equals("dtGeneratedDate")) {

			} else if (localName.equals("lastDownLoadDate")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("strMacId")) {

			} else if (localName.equals("strRegisterTabletId")) {

			} else if (localName.equals("strSyncMode")) {

			} else if (localName.equals("iShipId")) {

			}

		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isSyncHistorySucess) {
				// if (localName.equals("SyncHistoryList")) {

				if (localName.equals("VesselInspectionSyncHistory")) {
					isSyncHistorySucess = false;
					syncHistoryList.add(syncHistory);
					Log.i("tag list true", "" + isSyncHistorySucess + isSuccess);
				} else if (localName.equals("iVesselInspectionSyncHistoryId")) {

					syncHistory.setIsyncHistoryId(buffer.toString().trim());

				} else if (localName.equals("dtSyncDate")) {

					syncHistory.setDtSyncDate(buffer.toString().trim());

				} else if (localName.equals("logMessage")) {

					syncHistory.setLogMessage(buffer.toString().trim());

				} else if (localName.equals("progress")) {

					syncHistory.setProgress(buffer.toString());

				} else if (localName.equals("generator")) {

					syncHistory.setGenerator(buffer.toString().trim());

				} else if (localName.equals("strFilename")) {

					syncHistory.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					syncHistory.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("syncOrderid")) {

					syncHistory.setSyncOrderid(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtAcknowledgeDate")) {

					syncHistory.setDtAcknowledgeDate(buffer.toString().trim());

				} else if (localName.equals("dtGeneratedDate")) {

					syncHistory.setDtGeneratedDate(buffer.toString().trim());

				} else if (localName.equals("lastDownLoadDate")) {

					syncHistory.setLastDownLoadDate(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					syncHistory.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("strMacId")) {

					syncHistory.setStrMacId(buffer.toString().trim());
				} else if (localName.equals("strRegisterTabletId")) {

					syncHistory
							.setStrRegisterTabletId(buffer.toString().trim());
				} else if (localName.equals("strSyncMode")) {

					syncHistory.setStrSyncMode(buffer.toString().trim());
				} else if (localName.equals("iShipId")) {

					syncHistory.setiShipId(Integer.parseInt(buffer.toString()
							.trim()));
				}

			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}
