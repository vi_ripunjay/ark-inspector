package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.CheckListSection;
import com.tams.utils.CommonUtil;

public class CheckListSectionParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-05-11
	 */

	private static final String TAG = CheckListSectionParser.class.getName()
			.toString();

	private List<CheckListSection> checkListSectionList;
	private boolean isSuccess;
	private CheckListSection checkListSection;
	private StringBuffer buffer;
	private boolean isCheckListSectionSucess = false;
	private boolean debug = true;

	/**
	 * get the value of CheckListSection data
	 * 
	 * @return list with content objects
	 */
	public List<CheckListSection> getCheckListSectionData() {
		return checkListSectionList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		checkListSectionList = new ArrayList<CheckListSection>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("CheckListSection")) {
				isCheckListSectionSucess = true;
				checkListSection = new CheckListSection();
				Log.i("tag list true", "" + isCheckListSectionSucess
						+ isSuccess);
			}/* else if (localName.equals("checkListSection")) {
				checkListSection = new CheckListSection();
			} */else if (localName.equals("iCheckListSectionId")) {

			} else if (localName.equals("strSectionName")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isCheckListSectionSucess) {
				if (localName.equals("CheckListSection")) {
					isCheckListSectionSucess = false;
					checkListSectionList.add(checkListSection);
					Log.i("tag list true", "" + isCheckListSectionSucess
							+ isSuccess);
				}/* else if (localName.equals("checkListSection")) {
					checkListSectionList.add(checkListSection);
				} */else if (localName.equals("iCheckListSectionId")) {

					checkListSection.setiCheckListSectionId(buffer.toString().trim());

				} else if (localName.equals("strSectionName")) {
					
					checkListSection.setStrSectionName(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strDescription")) {
					
					checkListSection.setStrRemarks(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("flgStatus")) {
					
					checkListSection.setFlgStatus(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("sequence")) {
					
					checkListSection.setSequence(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgDeleted")) {
					
					checkListSection.setFlgDeleted(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {
					
					checkListSection.setFlgIsDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("createdDate")) {
					
					checkListSection.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {
					
					checkListSection.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {
					
					checkListSection.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {
					
					checkListSection.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					
					checkListSection.setiTenantId(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("iShipId")) {
					
					checkListSection.setiShipId(Integer.parseInt(buffer.toString().trim()));

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}
