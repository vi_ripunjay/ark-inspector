package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FilledFormImages;
import com.tams.utils.CommonUtil;

public class FilledFormImageparser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 21 Jun 2015
	 */

	private static final String TAG = FilledFormImageparser.class.getName()
			.toString();
	private List<FilledFormImages> list;
	private boolean isSuccess = false;
	private boolean debug = true;
	private FilledFormImages image = null;
	private StringBuffer buffer = new StringBuffer();
	private int hasMore = 0;

	// private Template template;

	/**
	 * get the value of Form data
	 * 
	 * @return list with Form objects
	 */
	public List<FilledFormImages> getImageData() {
		return list;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		list = new ArrayList<FilledFormImages>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("hasMore")) {

		}
		if (isSuccess) {
			if (localName.equals("FormImages")) {
				image = new FilledFormImages();
			} else if (localName.equals("strFormImageId")) {

			} else if (localName.equals("strImageTitle")) {

			} else if (localName.equals("strImagePath")) {

			} else if (localName.equals("strRelativeImagePath")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("downloadPath")) {

			} else if (localName.equals("strFormId")) {

			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("iFormSectionItemId")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("shipId")) {

			} else if (localName.equals("tenantId")) {

			} else if (localName.equals("strImageFileName")) {

			} else if (localName.equals("flgIsEdited")) {

			} else if (localName.equals("downLoadPath")) {

			} else if (localName.equals("iFormSectionId")) {

			} else if (localName.equals("deviceOrininalPath")) {

			} else if (localName.equals("deviceRelativePath")) {

			}
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		buffer.append(new String(ch, start, length));
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (buffer.toString().trim().equals("0")) {
				isSuccess = true;
				printInfoLog("success");
			}
			printInfoLog(localName + "	:	" + isSuccess);
		} else if (localName.equals("message")) {

		} else if (localName.equals("hasMore")) {
			setHasMore(Integer.parseInt(buffer.toString().trim()));
		}
		if (isSuccess) {
			if (localName.equals("FormImages")) {
				list.add(image);
			} else if (localName.equals("strFormImageId")) {
				image.setStrFilledFormImageId(buffer.toString().trim());

			} else if (localName.equals("strImageTitle")) {
				image.setStrDesc(buffer.toString().trim());

			} else if (localName.equals("strImagePath")) {

				image.setStrImagepath(buffer.toString().trim());
			} else if (localName.equals("strRelativeImagePath")) {
				image.setStrImagepath(buffer.toString().trim());
			} else if (localName.equals("flgDeleted")) {
				image.setFlgDeleted(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("strCreatedBy")) {
				image.setCreatedBy(buffer.toString().trim());
			} else if (localName.equals("dtCreated")) {
				image.setCreatedDate(buffer.toString().trim());
			} else if (localName.equals("downloadPath")) {
				image.setDownLoadPath(buffer.toString().trim());
			} else if (localName.equals("strFormId")) {
				image.setiFilledFormId(buffer.toString().trim());
			} else if (localName.equals("iVesselInspectionId")) {
				image.setiVesselInspectionId(buffer.toString().trim());
			} else if (localName.equals("iFormSectionItemId")) {
				image.setiFormSectionItemId(buffer.toString().trim());
			} else if (localName.equals("strDescription")) {

				if(buffer != null && buffer.toString() != null && !"".equals(buffer.toString().trim()) && !"null".equals(buffer.toString().trim()))
				{
					image.setStrDesc(CommonUtil.unmarshal(buffer.toString().trim()));
				}
			} else if (localName.equals("sequence")) {
				image.setSequence(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("flgStatus")) {
				image.setFlgStatus(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("shipId")) {
				image.setiShipId(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("tenantId")) {
				image.setiTenantId(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("strImageFileName")) {
				image.setStrImageName(buffer.toString().trim());
			} else if (localName.equals("flgIsEdited")) {
				image.setFlgIsEdited(Integer.parseInt(buffer.toString().trim()));
			} else if (localName.equals("iFormSectionId")) {
				image.setiFormSectionId(buffer.toString().trim());
			} else if (localName.equals("deviceOrininalPath")) {
				image.setDeviceOriginalImagePath(buffer.toString().trim());
			} else if (localName.equals("deviceRelativePath")) {
				image.setDeviceRelativeImagePath(buffer.toString().trim());
			}
		}
		clearBuffer();

	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();
		isSuccess = false;
	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getHasMore() {
		return hasMore;
	}

	public void setHasMore(int hasMore) {
		this.hasMore = hasMore;
	}

}
