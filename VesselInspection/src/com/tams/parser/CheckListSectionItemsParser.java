package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.CheckListSectionItems;
import com.tams.utils.CommonUtil;

public class CheckListSectionItemsParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-05-11
	 */

	private static final String TAG = CheckListSectionItemsParser.class
			.getName().toString();

	private List<CheckListSectionItems> checkListSectionItemsList;
	private boolean isSuccess;
	private CheckListSectionItems checkListSectionItems;
	private StringBuffer buffer;
	private boolean isCheckListSectionItemsSucess = false;
	private boolean debug = true;

	/**
	 * get the value of CheckListSectionItems data
	 * 
	 * @return list with content objects
	 */
	public List<CheckListSectionItems> getCheckListSectionItemsData() {
		return checkListSectionItemsList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		checkListSectionItemsList = new ArrayList<CheckListSectionItems>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			//if (localName.equals("CheckListSectionItemsList")) {
			if (localName.equals("CheckListSectionItem")) {
				isCheckListSectionItemsSucess = true;
				checkListSectionItems = new CheckListSectionItems();
				Log.i("tag list true", "" + isCheckListSectionItemsSucess
						+ isSuccess);
			}/* else if (localName.equals("checkListSectionItems")) {
				checkListSectionItems = new CheckListSectionItems();
			} */else if (localName.equals("iCheckListSectionItemsId")) {

			} else if (localName.equals("iCheckListSectionId")) {

			} else if (localName.equals("strItemNames")) {

			} else if (localName.equals("strItemConditions")) {

			} else if (localName.equals("strCommentory")) {

			} else if (localName.equals("strItemRemarks")) {

			} else if (localName.equals("flgInspected")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isCheckListSectionItemsSucess) {
				//if (localName.equals("CheckListSectionItemsList")) {
				if (localName.equals("CheckListSectionItem")) {
					isCheckListSectionItemsSucess = false;
					checkListSectionItemsList.add(checkListSectionItems);
					Log.i("tag list true", "" + isCheckListSectionItemsSucess
							+ isSuccess);
				}/* else if (localName.equals("checkListSectionItems")) {
					checkListSectionItemsList.add(checkListSectionItems);
				} */else if (localName.equals("iCheckListSectionItemsId")) {

					checkListSectionItems.setiCheckListSectionItemsId(buffer.toString().trim());

				} else if (localName.equals("iCheckListSectionId")) {

					checkListSectionItems.setiCheckListSectionId(buffer.toString().trim());

				} else if (localName.equals("strItemNames")) {
					checkListSectionItems.setStrItemNames(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strItemConditions")) {
					
					checkListSectionItems.setStrItemConditions(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strCommentory")) {
					
					checkListSectionItems.setStrCommentory(buffer.toString().trim());

				} else if (localName.equals("strItemRemarks")) {
					
					checkListSectionItems.setStrItemRemarks(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("flgInspected")) {
					
					checkListSectionItems.setFlgInspected(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {
					
					checkListSectionItems.setFlgStatus(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("sequence")) {
					
					checkListSectionItems.setSequence(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgDeleted")) {
					
					checkListSectionItems.setFlgDeleted(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {
					
					checkListSectionItems.setFlgIsDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("dtCreated")) {
					
					checkListSectionItems.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {
					
					checkListSectionItems.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {
					
					checkListSectionItems.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {
					
					checkListSectionItems.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					checkListSectionItems.setiTenantId(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("iShipId")) {
					checkListSectionItems.setiShipId(Integer.parseInt(buffer.toString().trim()));
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}
