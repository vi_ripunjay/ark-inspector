package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.Role;

public class RoleParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = RoleParsar.class.getName().toString();

	private List<Role> roleList;
	private boolean isSuccess;
	private Role role;
	private StringBuffer buffer;
	private boolean isRoleSuccess = false;
	private boolean debug = true;

	/**
	 * get the value of Role data
	 * 
	 * @return list with content objects
	 */
	public List<Role> getRoleData() {
		return roleList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		roleList = new ArrayList<Role>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("RoleList")) {
				isRoleSuccess = true;
				Log.i("tag list true", "" + isRoleSuccess + isSuccess);
			} else if (localName.equals("role")) {
				role = new Role();
			} else if (localName.equals("roleId")) {

			} else if (localName.equals("roleName")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isRoleSuccess) {
				if (localName.equals("roleList")) {
					isRoleSuccess = false;
					Log.i("tag list true", "" + isRoleSuccess + isSuccess);
				} else if (localName.equals("role")) {
					roleList.add(role);
				} else if (localName.equals("roleId")) {

					role.setRoleId(buffer.toString().trim());

				} else if (localName.equals("roleName")) {

					role.setRoleName(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					role.setFlgStatus(Integer
							.parseInt(buffer.toString().trim()));

				} else if (localName.equals("sequence")) {

					role.setSequece(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgDeleted")) {

					role.setIsDeleted(Integer
							.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					role.setFlgIsDirty(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("createdDate")) {

					role.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					role.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {

					role.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					role.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					role.setiTenantId(Integer
							.parseInt(buffer.toString().trim()));

				} else if (localName.equals("iShipId")) {

					role.setiShipId(Integer.parseInt(buffer.toString().trim()));

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}