package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.RoleTemplate;

public class RoleTemplateParser extends DefaultHandler{
	
	private static final String TAG = RoleParsar.class.getName().toString();

	private List<RoleTemplate> roleTemplateList;
	private boolean isSuccess;
	private RoleTemplate roleTemplate;
	private StringBuffer buffer;
	private boolean isRoleTemplateSuccess = false;
	private boolean debug = true;

	/**
	 * get the value of Role data
	 * 
	 * @return list with content objects
	 */
	public List<RoleTemplate> getRoleTemplateData() {
		return roleTemplateList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		roleTemplateList = new ArrayList<RoleTemplate>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("RoleTemplateList")) {
				isRoleTemplateSuccess = true;
				Log.i("tag list true", "" + isRoleTemplateSuccess + isSuccess);
			}else if (localName.equals("RoleTemplate")) {
				roleTemplate = new RoleTemplate();
				
			} else if (localName.equals("iVIRoleTemplateId")) {
				
			} else if (localName.equals("iroleId")) {
				
			} else if (localName.equals("icons")) {
				
			} else if (localName.equals("template_name")) {
				
			} else if (localName.equals("sequence")) {
				
			} else if (localName.equals("flgDeleted")) {
				
			} else if (localName.equals("flgIsDirty")) {
				
			} else if (localName.equals("createdDate")) {
				
			} else if (localName.equals("modifiedDate")) {
				
			} else if (localName.equals("createdBy")) {
				
			} else if (localName.equals("modifiedBy")) {
				
			} else if (localName.equals("iTenantId")) {
				
			} else if (localName.equals("template_code")) {
				
			} else if (localName.equals("iFormCategoryId")) {
				
			}
		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isRoleTemplateSuccess) {
				if (localName.equals("RoleTemplateList")) {
					isRoleTemplateSuccess = false;
					Log.i("tag list true", "" + isRoleTemplateSuccess + isSuccess);
				} else if (localName.equals("RoleTemplate")) {
					roleTemplateList.add(roleTemplate);
					
				} else if (localName.equals("iVIRoleTemplateId")) {
					roleTemplate.setiRoleTemplateId(buffer.toString().trim());
				} else if (localName.equals("iroleId")) {
					roleTemplate.setIroleId(Integer.parseInt(buffer.toString().trim()));
				} else if (localName.equals("icons")) {
					roleTemplate.setIcons(buffer.toString().trim());
				} else if (localName.equals("template_name")) {
					roleTemplate.setTemplate_name(buffer.toString().trim());
				} else if (localName.equals("sequence")) {
					roleTemplate.setSequence(Integer.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgDeleted")) {
					roleTemplate.setFlgDeleted(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsDirty")) {
					roleTemplate.setFlgIsDirty(Integer.parseInt(buffer.toString()
							.trim()));
				} else if (localName.equals("createdDate")) {
					roleTemplate.setCreatedDate(buffer.toString().trim());
				} else if (localName.equals("modifiedDate")) {
					roleTemplate.setModifiedDate(buffer.toString().trim());
				} else if (localName.equals("createdBy")) {
					roleTemplate.setCreatedBy(buffer.toString().trim());
				} else if (localName.equals("modifiedBy")) {
					roleTemplate.setModifiedBy(buffer.toString().trim());
				} else if (localName.equals("iTenantId")) {
					roleTemplate.setiTenantId(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("template_code")) {
					roleTemplate.setTemplate_code(buffer.toString().trim());
				} else if (localName.equals("iFormCategoryId")) {
					roleTemplate.setiFormCategoryId(buffer.toString().trim());
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}


}
