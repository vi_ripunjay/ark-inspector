package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FormSectionItem;
import com.tams.utils.CommonUtil;

public class FormSectionItemParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = FormSectionItemParsar.class.getName()
			.toString();

	private List<FormSectionItem> formSectionItemsList;
	private boolean isSuccess;
	private FormSectionItem formSectionItems;
	private StringBuffer buffer;
	private boolean isFormSectionItemsSucess = false;
	private boolean debug = true;

	/**
	 * get the value of FormSectionItem data
	 * 
	 * @return list with content objects
	 */
	public List<FormSectionItem> getFormSectionItemData() {
		return formSectionItemsList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		formSectionItemsList = new ArrayList<FormSectionItem>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			//if (localName.equals("FormSectionItemList")) {
			if (localName.equals("FormSectionItem")) {
				isFormSectionItemsSucess = true;
				formSectionItems = new FormSectionItem();
				Log.i("tag list true", "" + isFormSectionItemsSucess
						+ isSuccess);
			}/* else if (localName.equals("formSectionItems")) {
				formSectionItems = new FormSectionItem();
			}*/ else if (localName.equals("iFormSectionItemId")) {

			} else if (localName.equals("iFormSectionId")) {

			} else if (localName.equals("strItemRemarks")) {

			} else if (localName.equals("strRemarks")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isFormSectionItemsSucess) {
				//if (localName.equals("FormSectionItemList")) {
				if (localName.equals("FormSectionItem")) {
					isFormSectionItemsSucess = false;
					formSectionItemsList.add(formSectionItems);
					Log.i("tag list true", "" + isFormSectionItemsSucess
							+ isSuccess);
				}/* else if (localName.equals("formSectionItems")) {
					formSectionItemsList.add(formSectionItems);
				}*/ else if (localName.equals("iFormSectionItemsId")) {

					formSectionItems.setiFormSectionItemId(buffer.toString()
							.trim());

				} else if (localName.equals("iFormSectionId")) {

					formSectionItems
							.setiFormSectionId(buffer.toString().trim());

				} else if (localName.equals("strSectionItemNames")) {
					formSectionItems.setStrItemName(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strItemRemarks")) {

					formSectionItems.setStrRemarks(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					formSectionItems.setFlgStatus(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("sequence")) {

					formSectionItems.setSequence(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgDeleted")) {

					formSectionItems.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					formSectionItems.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtCreated")) {

					formSectionItems.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					formSectionItems.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {

					formSectionItems.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					formSectionItems.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					formSectionItems.setiTenantId(Integer.parseInt(buffer
							.toString().trim()));

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}