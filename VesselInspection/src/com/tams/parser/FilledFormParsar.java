package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FilledForm;
import com.tams.utils.CommonUtil;

public class FilledFormParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = FilledFormParsar.class.getName()
			.toString();

	private List<FilledForm> filledFormList;
	private boolean isSuccess;
	private FilledForm filledForm;
	private StringBuffer buffer;
	private boolean isFilledFormSucess = false;
	private boolean debug = true;

	/**
	 * get the value of FilledForm data
	 * 
	 * @return list with content objects
	 */
	public List<FilledForm> getFilledFormData() {
		return filledFormList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		filledFormList = new ArrayList<FilledForm>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("filledFormList")) {
				isFilledFormSucess = true;
				Log.i("tag list true", "" + isFilledFormSucess + isSuccess);
			} else if (localName.equals("filledForm")) {
				filledForm = new FilledForm();
			} else if (localName.equals("iFilledFormId")) {

			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("iFormSectionId")) {

			} else if (localName.equals("iFormSectionItemId")) {

			} else if (localName.equals("strDesc")) {

			} else if (localName.equals("strRemarks")) {

			} else if (localName.equals("strHeaderDesc")) {

			} else if (localName.equals("strFooterDesc")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("flgChecked")) {

			} else if (localName.equals("flgIsEdited")) {

			} else if (localName.equals("flgIsHeaderEdited")) {

			} else if (localName.equals("flgIsFooterEdited")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isFilledFormSucess) {
				if (localName.equals("FilledForm")) {
					isFilledFormSucess = false;
					Log.i("tag list true", "" + isFilledFormSucess + isSuccess);
				} else if (localName.equals("filledForm")) {
					filledFormList.add(filledForm);
				} else if (localName.equals("iFilledFormId")) {

					filledForm.setiFilledFormId(buffer.toString().trim());

				} else if (localName.equals("iVesselInspectionId")) {

					filledForm.setiVesselInspectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionId")) {

					filledForm.setiFormSectionId(buffer.toString().trim());

				} else if (localName.equals("iFormSectionItemId")) {

					filledForm.setiFormSectionItemId(buffer.toString().trim());

				} else if (localName.equals("strDesc")) {

					filledForm.setStrDesc(buffer.toString().trim());

				} else if (localName.equals("strRemarks")) {

					filledForm.setStrRemarks(buffer.toString().trim());

				} else if (localName.equals("strHeaderDesc")) {

					filledForm.setStrHeaderDesc(CommonUtil.unmarshal(buffer
							.toString().trim()));

				} else if (localName.equals("strFooterDesc")) {

					filledForm.setStrFooterDesc(CommonUtil.unmarshal(buffer
							.toString().trim()));

				} else if (localName.equals("flgStatus")) {

					filledForm.setFlgStatus(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("sequence")) {

					filledForm.setSequence(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgDeleted")) {

					filledForm.setFlgDeleted(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgIsDirty")) {

					filledForm.setFlgIsDirty(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("createdDate")) {

					filledForm.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					filledForm.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {

					filledForm.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					filledForm.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					filledForm.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("iShipId")) {

					filledForm.setiShipId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgChecked")) {
					if(buffer != null && buffer.toString() != null && !"".equals(buffer.toString().trim()))
					{
                        filledForm.setFlgChecked(buffer.toString().trim());
					}
				} else if (localName.equals("flgIsEdited")) {
					filledForm.setFlgIsEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsHeaderEdited")) {
					filledForm.setFlgIsHeaderEdited(Integer
							.parseInt(buffer.toString().trim()));
				} else if (localName.equals("flgIsFooterEdited")) {
					filledForm.setFlgIsFooterEdited(Integer
							.parseInt(buffer.toString().trim()));		
				} else if (localName.equals("flgIsDeviceDirty")) {
					filledForm.setFlgIsDeviceDirty(Integer
							.parseInt(buffer.toString().trim()));
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}