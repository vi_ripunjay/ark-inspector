package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FormSection;
import com.tams.utils.CommonUtil;

public class FormSectionParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-05-11
	 */

	private static final String TAG = FormSectionParsar.class.getName()
			.toString();

	private List<FormSection> formSectionList;
	private boolean isSuccess;
	private FormSection formSection;
	private StringBuffer buffer;
	private boolean isFormSectionSucess = false;
	private boolean debug = true;

	/**
	 * get the value of FormSection data
	 * 
	 * @return list with content objects
	 */
	public List<FormSection> getFormSectionData() {
		return formSectionList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		formSectionList = new ArrayList<FormSection>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			//if (localName.equals("FormSectionList")) {
			if (localName.equals("FormSection")) {
				isFormSectionSucess = true;
				formSection = new FormSection();
				Log.i("tag list true", "" + isFormSectionSucess + isSuccess);
			}/* else if (localName.equals("formSection")) {
				formSection = new FormSection();
			} */else if (localName.equals("iFormSectionId")) {

			}// else if (localName.equals("strSectionName")) {
				else if (localName.equals("iFormCategoryId")) {

			} else if (localName.equals("strFormCategory")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			}
		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isFormSectionSucess) {
				//if (localName.equals("FormSectionList")) {
				if (localName.equals("FormSection")) {
					isFormSectionSucess = false;
					formSectionList.add(formSection);
					Log.i("tag list true", "" + isFormSectionSucess + isSuccess);
				} /*else if (localName.equals("formSection")) {
					formSectionList.add(formSection);
				}*/ else if (localName.equals("iFormSectionId")) {

					formSection.setiFormSectionId(buffer.toString().trim());

				} else if (localName.equals("strSectionName")) {

					formSection.setStrSectionName(CommonUtil.unmarshal(buffer.toString().trim()));

				} //else if (localName.equals("strFormCategory")) {
					else if (localName.equals("iFormCategoryId")) {

					formSection.setStrFormCategory(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					formSection.setFlgStatus(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("sequence")) {

					formSection.setSequence(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgDeleted")) {

					formSection.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					formSection.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtCreated")) {

					formSection.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					formSection.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {

					formSection.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					formSection.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					formSection.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}