package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.SyncHistory;
import com.tams.model.VITemplateVersion;
import com.tams.utils.CommonUtil;

public class VITemplateVersionParser extends DefaultHandler{
	
	private static final String TAG = VITemplateVersionParser.class.getName()
			.toString();

	private List<VITemplateVersion> templateVersionList;
	private boolean isSuccess;
	private VITemplateVersion templateVersion;
	private StringBuffer buffer;
	private boolean isTemplateVersaionSucess = false;
	private boolean debug = true;
	private boolean syncMatch = false;
	private int fullData=0;
	int errorCode = 0;

	/**
	 * get the value of SyncHistory data
	 * 
	 * @return list with content objects
	 */
	public List<VITemplateVersion> getTempalteVersionData() {
		return templateVersionList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		templateVersionList = new ArrayList<VITemplateVersion>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			// if (localName.equals("SyncHistoryList")) {

			if (localName.equals("VITemplateVersion")) {
				isTemplateVersaionSucess = true;
				templateVersion = new VITemplateVersion();
				Log.i("tag list true", "" + isTemplateVersaionSucess + isSuccess);

			} else if (localName.equals("iVitemplateVersionId")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} /*else if (localName.equals("macId")) {

			}*/ else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("updatedBy")) {

			} else if (localName.equals("strRemarks")) {

			} else if (localName.equals("competibleVersion")) {

			} else if (localName.equals("versionNumber")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("flgActiveVersion")) {
				
			}

		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isTemplateVersaionSucess) {
				// if (localName.equals("SyncHistoryList")) {

				if (localName.equals("VITemplateVersion")) {
					isTemplateVersaionSucess = false;
					templateVersionList.add(templateVersion);
					Log.i("tag list true", "" + isTemplateVersaionSucess + isSuccess);
				} else if (localName.equals("iVitemplateVersionId")) {
					
					templateVersion.setiVitemplateVersionId(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					templateVersion.setFlgStatus(Integer.parseInt(buffer.toString().trim()));
					
				} else if (localName.equals("sequence")) {
					
					templateVersion.setSequence(Integer.parseInt(buffer.toString().trim()));

				} /*else if (localName.equals("macId")) {

					templateVersion.setMacId(buffer.toString().trim());
					
				}*/ else if (localName.equals("flgDeleted")) {
					
					templateVersion.setFlgDeleted(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {
					
					templateVersion.setFlgIsDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDeviceDirty")) {
					
					templateVersion.setFlgIsDeviceDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("dtCreated")) {
					
					templateVersion.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {
					
					templateVersion.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {
					
					templateVersion.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("updatedBy")) {
					
					templateVersion.setUpdatedBy(buffer.toString().trim());

				} else if (localName.equals("strRemarks")) {
					
					templateVersion.setStrRemarks(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("competibleVersion")) {
					
					templateVersion.setCompetibleVersion(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("versionNumber")) {
					
					templateVersion.setVersionNumber(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("iTenantId")) {

					templateVersion.setiTenantId(Integer.parseInt(buffer.toString().trim()));
					
				} else if (localName.equals("flgActiveVersion")) {

					templateVersion.setFlgActiveVersion(Integer.parseInt(buffer.toString().trim()));
					
				}

			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}


}
