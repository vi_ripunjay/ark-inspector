package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.FormCategory;
import com.tams.utils.CommonUtil;

/** 
 * @author pushkar.m
 * @since 23 July 15
 */

public class FormCategoryParser extends DefaultHandler{

	private static final String TAG = FormCategoryParser.class.getName()
			.toString();

	private List<FormCategory> formCategoryList;
	private boolean isSuccess;
	private FormCategory formCategory;
	private StringBuffer buffer;
	private boolean isFormCategorySucess = false;
	private boolean debug = true;

	/**
	 * get the value of FormCategory data
	 * 
	 * @return list with content objects
	 */
	public List<FormCategory> getFormCategoryData() {
		return formCategoryList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		formCategoryList = new ArrayList<FormCategory>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			//if (localName.equals("FormCategoryList")) {
			if (localName.equals("FormCategory")) {
				isFormCategorySucess = true;
				formCategory = new FormCategory();
				Log.i("tag list true", "" + isFormCategorySucess + isSuccess);
			}/* else if (localName.equals("FormCategory")) {
				FormCategory = new FormCategory();
			} */else if (localName.equals("iFormCategoryId")) {

			} else if (localName.equals("strCategoryName")) {

			} else if (localName.equals("strCategoryCode")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iVesselInspectionTemplateId")) {

			}
		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isFormCategorySucess) {
				//if (localName.equals("FormCategoryList")) {
				if (localName.equals("FormCategory")) {
					isFormCategorySucess = false;
					formCategoryList.add(formCategory);
					Log.i("tag list true", "" + isFormCategorySucess + isSuccess);
				} /*else if (localName.equals("FormCategory")) {
					FormCategoryList.add(FormCategory);
				}*/ else if (localName.equals("iFormCategoryId")) {

					formCategory.setiFormCategoryId(buffer.toString().trim());

				} else if (localName.equals("strCategoryName")) {

					formCategory.setStrCategoryName(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strCategoryCode")) {

					formCategory.setStrCategoryCode(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("flgStatus")) {

					formCategory.setFlgStatus(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("sequence")) {

					formCategory.setSequence(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("flgDeleted")) {

					formCategory.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					formCategory.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtCreated")) {

					formCategory.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {

					formCategory.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {

					formCategory.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {

					formCategory.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					formCategory.setiTenantId(Integer.parseInt(buffer.toString()
							.trim()));

				} else if (localName.equals("iVesselInspectionTemplateId")) {

					formCategory.setiVesselInspectionTemplateId(buffer.toString().trim());
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}
}
