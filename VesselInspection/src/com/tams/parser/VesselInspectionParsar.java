package com.tams.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.tams.model.VesselInspection;

public class VesselInspectionParsar extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = VesselInspectionParsar.class.getName()
			.toString();

	private List<VesselInspection> vesselInspectionList;
	private boolean isSuccess;
	private VesselInspection vesselInspection;
	private StringBuffer buffer;
	private boolean isVesselInspectionSucess = false;
	private boolean debug = true;

	/**
	 * get the value of VesselInspection data
	 * 
	 * @return list with content objects
	 */
	public List<VesselInspection> getVesselInspectionData() {
		return vesselInspectionList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		vesselInspectionList = new ArrayList<VesselInspection>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equalsIgnoreCase("VesselInspectionList")) {
				isVesselInspectionSucess = true;
				Log.i("tag list true", "" + isVesselInspectionSucess
						+ isSuccess);
			} else if (localName.equalsIgnoreCase("VesselInspection")) {
				vesselInspection = new VesselInspection();
			} else if (localName.equals("iVesselInspectionId")) {

			} else if (localName.equals("strVesselInspectionName")) {

			} else if (localName.equals("strRemarks")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("inspectionDate")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("strCreatedBy")) {

			} else if (localName.equals("strUpdatedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("flgIsLocked")) {

			} else if (localName.equals("inspectedBy")) {

			} else if (localName.equals("flgReverted")) {

			} else if (localName.equals("reviewedBy")) {

			} else if (localName.equals("approvedBy")) {

			} else if (localName.equals("acceptedBy")) {

			} else if (localName.equals("completedBy")) {

			} else if (localName.equals("flgIsEdited")) {

			} else if (localName.equals("iVesselInspectionStatusId")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			}			

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isVesselInspectionSucess) {
				if (localName.equalsIgnoreCase("vesselInspectionList")) {
					isVesselInspectionSucess = false;
					Log.i("tag list true", "" + isVesselInspectionSucess
							+ isSuccess);
				} else if (localName.equalsIgnoreCase("VesselInspection")) {
					vesselInspectionList.add(vesselInspection);
				} else if (localName.equals("iVesselInspectionId")) {

					vesselInspection.setiVesselInspectionId(buffer.toString()
							.trim());

				} else if (localName.equals("strVesselInspectionName")) {

					vesselInspection.setStrInspectionTitle(buffer.toString()
							.trim());

				} else if (localName.equals("strRemarks")) {

					vesselInspection.setStrRemarks(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					vesselInspection.setFlgStatus(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("sequence")) {

					vesselInspection.setSequence(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgDeleted")) {

					vesselInspection.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {

					vesselInspection.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("inspectionDate")) {

					vesselInspection
							.setInspectionDate(buffer.toString().trim());

				} else if (localName.equals("dtCreated")) {

					vesselInspection.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					vesselInspection.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("strCreatedBy")) {

					vesselInspection.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("strUpdatedBy")) {

					vesselInspection.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					vesselInspection.setiTenantId(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("iShipId")) {

					vesselInspection.setiShipId(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgIsLocked")) {

					vesselInspection.setFlgIsLocked(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("inspectedBy")) {
					vesselInspection.setInspectedBy(buffer.toString().trim());
				} else if (localName.equals("flgReverted")) {

					vesselInspection.setFlgReverted(Integer.parseInt(buffer
							.toString().trim()));
				} else if (localName.equals("reviewedBy")) {

					vesselInspection.setReviewedBy(buffer.toString().trim());

				} else if (localName.equals("approvedBy")) {
					vesselInspection.setApprovedBy(buffer.toString().trim());

				} else if (localName.equals("acceptedBy")) {
					vesselInspection.setAcceptedBy(buffer.toString().trim());

				} else if (localName.equals("completedBy")) {
					vesselInspection.setCompletedBy(buffer.toString().trim());

				} else if (localName.equals("flgIsEdited")) {
					vesselInspection.setFlgIsEdited(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("iVesselInspectionStatusId")) {
					vesselInspection.setiVesselInspectionStatusId(buffer
							.toString().trim());

				} else if (localName.equals("flgIsDeviceDirty")) {
					vesselInspection.setFlgIsDeviceDirty(Integer.parseInt(buffer
							.toString().trim()));
				}

			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}
