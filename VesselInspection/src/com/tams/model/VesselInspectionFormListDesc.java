package com.tams.model;

public class VesselInspectionFormListDesc {
	
	/**
	 * @author Ripujay Shukla
	 * @since 2016-01-11
	 */

	private String iVesselInspectionFormListDescId = null;
	private String iFilledFormId = null;
	private String iVesselInspectionId = null;
	private String iFormSectionId = null;
	private String iFormSectionItemId = null;	
	private String strDesc = null;
	
	/**
	 * flgChecked= 'Yes' then selected that part otherwise 'No'
	 */
	private String flgChecked = null;
	private Boolean flgInspectedYes = null;
	private Boolean flgInspectedNo = null;
	private boolean showDesc = false;

	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private int flgIsEdited=0;
	private int flgIsHeaderEdited = 0;
	private int flgIsDeviceDirty=0;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;
	private String strDescType;
	
	public VesselInspectionFormListDesc() {
		super();
	}

	

	public String getStrDesc() {
		return strDesc;
	}



	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}



	public String getStrDescType() {
		return strDescType;
	}



	public void setStrDescType(String strDescType) {
		this.strDescType = strDescType;
	}



	public String getiVesselInspectionFormListDescId() {
		return iVesselInspectionFormListDescId;
	}

	public void setiVesselInspectionFormListDescId(
			String iVesselInspectionFormListDescId) {
		this.iVesselInspectionFormListDescId = iVesselInspectionFormListDescId;
	}

	public String getiFilledFormId() {
		return iFilledFormId;
	}

	public void setiFilledFormId(String iFilledFormId) {
		this.iFilledFormId = iFilledFormId;
	}

	public String getiVesselInspectionId() {
		return iVesselInspectionId;
	}

	public void setiVesselInspectionId(String iVesselInspectionId) {
		this.iVesselInspectionId = iVesselInspectionId;
	}

	public String getiFormSectionId() {
		return iFormSectionId;
	}

	public void setiFormSectionId(String iFormSectionId) {
		this.iFormSectionId = iFormSectionId;
	}

	public String getiFormSectionItemId() {
		return iFormSectionItemId;
	}

	public void setiFormSectionItemId(String iFormSectionItemId) {
		this.iFormSectionItemId = iFormSectionItemId;
	}

	

	public String getFlgChecked() {
		return flgChecked;
	}

	public void setFlgChecked(String flgChecked) {
		this.flgChecked = flgChecked;
	}

	public Boolean getFlgInspectedYes() {
		return flgInspectedYes;
	}

	public void setFlgInspectedYes(Boolean flgInspectedYes) {
		this.flgInspectedYes = flgInspectedYes;
	}

	public Boolean getFlgInspectedNo() {
		return flgInspectedNo;
	}

	public void setFlgInspectedNo(Boolean flgInspectedNo) {
		this.flgInspectedNo = flgInspectedNo;
	}

	public boolean isShowDesc() {
		return showDesc;
	}

	public void setShowDesc(boolean showDesc) {
		this.showDesc = showDesc;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public int getFlgIsEdited() {
		return flgIsEdited;
	}

	public void setFlgIsEdited(int flgIsEdited) {
		this.flgIsEdited = flgIsEdited;
	}

	public int getFlgIsHeaderEdited() {
		return flgIsHeaderEdited;
	}

	public void setFlgIsHeaderEdited(int flgIsHeaderEdited) {
		this.flgIsHeaderEdited = flgIsHeaderEdited;
	}

	public int getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(int flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iVesselInspectionFormListDescId == null) ? 0
						: iVesselInspectionFormListDescId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VesselInspectionFormListDesc other = (VesselInspectionFormListDesc) obj;
		if (iVesselInspectionFormListDescId == null) {
			if (other.iVesselInspectionFormListDescId != null)
				return false;
		} else if (!iVesselInspectionFormListDescId
				.equals(other.iVesselInspectionFormListDescId))
			return false;
		return true;
	}

}
