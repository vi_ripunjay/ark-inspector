package com.tams.model;

public class VIWebDirtyTables {
	
	/**
	 * @author ripunjay.s
	 * 21 Jan 2016
	 */
	private String iVIWebDirtyTablesId = null;
	private String strRemarks = null;
	private String strTableName = null;	
	private int flgDirtyStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private int iTenantId = 0;
	
	public VIWebDirtyTables() {
		super();
	}

	public VIWebDirtyTables(String iVIWebDirtyTablesId, String strRemarks,
			String strTableName, int flgDirtyStatus, int sequence,
			int flgDeleted, int flgIsDirty, int iTenantId) {
		super();
		this.iVIWebDirtyTablesId = iVIWebDirtyTablesId;
		this.strRemarks = strRemarks;
		this.strTableName = strTableName;
		this.flgDirtyStatus = flgDirtyStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iTenantId = iTenantId;
	}

	public String getiVIWebDirtyTablesId() {
		return iVIWebDirtyTablesId;
	}

	public void setiVIWebDirtyTablesId(String iVIWebDirtyTablesId) {
		this.iVIWebDirtyTablesId = iVIWebDirtyTablesId;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public String getStrTableName() {
		return strTableName;
	}

	public void setStrTableName(String strTableName) {
		this.strTableName = strTableName;
	}

	public int getFlgDirtyStatus() {
		return flgDirtyStatus;
	}

	public void setFlgDirtyStatus(int flgDirtyStatus) {
		this.flgDirtyStatus = flgDirtyStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iVIWebDirtyTablesId == null) ? 0 : iVIWebDirtyTablesId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VIWebDirtyTables other = (VIWebDirtyTables) obj;
		if (iVIWebDirtyTablesId == null) {
			if (other.iVIWebDirtyTablesId != null)
				return false;
		} else if (!iVIWebDirtyTablesId.equals(other.iVIWebDirtyTablesId))
			return false;
		return true;
	}
}
