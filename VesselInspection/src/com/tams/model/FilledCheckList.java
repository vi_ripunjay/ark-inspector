package com.tams.model;

public class FilledCheckList {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String iFilledCheckListId = null;
	private String iVesselInspectionId = null;
	private String iCheckListSectionItemsId = null;
	private String iCheckListSectionId = null;
	/**
	 * flgChecked= 'Yes' then selected that part otherwise 'No'
	 */
	private String flgChecked = null;
	private Boolean flgInspectedYes = null;
	private Boolean flgInspectedNo = null;
	private String strDesc = null;
	private String strRemarks = null;
	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;
	private boolean showDesc=false;
	private int flgIsEdited = 0;
	private int flgIsDeviceDirty = 0;

	public FilledCheckList() {
		super();

	}

	public FilledCheckList(String iFilledCheckListId,
			String iVesselInspectionId, String iCheckListSectionItemsId,
			String iCheckListSectionId, String flgChecked,
			Boolean flgInspectedYes, Boolean flgInspectedNo, String strDesc,
			String strRemarks, int flgStatus, int sequence, int flgDeleted,
			int flgIsDirty, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, int iTenantId, int iShipId) {
		super();
		this.iFilledCheckListId = iFilledCheckListId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iCheckListSectionItemsId = iCheckListSectionItemsId;
		this.iCheckListSectionId = iCheckListSectionId;
		this.flgChecked = flgChecked;
		this.flgInspectedYes = flgInspectedYes;
		this.flgInspectedNo = flgInspectedNo;
		this.strDesc = strDesc;
		this.strRemarks = strRemarks;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getiFilledCheckListId() {
		return iFilledCheckListId;
	}

	public void setiFilledCheckListId(String iFilledCheckListId) {
		this.iFilledCheckListId = iFilledCheckListId;
	}

	public String getiVesselInspectionId() {
		return iVesselInspectionId;
	}

	public void setiVesselInspectionId(String iVesselInspectionId) {
		this.iVesselInspectionId = iVesselInspectionId;
	}

	public String getiCheckListSectionItemsId() {
		return iCheckListSectionItemsId;
	}

	public void setiCheckListSectionItemsId(String iCheckListSectionItemsId) {
		this.iCheckListSectionItemsId = iCheckListSectionItemsId;
	}

	public String getiCheckListSectionId() {
		return iCheckListSectionId;
	}

	public void setiCheckListSectionId(String iCheckListSectionId) {
		this.iCheckListSectionId = iCheckListSectionId;
	}

	public String getFlgChecked() {
		return flgChecked;
	}

	public void setFlgChecked(String flgChecked) {
		this.flgChecked = flgChecked;
	}

	public Boolean isFlgInspectedYes() {
		return flgInspectedYes;
	}

	public void setFlgInspectedYes(Boolean flgInspectedYes) {
		this.flgInspectedYes = flgInspectedYes;
	}

	public Boolean isFlgInspectedNo() {
		return flgInspectedNo;
	}

	public void setFlgInspectedNo(Boolean flgInspectedNo) {
		this.flgInspectedNo = flgInspectedNo;
	}

	public String getStrDesc() {
		return strDesc;
	}

	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public boolean isShowDesc() {
		return showDesc;
	}

	public void setShowDesc(boolean showDesc) {
		this.showDesc = showDesc;
	}

	public int getFlgIsEdited() {
		return flgIsEdited;
	}

	public void setFlgIsEdited(int flgIsEdited) {
		this.flgIsEdited = flgIsEdited;
	}

	public int getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(int flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iFilledCheckListId == null) ? 0 : iFilledCheckListId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilledCheckList other = (FilledCheckList) obj;
		if (iFilledCheckListId == null) {
			if (other.iFilledCheckListId != null)
				return false;
		} else if (!iFilledCheckListId.equals(other.iFilledCheckListId))
			return false;
		return true;
	}

}
