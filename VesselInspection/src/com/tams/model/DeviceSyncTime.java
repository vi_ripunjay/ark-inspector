package com.tams.model;

public class DeviceSyncTime {
	/**
	 * @author pushkar.m
	 */
	
	private String iDeviceSyncTimeId;
	private String shipSyncTime;
	private String shoreSyncTime;
	private String strMacId;
	private Integer flgDeleted = 0;
	private Integer flgIsDirty = 1;
	private Integer flgIsDeviceDirty = 1;
	private Integer iTenantId;
	private Integer iShipId;
	private String createdBy;
	private String updatedBy;
	private String dtCreated;
	private String dtUpdated;
	
	public DeviceSyncTime(){
		super();
	}

		
	public DeviceSyncTime(String iDeviceSyncTimeId, String shipSyncTime,
			String shoreSyncTime, String strMacId, Integer flgDeleted,
			Integer flgIsDirty, Integer flgIsDeviceDirty, Integer iTenantId,
			Integer iShipId, String createdBy, String updatedBy,
			String dtCreated, String dtUpdated) {
		super();
		this.iDeviceSyncTimeId = iDeviceSyncTimeId;
		this.shipSyncTime = shipSyncTime;
		this.shoreSyncTime = shoreSyncTime;
		this.strMacId = strMacId;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgIsDeviceDirty = flgIsDeviceDirty;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
	}


	public String getiDeviceSyncTimeId() {
		return iDeviceSyncTimeId;
	}

	public void setiDeviceSyncTimeId(String iDeviceSyncTimeId) {
		this.iDeviceSyncTimeId = iDeviceSyncTimeId;
	}

	public String getShipSyncTime() {
		return shipSyncTime;
	}

	public void setShipSyncTime(String shipSyncTime) {
		this.shipSyncTime = shipSyncTime;
	}

	public String getShoreSyncTime() {
		return shoreSyncTime;
	}

	
	public String getStrMacId() {
		return strMacId;
	}


	public void setStrMacId(String strMacId) {
		this.strMacId = strMacId;
	}


	public void setShoreSyncTime(String shoreSyncTime) {
		this.shoreSyncTime = shoreSyncTime;
	}

	public Integer getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(Integer flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public Integer getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(Integer flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public Integer getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(Integer flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	public Integer getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(Integer iTenantId) {
		this.iTenantId = iTenantId;
	}

	public Integer getiShipId() {
		return iShipId;
	}

	public void setiShipId(Integer iShipId) {
		this.iShipId = iShipId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iDeviceSyncTimeId == null) ? 0 : iDeviceSyncTimeId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceSyncTime other = (DeviceSyncTime) obj;
		if (iDeviceSyncTimeId == null) {
			if (other.iDeviceSyncTimeId != null)
				return false;
		} else if (!iDeviceSyncTimeId.equals(other.iDeviceSyncTimeId))
			return false;
		return true;
	}
	
		
}
