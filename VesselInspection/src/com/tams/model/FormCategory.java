package com.tams.model;

/**
 * @author pushkar.m
 * @since 23 July 15
 */

public class FormCategory implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String iFormCategoryId;
	private String strCategoryName;
	private String strCategoryCode;
	private String strDescription;

	private int sequence;
	private int iTenantId;
	private int flgStatus = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate;
	private String modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private String iVesselInspectionTemplateId;
	

	public FormCategory() {

	}
	
	
	public FormCategory(String iFormCategoryId, String strCategoryName,
			String strCategoryCode, String strDescription, int sequence,
			int iTenantId, int flgStatus, int flgDeleted, int flgIsDirty,
			String createdDate, String modifiedDate, String createdBy,
			String modifiedBy) {
		super();
		this.iFormCategoryId = iFormCategoryId;
		this.strCategoryName = strCategoryName;
		this.strCategoryCode = strCategoryCode;
		this.strDescription = strDescription;
		this.sequence = sequence;
		this.iTenantId = iTenantId;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
	}


	public String getiFormCategoryId() {
		return iFormCategoryId;
	}


	public void setiFormCategoryId(String iFormCategoryId) {
		this.iFormCategoryId = iFormCategoryId;
	}


	public String getStrCategoryName() {
		return strCategoryName;
	}


	public void setStrCategoryName(String strCategoryName) {
		this.strCategoryName = strCategoryName;
	}


	public String getStrCategoryCode() {
		return strCategoryCode;
	}


	public void setStrCategoryCode(String strCategoryCode) {
		this.strCategoryCode = strCategoryCode;
	}


	public String getStrDescription() {
		return strDescription;
	}


	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}


	public int getSequence() {
		return sequence;
	}


	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	
	public int getiTenantId() {
		return iTenantId;
	}


	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}


	public int getFlgStatus() {
		return flgStatus;
	}


	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}


	public int getFlgDeleted() {
		return flgDeleted;
	}


	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}


	public int getFlgIsDirty() {
		return flgIsDirty;
	}


	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}


	public String getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}


	public String getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getiVesselInspectionTemplateId() {
		return iVesselInspectionTemplateId;
	}


	public void setiVesselInspectionTemplateId(String iVesselInspectionTemplateId) {
		this.iVesselInspectionTemplateId = iVesselInspectionTemplateId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iFormCategoryId == null) ? 0 : iFormCategoryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormCategory other = (FormCategory) obj;
		if (iFormCategoryId == null) {
			if (other.iFormCategoryId != null)
				return false;
		} else if (!iFormCategoryId.equals(other.iFormCategoryId))
			return false;
		return true;
	}

	
	

}
