package com.tams.model;

public class FilledForm {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String iFilledFormId = null;
	private String iVesselInspectionId = null;
	private String iFormSectionId = null;
	private String iFormSectionItemId = null;
	private String strDesc = null;
	private String strRemarks = null;
	private String strHeaderDesc = null;
	private String strFooterDesc = null;
	/**
	 * flgChecked= 'Yes' then selected that part otherwise 'No' 26/08/205
	 */
	private String flgChecked = null;
	private Boolean flgInspectedYes = null;
	private Boolean flgInspectedNo = null;
	private boolean showDesc = false;

	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private int flgIsEdited=0;
	private int flgIsHeaderEdited = 0;
	private int flgIsFooterEdited = 0;
	private int flgIsDeviceDirty=0;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;

	public FilledForm() {
		super();

	}

	public FilledForm(String iFilledFormId, String iVesselInspectionId,
			String iFormSectionId, String iFormSectionItemId,
			String strHeaderDesc, String strFooterDesc, int sequence,
			String createdDate, String modifiedDate, int iTenantId, int iShipId) {
		super();
		this.iFilledFormId = iFilledFormId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iFormSectionId = iFormSectionId;
		this.iFormSectionItemId = iFormSectionItemId;
		this.strHeaderDesc = strHeaderDesc;
		this.strFooterDesc = strFooterDesc;
		this.sequence = sequence;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public FilledForm(String iFilledFormId, String iVesselInspectionId,
			String iFormSectionId, String strDesc, String strRemarks,
			String strHeaderDesc, String strFooterDesc, int flgStatus,
			int sequence, int flgDeleted, int flgIsDirty, String createdDate,
			String modifiedDate, String createdBy, String modifiedBy,
			int iTenantId, int iShipId) {
		super();
		this.iFilledFormId = iFilledFormId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iFormSectionId = iFormSectionId;
		this.strDesc = strDesc;
		this.strRemarks = strRemarks;
		this.strHeaderDesc = strHeaderDesc;
		this.strFooterDesc = strFooterDesc;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public FilledForm(String iFilledFormId, String iVesselInspectionId,
			String iFormSectionId, String iFormSectionItemId, String strDesc,
			String strRemarks, String strHeaderDesc, String strFooterDesc,
			String flgChecked, Boolean flgInspectedYes, Boolean flgInspectedNo,
			int flgStatus, int sequence, int flgDeleted, int flgIsDirty,
			String createdDate, String modifiedDate, String createdBy,
			String modifiedBy, int iTenantId, int iShipId) {
		super();
		this.iFilledFormId = iFilledFormId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iFormSectionId = iFormSectionId;
		this.iFormSectionItemId = iFormSectionItemId;
		this.strDesc = strDesc;
		this.strRemarks = strRemarks;
		this.strHeaderDesc = strHeaderDesc;
		this.strFooterDesc = strFooterDesc;
		this.flgChecked = flgChecked;
		this.flgInspectedYes = flgInspectedYes;
		this.flgInspectedNo = flgInspectedNo;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}
		
	
	public FilledForm(String iFilledFormId, String iVesselInspectionId,
			String iFormSectionId, String iFormSectionItemId, String strDesc,
			String strRemarks, String strHeaderDesc, String strFooterDesc,
			String flgChecked, Boolean flgInspectedYes, Boolean flgInspectedNo,
			boolean showDesc, int flgStatus, int sequence, int flgDeleted,
			int flgIsDirty, int flgIsEdited, int flgIsHeaderEdited,
			int flgIsFooterEdited, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, int iTenantId, int iShipId) {
		super();
		this.iFilledFormId = iFilledFormId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iFormSectionId = iFormSectionId;
		this.iFormSectionItemId = iFormSectionItemId;
		this.strDesc = strDesc;
		this.strRemarks = strRemarks;
		this.strHeaderDesc = strHeaderDesc;
		this.strFooterDesc = strFooterDesc;
		this.flgChecked = flgChecked;
		this.flgInspectedYes = flgInspectedYes;
		this.flgInspectedNo = flgInspectedNo;
		this.showDesc = showDesc;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgIsEdited = flgIsEdited;
		this.flgIsHeaderEdited = flgIsHeaderEdited;
		this.flgIsFooterEdited = flgIsFooterEdited;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getiFilledFormId() {
		return iFilledFormId;
	}

	public void setiFilledFormId(String iFilledFormId) {
		this.iFilledFormId = iFilledFormId;
	}

	public String getiVesselInspectionId() {
		return iVesselInspectionId;
	}

	public void setiVesselInspectionId(String iVesselInspectionId) {
		this.iVesselInspectionId = iVesselInspectionId;
	}

	public String getiFormSectionId() {
		return iFormSectionId;
	}

	public void setiFormSectionId(String iFormSectionId) {
		this.iFormSectionId = iFormSectionId;
	}

	public String getStrDesc() {
		return strDesc;
	}

	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

		
	public int getFlgIsEdited() {
		return flgIsEdited;
	}
		
	public int getFlgIsHeaderEdited() {
		return flgIsHeaderEdited;
	}

	public void setFlgIsHeaderEdited(int flgIsHeaderEdited) {
		this.flgIsHeaderEdited = flgIsHeaderEdited;
	}

	public int getFlgIsFooterEdited() {
		return flgIsFooterEdited;
	}

	public void setFlgIsFooterEdited(int flgIsFooterEdited) {
		this.flgIsFooterEdited = flgIsFooterEdited;
	}

	public void setFlgIsEdited(int flgIsEdited) {
		this.flgIsEdited = flgIsEdited;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public String getStrHeaderDesc() {
		return strHeaderDesc;
	}

	public void setStrHeaderDesc(String strHeaderDesc) {
		this.strHeaderDesc = strHeaderDesc;
	}

	public String getStrFooterDesc() {
		return strFooterDesc;
	}

	public void setStrFooterDesc(String strFooterDesc) {
		this.strFooterDesc = strFooterDesc;
	}

	public String getiFormSectionItemId() {
		return iFormSectionItemId;
	}

	public void setiFormSectionItemId(String iFormSectionItemId) {
		this.iFormSectionItemId = iFormSectionItemId;
	}

	/**
	 * 26/08/2015
	 * 
	 * @return
	 */
	public String getFlgChecked() {
		return flgChecked;
	}

	public void setFlgChecked(String flgChecked) {
		this.flgChecked = flgChecked;
	}

	public Boolean getFlgInspectedYes() {
		return flgInspectedYes;
	}

	public void setFlgInspectedYes(Boolean flgInspectedYes) {
		this.flgInspectedYes = flgInspectedYes;
	}

	public Boolean getFlgInspectedNo() {
		return flgInspectedNo;
	}

	public void setFlgInspectedNo(Boolean flgInspectedNo) {
		this.flgInspectedNo = flgInspectedNo;
	}

	public boolean isShowDesc() {
		return showDesc;
	}

	public void setShowDesc(boolean showDesc) {
		this.showDesc = showDesc;
	}

	public int getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(int flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iFilledFormId == null) ? 0 : iFilledFormId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilledForm other = (FilledForm) obj;
		if (iFilledFormId == null) {
			if (other.iFilledFormId != null)
				return false;
		} else if (!iFilledFormId.equals(other.iFilledFormId))
			return false;
		return true;
	}

}