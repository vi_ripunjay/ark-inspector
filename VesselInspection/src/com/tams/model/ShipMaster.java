package com.tams.model;

public class ShipMaster {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	Integer iShipId;
	Integer iTenantId;      
	String iClassificationSocietyId;
	String iShipTypeId;
	String strShipName;
	String strDescription;
	String dtCreated;
	String dtUpdated;
	Integer flgStatus=0;
	Integer flgDeleted=0;
	Integer flgIsDirty=1;
	String iRuleListId;
	String iShipIMONumber;
	String strFlag;
	String strLogo;
	String fileSize;
	String strFileName;
	String strFilePath;
	String strFileType;
	String strShipCode;

	public ShipMaster() {
		super();		
	}

	public ShipMaster(Integer iShipId, Integer iTenantId,
			String iClassificationSocietyId, String iShipTypeId,
			String strShipName, String strDescription, String dtCreated,
			String dtUpdated, Integer flgStatus, Integer flgDeleted,
			Integer flgIsDirty, String iRuleListId, String iShipIMONumber,
			String strFlag, String strLogo, String fileSize,
			String strFileName, String strFilePath, String strFileType,
			String strShipCode) {
		super();
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.iClassificationSocietyId = iClassificationSocietyId;
		this.iShipTypeId = iShipTypeId;
		this.strShipName = strShipName;
		this.strDescription = strDescription;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iRuleListId = iRuleListId;
		this.iShipIMONumber = iShipIMONumber;
		this.strFlag = strFlag;
		this.strLogo = strLogo;
		this.fileSize = fileSize;
		this.strFileName = strFileName;
		this.strFilePath = strFilePath;
		this.strFileType = strFileType;
		this.strShipCode = strShipCode;
	}

	public Integer getiShipId() {
		return iShipId;
	}

	public void setiShipId(Integer iShipId) {
		this.iShipId = iShipId;
	}

	public Integer getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(Integer iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getiClassificationSocietyId() {
		return iClassificationSocietyId;
	}

	public void setiClassificationSocietyId(String iClassificationSocietyId) {
		this.iClassificationSocietyId = iClassificationSocietyId;
	}

	public String getiShipTypeId() {
		return iShipTypeId;
	}

	public void setiShipTypeId(String iShipTypeId) {
		this.iShipTypeId = iShipTypeId;
	}

	public String getStrShipName() {
		return strShipName;
	}

	public void setStrShipName(String strShipName) {
		this.strShipName = strShipName;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public Integer getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(Integer flgStatus) {
		this.flgStatus = flgStatus;
	}

	public Integer getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(Integer flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public Integer getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(Integer flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getiRuleListId() {
		return iRuleListId;
	}

	public void setiRuleListId(String iRuleListId) {
		this.iRuleListId = iRuleListId;
	}

	public String getiShipIMONumber() {
		return iShipIMONumber;
	}

	public void setiShipIMONumber(String iShipIMONumber) {
		this.iShipIMONumber = iShipIMONumber;
	}

	public String getStrFlag() {
		return strFlag;
	}

	public void setStrFlag(String strFlag) {
		this.strFlag = strFlag;
	}

	public String getStrLogo() {
		return strLogo;
	}

	public void setStrLogo(String strLogo) {
		this.strLogo = strLogo;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getStrFileName() {
		return strFileName;
	}

	public void setStrFileName(String strFileName) {
		this.strFileName = strFileName;
	}

	public String getStrFilePath() {
		return strFilePath;
	}

	public void setStrFilePath(String strFilePath) {
		this.strFilePath = strFilePath;
	}

	public String getStrFileType() {
		return strFileType;
	}

	public void setStrFileType(String strFileType) {
		this.strFileType = strFileType;
	}

	public String getStrShipCode() {
		return strShipCode;
	}

	public void setStrShipCode(String strShipCode) {
		this.strShipCode = strShipCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iShipId == null) ? 0 : iShipId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipMaster other = (ShipMaster) obj;
		if (iShipId == null) {
			if (other.iShipId != null)
				return false;
		} else if (!iShipId.equals(other.iShipId))
			return false;
		return true;
	}	
}
