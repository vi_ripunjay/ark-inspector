package com.tams.model;

public class VIAlertTime {

	private String iVIAlertTimeId;
	private String alertTime;
	private String strMacId;
	private int iTenantId;
	private int flgStatus = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String dtCreated;
	private String dtUpdated;
	private String createdBy;
	private String updatedBy;

	public VIAlertTime() {
		super();
	}

	public VIAlertTime(String iVIAlertTimeId, String alertTime, String strMacId, int iTenantId,
			int flgStatus, int flgDeleted, int flgIsDirty, String dtCreated,
			String dtUpdated, String createdBy, String updatedBy) {
		super();
		this.iVIAlertTimeId = iVIAlertTimeId;
		this.alertTime = alertTime;
		this.strMacId = strMacId;
		this.iTenantId = iTenantId;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public String getiVIAlertTimeId() {
		return iVIAlertTimeId;
	}

	public void setiVIAlertTimeId(String iVIAlertTimeId) {
		this.iVIAlertTimeId = iVIAlertTimeId;
	}

	public String getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(String alertTime) {
		this.alertTime = alertTime;
	}

	public String getStrMacId() {
		return strMacId;
	}

	public void setStrMacId(String strMacId) {
		this.strMacId = strMacId;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iVIAlertTimeId == null) ? 0 : iVIAlertTimeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VIAlertTime other = (VIAlertTime) obj;
		if (iVIAlertTimeId == null) {
			if (other.iVIAlertTimeId != null)
				return false;
		} else if (!iVIAlertTimeId.equals(other.iVIAlertTimeId))
			return false;
		return true;
	}

}
