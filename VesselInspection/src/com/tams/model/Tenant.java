package com.tams.model;

public class Tenant {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private int iTenantId = 0;
	private String strTenantName = null;
	private String strDesc = null;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate = null;

	public Tenant() {
		super();
		
	}

	public Tenant(int iTenantId, String strTenantName, String strDesc,
			int flgDeleted, int flgIsDirty, String createdDate) {
		super();
		this.iTenantId = iTenantId;
		this.strTenantName = strTenantName;
		this.strDesc = strDesc;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getStrTenantName() {
		return strTenantName;
	}

	public void setStrTenantName(String strTenantName) {
		this.strTenantName = strTenantName;
	}

	public String getStrDesc() {
		return strDesc;
	}

	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + iTenantId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tenant other = (Tenant) obj;
		if (iTenantId != other.iTenantId)
			return false;
		return true;
	}

}
