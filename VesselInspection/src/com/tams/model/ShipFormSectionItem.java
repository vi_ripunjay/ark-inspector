package com.tams.model;

public class ShipFormSectionItem {

	/**
	 * @author Ripunjay Shukla
	 * @since 25-Jan-2016
	 */

	private String iShipFormSectionItemId ;
	private String iFormSectionId ;
	private String iFormSectionItemId;
	//private String strItemName ;
	//private String strRemarks ;
	private int flgStatus = 0;
	//private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate ;
	private String modifiedDate ;
	private String createdBy ;
	private String modifiedBy ;
	private int iTenantId = 0;
	private int iShipId = 0;
	private String strField1;
	private String strField2;
	
	private String iShipTypeId;
	private String associatedTenant;
	private String iDynamicFormListId;
	
	public ShipFormSectionItem() {
		super();
	}

	public ShipFormSectionItem(String iShipFormSectionItemId,
			String iFormSectionId, String iFormSectionItemId, int flgStatus,
			int flgDeleted, int flgIsDirty, String createdDate,
			String modifiedDate, String createdBy, String modifiedBy,
			int iTenantId, int iShipId, String strField1, String strField2,
			String iShipTypeId, String associatedTenant,
			String iDynamicFormListId) {
		super();
		this.iShipFormSectionItemId = iShipFormSectionItemId;
		this.iFormSectionId = iFormSectionId;
		this.iFormSectionItemId = iFormSectionItemId;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.strField1 = strField1;
		this.strField2 = strField2;
		this.iShipTypeId = iShipTypeId;
		this.associatedTenant = associatedTenant;
		this.iDynamicFormListId = iDynamicFormListId;
	}

	public String getiShipFormSectionItemId() {
		return iShipFormSectionItemId;
	}

	public void setiShipFormSectionItemId(String iShipFormSectionItemId) {
		this.iShipFormSectionItemId = iShipFormSectionItemId;
	}

	public String getiFormSectionId() {
		return iFormSectionId;
	}

	public void setiFormSectionId(String iFormSectionId) {
		this.iFormSectionId = iFormSectionId;
	}

	/*public String getStrItemName() {
		return strItemName;
	}

	public void setStrItemName(String strItemName) {
		this.strItemName = strItemName;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}
*/
	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	/*public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}*/

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}
	
	public String getiFormSectionItemId() {
		return iFormSectionItemId;
	}


	public void setiFormSectionItemId(String iFormSectionItemId) {
		this.iFormSectionItemId = iFormSectionItemId;
	}

	
	public String getStrField1() {
		return strField1;
	}


	public void setStrField1(String strField1) {
		this.strField1 = strField1;
	}


	public String getStrField2() {
		return strField2;
	}


	public void setStrField2(String strField2) {
		this.strField2 = strField2;
	}

	public String getiShipTypeId() {
		return iShipTypeId;
	}

	public void setiShipTypeId(String iShipTypeId) {
		this.iShipTypeId = iShipTypeId;
	}

	public String getAssociatedTenant() {
		return associatedTenant;
	}

	public void setAssociatedTenant(String associatedTenant) {
		this.associatedTenant = associatedTenant;
	}

	public String getiDynamicFormListId() {
		return iDynamicFormListId;
	}

	public void setiDynamicFormListId(String iDynamicFormListId) {
		this.iDynamicFormListId = iDynamicFormListId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iShipFormSectionItemId == null) ? 0
						: iShipFormSectionItemId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipFormSectionItem other = (ShipFormSectionItem) obj;
		if (iShipFormSectionItemId == null) {
			if (other.iShipFormSectionItemId != null)
				return false;
		} else if (!iShipFormSectionItemId.equals(other.iShipFormSectionItemId))
			return false;
		return true;
	}
}
