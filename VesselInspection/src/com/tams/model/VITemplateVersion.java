package com.tams.model;

/**
 * @author ripunjay.s This class made for maintain version of template.
 */
public class VITemplateVersion {

	private static final long serialVersionUID = 1L;
	private String iVitemplateVersionId;
	private Integer flgStatus = 0;
	private Integer sequence = 0;
	//private String macId;
	private Integer flgDeleted = 0;
	private Integer flgIsDirty = 1;
	private String dtCreated;
	private String dtUpdated;
	private String createdBy;
	private String updatedBy;
	//private Integer iShipId = 0;
	private Integer iTenantId = 0;
	private Integer flgIsDeviceDirty = 1;
	private String strRemarks;

	private String competibleVersion="0";
	private String versionNumber="0";
	private Integer flgActiveVersion = 0;

	public VITemplateVersion() {
		super();
	}


	public VITemplateVersion(String iVitemplateVersionId, Integer flgStatus,
			Integer sequence, Integer flgDeleted, Integer flgIsDirty,
			String dtCreated, String dtUpdated, String createdBy,
			String updatedBy, Integer iTenantId, Integer flgIsDeviceDirty,
			String strRemarks, String competibleVersion,
			String versionNumber, Integer flgActiveVersion) {
		super();
		this.iVitemplateVersionId = iVitemplateVersionId;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.iTenantId = iTenantId;
		this.flgIsDeviceDirty = flgIsDeviceDirty;
		this.strRemarks = strRemarks;
		this.competibleVersion = competibleVersion;
		this.versionNumber = versionNumber;
		this.flgActiveVersion = flgActiveVersion;
	}



	public String getiVitemplateVersionId() {
		return iVitemplateVersionId;
	}

	public void setiVitemplateVersionId(String iVitemplateVersionId) {
		this.iVitemplateVersionId = iVitemplateVersionId;
	}

	public Integer getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(Integer flgStatus) {
		this.flgStatus = flgStatus;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	
	/*public String getMacId() {
		return macId;
	}


	public void setMacId(String macId) {
		this.macId = macId;
	}*/


	public Integer getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(Integer flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public Integer getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(Integer flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

		
	/*public Integer getiShipId() {
		return iShipId;
	}


	public void setiShipId(Integer iShipId) {
		this.iShipId = iShipId;
	}*/


	public Integer getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(Integer iTenantId) {
		this.iTenantId = iTenantId;
	}

	public Integer getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(Integer flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public String getCompetibleVersion() {
		return competibleVersion;
	}

	public void setCompetibleVersion(String competibleVersion) {
		this.competibleVersion = competibleVersion;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public Integer getFlgActiveVersion() {
		return flgActiveVersion;
	}


	public void setFlgActiveVersion(Integer flgActiveVersion) {
		this.flgActiveVersion = flgActiveVersion;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iVitemplateVersionId == null) ? 0 : iVitemplateVersionId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VITemplateVersion other = (VITemplateVersion) obj;
		if (iVitemplateVersionId == null) {
			if (other.iVitemplateVersionId != null)
				return false;
		} else if (!iVitemplateVersionId.equals(other.iVitemplateVersionId))
			return false;
		return true;
	}

}
