package com.tams.model;

public class SyncHistory {

	private String isyncHistoryId;
	private String dtSyncDate;
	private String logMessage;
	private String progress;
	private String generator;
	private String strFilename;
	private int flgIsDirty;
	private int syncOrderid;
	private int flgDeleted;
	private String dtAcknowledgeDate;
	private String dtGeneratedDate;
	private String lastDownLoadDate;
	private String strMacId;
	private String strRegisterTabletId;
	private String strSyncMode;
	
	private int iTenantId;
	private int iShipId;
	
	public SyncHistory() {
		super();
	}

	public SyncHistory(String isyncHistoryId, String dtSyncDate,
			String logMessage, String progress, String generator,
			String strFilename, int flgIsDirty, int syncOrderid, int iTenantId,
			int iShipId) {
		super();
		this.isyncHistoryId = isyncHistoryId;
		this.dtSyncDate = dtSyncDate;
		this.logMessage = logMessage;
		this.progress = progress;
		this.generator = generator;
		this.strFilename = strFilename;
		this.flgIsDirty = flgIsDirty;
		this.syncOrderid = syncOrderid;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public SyncHistory(String isyncHistoryId, String dtSyncDate,
			String logMessage, String progress, String generator,
			String strFilename, int flgIsDirty, int syncOrderid,
			String dtAcknowledgeDate, String dtGeneratedDate,
			String lastDownLoadDate, String strMacId,
			String strRegisterTabletId, int iTenantId, int iShipId) {
		super();
		this.isyncHistoryId = isyncHistoryId;
		this.dtSyncDate = dtSyncDate;
		this.logMessage = logMessage;
		this.progress = progress;
		this.generator = generator;
		this.strFilename = strFilename;
		this.flgIsDirty = flgIsDirty;
		this.syncOrderid = syncOrderid;
		this.dtAcknowledgeDate = dtAcknowledgeDate;
		this.dtGeneratedDate = dtGeneratedDate;
		this.lastDownLoadDate = lastDownLoadDate;
		this.strMacId = strMacId;
		this.strRegisterTabletId = strRegisterTabletId;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getIsyncHistoryId() {
		return isyncHistoryId;
	}

	public void setIsyncHistoryId(String isyncHistoryId) {
		this.isyncHistoryId = isyncHistoryId;
	}

	public String getDtSyncDate() {
		return dtSyncDate;
	}

	public void setDtSyncDate(String dtSyncDate) {
		this.dtSyncDate = dtSyncDate;
	}

	public String getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public String getGenerator() {
		return generator;
	}

	public void setGenerator(String generator) {
		this.generator = generator;
	}

	public String getStrFilename() {
		return strFilename;
	}

	public void setStrFilename(String strFilename) {
		this.strFilename = strFilename;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public int getSyncOrderid() {
		return syncOrderid;
	}

	public void setSyncOrderid(int syncOrderid) {
		this.syncOrderid = syncOrderid;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public String getDtAcknowledgeDate() {
		return dtAcknowledgeDate;
	}

	public void setDtAcknowledgeDate(String dtAcknowledgeDate) {
		this.dtAcknowledgeDate = dtAcknowledgeDate;
	}

	public String getDtGeneratedDate() {
		return dtGeneratedDate;
	}

	public void setDtGeneratedDate(String dtGeneratedDate) {
		this.dtGeneratedDate = dtGeneratedDate;
	}

	public String getLastDownLoadDate() {
		return lastDownLoadDate;
	}

	public void setLastDownLoadDate(String lastDownLoadDate) {
		this.lastDownLoadDate = lastDownLoadDate;
	}

	public String getStrMacId() {
		return strMacId;
	}

	public void setStrMacId(String strMacId) {
		this.strMacId = strMacId;
	}

	public String getStrRegisterTabletId() {
		return strRegisterTabletId;
	}

	public void setStrRegisterTabletId(String strRegisterTabletId) {
		this.strRegisterTabletId = strRegisterTabletId;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getStrSyncMode() {
		return strSyncMode;
	}

	public void setStrSyncMode(String strSyncMode) {
		this.strSyncMode = strSyncMode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((isyncHistoryId == null) ? 0 : isyncHistoryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncHistory other = (SyncHistory) obj;
		if (isyncHistoryId == null) {
			if (other.isyncHistoryId != null)
				return false;
		} else if (!isyncHistoryId.equals(other.isyncHistoryId))
			return false;
		return true;
	}
}
