package com.tams.model;
/**
 * @since 14-05-2016
 * @author ripunjay
 *
 */
public class UserMaster {

	String iUserId;
	String iTenantId;
	String strUserName;
	String strPassword;
	String strFirstName;
	String strLastName;
	String dtDateOfBirth;
	String txtDescription;
	String strEmail;
	String dtCreated;
	String dtUpdated;
	int flgStatus=0;
	int flgDeleted=0;
	int flgIsDirty=1;
	String iShipHolidayListId;
	String fltMinWorkHourWeekDays;
	String fltMinWorkHourSaturdays;
	String fltMinWorkHourSundays;
	String fltMinWorkHourHolidays;
	String fltOTIncludedInWage;
	String fltOTRatePerHour;
	String flgIsOverTimeEnabled;
	String iRoleId;
	String flgIsWatchkeeper;
	String faxNumber;
	String handPhone;
	String landLineNumber;
	String panNumber;
	String pinCode;
	String addressFirst;
	String addressSecond;
	String addressThird;
	String city;
	String state;
	String iCountryId;
	String strEmployeeNo;
	String strMiddleName;

	Integer rankPriority;
	
	int iShipId = 0;

	public UserMaster() {

	}

	public UserMaster(String iUserId, String iTenantId, String strUserName,
			String strPassword, String strFirstName, String strLastName,
			String dtDateOfBirth, String txtDescription, String strEmail,
			String dtCreated, String dtUpdated, int flgStatus,
			int flgDeleted, int flgIsDirty, String iShipHolidayListId,
			String fltMinWorkHourWeekDays, String fltMinWorkHourSaturdays,
			String fltMinWorkHourSundays, String fltMinWorkHourHolidays,
			String fltOTIncludedInWage, String fltOTRatePerHour,
			String flgIsOverTimeEnabled, String iRoleId,
			String flgIsWatchkeeper, String faxNumber, String handPhone,
			String landLineNumber, String panNumber, String pinCode,
			String addressFirst, String addressSecond, String addressThird,
			String city, String state, String iCountryId, String strEmployeeNo,
			String strMiddleName) {

		this.iUserId = iUserId;
		this.iTenantId = iTenantId;
		this.strUserName = strUserName;
		this.strPassword = strPassword;
		this.strFirstName = strFirstName;
		this.strLastName = strLastName;
		this.dtDateOfBirth = dtDateOfBirth;
		this.txtDescription = txtDescription;
		this.strEmail = strEmail;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iShipHolidayListId = iShipHolidayListId;
		this.fltMinWorkHourWeekDays = fltMinWorkHourWeekDays;
		this.fltMinWorkHourSaturdays = fltMinWorkHourSaturdays;
		this.fltMinWorkHourSundays = fltMinWorkHourSundays;
		this.fltMinWorkHourHolidays = fltMinWorkHourHolidays;
		this.fltOTIncludedInWage = fltOTIncludedInWage;
		this.fltOTRatePerHour = fltOTRatePerHour;
		this.flgIsOverTimeEnabled = flgIsOverTimeEnabled;
		this.iRoleId = iRoleId;
		this.flgIsWatchkeeper = flgIsWatchkeeper;
		this.faxNumber = faxNumber;
		this.handPhone = handPhone;
		this.landLineNumber = landLineNumber;
		this.panNumber = panNumber;
		this.pinCode = pinCode;
		this.addressFirst = addressFirst;
		this.addressSecond = addressSecond;
		this.addressThird = addressThird;
		this.city = city;
		this.state = state;
		this.iCountryId = iCountryId;
		this.strEmployeeNo = strEmployeeNo;
		this.strMiddleName = strMiddleName;

	}

	public UserMaster(String iUserId, String iTenantId, String strUserName,
			String strPassword, String strFirstName, String strLastName,
			String dtDateOfBirth, String txtDescription, String dtCreated,
			String dtUpdated, int flgStatus, int flgDeleted,
			int flgIsDirty, String iRoleId) {
		this.iUserId = iUserId;
		this.iTenantId = iTenantId;
		this.strUserName = strUserName;
		this.strPassword = strPassword;
		this.strFirstName = strFirstName;
		this.strLastName = strLastName;
		this.dtDateOfBirth = dtDateOfBirth;
		this.txtDescription = txtDescription;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iRoleId = iRoleId;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getStrUserName() {
		return strUserName;
	}

	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}

	public String getStrPassword() {
		return strPassword;
	}

	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}

	public String getStrFirstName() {
		return strFirstName;
	}

	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public String getDtDateOfBirth() {
		return dtDateOfBirth;
	}

	public void setDtDateOfBirth(String dtDateOfBirth) {
		this.dtDateOfBirth = dtDateOfBirth;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getStrEmail() {
		return strEmail;
	}

	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getiShipHolidayListId() {
		return iShipHolidayListId;
	}

	public void setiShipHolidayListId(String iShipHolidayListId) {
		this.iShipHolidayListId = iShipHolidayListId;
	}

	public String getFltMinWorkHourWeekDays() {
		return fltMinWorkHourWeekDays;
	}

	public void setFltMinWorkHourWeekDays(String fltMinWorkHourWeekDays) {
		this.fltMinWorkHourWeekDays = fltMinWorkHourWeekDays;
	}

	public String getFltMinWorkHourSaturdays() {
		return fltMinWorkHourSaturdays;
	}

	public void setFltMinWorkHourSaturdays(String fltMinWorkHourSaturdays) {
		this.fltMinWorkHourSaturdays = fltMinWorkHourSaturdays;
	}

	public String getFltMinWorkHourSundays() {
		return fltMinWorkHourSundays;
	}

	public void setFltMinWorkHourSundays(String fltMinWorkHourSundays) {
		this.fltMinWorkHourSundays = fltMinWorkHourSundays;
	}

	public String getFltMinWorkHourHolidays() {
		return fltMinWorkHourHolidays;
	}

	public void setFltMinWorkHourHolidays(String fltMinWorkHourHolidays) {
		this.fltMinWorkHourHolidays = fltMinWorkHourHolidays;
	}

	public String getFltOTIncludedInWage() {
		return fltOTIncludedInWage;
	}

	public void setFltOTIncludedInWage(String fltOTIncludedInWage) {
		this.fltOTIncludedInWage = fltOTIncludedInWage;
	}

	public String getFltOTRatePerHour() {
		return fltOTRatePerHour;
	}

	public void setFltOTRatePerHour(String fltOTRatePerHour) {
		this.fltOTRatePerHour = fltOTRatePerHour;
	}

	public String getFlgIsOverTimeEnabled() {
		return flgIsOverTimeEnabled;
	}

	public void setFlgIsOverTimeEnabled(String flgIsOverTimeEnabled) {
		this.flgIsOverTimeEnabled = flgIsOverTimeEnabled;
	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getFlgIsWatchkeeper() {
		return flgIsWatchkeeper;
	}

	public void setFlgIsWatchkeeper(String flgIsWatchkeeper) {
		this.flgIsWatchkeeper = flgIsWatchkeeper;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getHandPhone() {
		return handPhone;
	}

	public void setHandPhone(String handPhone) {
		this.handPhone = handPhone;
	}

	public String getLandLineNumber() {
		return landLineNumber;
	}

	public void setLandLineNumber(String landLineNumber) {
		this.landLineNumber = landLineNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getAddressFirst() {
		return addressFirst;
	}

	public void setAddressFirst(String addressFirst) {
		this.addressFirst = addressFirst;
	}

	public String getAddressSecond() {
		return addressSecond;
	}

	public void setAddressSecond(String addressSecond) {
		this.addressSecond = addressSecond;
	}

	public String getAddressThird() {
		return addressThird;
	}

	public void setAddressThird(String addressThird) {
		this.addressThird = addressThird;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getiCountryId() {
		return iCountryId;
	}

	public void setiCountryId(String iCountryId) {
		this.iCountryId = iCountryId;
	}

	public Integer getRankPriority() {
		return rankPriority;
	}

	public void setRankPriority(Integer rankPriority) {
		this.rankPriority = rankPriority;
	}

	public String getStrEmployeeNo() {
		return strEmployeeNo;
	}

	public void setStrEmployeeNo(String strEmployeeNo) {
		this.strEmployeeNo = strEmployeeNo;
	}

	public String getStrMiddleName() {
		return strMiddleName;
	}

	public void setStrMiddleName(String strMiddleName) {
		this.strMiddleName = strMiddleName;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

}