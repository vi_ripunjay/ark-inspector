package com.tams.model;

public class VesselInspection {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String iVesselInspectionId = null;
	private String strInspectionTitle = null;
	private String strRemarks = null;
	private int flgStatus = 0;// 1: for pending and 0 for complete
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String inspectionDate = null;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private String inspectedBy;
	private int iTenantId = 0;
	private int iShipId = 0;
	private int flgIsLocked = 0;
	private String iVesselInspectionStatusId;
	private Integer flgReverted = 0;
	private String reviewedBy;
	private String approvedBy;
	private String acceptedBy;
	private String completedBy;
	private Integer flgIsDeviceDirty = 1;
	private Integer flgIsEdited = 1;

	public VesselInspection() {
		super();

	}

	public VesselInspection(String iVesselInspectionId,
			String strInspectionTitle, String strRemarks, int flgStatus,
			int sequence, int flgDeleted, int flgIsDirty,
			String inspectionDate, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, int iTenantId, int iShipId,
			int flgIsLocked, String inspectedBy) {
		super();
		this.iVesselInspectionId = iVesselInspectionId;
		this.strInspectionTitle = strInspectionTitle;
		this.strRemarks = strRemarks;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.inspectionDate = inspectionDate;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.flgIsLocked = flgIsLocked;
		this.inspectedBy = inspectedBy;
	}

	public VesselInspection(String iVesselInspectionId,
			String strInspectionTitle, String strRemarks, int flgStatus,
			int sequence, int flgDeleted, int flgIsDirty,
			String inspectionDate, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, String inspectedBy,
			int iTenantId, int iShipId, int flgIsLocked,
			String iVesselInspectionStatusId, Integer flgReverted,
			String reviewedBy, String approvedBy, String acceptedBy,
			String completedBy, Integer flgIsDeviceDirty, Integer flgIsEdited) {
		super();
		this.iVesselInspectionId = iVesselInspectionId;
		this.strInspectionTitle = strInspectionTitle;
		this.strRemarks = strRemarks;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.inspectionDate = inspectionDate;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.inspectedBy = inspectedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.flgIsLocked = flgIsLocked;
		this.iVesselInspectionStatusId = iVesselInspectionStatusId;
		this.flgReverted = flgReverted;
		this.reviewedBy = reviewedBy;
		this.approvedBy = approvedBy;
		this.acceptedBy = acceptedBy;
		this.completedBy = completedBy;
		this.flgIsDeviceDirty = flgIsDeviceDirty;
		this.flgIsEdited = flgIsEdited;
	}

	public String getiVesselInspectionId() {
		return iVesselInspectionId;
	}

	public void setiVesselInspectionId(String iVesselInspectionId) {
		this.iVesselInspectionId = iVesselInspectionId;
	}

	public String getStrInspectionTitle() {
		return strInspectionTitle;
	}

	public void setStrInspectionTitle(String strInspectionTitle) {
		this.strInspectionTitle = strInspectionTitle;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public int getFlgIsLocked() {
		return flgIsLocked;
	}

	public void setFlgIsLocked(int flgIsLocked) {
		this.flgIsLocked = flgIsLocked;
	}

	public String getInspectedBy() {
		return inspectedBy;
	}

	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}

	public String getiVesselInspectionStatusId() {
		return iVesselInspectionStatusId;
	}

	public void setiVesselInspectionStatusId(String iVesselInspectionStatusId) {
		this.iVesselInspectionStatusId = iVesselInspectionStatusId;
	}

	public Integer getFlgReverted() {
		return flgReverted;
	}

	public void setFlgReverted(Integer flgReverted) {
		this.flgReverted = flgReverted;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getAcceptedBy() {
		return acceptedBy;
	}

	public void setAcceptedBy(String acceptedBy) {
		this.acceptedBy = acceptedBy;
	}

	public String getCompletedBy() {
		return completedBy;
	}

	public void setCompletedBy(String completedBy) {
		this.completedBy = completedBy;
	}

	public Integer getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(Integer flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	public Integer getFlgIsEdited() {
		return flgIsEdited;
	}

	public void setFlgIsEdited(Integer flgIsEdited) {
		this.flgIsEdited = flgIsEdited;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iVesselInspectionId == null) ? 0 : iVesselInspectionId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VesselInspection other = (VesselInspection) obj;
		if (iVesselInspectionId == null) {
			if (other.iVesselInspectionId != null)
				return false;
		} else if (!iVesselInspectionId.equals(other.iVesselInspectionId))
			return false;
		return true;
	}

}
