package com.tams.model;

public class CheckListSectionItems {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String iCheckListSectionItemsId = null;
	private String iCheckListSectionId = null;
	private String strItemNames = null;
	private String strItemConditions = null;
	private String strCommentory = null;
	private String strItemRemarks = null;
	private String flgInspected = null;
	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;

	public CheckListSectionItems() {
		super();

	}

	public CheckListSectionItems(String iCheckListSectionItemsId,
			String iCheckListSectionId, String strItemNames,
			String strItemConditions, String strCommentory,
			String strItemRemarks, String flgInspected, int flgStatus,
			int sequence, int flgDeleted, int flgIsDirty, String createdDate,
			String modifiedDate, String createdBy, String modifiedBy,
			int iTenantId, int iShipId) {
		super();
		this.iCheckListSectionItemsId = iCheckListSectionItemsId;
		this.iCheckListSectionId = iCheckListSectionId;
		this.strItemNames = strItemNames;
		this.strItemConditions = strItemConditions;
		this.strCommentory = strCommentory;
		this.strItemRemarks = strItemRemarks;
		this.flgInspected = flgInspected;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getiCheckListSectionItemsId() {
		return iCheckListSectionItemsId;
	}

	public void setiCheckListSectionItemsId(String iCheckListSectionItemsId) {
		this.iCheckListSectionItemsId = iCheckListSectionItemsId;
	}

	public String getiCheckListSectionId() {
		return iCheckListSectionId;
	}

	public void setiCheckListSectionId(String iCheckListSectionId) {
		this.iCheckListSectionId = iCheckListSectionId;
	}

	public String getStrItemNames() {
		return strItemNames;
	}

	public void setStrItemNames(String strItemNames) {
		this.strItemNames = strItemNames;
	}

	public String getStrItemConditions() {
		return strItemConditions;
	}

	public void setStrItemConditions(String strItemConditions) {
		this.strItemConditions = strItemConditions;
	}

	public String getStrCommentory() {
		return strCommentory;
	}

	public void setStrCommentory(String strCommentory) {
		this.strCommentory = strCommentory;
	}

	public String getStrItemRemarks() {
		return strItemRemarks;
	}

	public void setStrItemRemarks(String strItemRemarks) {
		this.strItemRemarks = strItemRemarks;
	}

	public String getFlgInspected() {
		return flgInspected;
	}

	public void setFlgInspected(String flgInspected) {
		this.flgInspected = flgInspected;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iCheckListSectionItemsId == null) ? 0
						: iCheckListSectionItemsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckListSectionItems other = (CheckListSectionItems) obj;
		if (iCheckListSectionItemsId == null) {
			if (other.iCheckListSectionItemsId != null)
				return false;
		} else if (!iCheckListSectionItemsId
				.equals(other.iCheckListSectionItemsId))
			return false;
		return true;
	}

}
