package com.tams.model;

public class FilledFormImages {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String strFilledFormImageId;
	private String iFilledFormId = null;
	private String iVesselInspectionId = null;
	private String iFormSectionItemId = null;
	private String iFormSectionId = null;
	private String strImagepath;
	private String strRelativeImagePath;
	private String strImageName;
	private String strDesc;
	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;
	private String downLoadPath;
	private int flgIsEdited = 0;
	private int flgIsDeviceDirty = 0;
	
	private String deviceRelativeImagePath;
	private String deviceOriginalImagePath;

	public FilledFormImages() {

	}

	public FilledFormImages(String strFilledFormImageId, String iFilledFormId,
			String iVesselInspectionId, String iFormSectionItemId,
			String iFormSectionId, String strImagepath,
			String strRelativeImagePath, String strImageName, String strDesc,
			int flgStatus, int sequence, int flgDeleted, int flgIsDirty,
			String createdDate, String modifiedDate, String createdBy,
			String modifiedBy, int iTenantId, int iShipId, String downLoadPath,
			int flgIsEdited, int flgIsDeviceDirty,
			String deviceRelativeImagePath, String deviceOriginalImagePath) {
		super();
		this.strFilledFormImageId = strFilledFormImageId;
		this.iFilledFormId = iFilledFormId;
		this.iVesselInspectionId = iVesselInspectionId;
		this.iFormSectionItemId = iFormSectionItemId;
		this.iFormSectionId = iFormSectionId;
		this.strImagepath = strImagepath;
		this.strRelativeImagePath = strRelativeImagePath;
		this.strImageName = strImageName;
		this.strDesc = strDesc;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.downLoadPath = downLoadPath;
		this.flgIsEdited = flgIsEdited;
		this.flgIsDeviceDirty = flgIsDeviceDirty;
		this.deviceRelativeImagePath = deviceRelativeImagePath;
		this.deviceOriginalImagePath = deviceOriginalImagePath;
	}

	public String getStrFilledFormImageId() {
		return strFilledFormImageId;
	}

	public void setStrFilledFormImageId(String strFilledFormImageId) {
		this.strFilledFormImageId = strFilledFormImageId;
	}

	public String getiFilledFormId() {
		return iFilledFormId;
	}

	public void setiFilledFormId(String iFilledFormId) {
		this.iFilledFormId = iFilledFormId;
	}

	public String getiVesselInspectionId() {
		return iVesselInspectionId;
	}

	public void setiVesselInspectionId(String iVesselInspectionId) {
		this.iVesselInspectionId = iVesselInspectionId;
	}

	public String getStrImagepath() {
		return strImagepath;
	}

	public void setStrImagepath(String strImagepath) {
		this.strImagepath = strImagepath;
	}

	public String getStrImageName() {
		return strImageName;
	}

	public void setStrImageName(String strImageName) {
		this.strImageName = strImageName;
	}

	public String getStrDesc() {
		return strDesc;
	}

	public void setStrDesc(String strDesc) {
		this.strDesc = strDesc;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getFlgIsEdited() {
		return flgIsEdited;
	}

	public void setFlgIsEdited(int flgIsEdited) {
		this.flgIsEdited = flgIsEdited;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public String getiFormSectionItemId() {
		return iFormSectionItemId;
	}

	public void setiFormSectionItemId(String iFormSectionItemId) {
		this.iFormSectionItemId = iFormSectionItemId;
	}

	public String getiFormSectionId() {
		return iFormSectionId;
	}

	public void setiFormSectionId(String iFormSectionId) {
		this.iFormSectionId = iFormSectionId;
	}

	public String getStrRelativeImagePath() {
		return strRelativeImagePath;
	}

	public void setStrRelativeImagePath(String strRelativeImagePath) {
		this.strRelativeImagePath = strRelativeImagePath;
	}

	public String getDownLoadPath() {
		return downLoadPath;
	}

	public void setDownLoadPath(String downLoadPath) {
		this.downLoadPath = downLoadPath;
	}

	public int getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}

	public void setFlgIsDeviceDirty(int flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}

	public String getDeviceRelativeImagePath() {
		return deviceRelativeImagePath;
	}

	public void setDeviceRelativeImagePath(String deviceRelativeImagePath) {
		this.deviceRelativeImagePath = deviceRelativeImagePath;
	}

	public String getDeviceOriginalImagePath() {
		return deviceOriginalImagePath;
	}

	public void setDeviceOriginalImagePath(String deviceOriginalImagePath) {
		this.deviceOriginalImagePath = deviceOriginalImagePath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((strFilledFormImageId == null) ? 0 : strFilledFormImageId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilledFormImages other = (FilledFormImages) obj;
		if (strFilledFormImageId == null) {
			if (other.strFilledFormImageId != null)
				return false;
		} else if (!strFilledFormImageId.equals(other.strFilledFormImageId))
			return false;
		return true;
	}

}
