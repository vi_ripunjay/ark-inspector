package com.tams.model;

public class UserServiceTerm {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-14
	 */

	private String iserServiceTermId = null;
	private String userId = null;
	String startDate = null;
	String endDate = null;
	int flgStatus = 0;
	int sequece = 0;
	int isDeleted = 0; // 0 - not deleted & 1 - deleted
	int flgIsDirty = 1;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;

	public UserServiceTerm() {
		super();
		
	}

	public UserServiceTerm(String iserServiceTermId, String userId,
			String startDate, String endDate, int flgStatus, int sequece,
			int isDeleted, int flgIsDirty, String createdDate,
			String modifiedDate, String createdBy, String modifiedBy,
			int iTenantId, int iShipId) {
		super();
		this.iserServiceTermId = iserServiceTermId;
		this.userId = userId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.flgStatus = flgStatus;
		this.sequece = sequece;
		this.isDeleted = isDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getIserServiceTermId() {
		return iserServiceTermId;
	}

	public void setIserServiceTermId(String iserServiceTermId) {
		this.iserServiceTermId = iserServiceTermId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getSequece() {
		return sequece;
	}

	public void setSequece(int sequece) {
		this.sequece = sequece;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iserServiceTermId == null) ? 0 : iserServiceTermId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserServiceTerm other = (UserServiceTerm) obj;
		if (iserServiceTermId == null) {
			if (other.iserServiceTermId != null)
				return false;
		} else if (!iserServiceTermId.equals(other.iserServiceTermId))
			return false;
		return true;
	}

}
