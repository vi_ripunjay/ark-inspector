package com.tams.model;

public class SyncStatus {

	private String iSyncStatusId;
	private String dtSyncDate;
	private String syncMode;
	private String dataSyncMode;//cable or web
	private String serverAddress;	
	private int iTenantId;
	private int flgStatus = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate;
	private String modifiedDate;
	private String createdBy;
	private String modifiedBy;
	
	public SyncStatus() {
		super();
		
	}

	public SyncStatus(String iSyncStatusId, String dtSyncDate, String syncMode,
			String serverAddress, int iTenantId, int flgStatus, int flgDeleted,
			int flgIsDirty, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, String dataSyncMode) {
		super();
		this.iSyncStatusId = iSyncStatusId;
		this.dtSyncDate = dtSyncDate;
		this.syncMode = syncMode;
		this.serverAddress = serverAddress;
		this.iTenantId = iTenantId;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.dataSyncMode = dataSyncMode;
	}

	public String getiSyncStatusId() {
		return iSyncStatusId;
	}

	public void setiSyncStatusId(String iSyncStatusId) {
		this.iSyncStatusId = iSyncStatusId;
	}

	public String getDtSyncDate() {
		return dtSyncDate;
	}

	public void setDtSyncDate(String dtSyncDate) {
		this.dtSyncDate = dtSyncDate;
	}

	public String getSyncMode() {
		return syncMode;
	}

	public void setSyncMode(String syncMode) {
		this.syncMode = syncMode;
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getDataSyncMode() {
		return dataSyncMode;
	}

	public void setDataSyncMode(String dataSyncMode) {
		this.dataSyncMode = dataSyncMode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iSyncStatusId == null) ? 0 : iSyncStatusId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncStatus other = (SyncStatus) obj;
		if (iSyncStatusId == null) {
			if (other.iSyncStatusId != null)
				return false;
		} else if (!iSyncStatusId.equals(other.iSyncStatusId))
			return false;
		return true;
	}
}
