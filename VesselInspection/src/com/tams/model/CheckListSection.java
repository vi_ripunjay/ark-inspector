package com.tams.model;

public class CheckListSection {

	/**
	 * @author Ripujay Shukla
	 * @since 2015-05-11
	 */

	private String iCheckListSectionId = null;
	private String strSectionName = null;
	private String strRemarks = null; //this use as strDescription
	private int flgStatus = 0;
	private int sequence = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private String createdDate = null;
	private String modifiedDate = null;
	private String createdBy = null;
	private String modifiedBy = null;
	private int iTenantId = 0;
	private int iShipId = 0;

	public CheckListSection() {
		super();

	}

	public CheckListSection(String iCheckListSectionId, String strSectionName,
			String strRemarks, int flgStatus, int sequence, int flgDeleted,
			int flgIsDirty, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy, int iTenantId, int iShipId) {
		super();
		this.iCheckListSectionId = iCheckListSectionId;
		this.strSectionName = strSectionName;
		this.strRemarks = strRemarks;
		this.flgStatus = flgStatus;
		this.sequence = sequence;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getiCheckListSectionId() {
		return iCheckListSectionId;
	}

	public void setiCheckListSectionId(String iCheckListSectionId) {
		this.iCheckListSectionId = iCheckListSectionId;
	}

	public String getStrSectionName() {
		return strSectionName;
	}

	public void setStrSectionName(String strSectionName) {
		this.strSectionName = strSectionName;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getStrRemarks() {
		return strRemarks;
	}

	public void setStrRemarks(String strRemarks) {
		this.strRemarks = strRemarks;
	}

	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iCheckListSectionId == null) ? 0 : iCheckListSectionId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckListSection other = (CheckListSection) obj;
		if (iCheckListSectionId == null) {
			if (other.iCheckListSectionId != null)
				return false;
		} else if (!iCheckListSectionId.equals(other.iCheckListSectionId))
			return false;
		return true;
	}

}
