package com.tams.model;

public class RoleTemplate {

	/**
	 * @author pushkar.m
	 */

	private String iRoleTemplateId;
	private String iFormCategoryId;
	private int iroleId;
	private String template_name;
	private String template_code;
	private int flgDeleted = 0; // 0 - not deleted & 1 - deleted
	private int flgIsDirty = 1;
	private String createdDate;
	private String modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private int iTenantId;
	private int sequence;
	private String icons;
	private String iVesselInspectionTemplateId;

	public RoleTemplate() {

	}

	public RoleTemplate(String iRoleTemplateId, String iFormCategoryId,
			int iroleId, String template_name, String template_code,
			int flgDeleted, int flgIsDirty, String createdDate,
			String modifiedDate, String createdBy, String modifiedBy,
			int iTenantId, int sequence, String icons, String iVesselInspectionTemplateId) {
		super();
		this.iRoleTemplateId = iRoleTemplateId;
		this.iFormCategoryId = iFormCategoryId;
		this.iroleId = iroleId;
		this.template_name = template_name;
		this.template_code = template_code;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.iTenantId = iTenantId;
		this.sequence = sequence;
		this.icons = icons;
		this.iVesselInspectionTemplateId = iVesselInspectionTemplateId;
	}

	public String getiRoleTemplateId() {
		return iRoleTemplateId;
	}

	public void setiRoleTemplateId(String iRoleTemplateId) {
		this.iRoleTemplateId = iRoleTemplateId;
	}

	public String getiFormCategoryId() {
		return iFormCategoryId;
	}

	public void setiFormCategoryId(String iFormCategoryId) {
		this.iFormCategoryId = iFormCategoryId;
	}

	public int getIroleId() {
		return iroleId;
	}

	public void setIroleId(int iroleId) {
		this.iroleId = iroleId;
	}

	public String getTemplate_name() {
		return template_name;
	}

	public void setTemplate_name(String template_name) {
		this.template_name = template_name;
	}

	public String getTemplate_code() {
		return template_code;
	}

	public void setTemplate_code(String template_code) {
		this.template_code = template_code;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getIcons() {
		return icons;
	}

	public void setIcons(String icons) {
		this.icons = icons;
	}

	public String getiVesselInspectionTemplateId() {
		return iVesselInspectionTemplateId;
	}

	public void setiVesselInspectionTemplateId(String iVesselInspectionTemplateId) {
		this.iVesselInspectionTemplateId = iVesselInspectionTemplateId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iRoleTemplateId == null) ? 0 : iRoleTemplateId.hashCode());
		result = prime * result + ((icons == null) ? 0 : icons.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleTemplate other = (RoleTemplate) obj;
		if (iRoleTemplateId == null) {
			if (other.iRoleTemplateId != null)
				return false;
		} else if (!iRoleTemplateId.equals(other.iRoleTemplateId))
			return false;
		if (icons == null) {
			if (other.icons != null)
				return false;
		} else if (!icons.equals(other.icons))
			return false;
		return true;
	}

}
