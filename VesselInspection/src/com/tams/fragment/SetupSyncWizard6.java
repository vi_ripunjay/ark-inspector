package com.tams.fragment;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.storage.StorageManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tams.sync.ParseImageSyncService;
import com.tams.sync.ParseXmlService;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;

public class SetupSyncWizard6 extends Fragment {

	public static Context mContext;
	public static ProgressBar progressBarParse;
	public static TextView fileParseComplete;
	private SyncTab adminSyncTab;
	public static ImageView importedImageaView;

	public boolean expectionExist = false;
	Handler handler;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = null;

		mContext = getActivity().getApplicationContext();
		adminSyncTab = new SyncTab();
		adminSyncTab.wizard5.setEnabled(false);

		expectionExist = false;
		try {

			StorageManager storage = (StorageManager) mContext
					.getApplicationContext().getSystemService(
							getActivity().STORAGE_SERVICE);
			Method method = storage.getClass().getDeclaredMethod(
					"disableUsbMassStorage");
			method.setAccessible(true);
			Object r = method.invoke(storage);

			// System.out.println("Storage disable ============================== ripunjay");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Log.i(getTag(),
			// "Storage disable ============error=================="+e.getMessage());
			expectionExist = true;
			// System.out.println("Storage disable ============error================== ripunjay");
		}
		if (!expectionExist) {
			if (fileExist()) {

				rootView = inflater.inflate(R.layout.wizard6, container, false);
				adminSyncTab.wizard5.setTextColor(getResources().getColor(
						R.color.black));

				progressBarParse = (ProgressBar) rootView
						.findViewById(R.id.progressBarParse);
				fileParseComplete = (TextView) rootView
						.findViewById(R.id.fileParseComplete);
				importedImageaView = (ImageView) rootView
						.findViewById(R.id.importedImageaView);

				parse();
				adminSyncTab.fileNotFound = false;
			} else {
				adminSyncTab.fileNotFound = true;
				adminSyncTab.wizard2.callOnClick();

			}
		}
		return rootView;
	}

	public static void parse() {
		SetupSyncWizard6 ast = new SetupSyncWizard6();
		SetupSyncWizard6.SynchronizationParseTask syncParseTask = ast.new SynchronizationParseTask();
		syncParseTask.execute((Void) null);
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationParseTask extends
			AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			showProgress(true);

		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				ParseXmlService pxs = new ParseXmlService(mContext);
				pxs.startParseXmlAfterZip();

			} catch (Exception e) {
				return false;
			}
			
			/*try {

				ParseImageSyncService piss = new ParseImageSyncService(mContext);
				piss.startParseXmlAfterZip();

			} catch (Exception e) {
				return false;
			}*/

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				if(CommonUtil.getSendTemplateData(adminSyncTab.astContext) != null && "1".equalsIgnoreCase(CommonUtil.getSendTemplateData(adminSyncTab.astContext))){
					fileParseComplete
					.setText(adminSyncTab.astContext.getResources().getString(R.string.templateDataMsg));
					fileParseComplete.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.red));
					fileParseComplete.invalidate();
				}
				else{
					fileParseComplete
					.setText("Your device is now up to date and in synch with The Ark system.");
					fileParseComplete.invalidate();
				}
				
				adminSyncTab.wizard5.setEnabled(true);
				adminSyncTab.wizard5.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.white));
				showProgress(false);
				importedImageaView.setVisibility(View.VISIBLE);
				importedImageaView
						.setImageResource(R.drawable.complete_mark_icon);
				importedImageaView.invalidate();

			} else {
				fileParseComplete.setText("Error");
				adminSyncTab.wizard5.setEnabled(false);
				adminSyncTab.wizard5.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.black));
				importedImageaView.setVisibility(View.VISIBLE);
				importedImageaView
						.setImageResource(R.drawable.complete_mark_icon);
				importedImageaView.invalidate();
			}

		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	public boolean fileExist() {
		boolean exist = false;
		String xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
			CommonUtil.setFileNotmatchFlg(adminSyncTab.astContext, "0");
			/**
			 * below work do for parshing correct file.
			 */
			for(String fileName : allzipFileFromXmlProcessPath){
				if(CommonUtil.getGeneratedUuid(adminSyncTab.astContext) != null && !fileName.contains(CommonUtil.getGeneratedUuid(adminSyncTab.astContext))){
					CommonUtil.setFileNotmatchFlg(adminSyncTab.astContext, "1");
					exist = false;
				}
			}
			
		}
		return exist;
	}

	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public void showProgress(boolean start) {
		if (start) {
			progressBarParse.setVisibility(View.VISIBLE);
			fileParseComplete.setVisibility(View.GONE);
		} else if (!start) {

			progressBarParse.setVisibility(View.GONE);
			fileParseComplete.setVisibility(View.VISIBLE);
		}
	}
}
