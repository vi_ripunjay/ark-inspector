package com.tams.fragment;

import java.io.File;
import java.lang.reflect.Method;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tams.sync.GenerateImageSyncService;
import com.tams.sync.GenerateXmlService;
import com.tams.vessalinspection.R;

public class SetupSyncWizard3 extends Fragment {
	
	public static Context mContext;
	public static ProgressBar progressBarGen;
	public static TextView fileGenComplete;
	private SyncTab adminSyncTab;
	public static ImageView generateImage;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.wizard3, container,
				false);
		
		mContext = getActivity().getApplicationContext();
		adminSyncTab = new SyncTab();
		adminSyncTab.wizard2.setEnabled(false);
		adminSyncTab.wizard2.setTextColor(getActivity().getResources().getColor(R.color.black));
		
		progressBarGen = (ProgressBar) rootView.findViewById(R.id.progressBarGen);
		fileGenComplete = (TextView) rootView.findViewById(R.id.fileGenComplete);
		generateImage = (ImageView) rootView.findViewById(R.id.generateImage);
		
		try {

			StorageManager storage = (StorageManager) mContext
					.getApplicationContext().getSystemService(
							getActivity().STORAGE_SERVICE);
			Method method = storage.getClass().getDeclaredMethod(
					"disableUsbMassStorage");
			method.setAccessible(true);
			Object r = method.invoke(storage);

		} catch (Exception e) {			
			e.printStackTrace();			
		}
		
		generate();
		
		
		return rootView;
	}
	
	
	
	public static void generate() {
		SetupSyncWizard3 ssw3 = new SetupSyncWizard3();
		SetupSyncWizard3.SynchronizationTask syncTask = ssw3.new SynchronizationTask();
		syncTask.execute((Void) null);
	}
	
	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			showProgress(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				GenerateXmlService gxs = new GenerateXmlService(mContext);
				gxs.generateXmlForDirtyRecord();

				// Simulate network access.
				Thread.sleep(3000);
			} catch (Exception e) {
				return false;
			}
			
			try {

				GenerateImageSyncService giss = new GenerateImageSyncService(mContext);
				giss.generateXmlForDirtyRecord();

				// Simulate network access.
				Thread.sleep(3000);
			} catch (Exception e) {
				return false;
			}


			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				fileGenComplete.setText("File Generated, Click next to continue...");
				adminSyncTab.wizard2.setEnabled(true);
				adminSyncTab.wizard2.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.white));
				generateImage.setVisibility(View.VISIBLE);
				generateImage.setImageResource(R.drawable.file_generated_icon);
				generateImage.invalidate();
				
			} else {
				fileGenComplete.setText("Error");
				adminSyncTab.wizard2.setEnabled(false);
				adminSyncTab.wizard2.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.black));
				generateImage.setVisibility(View.VISIBLE);
				generateImage.setImageResource(R.drawable.usb_error);
				generateImage.invalidate();
			}
			showProgress(false);
		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}
	
	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public void showProgress(boolean start) {
		if (start) {
			progressBarGen.setVisibility(View.VISIBLE);
			fileGenComplete.setVisibility(View.GONE);
		} else if (!start) {
			
			progressBarGen.setVisibility(View.GONE);
			fileGenComplete.setVisibility(View.VISIBLE);

		}
	}

}
