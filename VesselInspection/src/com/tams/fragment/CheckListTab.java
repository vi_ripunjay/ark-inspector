package com.tams.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;

import com.tams.adapter.CheckListAdapter;
import com.tams.adapter.PinnedHeaderExpListView;
import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.FilledCheckList;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.Simple;

public class CheckListTab extends Fragment {

	// ExpandableListView checkList;
	PinnedHeaderExpListView checkList;
	Button submitButton;
	Intent intentCL;
	public static String iFormCategoryId;
	private int lastClickedPosition = 0;

	// ArrayList<String> groupItem = new ArrayList<String>();
	// ArrayList<Object> childItem = new ArrayList<Object>();

	ArrayList<CheckListSection> checkListSection = new ArrayList<CheckListSection>();
	public static ArrayList<Object> checkListSectionItem = new ArrayList<Object>();
	public static Context clContext;

	public String strViId = "";
	View vesselInspecCL;
	ImageButton floatingButton;
	/**
	 * showAllGroup 0 for all 1 for only one
	 */
	public int showAllGroup = 0;
	public String strCheckListSectionId = null;
	ActionBar actionBar;

	public CheckListTab() {

	}

	public CheckListTab(String iFormCategoryId) {
		this.iFormCategoryId = iFormCategoryId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (savedInstanceState != null) {

		} else {
			showAllGroup = 0;
		}

		intentCL = getActivity().getIntent();
		actionBar = getActivity().getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		String strVesselInspectionId = intentCL
				.getStringExtra("strVesselInspectionId");
		if (strVesselInspectionId == null
				|| "".equalsIgnoreCase(strVesselInspectionId)) {
			strVesselInspectionId = CommonUtil
					.getVesselInspectionId(getActivity());
		}
		strViId = strVesselInspectionId;

		vesselInspecCL = inflater.inflate(R.layout.checklist_frag, container,
				false);
		clContext = getActivity();
		// ((TextView)android.findViewById(R.id.textView)).setText("CheckList");

		checkList = (PinnedHeaderExpListView) vesselInspecCL
				.findViewById(R.id.checkList);
		floatingButton = (ImageButton) vesselInspecCL
				.findViewById(R.id.imageButtonAsFloatingCL);
		if (showAllGroup == 0) {
			floatingButton.setVisibility(View.GONE);
		} else {
			floatingButton.setVisibility(View.VISIBLE);
		}
		/*
		 * submitButton = (Button) vesselInspecCL.findViewById(R.id.btnSubmit);
		 * submitButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { for (Object ci :
		 * checkListSectionItem) { ArrayList<FilledCheckList> cildList =
		 * (ArrayList<FilledCheckList>) ci; for (FilledCheckList fcl : cildList)
		 * { System.out.println("child Title : " + fcl.getFlgChecked());
		 * System.out.println("child flgChecked : " + fcl.getFlgChecked());
		 * System.out.println("child Desc : " + fcl.getStrDesc());
		 * 
		 * DBManager db = new DBManager(getActivity()); db.open();
		 * 
		 * List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
		 * checkList = db.getFilledCheckListDataById(fcl
		 * .getiFilledCheckListId()); if (checkList == null || checkList.size()
		 * == 0) { if (fcl.isFlgInspectedYes()) { fcl.setFlgChecked("yes"); }
		 * else if (fcl.isFlgInspectedNo()) { fcl.setFlgChecked("no"); } else {
		 * fcl.setFlgChecked(null); } db.insertFilldCheckListTable(fcl); } else
		 * { if (fcl.isFlgInspectedYes()) { fcl.setFlgChecked("yes"); } else if
		 * (fcl.isFlgInspectedNo()) { fcl.setFlgChecked("no"); } else {
		 * fcl.setFlgChecked(null); } db.updateFilldCheckListTable(fcl); }
		 * db.close();
		 * 
		 * } }
		 * 
		 * } });
		 */

		if (savedInstanceState == null) {
			setGroupData(strVesselInspectionId, strCheckListSectionId);
		}
		// setChildGroupData();

		/*
		 * CheckListAdapter mNewAdapter = new CheckListAdapter(0,
		 * checkListSection, checkListSectionItem, getActivity());
		 * mNewAdapter.setInflater((LayoutInflater) getActivity()
		 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE), getActivity());
		 * checkList.setAdapter(mNewAdapter);
		 */

		CheckListAdapter mNewAdapter = new CheckListAdapter(showAllGroup,
				checkListSection, checkListSectionItem, getActivity());
		mNewAdapter.setInflater((LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				getActivity());

		checkList.setAdapter(mNewAdapter);
		checkList.setGroupIndicator(null);
		View h = LayoutInflater.from(getActivity()).inflate(R.layout.grouprow,
				(ViewGroup) vesselInspecCL.findViewById(R.id.rootCL), false);
		checkList.setPinnedHeaderView(h);
		checkList.setOnScrollListener((OnScrollListener) mNewAdapter);
		checkList.setDividerHeight(0);

		checkList.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub

				// viFormList.setSelection(groupPosition);
				Boolean shouldExpand = (!checkList
						.isGroupExpanded(groupPosition));
				checkList.collapseGroup(lastClickedPosition);

				/*
				 * if (shouldExpand){ //generateExpandableList();
				 * checkList.expandGroup(groupPosition);
				 * checkList.setSelectionFromTop(groupPosition, 0); }
				 */

				lastClickedPosition = groupPosition;
				// v.setBackgroundColor(getResources().getColor(R.color.green));

				/**
				 * @author ripunjay below work for open single section at a
				 *         time.
				 */
				// =========================== Start
				// ====================================
				String strCheckListSectionId = null;
				if (showAllGroup == 0) {
					showAllGroup = 1;

				}
				if (showAllGroup == 1) {
					floatingButton.setVisibility(View.VISIBLE);
					strCheckListSectionId = checkListSection.get(groupPosition)
							.getiCheckListSectionId();
				} else if (showAllGroup == 2) {
					floatingButton.setVisibility(View.GONE);
					strCheckListSectionId = null;
					saveCheckListData();
				}
				setGroupData(strViId, strCheckListSectionId);

				/*
				 * CheckListAdapter clfAdapter = new CheckListAdapter(
				 * showAllGroup, checkListSection, checkListSectionItem,
				 * getActivity()); clfAdapter.setInflater((LayoutInflater)
				 * getActivity()
				 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				 * getActivity()); checkList.setAdapter(clfAdapter);
				 */

				CheckListAdapter mNewAdapter = new CheckListAdapter(
						showAllGroup, checkListSection, checkListSectionItem,
						getActivity());
				mNewAdapter.setInflater((LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
						getActivity());

				checkList.setAdapter(mNewAdapter);
				checkList.setGroupIndicator(null);
				View h = LayoutInflater.from(getActivity()).inflate(
						R.layout.grouprow,
						(ViewGroup) vesselInspecCL.findViewById(R.id.rootCL),
						false);
				checkList.setPinnedHeaderView(h);
				checkList.setOnScrollListener((OnScrollListener) mNewAdapter);
				checkList.setDividerHeight(0);

				if (showAllGroup == 1) {
					checkList.expandGroup(0);
					checkList.setSelectionFromTop(0, 1);
					showAllGroup = 2;
				} else if (showAllGroup == 2) {
					showAllGroup = 1;

				}
				// =====================end==========================================
				return true;
			}
		});

		floatingButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				floatingButton.setVisibility(View.GONE);

				showAllGroup = 0;
				/*
				 * if (showAllGroup == 1) { //checkList.expandGroup(0);
				 * //checkList.setSelectionFromTop(0, 1); showAllGroup = 2; }
				 * else if (showAllGroup == 2) { showAllGroup = 1;
				 * 
				 * }
				 */

				setGroupData(strViId, null);

				CheckListAdapter mNewAdapter = new CheckListAdapter(
						showAllGroup, checkListSection, checkListSectionItem,
						getActivity());
				mNewAdapter.setInflater((LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
						getActivity());

				checkList.setAdapter(mNewAdapter);
				checkList.setGroupIndicator(null);
				View h = LayoutInflater.from(getActivity()).inflate(
						R.layout.grouprow,
						(ViewGroup) vesselInspecCL.findViewById(R.id.rootCL),
						false);
				checkList.setPinnedHeaderView(h);
				checkList.setOnScrollListener((OnScrollListener) mNewAdapter);
				checkList.setDividerHeight(0);

			}
		});

		// expandbleLis.setOnChildClickListener(this);
		return vesselInspecCL;
	}

	public void setGroupData(String strVesselInspectionId,
			String strCheckListSectionId) {
		/*
		 * groupItem.add("TechNology"); groupItem.add("Mobile");
		 * groupItem.add("Manufacturer"); groupItem.add("Extras");
		 */

		checkListSection = new ArrayList<CheckListSection>();
		checkListSectionItem = new ArrayList<Object>();

		DBManager db = new DBManager(getActivity());
		db.open();
		List<CheckListSection> csList = new ArrayList<CheckListSection>();

		if (strCheckListSectionId != null && !"".equals(strCheckListSectionId)) {
			csList = db.getCheckListSectionDataById(strCheckListSectionId);
			intentCL.putExtra("viTitle", csList.get(0).getStrSectionName());
		} else {
			csList = db.getCheckListSectionData();
		}

		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (csList != null && csList.size() > 0) {

			for (CheckListSection cs : csList) {
				if (cs.getFlgDeleted() == 0) {
					checkListSection.add(cs);

					/**
					 * load form section item
					 */
					List<CheckListSectionItems> clsiList = new ArrayList<CheckListSectionItems>();
					// need to change method
					clsiList = db.getCheckListSectionItemsDataBySectionId(cs
							.getiCheckListSectionId());

					List<FilledCheckList> filedCheckListBySecList = new ArrayList<FilledCheckList>();
					int seq = 1;
					if (clsiList != null) {
						for (CheckListSectionItems clsSecitm : clsiList) {
							if (clsSecitm.getFlgDeleted() == 0) {
								/**
								 * get filled checkList value on the basis of
								 * vesselInspection, checklistsection and
								 * checklistsectionitrm
								 */
								List<FilledCheckList> fldCheckList = new ArrayList<FilledCheckList>();
								fldCheckList = db
										.getFilledCheckListDataByIds(
												strVesselInspectionId,
												cs.getiCheckListSectionId(),
												clsSecitm
														.getiCheckListSectionItemsId());

								if (fldCheckList != null
										&& fldCheckList.size() > 0) {

									filedCheckListBySecList
											.addAll(fldCheckList);

								} else {
									/*
									 * FilledCheckList filledCheckList = new
									 * FilledCheckList( getPk(),
									 * strVesselInspectionId,
									 * clsSecitm.getiCheckListSectionItemsId(),
									 * cs.getiCheckListSectionId(), null,
									 * false,false, "", "strRemarks", 0, seq, 0,
									 * 1, format.format(cudate),
									 * format.format(cudate), "createdBy",
									 * "modifiedBy", Integer.parseInt(CommonUtil
									 * .getTenantId(clContext)), 0);
									 */
									FilledCheckList filledCheckList = new FilledCheckList(
											getPk(),
											strVesselInspectionId,
											clsSecitm
													.getiCheckListSectionItemsId(),
											cs.getiCheckListSectionId(), null,
											false, false, "", "", 0, seq, 0, 1,
											format.format(cudate), format
													.format(cudate), CommonUtil
													.getUserId(clContext),
											CommonUtil.getUserId(clContext),
											Integer.parseInt(CommonUtil
													.getTenantId(clContext)),
											Integer.parseInt(CommonUtil
													.getShipId(clContext)));
									filedCheckListBySecList
											.add(filledCheckList);
								}

								seq++;
							}
						}
					}

					checkListSectionItem.add(filedCheckListBySecList);
				}
			}
		}

		db.close();

	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getActivity()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	public void setChildGroupData() {
		/**
		 * Add Data For TecthNology
		 */
		ArrayList<Simple> child = new ArrayList<Simple>();

		child.add(new Simple("Java", false, ""));
		child.add(new Simple(".Net", false, ""));
		child.add(new Simple("PHP", false, ""));
		child.add(new Simple("Drupal", false, ""));
		// childItem.add(child);

		/**
		 * Add Data For Mobile
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Android", true, "test"));
		child.add(new Simple("Window Mobile", false, ""));
		child.add(new Simple("iPHone", true, "ok"));
		child.add(new Simple("Blackberry", false, ""));
		// childItem.add(child);
		/**
		 * Add Data For Manufacture
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("HTC", true, ""));
		child.add(new Simple("Apple", false, ""));
		child.add(new Simple("Samsung", true, "ripunjay"));
		child.add(new Simple("Nokia", false, ""));
		// childItem.add(child);
		/**
		 * Add Data For Extras
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Contact Us", false, ""));
		child.add(new Simple("About Us", true, ""));
		child.add(new Simple("Location", true, "my will"));
		child.add(new Simple("Root Cause", false, ""));
		// childItem.add(child);
	}

	@SuppressWarnings("unchecked")
	public void saveCheckListData() {

		DBManager db = new DBManager(clContext);
		db.open();

		for (Object ci : checkListSectionItem) {
			ArrayList<FilledCheckList> cildList = (ArrayList<FilledCheckList>) ci;
			for (FilledCheckList fcl : cildList) {
				System.out.println("child Title : " + fcl.getFlgChecked());
				System.out.println("child flgChecked : " + fcl.getFlgChecked());
				System.out.println("child Desc : " + fcl.getStrDesc());

				/*
				 * List<FilledCheckList> checkList = new
				 * ArrayList<FilledCheckList>(); checkList =
				 * db.getFilledCheckListDataById(fcl.getiFilledCheckListId());
				 * if(checkList == null || checkList.size() == 0){
				 * if(fcl.isFlgInspectedYes()!= null && fcl.isFlgInspectedYes()
				 * == true){ fcl.setFlgChecked("yes"); }else
				 * if(fcl.isFlgInspectedNo() != null && fcl.isFlgInspectedNo()
				 * == true){ fcl.setFlgChecked("no"); } else
				 * if(fcl.isFlgInspectedYes() == null && fcl.isFlgInspectedNo()
				 * == null){ fcl.setFlgChecked(null); }
				 * db.insertFilldCheckListTable(fcl); } else{
				 * if(fcl.isFlgInspectedYes() !=null && fcl.isFlgInspectedYes()
				 * == true){ fcl.setFlgChecked("yes"); }else
				 * if(fcl.isFlgInspectedNo() != null && fcl.isFlgInspectedNo()
				 * == true){ fcl.setFlgChecked("no"); } else
				 * if(fcl.isFlgInspectedYes() == null && fcl.isFlgInspectedNo()
				 * == null){ fcl.setFlgChecked(null); }
				 * db.updateFilldCheckListTable(fcl); }
				 */

			}
		}
		db.close();
	}
}
