package com.tams.fragment;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.storage.StorageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.LoginActivity;
import com.tams.vessalinspection.MyBroadcast;
import com.tams.vessalinspection.R;

public class SyncTab extends Fragment {
	Context context;
	ActionBar actionBar;
	View view;
	public static Context astContext;

	public static Button btnSync;
	public static ImageView roundedImage;
	public static Animation animRound;
	public static LinearLayout synclayOut;
	private ProgressBar mProgress;
	public static TextView syncMessages;
	public static boolean setTime = false;

	public static MyBroadcast myBroadcast;

	TextView textSync;
	public static Button start, wizard1, wizard2, wizard3, wizard4, wizard5,
			cancel;

	public static boolean fileNotFound = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		context = getActivity();
		View rootView = inflater.inflate(R.layout.sync_tab, container, false);
		view = rootView;

		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		CommonUtil.setMacID(context.getApplicationContext(),
				(tm.getDeviceId() != null ? tm.getDeviceId() : "tmpdvid"));
		
		astContext = getActivity().getApplicationContext();
		isConnected(astContext);

		setTime = false;
		fileNotFound = false;

		start = (Button) rootView.findViewById(R.id.start);
		wizard1 = (Button) rootView.findViewById(R.id.button1);
		wizard2 = (Button) rootView.findViewById(R.id.button2);
		wizard3 = (Button) rootView.findViewById(R.id.button3);
		wizard4 = (Button) rootView.findViewById(R.id.button4);
		wizard5 = (Button) rootView.findViewById(R.id.button5);
		cancel = (Button) rootView.findViewById(R.id.cancel);

		textSync = (TextView) rootView.findViewById(R.id.textSync);
		selectFrag(rootView);

		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					
				/**
				 * Place for ship validation
				 */
				boolean loginfromShip = false;
				if(CommonUtil.getLoginFromShip(astContext) != null && "1".equalsIgnoreCase(CommonUtil.getLoginFromShip(astContext))){
					loginfromShip = true;
				}
				
				if((CommonUtil.getSyncShipId(astContext) != null && !"".equals(CommonUtil.getSyncShipId(astContext))) || loginfromShip){
				
					/*String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
					if (dataSyncMode != null
							&& "cable".equalsIgnoreCase(dataSyncMode)) {*/
						selectFrag(v);
						start.setVisibility(View.GONE);
						wizard1.setVisibility(View.VISIBLE);
						wizard2.setVisibility(View.GONE);
						wizard3.setVisibility(View.GONE);
						wizard4.setVisibility(View.GONE);
						wizard5.setVisibility(View.GONE);
						cancel.setVisibility(View.VISIBLE);
						textSync.setText("Step 1 of 4");
					/*} else {
						*//**
						 * Nothing to do hear.
						 *//*
						launchPopUpForNoSync(
								getResources().getString(
										R.string.cableSyncNotPossible), 0);
	
					}*/
				}else{
					StartSync.shipError.setVisibility(View.VISIBLE);
				}
			}
		});

		wizard1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.VISIBLE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.VISIBLE);
				textSync.setText("Step 2 of 4");

			}
		});

		wizard2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.VISIBLE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.VISIBLE);
				textSync.setText("Step 3 of 4");

				wizard4.setEnabled(false);
				wizard4.setTextColor(getResources().getColor(R.color.black));
				wizard4.invalidate();
			}
		});

		wizard4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.GONE);
				textSync.setText("Step 4 of 4");

			}
		});
		wizard5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.VISIBLE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				textSync.setText("");

				if(!(CommonUtil.getSendTemplateData(context) != null && "1".equalsIgnoreCase(CommonUtil.getSendTemplateData(context))))
				{
					/**
					 * After finish call refresh activity.
					 */
					//Intent i = new Intent(getActivity(), VesselInspectionStartActivity.class);
					Intent i = new Intent(getActivity(), LoginActivity.class);
					startActivity(i);
					getActivity().finish();
				}
				
				/*
				 * Intent i = new Intent(getActivity(),
				 * AdminMainActivity.class); startActivity(i);
				 * 
				 * getActivity().finish();
				 */

			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectFrag(v);
				start.setVisibility(View.VISIBLE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.GONE);
				textSync.setText("");

				fileNotFound = false;				
				//CommonUtil.setLoginFromShip(context, "0");
				CommonUtil.setActiveInspectionID(context, null);
				CommonUtil.setCallingThrough(context, null);

				try {
					disableUms();
				} catch (Exception ex) {
				}
			}
		});

		return rootView;
	}

	public void disableUms() {
		try {

			StorageManager storage = (StorageManager) astContext
					.getApplicationContext().getSystemService(
							getActivity().STORAGE_SERVICE);
			Method method = storage.getClass().getDeclaredMethod(
					"disableUsbMassStorage");
			method.setAccessible(true);
			Object r = method.invoke(storage);

			// System.out.println("Storage disable ============================== ripunjay");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			// System.out.println("Storage disable ============error================== ripunjay");
		}
	}

	public static void generate() {
		SyncTab ast = new SyncTab();
		SyncTab.SynchronizationTask syncTask = ast.new SynchronizationTask();
		syncTask.execute((Void) null);
	}

	public static void fileCheckingTask() {
		SyncTab ast = new SyncTab();
		syncMessages.setText("Waiting for file.");
		SyncTab.SynchronizationFileCheckTask syncFileCheckTask = ast.new SynchronizationFileCheckTask();
		syncFileCheckTask.execute((Void) null);
	}

	public static void parse() {
		SyncTab ast = new SyncTab();
		syncMessages.setText("Waiting for file.");
		SyncTab.SynchronizationParseTask syncParseTask = ast.new SynchronizationParseTask();
		syncParseTask.execute((Void) null);
	}

	public static Handler handler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == 1) {
				generate();
			} else if (msg.what == 2) {
				fileCheckingTask();
			} else if (msg.what == 3) {
				parse();
			}

			return true;
		}
	});

	public void selectFrag(View view) {
		System.out
				.println("............................................................");

		Fragment fr;
		if (view == this.view.findViewById(R.id.start)) {
			System.out.println("1");
			fr = new SetupSyncWizard2();

		} else if (view == this.view.findViewById(R.id.button1)) {
			System.out.println("2");
			fr = new SetupSyncWizard3();
			// generate();

		} else if (view == this.view.findViewById(R.id.button2)) {
			System.out.println("3");
			// fr = new SetupSyncWizard4();

			fr = new SetupSyncWizard5();

		} else if (view == this.view.findViewById(R.id.button3)) {
			System.out.println("4");
			fr = new SetupSyncWizard5();
			// fileCheckingTask();

		} else if (view == this.view.findViewById(R.id.button4)) {
			System.out.println("5");
			fr = new SetupSyncWizard6();

			// parse();

		} else if (view == this.view.findViewById(R.id.button5)) {
			System.out.println("6");
			fr = new StartSync();
		} else if (view == this.view.findViewById(R.id.cancel)) {
			System.out.println("6");

			fr = new StartSync();
		} else {
			System.out.println("7");
			fr = new StartSync();
		}

		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_container, fr);
		fragmentTransaction.commit();

	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages.setText("Generate is in progress.....");
			showProgress(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				/*
				 * GenerateXmlService gxs = new GenerateXmlService(astContext);
				 * gxs.generateXmlForDirtyRecord();
				 */
				// Simulate network access.
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				return false;
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {

			} else {

			}

			Message message = new Message();
			message.what = 2;
			handler.sendMessage(message);
		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationParseTask extends
			AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages
					.setText("File has been received and files are parsing...");
			showProgress(true);

		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				/*
				 * ParseXmlService pxs = new ParseXmlService(astContext);
				 * pxs.startParseXmlAfterZip();
				 */

			} catch (Exception e) {
				return false;
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				showProgress(false);
				syncMessages.setText("Synchronization is completed.");
			} else {

			}

		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationFileCheckTask extends
			AsyncTask<Void, Void, Boolean> {

		int counter = 0;
		boolean fileExist = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages.setText("Waiting for file.");
			showProgress(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				do {
					if (fileExist()) {
						syncMessages.setText("File has been received.");
						fileExist = true;
					} else {
						fileExist = false;
					}
					if (counter > 100) {
						break;
					}
					counter++;
					Thread.sleep(3000);
				} while (!fileExist);

			} catch (InterruptedException e) {
				return false;
			}

			// TODO: register the new account here.
			return fileExist;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				showProgress(true);
				syncMessages.setText("File has been received.");

				Message message = new Message();
				message.what = 3;
				handler.sendMessage(message);
			} else {

				syncMessages.setText("File not found and time will be exceed.");
				showProgress(false);
			}

		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	public boolean fileExist() {
		boolean exist = false;
		String xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/theark/rec/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public void showProgress(boolean start) {
		if (start) {
			btnSync.setVisibility(View.GONE);
			synclayOut.setVisibility(View.VISIBLE);
			roundedImage.setVisibility(View.VISIBLE);
			syncMessages.setVisibility(View.VISIBLE);
			// start the animation
			roundedImage.startAnimation(animRound);
		} else if (!start) {
			animRound.cancel();
			roundedImage.setVisibility(View.GONE);
			synclayOut.setVisibility(View.GONE);
			syncMessages.setVisibility(View.GONE);
			btnSync.setVisibility(View.VISIBLE);

		}
	}

	@SuppressWarnings("deprecation")
	public static boolean isConnected(Context context) {
		myBroadcast = new MyBroadcast();
		IntentFilter inte = new IntentFilter();
		inte.addAction(Intent.ACTION_BATTERY_CHANGED);
		inte.addAction(Intent.ACTION_POWER_CONNECTED);
		inte.addAction(Intent.ACTION_POWER_DISCONNECTED);
		Intent intent = context.registerReceiver(myBroadcast, inte);

		int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		return plugged == BatteryManager.BATTERY_PLUGGED_AC
				|| plugged == BatteryManager.BATTERY_PLUGGED_USB;
	}

	@Override
	public void onStop() {
		super.onStop();
		try {
			if (myBroadcast != null) {
				astContext.unregisterReceiver(myBroadcast);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForNoSync(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

}
