package com.tams.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tams.adapter.AdminTabsPagerAdapter;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;

public class AdminFragment extends Fragment {

	ViewPager Tab;
	AdminTabsPagerAdapter TabAdapter;
	ActionBar actionBar;
	private FragmentActivity myContext;
	int tabvalue = 0;

	// Sync Info 0->date , 1->mode , 2->serverAddress
	String syncInfo[] = new String[3];
	public static boolean isAdminFragment = false;
	public static String homeOrStart = "";

	// Mandatory Constructor
	public AdminFragment() {
	}

	public AdminFragment(int i) {
		tabvalue = i;
	}

	@Override
	public void onAttach(Activity activity) {
		myContext = (FragmentActivity) activity;
		super.onAttach(activity);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater
				.inflate(R.layout.admin_pager, container, false);

		getActivity().getActionBar().setTitle("Admin");
		TabAdapter = new AdminTabsPagerAdapter(getChildFragmentManager());

		Tab = (ViewPager) rootView.findViewById(R.id.pager_admin);

		Tab.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {

				actionBar = getActivity().getActionBar();
				actionBar.setSelectedNavigationItem(position);
			}
		});

		Tab.setAdapter(TabAdapter);

		actionBar = getActivity().getActionBar();
		// Enable Tabs on Action Bar
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabReselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

				Tab.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub

			}
		};

		actionBar.removeAllTabs();
		if (actionBar.getTabCount() == 0) {
			// Add New Tab
			if (CommonUtil.getLoginFromShip(myContext) != null
					&& "1".equalsIgnoreCase(CommonUtil
							.getLoginFromShip(myContext))) {
				tabvalue=1;

			}
			actionBar.addTab(actionBar
					.newTab()
					.setText(
							myContext.getResources().getString(
									R.string.frgTabAdmin))
					.setTabListener(tabListener));
			actionBar.addTab(actionBar
					.newTab()
					.setText(
							myContext.getResources().getString(
									R.string.frgTabSync))
					.setTabListener(tabListener));
			actionBar.setSelectedNavigationItem(tabvalue);

		}
		return rootView;
	}

}