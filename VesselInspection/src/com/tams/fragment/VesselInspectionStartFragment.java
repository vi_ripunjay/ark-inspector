package com.tams.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.tams.adapter.ShipListAdapter;
import com.tams.adapter.VesselInspectionDraftAdapter;
import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.FilledCheckList;
import com.tams.model.FilledForm;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.model.RoleTemplate;
import com.tams.model.ShipMaster;
import com.tams.model.VIAlertTime;
import com.tams.model.VesselInspection;
import com.tams.model.VesselInspectionFormListDesc;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.ShipSelectItem;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.VessalInspectionHome;
import com.tams.vessalinspection.VesselInspectionStartActivity;

public class VesselInspectionStartFragment extends Fragment {

	ActionBar actionBar;
	View startInspec;
	Context mContext;

	private Button startInspection;
	List<ShipMaster> shipList;
	Spinner shipMasterDropDown;
	EditText inspectionDate;
	private DatePickerDialog datePickerDialog;
	private SimpleDateFormat dateFormatter;
	private TextView welcomeText;
	private ImageView undoImage;
	private TextView errorShipSelectView;
	private LinearLayout linearLayoutDrafted;
	private LinearLayout linearLayout2;
	private View seperatorVi;
	private ListView draftedViList;

	private FragmentActivity myContext;
	Intent intentStart;
	private String strVesselInspectionOldId = "";
	public static VesselInspection lastActiveInspection;
	Handler handler;

	public VesselInspectionStartFragment() {
		super();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		intentStart = getActivity().getIntent();

		actionBar = getActivity().getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		startInspec = inflater.inflate(
				R.layout.activity_vessal_inspection_home, container, false);
		mContext = getActivity();
		CommonUtil.setShipID(mContext, "");

		welcomeText = (TextView) startInspec.findViewById(R.id.wecomeText);
		undoImage = (ImageView) startInspec.findViewById(R.id.undoImage);
		errorShipSelectView = (TextView) startInspec
				.findViewById(R.id.erroShipSelect);
		errorShipSelectView.setVisibility(View.GONE);

		linearLayoutDrafted = (LinearLayout) startInspec
				.findViewById(R.id.linearLayoutDrafted);
		linearLayout2 = (LinearLayout) startInspec
				.findViewById(R.id.linearLayout2);
		seperatorVi = (View) startInspec.findViewById(R.id.seperatorVi);
		draftedViList = (ListView) startInspec.findViewById(R.id.draftedViList);

		strVesselInspectionOldId = intentStart
				.getStringExtra("strVesselInspectionId");

		DBManager db = new DBManager(getActivity());
		db.open();
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		/**
		 * Get active vesselInspection and get their dirty count and alert generated.
		 */
		List<VesselInspection> vslActiveList = db.getVesselInspectionDataByflgStatus(0);
		if(vslActiveList != null && vslActiveList.size() > 0){
			for(VesselInspection vslInsp : vslActiveList){		
				long dirtyCount = db.getVesselInspectionDirtyData(vslInsp.getiVesselInspectionId());
				if(dirtyCount > 0){
					List<VIAlertTime> viAlertTimeList = new ArrayList<VIAlertTime>();
					viAlertTimeList = db.getVIAlertTimeData();
					if(viAlertTimeList != null && viAlertTimeList.size() > 0){
						VIAlertTime viAlertTime = new VIAlertTime();						
						viAlertTime = viAlertTimeList.get(0);
						if((curDate.getTime() - Long.parseLong(viAlertTime.getAlertTime())) >= 43200000){
							
							viAlertTime.setAlertTime(""+curDate.getTime());
							viAlertTime.setDtUpdated(format.format(curDate));
							viAlertTime.setUpdatedBy(CommonUtil.getUserId(mContext));
							viAlertTime.setFlgIsDirty(1);						
							db.updateVIAlertTimeTable(viAlertTime);
							CommonUtil.launchPopUpForDirtyAlert(getResources().getString(R.string.dirtyAlertString), mContext);
						}
					}
					else{
						VIAlertTime viAlertTime = new VIAlertTime();
						viAlertTime.setiVIAlertTimeId(getPk());
						viAlertTime.setAlertTime(""+curDate.getTime());
						viAlertTime.setStrMacId(CommonUtil.getMacId(mContext));
						viAlertTime.setDtCreated(format.format(curDate));
						viAlertTime.setDtUpdated(format.format(curDate));
						viAlertTime.setCreatedBy(CommonUtil.getUserId(mContext));
						viAlertTime.setUpdatedBy(CommonUtil.getUserId(mContext));
						viAlertTime.setiTenantId((CommonUtil.getTenantId(mContext) != null && !"".equalsIgnoreCase(CommonUtil.getTenantId(mContext)) ? Integer.parseInt(CommonUtil.getTenantId(mContext)) : 0));
						
						db.insertVIAlertTimeTable(viAlertTime);
					}
				}
			}
		}
		
		
		List<VesselInspection> vinsp = new ArrayList<VesselInspection>();
		vinsp = db.getVesselInspectionDataById(strVesselInspectionOldId);

		if (vinsp != null && vinsp.size() > 0) {
			VesselInspection vi = new VesselInspection();

			if (vi.getFlgStatus() == 0)
				undoImage.setVisibility(View.GONE);
		}
		db.close();

		if (strVesselInspectionOldId != null
				&& !"".equals(strVesselInspectionOldId)) {

			if (vinsp != null && vinsp.size() > 0
					&& vinsp.get(0).getFlgDeleted() > 0)
				welcomeText.setText(getActivity().getResources().getString(
						R.string.deleteSuccessMsg));
			else
				welcomeText.setText(getActivity().getResources().getString(
						R.string.congratulationTxt));
			if (VesselInspectionStartActivity.undoIconVisibleOrNot == 1) {
				welcomeText.setVisibility(View.GONE);
			} else {
				welcomeText.setVisibility(View.GONE);
			}
			// undoImage.setVisibility(View.VISIBLE);

			if (VesselInspectionStartActivity.miActionProgressItem != null)
				VesselInspectionStartActivity.miActionProgressItem
						.setVisible(true);

		}

		/**
		 * Load vesselInspection which have flagStatus=0 i.e. in draft mode
		 * while this exist more than one in db.
		 */
		// DBManager db = new DBManager(getActivity());
		db.open();
		List<VesselInspection> viDraftList = new ArrayList<VesselInspection>();
		// viDraftList = db.getVesselInspectionData();
		viDraftList = db.getVesselInspectionDataWithinStatus(3);
		if (viDraftList != null && viDraftList.size() >= 1) {
			linearLayoutDrafted.setVisibility(View.VISIBLE);
			seperatorVi.setVisibility(View.VISIBLE);

			if (lastActiveInspection != null) {
				// viDraftList.set(0, lastActiveInspection);
				if (viDraftList.indexOf(lastActiveInspection) != 0) {
					if (viDraftList.contains(lastActiveInspection)) {
						int i = viDraftList.indexOf(lastActiveInspection);
						Collections.swap(viDraftList, i, 0);
					}
				}
				lastActiveInspection = null;
			}
			
			int index = 0;
			for(VesselInspection vi : viDraftList){
				if(vi.getFlgStatus() == 0){
					if(index != 0){
						viDraftList.remove(index);
						viDraftList.add(0, vi);
					}
					break;
				}
				index++;
			}

			VesselInspectionDraftAdapter viDftAdpt = new VesselInspectionDraftAdapter(
					viDraftList, mContext, getFragmentManager());
			draftedViList.setAdapter(viDftAdpt);
		} else {
			linearLayoutDrafted.setVisibility(View.GONE);
			seperatorVi.setVisibility(View.GONE);
		}

		db.close();
		draftedViList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				VesselInspection vesselInsp = (VesselInspection) draftedViList
						.getItemAtPosition(position);
				if (vesselInsp != null
						&& vesselInsp.getiVesselInspectionId() != null) {

					if (vesselInsp.getFlgStatus() >= 0) {

						/**
						 * below comment code is unnecessary. 
						 */
						/*
						DBManager db1 = new DBManager(getActivity());
						db1.open();
						vesselInsp.setFlgIsDirty(0);
						db1.updateVesselInspectionTable(vesselInsp);
						db1.close();
						*/

						CommonUtil.setTenantID(getActivity(),
								String.valueOf(vesselInsp.getiTenantId()));
						CommonUtil.setShipID(getActivity(),
								String.valueOf(vesselInsp.getiShipId()));
						CommonUtil.setVesselInspectionId(getActivity(),
								vesselInsp.getiVesselInspectionId());

						Intent i = new Intent(getActivity(),
								VessalInspectionHome.class);
						i.putExtra("strVesselInspectionId",
								vesselInsp.getiVesselInspectionId());
						startActivity(i);
						getActivity().finish();
					}
				}
			}
		});

		/**
		 * Write code list view scroll bcoz parent scroll active then listview
		 * scroll of.
		 */
		draftedViList.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					// Disallow ScrollView to intercept touch events.
					v.getParent().requestDisallowInterceptTouchEvent(true);
					break;

				case MotionEvent.ACTION_UP:
					// Allow ScrollView to intercept touch events.
					v.getParent().requestDisallowInterceptTouchEvent(false);
					break;
				}

				// Handle ListView touch events.
				v.onTouchEvent(event);
				return true;
			}
		});

		startInspection = (Button) startInspec.findViewById(R.id.loginButton);
		startInspection.setText(getResources().getString(R.string.startInspection));
		startInspection.setEnabled(true);
		startInspection.setTextColor(getResources().getColor(R.color.white));
		
		shipMasterDropDown = (Spinner) startInspec.findViewById(R.id.shipName);
		inspectionDate = (EditText) startInspec
				.findViewById(R.id.inspectionDate);
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

		inspectionDate.setText(dateFormatter.format(new Date()));
		inspectionDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setDateField();
			}
		});

		/**
		 * Data for testing Check List :
		 */
		TelephonyManager tm = (TelephonyManager) getActivity()
				.getSystemService(Context.TELEPHONY_SERVICE);
		CommonUtil.setMacID(mContext,
				(tm.getDeviceId() != null ? tm.getDeviceId() : "tmpdvid"));

		// DBManager db = new DBManager(mContext);
		db.open();
		shipList = new ArrayList<ShipMaster>();
		shipList = db.getShipMasterData();

		ArrayList<ShipSelectItem> shipSelectlist = new ArrayList<ShipSelectItem>();
		// shipSelectlist.add(new ShipSelectItem("Select Ship", "Select Ship",
		// "Select Ship"));
		shipSelectlist.add(new ShipSelectItem("Select Ship", "0", "0"));
		if (shipList != null && shipList.size() > 0) {
			for (ShipMaster sm : shipList) {
				String shipId = String.valueOf(sm.getiShipId());
				String tenantId = String.valueOf(sm.getiTenantId());
				String shipName = sm.getStrShipName();
				shipSelectlist.add(new ShipSelectItem(shipName, shipId,
						tenantId));
			}
		}

		ShipListAdapter myAdapter = new ShipListAdapter(shipSelectlist,
				mContext);

		// shipMasterDropDown.setPopupBackgroundResource(R.drawable.spinner);
		shipMasterDropDown.setAdapter(myAdapter);
		shipMasterDropDown.setSelection(0, true);

		shipMasterDropDown
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int position, long arg3) {

						// String message = "";
						String shipid = "";
						ShipSelectItem mydata;
						if (!(shipMasterDropDown.getSelectedItem() == null)) {
							mydata = (ShipSelectItem) shipMasterDropDown
									.getSelectedItem();
							shipid = mydata.getUserId();
							// if(shipid != null && !("0").equals(shipid)){
							if (Integer.parseInt(shipid) > 0) {
								CommonUtil.setShipID(mContext, shipid);
								errorShipSelectView.setVisibility(View.GONE);
							} else {
								CommonUtil.setShipID(mContext, null);
							}

							// System.out.println("Value : " + shipid);
						}
						// String text = shipMasterDropDown.getSelectedItem()
						// .toString();

					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
						// errorShipSelectView.setVisibility(View.GONE);
					}
				});

		startInspection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startInspection.setText(getResources().getString(R.string.creatingInspection));
				startInspection.setEnabled(false);
				startInspection.setTextColor(getResources().getColor(R.color.black));
				startInspection.invalidate();
				handler = new Handler();
				Runnable runnable = new Runnable() {
					
					public void run() {
						createInspectionByStart();
					}
				};
				handler.postDelayed(runnable, 1000);
				
			}
		});

		return startInspec;
	}
	
	private void createInspectionByStart(){


		CommonUtil.setActiveInspectionID(mContext, null);
		CommonUtil.setCallingThrough(mContext, null);
		
		if (CommonUtil.getShipId(mContext) != null
				&& !"".equals(CommonUtil.getShipId(mContext))) {

			/**
			 * insert record in vesselInspection
			 */

			String inspecdateArr[] = inspectionDate.getText()
					.toString().split("-");
			String inspecDate = inspecdateArr[2] + "-"
					+ inspecdateArr[1] + "-" + inspecdateArr[0];

			Date curDate = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			DBManager db1 = new DBManager(mContext);
			db1.open();

			/*
			 * List<VesselInspection> vesselInspDoneList = db1
			 * .getVesselInspectionDataByflgStatusDateAndShipId(1,
			 * CommonUtil.getShipId(mContext), inspecDate);
			 */
			List<VesselInspection> vesselInspDoneList = db1
					.getVesselInspectionDataByflgStatusDateAndShipId(3,
							CommonUtil.getShipId(mContext), inspecDate);
			
			db1.close();

			if (vesselInspDoneList != null
					&& vesselInspDoneList.size() > 0) {

				// Give message to user that request to unlock
				// inspection on web to edit it
				if (vesselInspDoneList.get(0).getFlgStatus() == -1) {
					launchPopUp(
							getActivity().getResources().getString(
									R.string.existingInspectionMsg)
									+ getActivity()
											.getResources()
											.getString(
													R.string.existingInspectionMsg2),
							1);
				} else {
					launchPopUp(
							getActivity().getResources().getString(
									R.string.existingInspectionMsg), 1);
				}

			} else {
				
				
				StringBuffer dirtyInspection = new StringBuffer();
				int dirtyCounter = 0;
				int syncCounter = 1; 
				try 
				{
					DBManager db = new DBManager(mContext);
					db.open();
					List<VesselInspection> vslActiveList = db.getVesselInspectionDataByflgStatus(0);
					if(vslActiveList != null && vslActiveList.size() > 0){
						for(VesselInspection vslInsp : vslActiveList){
							if(vslInsp.getFlgIsEdited() < 5){
								syncCounter = 0;
							}
							else{
								syncCounter = 1;
							}
							long dirtyCount = db.getVesselInspectionDirtyData(vslInsp.getiVesselInspectionId());
							if(dirtyCount > 0){
								
								String shipName = db.getShipNameById(vslInsp.getiShipId());
								if(!(dirtyInspection.indexOf(shipName) > 0)){
									if(dirtyCounter > 0){
										dirtyInspection.append(","+shipName);
									}
									else{
										dirtyInspection.append(""+shipName);
									}
								}
								dirtyCounter++;
							}
						}
					}
					
					db.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(dirtyCounter > 0){ 
					String withoutSyncMsg="";
					if(syncCounter == 0){
						withoutSyncMsg = getResources().getString(R.string.withoutSyncMsg);
					}
					launchPopUpForDiryVslData(
							getResources().getString(
									R.string.dirtyVIMsg1)
									+ dirtyInspection.toString()
									+ "\n"
									+ getResources().getString(
											R.string.dirtyVIMsg2)
									+ "\n" + withoutSyncMsg, 0, inspecDate);
				}
				else{
					
					/**
					 * Before create new inspection need to delete undirty 
					 * data from device.
					 */
					CommonUtil.deleteAllRelatedDataOfUndirtyInspection(mContext);							
					
					DBManager db = new DBManager(mContext);
					db.open();
					
					String strVesselInspectionId = getPk();

					List<VesselInspection> vesselInspDraftList = db
							.getVesselInspectionDataByflgStatusDateAndShipId(
									0, CommonUtil.getShipId(mContext),
									inspecDate);
					if (vesselInspDraftList != null
							&& vesselInspDraftList.size() > 0) {
						strVesselInspectionId = vesselInspDraftList.get(0)
								.getiVesselInspectionId();
						VesselInspection vslIns = vesselInspDraftList
								.get(0);
						vslIns.setFlgIsDirty(1);
						db.updateVesselInspectionTable(vslIns);
					} else {
						
						String version = "4.2";
						try {
							PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(
									mContext.getPackageName(), 0);
							if (pInfo != null && pInfo.versionName != null) {
								version = pInfo.versionName;
							} else {
								version = "2.0";
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						VesselInspection vslInsp = new VesselInspection(
								strVesselInspectionId,
								"strInspectionTitle", version, 0, 1,
								0, 1, inspecDate, format.format(curDate),
								format.format(curDate), CommonUtil
										.getUserId(getActivity()),
								CommonUtil.getUserId(getActivity()),
								Integer.parseInt(CommonUtil
										.getTenantId(mContext)), Integer
										.parseInt(CommonUtil
												.getShipId(mContext)), 0,
								CommonUtil.getUserId(getActivity()));

						db.insertVesselInspectionTable(vslInsp);
						
						insertRelatedData(strVesselInspectionId, Integer
								.parseInt(CommonUtil.getRoleId(mContext)));

					}

					db.close();
					CommonUtil.setVesselInspectionId(mContext,
							strVesselInspectionId);

					Intent i = new Intent(getActivity(),
							VessalInspectionHome.class);
					i.putExtra("strVesselInspectionId",
							strVesselInspectionId);
					startActivity(i);
					getActivity().finish();
					
				}
			}

		} else {
			errorShipSelectView.setVisibility(View.VISIBLE);
			startInspection.setText(getResources().getString(R.string.startInspection));
			startInspection.setEnabled(true);
			startInspection.setTextColor(getResources().getColor(R.color.white));
			startInspection.invalidate();
		}
	
	}
	
	private void createInspectionByClearData(String inspecDate){
		/**
		 * Before create new inspection need to delete undirty 
		 * data from device.
		 */
		CommonUtil.deleteAllRelatedDataOfDirtyInspection(mContext);	
		
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");				
		DBManager db = new DBManager(mContext);
		db.open();
		
		String strVesselInspectionId = getPk();

		List<VesselInspection> vesselInspDraftList = db
				.getVesselInspectionDataByflgStatusDateAndShipId(
						0, CommonUtil.getShipId(mContext),
						inspecDate);
		if (vesselInspDraftList != null
				&& vesselInspDraftList.size() > 0) {
			strVesselInspectionId = vesselInspDraftList.get(0)
					.getiVesselInspectionId();
			VesselInspection vslIns = vesselInspDraftList
					.get(0);
			vslIns.setFlgIsDirty(1);
			db.updateVesselInspectionTable(vslIns);
		} else {
			
			String version = "4.2";
			try {
				PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(
						mContext.getPackageName(), 0);
				if (pInfo != null && pInfo.versionName != null) {
					version = pInfo.versionName;
				} else {
					version = "2.0";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			VesselInspection vslInsp = new VesselInspection(
					strVesselInspectionId,
					"strInspectionTitle", version, 0, 1,
					0, 1, inspecDate, format.format(curDate),
					format.format(curDate), CommonUtil
							.getUserId(getActivity()),
					CommonUtil.getUserId(getActivity()),
					Integer.parseInt(CommonUtil
							.getTenantId(mContext)), Integer
							.parseInt(CommonUtil
									.getShipId(mContext)), 0,
					CommonUtil.getUserId(getActivity()));

			db.insertVesselInspectionTable(vslInsp);
			
			insertRelatedData(strVesselInspectionId, Integer
					.parseInt(CommonUtil.getRoleId(mContext)));

		}

		db.close();
		CommonUtil.setVesselInspectionId(mContext,
				strVesselInspectionId);

		Intent i = new Intent(getActivity(),
				VessalInspectionHome.class);
		i.putExtra("strVesselInspectionId",
				strVesselInspectionId);
		startActivity(i);
		getActivity().finish();
	}
	
	/**
	 * blank filled form created in db.
	 */
	private void insertRelatedData(String vesselInspectionId, int roleId) {

		DBManager dbManager = new DBManager(getActivity());
		dbManager.open();
		List<RoleTemplate> templateList = dbManager
				.getRoleTemplateDataByRoleId(roleId);

		List<FormSection> fsList = new ArrayList<FormSection>();
		List<CheckListSection> clsList = new ArrayList<CheckListSection>();
		for (RoleTemplate rt : templateList) {
			if (rt.getTemplate_code() != null
					&& !"".equals(rt.getTemplate_code())
					&& !"_checkList".equalsIgnoreCase(rt.getTemplate_code())) {
				if (rt.getiFormCategoryId() != null
						&& !"".equals(rt.getiFormCategoryId())
						&& rt.getFlgDeleted() == 0) {
					fsList = dbManager.getFormSectionData(rt
							.getiFormCategoryId());
					Date cudate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (fsList != null && fsList.size() > 0) {

						for (FormSection fs : fsList) {

							if (fs.getFlgDeleted() == 0) {
								List<FormSectionItem> fsiList = new ArrayList<FormSectionItem>();
								
								
								if(fs.getFlgStatus() == 3){
									
									fsiList = dbManager
											.getShipFormSectionItemDataBySectionId(fs
													.getiFormSectionId(), Integer.parseInt(CommonUtil.getShipId(getActivity())));
								}else{
									
									fsiList = dbManager
											.getFormSectionItemDataBySectionId(fs
													.getiFormSectionId());
								}
								
								
								
								// formSectionItem.add(fsiList);
								List<FilledForm> filedFormBySecList = new ArrayList<FilledForm>();
								int seq = 1;
								if (fsiList != null) {
									for (FormSectionItem frmSecitm : fsiList) {
										if (frmSecitm.getFlgDeleted() == 0) {
											/**
											 * get filled checkList value on the
											 * basis of vesselInspection,
											 * formsection and formsectionitrm
											 */
											List<FilledForm> fldFrmList = new ArrayList<FilledForm>();
											fldFrmList = dbManager
													.getFilledFormDataByIds(
															vesselInspectionId,
															fs.getiFormSectionId(),
															frmSecitm
																	.getiFormSectionItemId());
											if (fldFrmList == null
													|| fldFrmList.size() == 0) {
												FilledForm filledForm = new FilledForm();
												filledForm
														.setiFilledFormId(getPk());
												filledForm
														.setiVesselInspectionId(vesselInspectionId);
												filledForm.setiFormSectionId(fs
														.getiFormSectionId());
												filledForm
														.setiFormSectionItemId(frmSecitm
																.getiFormSectionItemId());
												filledForm.setStrHeaderDesc("");
												filledForm.setStrFooterDesc("");
												filledForm.setSequence(seq);
												filledForm
														.setCreatedDate(format
																.format(cudate));
												filledForm
														.setModifiedDate(format
																.format(cudate));
												filledForm
														.setiTenantId(Integer
																.parseInt(CommonUtil
																		.getTenantId(getActivity())));
												filledForm
														.setiShipId(Integer
																.parseInt(CommonUtil
																		.getShipId(getActivity())));
												filledForm
														.setCreatedBy(CommonUtil
																.getUserId(getActivity()));
												filledForm
														.setModifiedBy(CommonUtil
																.getUserId(getActivity()));
												filledForm.setFlgIsDirty(1);

												/**
												 * Insert blank form in device.
												 */
												dbManager
														.insertFilldFormTable(filledForm);
												
												/**
												 * Pushkar : Insert VesselInspectionFormListDesc for both header&footer . Firstly check for existence 
												 * 
												 */
												VesselInspectionFormListDesc viflDescHeader = new VesselInspectionFormListDesc();
												
												viflDescHeader.setCreatedBy(CommonUtil
														.getUserId(getActivity()));
												viflDescHeader.setCreatedDate(format
														.format(cudate));
												viflDescHeader.setFlgDeleted(0);
												viflDescHeader.setFlgIsDeviceDirty(0);
												viflDescHeader.setFlgIsDirty(1);
												viflDescHeader.setFlgIsEdited(0);
												viflDescHeader.setFlgStatus(0);
												viflDescHeader.setiFilledFormId(filledForm.getiFilledFormId());
												viflDescHeader.setiFormSectionId(filledForm.getiFormSectionId());
												viflDescHeader.setiFormSectionItemId(filledForm.getiFormSectionItemId());
												viflDescHeader.setiVesselInspectionFormListDescId(getPk());
												viflDescHeader.setiShipId(Integer
														.parseInt(CommonUtil
																.getShipId(getActivity())));
												viflDescHeader.setiTenantId(Integer
														.parseInt(CommonUtil
																.getTenantId(getActivity())));
												viflDescHeader.setiVesselInspectionId(filledForm.getiVesselInspectionId());
												viflDescHeader.setModifiedBy(CommonUtil
														.getUserId(getActivity()));
												viflDescHeader.setModifiedDate(format
														.format(cudate));
												viflDescHeader.setSequence(filledForm.getSequence());
												viflDescHeader.setStrDescType(CommonUtil.DESC_TYPE_HEADER);
												viflDescHeader.setStrDesc("");
												
												dbManager.insertVIFLDesc(viflDescHeader);
												
												if("_form".equalsIgnoreCase(rt.getTemplate_code()))
												{												
													VesselInspectionFormListDesc viflDescFooter = new VesselInspectionFormListDesc();
													
													viflDescFooter.setCreatedBy(CommonUtil
															.getUserId(getActivity()));
													viflDescFooter.setCreatedDate(format
															.format(cudate));
													viflDescFooter.setFlgDeleted(0);
													viflDescFooter.setFlgIsDeviceDirty(0);
													viflDescFooter.setFlgIsDirty(1);
													viflDescFooter.setFlgIsEdited(0);
													viflDescFooter.setFlgStatus(0);
													viflDescFooter.setiFilledFormId(filledForm.getiFilledFormId());
													viflDescFooter.setiFormSectionId(filledForm.getiFormSectionId());
													viflDescFooter.setiFormSectionItemId(filledForm.getiFormSectionItemId());
													viflDescFooter.setiVesselInspectionFormListDescId(getPk());
													viflDescFooter.setiShipId(Integer
															.parseInt(CommonUtil
																	.getShipId(getActivity())));
													viflDescFooter.setiTenantId(Integer
															.parseInt(CommonUtil
																	.getTenantId(getActivity())));
													viflDescFooter.setiVesselInspectionId(filledForm.getiVesselInspectionId());
													viflDescFooter.setModifiedBy(CommonUtil
															.getUserId(getActivity()));
													viflDescFooter.setModifiedDate(format
															.format(cudate));
													viflDescFooter.setSequence(filledForm.getSequence());
													viflDescFooter.setStrDescType(CommonUtil.DESC_TYPE_FOOTER);
													viflDescFooter.setStrDesc("");
													
													dbManager.insertVIFLDesc(viflDescFooter);
												}
												
												
												//Used for Color condition
												VesselInspectionFormListDesc viflColorCondition = new VesselInspectionFormListDesc();
												
												viflColorCondition.setCreatedBy(CommonUtil
														.getUserId(getActivity()));
												viflColorCondition.setCreatedDate(format
														.format(cudate));
												viflColorCondition.setFlgDeleted(0);
												viflColorCondition.setFlgIsDeviceDirty(0);
												viflColorCondition.setFlgIsDirty(1);
												viflColorCondition.setFlgIsEdited(1);
												viflColorCondition.setFlgStatus(0);
												viflColorCondition.setiFilledFormId(filledForm.getiFilledFormId());
												viflColorCondition.setiFormSectionId(filledForm.getiFormSectionId());
												viflColorCondition.setiFormSectionItemId(filledForm.getiFormSectionItemId());
												viflColorCondition.setiVesselInspectionFormListDescId(getPk());
												viflColorCondition.setiShipId(Integer
														.parseInt(CommonUtil
																.getShipId(getActivity())));
												viflColorCondition.setiTenantId(Integer
														.parseInt(CommonUtil
																.getTenantId(getActivity())));
												viflColorCondition.setiVesselInspectionId(filledForm.getiVesselInspectionId());
												viflColorCondition.setModifiedBy(CommonUtil
														.getUserId(getActivity()));
												viflColorCondition.setModifiedDate(format
														.format(cudate));
												viflColorCondition.setSequence(filledForm.getSequence());
												viflColorCondition.setStrDescType(CommonUtil.COLOR_CONDITION);
												viflColorCondition.setStrDesc("");
												
												dbManager.insertVIFLDesc(viflColorCondition);
												
												
											}

											seq++;
										}
									}
								}
							}
						}
					}
				}
			} else if (rt.getTemplate_code() != null
					&& !"".equals(rt.getTemplate_code())
					&& "_checkList".equalsIgnoreCase(rt.getTemplate_code())) {
				if (rt.getiFormCategoryId() != null
						&& !"".equals(rt.getiFormCategoryId())
						&& rt.getFlgDeleted() == 0) {
					clsList = dbManager.getCheckListSectionData();
					Date cudate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (clsList != null && clsList.size() > 0) {

						for (CheckListSection cls : clsList) {
							if (cls.getFlgDeleted() == 0) {
								List<CheckListSectionItems> clsiList = new ArrayList<CheckListSectionItems>();
								clsiList = dbManager
										.getCheckListSectionItemsDataBySectionId(cls
												.getiCheckListSectionId());
								// formSectionItem.add(fsiList);
								List<FilledCheckList> filedCheckListBySecList = new ArrayList<FilledCheckList>();
								int seq = 1;
								if (clsiList != null) {
									for (CheckListSectionItems clSecitm : clsiList) {
										if (clSecitm.getFlgDeleted() == 0) {
											/**
											 * get filled checkList value on the
											 * basis of vesselInspection,
											 * formsection and formsectionitrm
											 */
											List<FilledCheckList> fldCheckList = new ArrayList<FilledCheckList>();
											fldCheckList = dbManager
													.getFilledCheckListDataByIds(
															vesselInspectionId,
															cls.getiCheckListSectionId(),
															clSecitm.getiCheckListSectionItemsId());

											if (fldCheckList == null
													|| fldCheckList.size() == 0) {

												FilledCheckList filledCheckList = new FilledCheckList(
														getPk(),
														vesselInspectionId,
														clSecitm.getiCheckListSectionItemsId(),
														cls.getiCheckListSectionId(),
														null,
														false,
														false,
														"",
														"",
														0,
														seq,
														0,
														1,
														format.format(cudate),
														format.format(cudate),
														CommonUtil
																.getUserId(getActivity()),
														CommonUtil
																.getUserId(getActivity()),
														Integer.parseInt(CommonUtil
																.getTenantId(getActivity())),
														Integer.parseInt(CommonUtil
																.getShipId(getActivity())));

												filledCheckList
														.setFlgIsDirty(1);

												/**
												 * Insert blank form in device.
												 */
												dbManager
														.insertFilldCheckListTable(filledCheckList);
											}

											seq++;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		dbManager.close();
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(mContext) + "_" + curDate.getTime();

		return pkId;
	}

	private void setDateField() {
		// System.out.println("hello  i am here");
		Calendar newCalendar = Calendar.getInstance();
		datePickerDialog = new DatePickerDialog(getActivity(),
				new OnDateSetListener() {

					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						inspectionDate.setText(dateFormatter.format(newDate
								.getTime()));
					}

				}, newCalendar.get(Calendar.YEAR),
				newCalendar.get(Calendar.MONTH),
				newCalendar.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUp(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				startInspection.setText(getResources().getString(R.string.startInspection));
				startInspection.setEnabled(true);
				startInspection.setTextColor(getResources().getColor(R.color.white));
				startInspection.invalidate();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				startInspection.setText(getResources().getString(R.string.startInspection));
				startInspection.setEnabled(true);
				startInspection.setTextColor(getResources().getColor(R.color.white));
				startInspection.invalidate();
				
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForDiryVslData(String Message, final int position, final String inspecDate) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_dirty);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button sync = (Button) dialog.findViewById(R.id.sync);

		sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				dialog.dismiss();
				startInspection.setText(getResources().getString(R.string.startInspection));
				startInspection.setEnabled(true);
				startInspection.setTextColor(getResources().getColor(R.color.white));
				startInspection.invalidate();
		
				}
		});
		Button logOut = (Button) dialog.findViewById(R.id.logOut);
		logOut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				dialog.dismiss();
				handler = new Handler();
				Runnable runnable = new Runnable() {
					
					public void run() {
						createInspectionByClearData(inspecDate);
					}
				};
				handler.postDelayed(runnable, 1000);
								
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}
}
