package com.tams.fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tams.model.SyncStatus;
import com.tams.model.VesselInspection;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.LoginActivity;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.SyncHandler;
import com.tams.vessalinspection.VessalInspectionHome;
import com.tams.vessalinspection.VesselInspectionStartActivity;

public class AdminTab extends Fragment {

	/**
	 * @author Ripunjay.s
	 */
	Context context;

	Button changeUser;

	Button changeSoapUrl;

	EditText newWsdlUrl;
	Button saveWsdlUrl;
	Button resetData;

	TextView versionNumber;
	TextView versionNumberTitle;
	TextView wsdlPreUrl;
	public static TextView syncDate;
	LinearLayout wsdlUrlLayout;
	LinearLayout changeServerLayout;
	LinearLayout dataAccessModeLayout;
	LinearLayout wipeDataLayout;
	LinearLayout exportDataLayout, importDataLayout, dirtyDataLayout,
			appVersionLinearLayout, dirtyImageDataLayout;
	VessalInspectionHome vesselInspectionHome;
	LoginActivity loginActivity;
	ActionBar actionBar;
	Button syncMasterData, exportData, dirtyData, dirtyImageData, importData,
			resizeImgData;
	TextView updatetemplatData;
	View switchServerSepetor, wipeDataSepetor, exportDataSepetor,
			dirtyDataSepetor, dirtyImageDataSepetor, importDataSepetor;
	TextView wrongWsdl;

	Button setAccessMode;
	RadioButton wifiButton;
	RadioButton mobileDataButton;
	RadioGroup radioGroup;
	CheckBox wifiOnly;
	TextView fName, lName;

	private int counter = 0;
	private long oldTime = 0;
	private long newTime = 0;

	// Sync Info 0->date , 1->mode , 2->serverAddress
	String syncInfo[] = new String[3];

	public static boolean isAdminFragment = false;
	public static String homeOrStart = "";

	public AdminTab() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		context = getActivity();
		actionBar = getActivity().getActionBar();
		// actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		// getActivity().invalidateOptionsMenu();

		View rootView = inflater.inflate(R.layout.admin_fragment, container,
				false);

		fName = (TextView) rootView.findViewById(R.id.fName);
		lName = (TextView) rootView.findViewById(R.id.lName);
		syncDate = (TextView) rootView.findViewById(R.id.syncDate);
		versionNumber = (TextView) rootView.findViewById(R.id.versionNumberId);
		versionNumberTitle = (TextView) rootView
				.findViewById(R.id.versionNumberTitle);

		PackageInfo pInfo = null;
		String version = "";
		try {
			pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (pInfo != null && pInfo.versionName != null) {
			version = pInfo.versionName;
		} else {
			version = "2.0";
		}

		versionNumber.setText(version);
		syncInfo = getSyncInfo();

		syncDate.setText(syncInfo[0]);

		fName.setText(CommonUtil.getUserFName(context));
		lName.setText(CommonUtil.getUserLName(context));

		changeUser = (Button) rootView.findViewById(R.id.changeUserId);
		resetData = (Button) rootView.findViewById(R.id.resetData);
		changeSoapUrl = (Button) rootView.findViewById(R.id.changeSoapUrl);
		newWsdlUrl = (EditText) rootView.findViewById(R.id.wsdlUrl);

		updatetemplatData = (TextView) rootView
				.findViewById(R.id.updatetemplatData);
		switchServerSepetor = (View) rootView
				.findViewById(R.id.switchServerSepetor);
		changeServerLayout = (LinearLayout) rootView
				.findViewById(R.id.changeServerLayout);
		wipeDataSepetor = (View) rootView.findViewById(R.id.wipeDataSepetor);
		wipeDataLayout = (LinearLayout) rootView
				.findViewById(R.id.wipeDataLayout);

		exportDataSepetor = (View) rootView
				.findViewById(R.id.exportDataSepetor);
		importDataSepetor = (View) rootView
				.findViewById(R.id.importDataSepetor);

		exportDataLayout = (LinearLayout) rootView
				.findViewById(R.id.exportDataLayout);

		importDataLayout = (LinearLayout) rootView
				.findViewById(R.id.importDataLayout);

		appVersionLinearLayout = (LinearLayout) rootView
				.findViewById(R.id.appVersionLinearLayout);

		dirtyDataSepetor = (View) rootView.findViewById(R.id.dirtyDataSepetor);
		dirtyDataLayout = (LinearLayout) rootView
				.findViewById(R.id.dirtyDataLayout);

		dirtyImageDataSepetor = (View) rootView
				.findViewById(R.id.dirtyImageDataSepetor);
		dirtyImageDataLayout = (LinearLayout) rootView
				.findViewById(R.id.dirtyImageDataLayout);

		wrongWsdl = (TextView) rootView.findViewById(R.id.wrongWsdlId);
		/*
		 * if ("Dev".equalsIgnoreCase(getActivity().getResources().getString(
		 * R.string.app_mode))) {
		 * changeServerLayout.setVisibility(View.VISIBLE); } else {
		 * changeServerLayout.setVisibility(View.GONE); }
		 */

		resizeImgData = (Button) rootView.findViewById(R.id.resizeImgData);

		exportData = (Button) rootView.findViewById(R.id.exportData);
		importData = (Button) rootView.findViewById(R.id.importData);
		dirtyData = (Button) rootView.findViewById(R.id.dirtyData);
		dirtyImageData = (Button) rootView.findViewById(R.id.dirtyImageData);
		saveWsdlUrl = (Button) rootView.findViewById(R.id.saveWsdlUrl);

		syncMasterData = (Button) rootView.findViewById(R.id.syncMasterData);

		wifiButton = (RadioButton) rootView.findViewById(R.id.wifi);
		mobileDataButton = (RadioButton) rootView.findViewById(R.id.mobileData);

		radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup1);

		wifiOnly = (CheckBox) rootView.findViewById(R.id.wiFiOnly);

		if ("Cable".equalsIgnoreCase(syncInfo[3])) {
			wifiButton.setChecked(true);
		}
		if ("Web".equalsIgnoreCase(syncInfo[3])) {
			mobileDataButton.setChecked(true);
		}

		if (syncInfo[1] != null && syncInfo[1].equals("WiFi"))
			wifiOnly.setChecked(true);
		else
			wifiOnly.setChecked(false);

		wsdlPreUrl = (TextView) rootView.findViewById(R.id.preWsdlUrl);

		wsdlPreUrl.setText(syncInfo[2]);

		wsdlUrlLayout = (LinearLayout) rootView.findViewById(R.id.wsdlLayer);
		dataAccessModeLayout = (LinearLayout) rootView
				.findViewById(R.id.dataAccessModeLayout);

		counter = 0;
		appVersionLinearLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				counter++;
				if (counter == 6) {

					launchPopUpForConfirmation(context.getResources()
							.getString(R.string.adminFunctionUseInfoMsg), 0);
					/*
					 * changeServerLayout.setVisibility(View.VISIBLE);
					 * switchServerSepetor.setVisibility(View.VISIBLE);
					 * wipeDataLayout.setVisibility(View.VISIBLE);
					 * wipeDataSepetor.setVisibility(View.VISIBLE);
					 * exportDataLayout.setVisibility(View.VISIBLE);
					 * exportDataSepetor.setVisibility(View.VISIBLE);
					 * dirtyDataSepetor.setVisibility(View.VISIBLE);
					 * dirtyDataLayout.setVisibility(View.VISIBLE);
					 */
				}
				newTime = new Date().getTime();
				if (oldTime == 0 || counter == 1) {
					oldTime = newTime;
				}
				if ((newTime - oldTime) > 5000) {
					counter = 0;
				} else {
					oldTime = newTime;
				}

			}
		});

		changeUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					DBManager db = new DBManager(context);
					db.open();
					// db.dropDb();

					/* Pushkar : Check Dirty Data . If exists , inform user */

					List<VesselInspection> dirtyVIList = db
							.getDirtyVesselInspection();
					List<String> shipList = new ArrayList<String>();
					StringBuffer strBuf = new StringBuffer();

					if (dirtyVIList != null && dirtyVIList.size() > 0) {

						CommonUtil.setLastActiveShip(context,
								CommonUtil.getShipId(context));

						strBuf = new StringBuffer();
						// /String shipName = "";
						for (VesselInspection vi : dirtyVIList) {

							CommonUtil.setShipID(context,
									String.valueOf(vi.getiShipId()));
							if (!shipList.contains(db.getShipNameById(vi
									.getiShipId())))
								shipList.add(db.getShipNameById(vi.getiShipId()));

							/*
							 * shipName = db.getShipNameById(vi.getiShipId());
							 * if(strBuf.indexOf(shipName) <= 0)
							 * strBuf.append(shipName+ " , ");
							 */
						}
						for (String sName : shipList)
							strBuf.append(sName + " , ");

					} /*
					 * else { db.dropDb(); deleteRecursive(new File(Environment
					 * .getExternalStorageDirectory() + "/theark/vi"));
					 * LoginActivity.isNewUser = true; Intent intent = new
					 * Intent(getActivity(), LoginActivity.class);
					 * startActivity(intent); getActivity().finish(); }
					 */
					/**
					 * Tested.
					 */
					if (strBuf != null && !"".equals(strBuf.toString())) {
						launchPopUpForDiryViData(
								context.getResources().getString(
										R.string.dirtyVIMsg1)
										+ strBuf.substring(0,
												strBuf.length() - 2)
										+ "\n"
										+ context.getResources().getString(
												R.string.dirtyVIMsg2), 0);
					} else {
						launchPopUpForDiryViData(context.getResources()
								.getString(R.string.dirtyVIMsg2), 0);
					}

					db.close();

					/*
					 * deleteRecursive(new File(Environment
					 * .getExternalStorageDirectory() + "/theark/vi"));
					 */

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// LoginActivity.isNewUser = true;
				/*
				 * Intent intent = new Intent(getActivity(),
				 * LoginActivity.class); startActivity(intent);
				 * getActivity().finish();
				 */
			}
		});

		changeSoapUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (wsdlUrlLayout.getVisibility() == View.GONE)
					wsdlUrlLayout.setVisibility(View.VISIBLE);
				else
					wsdlUrlLayout.setVisibility(View.GONE);

				wrongWsdl.setVisibility(View.GONE);
			}
		});

		saveWsdlUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String preUrl = getResources().getString(R.string.preUrl);
				String urlPort = getResources().getString(R.string.portUrl);
				String postUrl = getResources().getString(R.string.postUrl);

				CommonUtil.setServerAddress(context, "");
				String url = newWsdlUrl.getText().toString();
				if (url != null && !"".equals(url.trim())) {
					if (url != null && !"".equals(url)
							&& !url.contains("TheArk-TheArk")) {
						if (!url.contains(preUrl))
							url = preUrl + url;
						if (url.contains("thearkbis.com")) {
							// urlPort = "80";
						} else {
							urlPort = getResources()
									.getString(R.string.portUrl);
							;
							if (!url.contains(urlPort))
								url = url + ":" + urlPort;
						}

						url = url + postUrl;
					}
					CommonUtil.setServerAddress(context, url);
					CommonUtil.updateServerAddress(context, url);
					;
					wsdlUrlLayout.setVisibility(View.GONE);

					wsdlPreUrl.setText(url);

					if (CommonUtil.getServerAddress(context) != null
							&& !"".equals(CommonUtil.getServerAddress(context))) {
						wsdlPreUrl.setText(CommonUtil.getServerAddress(context)
								.toString());
					}

					newWsdlUrl.setText("");

				} else {
					wrongWsdl.setVisibility(View.VISIBLE);
				}
			}
		});

		radioGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						RadioButton rb = (RadioButton) group
								.findViewById(checkedId);

						if (null != rb && checkedId > -1) {
							String mode = rb.getText().toString();
							// updateAccessMode(mode);
							/**
							 * need to update mode for cable or web.
							 */
							updateDataSyncMode(mode);
						}

					}
				});

		wifiOnly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (wifiOnly.isChecked())
					updateAccessMode("WiFi");
				else
					updateAccessMode("");
			}
		});
		syncMasterData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean isReady = false;
				CommonUtil.setActiveInspectionID(context, null);
				CommonUtil.setDropDbFlag(context, "0");
				/*
				 * String dataSyncMode =
				 * CommonUtil.getDataSyncModeFromDb(context); if (dataSyncMode
				 * != null && "cable".equalsIgnoreCase(dataSyncMode)) {
				 *//**
				 * Nothing to do hear.
				 */
				/*
				 * CommonUtil.launchPopUpEnablingNetwork(context .getResources()
				 * .getString(R.string.changeCableToWeb), context); } else {
				 */
				isReady = CommonUtil.checkConnectivity(context);
				if (isReady) {
					if (VessalInspectionHome.miActionProgressItem != null
							&& "home"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VessalInspectionHome.miActionProgressItem
								.setVisible(true);
					} else if (VesselInspectionStartActivity.miActionProgressItem != null
							&& "start"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VesselInspectionStartActivity.miActionProgressItem
								.setVisible(true);
						VesselInspectionStartActivity.miActionUndoItem
								.setVisible(false);
					}

					CommonUtil.setSynckFrom(context, "admin");

					SyncHandler.context = context;
					Message shipMessage = new Message();
					// shipMessage.what =
					// SyncHandler.MSG_UPDATE_SHIP_MASETR;
					shipMessage.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(shipMessage);

					syncDate.setText(getSyncInfo()[0]);
				}
			}
			// }
		});

		resetData.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CommonUtil.setLoginFromShip(context, "0");
				displayWarningForResetData(context.getResources().getString(
						R.string.resetMsg));
			}
		});

		exportData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				exportDB();
			}
		});

		importData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				importDB();
			}
		});

		resizeImgData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resizeImage();
			}
		});

		dirtyData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				markAllDataDirty();
			}
		});

		dirtyImageData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				markAllImageDataDirty();
			}
		});

		return rootView;
	}

	/**
	 * Ripunjay.S
	 * 
	 * @param fileOrDirectory
	 */
	private void exportDB() {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();
			FileChannel source = null;
			FileChannel destination = null;
			String currentDBPath = "/data/" + context.getPackageName()
					+ "/databases/" + DBHelper.DATABASE_NAME;

			StringBuffer sb = new StringBuffer(
					Environment.getExternalStorageDirectory() + "/TheArk/");
			File file = new File(sb.toString());
			if (!file.exists()) {
				file.mkdirs();
			}
			String backupDBPath = DBHelper.DATABASE_NAME;
			File currentDB = new File(data, currentDBPath);
			File backupDB = new File(file, backupDBPath);
			try {
				source = new FileInputStream(currentDB).getChannel();
				destination = new FileOutputStream(backupDB).getChannel();
				destination.transferFrom(source, 0, source.size());
				source.close();
				destination.close();
				Toast.makeText(context, "DB Exported!", Toast.LENGTH_LONG)
						.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Ripunjay.S
	 * 
	 * @param fileOrDirectory
	 */
	private void importDB() {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();
			FileChannel source = null;
			FileChannel destination = null;
			String currentDBPath = "/data/" + context.getPackageName()
					+ "/databases/" + DBHelper.DATABASE_NAME;

			StringBuffer sb = new StringBuffer(
					Environment.getExternalStorageDirectory() + "/TheArk/");
			File file = new File(sb.toString());
			if (!file.exists()) {
				file.mkdirs();
			}
			String backupDBPath = DBHelper.DATABASE_NAME;
			File currentDB = new File(data, currentDBPath);
			File backupDB = new File(file, backupDBPath);
			if (backupDB.exists()) {
				try {
					source = new FileInputStream(backupDB).getChannel();
					destination = new FileOutputStream(currentDB).getChannel();
					destination.transferFrom(source, 0, source.size());
					source.close();
					destination.close();
					Toast.makeText(context, "DB Imported!", Toast.LENGTH_LONG)
							.show();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Ripunjay.S mark only latest inspection DataDirty
	 * 
	 * @param fileOrDirectory
	 */
	private void markAllDataDirty() {
		try {

			List<VesselInspection> latestVIList = new ArrayList<VesselInspection>();

			DBManager db = new DBManager(context);
			db.open();

			latestVIList = db.getVesselInspectionLatestData(0);
			if (latestVIList != null && latestVIList.size() > 0) {
				db.updateAllDataDirty(latestVIList.get(0)
						.getiVesselInspectionId(), ""
						+ latestVIList.get(0).getiShipId());
			}

			db.close();
			Toast.makeText(context, "Data marked as dirty.", Toast.LENGTH_LONG)
					.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Ripunjay.S mark only latest inspection image DataDirty
	 * 
	 * @param fileOrDirectory
	 */
	private void markAllImageDataDirty() {
		try {

			List<VesselInspection> latestVIList = new ArrayList<VesselInspection>();

			DBManager db = new DBManager(context);
			db.open();

			latestVIList = db.getVesselInspectionLatestData(0);
			if (latestVIList != null && latestVIList.size() > 0) {
				db.updateAllImageDataDirty(latestVIList.get(0)
						.getiVesselInspectionId());
			}

			db.close();
			Toast.makeText(context, "Data marked as dirty.", Toast.LENGTH_LONG)
					.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteRecursive(child);

		fileOrDirectory.delete();
	}

	/**
	 * @author ripunjay resize image code write here : acc PN
	 */
	private void resizeImage(){
		
		/**
		 * Below code for resize and compress image.
		 */
		try {
			File storageActualFiles = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath(), context.getResources().getString(R.string.actualPath));
			
			File storageProcessFiles = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath(), context.getResources().getString(R.string.imagePath));
			if(!storageProcessFiles.exists()){
				storageProcessFiles.mkdirs();
			}
			
			if(storageActualFiles.exists()){				
					File[] imagesList = storageActualFiles.listFiles();
					if(imagesList != null && imagesList.length > 0){
						for(File files : imagesList){
							if(!files.getName().equalsIgnoreCase("process")){
								File[] imagesInnerList = files.listFiles();
								if(imagesInnerList != null && imagesInnerList.length > 0){
									for(File shipFile : imagesInnerList){
										File[] secItemImageList = shipFile.listFiles();		
										if(secItemImageList != null && secItemImageList.length >0){
											for(File secItemFile : secItemImageList){
												/**
												 * Below code for resize and compress image.
												 */
												try {
													
													Double heightWidth = Double.parseDouble(getResources().getString(R.string.regularHeightWidth).trim());
													Double height = 870.0;
													Double width = 695.0;
													Double aspectratio = 1.0;
													DecimalFormat decFormat = new DecimalFormat(
															"##");
																			
													String originalPath = secItemFile.getAbsolutePath();

													BitmapFactory.Options options = new BitmapFactory.Options();
							                        options.inJustDecodeBounds = true;

							                        //Returns null, sizes are in the options variable
							                        BitmapFactory.decodeFile(originalPath, options);
							                        int actualWidth = options.outWidth;
							                        int actualHeight = options.outHeight;

							                        if(actualHeight > heightWidth || actualWidth > heightWidth)
							                        {
														if (actualHeight > actualWidth) {
														   height = heightWidth;
							                               aspectratio = (double) actualHeight/ (double) actualWidth;
							                               width = height/aspectratio;
														} else {
															width = heightWidth;
															aspectratio = (double) actualWidth/ (double) actualHeight;
								                            height = width/aspectratio;
														}
							                        }
							                        else{
							                        	 height = (double) actualHeight;
							                             width = (double) actualWidth;
							                        }
												
													Bitmap imageBitmap = decodeSampledBitmapFromUri(options,
															originalPath, Integer.parseInt(decFormat.format(width)),
															Integer.parseInt(decFormat.format(height)));
													Bitmap scaleimageBitmap = Bitmap.createScaledBitmap(imageBitmap, Integer.parseInt(decFormat.format(width)), Integer.parseInt(decFormat.format(height)), false);
													ByteArrayOutputStream bytes = new ByteArrayOutputStream();
													scaleimageBitmap.compress(
															Bitmap.CompressFormat.JPEG, 60, bytes);

													String relativePathArr[] = originalPath
															.split("/vi/");
													if (relativePathArr != null
															&& relativePathArr.length > 0) {
														originalPath = relativePathArr[0]
																+ "/vi/process/"
																+ relativePathArr[1];
													}

													String procesImageDirpath = originalPath
															.substring(0,
																	originalPath.lastIndexOf("/"));
													File storageDir = new File(procesImageDirpath);
													if (!storageDir.exists()) {

														storageDir.mkdirs();
													}

													File f = new File(originalPath);
													if (!f.exists()) {
														f.createNewFile();
													}
													FileOutputStream fo = new FileOutputStream(f);
													fo.write(bytes.toByteArray());
													fo.close();
													
													
												} catch (FileNotFoundException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												} catch (IOException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												// =================End
												// resizing=============================
												
													}
												}
												
											}
										}
									}
								}
							}
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author Ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForConfirmation(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		// cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				changeServerLayout.setVisibility(View.VISIBLE);
				switchServerSepetor.setVisibility(View.VISIBLE);
				wipeDataLayout.setVisibility(View.VISIBLE);
				wipeDataSepetor.setVisibility(View.VISIBLE);
				exportDataLayout.setVisibility(View.VISIBLE);
				importDataLayout.setVisibility(View.GONE);//ripunjay
				exportDataSepetor.setVisibility(View.VISIBLE);
				importDataSepetor.setVisibility(View.GONE);//ripunjay
				dirtyDataSepetor.setVisibility(View.VISIBLE);
				dirtyDataLayout.setVisibility(View.VISIBLE);
				dirtyImageDataSepetor.setVisibility(View.VISIBLE);
				dirtyImageDataLayout.setVisibility(View.VISIBLE);

				appVersionLinearLayout.setBackgroundColor(getResources()
						.getColor(R.color.appVersionActiveColor));

			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m This method return admin info like lastSyncDate ,
	 *         serverAddress , accessMode from Database
	 */
	private String[] getSyncInfo() {
		String[] syncInfo = new String[4];
		DBManager db = new DBManager(context);
		db.open();
		List<SyncStatus> syncStatusList = db.getSyncStatusData();

		if (syncStatusList != null && syncStatusList.size() > 0) {

			SyncStatus syncStatus = syncStatusList.get(0);

			syncInfo[0] = "Last Sync Date : " + syncStatus.getDtSyncDate();
			syncInfo[1] = syncStatus.getSyncMode();
			syncInfo[2] = syncStatus.getServerAddress();
			syncInfo[3] = syncStatus.getDataSyncMode();
		} else {

			syncInfo[0] = "Last Sync Date : "
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			syncInfo[1] = context.getResources().getString(
					R.string.defultAccessMode);
			syncInfo[2] = context.getResources().getString(
					R.string.defultServiceUrl);
			syncInfo[3] = context.getResources().getString(
					R.string.defultDataSyncMode);
		}
		db.close();
		return syncInfo;
	}

	private void updateAccessMode(String syncMode) {
		try {
			DBManager db = new DBManager(context);
			SyncStatus syncStatus = null;
			db.open();
			DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			if (syncStatusList != null && syncStatusList.size() > 0) {
				syncStatus = new SyncStatus();
				syncStatus = syncStatusList.get(0);
				syncStatus.setSyncMode(syncMode);
				db.updateSyncStatusTable(syncStatus);
			} else {
				syncStatus = new SyncStatus("123" + new Date().getTime(),
						dtFormat.format(new Date()), syncMode,
						CommonUtil.getServerAddress(context),
						Integer.parseInt(CommonUtil.getTenantId(context)), 0,
						0, 1, dtFormat.format(new Date()),
						dtFormat.format(new Date()),
						CommonUtil.getUserId(context),
						CommonUtil.getUserId(context),
						CommonUtil.getDataSyncModeFromDb(context));

				db.insertSyncStatusTable(syncStatus);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void updateDataSyncMode(String dataSyncMode) {
		try {
			DBManager db = new DBManager(context);
			SyncStatus syncStatus = null;
			db.open();
			DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			if (syncStatusList != null && syncStatusList.size() > 0) {
				syncStatus = new SyncStatus();
				syncStatus = syncStatusList.get(0);
				// syncStatus.setSyncMode(syncMode);
				syncStatus.setDataSyncMode(dataSyncMode);
				db.updateSyncStatusTable(syncStatus);
			} else {
				syncStatus = new SyncStatus("123" + new Date().getTime(),
						dtFormat.format(new Date()),
						CommonUtil.getDataAccessMode(context),
						CommonUtil.getServerAddress(context),
						Integer.parseInt(CommonUtil.getTenantId(context)), 0,
						0, 1, dtFormat.format(new Date()),
						dtFormat.format(new Date()),
						CommonUtil.getUserId(context),
						CommonUtil.getUserId(context), dataSyncMode);

				db.insertSyncStatusTable(syncStatus);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForDiryViData(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_logout);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button sync = (Button) dialog.findViewById(R.id.sync);

		sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * / Syncing process
				 */
				dialog.dismiss();
				/*
				 * String dataSyncMode =
				 * CommonUtil.getDataSyncModeFromDb(context); if (dataSyncMode
				 * != null && "cable".equalsIgnoreCase(dataSyncMode)) {
				 * 
				 * CommonUtil.launchPopUpEnablingNetwork(context .getResources()
				 * .getString(R.string.changeCableToWeb), context);
				 * 
				 * } else {
				 */
				if (CommonUtil.checkConnectivity(context)) {
					if (VessalInspectionHome.miActionProgressItem != null
							&& "home"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VessalInspectionHome.miActionProgressItem
								.setVisible(true);
					} else if (VesselInspectionStartActivity.miActionProgressItem != null
							&& "start"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VesselInspectionStartActivity.miActionProgressItem
								.setVisible(true);
						VesselInspectionStartActivity.miActionUndoItem
								.setVisible(false);
					}

					CommonUtil.setSynckFrom(context, "process");

					SyncHandler.context = context;
					Message shipMessage = new Message();
					// shipMessage.what =
					// SyncHandler.MSG_UPDATE_SHIP_MASETR;
					shipMessage.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(shipMessage);

					syncDate.setText(getSyncInfo()[0]);
				}
			}

			// }
		});
		Button logOut = (Button) dialog.findViewById(R.id.logOut);
		logOut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/**
				 * Log out process
				 */

				DBManager db = new DBManager(context);
				db.open();
				db.dropDb();
				db.close();
				deleteRecursive(new File(Environment
						.getExternalStorageDirectory() + "/theark/vi"));
				LoginActivity.isNewUser = true;
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
				getActivity().finish();
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	public void displayWarningForResetData(String message) {

		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_cance_confirmation_popup);
		Button crossButton = (Button) dialog
				.findViewById(R.id.cancelCrossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog
				.findViewById(R.id.cancelDialogMessage);
		textView.setText(message);
		Button ok = (Button) dialog.findViewById(R.id.cancelYes);

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				DBManager db = new DBManager(context);
				db.open();
				db.dropDb();
				db.close();
				deleteRecursive(new File(Environment
						.getExternalStorageDirectory() + "/theark/vi"));
				LoginActivity.isNewUser = true;
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
				getActivity().finish();
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancelNo);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}
	
	public Bitmap decodeSampledBitmapFromUri(BitmapFactory.Options options, String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		//////////final BitmapFactory.Options options = new BitmapFactory.Options();
		/////////options.inJustDecodeBounds = true;
		//BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, reqWidth,	reqHeight);		
		
		// this options allow android to claim the bitmap memory if it runs low on memory
        //options.inPurgeable = true;
        //options.inInputShareable = true;
        //options.inTempStorage = new byte[16 * 1024];
        
		try {
			bm = BitmapFactory.decodeFile(path, options);
			//bm = BitmapFactory.decodeFile(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}


}
