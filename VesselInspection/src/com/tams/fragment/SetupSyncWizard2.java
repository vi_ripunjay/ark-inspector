package com.tams.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tams.vessalinspection.MyBroadcast;
import com.tams.vessalinspection.R;

public class SetupSyncWizard2 extends Fragment {

	Intent intent;
	// private static boolean datacableConnect = false;
	public static TextView cableConnect;
	public static Context mContext;
	int counter = 0;
	private SyncTab adminSyncTab;
	public static ImageView connectionImage;

	@SuppressWarnings("static-access")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.wizard2, container, false);

		mContext = getActivity().getApplicationContext();
		SyncTab.isConnected(mContext);
		adminSyncTab = new SyncTab();
		adminSyncTab.wizard1.setEnabled(false);
		adminSyncTab.wizard1.setTextColor(getActivity().getResources()
				.getColor(R.color.black));
		adminSyncTab.wizard1.invalidate();

		cableConnect = (TextView) rootView.findViewById(R.id.cableConnect);
		connectionImage = (ImageView) rootView.findViewById(R.id.usbConnection);

		usbConnectionCheck();

		return rootView;
	}

	public static void usbConnectionCheck() {
		SetupSyncWizard2 ast = new SetupSyncWizard2();
		SetupSyncWizard2.SynchronizationUSBConnTask syncConnectionTask = ast.new SynchronizationUSBConnTask();
		syncConnectionTask.execute((Void) null);

	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationUSBConnTask extends
			AsyncTask<Void, Void, Boolean> {

		boolean massConnection = false;
		boolean datacableConnect = false;
		private String strBlack = "#000000";
		private String strWhite = "#ffffff";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			datacableConnect = false;
			SyncTab.isConnected(mContext);
			if (MyBroadcast.powerConnected) {
				cableConnect
						.setText("Your device is now connected to the PC. Please click on Next");
				adminSyncTab.wizard1.setEnabled(true);
				adminSyncTab.wizard1.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.white));
				adminSyncTab.wizard1.invalidate();

				connectionImage.setVisibility(View.VISIBLE);
				connectionImage.setImageResource(R.drawable.connect_usb);
				connectionImage.invalidate();
			} else {
				cableConnect
						.setText("The system is waiting for you to connect the device to the PC...");
				cableConnect
						.setText("The system is waiting for you to connect the device to the PC...");
				adminSyncTab.wizard1.setEnabled(false); //ripunjay
				adminSyncTab.wizard1.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.black));
				adminSyncTab.wizard1.invalidate();

				connectionImage.setVisibility(View.VISIBLE);
				connectionImage.setImageResource(R.drawable.usb_error);
				connectionImage.invalidate();
			}
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				datacableConnect = false;

				if (MyBroadcast.powerConnected) {
					datacableConnect = true;
					// massConnection = true;

				} else if (getMassStorageOn()) {
					datacableConnect = true;
					// massConnection = false;

				} else {
					datacableConnect = false;
					// massConnection = false;
				}

				Thread.sleep(2000);

			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			// TODO: register the new account here.
			return datacableConnect;
		}

		@SuppressWarnings("static-access")
		@Override
		protected void onPostExecute(final Boolean success) {

			if (datacableConnect) {
				cableConnect
						.setText("Your device is now connected to the PC. Please click on Next");
				adminSyncTab.wizard1.setEnabled(true);
				adminSyncTab.wizard1.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.white));
				adminSyncTab.wizard1.invalidate();

				connectionImage.setVisibility(View.VISIBLE);
				connectionImage.setImageResource(R.drawable.connect_usb);
				connectionImage.invalidate();

			} else {
				cableConnect
						.setText("The system is waiting for you to connect the device to the PC...");
				adminSyncTab.wizard1.setEnabled(false); //ripunjay
				adminSyncTab.wizard1.setTextColor(adminSyncTab.astContext
						.getResources().getColor(R.color.black));
				adminSyncTab.wizard1.invalidate();

				connectionImage.setVisibility(View.VISIBLE);
				connectionImage.setImageResource(R.drawable.usb_error);
				connectionImage.invalidate();

			}

			usbConnectionCheck();
		}

		@Override
		protected void onCancelled() {

		}
	}

	private boolean canWriteToFlash() {
		String state = Environment.getExternalStorageState();
		System.out.println("state : " + state);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (Environment.MEDIA_SHARED.equals(state)) {
			// Read only isn't good enough

			return false;
		} else {
			return false;
		}
	}

	private boolean getMassStorageOn() {
		String state = Environment.getExternalStorageState();
		System.out.println("state : " + state);
		if (Environment.MEDIA_SHARED.equals(state)
				|| Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SyncTab.isConnected(mContext);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if (SyncTab.myBroadcast != null) {
				mContext.unregisterReceiver(SyncTab.myBroadcast);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
