package com.tams.fragment;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.storage.StorageManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;

public class SetupSyncWizard5 extends Fragment {

	public static ProgressBar progressBarFileCheck;
	public static TextView textViewFileRecevied;
	public static Context mContext;
	public static SyncTab adminSyncTab;
	public static TextView textViewHome;
	public static TextView fileNotFoundtxtView;
	Handler handler;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.wizard5, container, false);
		mContext = getActivity().getApplicationContext();
		// SyncTab.isConnected(mContext);
		adminSyncTab = new SyncTab();

		adminSyncTab.wizard4.setEnabled(false);
		adminSyncTab.wizard4.setTextColor(getResources()
				.getColor(R.color.black));
		adminSyncTab.wizard4.invalidate();

		progressBarFileCheck = (ProgressBar) rootView
				.findViewById(R.id.progressBarFileCheck);
		textViewFileRecevied = (TextView) rootView
				.findViewById(R.id.textViewFileRecevied);
		textViewHome = (TextView) rootView.findViewById(R.id.textViewHome);
		fileNotFoundtxtView = (TextView) rootView
				.findViewById(R.id.fileNotFoundtxtView);
		if (adminSyncTab.fileNotFound) {
			fileNotFoundtxtView.setVisibility(View.VISIBLE);
			if(CommonUtil.getFileNotmatchFlg(adminSyncTab.astContext) != null && "1".equalsIgnoreCase(CommonUtil.getFileNotmatchFlg(adminSyncTab.astContext))){
				fileNotFoundtxtView.setText(mContext.getResources().getString(R.string.fileMissmatchMsg));
			}
		} else {
			fileNotFoundtxtView.setVisibility(View.GONE);
		}

		try {
			StorageManager storage = (StorageManager) mContext
					.getApplicationContext().getSystemService(
							getActivity().STORAGE_SERVICE);
			Method method = storage.getClass().getDeclaredMethod(
					"enableUsbMassStorage");
			method.setAccessible(true);
			Object r = method.invoke(storage);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fileCheckingTask();

		return rootView;
	}

	public static void fileCheckingTask() {
		SetupSyncWizard5 ast = new SetupSyncWizard5();
		SetupSyncWizard5.SynchronizationFileCheckTask syncFileCheckTask = ast.new SynchronizationFileCheckTask();
		syncFileCheckTask.execute((Void) null);
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationFileCheckTask extends
			AsyncTask<Void, Void, Boolean> {

		/*int counter = 0;
		boolean fileExist = false;
		boolean dataConnection = false;*/

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			// showProgress(true);
			adminSyncTab.wizard4.setEnabled(false);
			adminSyncTab.wizard4.setTextColor(mContext.getResources()
					.getColor(R.color.black));
			adminSyncTab.wizard4.invalidate();
		
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 * try {
			 * 
			 * 
			 * if (!getMassStorageOn()) { dataConnection = false;
			 * 
			 * do { if (fileExist()) {
			 * 
			 * fileExist = true; } else { fileExist = false; } if (counter >
			 * 200) { break; } counter++; Thread.sleep(3000); } while
			 * (!fileExist);
			 * 
			 * 
			 * } else{ dataConnection = true; }
			 * 
			 * } catch (InterruptedException e) { return false; }
			 */

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			
			adminSyncTab.wizard4.setEnabled(true);
			adminSyncTab.wizard4.setTextColor(mContext.getResources().getColor(
					R.color.white));
			adminSyncTab.wizard4.invalidate();	

			// if(!dataConnection){
			/*
			 * if (success) { // textViewFileRecevied.setText(
			 * "File has been received successfully, Click on next button...");
			 * adminSyncTab.wizard4.setEnabled(true);
			 * adminSyncTab.wizard4.setTextColor(adminSyncTab.astContext
			 * .getResources().getColor(R.color.white)); showProgress(false);
			 * 
			 * } else { adminSyncTab.wizard4.setEnabled(false);
			 * adminSyncTab.wizard4.setTextColor(adminSyncTab.astContext
			 * .getResources().getColor(R.color.black)); showProgress(true); }
			 */
			/*
			 * } else{ textViewHome.setText("USB Mass storage should be off");
			 * adminSyncTab.wizard4.setEnabled(false);
			 * adminSyncTab.wizard4.setTextColor
			 * (adminSyncTab.astContext.getResources().getColor(R.color.black));
			 * showProgress(false); fileCheckingTask(); }
			 */

		}

		@Override
		protected void onCancelled() {
			// showProgress(false);
		}
	}

	public boolean fileExist() {
		boolean exist = false;
		String xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public void showProgress(boolean start) {
		if (start) {
			// progressBarFileCheck.setVisibility(View.VISIBLE);

			// textViewFileRecevied.setVisibility(View.GONE);

		} else if (!start) {

			// progressBarFileCheck.setVisibility(View.GONE);

			// textViewFileRecevied.setVisibility(View.VISIBLE);
		}
	}

	private boolean canWriteToFlash() {
		String state = Environment.getExternalStorageState();
		System.out.println("state : " + state);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (Environment.MEDIA_SHARED.equals(state)) {
			// Read only isn't good enough

			return false;
		} else {
			return false;
		}
	}

	private boolean getMassStorageOn() {
		String state = Environment.getExternalStorageState();
		System.out.println("state : " + state);
		if (Environment.MEDIA_SHARED.equals(state)
				|| Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SyncTab.isConnected(mContext);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if (SyncTab.myBroadcast != null) {
				mContext.unregisterReceiver(SyncTab.myBroadcast);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
