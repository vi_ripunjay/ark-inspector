package com.tams.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tams.vessalinspection.R;

public class SetupSyncWizard4 extends Fragment {

	public static Context mContext;
	public static TextView usbMassConnect;
	private boolean datacableConnect = false;
//	private boolean massStorageConnected = false;
//	private boolean massStorageConnectedOneTime = false;
	int counter = 0;
	private SyncTab adminSyncTab;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.wizard4, container, false);

		mContext = getActivity().getApplicationContext();

		//SyncTab.isConnected(mContext);
		adminSyncTab = new SyncTab();
		adminSyncTab.wizard3.setEnabled(false);
		adminSyncTab.wizard3.setTextColor(getResources().getColor(R.color.black));

	/*	massStorageConnectedOneTime = false;
		massStorageConnected = false;*/
		
		usbMassConnect = (TextView) rootView.findViewById(R.id.usbMassConnect);

		usbConnectionCheck();

		return rootView;
	}

	public static void usbConnectionCheck() {
		SetupSyncWizard4 ast = new SetupSyncWizard4();
		SetupSyncWizard4.SynchronizationUSBConnectionTask syncConnectionTask = ast.new SynchronizationUSBConnectionTask();
		syncConnectionTask.execute((Void) null);
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationUSBConnectionTask extends
			AsyncTask<Void, Void, Boolean> {

	

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			usbMassConnect
			.setText("Ready for file transfering from Import and Export.");
			/*if (getMassStorageOn()) {
				usbMassConnect
						.setText("Ready for file transfering from Import and Export.");
				massStorageConnected = true;
				massStorageConnectedOneTime  = true;

			} else if (MyBroadcast.powerConnected) {
				if(massStorageConnectedOneTime){
					usbMassConnect
					.setText("USB mass storage off, click on next button.");
					adminSyncTab.wizard3.setEnabled(true);
					adminSyncTab.wizard3.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.white));
				}
				else{
					usbMassConnect
					.setText("USB Connected, Please on usb mass storage.");
				}
				
				massStorageConnected = false;
			} else {
				usbMassConnect.setText("Dissconnected,Please connect usb with pc.");
				massStorageConnected = false;
			}*/
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				
				/*if (getMassStorageOn()) {
					datacableConnect = true;
					massStorageConnected = true;
					massStorageConnectedOneTime = true;

				}
				else if (MyBroadcast.powerConnected) {

					datacableConnect = true;
					massStorageConnected = false;

				} 
				else {
					datacableConnect = false;
					massStorageConnected = false;

				}

				counter++;
				*/
				Thread.sleep(2000);

			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			// TODO: register the new account here.
			return datacableConnect;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			
		/*	usbMassConnect
			.setText("USB mass storage off, click on next button.");*/
			adminSyncTab.wizard3.setEnabled(true);
			adminSyncTab.wizard3.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.white));

		/*	if (massStorageConnected) {
				usbMassConnect.setText("USB mass storage on.");
				adminSyncTab.wizard3.setEnabled(false);
				adminSyncTab.wizard3.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.black));
				usbConnectionCheck();
			} else {
				usbMassConnect
						.setText("USB mass storage off, click on next button.");
				if (massStorageConnectedOneTime) {
					adminSyncTab.wizard3.setEnabled(true);
					adminSyncTab.wizard3.setTextColor(adminSyncTab.astContext.getResources().getColor(R.color.white));
				}
				usbConnectionCheck();
			}*/

		}

		@Override
		protected void onCancelled() {

		}
	}

	private boolean canWriteToFlash() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// Read only isn't good enough
			return false;
		} else {
			return false;
		}
	}

	private boolean getMassStorageOn() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_SHARED.equals(state) || Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SyncTab.isConnected(mContext);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if(SyncTab.myBroadcast != null){
				mContext.unregisterReceiver(SyncTab.myBroadcast);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
