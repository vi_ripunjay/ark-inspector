package com.tams.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tams.adapter.TabsPagerAdapter;
import com.tams.vessalinspection.R;


public class VesselInspectionFragment extends Fragment {
	ViewPager Tab;
    TabsPagerAdapter TabAdapter;
	ActionBar actionBar;
	private FragmentActivity myContext;
    int tabvalue=0;
    String template_code;
    

    //Mandatory Constructor
    public VesselInspectionFragment() {
    }

    public VesselInspectionFragment(int i) {
    	tabvalue = i;
    }
    
    public VesselInspectionFragment(int position ,String template_code) {
    	this.template_code = template_code;
    	tabvalue = position;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    
   

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_vessel_inspection,container, false);
        
       
        TabAdapter = new TabsPagerAdapter(getChildFragmentManager());
        
        Tab = (ViewPager) rootView.findViewById(R.id.pager);
        
        Tab.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                       
                    	actionBar = getActivity().getActionBar();
                    	actionBar.setSelectedNavigationItem(position);                    }
                });
        
        Tab.setAdapter(TabAdapter);
        
        
        actionBar = getActivity().getActionBar();
        //Enable Tabs on Action Bar
       // actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener(){

			@Override
			public void onTabReselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}

			@Override
			 public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
	          
	            Tab.setCurrentItem(tab.getPosition());
	        }

			@Override
			public void onTabUnselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}};
			
			actionBar.removeAllTabs();
			if(actionBar.getTabCount() == 0)
		    {
		     
		    //Add New Tab
			/*				
			actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabChecklist)).setTabListener(tabListener));
			actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabVslInsp)).setTabListener(tabListener));
			actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabMrplInsp)).setTabListener(tabListener));
			
			*/
				
			if("_checkList".equals(template_code))
				actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabChecklist)).setTabListener(tabListener));
			if("_form".equals(template_code))
				actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabVslInsp)).setTabListener(tabListener));
			if("_form".equals(template_code))
				actionBar.addTab(actionBar.newTab().setText(myContext.getResources().getString(R.string.frgTabMrplInsp)).setTabListener(tabListener));
			
			actionBar.setSelectedNavigationItem(tabvalue);
				
		    }
        return rootView;
    }
}


