package com.tams.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;

import com.tams.adapter.MarpolInspectionFormAdapter;
import com.tams.adapter.PinnedHeaderMarpolInspListView;
import com.tams.adapter.VesselInsepectionFormAdapter;
import com.tams.model.FilledForm;
import com.tams.model.FormCategory;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.model.VesselInspectionFormListDesc;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.Simple;

public class MarpolInspectionTab extends Fragment {

	// public static ExpandableListView marpolFormList;
	public static PinnedHeaderMarpolInspListView marpolFormList;

	Button submitButton;
	Intent intentMI;
	private int lastClickedPosition = 0;

	public static List<FormSection> formSection = new ArrayList<FormSection>();
	public static List<Object> formSectionItem = new ArrayList<Object>();
	public static Context miContext;
	public static int selectedChildPosition = 0;
	public String strViId = "";
	View vesselInspec;
	public static String iFormCategoryId;
	/**
	 * showAllGroup 0 for all 1 for only one
	 */
	public static int showAllGroup = 0;
	public String strFormSectionId = null;
	public String strVesselInspectionId = null;
	ActionBar actionBar;
	ImageButton floatingButton;

	public MarpolInspectionTab() {

	}

	public MarpolInspectionTab(String iFormCategoryId) {
		this.iFormCategoryId = iFormCategoryId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (savedInstanceState != null) {

		} else {
			showAllGroup = 0;
		}

		intentMI = getActivity().getIntent();

		actionBar = getActivity().getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		strVesselInspectionId = intentMI
				.getStringExtra("strVesselInspectionId");
		if (strVesselInspectionId == null
				|| "".equalsIgnoreCase(strVesselInspectionId)) {
			strVesselInspectionId = CommonUtil
					.getVesselInspectionId(getActivity());
		}
		strViId = strVesselInspectionId;

		vesselInspec = inflater.inflate(R.layout.marpol_inspection_frag,
				container, false);

		miContext = getActivity();

		marpolFormList = (PinnedHeaderMarpolInspListView) vesselInspec
				.findViewById(R.id.marpolInspectionFormId);
		floatingButton = (ImageButton) vesselInspec
				.findViewById(R.id.imageButtonAsFloatingMI);
		if (showAllGroup == 0) {
			floatingButton.setVisibility(View.GONE);
		} else {
			floatingButton.setVisibility(View.VISIBLE);
		}
		/*
		 * submitButton = (Button) vesselInspec.findViewById(R.id.btnMISubmit);
		 * submitButton.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { for (Object fsi :
		 * formSectionItem) {
		 * 
		 * @SuppressWarnings("unchecked") List<FilledForm> fildFrmItmList =
		 * (ArrayList<FilledForm>) fsi;
		 * 
		 * for (FilledForm fldFrm : fildFrmItmList) {
		 * 
		 * System.out.println("child Title : " + fldFrm.getStrHeaderDesc());
		 * System.out.println("child footer : " + fldFrm.getStrFooterDesc());
		 * System.out.println("child Desc : " + fldFrm.getCreatedBy());
		 * 
		 * DBManager db = new DBManager(getActivity()); db.open();
		 * 
		 * List<FilledForm> checkList = new ArrayList<FilledForm>(); checkList =
		 * db.getFilledFormDataById(fldFrm .getiFilledFormId()); if (checkList
		 * == null || checkList.size() == 0) { db.insertFilldFormTable(fldFrm);
		 * } else {
		 * 
		 * db.updateFilldFormTable(fldFrm); } db.close(); } }
		 * 
		 * } });
		 */

		if (savedInstanceState == null) {

			setGroupData(strVesselInspectionId, strFormSectionId);
		}
		// setChildGroupData();

		/*
		 * MarpolInspectionFormAdapter mifAdapter = new
		 * MarpolInspectionFormAdapter( 0, getActivity(), formSection,
		 * formSectionItem); mifAdapter.setInflater( (LayoutInflater)
		 * getActivity().getSystemService( Context.LAYOUT_INFLATER_SERVICE),
		 * getActivity()); marpolFormList.setAdapter(mifAdapter);
		 */

		MarpolInspectionFormAdapter mifAdapter = new MarpolInspectionFormAdapter(
				showAllGroup, getActivity(), formSection, formSectionItem);
		mifAdapter.setInflater(
				(LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE), getActivity());

		marpolFormList.setAdapter(mifAdapter);
		marpolFormList.setGroupIndicator(null);
		View h = LayoutInflater.from(getActivity()).inflate(R.layout.grouprow,
				(ViewGroup) vesselInspec.findViewById(R.id.rootMI), false);
		marpolFormList.setPinnedHeaderView(h);
		marpolFormList.setOnScrollListener((OnScrollListener) mifAdapter);
		marpolFormList.setDividerHeight(0);

		marpolFormList.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub

				// viFormList.setSelection(groupPosition);
				Boolean shouldExpand = (!marpolFormList
						.isGroupExpanded(groupPosition));
				marpolFormList.collapseGroup(lastClickedPosition);

				/*
				 * if (shouldExpand) { // generateExpandableList();
				 * marpolFormList.expandGroup(groupPosition);
				 * marpolFormList.setSelectionFromTop(groupPosition, 0); }
				 */

				lastClickedPosition = groupPosition;
				// v.setBackgroundColor(getResources().getColor(R.color.green));
				/**
				 * @author ripunjay below work for open single section at a
				 *         time.
				 */
				// =========================== Start
				// ====================================
				String strFormSectionId = null;
				if (showAllGroup == 0) {
					showAllGroup = 1;

				}
				if (showAllGroup == 1) {
					floatingButton.setVisibility(View.VISIBLE);
					strFormSectionId = formSection.get(groupPosition)
							.getiFormSectionId();
				} else if (showAllGroup == 2) {
					floatingButton.setVisibility(View.GONE);
					strFormSectionId = null;
					saveMiData();
				}
				setGroupData(strViId, strFormSectionId);

				/*
				 * MarpolInspectionFormAdapter mifAdapter = new
				 * MarpolInspectionFormAdapter( showAllGroup, getActivity(),
				 * formSection, formSectionItem);
				 * mifAdapter.setInflater((LayoutInflater) getActivity()
				 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				 * getActivity()); marpolFormList.setAdapter(mifAdapter);
				 */

				MarpolInspectionFormAdapter mifAdapter = new MarpolInspectionFormAdapter(
						showAllGroup, getActivity(), formSection,
						formSectionItem);
				mifAdapter.setInflater((LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
						getActivity());

				marpolFormList.setAdapter(mifAdapter);
				marpolFormList.setGroupIndicator(null);
				View h = LayoutInflater.from(getActivity()).inflate(
						R.layout.grouprow,
						(ViewGroup) vesselInspec.findViewById(R.id.rootMI),
						false);
				marpolFormList.setPinnedHeaderView(h);
				marpolFormList
						.setOnScrollListener((OnScrollListener) mifAdapter);
				marpolFormList.setDividerHeight(0);

				if (showAllGroup == 1) {
					marpolFormList.expandGroup(0);
					marpolFormList.setSelectionFromTop(0, 1);
					// viFormList.setSelectedChild(0, 0, true);
					showAllGroup = 2;
				} else if (showAllGroup == 2) {
					showAllGroup = 1;

				}
				// =====================end==========================================
				return true;
			}
		});

		floatingButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				floatingButton.setVisibility(View.GONE);

				/*
				 * if (showAllGroup == 1) { //marpolFormList.expandGroup(0);
				 * //marpolFormList.setSelectionFromTop(0, 1); //
				 * viFormList.setSelectedChild(0, 0, true); showAllGroup = 2; }
				 * else if (showAllGroup == 2) { showAllGroup = 1;
				 * 
				 * }
				 */
				showAllGroup = 0;

				setGroupData(strVesselInspectionId, null);

				MarpolInspectionFormAdapter mifAdapter = new MarpolInspectionFormAdapter(
						showAllGroup, getActivity(), formSection,
						formSectionItem);
				mifAdapter.setInflater((LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
						getActivity());

				marpolFormList.setAdapter(mifAdapter);
				marpolFormList.setGroupIndicator(null);
				View h = LayoutInflater.from(getActivity()).inflate(
						R.layout.grouprow,
						(ViewGroup) vesselInspec.findViewById(R.id.rootMI),
						false);
				marpolFormList.setPinnedHeaderView(h);
				marpolFormList
						.setOnScrollListener((OnScrollListener) mifAdapter);
				marpolFormList.setDividerHeight(0);

			}
		});

		return vesselInspec;
	}

	public void setGroupData(String strVesselInspectionId,
			String strFormSectionId) {

		formSection = new ArrayList<FormSection>();
		formSectionItem = new ArrayList<Object>();
		String formCategoryId = "";
		DBManager db = new DBManager(getActivity());
		db.open();
		List<FormSection> fsList = new ArrayList<FormSection>();

		List<FormCategory> fcList = new ArrayList<FormCategory>();
		fcList = db.getFormCategoryByCode("MI");

		if (strFormSectionId != null && !"".equals(strFormSectionId)) {
			fsList = db.getFormSectionDataById(strFormSectionId);
			intentMI.putExtra("viTitle", fsList.get(0).getStrSectionName());
		} else {

			if (fcList.size() > 0)
				formCategoryId = fcList.get(0).getiFormCategoryId();
			// fsList = db.getFormSectionData("MI");
			// fsList = db.getFormSectionData(formCategoryId);
			fsList = db.getFormSectionData(iFormCategoryId);
		}

		Date cudate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (fsList != null && fsList.size() > 0) {

			for (FormSection fs : fsList) {
				if (fs.getFlgDeleted() == 0) {
					formSection.add(fs);

					/**
					 * load form section item
					 */
					List<FormSectionItem> fsiList = new ArrayList<FormSectionItem>();
					fsiList = db.getFormSectionItemDataBySectionId(fs
							.getiFormSectionId());
					// formSectionItem.add(fsiList);
					List<FilledForm> filedFormBySecList = new ArrayList<FilledForm>();
					int seq = 1;
					if (fsiList != null) {
						for (FormSectionItem frmSecitm : fsiList) {
							if (frmSecitm.getFlgDeleted() == 0) {
								/**
								 * get filled checkList value on the basis of
								 * vesselInspection, formsection and
								 * formsectionitrm
								 */
								List<FilledForm> fldFrmList = new ArrayList<FilledForm>();
								fldFrmList = db.getFilledFormDataByIds(
										strVesselInspectionId,
										fs.getiFormSectionId(),
										frmSecitm.getiFormSectionItemId());
								if (fldFrmList != null && fldFrmList.size() > 0) {

									filedFormBySecList.addAll(fldFrmList);
									

									List<VesselInspectionFormListDesc> descList = db.getVIFLDescForSectionItem(fldFrmList.get(0).getiVesselInspectionId(), fldFrmList.get(0).getiFilledFormId(), fldFrmList.get(0).getiFormSectionItemId());
									if(descList == null || descList.size() == 0){
										VesselInspectionFormListDesc viflDescHeader = new VesselInspectionFormListDesc();
										
										viflDescHeader.setCreatedBy(CommonUtil
												.getUserId(getActivity()));
										viflDescHeader.setCreatedDate(format
												.format(cudate));
										viflDescHeader.setFlgDeleted(0);
										viflDescHeader.setFlgIsDeviceDirty(0);
										viflDescHeader.setFlgIsDirty(1);
										viflDescHeader.setFlgIsEdited(0);
										viflDescHeader.setFlgStatus(0);
										viflDescHeader.setiFilledFormId(fldFrmList.get(0).getiFilledFormId());
										viflDescHeader.setiFormSectionId(fldFrmList.get(0).getiFormSectionId());
										viflDescHeader.setiFormSectionItemId(fldFrmList.get(0).getiFormSectionItemId());
										viflDescHeader.setiVesselInspectionFormListDescId(getPk());
										viflDescHeader.setiShipId(Integer
												.parseInt(CommonUtil
														.getShipId(getActivity())));
										viflDescHeader.setiTenantId(Integer
												.parseInt(CommonUtil
														.getTenantId(getActivity())));
										viflDescHeader.setiVesselInspectionId(fldFrmList.get(0).getiVesselInspectionId());
										viflDescHeader.setModifiedBy(CommonUtil
												.getUserId(getActivity()));
										viflDescHeader.setModifiedDate(format
												.format(cudate));
										viflDescHeader.setSequence(fldFrmList.get(0).getSequence());
										viflDescHeader.setStrDescType(CommonUtil.DESC_TYPE_HEADER);
										viflDescHeader.setStrDesc("");
										
										db.insertVIFLDesc(viflDescHeader);
										
										
										VesselInspectionFormListDesc viflColorCondition = new VesselInspectionFormListDesc();
										
										viflColorCondition.setCreatedBy(CommonUtil
												.getUserId(getActivity()));
										viflColorCondition.setCreatedDate(format
												.format(cudate));
										viflColorCondition.setFlgDeleted(0);
										viflColorCondition.setFlgIsDeviceDirty(0);
										viflColorCondition.setFlgIsDirty(1);
										viflColorCondition.setFlgIsEdited(1);
										viflColorCondition.setFlgStatus(0);
										viflColorCondition.setiFilledFormId(fldFrmList.get(0).getiFilledFormId());
										viflColorCondition.setiFormSectionId(fldFrmList.get(0).getiFormSectionId());
										viflColorCondition.setiFormSectionItemId(fldFrmList.get(0).getiFormSectionItemId());
										viflColorCondition.setiVesselInspectionFormListDescId(getPk());
										viflColorCondition.setiShipId(Integer
												.parseInt(CommonUtil
														.getShipId(getActivity())));
										viflColorCondition.setiTenantId(Integer
												.parseInt(CommonUtil
														.getTenantId(getActivity())));
										viflColorCondition.setiVesselInspectionId(fldFrmList.get(0).getiVesselInspectionId());
										viflColorCondition.setModifiedBy(CommonUtil
												.getUserId(getActivity()));
										viflColorCondition.setModifiedDate(format
												.format(cudate));
										viflColorCondition.setSequence(fldFrmList.get(0).getSequence());
										viflColorCondition.setStrDescType(CommonUtil.COLOR_CONDITION);
										viflColorCondition.setStrDesc("");
										
										db.insertVIFLDesc(viflColorCondition);
									}else{
										//Checking row for color condition in VIFormListDesc
										List<VesselInspectionFormListDesc> viflColorDescList = db
												.getVIFLDescForSectionItemByType(
														fldFrmList
																.get(0)
																.getiVesselInspectionId(),
														fldFrmList
																.get(0)
																.getiFilledFormId(),
														fldFrmList
																.get(0)
																.getiFormSectionItemId(),
														CommonUtil.COLOR_CONDITION);
										
										if(viflColorDescList != null && viflColorDescList.size() > 0){
											//nothing to do
										}else{
											//create new row of viflDesc for color condition
											VesselInspectionFormListDesc viflColorCondition = new VesselInspectionFormListDesc();
											
											viflColorCondition.setCreatedBy(CommonUtil
													.getUserId(getActivity()));
											viflColorCondition.setCreatedDate(format
													.format(cudate));
											viflColorCondition.setFlgDeleted(0);
											viflColorCondition.setFlgIsDeviceDirty(0);
											viflColorCondition.setFlgIsDirty(1);
											viflColorCondition.setFlgIsEdited(1);
											viflColorCondition.setFlgStatus(0);
											viflColorCondition.setiFilledFormId(fldFrmList.get(0).getiFilledFormId());
											viflColorCondition.setiFormSectionId(fldFrmList.get(0).getiFormSectionId());
											viflColorCondition.setiFormSectionItemId(fldFrmList.get(0).getiFormSectionItemId());
											viflColorCondition.setiVesselInspectionFormListDescId(getPk());
											viflColorCondition.setiShipId(Integer
													.parseInt(CommonUtil
															.getShipId(getActivity())));
											viflColorCondition.setiTenantId(Integer
													.parseInt(CommonUtil
															.getTenantId(getActivity())));
											viflColorCondition.setiVesselInspectionId(fldFrmList.get(0).getiVesselInspectionId());
											viflColorCondition.setModifiedBy(CommonUtil
													.getUserId(getActivity()));
											viflColorCondition.setModifiedDate(format
													.format(cudate));
											viflColorCondition.setSequence(fldFrmList.get(0).getSequence());
											viflColorCondition.setStrDescType(CommonUtil.COLOR_CONDITION);
											viflColorCondition.setStrDesc("");
											
											db.insertVIFLDesc(viflColorCondition);
										}
									}

								} else {
									

									FilledForm filledForm = new FilledForm();
									filledForm.setiFilledFormId(getPk());
									filledForm
											.setiVesselInspectionId(strVesselInspectionId);
									filledForm.setiFormSectionId(fs
											.getiFormSectionId());
									filledForm.setiFormSectionItemId(frmSecitm
											.getiFormSectionItemId());
									filledForm.setStrHeaderDesc("");
									filledForm.setStrFooterDesc("");
									filledForm.setSequence(seq);
									filledForm.setCreatedDate(format
											.format(cudate));
									filledForm.setModifiedDate(format
											.format(cudate));
									filledForm
											.setiTenantId(Integer.parseInt(CommonUtil
													.getTenantId(getActivity())));
									filledForm.setiShipId(Integer
											.parseInt(CommonUtil
													.getShipId(getActivity())));
									filledForm.setCreatedBy(CommonUtil
											.getUserId(getActivity()));
									filledForm.setModifiedBy(CommonUtil
											.getUserId(getActivity()));
									filledForm.setFlgIsDirty(1);
									/**
									 * insert blank record in db.
									 */
									db.insertFilldFormTable(filledForm);

									filedFormBySecList.add(filledForm);							
									
									
									VesselInspectionFormListDesc viflDescHeader = new VesselInspectionFormListDesc();
									
									viflDescHeader.setCreatedBy(CommonUtil
											.getUserId(getActivity()));
									viflDescHeader.setCreatedDate(format
											.format(cudate));
									viflDescHeader.setFlgDeleted(0);
									viflDescHeader.setFlgIsDeviceDirty(0);
									viflDescHeader.setFlgIsDirty(1);
									viflDescHeader.setFlgIsEdited(1);
									viflDescHeader.setFlgStatus(1);
									viflDescHeader.setiFilledFormId(filledForm.getiFilledFormId());
									viflDescHeader.setiFormSectionId(filledForm.getiFormSectionId());
									viflDescHeader.setiFormSectionItemId(filledForm.getiFormSectionItemId());
									viflDescHeader.setiVesselInspectionFormListDescId(getPk());
									viflDescHeader.setiShipId(Integer
											.parseInt(CommonUtil
													.getShipId(getActivity())));
									viflDescHeader.setiTenantId(Integer
											.parseInt(CommonUtil
													.getTenantId(getActivity())));
									viflDescHeader.setiVesselInspectionId(filledForm.getiVesselInspectionId());
									viflDescHeader.setModifiedBy(CommonUtil
											.getUserId(getActivity()));
									viflDescHeader.setModifiedDate(format
											.format(cudate));
									viflDescHeader.setSequence(filledForm.getSequence());
									viflDescHeader.setStrDescType(CommonUtil.DESC_TYPE_HEADER);
									viflDescHeader.setStrDesc("");
									
									db.insertVIFLDesc(viflDescHeader);
									
									VesselInspectionFormListDesc viflColorCondition = new VesselInspectionFormListDesc();
									
									viflColorCondition.setCreatedBy(CommonUtil
											.getUserId(getActivity()));
									viflColorCondition.setCreatedDate(format
											.format(cudate));
									viflColorCondition.setFlgDeleted(0);
									viflColorCondition.setFlgIsDeviceDirty(0);
									viflColorCondition.setFlgIsDirty(1);
									viflColorCondition.setFlgIsEdited(1);
									viflColorCondition.setFlgStatus(0);
									viflColorCondition.setiFilledFormId(filledForm.getiFilledFormId());
									viflColorCondition.setiFormSectionId(filledForm.getiFormSectionId());
									viflColorCondition.setiFormSectionItemId(filledForm.getiFormSectionItemId());
									viflColorCondition.setiVesselInspectionFormListDescId(getPk());
									viflColorCondition.setiShipId(Integer
											.parseInt(CommonUtil
													.getShipId(getActivity())));
									viflColorCondition.setiTenantId(Integer
											.parseInt(CommonUtil
													.getTenantId(getActivity())));
									viflColorCondition.setiVesselInspectionId(filledForm.getiVesselInspectionId());
									viflColorCondition.setModifiedBy(CommonUtil
											.getUserId(getActivity()));
									viflColorCondition.setModifiedDate(format
											.format(cudate));
									viflColorCondition.setSequence(filledForm.getSequence());
									viflColorCondition.setStrDescType(CommonUtil.COLOR_CONDITION);
									viflColorCondition.setStrDesc("");
									
									db.insertVIFLDesc(viflColorCondition);
								}

								seq++;
							}
						}
					}

					formSectionItem.add(filedFormBySecList);
				}
			}
		}

		db.close();
		/*
		 * groupItem.add("EXTERNAL CONDITION");
		 * groupItem.add("DECK CONDITION & MACHINERY");
		 * groupItem.add("CARGO HANDLING EQUIPMENT");
		 */
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(getActivity()) + "_"
				+ curDate.getTime();

		return pkId;
	}

	public void setChildGroupData() {
		/**
		 * Add Data For EXTERNAL CONDITION
		 */
		ArrayList<Simple> child = new ArrayList<Simple>();

		child.add(new Simple("Top sides", false, ""));
		child.add(new Simple("Anti fouling", false, ""));
		child.add(new Simple("Markings and Logos", false, ""));
		child.add(new Simple("Hull openings", false, ""));

		// childItem.add(child);

		/**
		 * Add Data For DECK CONDITION & MACHINERY
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Main deck", false, "test"));
		child.add(new Simple("Focsle", false, ""));
		child.add(new Simple("Accommodation decks", false, ""));
		child.add(new Simple("Poop", false, ""));
		child.add(new Simple("Monkey Island and fittings including masts",
				false, ""));

		// childItem.add(child);
		/**
		 * Add Data For CARGO HANDLING EQUIPMEN
		 */
		child = new ArrayList<Simple>();
		child.add(new Simple("Loadicator Coamings & compression bars", false,
				""));
		child.add(new Simple(
				"Cargo Securing material (twistlocks, turnbuckles)", false, ""));
		child.add(new Simple(
				"Container base sockets + lashing eyes on pontoons & pedestals",
				false, ""));
		child.add(new Simple("Lashing Bins", false, ""));
		child.add(new Simple(
				"Stevedore Safety arrangements (walkways, ladders, lighting, railings)",
				false, ""));

		// childItem.add(child);

	}

	@SuppressWarnings("unchecked")
	public void saveMiData() {
		if (miContext != null) {
			try {
				DBManager db = new DBManager(miContext);
				db.open();

				for (Object fsi : formSectionItem) {

					List<FilledForm> fildFrmItmList = (ArrayList<FilledForm>) fsi;

					for (FilledForm fldFrm : fildFrmItmList) {
						/*
						 * System.out.println("child Title : " +
						 * fldFrm.getStrHeaderDesc());
						 * System.out.println("child footer : " +
						 * fldFrm.getStrFooterDesc());
						 * System.out.println("child Desc : " +
						 * fldFrm.getCreatedBy());
						 */

						/*
						 * List<FilledForm> checkList = new
						 * ArrayList<FilledForm>(); checkList =
						 * db.getFilledFormDataById(fldFrm .getiFilledFormId());
						 * if (checkList == null || checkList.size() == 0) {
						 * db.insertFilldFormTable(fldFrm); } else {
						 * 
						 * db.updateFilldFormTable(fldFrm); }
						 */

					}
				}
				db.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void refreshAdapter() {
		MarpolInspectionFormAdapter mifAdapter = new MarpolInspectionFormAdapter(
				showAllGroup, miContext, formSection, formSectionItem);
		mifAdapter.setInflater((LayoutInflater) miContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
				(Activity) miContext);
		marpolFormList.setAdapter(mifAdapter);
		marpolFormList.expandGroup(0);
		marpolFormList.setSelectionFromTop(0, 1);
		marpolFormList.setSelectedChild(0, selectedChildPosition, true);
	}
}
