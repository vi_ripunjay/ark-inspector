package com.tams.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;
 

public class Exit extends Fragment {
	ListView lv;

	public Exit() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.exit, container,
				false);
		CommonUtil.setLoginFromShip(getActivity(), "0");
        getActivity().finish();	
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
		
		return rootView;
	}
	
	

}
