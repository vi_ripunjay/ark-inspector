package com.tams.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.tams.adapter.ShipListAdapter;
import com.tams.adapter.ShipListSyncAdapter;
import com.tams.model.ShipMaster;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.ShipSelectItem;
import com.tams.vessalinspection.R;
public class StartSync extends Fragment{
	
	public static Context mContext;
	public TextView setDateTimeText;
	public TextView startTextView;
	public static TextView shipError;
	//private SyncTab adminSyncTab;
	private Spinner shipDropDown;
	List<ShipMaster> shipList = new ArrayList<ShipMaster>();
	
	   @Override
	   public View onCreateView(LayoutInflater inflater,
	      ViewGroup container, Bundle savedInstanceState) {
	      // Inflate the layout for this fragment
		   View rootView = inflater.inflate(R.layout.startwizard, container, false);
		   mContext = getActivity().getApplicationContext();
		   
		   setDateTimeText = (TextView)rootView.findViewById(R.id.setDateTimeText);
		   startTextView = (TextView)rootView.findViewById(R.id.startTextView);
		   shipError = (TextView)rootView.findViewById(R.id.erroShipSelectForCableSync);
		   startTextView.setVisibility(View.VISIBLE);
		   setDateTimeText.setVisibility(View.GONE);
		   
		   shipDropDown = (Spinner)rootView.findViewById(R.id.shipDropDown);
		   CommonUtil.setSyncShipID(mContext, null);
		   DBManager db = new DBManager(mContext);
		   db.open();
		   	shipList = new ArrayList<ShipMaster>();
			shipList = db.getShipMasterData();

			ArrayList<ShipSelectItem> shipSelectlist = new ArrayList<ShipSelectItem>();
			shipSelectlist.add(new ShipSelectItem("Select Ship", "0", "0"));
			int activeShipIndex=0;
			if (shipList != null && shipList.size() > 0) {
				int index = 1;
				for (ShipMaster sm : shipList) {
					
					String shipId = String.valueOf(sm.getiShipId());
					String tenantId = String.valueOf(sm.getiTenantId());
					String shipName = sm.getStrShipName();
					shipSelectlist.add(new ShipSelectItem(shipName, shipId,
							tenantId));
					
					if(CommonUtil.getShipId(mContext) != null && CommonUtil.getShipId(mContext).equalsIgnoreCase(shipId)){
						activeShipIndex = index;
						CommonUtil.setSyncShipID(mContext, shipId);
						CommonUtil.setTenantID(mContext, tenantId);
					}
					
					index++;
				}
			}

			//ShipListAdapter myAdapter = new ShipListAdapter(shipSelectlist,	mContext);
			ShipListSyncAdapter myAdapter = new ShipListSyncAdapter(shipSelectlist,	mContext);
			shipDropDown.setAdapter(myAdapter);
			shipDropDown.setSelection(activeShipIndex, true);
			shipDropDown
			.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> parent, View arg1,
						int position, long arg3) {
					CommonUtil.setSyncShipID(mContext, null);
					String shipid = "";
					String tenantId = "";
					ShipSelectItem mydata;
					if (!(shipDropDown.getSelectedItem() == null)) {
						mydata = (ShipSelectItem) shipDropDown
								.getSelectedItem();
						shipid = mydata.getShipId();
						tenantId = mydata.getiTenantId();
						
						if (Integer.parseInt(shipid) > 0) {
							CommonUtil.setSyncShipID(mContext, shipid);
							CommonUtil.setTenantID(mContext, tenantId);
							
							if(CommonUtil.getShipId(mContext) == null){
								CommonUtil.setShipID(mContext, shipid);
							}
							//errorShipSelectView.setVisibility(View.GONE);
						} else {
							CommonUtil.setSyncShipID(mContext, null);
						}

					}
									
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					
				}
			});
			
		   db.close();
		   
	      return rootView;
	   }
	   

		private boolean canWriteToFlash() {
			String state = Environment.getExternalStorageState();
			System.out.println("state : " + state);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			} else if (Environment.MEDIA_SHARED.equals(state)) {
				// Read only isn't good enough

				return false;
			} else {
				return false;
			}
		}

		private boolean getMassStorageOn() {
			String state = Environment.getExternalStorageState();
			System.out.println("state : " + state);
			if (Environment.MEDIA_SHARED.equals(state) || Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			SyncTab.isConnected(mContext);
		}

		@Override
		public void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			try {
				if (SyncTab.myBroadcast != null) {
					mContext.unregisterReceiver(SyncTab.myBroadcast);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
