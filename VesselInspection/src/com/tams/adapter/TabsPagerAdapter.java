package com.tams.adapter;

import com.tams.fragment.CheckListTab;
import com.tams.fragment.MarpolInspectionTab;
import com.tams.fragment.VesselInspectionTab;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
     Fragment fragment = null;
		switch (index) {
		case 0:
			
		    fragment =  new CheckListTab();
			
		    return fragment;
			//break;
		case 1:
			
		    fragment = new VesselInspectionTab();
			return fragment;
	       // break;
		case 2:
			
		    fragment = new MarpolInspectionTab();
			return fragment;
	       // break;
	        
	        
		}

		return null;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {

	}
	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}
