package com.tams.adapter;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xml.sax.XMLReader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.Html.TagHandler;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tams.adapter.PinnedHeaderExpListView.PinnedHeaderAdapter;
import com.tams.fragment.MarpolInspectionTab;
import com.tams.model.FilledForm;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.model.VesselInspectionFormListDesc;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.VessalInspectionHome;

@SuppressWarnings("unchecked")
public class MarpolInspectionFormAdapter extends BaseExpandableListAdapter
		implements PinnedHeaderAdapter, OnScrollListener {

	Context mContext;
	private VessalInspectionHome mainActivity;
	public List<FormSection> groupItem;
	public List<FilledForm> tempChild;
	public List<Object> Childtem = new ArrayList<Object>();
	public LayoutInflater minflater;
	public Activity activity;
	public int childPositionForOuter = 0;
	public int showAllGroup = 0;
	
	RadioGroup radioGroup;
	RadioButton rbRed;
	RadioButton rbOrange;
	RadioButton rbYellow;
	RadioButton rbGreen;

	// private Uri[] mUrls;
	// String[] mFiles = null;

	public MarpolInspectionFormAdapter(Context context) {
		mContext = context;
	}

	public MarpolInspectionFormAdapter(int mshowAllGroup, Context context,
			List<FormSection> grList, List<Object> childItem) {
		mainActivity = (VessalInspectionHome) context;
		groupItem = grList;
		this.Childtem = childItem;
		showAllGroup = mshowAllGroup;
		mContext = context;
		if(this.Childtem == null || this.Childtem.size() <=0){
			Toast.makeText(mContext, "Items not found.", Toast.LENGTH_LONG).show();
		}
		else if(this.Childtem.size() > 0 && (((ArrayList<Object>)(this.Childtem.get(0))) == null || ((ArrayList<Object>)(this.Childtem.get(0))).size() == 0)){
			Toast.makeText(mContext, "Items not found.", Toast.LENGTH_LONG).show();
		}
	}

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.minflater = mInflater;
		activity = act;
	}

	@SuppressWarnings("unchecked")
	public Object getChild(int groupPosition, int childPosition) {
		return ((ArrayList<Object>) Childtem.get(groupPosition))
				.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@SuppressWarnings("unchecked")
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<Object>) Childtem.get(groupPosition)).size();
	}

	private class ViewHolderMI {
		TextView text;
		EditText strHeader;
		EditText strFooter;
		ImageButton imageButton;
		ImageButton imageButtonGlView;
		@SuppressWarnings("deprecation")
		private Gallery galleryForView;
		private int position;
		private Uri[] mUrls;
		String[] mFiles = null;
		LinearLayout myGalleryL;
		private CheckBox inspectedYes;
		private CheckBox inspectedNo;
		private EditText strDesc;
		private ImageButton commentBtn;
		private boolean showDesc = false;
		private WebView webView;

	}

	public TextView getGenericView() {
		// Layout parameters for the ExpandableListView
		AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 64);

		TextView textView = new TextView(mContext);
		textView.setLayoutParams(lp);
		// Center the text vertically
		textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
		// Set the text starting position
		textView.setPadding(36, 0, 0, 0);
		return textView;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		tempChild = (ArrayList<FilledForm>) Childtem.get(groupPosition);

		final ViewHolderMI holder;
		holder = new ViewHolderMI();

		convertView = minflater.inflate(R.layout.mi_form_childrow, null);

		holder.text = (TextView) convertView.findViewById(R.id.textView1);

		holder.webView = (WebView) convertView.findViewById(R.id.webViewMi);
		/**
		 * in case of marpole comment goes istrHeadeer sction.
		 */
		holder.strHeader = (EditText) convertView.findViewById(R.id.strDesc);

		holder.imageButton = (ImageButton) convertView
				.findViewById(R.id.miImageButtonCamara);

		holder.inspectedYes = (CheckBox) convertView
				.findViewById(R.id.checkInspectedYes);
		holder.inspectedNo = (CheckBox) convertView
				.findViewById(R.id.checkInspectedNo);
		holder.commentBtn = (ImageButton) convertView
				.findViewById(R.id.imageButtomComment);

		holder.position = childPosition;
		childPositionForOuter = holder.position;

		holder.galleryForView = (Gallery) convertView
				.findViewById(R.id.galleryImageRegular);
		holder.myGalleryL = (LinearLayout) convertView
				.findViewById(R.id.galleryImgRegular);
		
		radioGroup = (RadioGroup)convertView
				.findViewById(R.id.radioCondition);
		
		rbRed = (RadioButton)convertView
				.findViewById(R.id.radioRed);
		
		rbOrange = (RadioButton)convertView
				.findViewById(R.id.radioorng);
		
		rbYellow = (RadioButton)convertView
				.findViewById(R.id.radioYellow);
		
		rbGreen = (RadioButton)convertView
				.findViewById(R.id.radioGreen);
		

		List<FormSectionItem> formSecItem = new ArrayList<FormSectionItem>();
		DBManager db = new DBManager(activity);
		db.open();
		formSecItem = db.getFormSectionItemDataById(tempChild.get(
				holder.position).getiFormSectionItemId());

		holder.text.setText(formSecItem.get(0).getStrItemName());

		if ("yes".equalsIgnoreCase(tempChild.get(holder.position)
				.getFlgChecked())) {
			tempChild.get(holder.position).setFlgInspectedYes(true);
			tempChild.get(holder.position).setFlgInspectedNo(false);
			holder.inspectedYes.setChecked(true);
			holder.inspectedNo.setChecked(false);
		} else if ("no".equalsIgnoreCase(tempChild.get(holder.position)
				.getFlgChecked())) {
			tempChild.get(holder.position).setFlgInspectedYes(false);
			tempChild.get(holder.position).setFlgInspectedNo(true);
			holder.inspectedYes.setChecked(false);
			holder.inspectedNo.setChecked(true);
		}
		if (tempChild.get(holder.position).getFlgInspectedYes() != null)
			holder.inspectedYes.setChecked(tempChild.get(holder.position)
					.getFlgInspectedYes());
		if (tempChild.get(holder.position).getFlgInspectedNo() != null)
			holder.inspectedNo.setChecked(tempChild.get(holder.position)
					.getFlgInspectedNo());

		List<VesselInspectionFormListDesc> headerList = db
				.getVIFLDescForSectionItemByType(tempChild.get(holder.position)
						.getiVesselInspectionId(),
						tempChild.get(holder.position).getiFilledFormId(),
						tempChild.get(holder.position).getiFormSectionItemId(),
						CommonUtil.DESC_TYPE_HEADER);

		db.close();
		if (tempChild.get(holder.position).getFlgIsHeaderEdited() == 1 || (headerList != null && headerList.get(0).getFlgIsEdited() == 1)) {

			if (headerList != null && headerList.size() > 0) {

				holder.webView.loadData(CommonUtil.unmarshal(headerList.get(0).getStrDesc()),
						"text/html", null);
				holder.strHeader.setText("");

				holder.strHeader.setEnabled(false);
				holder.webView.setVisibility(View.VISIBLE);
				holder.strHeader.setVisibility(View.GONE);

			} else {

				holder.webView.loadData(CommonUtil.unmarshal(tempChild.get(holder.position)
						.getStrHeaderDesc()), "text/html", null);
				holder.strHeader.setText("");

				holder.strHeader.setEnabled(false);
				holder.webView.setVisibility(View.VISIBLE);
				holder.strHeader.setVisibility(View.GONE);
			}

		} else {

			if (headerList != null && headerList.size() > 0) {

				// holder.webViewHeader.loadData(headerList.get(0).getStrHeaderDesc(),
				// "text/html", null);
				holder.strHeader.setText(CommonUtil.unmarshal(headerList.get(0).getStrDesc()));

				holder.strHeader.setEnabled(true);
				holder.webView.setVisibility(View.GONE);
				holder.strHeader.setVisibility(View.VISIBLE);

			} else {
				/*
				 * holder.webViewHeader.loadData(tempChild.get(holder.position)
				 * .getStrHeaderDesc(), "text/html", null);
				 */
				holder.strHeader.setText(CommonUtil.unmarshal(tempChild.get(holder.position)
						.getStrHeaderDesc()));

				holder.strHeader.setEnabled(true);
				holder.webView.setVisibility(View.GONE);
				holder.strHeader.setVisibility(View.VISIBLE);
			}
		}

		holder.strHeader.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				holder.strHeader.requestFocus();

			}
		});
	

		holder.commentBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (tempChild.get(holder.position).isShowDesc()) {
					holder.strHeader.setVisibility(View.GONE);
					// holder.commentBtn.setVisibility(View.GONE);
					tempChild.get(holder.position).setShowDesc(false);
					// holder.commentBtn.setPressed(false);
				} else if (!tempChild.get(holder.position).isShowDesc()) {
					holder.strHeader.setVisibility(View.VISIBLE);
					holder.strHeader.requestFocus();
					tempChild.get(holder.position).setShowDesc(true);
					// holder.commentBtn.setPressed(true);

				}
				return true;
			}
		});

		holder.strHeader.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				// holder.strHeader.requestFocus();
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				// holder.strHeader.requestFocus();
				System.out
						.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			}

			@Override
			public void afterTextChanged(Editable s) {
				System.out.println("ccccccccccccccccccccccccccccc");

				// TODO Auto-generated method stub

				if (holder.position < tempChild.size()) {
					tempChild.get(holder.position).setStrHeaderDesc(
							s.toString());

					if (s != null && !"".equals(s.toString())) {
						tempChild.get(holder.position).setFlgIsDirty(1);
						CommonUtil.setEditedData(
								activity.getApplicationContext(), true);
					}
					/*
					 * else{ tempChild.get(holder.position).setFlgIsDirty(0); }
					 */

					/**
					 * Save data in db.
					 */
					FilledForm fldFrm = tempChild.get(holder.position);
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date curDate = new Date();
					fldFrm.setModifiedDate(df.format(curDate));
					fldFrm.setModifiedBy(CommonUtil.getUserId(activity));
					DBManager db = new DBManager(activity);
					db.open();
					List<FilledForm> checkList = new ArrayList<FilledForm>();
					checkList = db.getFilledFormDataById(fldFrm
							.getiFilledFormId());
					if (checkList == null || checkList.size() == 0) {

						db.insertFilldFormTable(fldFrm);
					} else {

						/**
						 * Conflict due to same time writing
						 */
						//db.updateFilldFormTable(fldFrm);
						db.updateDirtyRecordInMasterTable(
								DBHelper.VESSEL_INSPECTION,
								fldFrm.getiVesselInspectionId());
					}

					// Pushkar : Entry in VesselInspectionFormListDesc

					List<VesselInspectionFormListDesc> headerList = db
							.getVIFLDescForSectionItemByType(
									fldFrm.getiVesselInspectionId(),
									fldFrm.getiFilledFormId(),
									fldFrm.getiFormSectionItemId(),
									CommonUtil.DESC_TYPE_HEADER);

					if (headerList != null && headerList.size() > 0) {

						VesselInspectionFormListDesc viflDesc = headerList
								.get(0);
						viflDesc.setStrDesc(s.toString());
						viflDesc.setFlgIsDirty(1);
						viflDesc.setModifiedBy(CommonUtil.getUserId(mContext));
						viflDesc.setModifiedDate(df.format(curDate));
						db.updateVIFLDesc(viflDesc);

					} else {

						VesselInspectionFormListDesc viflDesc = new VesselInspectionFormListDesc();
						viflDesc.setiVesselInspectionFormListDescId(getPk());
						viflDesc.setiFilledFormId(fldFrm.getiFilledFormId());
						viflDesc.setiFormSectionId(fldFrm.getiFormSectionId());
						viflDesc.setiFormSectionItemId(fldFrm
								.getiFormSectionItemId());
						viflDesc.setiVesselInspectionId(fldFrm
								.getiVesselInspectionId());
						viflDesc.setCreatedBy(CommonUtil.getUserId(mContext));
						viflDesc.setCreatedDate(df.format(curDate));
						viflDesc.setFlgChecked(fldFrm.getFlgChecked());
						viflDesc.setFlgDeleted(fldFrm.getFlgDeleted());
						viflDesc.setFlgIsDeviceDirty(0);
						viflDesc.setFlgIsDirty(fldFrm.getFlgIsDirty());
						viflDesc.setFlgIsEdited(0);
						viflDesc.setFlgIsHeaderEdited(0);
						viflDesc.setFlgStatus(fldFrm.getFlgStatus());
						viflDesc.setiShipId(fldFrm.getiShipId());
						viflDesc.setiTenantId(fldFrm.getiTenantId());
						viflDesc.setSequence(fldFrm.getSequence());
						viflDesc.setModifiedBy(CommonUtil.getUserId(mContext));
						viflDesc.setModifiedDate(df.format(new Date()));
						viflDesc.setStrDesc(s.toString());
						viflDesc.setStrDescType(CommonUtil.DESC_TYPE_HEADER);

						db.insertVIFLDesc(viflDesc);

					}
					db.close();
				}
			}
		});

		holder.inspectedYes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				L.fv("View Id === : " + holder.position);
				// CheckBox cb = (CheckBox) v;
				if (holder.inspectedYes.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(true);
					tempChild.get(holder.position).setFlgInspectedNo(false);
					tempChild.get(holder.position).setFlgChecked("yes");
					holder.inspectedNo.setChecked(false);
					holder.inspectedYes.setChecked(true);
				} else if (!holder.inspectedYes.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(false);
					tempChild.get(holder.position).setFlgInspectedNo(true);
					tempChild.get(holder.position).setFlgChecked("no");
					holder.inspectedNo.setChecked(true);
					holder.inspectedYes.setChecked(false);
				}

				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);

				/**
				 * Save data in db
				 */
				FilledForm fldForm = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fldForm.setModifiedDate(df.format(curDate));
				fldForm.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();
				List<FilledForm> filledFormList = new ArrayList<FilledForm>();
				filledFormList = db.getFilledFormDataById(fldForm
						.getiFilledFormId());

				if (filledFormList == null || filledFormList.size() == 0) {
					if (fldForm.getFlgInspectedYes()) {
						fldForm.setFlgChecked("yes");
					} else if (fldForm.getFlgInspectedYes()) {
						fldForm.setFlgChecked("no");
					} else {
						fldForm.setFlgChecked(null);
					}
					db.insertFilldFormTable(fldForm);
				} else {
					if (fldForm.getFlgInspectedYes()) {
						fldForm.setFlgChecked("yes");
					} else if (fldForm.getFlgInspectedNo()) {
						fldForm.setFlgChecked("no");
					} else {
						fldForm.setFlgChecked(null);
					}
					db.updateFilldFormTable(fldForm);

					db.updateDirtyRecordInMasterTable(
							DBHelper.VESSEL_INSPECTION,
							fldForm.getiVesselInspectionId());
				}
				db.close();

			}
		});

		holder.inspectedNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// CheckBox cb = (CheckBox) v;
				if (holder.inspectedNo.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(false);
					tempChild.get(holder.position).setFlgInspectedNo(true);
					tempChild.get(holder.position).setFlgChecked("no");
					holder.inspectedNo.setChecked(true);
					holder.inspectedYes.setChecked(false);
				} else if (!holder.inspectedNo.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(true);
					tempChild.get(holder.position).setFlgInspectedNo(false);
					tempChild.get(holder.position).setFlgChecked("yes");
					holder.inspectedNo.setChecked(false);
					holder.inspectedYes.setChecked(true);
				}
				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);

				/**
				 * Save data in db
				 */
				FilledForm fldForm = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fldForm.setModifiedDate(df.format(curDate));
				fldForm.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();
				List<FilledForm> filledFormList = new ArrayList<FilledForm>();
				filledFormList = db.getFilledFormDataById(fldForm
						.getiFilledFormId());

				if (filledFormList == null || filledFormList.size() == 0) {
					if (fldForm.getFlgInspectedYes() != null
							&& fldForm.getFlgInspectedYes() == true) {
						fldForm.setFlgChecked("yes");
					} else if (fldForm.getFlgInspectedNo() != null
							&& fldForm.getFlgInspectedNo() == true) {
						fldForm.setFlgChecked("no");
					} else if (fldForm.getFlgInspectedYes() == null
							&& fldForm.getFlgInspectedNo() == null) {
						fldForm.setFlgChecked(null);
					}
					db.insertFilldFormTable(fldForm);
				} else {
					if (fldForm.getFlgInspectedYes() != null
							&& fldForm.getFlgInspectedYes() == true) {
						fldForm.setFlgChecked("yes");
					} else if (fldForm.getFlgInspectedNo() != null
							&& fldForm.getFlgInspectedNo() == true) {
						fldForm.setFlgChecked("no");
					} else if (fldForm.getFlgInspectedYes() == null
							&& fldForm.getFlgInspectedNo() == null) {
						fldForm.setFlgChecked(null);
					}
					db.updateFilldFormTable(fldForm);
					db.updateDirtyRecordInMasterTable(
							DBHelper.VESSEL_INSPECTION,
							fldForm.getiVesselInspectionId());
				}
				db.close();

			}
		});

		// Take picture Button

		holder.imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// take picture
				if (holder.position < tempChild.size()) {
					int ind = holder.position;
					VessalInspectionHome.viTabFlag = "mi";
					MarpolInspectionTab.selectedChildPosition = holder.position;
					mainActivity.takePicture(tempChild.get(ind));
					// tempChild.get(holder.position).setFlgIsDirty(1);
					CommonUtil.setEditedData(activity.getApplicationContext(),
							true);

					/**
					 * Save data in db.
					 */
					FilledForm fldFrm = tempChild.get(holder.position);
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date curDate = new Date();
					fldFrm.setModifiedDate(df.format(curDate));
					fldFrm.setModifiedBy(CommonUtil.getUserId(activity));
					DBManager db = new DBManager(activity);
					db.open();
					List<FilledForm> checkList = new ArrayList<FilledForm>();
					checkList = db.getFilledFormDataById(fldFrm
							.getiFilledFormId());
					if (checkList == null || checkList.size() == 0) {

						db.insertFilldFormTable(fldFrm);
					} else {

						/**
						 * Conflict due to same time writing
						 */
						//db.updateFilldFormTable(fldFrm);
						db.updateDirtyRecordInMasterTable(
								DBHelper.VESSEL_INSPECTION,
								fldFrm.getiVesselInspectionId());
					}
					db.close();
				}
			}
		});

		/*
		 * holder.imageButtonGlView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { if (holder.position <
		 * tempChild.size()) { int ind = holder.position;
		 * mainActivity.viewPicture(tempChild.get(ind)); }
		 * 
		 * } });
		 */

		// ggggggggggggggggggggggggggggggggggggggggggggggggggggggggg
		FilledForm flForm = new FilledForm();
		if (childPosition < tempChild.size()) {
			flForm = tempChild.get(childPosition);
		}
		
		
		DBManager dbManager = new DBManager(activity);
		dbManager.open();		
		
		VesselInspectionFormListDesc viflDesc4Color = null;
		
		List<VesselInspectionFormListDesc> viflColorDescList = dbManager
				.getVIFLDescForSectionItemByType(
						flForm
								.getiVesselInspectionId(),
						flForm
								.getiFilledFormId(),
						flForm
								.getiFormSectionItemId(),
						CommonUtil.COLOR_CONDITION);
		
		
		if(viflColorDescList != null && viflColorDescList.size() > 0){
			
			viflDesc4Color = viflColorDescList.get(0);		
			
		}else{
			//create new row of viflDesc for color condition
			
			Date cudate = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			viflDesc4Color = new VesselInspectionFormListDesc();			
			viflDesc4Color.setCreatedBy(CommonUtil
					.getUserId(activity));
			viflDesc4Color.setCreatedDate(format
					.format(cudate));
			viflDesc4Color.setFlgDeleted(0);
			viflDesc4Color.setFlgIsDeviceDirty(0);
			viflDesc4Color.setFlgIsDirty(1);
			viflDesc4Color.setFlgIsEdited(1);
			viflDesc4Color.setFlgStatus(0);
			viflDesc4Color.setiFilledFormId(flForm.getiFilledFormId());
			viflDesc4Color.setiFormSectionId(flForm.getiFormSectionId());
			viflDesc4Color.setiFormSectionItemId(flForm.getiFormSectionItemId());
			viflDesc4Color.setiVesselInspectionFormListDescId(getPk());
			viflDesc4Color.setiShipId(Integer
					.parseInt(CommonUtil
							.getShipId(activity)));
			viflDesc4Color.setiTenantId(Integer
					.parseInt(CommonUtil
							.getTenantId(activity)));
			viflDesc4Color.setiVesselInspectionId(flForm.getiVesselInspectionId());
			viflDesc4Color.setModifiedBy(CommonUtil
					.getUserId(activity));
			viflDesc4Color.setModifiedDate(format
					.format(cudate));
			viflDesc4Color.setSequence(flForm.getSequence());
			viflDesc4Color.setStrDescType(CommonUtil.COLOR_CONDITION);
			viflDesc4Color.setStrDesc("");
			
			dbManager.insertVIFLDesc(viflDesc4Color);
		}
		
		dbManager.close();
		if(viflDesc4Color.getFlgStatus() > 0){
			if(viflDesc4Color.getFlgStatus() == 1){
				rbRed.setChecked(true);
			}
			if(viflDesc4Color.getFlgStatus() == 2){
				rbOrange.setChecked(true);
			}
			if(viflDesc4Color.getFlgStatus() == 3){
				rbYellow.setChecked(true);
			}
			if(viflDesc4Color.getFlgStatus() == 4){
				rbGreen.setChecked(true);
			}
		}else{
			rbRed.setChecked(false);
			rbOrange.setChecked(false);
			rbYellow.setChecked(false);
			rbGreen.setChecked(false);
		}
		
		
		radioGroup
		.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				/*RadioButton rb = (RadioButton) group
						.findViewById(checkedId);
				String mode = rb.getText().toString();*/
				
				int selectedIndex = 0;
				
				int radioButtonID = group.getCheckedRadioButtonId();
				View radioButton = group.findViewById(radioButtonID);
				selectedIndex = group.indexOfChild(radioButton);

				if (selectedIndex+1 > 0) {
					
					DBManager db = new DBManager(activity);
					db.open();
					
					FilledForm fldForm = new FilledForm();
					if (holder.position < tempChild.size()) {
						fldForm = tempChild.get(holder.position);
					}
					VesselInspectionFormListDesc vifldesc = null;
					List<VesselInspectionFormListDesc> viflColorDescList = db
							.getVIFLDescForSectionItemByType(
									fldForm
											.getiVesselInspectionId(),
									fldForm
											.getiFilledFormId(),
									
									fldForm	.getiFormSectionItemId(),
									CommonUtil.COLOR_CONDITION);
					
					if(viflColorDescList != null && viflColorDescList.size() > 0){
						
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date curDate = new Date();
						vifldesc = viflColorDescList.get(0);
						
						vifldesc.setFlgStatus(selectedIndex+1);
						vifldesc.setFlgIsDirty(1);					
						vifldesc.setModifiedDate(df.format(curDate));
						vifldesc.setModifiedBy(CommonUtil.getUserId(mContext));
						db.updateVIFLDesc(vifldesc);
					}
					db.close();
				}

			}
		});

		String shipId = CommonUtil.getShipId(activity);
		String formId = flForm != null ? flForm.getiFilledFormId() : "form";
		// File storageDir = new File(Environment.getExternalStorageDirectory(),
		// "/theark/vi/process/" + shipId + "/" + formId);

		File storageDir = new File(Environment.getExternalStorageDirectory(),
				activity.getResources().getString(R.string.imagePath) + shipId
						+ "/" + formId);

		if (storageDir.exists()) {
			File[] imagesList = storageDir.listFiles();
			// L.fv("image list size :" + imagesList.length);
			holder.mFiles = new String[imagesList.length];

			for (int i = 0; i < imagesList.length; i++) {
				holder.mFiles[i] = imagesList[i].getAbsolutePath();
			}
			// L.fv("mFiles  size :" + mFiles.length);
			holder.mUrls = new Uri[holder.mFiles.length];

			for (int i = 0; i < holder.mFiles.length; i++) {
				holder.mUrls[i] = Uri.parse(holder.mFiles[i]);
			}
		}
		if (holder.mUrls != null && holder.mUrls.length > 0) {

			/*
			 * Uncomment for linear layout gallery
			 */
			int i = 0;
			for (String str : holder.mFiles) {
				holder.myGalleryL.addView(insertPhoto(str, i, holder.position));
				i++;
			}

			holder.galleryForView.setAdapter(new ImageAdapter(activity,
					holder.mUrls));

			holder.galleryForView
					.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) {

							if (holder.position < tempChild.size()) {
								VessalInspectionHome.viTabFlag = "mi";
								mainActivity.viewPicture(tempChild
										.get(holder.position));
							}

						}
					});
		} else {
			// Toast.makeText(activity, "Form image not found",
			// Toast.LENGTH_SHORT).show();
		}

		return convertView;
	}

	View insertPhoto(String str, int id, int childOuterPosition) {
		// TODO Auto-generated method stub

		final int pos = childOuterPosition;
		Bitmap bm = decodeSampledBitmapFromUri(str, 100, 100);

		LinearLayout layout = new LinearLayout(activity);
		layout.setLayoutParams(new LayoutParams(150, 150));
		layout.setGravity(Gravity.CENTER);
		layout.setId(id);
		// layout.setPadding(0, 0, 2, 0);

		final ImageView imageView = new ImageView(activity);
		imageView.setLayoutParams(new LayoutParams(120, 120));
		imageView.setId(id);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setImageBitmap(bm);

		layout.addView(imageView);

		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				VessalInspectionHome.viTabFlag = "mi";
				mainActivity.viewPictureForGl(tempChild.get(pos),
						imageView.getId());
				// Toast.makeText(activity, "Image View ",
				// Toast.LENGTH_LONG).show();
			}
		});
		return layout;
	}

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public Object getGroup(int groupPosition) {
		return groupItem.get(groupPosition);
	}

	public int getGroupCount() {
		return groupItem.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	/**
	 * ripunjay
	 */
	@SuppressLint("NewApi")
	public void configurePinnedHeader(View v, int position, int alpha) {
		CheckedTextView header = (CheckedTextView) v;
		final String title = ((FormSection) getGroup(position))
				.getStrSectionName();

		header.setText(title);
		if (showAllGroup == 1) {
			header.setBackground(activity.getResources().getDrawable(
					R.drawable.active_header));
			header.setChecked(true);
		} else if (showAllGroup == 2) {
			header.setBackground(activity.getResources().getDrawable(
					R.drawable.myrect));
			header.setChecked(false);
		}

		if (alpha == 255) {
			/*
			 * header.setBackgroundColor(mPinnedHeaderBackgroundColor);
			 * header.setTextColor(mPinnedHeaderTextColor);
			 */
		} else {
			/*
			 * header.setBackgroundColor(Color.argb(alpha,
			 * Color.red(mPinnedHeaderBackgroundColor),
			 * Color.green(mPinnedHeaderBackgroundColor),
			 * Color.blue(mPinnedHeaderBackgroundColor)));
			 * header.setTextColor(Color.argb(alpha,
			 * Color.red(mPinnedHeaderTextColor),
			 * Color.green(mPinnedHeaderTextColor),
			 * Color.blue(mPinnedHeaderTextColor)));
			 */
		}
	}

	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (view instanceof PinnedHeaderMarpolInspListView) {
			((PinnedHeaderMarpolInspListView) view)
					.configureHeaderView(firstVisibleItem);
		}

	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@SuppressLint("NewApi")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.grouprow, parent, false);
		}
		((CheckedTextView) convertView).setText(groupItem.get(groupPosition)
				.getStrSectionName());
		((CheckedTextView) convertView).setChecked(isExpanded);

		if (showAllGroup == 1) {
			convertView.setBackground(activity.getResources().getDrawable(
					R.drawable.active_header));
		} else if (showAllGroup == 2) {
			convertView.setBackground(activity.getResources().getDrawable(
					R.drawable.myrect));
		}

		convertView.invalidate();

		return convertView;
	}

	public class ImageAdapter extends BaseAdapter {
		private Context context;
		private int itemBackground;
		private Uri[] mUrl = null;

		public ImageAdapter(Context c, Uri[] mUrl) {
			context = c;

			this.mUrl = mUrl;
			// sets a grey background; wraps around the images
			TypedArray a = context
					.obtainStyledAttributes(R.styleable.MyGallery);
			itemBackground = a.getResourceId(
					R.styleable.MyGallery_android_galleryItemBackground, 0);
			a.recycle();
			// L.fv("Step 4.1 : enter in image adapter");
		}

		// returns the number of images
		public int getCount() {
			return (mUrl != null ? mUrl.length : 0);
		}

		// returns the ID of an item
		public Object getItem(int position) {
			return position;
		}

		// returns the ID of an item
		public long getItemId(int position) {
			return position;
		}

		// returns an ImageView view
		@SuppressWarnings("deprecation")
		public View getView(int position, View convertView, ViewGroup parent) {
			// L.fv("Step 4.2 : Image adaptor getView Method");
			ImageView imageView = new ImageView(context);

			Drawable toRecycle = imageView.getDrawable();
			if (toRecycle != null) {
				((BitmapDrawable) imageView.getDrawable()).getBitmap()
						.recycle();
			}

			imageView.setImageBitmap(decodeSampledBitmapFromResource(
					context.getResources(), R.id.formImage, 100, 100));

			// imageView.setImageURI(mUrl[position]);
			// imageView.setImageResource(imageIDs[position]);
			imageView.setLayoutParams(new Gallery.LayoutParams(50, 50));
			imageView.setBackgroundResource(itemBackground);
			return imageView;
		}

		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
				int reqWidth, int reqHeight) {

			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(res, resId, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeResource(res, resId, options);
		}
	}

	public class ListTagHandler implements TagHandler {
		boolean first = true;

		@Override
		public void handleTag(boolean opening, String tag, Editable output,
				XMLReader xmlReader) {
			// TODO Auto-generated method stub
			if (tag.equals("li")) {
				char lastChar = 0;
				if (output.length() > 0)
					lastChar = output.charAt(output.length() - 1);
				if (first) {
					if (lastChar == '\n')
						output.append("\t*  ");
					else
						output.append("\n\t*  ");
					first = false;
				} else {
					first = true;
				}
			}

		}
	}

	public String getPk() {
		Date curDate = new Date();
		String pkId = CommonUtil.getMacId(mContext) + "_" + curDate.getTime();

		return pkId;
	}
}
