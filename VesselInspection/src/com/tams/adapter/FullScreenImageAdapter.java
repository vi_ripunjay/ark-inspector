package com.tams.adapter;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tams.model.FilledFormImages;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.SimpleImageInfo;
import com.tams.vessalinspection.R;

public class FullScreenImageAdapter extends PagerAdapter {

	private Context _activity;
	private String[] _imagePaths;
	private LayoutInflater inflater;
	private List<FilledFormImages> imageList;
	DBManager db;

	// constructor
	public FullScreenImageAdapter(Context context, String[] imagePaths) {
		this._activity = context;
		this._imagePaths = imagePaths;
		db = new DBManager(_activity);
	}

	@Override
	public int getCount() {
		return this._imagePaths.length;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TouchImageView imgDisplay;
		ImageView image;
		// Button btnClose;

		inflater = (LayoutInflater) _activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(R.layout.image_list, container,
				false);
		
		final EditText imageComment = (EditText)viewLayout.findViewById(R.id.imageComment);
		imageComment.setId(position);		
		
		imageList =  new ArrayList<FilledFormImages>();
		db.open();
		//imageList = db.getFilledFormImagesByImagePath("/file:"+_imagePaths[position]);
		imageList = db.getFilledFormImagesByImagePath(_imagePaths[position]);
		db.close();
		if(imageList != null && imageList.size() > 0){
			if(imageList.get(0).getStrDesc() != null && !"null".equalsIgnoreCase(imageList.get(0).getStrDesc())){
				imageComment.setText(imageList.get(0).getStrDesc());
			}
			else{
				imageComment.setText("");
			}
		}
		
		
		imageComment.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
								
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(s != null){					
					imageList =  new ArrayList<FilledFormImages>();
					db.open();
					//imageList = db.getFilledFormImagesByImagePath("/file:"+_imagePaths[imageComment.getId()]);
					imageList = db.getFilledFormImagesByImagePath(_imagePaths[imageComment.getId()]);
					db.close();
					if(imageList != null && imageList.size() > 0){
						FilledFormImages filledFormImage = imageList.get(0);
						
						String imageName[] =  filledFormImage.getDeviceOriginalImagePath().split("/");
						if(imageName != null && imageName.length > 0 && "Image".equalsIgnoreCase(filledFormImage.getStrImageName())){
							filledFormImage.setStrImageName(imageName[imageName.length-1]);
						}						
						filledFormImage.setStrDesc(("".equals(s.toString().trim()) ? null : s.toString()));
						filledFormImage.setFlgIsDirty(1);
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date curDate = new Date();
						filledFormImage.setModifiedDate(df.format(curDate));
						CommonUtil.setEditedData(_activity, true);
						db.open();
						db.updateFilldFormImageTable(filledFormImage);
						db.close();
					}					
				}
			}
			
		});
		

		// imgDisplay = (TouchImageView)
		// viewLayout.findViewById(R.id.imgDisplay);

		image = (ImageView) viewLayout.findViewById(R.id.imageView1);

		// btnClose = (Button) viewLayout.findViewById(R.id.btnClose);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		// Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position),
		// options);

		// Bitmap bitmap =
		// BitmapFactory.decodeResource(_activity.getResources(),
		// _imagePaths[position]);
		SimpleImageInfo sii;
		Double height = 768.0;
		Double width = 1024.0;
		Double aspectratio = 1.0;
		DecimalFormat decFormat = new DecimalFormat("##");
		try {
			sii = new SimpleImageInfo(new File(_imagePaths[position]));

			if (sii.getHeight() > sii.getWidth()) {
				height = 1024.0;
				width = 768.0;
				aspectratio = (double) sii.getHeight()
						/ (double) sii.getWidth();
			} else {
				height = 768.0;
				width = 1024.0;
				aspectratio = (double) sii.getWidth()
						/ (double) sii.getHeight();
			}

			if (sii.getHeight() > height) {
				height = height * aspectratio;
				width = width * aspectratio;
			} else {
				height = (double) sii.getHeight();
				width = (double) sii.getWidth();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Bitmap bitmap = decodeSampledBitmapFromUri(_imagePaths[position], Integer.parseInt(decFormat.format(width)), Integer.parseInt(decFormat.format(height)));

		/*
		 * Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths[position]);
		 */
		// Bitmap bitmap = BitmapFactory.decodeFile(_activity.getResources(),
		// _imagePaths[position]);

		// imgDisplay.setImageBitmap(bitmap);

		//Picasso.with(_activity).load(_imagePaths[position]).into(image);
		image.setImageBitmap(bitmap);

		// close button click event
		/*
		 * btnClose.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { _activity.finish(); } });
		 */
		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}
	
	
	

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}
	
	

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

	private Object getContentResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}

}
