package com.tams.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.tams.fragment.AdminFragment;
import com.tams.fragment.VesselInspectionStartFragment;
import com.tams.model.ShipMaster;
import com.tams.model.VesselInspection;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.SyncHandler;
import com.tams.vessalinspection.VessalInspectionHome;
import com.tams.vessalinspection.VesselInspectionStartActivity;

public class VesselInspectionDraftAdapter extends BaseAdapter implements
		ListAdapter {
	private List<VesselInspection> list = new ArrayList<VesselInspection>();
	private Context context;
	FragmentManager fragmentManager;

	public VesselInspectionDraftAdapter(List<VesselInspection> viDraftList,
			Context context, FragmentManager fragmentManager) {
		this.list = viDraftList;
		this.context = context;
		this.fragmentManager = fragmentManager;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.vessel_inspection_draft_adapter, null);

		// Handle TextView and display string from your list
		final TextView viShipName = (TextView) view
				.findViewById(R.id.viShipName);
		final TextView viDate = (TextView) view.findViewById(R.id.viDate);

		final TextView activeInspection = (TextView) view
				.findViewById(R.id.activeInspectionId);

		int shipId = list.get(position).getiShipId();
		DBManager db = new DBManager(context);
		db.open();
		List<ShipMaster> smList = db.getShipMasterDataById(String
				.valueOf(shipId));
		db.close();
		if (smList != null && smList.size() > 0) {
			viShipName.setText(smList.get(0).getStrShipName());
		} else {
			viShipName.setText("");
		}

		viShipName.setTextColor(context.getResources().getColor(R.color.black));

		String dt = list.get(position).getInspectionDate();
		String dtArray[] = dt.split("-");

		viDate.setText(dtArray[2] + " "
				+ getMonth(Integer.parseInt(dtArray[1])) + " " + dtArray[0]);
		viDate.setTextColor(context.getResources().getColor(R.color.black));

		view.setBackgroundColor(Color.parseColor("#ffffff"));
		if (list.get(position).getFlgStatus() == -1)
		{
			activeInspection.setText("Inactive");
		}
		else if (!(list.get(position).getFlgStatus() == -1)){
			activeInspection.setText("Active");
		}

		activeInspection.setId(position);

		activeInspection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DBManager dbManager = new DBManager(context);
				dbManager.open();
				String activateMessage = "";
				if (activeInspection.getText().toString() != null
						&& "Active".equals(activeInspection.getText()
								.toString())) {
					
					DBManager db = new DBManager(context);
					db.open();
					long dirtyCount = db.getVesselInspectionDirtyData(list.get(activeInspection.getId()).getiVesselInspectionId());
					if(dirtyCount > 0){
						activateMessage = context.getResources().getString(
								R.string.deActivateMsg)
								+ " '"
								+ viShipName.getText().toString()
								+ " "
								+ viDate.getText().toString() + "'  ? ";
					}
					else{
						activateMessage = context.getResources().getString(
								R.string.deActivateUndirtyMsg);
					}
					

					openPopupForDeactivateInspection(activateMessage,
							activeInspection);

				} else {
					
					StringBuffer dirtyInspection = new StringBuffer();
					int dirtyCounter = 0;
					try 
					{
						DBManager db = new DBManager(context);
						db.open();
						List<VesselInspection> vslActiveList = db.getVesselInspectionDataByflgStatus(0);
						if(vslActiveList != null && vslActiveList.size() > 0){
							for(VesselInspection vslInsp : vslActiveList){
								long dirtyCount = db.getVesselInspectionDirtyData(vslInsp.getiVesselInspectionId());
								if(dirtyCount > 0){
									
									String shipName = db.getShipNameById(vslInsp.getiShipId());
									if(!(dirtyInspection.indexOf(shipName) > 0)){
										if(dirtyCounter > 0){
											dirtyInspection.append(","+shipName);
										}
										else{
											dirtyInspection.append(""+shipName);
										}
									}
									dirtyCounter++;
								}
							}
						}
						
						db.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					StringBuffer combinedDirtyMsg = new StringBuffer();
					
					if(dirtyCounter > 0){ 
						
						combinedDirtyMsg.append(
								context.getResources().getString(
										R.string.dirtyVIMsgActive1)
										+ dirtyInspection.toString()
										+ "\n"
										+ context.getResources().getString(
												R.string.dirtyVIMsgActive2)
												+"\n");
					}	
					
					
					activateMessage = context.getResources().getString(
							R.string.activateMsg1)
							+ " '"
							+ viShipName.getText().toString()
							+ " "
							+ viDate.getText().toString()
							+ "' . "
							+ context.getResources().getString(
									R.string.activateMsg2);

					openPopupForActivateInspection(combinedDirtyMsg.toString() + activateMessage,
							activeInspection,dirtyCounter);
					VesselInspectionStartFragment.lastActiveInspection = list
							.get(activeInspection.getId());

				}

			}
		});

		return view;
	}

	private String getMonth(int month) {
		String monthName = "";
		switch (month) {
		case 1:
			monthName = "Jan";
			break;
		case 2:
			monthName = "Feb";
			break;
		case 3:
			monthName = "Mar";
			break;
		case 4:
			monthName = "Apr";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "Jun";
			break;
		case 7:
			monthName = "Jul";
			break;
		case 8:
			monthName = "Aug";
			break;
		case 9:
			monthName = "Sep";
			break;
		case 10:
			monthName = "Oct";
			break;
		case 11:
			monthName = "Nov";
			break;
		case 12:
			monthName = "Dec";
			break;

		default:
			break;
		}

		return monthName;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void openPopupForActivateInspection(String Message,
			final TextView activeInspection, final int dirtyCounter) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_logout);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button sync = (Button) dialog.findViewById(R.id.sync);

		sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * / Syncing process
				 */
				dialog.dismiss();
				
				/**
				 * Before create new inspection need to delete undirty 
				 * data from device.
				 */
				if(dirtyCounter > 0){
					CommonUtil.deleteAllRelatedDataOfDirtyInspection(context);
				}
				else{
					CommonUtil.deleteAllRelatedDataOfUndirtyInspection(context);
				}

				CommonUtil.setActiveInspectionID(context,
						list.get(activeInspection.getId())
								.getiVesselInspectionId());
				CommonUtil.setDropDbFlag(context, "0");

				String dataSyncMode = CommonUtil.getDataSyncModeFromDb(context);
				if (dataSyncMode != null
						&& "cable".equalsIgnoreCase(dataSyncMode)) {
					
					if (VessalInspectionHome.miActionProgressItem != null
							&& "home"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VessalInspectionHome.miActionProgressItem
								.setVisible(false);
					} else if (VesselInspectionStartActivity.miActionProgressItem != null
							&& "start"
									.equalsIgnoreCase(AdminFragment.homeOrStart)) {
						VesselInspectionStartActivity.miActionProgressItem
								.setVisible(false);
						VesselInspectionStartActivity.miActionUndoItem
								.setVisible(false);
					}
					/**
					 * In case of cable open sync tab for sync.
					 */

					LayoutInflater inflater = (LayoutInflater) context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					Fragment fr = new AdminFragment(1);
					while (fragmentManager.getBackStackEntryCount() > 1) {
						fragmentManager.popBackStackImmediate();
					}

					fragmentManager
							.beginTransaction()
							.replace(
									inflater.inflate(
											R.layout.activity_start_fragment,
											null)
											.findViewById(
													R.id.frame_container_start)
											.getId(), fr).addToBackStack(null)
							.commitAllowingStateLoss();

					/*
					 * CommonUtil.launchPopUpEnablingNetwork(context
					 * .getResources() .getString(R.string.changeCableToWeb),
					 * context);
					 */
				} else {

					if (CommonUtil.checkConnectivity(context)) {
						if (VessalInspectionHome.miActionProgressItem != null
								&& "home"
										.equalsIgnoreCase(AdminFragment.homeOrStart)) {
							VessalInspectionHome.miActionProgressItem
									.setVisible(true);
						} else if (VesselInspectionStartActivity.miActionProgressItem != null
								&& "start"
										.equalsIgnoreCase(AdminFragment.homeOrStart)) {
							VesselInspectionStartActivity.miActionProgressItem
									.setVisible(true);
							VesselInspectionStartActivity.miActionUndoItem
									.setVisible(false);
						}

						CommonUtil.setSynckFrom(context, "process");
						CommonUtil.setUserLogin(context, "1");

						CommonUtil.setCallingThrough(context, "activate");
						CommonUtil.setActivatingShip(
								context,
								String.valueOf(list.get(
										activeInspection.getId()).getiShipId()));

						SyncHandler.context = context;
						Message shipMessage = new Message();
						// shipMessage.what =
						// SyncHandler.MSG_UPDATE_SHIP_MASETR;
						shipMessage.what = SyncHandler.MSG_GET_SHIP_EXIST;
						SyncHandler.handler.sendMessage(shipMessage);
						// syncDate.setText(getSyncInfo()[0]);
					}
				}
			}
		});
		Button btnCancel = (Button) dialog.findViewById(R.id.logOut);
		btnCancel.setText("Cancel");
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/**
				 * Log out process
				 */
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	private void openPopupForDeactivateInspection(String Message,
			final TextView activeInspection) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_logout);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button sync = (Button) dialog.findViewById(R.id.sync);
		sync.setText(" Ok ");

		sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();

				DBManager dbManager = new DBManager(context);
				dbManager.open();
				list.get(activeInspection.getId()).setFlgStatus(-1);
				dbManager.updateVesselInspectionTable(list.get(activeInspection
						.getId()));
				dbManager.close();
				/**
				 * Deleted all related data after confirmation.
				 */
				CommonUtil.deleteAllRelatedDataOfGivenInspection(context, list.get(activeInspection.getId()).getiVesselInspectionId());
				
				activeInspection.setText("Inactive");
				if (context instanceof VesselInspectionStartActivity) {
					((VesselInspectionStartActivity) context).finish();
					Intent i = new Intent(context,
							VesselInspectionStartActivity.class);
					context.startActivity(i);
				}
			}
		});
		Button btnCancel = (Button) dialog.findViewById(R.id.logOut);
		btnCancel.setText(" Cancel ");
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/**
				 * Log out process
				 */
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}
}