package com.tams.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tams.fragment.AdminTab;
import com.tams.fragment.SyncTab;

public class AdminTabsPagerAdapter extends FragmentPagerAdapter {

	public AdminTabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
     Fragment fragment = null;
		switch (index) {
		case 0:
			
		    fragment =  new AdminTab();
			
		    return fragment;
			//break;
		case 1:
			
		    fragment = new SyncTab();
			return fragment;
	       // break;	        
	        
		}

		return null;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {

	}
	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
