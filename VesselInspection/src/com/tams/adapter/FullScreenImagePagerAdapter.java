package com.tams.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tams.vessalinspection.R;

public class FullScreenImagePagerAdapter extends PagerAdapter {

	private Activity _activity;
	private ArrayList<Integer> _imagePaths;
	private LayoutInflater inflater;

	// constructor
	public FullScreenImagePagerAdapter(Activity activity,
			ArrayList<Integer> imagePaths) {
		this._activity = activity;
		this._imagePaths = imagePaths;
	}

	@Override
	public int getCount() {
		return this._imagePaths.size();
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
		// TODO Auto-generated method stub
	}
	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
     //   TouchImageView imgDisplay;
        ImageView image;
		Button btnClose;
 
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
 
     //   imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        
        image = (ImageView) viewLayout.findViewById(R.id.imageView1);
        
        
//        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    //    Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
        
        Bitmap bitmap = BitmapFactory.decodeResource(_activity.getResources(), _imagePaths.get(position));
    //    imgDisplay.setImageBitmap(bitmap);
        
        image.setImageBitmap(bitmap);
        
        // close button click event
  /*      btnClose.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				_activity.finish();
			}
		}); 
*/
        ((ViewPager) container).addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }

}
