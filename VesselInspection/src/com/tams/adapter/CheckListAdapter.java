package com.tams.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tams.adapter.PinnedHeaderExpListView.PinnedHeaderAdapter;
import com.tams.model.CheckListSection;
import com.tams.model.CheckListSectionItems;
import com.tams.model.FilledCheckList;
import com.tams.sql.DBHelper;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.utils.L;
import com.tams.vessalinspection.R;

@SuppressWarnings("unchecked")
public class CheckListAdapter extends BaseExpandableListAdapter implements
		PinnedHeaderAdapter, OnScrollListener {

	Context mContext;
	public ArrayList<CheckListSection> groupItem;
	public ArrayList<FilledCheckList> tempChild;
	public ArrayList<Object> Childtem = new ArrayList<Object>();
	public LayoutInflater minflater;
	public Activity activity;
	// TextView text = null;
	// CheckBox inspectedYes;
	// CheckBox inspectedNo;
	// EditText comment = null;
	public int showAllGroup = 0;

	public CheckListAdapter(Context context) {

		mContext = context;
	}

	public CheckListAdapter(int mshowAllGroup,
			ArrayList<CheckListSection> grList, ArrayList<Object> childItem,
			Context context) {
		groupItem = grList;
		this.Childtem = childItem;
		showAllGroup = mshowAllGroup;
		mContext = context;
		
		if(this.Childtem == null || this.Childtem.size() <=0){
			Toast.makeText(mContext, "Items not found.", Toast.LENGTH_LONG).show();
		}
		else if(this.Childtem.size() > 0 && (((ArrayList<Object>)(this.Childtem.get(0))) == null || ((ArrayList<Object>)(this.Childtem.get(0))).size() == 0)){
			Toast.makeText(mContext, "Items not found.", Toast.LENGTH_LONG).show();
		}
	}

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.minflater = mInflater;
		activity = act;
	}

	@SuppressWarnings("unchecked")
	public Object getChild(int groupPosition, int childPosition) {
		return ((ArrayList<Object>) Childtem.get(groupPosition))
				.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@SuppressWarnings("unchecked")
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<Object>) Childtem.get(groupPosition)).size();
	}

	private class ViewHolder {

		public ViewHolder() {

		}

		private TextView text;
		private CheckBox inspectedYes;
		private CheckBox inspectedNo;
		private EditText strDesc;
		private ImageButton commentBtn;
		private int position;
		private boolean showDesc = false;

	}

	public TextView getGenericView() {
		// Layout parameters for the ExpandableListView
		AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 64);

		TextView textView = new TextView(mContext);
		textView.setLayoutParams(lp);
		// Center the text vertically
		textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
		// Set the text starting position
		textView.setPadding(36, 0, 0, 0);
		return textView;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		tempChild = (ArrayList<FilledCheckList>) Childtem.get(groupPosition);

		final ViewHolder holder;
		holder = new ViewHolder();
		// L.fv(" groupPosition  : "+groupPosition+" childPosition : "+childPosition);

		convertView = minflater.inflate(R.layout.childrow, null);

		holder.text = (TextView) convertView.findViewById(R.id.textView1);
		holder.strDesc = (EditText) convertView.findViewById(R.id.strDesc);
		holder.inspectedYes = (CheckBox) convertView
				.findViewById(R.id.checkInspectedYes);
		holder.inspectedNo = (CheckBox) convertView
				.findViewById(R.id.checkInspectedNo);
		holder.commentBtn = (ImageButton) convertView
				.findViewById(R.id.imageButtomComment);
		holder.position = childPosition;

		convertView.setTag(holder);

		holder.strDesc.setVisibility(View.VISIBLE);
		/*if (tempChild.get(holder.position).isShowDesc()) {
			holder.strDesc.setVisibility(View.VISIBLE);
			// holder.commentBtn.setVisibility(View.GONE);
			holder.commentBtn.setPressed(true);
		} else if (!tempChild.get(holder.position).isShowDesc()) {
			holder.strDesc.setVisibility(View.GONE);
			// holder.commentBtn.setVisibility(View.VISIBLE);
			holder.commentBtn.setPressed(false);

		}*/

		List<CheckListSectionItems> checkSecItmlst = new ArrayList<CheckListSectionItems>();
		DBManager db = new DBManager(activity);
		db.open();
		checkSecItmlst = db.getCheckListSectionItemsDataById(tempChild.get(
				holder.position).getiCheckListSectionItemsId());
		db.close();

		holder.text.setText(checkSecItmlst.get(0).getStrItemNames());

		if ("yes".equalsIgnoreCase(tempChild.get(holder.position)
				.getFlgChecked())) {
			tempChild.get(holder.position).setFlgInspectedYes(true);
			tempChild.get(holder.position).setFlgInspectedNo(false);
			holder.inspectedYes.setChecked(true);
			holder.inspectedNo.setChecked(false);
		} else if ("no".equalsIgnoreCase(tempChild.get(holder.position)
				.getFlgChecked())) {
			tempChild.get(holder.position).setFlgInspectedYes(false);
			tempChild.get(holder.position).setFlgInspectedNo(true);
			holder.inspectedYes.setChecked(false);
			holder.inspectedNo.setChecked(true);
		}
		if (tempChild.get(holder.position).isFlgInspectedYes() != null)
			holder.inspectedYes.setChecked(tempChild.get(holder.position)
					.isFlgInspectedYes());
		if (tempChild.get(holder.position).isFlgInspectedNo() != null)
			holder.inspectedNo.setChecked(tempChild.get(holder.position)
					.isFlgInspectedNo());

		// holder.strDesc.setText(tempChild.get(holder.position).getStrDesc());
		// holder.strDesc.setText(tempChild.get(holder.position).getStrDesc());
		if (tempChild.get(holder.position).getStrRemarks() != null
				&& !"null".equalsIgnoreCase(tempChild.get(holder.position)
						.getStrRemarks())) {
			holder.strDesc.setText(tempChild.get(holder.position)
					.getStrRemarks());
		} else {
			holder.strDesc.setText("");
		}
		/*
		 * holder.inspectedYes.setOnCheckedChangeListener(new
		 * OnCheckedChangeListener() {
		 * 
		 * @Override public void onCheckedChanged(CompoundButton buttonView,
		 * boolean isChecked) { Toast.makeText(activity,
		 * "holder:position : "+holder.position, 2000).show(); if
		 * (holder.inspectedYes.isChecked()) {
		 * tempChild.get(holder.position).setFlgInspectedYes(true);
		 * tempChild.get(holder.position).setFlgInspectedNo(false);
		 * holder.inspectedNo.setChecked(false);
		 * holder.inspectedYes.setChecked(true); } else
		 * if(!holder.inspectedYes.isChecked()){
		 * tempChild.get(holder.position).setFlgInspectedYes(false);
		 * tempChild.get(holder.position).setFlgInspectedNo(true);
		 * holder.inspectedNo.setChecked(true);
		 * holder.inspectedYes.setChecked(false); }
		 * 
		 * } });
		 */
		holder.inspectedYes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				L.fv("View Id === : " + holder.position);
				// CheckBox cb = (CheckBox) v;
				if (holder.inspectedYes.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(true);
					tempChild.get(holder.position).setFlgInspectedNo(false);
					tempChild.get(holder.position).setFlgChecked("yes");
					holder.inspectedNo.setChecked(false);
					holder.inspectedYes.setChecked(true);
				} else if (!holder.inspectedYes.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(false);
					tempChild.get(holder.position).setFlgInspectedNo(true);
					tempChild.get(holder.position).setFlgChecked("no");
					holder.inspectedNo.setChecked(true);
					holder.inspectedYes.setChecked(false);
				}

				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);

				/**
				 * Save data in db
				 */
				FilledCheckList fcl = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fcl.setModifiedDate(df.format(curDate));
				fcl.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();

				List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
				checkList = db.getFilledCheckListDataById(fcl
						.getiFilledCheckListId());
				if (checkList == null || checkList.size() == 0) {
					if (fcl.isFlgInspectedYes()) {
						fcl.setFlgChecked("yes");
					} else if (fcl.isFlgInspectedNo()) {
						fcl.setFlgChecked("no");
					} else {
						fcl.setFlgChecked(null);
					}
					db.insertFilldCheckListTable(fcl);
					
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				} else {
					if (fcl.isFlgInspectedYes()) {
						fcl.setFlgChecked("yes");
					} else if (fcl.isFlgInspectedNo()) {
						fcl.setFlgChecked("no");
					} else {
						fcl.setFlgChecked(null);
					}
					db.updateFilldCheckListTable(fcl);
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				}
				db.close();

			}
		});

		/*
		 * holder.inspectedNo.setOnCheckedChangeListener(new
		 * OnCheckedChangeListener() {
		 * 
		 * @Override public void onCheckedChanged(CompoundButton buttonView,
		 * boolean isChecked) { if (holder.inspectedNo.isChecked()) {
		 * tempChild.get(holder.position).setFlgInspectedYes(true);
		 * tempChild.get(holder.position).setFlgInspectedNo(false);
		 * holder.inspectedNo.setChecked(true);
		 * holder.inspectedYes.setChecked(false); } else
		 * if(!holder.inspectedNo.isChecked()){
		 * tempChild.get(holder.position).setFlgInspectedYes(false);
		 * tempChild.get(holder.position).setFlgInspectedNo(true);
		 * holder.inspectedNo.setChecked(false);
		 * holder.inspectedYes.setChecked(true); }
		 * 
		 * } });
		 */
		holder.inspectedNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// CheckBox cb = (CheckBox) v;
				if (holder.inspectedNo.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(false);
					tempChild.get(holder.position).setFlgInspectedNo(true);
					tempChild.get(holder.position).setFlgChecked("no");
					holder.inspectedNo.setChecked(true);
					holder.inspectedYes.setChecked(false);
				} else if (!holder.inspectedNo.isChecked()) {
					tempChild.get(holder.position).setFlgInspectedYes(true);
					tempChild.get(holder.position).setFlgInspectedNo(false);
					tempChild.get(holder.position).setFlgChecked("yes");
					holder.inspectedNo.setChecked(false);
					holder.inspectedYes.setChecked(true);
				}
				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);

				/**
				 * Save data in db
				 */
				FilledCheckList fcl = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fcl.setModifiedDate(df.format(curDate));
				fcl.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();

				List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
				checkList = db.getFilledCheckListDataById(fcl
						.getiFilledCheckListId());
				if (checkList == null || checkList.size() == 0) {
					if (fcl.isFlgInspectedYes() != null
							&& fcl.isFlgInspectedYes() == true) {
						fcl.setFlgChecked("yes");
					} else if (fcl.isFlgInspectedNo() != null
							&& fcl.isFlgInspectedNo() == true) {
						fcl.setFlgChecked("no");
					} else if (fcl.isFlgInspectedYes() == null
							&& fcl.isFlgInspectedNo() == null) {
						fcl.setFlgChecked(null);
					}
					db.insertFilldCheckListTable(fcl);
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				} else {
					if (fcl.isFlgInspectedYes() != null
							&& fcl.isFlgInspectedYes() == true) {
						fcl.setFlgChecked("yes");
					} else if (fcl.isFlgInspectedNo() != null
							&& fcl.isFlgInspectedNo() == true) {
						fcl.setFlgChecked("no");
					} else if (fcl.isFlgInspectedYes() == null
							&& fcl.isFlgInspectedNo() == null) {
						fcl.setFlgChecked(null);
					}
					db.updateFilldCheckListTable(fcl);
					
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				}
				db.close();

			}
		});

		holder.commentBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tempChild.get(holder.position).isShowDesc()) {
					holder.strDesc.setVisibility(View.GONE);
					// holder.commentBtn.setVisibility(View.GONE);
					tempChild.get(holder.position).setShowDesc(false);
					holder.commentBtn.setPressed(false);

					tempChild.get(holder.position).setFlgIsDirty(1);
					CommonUtil.setEditedData(activity.getApplicationContext(),
							true);
				} else if (!tempChild.get(holder.position).isShowDesc()) {
					holder.strDesc.setVisibility(View.VISIBLE);
					holder.strDesc.requestFocus();
					// holder.commentBtn.setVisibility(View.GONE);
					tempChild.get(holder.position).setShowDesc(true);
					holder.commentBtn.setPressed(true);

				}

			}
		});

		holder.strDesc.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				// tempChild.get(holder.position).setStrDesc(s.toString());
				tempChild.get(holder.position).setStrRemarks(s.toString());

				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);
				//System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa : "+ s.toString());
				/**
				 * Save data in db
				 */
				FilledCheckList fcl = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fcl.setModifiedDate(df.format(curDate));
				fcl.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();

				List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
				checkList = db.getFilledCheckListDataById(fcl
						.getiFilledCheckListId());

				if (checkList == null || checkList.size() == 0) {

					db.insertFilldCheckListTable(fcl);
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				} else {

					db.updateFilldCheckListTable(fcl);
					db.updateDirtyRecordInMasterTable(DBHelper.VESSEL_INSPECTION,fcl.getiVesselInspectionId());
				}
				db.close();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				// tempChild.get(holder.position).setStrDesc(s.toString());
				tempChild.get(holder.position).setStrRemarks(s.toString());

				tempChild.get(holder.position).setFlgIsDirty(1);
				CommonUtil.setEditedData(activity.getApplicationContext(), true);
				//System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb : "+ s.toString());
				/**
				 * Save data in db
				 */
				FilledCheckList fcl = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fcl.setModifiedDate(df.format(curDate));
				fcl.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();

				List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
				checkList = db.getFilledCheckListDataById(fcl
						.getiFilledCheckListId());

				if (checkList == null || checkList.size() == 0) {

					db.insertFilldCheckListTable(fcl);
				} else {

					db.updateFilldCheckListTable(fcl);

				}
				db.close();
			}

			@Override
			public void afterTextChanged(Editable s) {
				System.out.println("ccccccccccccccccccccccccccccc :: "
						+ s.toString());

				// tempChild.get(holder.position).setStrDesc(s.toString());
				tempChild.get(holder.position).setStrRemarks(s.toString());

				if (s != null && !"".equals(s.toString())) {
					tempChild.get(holder.position).setFlgIsDirty(1);
					CommonUtil.setEditedData(activity.getApplicationContext(),
							true);
				}/* else {
					tempChild.get(holder.position).setFlgIsDirty(0);
				}*/

				/**
				 * Save data in db
				 */
				FilledCheckList fcl = tempChild.get(holder.position);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date curDate = new Date();
				fcl.setModifiedDate(df.format(curDate));
				fcl.setModifiedBy(CommonUtil.getUserId(activity));
				DBManager db = new DBManager(activity);
				db.open();

				List<FilledCheckList> checkList = new ArrayList<FilledCheckList>();
				checkList = db.getFilledCheckListDataById(fcl
						.getiFilledCheckListId());

				if (checkList == null || checkList.size() == 0) {

					db.insertFilldCheckListTable(fcl);
				} else {

					db.updateFilldCheckListTable(fcl);

				}
				db.close();

			}

		});

		/*
		 * convertView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * Toast.makeText(activity, tempChild.get(childPosition)+ "  : " +
		 * tempChild.get(childPosition).getStrDescription() ,
		 * Toast.LENGTH_SHORT).show();
		 * 
		 * } });
		 */

		return convertView;
	}

	public Object getGroup(int groupPosition) {
		return groupItem.get(groupPosition);
	}

	public int getGroupCount() {
		return groupItem.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	/*
	 * @SuppressLint("NewApi")
	 * 
	 * @Override public View getGroupView(int groupPosition, boolean isExpanded,
	 * View convertView, ViewGroup parent) { if (convertView == null) {
	 * convertView = minflater.inflate(R.layout.grouprow, null); }
	 * ((CheckedTextView) convertView).setText(groupItem.get(groupPosition)
	 * .getStrSectionName()); ((CheckedTextView)
	 * convertView).setChecked(isExpanded);
	 * 
	 * if(showAllGroup == 1) {
	 * convertView.setBackground(activity.getResources().
	 * getDrawable(R.drawable.active_header)); } else if(showAllGroup == 2){
	 * convertView
	 * .setBackground(activity.getResources().getDrawable(R.drawable.myrect)); }
	 * return convertView; }
	 */
	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	/**
	 * ripunjay
	 */
	@SuppressLint("NewApi")
	public void configurePinnedHeader(View v, int position, int alpha) {
		CheckedTextView header = (CheckedTextView) v;
		final String title = ((CheckListSection) getGroup(position))
				.getStrSectionName();

		header.setText(title);
		if (showAllGroup == 1) {
			header.setBackground(activity.getResources().getDrawable(
					R.drawable.active_header));
			header.setChecked(true);
		} else if (showAllGroup == 2) {
			header.setBackground(activity.getResources().getDrawable(
					R.drawable.myrect));
			header.setChecked(false);
		}

		if (alpha == 255) {
			/*
			 * header.setBackgroundColor(mPinnedHeaderBackgroundColor);
			 * header.setTextColor(mPinnedHeaderTextColor);
			 */
		} else {
			/*
			 * header.setBackgroundColor(Color.argb(alpha,
			 * Color.red(mPinnedHeaderBackgroundColor),
			 * Color.green(mPinnedHeaderBackgroundColor),
			 * Color.blue(mPinnedHeaderBackgroundColor)));
			 * header.setTextColor(Color.argb(alpha,
			 * Color.red(mPinnedHeaderTextColor),
			 * Color.green(mPinnedHeaderTextColor),
			 * Color.blue(mPinnedHeaderTextColor)));
			 */
		}
	}

	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (view instanceof PinnedHeaderExpListView) {
			((PinnedHeaderExpListView) view)
					.configureHeaderView(firstVisibleItem);
		}

	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@SuppressLint("NewApi")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TextView textView = (TextView)
		// LayoutInflater.from(mContext).inflate(R.layout.header, parent,
		// false);
		// CheckedTextView textView = (CheckedTextView)
		// LayoutInflater.from(mContext).inflate(R.layout.grouprow, parent,
		// false);
		// textView.setText(((CheckListSection)getGroup(groupPosition)).getStrSectionName());

		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.grouprow, parent, false);
		}
		((CheckedTextView) convertView).setText(groupItem.get(groupPosition)
				.getStrSectionName());
		((CheckedTextView) convertView).setChecked(isExpanded);

		if (showAllGroup == 1) {
			convertView.setBackground(activity.getResources().getDrawable(
					R.drawable.active_header));
		} else if (showAllGroup == 2) {
			convertView.setBackground(activity.getResources().getDrawable(
					R.drawable.myrect));
		}

		convertView.invalidate();
		/*
		 * if(showAllGroup == 1) {
		 * textView.setBackground(activity.getResources()
		 * .getDrawable(R.drawable.active_header)); } else if(showAllGroup ==
		 * 2){
		 * textView.setBackground(activity.getResources().getDrawable(R.drawable
		 * .myrect)); }
		 */
		return convertView;
	}

}
