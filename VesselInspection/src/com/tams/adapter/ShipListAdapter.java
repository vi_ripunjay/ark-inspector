package com.tams.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.tams.utils.ShipSelectItem;
import com.tams.vessalinspection.R;

@SuppressLint("InflateParams")
public class ShipListAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<ShipSelectItem> list = new ArrayList<ShipSelectItem>();
	private Context context;

	public ShipListAdapter(ArrayList<ShipSelectItem> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.ship_list_item, null);
		}

		// Handle TextView and display string from your list
		TextView spinnerShipname = (TextView) view
				.findViewById(R.id.spinnerShipName);		
		
		spinnerShipname.setText(list.get(position).getText());
		
		spinnerShipname.setTextColor(context.getResources().getColor(R.color.black));
		
		view.setBackgroundColor(Color.parseColor("#ffffff"));
			
		
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}