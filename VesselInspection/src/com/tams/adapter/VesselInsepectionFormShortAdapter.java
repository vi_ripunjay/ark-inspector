package com.tams.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tams.model.FilledForm;
import com.tams.model.FormSection;
import com.tams.model.FormSectionItem;
import com.tams.sql.DBManager;
import com.tams.utils.CommonUtil;
import com.tams.vessalinspection.R;
import com.tams.vessalinspection.Simple;
import com.tams.vessalinspection.VesselInspectionFormShortActivity;

@SuppressWarnings("unchecked")
public class VesselInsepectionFormShortAdapter extends BaseExpandableListAdapter {

	public List<FormSection> groupItem;
	public List<FilledForm> tempChild;
	public List<Object> Childtem = new ArrayList<Object>();
	public LayoutInflater minflater;
	public Activity activity;
	public int childPositionForOuter=0;
	
	VesselInspectionFormShortActivity viShortActivity;
	Context mContext;

	// private Integer imageIDs[];
	// private Bitmap thumbnails[];
	// private String arrPath[];

	// private int count;
	// private boolean thumbnailsselection[];

	public VesselInsepectionFormShortAdapter(Context context,
			List<FormSection> grList, List<Object> childItem) {
		mContext = context;
		viShortActivity = (VesselInspectionFormShortActivity) context;
		groupItem = grList;
		this.Childtem = childItem;
	}

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.minflater = mInflater;
		activity = act;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	private class ViewHolderVI {
		TextView text;
		EditText strHeader;
		EditText strFooter;
		ImageButton imageButton;
		ImageButton imageButtonGlView;
		@SuppressWarnings("deprecation")
		private Gallery galleryForView;
		private int position;
		private Uri[] mUrls;
		String[] mFiles = null;
		LinearLayout myGalleryL;

	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		tempChild = (ArrayList<FilledForm>) Childtem.get(groupPosition);

		final ViewHolderVI holder;
		holder = new ViewHolderVI();

		holder.mUrls = null;
		holder.mFiles = null;

		convertView = minflater.inflate(R.layout.short_form_child, null);

		holder.text = (TextView) convertView.findViewById(R.id.textView1);

		holder.strHeader = (EditText) convertView
				.findViewById(R.id.strHeaderTxt);
		holder.strFooter = (EditText) convertView
				.findViewById(R.id.strFooterTxt);
		holder.imageButton = (ImageButton) convertView
				.findViewById(R.id.imageButtonCamara);
		holder.imageButtonGlView = (ImageButton) convertView
				.findViewById(R.id.imageButtonGlView);
		holder.position = childPosition;
		childPositionForOuter = holder.position;

		holder.galleryForView = (Gallery) convertView
				.findViewById(R.id.galleryImageRegular);
		holder.myGalleryL = (LinearLayout) convertView
				.findViewById(R.id.galleryImgRegular);

		List<FormSectionItem> formSecItem = new ArrayList<FormSectionItem>();
		DBManager db = new DBManager(activity);
		db.open();
		formSecItem = db.getFormSectionItemDataById(tempChild.get(
				holder.position).getiFormSectionItemId());

		db.close();

		holder.text.setText(formSecItem.get(0).getStrItemName());
		holder.strHeader.setText(tempChild.get(holder.position)
				.getStrHeaderDesc());
		holder.strFooter.setText(tempChild.get(holder.position)
				.getStrFooterDesc());

		/*
		 * checkbox.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub CheckBox cb = (CheckBox)v; if(cb.isChecked()){
		 * tempChild.get(holder.position).setFlgChecked(true); } else {
		 * tempChild.get(holder.position).setFlgChecked(false); }
		 * 
		 * } });
		 */
		holder.strHeader.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				holder.strHeader.requestFocus();
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				holder.strHeader.requestFocus();
				System.out
						.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			}

			@Override
			public void afterTextChanged(Editable s) {
				System.out.println("ccccccccccccccccccccccccccccc");

				// TODO Auto-generated method stub

				if (holder.position < tempChild.size()) {
					tempChild.get(holder.position).setStrHeaderDesc(
							s.toString());
				}
			}
		});

		holder.strFooter.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				holder.strFooter.requestFocus();
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				holder.strFooter.requestFocus();
				System.out
						.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
			}

			@Override
			public void afterTextChanged(Editable s) {
				System.out.println("ccccccccccccccccccccccccccccc");

				// TODO Auto-generated method stub

				if (holder.position < tempChild.size()) {
					tempChild.get(holder.position).setStrFooterDesc(
							s.toString());
				}
			}
		});

		// Take picture Button

		holder.imageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// take picture
				if (holder.position < tempChild.size()) {
					int ind = holder.position;
					viShortActivity.takePicture(tempChild.get(ind));
				}

			}
		});

		holder.imageButtonGlView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (holder.position < tempChild.size()) {
					int ind = holder.position;
					viShortActivity.viewPicture(tempChild.get(ind));
				}

			}
		});

		// ggggggggggggggggggggggggggggggggggggggggggggggggggggggggg
		FilledForm flForm = new FilledForm();
		if (holder.position < tempChild.size()) {
			flForm = tempChild.get(holder.position);
		}

		String shipId = CommonUtil.getShipId(activity);
		String formId = flForm != null ? flForm.getiFilledFormId() : "form";
		//File storageDir = new File(Environment.getExternalStorageDirectory(),
			//	"/theark/vi/process/" + shipId + "/" + formId);

		File storageDir = new File(Environment.getExternalStorageDirectory(),
				activity.getResources().getString(R.string.imagePath) + shipId + "/" + formId);
		
		if (storageDir.exists()) {
			File[] imagesList = storageDir.listFiles();
			// L.fv("image list size :" + imagesList.length);
			holder.mFiles = new String[imagesList.length];

			for (int i = 0; i < imagesList.length; i++) {
				holder.mFiles[i] = imagesList[i].getAbsolutePath();
			}
			// L.fv("mFiles  size :" + mFiles.length);
			holder.mUrls = new Uri[holder.mFiles.length];

			for (int i = 0; i < holder.mFiles.length; i++) {
				holder.mUrls[i] = Uri.parse(holder.mFiles[i]);
			}
		}
		if (holder.mUrls != null && holder.mUrls.length > 0) {

		/*
		 * Uncomment for linear layout gallery
		 */
			int i = 0;
			for (String str : holder.mFiles) {
				holder.myGalleryL.addView(insertPhoto(str, i, holder.position));
				i++;
			}

			holder.galleryForView.setAdapter(new ImageAdapter(activity,
					holder.mUrls));
			// ImageView imageView = (ImageView) findViewById(R.id.formImage);
			// imageView.setImageURI(mUrls[0]);
			// L.fv("Step 44.0 : after setting adaptor");
			holder.galleryForView
					.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) {

							if (holder.position < tempChild.size()) {
								viShortActivity.viewPicture(tempChild
										.get(holder.position));
							}
							// L.fv("Step 44.1 : in listener");
							// Toast.makeText(getBaseContext(),"pic" +
							// (position + 1) +
							// " selected",Toast.LENGTH_SHORT).show();
							// display the images selected
							// ImageView imageView = (ImageView)
							// findViewById(R.id.formImage);
							// imageView.setImageURI(mUrls[position]);
							// imageView.setLayoutParams(new
							// Gallery.LayoutParams(150, 150));
							// imageView.setImageResource(imageIDs[position]);
						}
					});

		} else {
			//Toast.makeText(activity, "Form image not found", Toast.LENGTH_SHORT).show();
		}

		// ggggggggggggggggggggggggggggggggggggggggggggggggggggggggg

		/*
		 * convertView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * Toast.makeText(activity, tempChild.get(childPosition)+ "  : " +
		 * tempChild.get(childPosition).getStrDescription() ,
		 * Toast.LENGTH_SHORT).show();
		 * 
		 * } });
		 */
		return convertView;
	}

	View insertPhoto(String str, int id, int childOuterPosition) {
		// TODO Auto-generated method stub

		final int pos = childOuterPosition;
		Bitmap bm = decodeSampledBitmapFromUri(str, 100, 100);

		LinearLayout layout = new LinearLayout(activity);
		layout.setLayoutParams(new LayoutParams(150, 150));
		layout.setGravity(Gravity.CENTER);
		layout.setId(id);
		//layout.setPadding(0, 0, 2, 0);

		final ImageView imageView = new ImageView(activity);
		imageView.setLayoutParams(new LayoutParams(120, 120));
		imageView.setId(id);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setImageBitmap(bm);

		layout.addView(imageView);

		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				viShortActivity.viewPictureForGl(tempChild.get(pos), imageView.getId());
				//Toast.makeText(activity, "Image View ", Toast.LENGTH_LONG).show();
			}
		});
		return layout;
	}

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return ((ArrayList<Simple>) Childtem.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return groupItem.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = minflater.inflate(R.layout.short_group_header, null);
		}
		((CheckedTextView) convertView).setText(groupItem.get(groupPosition)
				.getStrSectionName());
		((CheckedTextView) convertView).setChecked(isExpanded);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	public class ImageAdapter extends BaseAdapter {
		private Context context;
		private int itemBackground;
		private Uri[] mUrl = null;

		public ImageAdapter(Context c, Uri[] mUrl) {
			context = c;

			this.mUrl = mUrl;
			// sets a grey background; wraps around the images
			TypedArray a = context
					.obtainStyledAttributes(R.styleable.MyGallery);
			itemBackground = a.getResourceId(
					R.styleable.MyGallery_android_galleryItemBackground, 0);
			a.recycle();
			// L.fv("Step 4.1 : enter in image adapter");
		}

		// returns the number of images
		public int getCount() {
			return (mUrl != null ? mUrl.length : 0);
		}

		// returns the ID of an item
		public Object getItem(int position) {
			return position;
		}

		// returns the ID of an item
		public long getItemId(int position) {
			return position;
		}

		// returns an ImageView view
		@SuppressWarnings("deprecation")
		public View getView(int position, View convertView, ViewGroup parent) {
			// L.fv("Step 4.2 : Image adaptor getView Method");
			ImageView imageView = new ImageView(context);

			Drawable toRecycle = imageView.getDrawable();
			if (toRecycle != null) {
				((BitmapDrawable) imageView.getDrawable()).getBitmap()
						.recycle();
			}

			imageView.setImageBitmap(decodeSampledBitmapFromResource(
					context.getResources(), R.id.formImage, 100, 100));

			// imageView.setImageURI(mUrl[position]);
			// imageView.setImageResource(imageIDs[position]);
			imageView.setLayoutParams(new Gallery.LayoutParams(220, 220));
			imageView.setBackgroundResource(itemBackground);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);

			return imageView;
		}

		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
				int reqWidth, int reqHeight) {

			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(res, resId, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeResource(res, resId, options);
		}
	}

}