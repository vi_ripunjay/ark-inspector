package com.tams.adapter;

import android.widget.CheckBox;
import android.widget.EditText;

public class ChildViewHolder {

	private CheckBox inspectedYes;
	private CheckBox inspectedNo;
	private EditText strDesc;
	private int position;
	
	
	public ChildViewHolder() {
		
	}
	
	public ChildViewHolder(CheckBox inspectedYes, CheckBox inspectedNo,
			EditText strDesc, int position) {
		
		this.inspectedYes = inspectedYes;
		this.inspectedNo = inspectedNo;
		this.strDesc = strDesc;
		this.position = position;
	}


	public CheckBox getInspectedYes() {
		return inspectedYes;
	}
	public void setInspectedYes(CheckBox inspectedYes) {
		this.inspectedYes = inspectedYes;
	}
	public CheckBox getInspectedNo() {
		return inspectedNo;
	}
	public void setInspectedNo(CheckBox inspectedNo) {
		this.inspectedNo = inspectedNo;
	}
	public EditText getStrDesc() {
		return strDesc;
	}
	public void setStrDesc(EditText strDesc) {
		this.strDesc = strDesc;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
}
